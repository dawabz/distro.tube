#+TITLE: Other Projects
#+DESCRIPTION: Distro.Tube - Other Projects
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/projects/header.org"

* Some of my projects

| *Project*                  | *Description*                                |
|--------------------------+--------------------------------------------|
| [[https://gitlab.com/dwt1/dotfiles][dotfiles]]                 | My personal dotfiles.                      |
| [[https://gitlab.com/dwt1/dmscripts][dmscripts]]                | Collection of useful dmenu scripts         |
| [[https://gitlab.com/dwt1/dmenu-distrotube][dmenu-distrotube]]         | My build of dmenu                          |
| [[https://gitlab.com/dwt1/dwm-distrotube][dwm-distrotube]]           | My build of dwm                            |
| [[https://gitlab.com/dwt1/dwmblocks-distrotube][dwmblocks-distrotube]]     | My build of dwmblocks                      |
| [[https://gitlab.com/dwt1/st-distrotube][st-distrotube]]            | My build of st                             |
| [[https://gitlab.com/dwt1/multicolor-sddm-theme][multicolor-sddm-theme]]    | An sddm theme used in DTOS                 |
| [[https://gitlab.com/dwt1/shell-color-scripts][shell-color-scripts]]      | A collection of 50+ terminal color scripts |
| [[./foss-code-of-conduct.org][The FOSS Code of Conduct]] | CoC without unnecessary political agendas  |
| [[https://gitlab.com/dwt1/wallpapers][wallpapers]]               | Collection of wallpapers for your desktop  |
