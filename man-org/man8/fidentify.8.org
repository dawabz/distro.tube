#+TITLE: Manpages - fidentify.8
#+DESCRIPTION: Linux manpage for fidentify.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fidentify - Determine file type using PhotoRec database

* SYNOPSIS
*"fidentify*/[--check]/*[directory|file]*

*"fidentify*/--version/

* DESCRIPTION
*fidentify* identify the file type, the "extension", by using the same
database than PhotoRec. When a file or directory is specified, fidentify
will output the type of file, or files under the specified directory. If
given no arguments, fidentify will output type of files under current
directory. fidentify is similar to file(1).

* OPTIONS
- *--check* :: check the file format like PhotoRec does by default

* SEE ALSO
*photorec(8),*testdisk(8),*file(1)*

* AUTHOR
PhotoRec 7.1, Data Recovery Utility, July 2019\\
Christophe GRENIER <grenier@cgsecurity.org>\\
https://www.cgsecurity.org
