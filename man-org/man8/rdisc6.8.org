#+TITLE: Manpages - rdisc6.8
#+DESCRIPTION: Linux manpage for rdisc6.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
rdisc - ICMPv6 Router Discovery tool

* SYNOPSIS
*rdisc6* [*-qv*] [*-r attempts*] [*-s source_ip*] **[*-w wait_ms*]
[*IPv6 address*] <*iface*>

* DESCRIPTON
*RDisc6* is an Unix program which implements the ICMPv6 Router Discovery
in userland (it is normally done by the kernel). It is used to lookup
the list of on-link routers and IPv6 prefixes.

It can also be used to force the kernel to update the state of a given
IPv6-autoconfigured network interface.

The name of the network interface to probe routers for must be
specified.

* OPTIONS
- *-1* or *--single* :: Exit as soon as the first advertisement is
  received.

- *-h* or *--help* :: Display some help and exit.

- *-m* or *--multiple* :: Wait for possible multiple advertisements and
  print all of them (default).

- *-n* or *--numeric* :: If the optional parameter is not a valid IPv6
  address, do not try to resolve it as a DNS hostname.

- *-q* or *--quiet* :: Only display advertised IPv6 prefixes. Display
  nothing in case of failure. That is mostly useful when calling the
  program from a shell script.

- *-r attempts* or *--retry attempts* :: Send ICMPv6 Router Discovery
  that many times until a reply is received, or abort. By default,
  rdisc6 will try 3 times before aborting (MAX_RTR_SOLICITATIONS from
  RFC2461).

- *-s source_ip* or *--source source_ip* :: Specify the IPv6 address to
  be used as the source for the router solicitation packets.

- *-V* or *--version* :: Display program version and license and exit.

- *-v* or *--verbose* :: Display verbose information. That is the
  default.

- *-w wait_ms* or *--wait wait_ms* :: Wait /wait_ms/ milliseconds for a
  response before retrying. By default, rdisc6 waits 4 second between
  each attempts (RTR_SOLICITATION_INTERVAL from RFC2461).

If *rdisc6* does not receive any response after the specified number of
attempts waiting for /wait_ms/ milliseconds each time, it will exit with
code 2. On error, it exits with code 1. Otherwise it exits with code 0.

* SECURITY
*rdisc6* must be /setuid/ /root/ to allow use by non privileged users.
It will drop its root privileges before any attempt is made to send or
receive data from the network to reduce the possible impact of a
security vulnerability.

* SEE ALSO
ndisc6(8), ipv6(7)

* AUTHOR
R�mi Denis-Courmont <remi at remlab dot net>

http://www.remlab.net/ndisc6/
