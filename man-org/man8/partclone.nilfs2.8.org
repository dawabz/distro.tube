#+TITLE: Manpages - partclone.nilfs2.8
#+DESCRIPTION: Linux manpage for partclone.nilfs2.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about partclone.nilfs2.8 is found in manpage for: [[../man8/partclone.8][man8/partclone.8]]