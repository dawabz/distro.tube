#+TITLE: Manpages - lsmod.8
#+DESCRIPTION: Linux manpage for lsmod.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lsmod - Show the status of modules in the Linux Kernel

* SYNOPSIS
*lsmod*

* DESCRIPTION
*lsmod* is a trivial program which nicely formats the contents of the
/proc/modules, showing what kernel modules are currently loaded.

* COPYRIGHT
This manual page originally Copyright 2002, Rusty Russell, IBM
Corporation. Maintained by Jon Masters and others.

* SEE ALSO
*insmod*(8), *modprobe*(8), *modinfo*(8) *depmod*(8)

* AUTHORS
*Jon Masters* <jcm@jonmasters.org>

#+begin_quote
  Developer
#+end_quote

*Lucas De Marchi* <lucas.de.marchi@gmail.com>

#+begin_quote
  Developer
#+end_quote
