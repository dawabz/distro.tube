#+TITLE: Manpages - resizepart.8
#+DESCRIPTION: Linux manpage for resizepart.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
resizepart - tell the kernel about the new size of a partition

* SYNOPSIS
*resizepart* /device partition length/

* DESCRIPTION
*resizepart* tells the Linux kernel about the new size of the specified
partition. The command is a simple wrapper around the "resize partition"
ioctl.

This command doesn't manipulate partitions on a block device.

* PARAMETERS
/device/

#+begin_quote
  The disk device.
#+end_quote

/partition/

#+begin_quote
  The partition number.
#+end_quote

/length/

#+begin_quote
  The new length of the partition (in 512-byte sectors).
#+end_quote

* SEE ALSO
*addpart*(8), *delpart*(8), *fdisk*(8), *parted*(8), *partprobe*(8),
*partx*(8)

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *resizepart* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
