#+TITLE: Manpages - pam_succeed_if.8
#+DESCRIPTION: Linux manpage for pam_succeed_if.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
pam_succeed_if - test account characteristics

* SYNOPSIS
*pam_succeed_if.so* [/flag/...] [/condition/...]

* DESCRIPTION
pam_succeed_if.so is designed to succeed or fail authentication based on
characteristics of the account belonging to the user being authenticated
or values of other PAM items. One use is to select whether to load other
modules based on this test.

The module should be given one or more conditions as module arguments,
and authentication will succeed only if all of the conditions are met.

* OPTIONS
The following /flag/s are supported:

*debug*

#+begin_quote
  Turns on debugging messages sent to syslog.
#+end_quote

*use_uid*

#+begin_quote
  Evaluate conditions using the account of the user whose UID the
  application is running under instead of the user being authenticated.
#+end_quote

*quiet*

#+begin_quote
  Dont log failure or success to the system log.
#+end_quote

*quiet_fail*

#+begin_quote
  Dont log failure to the system log.
#+end_quote

*quiet_success*

#+begin_quote
  Dont log success to the system log.
#+end_quote

*audit*

#+begin_quote
  Log unknown users to the system log.
#+end_quote

/Condition/s are three words: a field, a test, and a value to test for.

Available fields are /user/, /uid/, /gid/, /shell/, /home/, /ruser/,
/rhost/, /tty/ and /service/:

*field < number*

#+begin_quote
  Field has a value numerically less than number.
#+end_quote

*field <= number*

#+begin_quote
  Field has a value numerically less than or equal to number.
#+end_quote

*field eq number*

#+begin_quote
  Field has a value numerically equal to number.
#+end_quote

*field >= number*

#+begin_quote
  Field has a value numerically greater than or equal to number.
#+end_quote

*field > number*

#+begin_quote
  Field has a value numerically greater than number.
#+end_quote

*field ne number*

#+begin_quote
  Field has a value numerically different from number.
#+end_quote

*field = string*

#+begin_quote
  Field exactly matches the given string.
#+end_quote

*field != string*

#+begin_quote
  Field does not match the given string.
#+end_quote

*field =~ glob*

#+begin_quote
  Field matches the given glob.
#+end_quote

*field !~ glob*

#+begin_quote
  Field does not match the given glob.
#+end_quote

*field in item:item:...*

#+begin_quote
  Field is contained in the list of items separated by colons.
#+end_quote

*field notin item:item:...*

#+begin_quote
  Field is not contained in the list of items separated by colons.
#+end_quote

*user ingroup group[:group:....]*

#+begin_quote
  User is in given group(s).
#+end_quote

*user notingroup group[:group:....]*

#+begin_quote
  User is not in given group(s).
#+end_quote

*user innetgr netgroup*

#+begin_quote
  (user,host) is in given netgroup.
#+end_quote

*user notinnetgr group*

#+begin_quote
  (user,host) is not in given netgroup.
#+end_quote

* MODULE TYPES PROVIDED
All module types (*account*, *auth*, *password* and *session*) are
provided.

* RETURN VALUES
PAM_SUCCESS

#+begin_quote
  The condition was true.
#+end_quote

PAM_AUTH_ERR

#+begin_quote
  The condition was false.
#+end_quote

PAM_SERVICE_ERR

#+begin_quote
  A service error occurred or the arguments cant be parsed correctly.
#+end_quote

* EXAMPLES
To emulate the behaviour of /pam_wheel/, except there is no fallback to
group 0 being only approximated by checking also the root group
membership:

#+begin_quote
  #+begin_example
    auth required pam_succeed_if.so quiet user ingroup wheel:root
        
  #+end_example
#+end_quote

Given that the type matches, only loads the othermodule rule if the UID
is over 500. Adjust the number after default to skip several rules.

#+begin_quote
  #+begin_example
    type [default=1 success=ignore] pam_succeed_if.so quiet uid > 500
    type required othermodule.so arguments...
        
  #+end_example
#+end_quote

* SEE ALSO
*glob*(7), *pam*(8)

* AUTHOR
Nalin Dahyabhai <nalin@redhat.com>

Information about pam_succeed_if.8 is found in manpage for: [[../is designed to succeed or fail authentication based on characteristics of the account belonging to the user being authenticated or values of other PAM items\&. One use is to select whether to load other modules based on this test\&.
required pam_succeed_if\&.so quiet user ingroup wheel:root
[default=1 success=ignore] pam_succeed_if\&.so quiet uid > 500
required othermodule\&.so arguments\&.\&.\&.][is designed to succeed or fail authentication based on characteristics of the account belonging to the user being authenticated or values of other PAM items\&. One use is to select whether to load other modules based on this test\&.
required pam_succeed_if\&.so quiet user ingroup wheel:root
[default=1 success=ignore] pam_succeed_if\&.so quiet uid > 500
required othermodule\&.so arguments\&.\&.\&.]]