#+TITLE: Manpages - tdbbackup.8
#+DESCRIPTION: Linux manpage for tdbbackup.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tdbbackup - tool for backing up and for validating the integrity of
samba .tdb files

* SYNOPSIS
*tdbbackup* [-s suffix] [-v] [-h] [-n hashsize] [-l]

* DESCRIPTION
This tool is part of the *samba*(1) suite.

*tdbbackup* is a tool that may be used to backup samba .tdb files. This
tool may also be used to verify the integrity of the .tdb files prior to
samba startup or during normal operation. If it finds file damage and it
finds a prior backup the backup file will be restored.

* OPTIONS
-h

#+begin_quote
  Get help information.
#+end_quote

-s suffix

#+begin_quote
  The *-s* option allows the administrator to specify a file backup
  extension. This way it is possible to keep a history of tdb backup
  files by using a new suffix for each backup.
#+end_quote

-v

#+begin_quote
  The *-v* will check the database for damages (corrupt data) which if
  detected causes the backup to be restored.
#+end_quote

-n hashsize

#+begin_quote
  The *-n* option sets the hash size for the new backup tdb.
#+end_quote

-l

#+begin_quote
  This options disables any locking, by passing TDB_NOLOCK to
  tdb_open_ex(). Only use this for database files which are not used by
  any other process! And also only if it is otherwise not possible to
  open the database, e.g. databases which were created with mutex
  locking.
#+end_quote

* COMMANDS
/GENERAL INFORMATION/

The *tdbbackup* utility can safely be run at any time. It was designed
so that it can be used at any time to validate the integrity of tdb
files, even during Samba operation. Typical usage for the command will
be:

tdbbackup [-s suffix] *.tdb

Before restarting samba the following command may be run to validate
.tdb files:

tdbbackup -v [-s suffix] *.tdb

Samba .tdb files are stored in various locations, be sure to run backup
all .tdb file on the system. Important files includes:

#+begin_quote
  ·

  *secrets.tdb* - usual location is in the /usr/local/samba/private
  directory, or on some systems in /etc/samba.
#+end_quote

#+begin_quote
  ·

  *passdb.tdb* - usual location is in the /usr/local/samba/private
  directory, or on some systems in /etc/samba.
#+end_quote

#+begin_quote
  ·

  **.tdb* located in the /usr/local/samba/var directory or on some
  systems in the /var/cache or /var/lib/samba directories.
#+end_quote

* VERSION
This man page is correct for version 3 of the Samba suite.

* AUTHOR
The original Samba software and related utilities were created by Andrew
Tridgell. Samba is now developed by the Samba Team as an Open Source
project similar to the way the Linux kernel is developed.

The tdbbackup man page was written by John H Terpstra.
