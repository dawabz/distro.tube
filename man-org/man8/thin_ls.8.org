#+TITLE: Manpages - thin_ls.8
#+DESCRIPTION: Linux manpage for thin_ls.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*thin_ls *- List thin volumes within a pool.

* SYNOPSIS
#+begin_example
  thin_ls [options] {device|file}
#+end_example

* DESCRIPTION
*thin_ls displays infomation about thin volumes in a pool. Pass the
metadata* device on the command line, not the pool device.

This tool cannot be run on live metadata unless the *--metadata-snap*
option is used.

* OPTIONS
- **-h, --help** :: Print help and exit.

- **-V, --version** :: Print version information and exit.

- **-o, --format** :: Give a comma separated list of fields to be
  output.

#+begin_example
      Valid fields are:
        DEV, MAPPED_BLOCKS, EXCLUSIVE_BLOCKS, SHARED_BLOCKS, MAPPED_SECTORS,
        EXCLUSIVE_SECTORS, SHARED_SECTORS, MAPPED_BYTES, EXCLUSIVE_BYTES,
        SHARED_BYTES, MAPPED, EXCLUSIVE, SHARED, TRANSACTION, CREATE_TIME,
        SNAP_TIME
#+end_example

- **--no-headers** :: Don't output headers.

- **-m, --metadata-snap** :: Use metadata snapshot.

#+begin_example
      If you want to get information out of a live pool then you will need to
      take a metadata snapshot and use this switch.
#+end_example

* SEE ALSO
*thin_dump(8), thin_repair(8), thin_restore(8), thin_rmap(8),
thin_trim(8),* *thin_metadata_size(8)*

* AUTHOR
Joe Thornber <ejt@redhat.com>
