#+TITLE: Manpages - systemd-time-wait-sync.8
#+DESCRIPTION: Linux manpage for systemd-time-wait-sync.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about systemd-time-wait-sync.8 is found in manpage for: [[../systemd-time-wait-sync.service.8][systemd-time-wait-sync.service.8]]