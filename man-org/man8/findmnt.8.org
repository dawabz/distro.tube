#+TITLE: Manpages - findmnt.8
#+DESCRIPTION: Linux manpage for findmnt.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
findmnt - find a filesystem

* SYNOPSIS
*findmnt* [options]

*findmnt* [options] /device/|/mountpoint/

*findmnt* [options] [*--source*] /device/ [*--target*
/path/|*--mountpoint* /mountpoint/]

* DESCRIPTION
*findmnt* will list all mounted filesystems or search for a filesystem.
The *findmnt* command is able to search in //etc/fstab/, //etc/mtab/ or
//proc/self/mountinfo/. If /device/ or /mountpoint/ is not given, all
filesystems are shown.

The device may be specified by device name, major:minor numbers,
filesystem label or UUID, or partition label or UUID. Note that
*findmnt* follows *mount*(8) behavior where a device name may be
interpreted as a mountpoint (and vice versa) if the *--target*,
*--mountpoint* or *--source* options are not specified.

The command-line option *--target* accepts any file or directory and
then *findmnt* displays the filesystem for the given path.

The command prints all mounted filesystems in the tree-like format by
default.

* OPTIONS
*-A*, *--all*

#+begin_quote
  Disable all built-in filters and print all filesystems.
#+end_quote

*-a*, *--ascii*

#+begin_quote
  Use ascii characters for tree formatting.
#+end_quote

*-b*, *--bytes*

#+begin_quote
  Print the SIZE, USED and AVAIL columns in bytes rather than in a
  human-readable format.
#+end_quote

*-C*, *--nocanonicalize*

#+begin_quote
  Do not canonicalize paths at all. This option affects the comparing of
  paths and the evaluation of tags (LABEL, UUID, etc.).
#+end_quote

*-c*, *--canonicalize*

#+begin_quote
  Canonicalize all printed paths.
#+end_quote

*-D*, *--df*

#+begin_quote
  Imitate the output of *df*(1). This option is equivalent to *-o
  SOURCE,FSTYPE,SIZE,USED,AVAIL,USE%,TARGET* but excludes all pseudo
  filesystems. Use *--all* to print all filesystems.
#+end_quote

*-d*, *--direction* /word/

#+begin_quote
  The search direction, either *forward* or *backward*.
#+end_quote

*-e*, *--evaluate*

#+begin_quote
  Convert all tags (LABEL, UUID, PARTUUID or PARTLABEL) to the
  corresponding device names.
#+end_quote

*-F*, *--tab-file* /path/

#+begin_quote
  Search in an alternative file. If used with *--fstab*, *--mtab* or
  *--kernel*, then it overrides the default paths. If specified more
  than once, then tree-like output is disabled (see the *--list*
  option).
#+end_quote

*-f*, *--first-only*

#+begin_quote
  Print the first matching filesystem only.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help text and exit.
#+end_quote

*-i*, *--invert*

#+begin_quote
  Invert the sense of matching.
#+end_quote

*-J*, *--json*

#+begin_quote
  Use JSON output format.
#+end_quote

*-k*, *--kernel*

#+begin_quote
  Search in //proc/self/mountinfo/. The output is in the tree-like
  format. This is the default. The output contains only mount options
  maintained by kernel (see also *--mtab)*.
#+end_quote

*-l*, *--list*

#+begin_quote
  Use the list output format. This output format is automatically
  enabled if the output is restricted by the *-t*, *-O*, *-S* or *-T*
  option and the option *--submounts* is not used or if more that one
  source file (the option *-F*) is specified.
#+end_quote

*-M*, *--mountpoint* /path/

#+begin_quote
  Explicitly define the mountpoint file or directory. See also
  *--target*.
#+end_quote

*-m*, *--mtab*

#+begin_quote
  Search in //etc/mtab/. The output is in the list format by default
  (see *--tree*). The output may include user space mount options.
#+end_quote

*-N*, *--task* /tid/

#+begin_quote
  Use alternative namespace //proc/<tid>/mountinfo/ rather than the
  default //proc/self/mountinfo/. If the option is specified more than
  once, then tree-like output is disabled (see the *--list* option). See
  also the *unshare*(1) command.
#+end_quote

*-n*, *--noheadings*

#+begin_quote
  Do not print a header line.
#+end_quote

*-O*, *--options* /list/

#+begin_quote
  Limit the set of printed filesystems. More than one option may be
  specified in a comma-separated list. The *-t* and *-O* options are
  cumulative in effect. It is different from *-t* in that each option is
  matched exactly; a leading /no/ at the beginning does not have global
  meaning. The "no" can used for individual items in the list. The "no"
  prefix interpretation can be disabled by "+" prefix.
#+end_quote

*-o*, *--output* /list/

#+begin_quote
  Define output columns. See the *--help* output to get a list of the
  currently supported columns. The *TARGET* column contains tree
  formatting if the *--list* or *--raw* options are not specified.

  The default list of columns may be extended if /list/ is specified in
  the format /+list/ (e.g., *findmnt -o +PROPAGATION*).
#+end_quote

*--output-all*

#+begin_quote
  Output almost all available columns. The columns that require *--poll*
  are not included.
#+end_quote

*-P*, *--pairs*

#+begin_quote
  Produce output in the form of key="value" pairs. All potentially
  unsafe value characters are hex-escaped (\x<code>). The key (variable
  name) will be modified to contain only characters allowed for a shell
  variable identifiers, for example, FS_OPTIONS and USE_PCT instead of
  FS-OPTIONS and USE%.
#+end_quote

*-p*, *--poll*[/=list/]

#+begin_quote
  Monitor changes in the //proc/self/mountinfo/ file. Supported actions
  are: mount, umount, remount and move. More than one action may be
  specified in a comma-separated list. All actions are monitored by
  default.

  The time for which *--poll* will block can be restricted with the
  *--timeout* or *--first-only* options.

  The standard columns always use the new version of the information
  from the mountinfo file, except the umount action which is based on
  the original information cached by *findmnt*. The poll mode allows
  using extra columns:

  *ACTION*

  #+begin_quote
    mount, umount, move or remount action name; this column is enabled
    by default
  #+end_quote

  *OLD-TARGET*

  #+begin_quote
    available for umount and move actions
  #+end_quote

  *OLD-OPTIONS*

  #+begin_quote
    available for umount and remount actions
  #+end_quote
#+end_quote

*--pseudo*

#+begin_quote
  Print only pseudo filesystems.
#+end_quote

*--shadow*

#+begin_quote
  Print only filesystems over-mounted by another filesystem.
#+end_quote

*-R*, *--submounts*

#+begin_quote
  Print recursively all submounts for the selected filesystems. The
  restrictions defined by options *-t*, *-O*, *-S*, *-T* and
  *--direction* are not applied to submounts. All submounts are always
  printed in tree-like order. The option enables the tree-like output
  format by default. This option has no effect for *--mtab* or
  *--fstab*.
#+end_quote

*-r*, *--raw*

#+begin_quote
  Use raw output format. All potentially unsafe characters are
  hex-escaped (\x<code>).
#+end_quote

*--real*

#+begin_quote
  Print only real filesystems.
#+end_quote

*-S*, *--source* /spec/

#+begin_quote
  Explicitly define the mount source. Supported specifications are
  /device/, /maj/*:*/min/, *LABEL=*/label/, *UUID=*/uuid/,
  *PARTLABEL=*/label/ and *PARTUUID=*/uuid/.
#+end_quote

*-s*, *--fstab*

#+begin_quote
  Search in //etc/fstab/. The output is in the list format (see
  *--list*).
#+end_quote

*-T*, *--target* /path/

#+begin_quote
  Define the mount target. If /path/ is not a mountpoint file or
  directory, then *findmnt* checks the /path/ elements in reverse order
  to get the mountpoint (this feature is supported only when searching
  in kernel files and unsupported for *--fstab*). It's recommended to
  use the option *--mountpoint* when checks of /path/ elements are
  unwanted and /path/ is a strictly specified mountpoint.
#+end_quote

*-t*, *--types* /list/

#+begin_quote
  Limit the set of printed filesystems. More than one type may be
  specified in a comma-separated list. The list of filesystem types can
  be prefixed with *no* to specify the filesystem types on which no
  action should be taken. For more details see *mount*(8).
#+end_quote

*--tree*

#+begin_quote
  Enable tree-like output if possible. The options is silently ignored
  for tables where is missing child-parent relation (e.g., fstab).
#+end_quote

*--shadowed*

#+begin_quote
  Print only filesystems over-mounted by another filesystem.
#+end_quote

*-U*, *--uniq*

#+begin_quote
  Ignore filesystems with duplicate mount targets, thus effectively
  skipping over-mounted mount points.
#+end_quote

*-u*, *--notruncate*

#+begin_quote
  Do not truncate text in columns. The default is to not truncate the
  *TARGET*, *SOURCE*, *UUID*, *LABEL*, *PARTUUID*, *PARTLABEL* columns.
  This option disables text truncation also in all other columns.
#+end_quote

*-v*, *--nofsroot*

#+begin_quote
  Do not print a [/dir] in the SOURCE column for bind mounts or btrfs
  subvolumes.
#+end_quote

*-w*, *--timeout* /milliseconds/

#+begin_quote
  Specify an upper limit on the time for which *--poll* will block, in
  milliseconds.
#+end_quote

*-x*, *--verify*

#+begin_quote
  Check mount table content. The default is to verify //etc/fstab/
  parsability and usability. It's possible to use this option also with
  *--tab-file*. It's possible to specify source (device) or target
  (mountpoint) to filter mount table. The option *--verbose* forces
  findmnt to print more details.
#+end_quote

*--verbose*

#+begin_quote
  Force findmnt to print more information (*--verify* only for now).
#+end_quote

*--vfs-all*

#+begin_quote
  When used with *VFS-OPTIONS* column, print all VFS (fs-independent)
  flags. This option is designed for auditing purposes to list also
  default VFS kernel mount options which are normally not listed.
#+end_quote

* ENVIRONMENT
LIBMOUNT_FSTAB=<path>

#+begin_quote
  overrides the default location of the fstab file
#+end_quote

LIBMOUNT_MTAB=<path>

#+begin_quote
  overrides the default location of the mtab file
#+end_quote

LIBMOUNT_DEBUG=all

#+begin_quote
  enables libmount debug output
#+end_quote

LIBSMARTCOLS_DEBUG=all

#+begin_quote
  enables libsmartcols debug output
#+end_quote

LIBSMARTCOLS_DEBUG_PADDING=on

#+begin_quote
  use visible padding characters.
#+end_quote

* EXAMPLES
*findmnt --fstab -t nfs*

#+begin_quote
  Prints all NFS filesystems defined in //etc/fstab/.
#+end_quote

*findmnt --fstab /mnt/foo*

#+begin_quote
  Prints all //etc/fstab/ filesystems where the mountpoint directory is
  //mnt/foo/. It also prints bind mounts where //mnt/foo/ is a source.
#+end_quote

*findmnt --fstab --target /mnt/foo*

#+begin_quote
  Prints all //etc/fstab/ filesystems where the mountpoint directory is
  //mnt/foo/.
#+end_quote

*findmnt --fstab --evaluate*

#+begin_quote
  Prints all //etc/fstab/ filesystems and converts LABEL= and UUID= tags
  to the real device names.
#+end_quote

*findmnt -n --raw --evaluate --output=target LABEL=/boot*

#+begin_quote
  Prints only the mountpoint where the filesystem with label "/boot" is
  mounted.
#+end_quote

*findmnt --poll --mountpoint /mnt/foo*

#+begin_quote
  Monitors mount, unmount, remount and move on //mnt/foo/.
#+end_quote

*findmnt --poll=umount --first-only --mountpoint /mnt/foo*

#+begin_quote
  Waits for //mnt/foo/ unmount.
#+end_quote

*findmnt --poll=remount -t ext3 -O ro*

#+begin_quote
  Monitors remounts to read-only mode on all ext3 filesystems.
#+end_quote

* AUTHORS
* SEE ALSO
*fstab*(5), *mount*(8)

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *findmnt* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
