#+TITLE: Manpages - mkfs.f2fs.8
#+DESCRIPTION: Linux manpage for mkfs.f2fs.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
mkfs.f2fs - create an F2FS file system

* SYNOPSIS
*mkfs.f2fs* [ *-a* /heap-based-allocation/ ] [ *-c* /device-list/ ] [
*-d* /debug-level/ ] [ *-e* /extension-list/ ] [ *-E* /extension-list/ ]
[ *-f* ] [ *-g* ] [ *-i* ] [ *-l* /volume-label/ ] [ *-m* ] [ *-o*
/overprovision-ratio-percentage/ ] [ *-O* /feature-list/ ] [ *-C*
/encoding:flags/ ] [ *-q* ] [ *-r* ] [ *-R* /root_owner/ ] [ *-s*
/#-of-segments-per-section/ ] [ *-S* ] [ *-t* /nodiscard/discard/ ] [
*-T* /timestamp/ ] [ *-w* /wanted-sector-size/ ] [ *-z*
/#-of-sections-per-zone/ ] [ *-V* ] /device/ /[sectors]/

* DESCRIPTION
*mkfs.f2fs* is used to create a f2fs file system (usually in a disk
partition). /device/ is the special file corresponding to the device
(e.g. //dev/sdXX/). /sectors/ is optionally given for specifing the
filesystem size.

The exit code returned by *mkfs.f2fs* is 0 on success and 1 on failure.

* OPTIONS
- *-a*/ heap-based-allocation/ :: Specify 1 or 0 to enable/disable heap
  based block allocation policy. If the value is equal to 1, each of
  active log areas are initially assigned separately according to the
  whole volume size. The default value is 1.

- *-c*/ device-list/ :: Build f2fs with these additional comma separated
  devices, so that the user can see all the devices as one big volume.
  Supports up to 7 devices except meta device.

- *-d*/ debug-level/ :: Specify the level of debugging options. The
  default number is 0, which shows basic debugging messages.

- *-e*/ extension-list/ :: Specify a list of file extensions that f2fs
  will treat as cold files. The data of files with those extensions will
  be stored in the cold log. The default list includes most of the
  multimedia file extensions such as jpg, gif, mpeg, mkv, and so on.

- *-E*/ extension-list/ :: Specify a list of file extensions that f2fs
  will treat as hot files. The data of files with those extensions will
  be stored in the hot log. The default list includes database file
  extensions, such as db.

- *-f* :: Force overwrite when an existing filesystem is detected on the
  device. By default, mkfs.f2fs will not write to the device if it
  suspects that there is a filesystem or partition table on the device
  already.

- *-g* :: Add default Android options.

- *-i* :: Enable extended node bitmap. *-l*/ volume-label/ Specify the
  volume label to the partition mounted as F2FS.

- *-m* :: Specify f2fs filesystem to supports the block zoned feature.
  Without it, the filesystem doesn't support the feature.

- *-o*/ overprovision-ratio-percentage/ :: Specify the percentage of the
  volume that will be used as overprovision area. This area is hidden to
  users, and utilized by F2FS cleaner. If not specified, the best number
  will be assigned automatically according to the partition size.

- *-O*/ feature-list/ :: Set additional features for the filesystem.
  Features are comma separated, and the flag can be repeated. The
  following features are supported:

  - *encrypt* :: Enable support for filesystem level encryption.

  - *extra_attr* :: Enable extra attr feature, required for some of the
    other features.

  - *project_quota* :: Enable project ID tracking. This is used for
    projet quota accounting. Requires extra attr.

  - *inode_checksum* :: Enable inode checksum. Requires extra attr.

  - *flexible_inline_xattr* :: Enable flexible inline xattr. Requires
    extra attr.

  - *quota* :: Enable quotas.

  - *inode_crtime* :: Enable inode creation time feature. Requires extra
    attr.

  - *lost_found* :: Enable lost+found feature.

  - *verity* :: Reserved feature.

  - *sb_checksum* :: Enable superblock checksum.

  - *casefold* :: Enable casefolding support in the filesystem. Optional
    flags can be passed with *-C*

  - *compression* :: Enable support for filesystem level compression.
    Requires extra attr.

- *-C*/ encoding:flags/ :: Support casefolding with a specific encoding,
  with optional comma separated flags.

  - /encoding:/ :: 

    - *utf8* :: Use UTF-8 for casefolding.

  /flags:/

  #+begin_quote
    - *strict* :: This flag specifies that invalid strings should be
      rejected by the filesystem. Default is disabled.
  #+end_quote

- *-q* :: Quiet mode. With it, mkfs.f2fs does not show any messages,
  including the basic messages.

- *-r* :: Sets the checkpointing srand seed to 0.

- *-R* :: Give root_owner option for initial uid/gid assignment. Default
  is set by getuid()/getgid(), and assigned by "-R $uid:$gid".

- *-s*/ #-of-segments-per-section/ :: Specify the number of segments per
  section. A section consists of multiple consecutive segments, and is
  the unit of garbage collection. The default number is 1, which means
  one segment is assigned to a section.

- *-S* :: Enable sparse mode.

- *-t*/ 1/0/ :: Specify 1 or 0 to enable or disable discard policy,
  respectively. The default value is 1.

- *-T*/ timestamp/ :: Set inodes times to a given timestamp. By default,
  the current time will be used. This behaviour corresponds to the value
  -1.

- *-w*/ wanted-sector-size/ :: Specify the sector size in bytes. Without
  it, the sectors will be calculated by device sector size.

- *-z*/ #-of-sections-per-zone/ :: Specify the number of sections per
  zone. A zone consists of multiple sections. F2FS allocates segments
  for active logs with separated zones as much as possible. The default
  number is 1, which means a zone consists of one section.

- *sectors* :: Number of sectors. Default is determined by device size.

- *-V* :: Print the version number and exit.

* AUTHOR
This version of *mkfs.f2fs* has been written by Jaegeuk Kim
<jaegeuk.kim@samsung.com>.

* AVAILABILITY
*mkfs.f2fs* is available from
git://git.kernel.org/pub/scm/linux/kernel/git/jaegeuk/f2fs-tools.git.

* SEE ALSO
*mkfs*(8), *fsck.f2fs(8),* *dump.f2fs(8),* *defrag.f2fs(8),*
*resize.f2fs(8),* *sload.f2fs(8).*
