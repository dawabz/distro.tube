#+TITLE: Manpages - exfatfsck.8
#+DESCRIPTION: Linux manpage for exfatfsck.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*exfatfsck* - check an exFAT file system

* SYNOPSIS
*exfatfsck* [ *-a* | *-n* | *-p* | *-y* ] /device/\\
*exfatfsck* [ *-V* ]

* DESCRIPTION
*exfatfsck* checks an exFAT file system for errors. It can repair some
of them.

* COMMAND LINE OPTIONS
Command line options available:

- *-a* :: Automatically repair the file system. No user intervention
  required.

- *-n* :: No-operation mode: non-interactively check for errors, but
  don't write anything to the file system.

- *-p* :: Same as *-a* for compatibility with other *fsck.

- *-V* :: Print version and copyright.

- *-y* :: Same as *-a* for compatibility with other *fsck.

* EXIT CODES
Zero is returned if errors were not found. Any other code means an
error.

* AUTHOR
Andrew Nayenko

* SEE ALSO
*fsck*(8)
