#+TITLE: Manpages - pam_motd.8
#+DESCRIPTION: Linux manpage for pam_motd.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
pam_motd - Display the motd file

* SYNOPSIS
*pam_motd.so* [motd=//path/filename/] [motd_dir=//path/dirname.d/]

* DESCRIPTION
pam_motd is a PAM module that can be used to display arbitrary motd
(message of the day) files after a successful login. By default,
pam_motd shows files in the following locations:

#+begin_quote
  /etc/motd
#+end_quote

#+begin_quote
  /run/motd
#+end_quote

#+begin_quote
  /usr/lib/motd
#+end_quote

#+begin_quote
  /etc/motd.d/
#+end_quote

#+begin_quote
  /run/motd.d/
#+end_quote

#+begin_quote
  /usr/lib/motd.d/
#+end_quote

Each message size is limited to 64KB.

If /etc/motd does not exist, then /run/motd is shown. If /run/motd does
not exist, then /usr/lib/motd is shown.

Similar overriding behavior applies to the directories. Files in
/etc/motd.d/ override files with the same name in /run/motd.d/ and
/usr/lib/motd.d/. Files in /run/motd.d/ override files with the same
name in /usr/lib/motd.d/.

Files in the directories listed above are displayed in lexicographic
order by name. Moreover, the files are filtered by reading them with the
credentials of the target user authenticating on the system.

To silence a message, a symbolic link with target /dev/null may be
placed in /etc/motd.d with the same filename as the message to be
silenced. Example: Creating a symbolic link as follows silences
/usr/lib/motd.d/my_motd.

*ln -s /dev/null /etc/motd.d/my_motd*

The *MOTD_SHOWN=pam* environment variable is set after showing the motd
files, even when all of them were silenced using symbolic links.

* OPTIONS
*motd=*//path/filename/

#+begin_quote
  The /path/filename file is displayed as message of the day. Multiple
  paths to try can be specified as a colon-separated list. By default
  this option is set to /etc/motd:/run/motd:/usr/lib/motd.
#+end_quote

*motd_dir=*//path/dirname.d/

#+begin_quote
  The /path/dirname.d directory is scanned and each file contained
  inside of it is displayed. Multiple directories to scan can be
  specified as a colon-separated list. By default this option is set to
  /etc/motd.d:/run/motd.d:/usr/lib/motd.d.
#+end_quote

When no options are given, the default behavior applies for both
options. Specifying either option (or both) will disable the default
behavior for both options.

* MODULE TYPES PROVIDED
Only the *session* module type is provided.

* RETURN VALUES
PAM_ABORT

#+begin_quote
  Not all relevant data or options could be obtained.
#+end_quote

PAM_BUF_ERR

#+begin_quote
  Memory buffer error.
#+end_quote

PAM_IGNORE

#+begin_quote
  This is the default return value of this module.
#+end_quote

* EXAMPLES
The suggested usage for /etc/pam.d/login is:

#+begin_quote
  #+begin_example
    session  optional  pam_motd.so
          
  #+end_example
#+end_quote

To use a motd file from a different location:

#+begin_quote
  #+begin_example
    session  optional  pam_motd.so motd=/elsewhere/motd
          
  #+end_example
#+end_quote

To use a motd file from elsewhere, along with a corresponding .d
directory:

#+begin_quote
  #+begin_example
    session  optional  pam_motd.so motd=/elsewhere/motd motd_dir=/elsewhere/motd.d
          
  #+end_example
#+end_quote

* SEE ALSO
*motd*(5), *pam.conf*(5), *pam.d*(5), *pam*(8)

* AUTHOR
pam_motd was written by Ben Collins <bcollins@debian.org>.

The *motd_dir=* option was added by Allison Karlitskaya
<allison.karlitskaya@redhat.com>.

Information about pam_motd.8 is found in manpage for: [[../ optional  pam_motd\&.so motd=/elsewhere/motd
 optional  pam_motd\&.so motd=/elsewhere/motd motd_dir=/elsewhere/motd\&.d][ optional  pam_motd\&.so motd=/elsewhere/motd
 optional  pam_motd\&.so motd=/elsewhere/motd motd_dir=/elsewhere/motd\&.d]]