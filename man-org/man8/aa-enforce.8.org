#+TITLE: Manpages - aa-enforce.8
#+DESCRIPTION: Linux manpage for aa-enforce.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
aa-enforce - set an AppArmor security profile to enforce mode from being
disabled or complain mode.

* SYNOPSIS
*aa-enforce /<executable>/ [/<executable>/ ...] [/-d /path/to/profiles/]
[/--no-reload/]*

* OPTIONS
*-d --dir / path/to/profiles*

Specifies where to look for the AppArmor security profile set. Defaults
to /etc/apparmor.d.

*--no-reload* Do not reload the profile after modifying it.

* DESCRIPTION
*aa-enforce* is used to set one or more profiles to /enforce/ mode. This
command is only relevant in conjunction with the /aa-complain/ utility
which sets a profile to complain mode and the /aa-disable/ utility which
unloads and disables a profile. The default mode for a security policy
is enforce and the /aa-complain/ utility must be run to change this
behavior.

* BUGS
If you find any bugs, please report them at
<https://gitlab.com/apparmor/apparmor/-/issues>.

* SEE ALSO
/apparmor/ (7), /apparmor.d/ (5), /aa-complain/ (1), /aa-disable/ (1),
/aa_change_hat/ (2), and <https://wiki.apparmor.net>.
