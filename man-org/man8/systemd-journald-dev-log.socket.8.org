#+TITLE: Manpages - systemd-journald-dev-log.socket.8
#+DESCRIPTION: Linux manpage for systemd-journald-dev-log.socket.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about systemd-journald-dev-log.socket.8 is found in manpage for: [[../systemd-journald.service.8][systemd-journald.service.8]]