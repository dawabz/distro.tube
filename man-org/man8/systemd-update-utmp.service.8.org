#+TITLE: Manpages - systemd-update-utmp.service.8
#+DESCRIPTION: Linux manpage for systemd-update-utmp.service.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd-update-utmp.service, systemd-update-utmp-runlevel.service,
systemd-update-utmp - Write audit and utmp updates at bootup, runlevel
changes and shutdown

* SYNOPSIS
systemd-update-utmp.service

systemd-update-utmp-runlevel.service

/usr/lib/systemd/systemd-update-utmp

* DESCRIPTION
systemd-update-utmp-runlevel.service is a service that writes SysV
runlevel changes to utmp and wtmp, as well as the audit logs, as they
occur. systemd-update-utmp.service does the same for system reboots and
shutdown requests.

* SEE ALSO
*systemd*(1), *utmp*(5), *auditd*(8)
