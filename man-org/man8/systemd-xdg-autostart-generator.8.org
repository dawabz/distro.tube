#+TITLE: Manpages - systemd-xdg-autostart-generator.8
#+DESCRIPTION: Linux manpage for systemd-xdg-autostart-generator.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd-xdg-autostart-generator - User unit generator for XDG autostart
files

* SYNOPSIS
/usr/lib/systemd/user-generators/systemd-xdg-autostart-generator

* DESCRIPTION
systemd-xdg-autostart-generator is a generator that creates .service
units for *XDG autostart*[1] files. This permits desktop environments to
delegate startup of these applications to *systemd*(1) .

Units created by systemd-xdg-autostart-generator can be started by the
desktop environment using "xdg-desktop-autostart.target". See
*systemd.special*(7) for more details.

systemd-xdg-autostart-generator implements *systemd.generator*(7).

* SEE ALSO
*systemd*(1), *systemd.service*(5), *systemd.target*(5)

* NOTES
-  1. :: XDG autostart

  https://specifications.freedesktop.org/autostart-spec/autostart-spec-latest.html
