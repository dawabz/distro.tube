#+TITLE: Manpages - systemd-portabled.8
#+DESCRIPTION: Linux manpage for systemd-portabled.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about systemd-portabled.8 is found in manpage for: [[../systemd-portabled.service.8][systemd-portabled.service.8]]