#+TITLE: Manpages - foomatic-preferred-driver.8
#+DESCRIPTION: Linux manpage for foomatic-preferred-driver.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
foomatic-preferred-driver - <put a short description here>

* SYNOPSIS
*foomatic-preferred-driver*

[To be edited]

* SEE ALSO
/foomatic-XXX/(1),

* EXIT STATUS
*foomatic-preferred-driver* returns ...

* AUTHOR
Manfred Wassmann </manolo@NCC-1701.B.Shuttle.de/> for the foomatic
project using output from the associated binary.

* BUGS
This manpage contains no useful information.
