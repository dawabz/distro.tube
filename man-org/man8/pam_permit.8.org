#+TITLE: Manpages - pam_permit.8
#+DESCRIPTION: Linux manpage for pam_permit.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_permit - The promiscuous module

* SYNOPSIS
*pam_permit.so*

* DESCRIPTION
pam_permit is a PAM module that always permit access. It does nothing
else.

In the case of authentication, the users name will be set to /nobody/ if
the application didnt set one. Many applications and PAM modules become
confused if this name is unknown.

This module is very dangerous. It should be used with extreme caution.

* OPTIONS
This module does not recognise any options.

* MODULE TYPES PROVIDED
The *auth*, *account*, *password* and *session* module types are
provided.

* RETURN VALUES
PAM_SUCCESS

#+begin_quote
  This module always returns this value.
#+end_quote

* EXAMPLES
Add this line to your other login entries to disable account management,
but continue to permit users to log in.

#+begin_quote
  #+begin_example
    account  required  pam_permit.so
          
  #+end_example
#+end_quote

* SEE ALSO
*pam.conf*(5), *pam.d*(5), *pam*(8)

* AUTHOR
pam_permit was written by Andrew G. Morgan, <morgan@kernel.org>.
