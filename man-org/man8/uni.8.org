#+TITLE: Manpages - uni.8
#+DESCRIPTION: Linux manpage for uni.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
uni - a program to interface with the UniConf configuration system

* SYNOPSIS
*uni* get /KEY/ [/DEFAULT/]

*uni* set /KEY/ [/VALUE/]\\
*uni* xset /KEY/ [/VALUE/]

*uni* keys /KEY/\\
*uni* hkeys /KEY/\\
*uni* xkeys /KEY/

*uni* dump /KEY/\\
*uni* hdump /KEY/\\
*uni* xdump /KEY/

* DESCRIPTION
UniConf is the One True Configuration system that includes all the
others because it has plugin backends /and/ frontends. Or, less
grandiosely, it's a lightweight, distributed, cacheable tree of strings.

*uni* is used to interface directly with the UniConf system. It's
primary use is for diagnostic purposes, but it can be used to add
UniConf support to shell scripts.

* ENVIRONMENT VARIABLE
- /UNICONF/ :: Before using *uni*, you must tell it which UniConf
  moinker you wish to query by setting this environment variable.

Monikers are used to contact UniConf back-ends, be they a *uniconfd*
server, or a local file. For example, they could be:

#+begin_quote
  · a filename (ini://var/lib/app/config.ini/),\\
  · or a network address, (tcp:/open.nit.ca:4111/).
#+end_quote

* COMMANDS
- get :: Retreive the /VALUE/ associated with the provided /KEY/ within
  the UniConf database. If a /DEFAULT/ is provided, this will be
  returned if the /KEY/ has no associated /VALUE/.

- set :: Assign the provided /VALUE/ the the provided /KEY/. UniConf
  provides no guarentee that the entry committed throughout the
  database. The next “get” command for this /KEY/ may not return the
  most recently “set” value due to caching, or the existance of a
  read-only generator.

- xset :: Assign, to the provided /KEY/, the contents of the
  standard-input stream. Use this command to pipe information into the
  UniConf database.

- keys :: List all the sub-keys contained within the provided /KEY/.

- hkeys :: List all the sub-keys, recursively, contained within the
  provided /KEY/. Since any /KEY/ may contain sub-keys, UniConf provides
  no guarentee that there are no circular references.

- xkeys :: List all the sub-keys contained within the provided /KEY/,
  which can contain wildcards. See the *WILDCARDS* section.

- dump :: List all the sub-keys and their values, contained within the
  provided /KEY/.

- hdump :: List all the sub-keys and their values, recursively,
  contained within the provided /KEY/.

- xdump :: List all the sub-keys and their values, contained within the
  provided /KEY/, which can contain wildcards.

* WILDCARDS
A /KEY/ looks just like a normal slash-delimited path. The root of the
UniConf tree has a /KEY/ named “*/*”. Sub-keys can be accessed by names
such as “*/software/myapp/version*”.

With wildcards, you can access more than one key at a time.

- * :: To access a sub-key within any one level of keys, use the asterix
  like so: “*/software/*/version*”. This retrieves all keys of
  “*version*” that are one level beneath “*/software*”.

- ... :: To access a sub-key anywhere beneath a key, use the ellipsis
  like so: “*/.../version*”. This retrieves all keys of “*version*” that
  are zero or more levels beneath the root (/i.e./ any appearance of
  “*version*” within the database.

* SEE ALSO
*uniconfd*(8)

* AUTHORS
This software was written by the hackers at Net Integration
Technologies. Contact us at <wvstreams-dev@lists.nit.ca>
