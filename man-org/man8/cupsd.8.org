#+TITLE: Manpages - cupsd.8
#+DESCRIPTION: Linux manpage for cupsd.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cupsd - cups scheduler

* SYNOPSIS
*cupsd* [ *-c* /cupsd.conf/ ] [ *-f* ] [ *-F* ] [ *-h* ] [ *-l* ] [ *-s*
/cups-files.conf/ ] [ *-t* ]

* DESCRIPTION
*cupsd* is the scheduler for CUPS. It implements a printing system based
upon the Internet Printing Protocol, version 2.1, and supports most of
the requirements for IPP Everywhere. If no options are specified on the
command-line then the default configuration file //etc/cups/cupsd.conf/
will be used.

* OPTIONS
- *-c*/ cupsd.conf/ :: Uses the named cupsd.conf configuration file.

- *-f* :: Run *cupsd* in the foreground; the default is to run in the
  background as a "daemon".

- *-F* :: Run *cupsd* in the foreground but detach the process from the
  controlling terminal and current directory. This is useful for running
  *cupsd* from *init*(8).

- *-h* :: Shows the program usage.

- *-l* :: This option is passed to *cupsd* when it is run from
  *launchd*(8) or *systemd*(8).

- *-s*/ cups-files.conf/ :: Uses the named cups-files.conf configuration
  file.

- *-t* :: Test the configuration file for syntax errors.

* FILES
#+begin_example
  /etc/cups/classes.conf
  /etc/cups/cups-files.conf
  /etc/cups/cupsd.conf
  /usr/share/cups/mime/mime.convs
  /usr/share/cups/mime/mime.types
  /etc/cups/printers.conf
  /etc/cups/subscriptions.conf
#+end_example

* CONFORMING TO
*cupsd* implements all of the required IPP/2.1 attributes and
operations. It also implements several CUPS-specific administrative
operations.

* EXAMPLES
Run *cupsd* in the background with the default configuration file:

#+begin_example

      cupsd
#+end_example

Test a configuration file called /test.conf/:

#+begin_example

      cupsd -t -c test.conf
#+end_example

Run *cupsd* in the foreground with a test configuration file called
/test.conf/:

#+begin_example

      cupsd -f -c test.conf
#+end_example

* SEE ALSO
*backend*(7), *classes.conf*(5), *cups*(1), *cups-files.conf*(5),
*cups-lpd*(8), *cupsd.conf*(5), *cupsd-helper*(8), *cupsd-logs*(8),
*filter*(7), *launchd*(8), *mime.convs*(5), *mime.types*(5),
*printers.conf*(5), *systemd*(8), CUPS Online Help
(http://localhost:631/help)

* COPYRIGHT
Copyright © 2021 by OpenPrinting.
