#+TITLE: Manpages - lvmsar.8
#+DESCRIPTION: Linux manpage for lvmsar.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lvmsar --- LVM system activity reporter

* SYNOPSIS
*lvmsar*

* DESCRIPTION
lvmsar is not supported under LVM2. The device-mapper statistics
facility provides similar performance metrics using the *dmstats(8)*
command.

* SEE ALSO
*dmstats*(8), *lvm*(8)
