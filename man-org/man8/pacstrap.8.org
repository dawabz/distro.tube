#+TITLE: Manpages - pacstrap.8
#+DESCRIPTION: Linux manpage for pacstrap.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pacstrap - install packages to the specified new root directory

* SYNOPSIS
pacstrap [options] root [packages...]

* DESCRIPTION
pacstrap is designed to create a new system installation from scratch.
The specified packages will be installed into a given directory after
setting up some basic mountpoints. By default, the host system's pacman
signing keys and mirrorlist will be used to seed the chroot.

If no packages are specified to be installed, the /base/ metapackage
will be installed.

* OPTIONS
*-C* <config>

#+begin_quote
  Use an alternate config file for pacman.
#+end_quote

*-c*

#+begin_quote
  Use the package cache on the host, rather than the target.
#+end_quote

*-G*

#+begin_quote
  Avoid copying the host's pacman keyring to the target.
#+end_quote

*-i*

#+begin_quote
  Prompt for package confirmation when needed (run interactively).
#+end_quote

*-M*

#+begin_quote
  Avoid copying the host's mirrorlist to the target.
#+end_quote

*-U*

#+begin_quote
  Use pacman -U to install packages. Useful for obtaining fine-grained
  control over the installed packages.
#+end_quote

*-h*

#+begin_quote
  Output syntax and command line options.
#+end_quote

* SEE ALSO
*pacman*(8)

* BUGS
Bugs can be reported on the bug tracker /https://bugs.archlinux.org/ in
the Arch Linux category and title prefixed with [arch-install-scripts]
or via arch-projects@archlinux.org.

* AUTHORS
Maintainers:

#+begin_quote
  ·

  Dave Reisner <dreisner@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Eli Schwartz <eschwartz@archlinux.org>
#+end_quote

For additional contributors, use git shortlog -s on the
arch-install-scripts.git repository.
