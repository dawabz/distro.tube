#+TITLE: Manpages - pam_faildelay.8
#+DESCRIPTION: Linux manpage for pam_faildelay.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
pam_faildelay - Change the delay on failure per-application

* SYNOPSIS
*pam_faildelay.so* [debug] [delay=/microseconds/]

* DESCRIPTION
pam_faildelay is a PAM module that can be used to set the delay on
failure per-application.

If no *delay* is given, pam_faildelay will use the value of FAIL_DELAY
from /etc/login.defs.

* OPTIONS
*debug*

#+begin_quote
  Turns on debugging messages sent to syslog.
#+end_quote

*delay=*/N/

#+begin_quote
  Set the delay on failure to N microseconds.
#+end_quote

* MODULE TYPES PROVIDED
Only the *auth* module type is provided.

* RETURN VALUES
PAM_IGNORE

#+begin_quote
  Delay was successful adjusted.
#+end_quote

PAM_SYSTEM_ERR

#+begin_quote
  The specified delay was not valid.
#+end_quote

* EXAMPLES
The following example will set the delay on failure to 10 seconds:

#+begin_quote
  #+begin_example
    auth  optional  pam_faildelay.so  delay=10000000
          
  #+end_example
#+end_quote

* SEE ALSO
*pam_fail_delay*(3), *pam.conf*(5), *pam.d*(5), *pam*(8)

* AUTHOR
pam_faildelay was written by Darren Tucker <dtucker@zip.com.au>.

Information about pam_faildelay.8 is found in manpage for: [[../ optional  pam_faildelay\&.so  delay=10000000][ optional  pam_faildelay\&.so  delay=10000000]]