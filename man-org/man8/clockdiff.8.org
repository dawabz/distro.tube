#+TITLE: Manpages - clockdiff.8
#+DESCRIPTION: Linux manpage for clockdiff.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
clockdiff - measure clock difference between hosts

* SYNOPSIS
*clockdiff* [*-o*] [*-o1*] [*--time-format */ctime iso/] [*-V*]
{destination}

* DESCRIPTION
*clockdiff* Measures clock difference between us and /destination/ with
1 msec resolution using ICMP TIMESTAMP [2] packets or, optionally, IP
TIMESTAMP option [3] option added to ICMP ECHO. [1]

* OPTIONS
*-o*

#+begin_quote
  Use IP TIMESTAMP with ICMP ECHO instead of ICMP TIMESTAMP messages. It
  is useful with some destinations, which do not support ICMP TIMESTAMP
  (f.e. Solaris <2.4).
#+end_quote

*-o1*

#+begin_quote
  Slightly different form of *-o*, namely it uses three-term IP
  TIMESTAMP with prespecified hop addresses instead of four term one.
  What flavor works better depends on target host. Particularly, *-o* is
  better for Linux.
#+end_quote

*-T*, *--time-format */ctime iso/

#+begin_quote
  Print time stamp in output either ISO-8601 format or classical ctime
  format. The ctime format is default. The ISO time stamp includes
  timezone, and is easier to parse.
#+end_quote

*-I*

#+begin_quote
  Alias of *--time-format */iso/* * option and argument.
#+end_quote

*-h*, *--help*

#+begin_quote
  Print help and exit.
#+end_quote

*-V*, *--version*

#+begin_quote
  Print version and exit.
#+end_quote

* WARNINGS

#+begin_quote
  · Some nodes (Cisco) use non-standard timestamps, which is allowed by
  RFC, but makes timestamps mostly useless.
#+end_quote

#+begin_quote
  · Some nodes generate messed timestamps (Solaris>2.4), when run
  *xntpd*. Seems, its IP stack uses a corrupted clock source, which is
  synchronized to time-of-day clock periodically and jumps randomly
  making timestamps mostly useless. Good news is that you can use NTP in
  this case, which is even better.
#+end_quote

#+begin_quote
  · *clockdiff* shows difference in time modulo 24 days.
#+end_quote

* SEE ALSO
*ping*(8), *arping*(8), *tracepath*(8).

* REFERENCES
[1] ICMP ECHO, RFC0792, page 14.

[2] ICMP TIMESTAMP, RFC0792, page 16.

[3] IP TIMESTAMP option, RFC0791, 3.1, page 16.

* AUTHOR
*clockdiff* was compiled by Alexey Kuznetsov <kuznet@ms2.inr.ac.ru>. It
was based on code borrowed from BSD *timed* daemon.

* SECURITY
*clockdiff* requires CAP_NET_RAW capability to be executed. It is safe
to be used as set-uid root.

* AVAILABILITY
*clockdiff* is part of /iputils/ package.
