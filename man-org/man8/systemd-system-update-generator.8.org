#+TITLE: Manpages - systemd-system-update-generator.8
#+DESCRIPTION: Linux manpage for systemd-system-update-generator.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd-system-update-generator - Generator for redirecting boot to
offline update mode

* SYNOPSIS
/usr/lib/systemd/system-generators/systemd-system-update-generator

* DESCRIPTION
systemd-system-update-generator is a generator that automatically
redirects the boot process to system-update.target, if /system-update
exists. This is required to implement the logic explained in the
*systemd.offline-updates*(7).

systemd-system-update-generator implements *systemd.generator*(7).

* SEE ALSO
*systemd*(1), *systemd.special*(7)
