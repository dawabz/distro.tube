#+TITLE: Manpages - pam_wheel.8
#+DESCRIPTION: Linux manpage for pam_wheel.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_wheel - Only permit root access to members of group wheel

* SYNOPSIS
*pam_wheel.so* [debug] [deny] [group=/name/] [root_only] [trust]
[use_uid]

* DESCRIPTION
The pam_wheel PAM module is used to enforce the so-called /wheel/ group.
By default it permits access to the target user if the applicant user is
a member of the /wheel/ group. If no group with this name exist, the
module is using the group with the group-ID *0*.

* OPTIONS
*debug*

#+begin_quote
  Print debug information.
#+end_quote

*deny*

#+begin_quote
  Reverse the sense of the auth operation: if the user is trying to get
  UID 0 access and is a member of the wheel group (or the group of the
  *group* option), deny access. Conversely, if the user is not in the
  group, return PAM_IGNORE (unless *trust* was also specified, in which
  case we return PAM_SUCCESS).
#+end_quote

*group=*/name/

#+begin_quote
  Instead of checking the wheel or GID 0 groups, use the /name/ group to
  perform the authentication.
#+end_quote

*root_only*

#+begin_quote
  The check for wheel membership is done only when the target user UID
  is 0.
#+end_quote

*trust*

#+begin_quote
  The pam_wheel module will return PAM_SUCCESS instead of PAM_IGNORE if
  the user is a member of the wheel group (thus with a little play
  stacking the modules the wheel members may be able to su to root
  without being prompted for a passwd).
#+end_quote

*use_uid*

#+begin_quote
  The check will be done against the real uid of the calling process,
  instead of trying to obtain the user from the login session associated
  with the terminal in use.
#+end_quote

* MODULE TYPES PROVIDED
The *auth* and *account* module types are provided.

* RETURN VALUES
PAM_AUTH_ERR

#+begin_quote
  Authentication failure.
#+end_quote

PAM_BUF_ERR

#+begin_quote
  Memory buffer error.
#+end_quote

PAM_IGNORE

#+begin_quote
  The return value should be ignored by PAM dispatch.
#+end_quote

PAM_PERM_DENY

#+begin_quote
  Permission denied.
#+end_quote

PAM_SERVICE_ERR

#+begin_quote
  Cannot determine the user name.
#+end_quote

PAM_SUCCESS

#+begin_quote
  Success.
#+end_quote

PAM_USER_UNKNOWN

#+begin_quote
  User not known.
#+end_quote

* EXAMPLES
The root account gains access by default (rootok), only wheel members
can become root (wheel) but Unix authenticate non-root applicants.

#+begin_quote
  #+begin_example
    su      auth     sufficient     pam_rootok.so
    su      auth     required       pam_wheel.so
    su      auth     required       pam_unix.so
          
  #+end_example
#+end_quote

* SEE ALSO
*pam.conf*(5), *pam.d*(5), *pam*(8)

* AUTHOR
pam_wheel was written by Cristian Gafton <gafton@redhat.com>.
