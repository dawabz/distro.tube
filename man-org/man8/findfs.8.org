#+TITLE: Manpages - findfs.8
#+DESCRIPTION: Linux manpage for findfs.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
findfs - find a filesystem by label or UUID

* SYNOPSIS
*findfs* *NAME*=/value/

* DESCRIPTION
*findfs* will search the block devices in the system looking for a
filesystem or partition with specified tag. The currently supported tags
are:

*LABEL*=/<label>/

#+begin_quote
  Specifies filesystem label.
#+end_quote

*UUID*=/<uuid>/

#+begin_quote
  Specifies filesystem UUID.
#+end_quote

*PARTUUID*=/<uuid>/

#+begin_quote
  Specifies partition UUID. This partition identifier is supported for
  example for GUID Partition Table (GPT) partition tables.
#+end_quote

*PARTLABEL*=/<label>/

#+begin_quote
  Specifies partition label (name). The partition labels are supported
  for example for GUID Partition Table (GPT) or MAC partition tables.
#+end_quote

If the filesystem or partition is found, the device name will be printed
on stdout.

The complete overview about filesystems and partitions you can get for
example by

#+begin_quote
  *lsblk --fs*

  *partx --show <disk>*

  *blkid*\\
#+end_quote

* EXIT STATUS
*0*

#+begin_quote
  success
#+end_quote

*1*

#+begin_quote
  label or uuid cannot be found
#+end_quote

*2*

#+begin_quote
  usage error, wrong number of arguments or unknown option
#+end_quote

* ENVIRONMENT
LIBBLKID_DEBUG=all

#+begin_quote
  enables libblkid debug output.
#+end_quote

* AUTHORS
*findfs* was originally written by

and re-written for the util-linux package by

* SEE ALSO
*blkid*(8), *lsblk*(8), *partx*(8)

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *findfs* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
