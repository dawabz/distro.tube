#+TITLE: Manpages - linux32.8
#+DESCRIPTION: Linux manpage for linux32.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about linux32.8 is found in manpage for: [[../man8/setarch.8][man8/setarch.8]]