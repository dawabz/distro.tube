#+TITLE: Manpages - ip-ioam.8
#+DESCRIPTION: Linux manpage for ip-ioam.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ip-ioam - IPv6 In-situ OAM (IOAM)

* SYNOPSIS
*ip ioam* { /COMMAND/ | *help* }

*ip ioam namespace show*

*ip ioam namespace add* /ID/ * [ * *data* /DATA32/ *]* * [ * *wide*
/DATA64/ *]*

*ip ioam namespace del* /ID/

*ip ioam schema show*

*ip ioam schema add* /ID DATA/

*ip ioam schema del* /ID/

*ip ioam namespace set* /ID/ *schema* { /ID/ | *none* }

* DESCRIPTION
The *ip ioam* command is used to configure IPv6 In-situ OAM (IOAM6)
internal parameters, namely IOAM namespaces and schemas.

Those parameters also include the mapping between an IOAM namespace and
an IOAM schema.

* EXAMPLES
** Configure an IOAM namespace (ID = 1) with both data (32 bits) and
wide data (64 bits)
#+begin_example
  # ip ioam namespace add 1 data 0xdeadbeef wide 0xcafec0caf00dc0de
#+end_example

** Link an existing IOAM schema (ID = 7) to an existing IOAM namespace
(ID = 1)
#+begin_example
  # ip ioam namespace set 1 schema 7
#+end_example

* SEE ALSO
\\
*ip-route*(8)

* AUTHOR
Justin Iurman <justin.iurman@uliege.be>
