#+TITLE: Manpages - snapd-env-generator.8
#+DESCRIPTION: Linux manpage for snapd-env-generator.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
snapd-env-generator - internal tool to set /snap/bin to PATH

* SYNOPSIS

#+begin_quote

  #+begin_quote
    snapd-env-generator
  #+end_quote
#+end_quote

* DESCRIPTION
The /snapd-env-generator/ is run by systemd to ensure that the snap bin
dir (usually /snap/bin) is path of PATH.

* BUGS
Please report all bugs with /https://bugs.launchpad.net/snapd/+filebug/

* AUTHOR
michael.vogt@ubuntu.com

* COPYRIGHT
Canonical Ltd.
