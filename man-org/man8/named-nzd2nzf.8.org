#+TITLE: Manpages - named-nzd2nzf.8
#+DESCRIPTION: Linux manpage for named-nzd2nzf.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
named-nzd2nzf - convert an NZD database to NZF text format

* SYNOPSIS
*named-nzd2nzf* {filename}

* DESCRIPTION
*named-nzd2nzf* converts an NZD database to NZF format and prints it to
standard output. This can be used to review the configuration of zones
that were added to *named* via *rndc addzone*. It can also be used to
restore the old file format when rolling back from a newer version of
BIND to an older version.

* ARGUMENTS

#+begin_quote
  - **filename** :: This is the name of the *.nzd* file whose contents
    should be printed.
#+end_quote

* SEE ALSO
BIND 9 Administrator Reference Manual.

* AUTHOR
Internet Systems Consortium

* COPYRIGHT
2021, Internet Systems Consortium
