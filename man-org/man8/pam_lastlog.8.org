#+TITLE: Manpages - pam_lastlog.8
#+DESCRIPTION: Linux manpage for pam_lastlog.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
pam_lastlog - PAM module to display date of last login and perform
inactive account lock out

* SYNOPSIS
*pam_lastlog.so* [debug] [silent] [never] [nodate] [nohost] [noterm]
[nowtmp] [noupdate] [showfailed] [inactive=<days>] [unlimited]

* DESCRIPTION
pam_lastlog is a PAM module to display a line of information about the
last login of the user. In addition, the module maintains the
/var/log/lastlog file.

Some applications may perform this function themselves. In such cases,
this module is not necessary.

The module checks *LASTLOG_UID_MAX* option in /etc/login.defs and does
not update or display last login records for users with UID higher than
its value. If the option is not present or its value is invalid, no user
ID limit is applied.

If the module is called in the auth or account phase, the accounts that
were not used recently enough will be disallowed to log in. The check is
not performed for the root account so the root is never locked out. It
is also not performed for users with UID higher than the
*LASTLOG_UID_MAX* value.

* OPTIONS
*debug*

#+begin_quote
  Print debug information.
#+end_quote

*silent*

#+begin_quote
  Dont inform the user about any previous login, just update the
  /var/log/lastlog file. This option does not affect display of bad
  login attempts.
#+end_quote

*never*

#+begin_quote
  If the /var/log/lastlog file does not contain any old entries for the
  user, indicate that the user has never previously logged in with a
  welcome message.
#+end_quote

*nodate*

#+begin_quote
  Dont display the date of the last login.
#+end_quote

*noterm*

#+begin_quote
  Dont display the terminal name on which the last login was attempted.
#+end_quote

*nohost*

#+begin_quote
  Dont indicate from which host the last login was attempted.
#+end_quote

*nowtmp*

#+begin_quote
  Dont update the wtmp entry.
#+end_quote

*noupdate*

#+begin_quote
  Dont update any file.
#+end_quote

*showfailed*

#+begin_quote
  Display number of failed login attempts and the date of the last
  failed attempt from btmp. The date is not displayed when *nodate* is
  specified.
#+end_quote

*inactive=<days>*

#+begin_quote
  This option is specific for the auth or account phase. It specifies
  the number of days after the last login of the user when the user will
  be locked out by the module. The default value is 90.
#+end_quote

*unlimited*

#+begin_quote
  If the /fsize/ limit is set, this option can be used to override it,
  preventing failures on systems with large UID values that lead lastlog
  to become a huge sparse file.
#+end_quote

* MODULE TYPES PROVIDED
The *auth* and *account* module type allows one to lock out users who
did not login recently enough. The *session* module type is provided for
displaying the information about the last login and/or updating the
lastlog and wtmp files.

* RETURN VALUES
PAM_SUCCESS

#+begin_quote
  Everything was successful.
#+end_quote

PAM_SERVICE_ERR

#+begin_quote
  Internal service module error.
#+end_quote

PAM_USER_UNKNOWN

#+begin_quote
  User not known.
#+end_quote

PAM_AUTH_ERR

#+begin_quote
  User locked out in the auth or account phase due to inactivity.
#+end_quote

PAM_IGNORE

#+begin_quote
  There was an error during reading the lastlog file in the auth or
  account phase and thus inactivity of the user cannot be determined.
#+end_quote

* EXAMPLES
Add the following line to /etc/pam.d/login to display the last login
time of a user:

#+begin_quote
  #+begin_example
        session  required  pam_lastlog.so nowtmp
          
  #+end_example
#+end_quote

To reject the user if he did not login during the previous 50 days the
following line can be used:

#+begin_quote
  #+begin_example
        auth  required  pam_lastlog.so inactive=50
          
  #+end_example
#+end_quote

* FILES
/var/log/lastlog

#+begin_quote
  Lastlog logging file
#+end_quote

* SEE ALSO
*limits.conf*(5), *pam.conf*(5), *pam.d*(5), *pam*(8)

* AUTHOR
pam_lastlog was written by Andrew G. Morgan <morgan@kernel.org>.

Inactive account lock out added by Tomáš Mráz <tm@t8m.info>.

Information about pam_lastlog.8 is found in manpage for: [[../   session  required  pam_lastlog\&.so nowtmp
   auth  required  pam_lastlog\&.so inactive=50][   session  required  pam_lastlog\&.so nowtmp
   auth  required  pam_lastlog\&.so inactive=50]]