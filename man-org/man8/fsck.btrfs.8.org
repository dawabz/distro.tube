#+TITLE: Manpages - fsck.btrfs.8
#+DESCRIPTION: Linux manpage for fsck.btrfs.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fsck.btrfs - do nothing, successfully

* SYNOPSIS
*fsck.btrfs* [-aApy] [/<device>/...]

* DESCRIPTION
*fsck.btrfs* is a type of utility that should exist for any filesystem
and is called during system setup when the corresponding */etc/fstab*
entries contain non-zero value for *fs_passno*, see *fstab*(5) for more.

Traditional filesystems need to run their respective fsck utility in
case the filesystem was not unmounted cleanly and the log needs to be
replayed before mount. This is not needed for BTRFS. You should set
fs_passno to 0.

If you wish to check the consistency of a BTRFS filesystem or repair a
damaged filesystem, see *btrfs-check*(8). By default filesystem
consistency is checked, the repair mode is enabled via the /--repair/
option (use with care!).

* OPTIONS
The options are all the same and detect if *fsck.btrfs* is executed in
non-interactive mode and exits with success, otherwise prints a message
about btrfs check.

* EXIT STATUS
There are two possible exit codes returned:

0

#+begin_quote
  No error
#+end_quote

8

#+begin_quote
  Operational error, eg. device does not exist
#+end_quote

* FILES
*/etc/fstab*

* SEE ALSO
*btrfs*(8), *fsck*(8), *fstab*(5)
