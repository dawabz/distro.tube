#+TITLE: Manpages - openconnect.8
#+DESCRIPTION: Linux manpage for openconnect.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
openconnect - Multi-protocol VPN client, for Cisco AnyConnect VPNs and
others

* SYNOPSIS
*openconnect* [ *--config* configfile ] [ *-b,--background* ] [
*--pid-file* pidfile ] [ *-c,--certificate* cert ] [
*-e,--cert-expire-warning* days ] [ *-k,--sslkey* key ] [ *-C,--cookie*
cookie ] [ *--cookie-on-stdin* ] [ *--compression* MODE ] [
*-d,--deflate* ] [ *-D,--no-deflate* ] [ *--force-dpd* interval ] [
*--force-trojan* interval ] [ *-F,--form-entry* form:opt=value ] [
*-g,--usergroup* group ] [ *-h,--help* ] [ *--http-auth* methods ] [
*-i,--interface* ifname ] [ *-l,--syslog* ] [ *--timestamp* ] [
*--passtos* ] [ *-U,--setuid* user ] [ *--csd-user* user ] [ *-m,--mtu*
mtu ] [ *--base-mtu* mtu ] [ *-p,--key-password* pass ] [ *-P,--proxy*
proxyurl ] [ *--proxy-auth* methods ] [ *--no-proxy* ] [ *--libproxy* ]
[ *--key-password-from-fsid* ] [ *-q,--quiet* ] [ *-Q,--queue-len* len ]
[ *-s,--script* vpnc-script ] [ *-S,--script-tun* ] [ *-u,--user* name ]
[ *-V,--version* ] [ *-v,--verbose* ] [ *-x,--xmlconfig* config ] [
*--authgroup* group ] [ *--authenticate* ] [ *--cookieonly* ] [
*--printcookie* ] [ *--cafile* file ] [ *--disable-ipv6* ] [
*--dtls-ciphers* list ] [ *--dtls12-ciphers* list ] [
*--dtls-local-port* port ] [ *--dump-http-traffic* ] [
*--no-system-trust* ] [ *--pfs* ] [ *--no-dtls* ] [
*--no-http-keepalive* ] [ *--no-passwd* ] [ *--no-xmlpost* ] [
*--non-inter* ] [ *--passwd-on-stdin* ] [ *--protocol* proto ] [
*--token-mode* mode ] [ *--token-secret* {secret[/,counter/]|@/file/} ]
[ *--reconnect-timeout* ] [ *--resolve* host:ip ] [ *--servercert* sha1
] [ *--useragent* string ] [ *--version-string* string ] [
*--local-hostname* string ] [ *--os* string ]
*[https://]/server/*[:*/port/*][/*/group/*]**

* DESCRIPTION
The program *openconnect* connects to VPN servers which use standard
TLS/SSL, DTLS, and ESP protocols for data transport.

It was originally written to support Cisco "AnyConnect" VPN servers, and
has since been extended with experimental support for Juniper Network
Connect (*--protocol=nc*) and Junos Pulse VPN servers
(*--protocol=pulse*) and PAN GlobalProtect VPN servers
(*--protocol=gp*).

The connection happens in two phases. First there is a simple HTTPS
connection over which the user authenticates somehow - by using a
certificate, or password or SecurID, etc. Having authenticated, the user
is rewarded with an authentication cookie which can be used to make the
real VPN connection.

The second phase uses that cookie to connect to a tunnel via HTTPS, and
data packets can be passed over the resulting connection. When possible,
a UDP tunnel is also configured: AnyConnect uses DTLS, while Juniper and
GlobalProtect use UDP-encapsulated ESP. The UDP tunnel may be disabled
with *--no-dtls*, but is preferred when correctly supported by the
server and network for performance reasons. (TCP performs poorly and
unreliably over TCP-based tunnels; see
/http://sites.inka.de/~W1011/devel/tcp-tcp.html/.)

* OPTIONS
- *--config=CONFIGFILE* :: Read further options from /CONFIGFILE/ before
  continuing to process options from the command line. The file should
  contain long-format options as would be accepted on the command line,
  but without the two leading -- dashes. Empty lines, or lines where the
  first non-space character is a # character, are ignored.

Any option except the *config* option may be specified in the file.

- *-b,--background* :: Continue in background after startup

- *--pid-file=PIDFILE* :: Save the pid to /PIDFILE/ when backgrounding

- *-c,--certificate=CERT* :: Use SSL client certificate /CERT/ which may
  be either a file name or, if OpenConnect has been built with an
  appropriate version of GnuTLS, a PKCS#11 URL.

- *-e,--cert-expire-warning=DAYS* :: Give a warning when SSL client
  certificate has /DAYS/ left before expiry

- *-k,--sslkey=KEY* :: Use SSL private key /KEY/ which may be either a
  file name or, if OpenConnect has been built with an appropriate
  version of GnuTLS, a PKCS#11 URL.

- *-C,--cookie=COOKIE* :: Use authentication cookie /COOKIE/.

- *--cookie-on-stdin* :: Read cookie from standard input.

- *-d,--deflate* :: Enable all compression, including stateful modes. By
  default, only stateless compression algorithms are enabled.

- *-D,--no-deflate* :: Disable all compression.

- *--compression=MODE* :: Set compression mode, where /MODE/ is one of
  /stateless/, /none/, or /all/.

By default, only stateless compression algorithms which do not maintain
state from one packet to the next (and which can be used on UDP
transports) are enabled. By setting the mode to /all/ stateful
algorithms (currently only zlib deflate) can be enabled. Or all
compression can be disabled by setting the mode to /none/.

- *--force-dpd=INTERVAL* :: Use /INTERVAL/ as minimum Dead Peer
  Detection interval (in seconds) for CSTP and DTLS, forcing use of DPD
  even when the server doesn't request it.

- *-g,--usergroup=GROUP* :: Use /GROUP/ as login UserGroup

- *-F,--form-entry=FORM:OPTION=VALUE* :: Provide authentication form
  input, where /FORM/ and /OPTION/ are the identifiers from the form and
  the specific input field, and /VALUE/ is the string to be filled in
  automatically. For example, the standard username field /(also handled
  by the --user option)/ could also be provided with this option thus:
  /--form-entry/ /main:username=joebloggs/.

- *-h,--help* :: Display help text

- *--http-auth=METHODS* :: Use only the specified methods for HTTP
  authentication to a server. By default, only Negotiate, NTLM and
  Digest authentication are enabled. Basic authentication is also
  supported but because it is insecure it must be explicitly enabled.
  The argument is a comma-separated list of methods to be enabled. Note
  that the order does not matter: OpenConnect will use Negotiate, NTLM,
  Digest and Basic authentication in that order, if each is enabled,
  regardless of the order specified in the METHODS string.

- *-i,--interface=IFNAME* :: Use /IFNAME/ for tunnel interface

- *-l,--syslog* :: Use syslog for progress messages

- *--timestamp* :: Prepend a timestamp to each progress message

- *--passtos* :: Copy TOS / TCLASS of payload packet into DTLS and ESP
  packets. This is not set by default because it may leak information
  about the payload (for example, by differentiating voice/video
  traffic).

- *-U,--setuid=USER* :: Drop privileges after connecting, to become user
  /USER/

- *--csd-user=USER* :: Drop privileges during execution of trojan binary
  or script (CSD, TNCC, or HIP).

- *--csd-wrapper=SCRIPT* :: Run /SCRIPT/ instead of the trojan binary or
  script.

- *--force-trojan=INTERVAL* :: Use /INTERVAL/ as interval (in seconds)
  for repeat execution of Trojan binary or script, overriding default
  and/or server-set interval.

- *-m,--mtu=MTU* :: Request /MTU/ from server as the MTU of the tunnel.

- *--base-mtu=MTU* :: Indicate /MTU/ as the path MTU between client and
  server on the unencrypted network. Newer servers will automatically
  calculate the MTU to be used on the tunnel from this value.

- *-p,--key-password=PASS* :: Provide passphrase for certificate file,
  or SRK (System Root Key) PIN for TPM

- *-P,--proxy=PROXYURL* :: Use HTTP or SOCKS proxy for connection. A
  username and password can be provided in the given URL, and will be
  used for authentication. If authentication is required but no
  credentials are given, GSSAPI and automatic NTLM authentication using
  Samba's ntlm_auth helper tool may be attempted.

- *--proxy-auth=METHODS* :: Use only the specified methods for HTTP
  authentication to a proxy. By default, only Negotiate, NTLM and Digest
  authentication are enabled. Basic authentication is also supported but
  because it is insecure it must be explicitly enabled. The argument is
  a comma-separated list of methods to be enabled. Note that the order
  does not matter: OpenConnect will use Negotiate, NTLM, Digest and
  Basic authentication in that order, if each is enabled, regardless of
  the order specified in the METHODS string.

- *--no-proxy* :: Disable use of proxy

- *--libproxy* :: Use libproxy to configure proxy automatically (when
  built with libproxy support)

- *--key-password-from-fsid* :: Passphrase for certificate file is
  automatically generated from the /fsid/ of the file system on which it
  is stored. The /fsid/ is obtained from the *statvfs*(2) or *statfs*(2)
  system call, depending on the operating system. On a Linux or similar
  system with GNU coreutils, the /fsid/ used by this option should be
  equal to the output of the command:

#+begin_example
  stat --file-system --printf=%i\\n $CERTIFICATE
#+end_example

It is not the same as the 128-bit UUID of the file system.

- *-q,--quiet* :: Less output

- *-Q,--queue-len=LEN* :: Set packet queue limit to /LEN/ pkts

- *-s,--script=SCRIPT* :: Invoke /SCRIPT/ to configure the network after
  connection. Without this, routing and name service are unlikely to
  work correctly. The script is expected to be compatible with the
  *vpnc-script* which is shipped with the "vpnc" VPN client. See
  /http://www.infradead.org/openconnect/vpnc-script.html/ for more
  information. This version of OpenConnect is configured to use
  */etc/vpnc/vpnc-script* by default.

On Windows, a relative directory for the default script will be handled
as starting from the directory that the openconnect executable is
running from, rather than the current directory. The script will be
invoked with the command-based script host *cscript.exe*.

- *-S,--script-tun* :: Pass traffic to 'script' program over a UNIX
  socket, instead of to a kernel tun/tap device. This allows the VPN IP
  traffic to be handled entirely in userspace, for example by a program
  which uses lwIP to provide SOCKS access into the VPN.

- *-u,--user=NAME* :: Set login username to /NAME/

- *-V,--version* :: Report version number

- *-v,--verbose* :: More output (may be specified multiple times for
  additional output)

- *-x,--xmlconfig=CONFIG* :: XML config file

- *--authgroup=GROUP* :: Choose authentication login selection

- *--authenticate* :: Authenticate only, and output the information
  needed to make the connection a form which can be used to set shell
  environment variables. When invoked with this option, openconnect will
  not make the connection, but if successful will output something like
  the following to stdout:

#+begin_example
  COOKIE=3311180634@13561856@1339425499@B315A0E29D16C6FD92EE...
  HOST=10.0.0.1
  FINGERPRINT=469bb424ec8835944d30bc77c77e8fc1d8e23a42
#+end_example

Thus, you can invoke openconnect as a non-privileged user /(with access
to the user's PKCS#11 tokens, etc.)/ for authentication, and then invoke
openconnect separately to make the actual connection as root:

#+begin_example
  eval `openconnect --authenticate https://vpnserver.example.com`;
  [ -n $COOKIE ] && echo $COOKIE |
   sudo openconnect --cookie-on-stdin $HOST --servercert $FINGERPRINT
#+end_example

- *--cookieonly* :: Fetch and print cookie only; don't connect

- *--printcookie* :: Print cookie before connecting

- *--cafile=FILE* :: Additional CA file for server verification. By
  default, this simply causes OpenConnect to trust additional root CA
  certificate(s) in addition to those trusted by the system. Use
  *--no-system-trust* to prevent OpenConnect from trusting the system
  default certificate authorities.

- *--no-system-trust* :: Do not trust the system default certificate
  authorities. If this option is given, only certificate authorities
  given with the *--cafile* option, if any, will be trusted
  automatically.

- *--disable-ipv6* :: Do not advertise IPv6 capability to server

- *--dtls-ciphers=LIST* :: Set OpenSSL ciphers to support for DTLS

- *--dtls12-ciphers=LIST* :: Set OpenSSL ciphers for Cisco's DTLS v1.2

- *--dtls-local-port=PORT* :: Use /PORT/ as the local port for DTLS and
  UDP datagrams

- *--dump-http-traffic* :: Enable verbose output of all HTTP requests
  and the bodies of all responses received from the server.

- *--pfs* :: Enforces Perfect Forward Secrecy (PFS). That ensures that
  if the server's long-term key is compromised, any session keys
  established before the compromise will be unaffected. If this option
  is provided and the server does not support PFS in the TLS channel the
  connection will fail.

PFS is available in Cisco ASA releases 9.1(2) and higher; a suitable
cipher suite may need to be manually enabled by the administrator using
the *ssl encryption* setting.

- *--no-dtls* :: Disable DTLS and ESP

- *--no-http-keepalive* :: Version 8.2.2.5 of the Cisco ASA software has
  a bug where it will forget the client's SSL certificate when HTTP
  connections are being re-used for multiple requests. So far, this has
  only been seen on the initial connection, where the server gives an
  HTTP/1.0 redirect response with an explicit *Connection: Keep-Alive*
  directive. OpenConnect as of v2.22 has an unconditional workaround for
  this, which is never to obey that directive after an HTTP/1.0
  response.

However, Cisco's support team has failed to give any competent response
to the bug report and we don't know under what other circumstances their
bug might manifest itself. So this option exists to disable ALL re-use
of HTTP sessions and cause a new connection to be made for each request.
If your server seems not to be recognising your certificate, try this
option. If it makes a difference, please report this information to the
*openconnect-devel@lists.infradead.org* mailing list.

- *--no-passwd* :: Never attempt password (or SecurID) authentication.

- *--no-xmlpost* :: Do not attempt to post an XML
  authentication/configuration request to the server; use the old style
  GET method which was used by older clients and servers instead.

This option is a temporary safety net, to work around potential
compatibility issues with the code which falls back to the old method
automatically. It causes OpenConnect to behave more like older versions
(4.08 and below) did. If you find that you need to use this option, then
you have found a bug in OpenConnect. Please see
http://www.infradead.org/openconnect/mail.html and report this to the
developers.

- *--non-inter* :: Do not expect user input; exit if it is required.

- *--passwd-on-stdin* :: Read password from standard input

- *--protocol=PROTO* :: Select VPN protocol /PROTO/ to be used for the
  connection. Supported protocols are /anyconnect/ for Cisco AnyConnect
  (the default), /nc/ for experimental support for Juniper Network
  Connect (also supported by most Junos Pulse servers), /pulse/ for
  experimental support for Junos Pulse, and /gp/ for experimental
  support for PAN GlobalProtect.

OpenConnect does not yet support all of the authentication options used
by Pulse, nor does it support Host Checker/TNCC with Pulse. If your
Junos Pulse VPN is not yet supported with *--protocol=pulse*, then
*--protocol=nc* may be a useful fallback option.

- *--token-mode=MODE* :: Enable one-time password generation using the
  /MODE/ algorithm. *--token-mode=rsa* will call libstoken to generate
  an RSA SecurID tokencode, *--token-mode=totp* will call liboath to
  generate an RFC 6238 time-based password, and *--token-mode=hotp* will
  call liboath to generate an RFC 4226 HMAC-based password. Yubikey
  tokens which generate OATH codes in hardware are supported with
  *--token-mode=yubioath. --token-mode=oidc will use the provided*
  OpenIDConnect token as an RFC 6750 bearer token.

- *--token-secret={ SECRET[,COUNTER] | @FILENAME }* :: The secret to use
  when generating one-time passwords/verification codes. Base 32-encoded
  TOTP/HOTP secrets can be used by specifying "base32:" at the beginning
  of the secret, and for HOTP secrets the token counter can be specified
  following a comma.

RSA SecurID secrets can be specified as an Android/iPhone URI or a raw
numeric CTF string (with or without dashes).

For Yubikey OATH the token secret specifies the name of the credential
to be used. If not provided, the first OATH credential found on the
device will be used.

For OIDC the secret is the bearer token to be used.

/FILENAME/, if specified, can contain any of the above strings. Or, it
can contain a SecurID XML (SDTID) seed.

If this option is omitted, and --token-mode is "rsa", libstoken will try
to use the software token seed saved in *~/.stokenrc* by the "stoken
import" command.

- *--reconnect-timeout* :: Keep reconnect attempts until so much seconds
  are elapsed. The default timeout is 300 seconds, which means that
  openconnect can recover VPN connection after a temporary network down
  time of 300 seconds.

- *--resolve=HOST:IP* :: Automatically resolve the hostname /HOST/ to
  /IP/ instead of using the normal resolver to look it up.

- *--servercert=HASH* :: Accept server's SSL certificate only if the
  provided fingerprint matches. The allowed fingerprint types are
  /SHA1/, /SHA256/, and /PIN-SHA256/. They are distinguished by the
  'sha1:', 'sha256:' and 'pin-sha256:' prefixes to the encoded hash. The
  first two are custom identifiers providing hex encoding of the peer's
  public key, while 'pin-sha256:' is the RFC7469 key PIN, which utilizes
  base64 encoding. To ease certain testing use-cases, a partial match of
  the hash will also be accepted, if it is at least 4 characters past
  the prefix.

- *--useragent=STRING* :: Use /STRING/ as 'User-Agent:' field value in
  HTTP header. (e.g. --useragent 'Cisco AnyConnect VPN Agent for Windows
  2.2.0133')

- *--version-string=STRING* :: Use /STRING/ as the software version
  reported to the head end. (e.g. --version-string '2.2.0133')

- *--local-hostname=STRING* :: Use /STRING/ as 'X-CSTP-Hostname:' field
  value in HTTP header. For example --local-hostname 'mypc', will
  advertise the value 'mypc' as the suggested hostname to point to the
  provided IP address.

- *--os=STRING* :: OS type to report to gateway. Recognized values are:
  *linux*, *linux-64*, *win*, *mac-intel*, *android*, *apple-ios*.
  Reporting a different OS type may affect the dynamic access policy
  (DAP) applied to the VPN session. If the gateway requires CSD, it will
  also cause the corresponding CSD trojan binary to be downloaded, so
  you may need to use *--csd-wrapper* if this code is not executable on
  the local machine.

* SIGNALS
In the data phase of the connection, the following signals are handled:

- *SIGINT / SIGTERM* :: performs a clean shutdown by logging the session
  off, disconnecting from the gateway, and running the vpnc-script to
  restore the network configuration.

- *SIGHUP* :: disconnects from the gateway and runs the vpnc-script, but
  does not log the session off; this allows for reconnection later using
  *--cookie*.

- *SIGUSR2* :: forces an immediate disconnection and reconnection; this
  can be used to quickly recover from LAN IP address changes.

* LIMITATIONS
Note that although IPv6 has been tested on all platforms on which
*openconnect* is known to run, it depends on a suitable *vpnc-script* to
configure the network. The standard *vpnc-script* shipped with vpnc
0.5.3 is not capable of setting up IPv6 routes; the one from
*git://git.infradead.org/users/dwmw2/vpnc-scripts.git* will be required.

* SEE ALSO
*ocserv*(8)

* AUTHORS
David Woodhouse <dwmw2@infradead.org>
