#+TITLE: Manpages - sudo_sendlog.8
#+DESCRIPTION: Linux manpage for sudo_sendlog.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
can be used to send the existing

I/O log

to a remote log server such as

for central storage.

The options are as follows:

Only send the accept event, not the I/O associated with the log. This
can be used to test the logging of accept events without any associated
I/O.

The path to a certificate authority bundle file, in PEM format, to use
instead of the system's default certificate authority database when
authenticating the log server. The default is to use the system's
default certificate authority database.

The path to the client's certificate file in PEM format. This setting is
required when the connection to the remote log server is secured with
TLS.

Display a short help message to the standard output and exit.

Connect to the specified

instead of localhost.

Use the specified

when restarting a log transfer. The

is reported by the server when it creates the remote I/O log. This
option may only be used in conjunction with the

option.

The path to the client's private key file in PEM format. This setting is
required when the connection to the remote log server is secured with
TLS.

If specified, the server's certificate will not be verified during the
TLS handshake. By default,

verifies that the server's certificate is valid and that it contains
either the server's host name or its IP address. This setting is only
supported when the connection to the remote log server is secured with
TLS.

Use the specified network

when connecting to the log server instead of the default, port 30344.

Restart an interrupted connection to the log server. The specified

is used to tell the server the point in time at which to continue the
log. The

is specified in the form

and is usually the last commit point received from the server. The

option must also be specified when restarting a transfer.

Send a reject event for the command using the specified

even though it was actually accepted locally. This can be used to test
the logging of reject events; no I/O will be sent.

Stop sending log records and close the connection when

is reached. This can be used for testing purposes to send a partial I/O
log to the server. Partial logs can be restarted using the

option. The

is an elapsed time specified in the form

Open

simultaneous connections to the log server and send the specified I/O
log file on each one. This option is useful for performance testing.

Print the

version and exit.

supports a flexible debugging framework that is configured via

lines in the

file.

For more information on configuring

please refer to its manual.

Sudo front end configuration

Many people have worked on

over the years; this version consists of code written primarily by:

See the CONTRIBUTORS file in the

distribution (https://www.sudo.ws/contributors.html) for an exhaustive
list of people who have contributed to

If you feel you have found a bug in

please submit a bug report at https://bugzilla.sudo.ws/

Limited free support is available via the sudo-users mailing list, see
https://www.sudo.ws/mailman/listinfo/sudo-users to subscribe or search
the archives.

is provided

and any express or implied warranties, including, but not limited to,
the implied warranties of merchantability and fitness for a particular
purpose are disclaimed. See the LICENSE file distributed with

or https://www.sudo.ws/license.html for complete details.
