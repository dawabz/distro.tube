#+TITLE: Manpages - systemd-tmpfiles-setup.service.8
#+DESCRIPTION: Linux manpage for systemd-tmpfiles-setup.service.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about systemd-tmpfiles-setup.service.8 is found in manpage for: [[../systemd-tmpfiles.8][systemd-tmpfiles.8]]