#+TITLE: Manpages - pam_issue.8
#+DESCRIPTION: Linux manpage for pam_issue.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
pam_issue - PAM module to add issue file to user prompt

* SYNOPSIS
*pam_issue.so* [noesc] [issue=/issue-file-name/]

* DESCRIPTION
pam_issue is a PAM module to prepend an issue file to the username
prompt. It also by default parses escape codes in the issue file similar
to some common gettys (using \x format).

Recognized escapes:

*\d*

#+begin_quote
  current day
#+end_quote

*\l*

#+begin_quote
  name of this tty
#+end_quote

*\m*

#+begin_quote
  machine architecture (uname -m)
#+end_quote

*\n*

#+begin_quote
  machines network node hostname (uname -n)
#+end_quote

*\o*

#+begin_quote
  domain name of this system
#+end_quote

*\r*

#+begin_quote
  release number of operating system (uname -r)
#+end_quote

*\t*

#+begin_quote
  current time
#+end_quote

*\s*

#+begin_quote
  operating system name (uname -s)
#+end_quote

*\u*

#+begin_quote
  number of users currently logged in
#+end_quote

*\U*

#+begin_quote
  same as \u except it is suffixed with "user" or "users" (eg. "1 user"
  or "10 users")
#+end_quote

*\v*

#+begin_quote
  operating system version and build date (uname -v)
#+end_quote

* OPTIONS
*noesc*

#+begin_quote
  Turns off escape code parsing.
#+end_quote

*issue=*/issue-file-name/

#+begin_quote
  The file to output if not using the default.
#+end_quote

* MODULE TYPES PROVIDED
Only the *auth* module type is provided.

* RETURN VALUES
PAM_BUF_ERR

#+begin_quote
  Memory buffer error.
#+end_quote

PAM_IGNORE

#+begin_quote
  The prompt was already changed.
#+end_quote

PAM_SERVICE_ERR

#+begin_quote
  A service module error occurred.
#+end_quote

PAM_SUCCESS

#+begin_quote
  The new prompt was set successfully.
#+end_quote

* EXAMPLES
Add the following line to /etc/pam.d/login to set the user specific
issue at login:

#+begin_quote
  #+begin_example
            auth optional pam_issue.so issue=/etc/issue
          
  #+end_example
#+end_quote

* SEE ALSO
*pam.conf*(5), *pam.d*(5), *pam*(8)

* AUTHOR
pam_issue was written by Ben Collins <bcollins@debian.org>.

Information about pam_issue.8 is found in manpage for: [[../       auth optional pam_issue\&.so issue=/etc/issue][       auth optional pam_issue\&.so issue=/etc/issue]]