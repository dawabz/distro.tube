#+TITLE: Manpages - pam_exec.8
#+DESCRIPTION: Linux manpage for pam_exec.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
pam_exec - PAM module which calls an external command

* SYNOPSIS
*pam_exec.so* [debug] [expose_authtok] [seteuid] [quiet] [quiet_log]
[stdout] [log=/file/] [type=/type/] /command/ [/.../]

* DESCRIPTION
pam_exec is a PAM module that can be used to run an external command.

The childs environment is set to the current PAM environment list, as
returned by *pam_getenvlist*(3) In addition, the following PAM items are
exported as environment variables: /PAM_RHOST/, /PAM_RUSER/,
/PAM_SERVICE/, /PAM_TTY/, /PAM_USER/ and /PAM_TYPE/, which contains one
of the module types: *account*, *auth*, *password*, *open_session* and
*close_session*.

Commands called by pam_exec need to be aware of that the user can have
control over the environment.

* OPTIONS
*debug*

#+begin_quote
  Print debug information.
#+end_quote

*expose_authtok*

#+begin_quote
  During authentication the calling command can read the password from
  *stdin*(3). Only first /PAM_MAX_RESP_SIZE/ bytes of a password are
  provided to the command.
#+end_quote

*log=*/file/

#+begin_quote
  The output of the command is appended to file
#+end_quote

*type=*/type/

#+begin_quote
  Only run the command if the module type matches the given type.
#+end_quote

*stdout*

#+begin_quote
  Per default the output of the executed command is written to
  /dev/null. With this option, the stdout output of the executed command
  is redirected to the calling application. Its in the responsibility of
  this application what happens with the output. The *log* option is
  ignored.
#+end_quote

*quiet*

#+begin_quote
  Per default pam_exec.so will echo the exit status of the external
  command if it fails. Specifying this option will suppress the message.
#+end_quote

*quiet_log*

#+begin_quote
  Per default pam_exec.so will log the exit status of the external
  command if it fails. Specifying this option will suppress the log
  message.
#+end_quote

*seteuid*

#+begin_quote
  Per default pam_exec.so will execute the external command with the
  real user ID of the calling process. Specifying this option means the
  command is run with the effective user ID.
#+end_quote

* MODULE TYPES PROVIDED
All module types (*auth*, *account*, *password* and *session*) are
provided.

* RETURN VALUES
PAM_SUCCESS

#+begin_quote
  The external command was run successfully.
#+end_quote

PAM_BUF_ERR

#+begin_quote
  Memory buffer error.
#+end_quote

PAM_CONV_ERR

#+begin_quote
  The conversation method supplied by the application failed to obtain
  the username.
#+end_quote

PAM_INCOMPLETE

#+begin_quote
  The conversation method supplied by the application returned
  PAM_CONV_AGAIN.
#+end_quote

PAM_SERVICE_ERR

#+begin_quote
  No argument or a wrong number of arguments were given.
#+end_quote

PAM_SYSTEM_ERR

#+begin_quote
  A system error occurred or the command to execute failed.
#+end_quote

PAM_IGNORE

#+begin_quote
  *pam_setcred* was called, which does not execute the command. Or, the
  value given for the type= parameter did not match the module type.
#+end_quote

* EXAMPLES
Add the following line to /etc/pam.d/passwd to rebuild the NIS database
after each local password change:

#+begin_quote
  #+begin_example
            password optional pam_exec.so seteuid /usr/bin/make -C /var/yp
          
  #+end_example
#+end_quote

This will execute the command

#+begin_quote
  #+begin_example
    make -C /var/yp
  #+end_example
#+end_quote

with effective user ID.

* SEE ALSO
*pam.conf*(5), *pam.d*(5), *pam*(8)

* AUTHOR
pam_exec was written by Thorsten Kukuk <kukuk@thkukuk.de> and Josh
Triplett <josh@joshtriplett.org>.

Information about pam_exec.8 is found in manpage for: [[../default pam_exec\&.so will echo the exit status of the external command if it fails\&. Specifying this option will suppress the message\&.
default pam_exec\&.so will log the exit status of the external command if it fails\&. Specifying this option will suppress the log message\&.
default pam_exec\&.so will execute the external command with the real user ID of the calling process\&. Specifying this option means the command is run with the effective user ID\&.
       password optional pam_exec\&.so seteuid /usr/bin/make \-C /var/yp][default pam_exec\&.so will echo the exit status of the external command if it fails\&. Specifying this option will suppress the message\&.
default pam_exec\&.so will log the exit status of the external command if it fails\&. Specifying this option will suppress the log message\&.
default pam_exec\&.so will execute the external command with the real user ID of the calling process\&. Specifying this option means the command is run with the effective user ID\&.
       password optional pam_exec\&.so seteuid /usr/bin/make \-C /var/yp]]