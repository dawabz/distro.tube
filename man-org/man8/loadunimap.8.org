#+TITLE: Manpages - loadunimap.8
#+DESCRIPTION: Linux manpage for loadunimap.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
loadunimap - load the kernel unicode-to-font mapping table

* SYNOPSIS
*loadunimap* [ *-C* /console/ ] [ *-o* /oldmap/ ] [ /map/ ]

* DESCRIPTION
The *loadunimap* command is obsolete - its function is now built-in into
*setfont*(8). However, for backwards compatibility it is still available
as a separate command.

The program *loadunimap* loads the specified map in the kernel
unicode-to-font mapping table. If no map is given /def/ mapping table is
assumed. The default extension (that can be omitted) is /.uni/.

If the *-o* /oldmap/ option is given, the old map is saved in the file
specified.

On Linux 2.6.1 and later one can specify the console device using the
*-C* option.

Usually one does not call *loadunimap* directly - its function is also
built into *setfont*(8).

* FILES
- //usr/share/kbd/unimaps/ :: The default directory for unicode
  mappings.

- //usr/share/kbd/unimaps/def.uni/ :: The default mapping file.

* SEE ALSO
*mapscrn*(8), *setfont*(8)
