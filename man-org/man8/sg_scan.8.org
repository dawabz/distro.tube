#+TITLE: Manpages - sg_scan.8
#+DESCRIPTION: Linux manpage for sg_scan.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sg_scan - scans sg devices (or SCSI/ATAPI/ATA devices) and prints
results

* SYNOPSIS
*sg_scan* [/-a/] [/-i/] [/-n/] [/-w/] [/-x/] [/DEVICE/]*

* DESCRIPTION
If no /DEVICE/ names are given, sg_scan does a scan of the sg devices
and outputs a line of information for each sg device that is currently
bound to a SCSI device. If one or more /DEVICE/s are given only those
devices are scanned. Each device is opened with the O_NONBLOCK flag so
that the scan will not "hang" on any device that another process holds
an O_EXCL lock on.

Any given /DEVICE/ name is expected to comply with (to some extent) the
Storage Architecture Model (SAM see www.t10.org). Any device names
associated with the Linux SCSI subsystem (e.g. /dev/sda and /dev/st0m)
are suitable. Devices names associated with ATAPI devices (e.g. most
CD/DVD drives and ATAPI tape drives) are also suitable. If the device
does not fall into the above categories then an ATA IDENTIFY command is
tried.

In Linux 2.6 and 3 series kernels, the lsscsi utility may be helpful.
Apart from providing more information (by data-mining in the sysfs
pseudo file system), it does not need root permissions to execute, as
this utility would typically need.

* OPTIONS
- *-a* :: do alphabetical scan (i.e. sga, sgb, sgc). Note that sg device
  nodes with an alphabetical index have been deprecated since the Linux
  kernel 2.2 series.

- *-i* :: do a SCSI INQUIRY, output results in a second (indented) line.
  If the device is an ATA disk then output information from an ATA
  IDENTIFY command

- *-n* :: do numeric scan (i.e. sg0, sg1...) [default]

- *-w* :: use a read/write flag when opening sg device (default is
  read-only)

- *-x* :: extra information output about queueing

* NOTES
This utility was written at a time when hotplugging of SCSI devices was
not supported in Linux. It used a simple algorithm to scan sg device
nodes in ascending numeric or alphabetical order, stopping after there
were 4 consecutive errors.

In the Linux kernel 2.6 series, this utility uses sysfs to find which sg
device nodes are active and only checks those. Hence there can be large
"holes" in the numbering of sg device nodes (e.g. after an adapter has
been removed) and still all active sg device nodes will be listed. This
utility assumes that sg device nodes are named using the normal
conventions and searches from /dev/sg0 to /dev/sg4095 inclusive.

* EXIT STATUS
The exit status of sg_scan is 0 when it is successful. Otherwise see the
sg3_utils(8) man page.

* AUTHORS
Written by D. Gilbert and F. Jansen

* COPYRIGHT
Copyright © 1999-2013 Douglas Gilbert\\
This software is distributed under the GPL version 2. There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.

* SEE ALSO
*lsscsi(8)*
