#+TITLE: Manpages - systemd-timedated.8
#+DESCRIPTION: Linux manpage for systemd-timedated.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about systemd-timedated.8 is found in manpage for: [[../systemd-timedated.service.8][systemd-timedated.service.8]]