#+TITLE: Manpages - gssproxy.8
#+DESCRIPTION: Linux manpage for gssproxy.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gssproxy - GssProxy Daemon

* SYNOPSIS
*gssproxy* [/options/]

* DESCRIPTION
*gssproxy* provides a daemon to manage access to GSSAPI credentials.

*gssproxy* consists of the *gssproxy* daemon (configured by the
*gssproxy.conf*(5) file) and a GSSAPI interposer plugin
(*gssproxy-mech*(8)).

* OPTIONS
*-D*,*--daemon*

#+begin_quote
  Become a daemon after starting up.
#+end_quote

*-c*,*--config*

#+begin_quote
  Specify a config file to use as the main config file (read before the
  rest of the config directory). The default is to use the file
  /etc/gssproxy/gssproxy.conf. For reference on the config file syntax
  and options, consult the *gssproxy.conf*(5) manual page.
#+end_quote

*-C*,*--configdir*

#+begin_quote
  Specify a non-default config dir. Files named of the form
  "##-foo.conf" (that is, beginning with two digits and a dash, and
  ending in ".conf") will be read in numeric order from this directory,
  in addition to the config file itself. The default is /etc/gssproxy.
  For reference on the config file syntax and options, consult the
  *gssproxy.conf*(5) manual page.
#+end_quote

*-d*,*--debug*

#+begin_quote
  Turn on debugging. This option is identical to --debug-level=1.
#+end_quote

*--debug-level=*

#+begin_quote
  Turn on debugging at the specified level. 0 corresponds to no logging,
  while 1 turns on basic debug logging. Level 2 increases verbosity,
  including more detailed credential verification.

  At level 3 and above, KRB5_TRACE output is logged. If KRB5_TRACE was
  already set in the execution environment, trace output is sent to its
  value instead.
#+end_quote

*-i*,*--interactive*

#+begin_quote
  Run in the foreground, dont become a daemon.
#+end_quote

*-s*,*--socket*

#+begin_quote
  Specify a custom default socket name. This socket will be used by all
  sections that do not define an explicit socket.
#+end_quote

*--syslog-status*

#+begin_quote
  Enable additional logging to syslog.
#+end_quote

*--version*

#+begin_quote
  Print version number and exit.
#+end_quote

* SIGNALS
SIGTERM/SIGINT

#+begin_quote
  Informs the GssProxy to gracefully terminate all of its child
  processes and then shut down.
#+end_quote

SIGHUP

#+begin_quote
  Request a reload of all configuration for gssproxy. If there is an
  error in the configuration files, the existing configuration will not
  be replaced; if there is a problem applying the new configuration,
  gssproxy will exit.
#+end_quote

* SEE ALSO
*gssproxy.conf*(5) and *gssproxy-mech*(8).

* AUTHORS
*GSS-Proxy - http://fedorahosted.org/gss-proxy*
