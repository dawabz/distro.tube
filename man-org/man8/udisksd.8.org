#+TITLE: Manpages - udisksd.8
#+DESCRIPTION: Linux manpage for udisksd.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
udisksd - The udisks system daemon

* SYNOPSIS
*udisksd* [*--help*] [*--replace*] [*--debug*] [*--no-sigint*]
[*--force-load-modules*]

* DESCRIPTION
The *udisksd* program provides the /org.freedesktop.UDisks2/ name on the
system message bus. Users or administrators should never need to start
this daemon as it will be automatically started by *dbus-daemon*(1) or
*systemd*(1) whenever an application tries to access its D-Bus
interfaces.

See the *udisks*(8) man page for more information.

* OPTIONS
*--help*

#+begin_quote
  Show help options.
#+end_quote

*--replace*

#+begin_quote
  Replace existing daemon.
#+end_quote

*--debug*

#+begin_quote
  Print debug or informational messages on stdout/stderr.
#+end_quote

*--no-sigint*

#+begin_quote
  Do not handle SIGINT for controlled shutdown.
#+end_quote

*--force-load-modules*

#+begin_quote
  Activate modules on startup
#+end_quote

* AUTHOR
This man page was originally written for UDisks2 by David Zeuthen
<zeuthen@gmail.com> with a lot of help from many others.

* BUGS
Please send bug reports to either the distribution bug tracker or the
upstream bug tracker at
*https://github.com/storaged-project/udisks/issues*.

* SEE ALSO
*udisks*(8), *udisksctl*(1), *umount.udisks2*(8), *polkit*(8),
*dbus-daemon*(1), *systemd*(1)
