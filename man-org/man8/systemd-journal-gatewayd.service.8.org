#+TITLE: Manpages - systemd-journal-gatewayd.service.8
#+DESCRIPTION: Linux manpage for systemd-journal-gatewayd.service.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd-journal-gatewayd.service, systemd-journal-gatewayd.socket,
systemd-journal-gatewayd - HTTP server for journal events

* SYNOPSIS
systemd-journal-gatewayd.service

systemd-journal-gatewayd.socket

*/usr/lib/systemd/systemd-journal-gatewayd* [OPTIONS...]

* DESCRIPTION
*systemd-journal-gatewayd* serves journal events over the network.
Clients must connect using HTTP. The server listens on port 19531 by
default. If *--cert=* is specified, the server expects HTTPS
connections.

The program is started by *systemd*(1) and expects to receive a single
socket. Use *systemctl start systemd-journal-gatewayd.socket* to start
the service, and *systemctl enable systemd-journal-gatewayd.socket* to
have it started on boot.

* OPTIONS
The following options are understood:

*--cert=*

#+begin_quote
  Specify the path to a file or *AF_UNIX* stream socket to read the
  server certificate from. The certificate must be in PEM format. This
  option switches *systemd-journal-gatewayd* into HTTPS mode and must be
  used together with *--key=*.
#+end_quote

*--key=*

#+begin_quote
  Specify the path to a file or *AF_UNIX* stream socket to read the
  secret server key corresponding to the certificate specified with
  *--cert=* from. The key must be in PEM format.
#+end_quote

*--trust=*

#+begin_quote
  Specify the path to a file or *AF_UNIX* stream socket to read a CA
  certificate from. The certificate must be in PEM format.
#+end_quote

*--system*, *--user*

#+begin_quote
  Limit served entries to entries from system services and the kernel,
  or to entries from services of current user. This has the same meaning
  as *--system* and *--user* options for *journalctl*(1). If neither is
  specified, all accessible entries are served.
#+end_quote

*-m*, *--merge*

#+begin_quote
  Serve entries interleaved from all available journals, including other
  machines. This has the same meaning as *--merge* option for
  *journalctl*(1).
#+end_quote

*-D */DIR/, *--directory=*/DIR/

#+begin_quote
  Takes a directory path as argument. If specified,
  *systemd-journal-gatewayd* will serve the specified journal directory
  /DIR/ instead of the default runtime and system journal paths.
#+end_quote

*--file=*/GLOB/

#+begin_quote
  Takes a file glob as an argument. Serve entries from the specified
  journal files matching /GLOB/ instead of the default runtime and
  system journal paths. May be specified multiple times, in which case
  files will be suitably interleaved. This has the same meaning as
  *--file=* option for *journalctl*(1).
#+end_quote

*-h*, *--help*

#+begin_quote
  Print a short help text and exit.
#+end_quote

*--version*

#+begin_quote
  Print a short version string and exit.
#+end_quote

* SUPPORTED URLS
The following URLs are recognized:

/browse

#+begin_quote
  Interactive browsing.
#+end_quote

/entries[?option1&option2=value...]

#+begin_quote
  Retrieval of events in various formats.

  The *Accept:* part of the HTTP header determines the format. Supported
  values are described below.

  The *Range:* part of the HTTP header determines the range of events
  returned. Supported values are described below.

  GET parameters can be used to modify what events are returned.
  Supported parameters are described below.
#+end_quote

/machine

#+begin_quote
  Return a JSON structure describing the machine.

  Example:

  #+begin_quote
    #+begin_example
      { "machine_id" : "8cf7ed9d451ea194b77a9f118f3dc446",
        "boot_id" : "3d3c9efaf556496a9b04259ee35df7f7",
        "hostname" : "fedora",
        "os_pretty_name" : "Fedora 19 (Rawhide)",
        "virtualization" : "kvm",
        ...}
    #+end_example
  #+end_quote
#+end_quote

/fields//FIELD_NAME/

#+begin_quote
  Return a list of values of this field present in the logs.
#+end_quote

* ACCEPT HEADER
*Accept: */format/

Recognized formats:

*text/plain*

#+begin_quote
  The default. Plaintext syslog-like output, one line per journal entry
  (like *journalctl --output short*).
#+end_quote

*application/json*

#+begin_quote
  Entries are formatted as JSON data structures, one per line (like
  *journalctl --output json*). See *Journal JSON Format*[1] for more
  information.
#+end_quote

*text/event-stream*

#+begin_quote
  Entries are formatted as JSON data structures, wrapped in a format
  suitable for *Server-Sent Events*[2] (like *journalctl --output
  json-sse*).
#+end_quote

*application/vnd.fdo.journal*

#+begin_quote
  Entries are serialized into a binary (but mostly text-based) stream
  suitable for backups and network transfer (like *journalctl --output
  export*). See *Journal Export Format*[3] for more information.
#+end_quote

* RANGE HEADER
*Range: entries=*/cursor/*[[:*/num_skip/*]:*/num_entries/*]*

where /cursor/ is a cursor string, /num_skip/ is an integer,
/num_entries/ is an unsigned integer.

Range defaults to all available events.

* URL GET PARAMETERS
Following parameters can be used as part of the URL:

follow

#+begin_quote
  wait for new events (like *journalctl --follow*, except that the
  number of events returned is not limited).
#+end_quote

discrete

#+begin_quote
  Test that the specified cursor refers to an entry in the journal.
  Returns just this entry.
#+end_quote

boot

#+begin_quote
  Limit events to the current boot of the system (like *journalctl -b*).
#+end_quote

/KEY/=/match/

#+begin_quote
  Match journal fields. See *systemd.journal-fields*(7).
#+end_quote

* EXAMPLES
Retrieve events from this boot from local journal in *Journal Export
Format*[3]:

#+begin_quote
  #+begin_example
    curl --silent -HAccept: application/vnd.fdo.journal \
           http://localhost:19531/entries?boot
  #+end_example
#+end_quote

Listen for core dumps:

#+begin_quote
  #+begin_example
    curl http://localhost:19531/entries?follow&MESSAGE_ID=fc2e22bc6ee647b6b90729ab34a250b1
  #+end_example
#+end_quote

* SEE ALSO
*systemd*(1), *journalctl*(1), *systemd.journal-fields*(7),
*systemd-journald.service*(8), *systemd-journal-remote.service*(8),
*systemd-journal-upload.service*(8)

* NOTES
-  1. :: Journal JSON Format

  https://www.freedesktop.org/wiki/Software/systemd/json

-  2. :: Server-Sent Events

  https://developer.mozilla.org/en-US/docs/Server-sent_events/Using_server-sent_events

-  3. :: Journal Export Format

  https://www.freedesktop.org/wiki/Software/systemd/export
