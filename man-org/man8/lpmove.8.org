#+TITLE: Manpages - lpmove.8
#+DESCRIPTION: Linux manpage for lpmove.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lpmove - move a job or all jobs to a new destination

* SYNOPSIS
*lpmove* [ *-E* ] [ *-h */server/[*:*/port/] ] [ *-U* /username/ ] /job/
/destination/\\
*lpmove* [ *-E* ] [ *-h */server/[*:*/port/] ] [ *-U* /username/ ]
/source/ /destination/

* DESCRIPTION
*lpmove* moves the specified /job/ or all jobs from /source/ to
/destination/. /job/ can be the job ID number or the old destination and
job ID.

* OPTIONS
The *lpmove* command supports the following options:

- *-E* :: Forces encryption when connecting to the server.

- *-U */username/ :: Specifies an alternate username.

- *-h */server/[*:*/port/] :: Specifies an alternate server.

* EXAMPLES
Move job 123 from "oldprinter" to "newprinter":

#+begin_example

      lpmove 123 newprinter

              or

      lpmove oldprinter-123 newprinter
#+end_example

Move all jobs from "oldprinter" to "newprinter":

#+begin_example

      lpmove oldprinter newprinter
#+end_example

* SEE ALSO
*cancel*(1), *lp*(1), *lpr*(1), *lprm*(1),\\
CUPS Online Help (http://localhost:631/help)

* COPYRIGHT
Copyright © 2021 by OpenPrinting.
