#+TITLE: Manpages - sg_sat_read_gplog.8
#+DESCRIPTION: Linux manpage for sg_sat_read_gplog.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sg_sat_read_gplog - use ATA READ LOG EXT command via a SCSI to ATA
Translation (SAT) layer

* SYNOPSIS
*sg_sat_read_gplog* [/--ck_cond/] [/--count=CO/] [/--dma/] [/--help/]
[/--hex/] [/--len=/{16|12}] [/--log=/LA] [/--page=/PN] [/--readonly/]
[/--verbose/] [/--version/] /DEVICE/

* DESCRIPTION
This utility sends an ATA READ LOG EXT or an ATA READ LOG DMA EXT
command to the /DEVICE/. This command is used to read the general
purpose log of (S)ATA disks (not ATAPI devices such as DVD driver).
Rather than send the READ LOG (DMA) EXT command directly to the device
it is sent via a SCSI transport which is assumed to contain a SCSI to
ATA Translation (SAT) Layer (SATL). The SATL may be in an operating
system driver, in host bus adapter (HBA) firmware or in some external
enclosure.

This utility does not currently attempt to decode the response from the
ATA disk, rather it outputs the response in ASCII hexadecimal grouped in
16 bit words. Following ATA conventions those words are decoded little
endian (note that SCSI commands use a big endian representation). In the
future this utility may attempt to decode some log pages, perhaps using
the /--decode/ option.

The SAT-2 standard (SAT ANSI INCITS 465-2010, prior draft: sat2r09.pdf
at www.t10.org) defines two SCSI "ATA PASS-THROUGH" commands: one using
a 16 byte "cdb" and the other with a 12 byte cdb. This utility defaults
to using the 16 byte cdb variant.

* OPTIONS
Arguments to long options are mandatory for short options as well.

- *-C*, *--ck_cond* :: sets the CK_COND bit in the ATA PASS-THROUGH SCSI
  cdb. The default setting is clear (i.e. 0). When set the SATL should
  yield a sense buffer containing a ATA Result descriptor irrespective
  of whether the ATA command succeeded or failed. When clear the SATL
  should only yield a sense buffer containing a ATA Result descriptor if
  the ATA command failed.

- *-c*, *--count*=/CO/ :: the number /CO/ is placed in the "count" field
  in the ATA READ LOG EXT command. This specified the number of 512-byte
  blocks of data to be read from the specified log.

- *-d*, *--dma* :: use the ATA READ LOG DMA EXT command instead of ATA
  READ LOG EXT command. Some devices require this to return valid log
  data.

- *-h*, *--help* :: outputs the usage message summarizing command line
  options then exits. Ignores /DEVICE/ if given.

- *-H*, *--hex* :: when given once, the response is output in ASCII
  hexadecimal bytes. When given twice, then the response is grouped into
  16 bit words using ATA conventions (i.e. little endian); this is the
  default output (i.e. when this option is not given). When given thrice
  (i.e. '-HHH') the output is in hex, grouped in 16 bit words (without a
  leading offset and trailing ASCII on each line), in a format that is
  acceptable for 'hdparm --Istdin' to process.

- *-L*, *--log*=/LA/ :: the number /LA/ is known as the "log address" in
  the ATA standards and is placed in bits 7:0 of the "lba" field of the
  ATA READ LOG (DMA) EXT command. This specifies the log to be returned
  (See ATA-ACS for a detailed list of available log addresses). The
  default value placed in the "lba field is 0, returning the directory
  of available logs. The maximum value allowed for /LOG/ is 0xff.

- *-p*, *--page*=/PN/ :: the number /PN/ is the page number (within the
  log address) and is placed in bits 32:16 of the "lba" field of the ATA
  READ LOG (DMA) EXT command. The default value placed in the "lba"
  field is 0. The maximum value allowed for /LOG/ is 0xffff.

- *-l*, *--len*={16|12} :: this is the length of the SCSI cdb used for
  the ATA PASS-THROUGH commands. The argument can either be 16 or 12.
  The default is 16. Some SCSI transports cannot convey SCSI commands
  longer than 12 bytes.

- *-r*, *--readonly* :: causes the /DEVICE/ to be opened with the
  read-only flag (O_RDONLY in Unix). The default action is to open
  /DEVICE/ with the read-write flag (O_RDWR in Unix). In some cases
  sending power management commands to ATA disks are defeated by OS
  actions on the close() if the /DEVICE/ was opened with the read-write
  flag (e.g. the OS might think it needs to flush something to disk).

- *-v*, *--verbose* :: increases the level or verbosity.

- *-V*, *--version* :: print out version string

* NOTES
Prior to Linux kernel 2.6.29 USB mass storage limited sense data to 18
bytes which made the *--ck_cond* option yield strange (truncated)
results.

* EXIT STATUS
The exit status of sg_sat_read_gplog is 0 when it is successful.
Otherwise see the sg3_utils(8) man page.

* AUTHOR
Written by Hannes Reinecke and Douglas Gilbert

* REPORTING BUGS
Report bugs to <dgilbert at interlog dot com>.

* COPYRIGHT
Copyright © 2014-2015 Hannes Reinecke, SUSE Linux GmbH\\
This software is distributed under a FreeBSD license. There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.

* SEE ALSO
*sg_sat_identify(sg3_utils), sg_inq(sg3_utils), sdparm(sdparm),*
*hdparm(hdparm)*
