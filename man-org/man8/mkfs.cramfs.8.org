#+TITLE: Manpages - mkfs.cramfs.8
#+DESCRIPTION: Linux manpage for mkfs.cramfs.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
mkfs.cramfs - make compressed ROM file system

* SYNOPSIS
*mkfs.cramfs* [options] /directory file/

* DESCRIPTION
Files on cramfs file systems are zlib-compressed one page at a time to
allow random read access. The metadata is not compressed, but is
expressed in a terse representation that is more space-efficient than
conventional file systems.

The file system is intentionally read-only to simplify its design;
random write access for compressed files is difficult to implement.
cramfs ships with a utility (*mkcramfs*(8)) to pack files into new
cramfs images.

File sizes are limited to less than 16 MB.

Maximum file system size is a little under 272 MB. (The last file on the
file system must begin before the 256 MB block, but can extend past it.)

* ARGUMENTS
The /directory/ is simply the root of the directory tree that we want to
generate a compressed filesystem out of.

The /file/ will contain the cram file system, which later can be
mounted.

* OPTIONS
*-v*

#+begin_quote
  Enable verbose messaging.
#+end_quote

*-E*

#+begin_quote
  Treat all warnings as errors, which are reflected as command exit
  status.
#+end_quote

*-b* /blocksize/

#+begin_quote
  Use defined block size, which has to be divisible by page size.
#+end_quote

*-e* /edition/

#+begin_quote
  Use defined file system edition number in superblock.
#+end_quote

*-N* /big, little, host/

#+begin_quote
  Use defined endianness. Value defaults to /host/.
#+end_quote

*-i* /file/

#+begin_quote
  Insert a /file/ to cramfs file system.
#+end_quote

*-n* /name/

#+begin_quote
  Set name of the cramfs file system.
#+end_quote

*-p*

#+begin_quote
  Pad by 512 bytes for boot code.
#+end_quote

*-s*

#+begin_quote
  This option is ignored. Originally the *-s* turned on directory entry
  sorting.
#+end_quote

*-z*

#+begin_quote
  Make explicit holes.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help text and exit.
#+end_quote

*-V*, *--version*

#+begin_quote
  Display version information and exit.
#+end_quote

* EXIT STATUS
*0*

#+begin_quote
  success
#+end_quote

*8*

#+begin_quote
  operation error, such as unable to allocate memory
#+end_quote

* SEE ALSO
*fsck.cramfs*(8), *mount*(8)

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *mkfs.cramfs* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
