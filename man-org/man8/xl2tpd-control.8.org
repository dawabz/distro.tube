#+TITLE: Manpages - xl2tpd-control.8
#+DESCRIPTION: Linux manpage for xl2tpd-control.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xl2tpd-control - xl2tpd control utility.

* DESCRIPTION
xl2tpd is an implementation of the Layer 2 Tunneling Protocol (RFC
2661). L2TP allows to tunnel PPP over UDP. Some ISPs use L2TP to tunnel
user sessions from dial-in servers (modem banks, ADSL DSLAMs) to
back-end PPP servers. Another important application is Virtual Private
Networks where the IPsec protocol is used to secure the L2TP connection
(L2TP/IPsec, RFC 3193).

xl2tpd works by opening a pseudo-tty for communicating with pppd. It
runs completely in userspace but supports kernel mode L2TP.

xl2tpd supports IPsec SA Reference tracking to enable overlapping
internak NAT'ed IP's by different clients (eg all clients connecting
from their linksys internal IP 192.168.1.101) as well as multiple
clients behind the same NAT router.

* SYNOPSIS
*xl2tpd-control* [/-c/ PATH] /<COMMAND>/ /<TUNNEL_NAME>/ [/OPTIONS/]

* OPTIONS
- *-c* :: This option specifies xl2tpd control file.

- *-d* :: This option enables debugging mode.

* COMMANDS
- *add-lac* :: Adds new or modify existing LAC (L2TP Access
  Concentrator) configuration. Configuration should be specified as a
  command options in <key>=<value> pairs format. See available options
  in xl2tpd.conf(5).

- *connect-lac* :: Establish new connection to LAC. Username and secret
  for tunnel can be passed as a command options.

- *disconnect-lac* :: Disconnects tunnel.

- *remove-lac* :: Removes existing LAC configuration. xl2tpd disconnects
  the tunnel before removing.

- *add-lns* :: Adds new or modify existing LNS (L2TP Network Server)
  configuration.

- *remove-lns* :: Removes existing LNS configuration.

- *status-lns* :: Check the status of LNS.

- *available* :: Check availability.

* BUGS
Please use the github project page https://github.com/xelerance/xl2tpd
to send bugreports, issues and any other feedback.

* SEE ALSO
xl2tpd.conf(5), xl2tpd(8), pppd(8)

* COPYLEFT
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program (see the file LICENSE); if not, see
https://www.gnu.org/licenses/, or contact Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

* CONTRIBUTORS
Alexander Dorokhov <alex.dorokhov@gmail.com>\\
Alexander Naumov <alexander_naumov@opensuse.org>

* AUTHORS
Forked from l2tpd by Xelerance: https://github.com/xelerance/xl2tpd

Michael Richardson <mcr@xelerance.com>\\
Paul Wouters <paul@xelerance.com>\\
Samir Hussain <shussain@xelerance.com>

Previous development was hosted at sourceforge
(http://www.sourceforge.net/projects/l2tpd) by:

Scott Balmos <sbalmos@iglou.com>\\
David Stipp <dstipp@one.net>\\
Jeff McAdams <jeffm@iglou.com>

Based off of l2tpd version 0.61. Many thanks to Jacco de Leeuw
<jacco2@dds.nl> for maintaining l2tpd.

\\
Copyright (C)1998 Adtran, Inc.\\
Mark Spencer <markster@marko.net>
