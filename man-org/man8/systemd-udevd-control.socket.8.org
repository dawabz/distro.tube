#+TITLE: Manpages - systemd-udevd-control.socket.8
#+DESCRIPTION: Linux manpage for systemd-udevd-control.socket.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about systemd-udevd-control.socket.8 is found in manpage for: [[../systemd-udevd.service.8][systemd-udevd.service.8]]