#+TITLE: Manpages - rdisc.8
#+DESCRIPTION: Linux manpage for rdisc.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
rdisc - network router discovery daemon

* SYNOPSIS
*rdisc* [*-abdfrstvV*] [*-p */preference/] [*-T */max_interval/]
[send_address] [receive_address]

* DESCRIPTION
*rdisc* implements client side of the ICMP Internet Router Discovery
Protocol (IRDP). *rdisc* is invoked at boot time to populate the network
routing tables with default routes.

*rdisc* listens on the ALL_HOSTS (224.0.0.1) multicast address (or
/receive_address/ if it is provided) for ROUTER_ADVERTISE messages from
routers. The received messages are handled by first ignoring those
listed router addresses with which the host does not share a network.
Among the remaining addresses the ones with the highest preference are
selected as default routers and a default route is entered in the kernel
routing table for each one of them.

Optionally, *rdisc* can avoid waiting for routers to announce themselves
by sending out a few ROUTER_SOLICITATION messages to the ALL_ROUTERS
(224.0.0.2) multicast address (or /send_address/ if it is provided) when
it is started.

A timer is associated with each router address and the address will no
longer be considered for inclusion in the routing tables if the timer
expires before a new *advertise* message is received from the router.
The address will also be excluded from consideration if the received
*advertise* message has a preference of maximum negative.

Server side of router discovery protocol is supported by Cisco IOS and
by any more or less complete UNIX routing daemon, for example *gated*.
If compiled with ENABLE_RDISC_SERVER, *rdisc* can act as responder.

* OPTIONS
*-a*

#+begin_quote
  Accept all routers independently of the preference they have in their
  *advertise* messages. Normally *rdisc* only accepts (and enters in the
  kernel routing tables) the router or routers with the highest
  preference.
#+end_quote

*-b*

#+begin_quote
  Opposite to *-a*, i.e. install only router with the best preference
  value. This is the default behaviour.
#+end_quote

*-d*

#+begin_quote
  Send debugging messages to syslog.
#+end_quote

*-f*

#+begin_quote
  Keep *rdisc* running in the background even if no routers are found.
  Normally *rdisc* gives up if it has not received any *advertise*
  message after soliciting three times. In this case it exits with a
  non-zero exit code. If *-f* is not specified in the first form then
  *-s* must be specified.
#+end_quote

*-r*

#+begin_quote
  Responder mode, available only if compiled with ENABLE_RDISC_SERVER.
#+end_quote

*-s*

#+begin_quote
  Send three *solicitation* messages initially to quickly discover the
  routers when the system is booted. When *-s* is specified *rdisc*
  exits with a non-zero exit code if it can not find any routers. This
  can be overridden with the *-f* option.
#+end_quote

*-p* /preference/

#+begin_quote
  Set preference in advertisement messages. Available only with -r
  option.
#+end_quote

*-T* /max_interval/

#+begin_quote
  Set maximum advertisement interval in seconds. Default is 600.
  Available only with -r option.
#+end_quote

*-t*

#+begin_quote
  Test mode. Do not go to background.
#+end_quote

*-v*

#+begin_quote
  Be verbose and send lots of debugging messages to syslog.
#+end_quote

*-V*

#+begin_quote
  Print version and exit.
#+end_quote

* HISTORY
This program was developed by Sun Microsystems (see copyright notice in
source file). It was ported to Linux by Alexey Kuznetsov
<kuznet@ms2.inr.ac.ru>.

* SEE ALSO
*icmp*(7), *inet*(7), *ping*(8).

* REFERENCES
RFC1256 ICMP Router Discovery Messages. S. Deering, Ed.. September 1991.

* SECURITY
*rdisc* requires CAP_NET_RAW to listen and send ICMP messages and
capability CAP_NET_ADMIN to update routing tables.

* AVAILABILITY
*rdisc* is part of /iputils/ package.
