#+TITLE: Manpages - systemd-bless-boot-generator.8
#+DESCRIPTION: Linux manpage for systemd-bless-boot-generator.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd-bless-boot-generator - Pull systemd-bless-boot.service into the
initial boot transaction when boot counting is in effect

* SYNOPSIS
/usr/lib/systemd/system-generators/systemd-bless-boot-generator

* DESCRIPTION
systemd-bless-boot-generator is a generator that pulls
systemd-bless-boot.service into the initial boot transaction when boot
counting, as implemented by *systemd-boot*(7), is enabled.

systemd-bless-boot-generator implements *systemd.generator*(7).

* SEE ALSO
*systemd*(1), *systemd-bless-boot.service*(8), *systemd-boot*(7)
