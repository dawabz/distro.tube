#+TITLE: Manpages - arptables-nft-restore.8
#+DESCRIPTION: Linux manpage for arptables-nft-restore.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
arptables-restore - Restore ARP Tables (nft-based)

* SYNOPSIS
*arptables-restore*

* DESCRIPTION
*arptables-restore* is used to restore ARP Tables from data specified on
STDIN or via a file as first argument. Use I/O redirection provided by
your shell to read from a file

- *arptables-restore* :: flushes (deletes) all previous contents of the
  respective ARP Table.

* AUTHOR
Jesper Dangaard Brouer <brouer@redhat.com>

* SEE ALSO
*arptables-save(8), arptables(8)*
