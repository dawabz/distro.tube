#+TITLE: Manpages - ip-netconf.8
#+DESCRIPTION: Linux manpage for ip-netconf.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ip-netconf - network configuration monitoring

* SYNOPSIS
*ip * [ ip-OPTIONS ] *netconf show* [ *dev* /NAME/ ]

* DESCRIPTION
The *ip netconf* utility can monitor IPv4 and IPv6 parameters (see
*/proc/sys/net/ipv[4|6]/conf/[all|DEV]/*) like forwarding, rp_filter,
proxy_neigh, ignore_routes_with_linkdown or mc_forwarding status.

If no interface is specified, the entry *all* is displayed.

** ip netconf show - display network parameters
- *dev*/ NAME/ :: the name of the device to display network parameters
  for.

* SEE ALSO
\\
*ip*(8)

* AUTHOR
Original Manpage by Nicolas Dichtel <nicolas.dichtel@6wind.com>
