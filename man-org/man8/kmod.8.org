#+TITLE: Manpages - kmod.8
#+DESCRIPTION: Linux manpage for kmod.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
kmod - Program to manage Linux Kernel modules

* SYNOPSIS
*kmod* [*OPTIONS*...] [/COMMAND/] [*COMMAND_OPTIONS*...]

* DESCRIPTION
*kmod* is a multi-call binary which implements the programs used to
control Linux Kernel modules. Most users will only run it using its
other names.

* OPTIONS
*-V* *--version*

#+begin_quote
  Show the program version and exit.
#+end_quote

*-h* *--help*

#+begin_quote
  Show the help message.
#+end_quote

* COMMANDS
*help*

#+begin_quote
  Show the help message.
#+end_quote

*list*

#+begin_quote
  List the currently loaded modules.
#+end_quote

*static-nodes*

#+begin_quote
  Output the static device nodes information provided by the modules of
  the currently running kernel version.
#+end_quote

* COPYRIGHT
This manual page originally Copyright 2014, Marco dItri. Maintained by
Lucas De Marchi and others.

* SEE ALSO
*lsmod*(8), *rmmod*(8), *insmod*(8), *modinfo*(8), *modprobe*(8),
*depmod*(8)

* AUTHOR
*Lucas De Marchi* <lucas.de.marchi@gmail.com>

#+begin_quote
  Developer
#+end_quote
