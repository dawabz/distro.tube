#+TITLE: Manpages - grub-macbless.8
#+DESCRIPTION: Linux manpage for grub-macbless.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
grub-macbless - bless a mac file/directory

* SYNOPSIS
*grub-macbless* [/OPTION/...] /--ppc PATH|--x86 FILE/

* DESCRIPTION
Mac-style bless on HFS or HFS+

- *-p*, *--ppc* :: bless for ppc-based macs

- *-v*, *--verbose* :: print verbose messages.

- *-x*, *--x86* :: bless for x86-based macs

- -?, *--help* :: give this help list

- *--usage* :: give a short usage message

- *-V*, *--version* :: print program version

* REPORTING BUGS
Report bugs to <bug-grub@gnu.org>.

* SEE ALSO
*grub-install*(1)

The full documentation for *grub-macbless* is maintained as a Texinfo
manual. If the *info* and *grub-macbless* programs are properly
installed at your site, the command

#+begin_quote
  *info grub-macbless*
#+end_quote

should give you access to the complete manual.
