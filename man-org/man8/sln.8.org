#+TITLE: Manpages - sln.8
#+DESCRIPTION: Linux manpage for sln.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
sln - create symbolic links

* SYNOPSIS
#+begin_example
  sln source dest
  sln filelist
#+end_example

* DESCRIPTION
The *sln* program creates symbolic links. Unlike the *ln*(1) program, it
is statically linked. This means that if for some reason the dynamic
linker is not working, *sln* can be used to make symbolic links to
dynamic libraries.

The command line has two forms. In the first form, it creates /dest/ as
a new symbolic link to /source/.

In the second form, /filelist/ is a list of space-separated pathname
pairs, and the effect is as if *sln* was executed once for each line of
the file, with the two pathnames as the arguments.

The *sln* program supports no command-line options.

* SEE ALSO
*ln*(1), *ld.so*(8), *ldconfig*(8)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.

Information about sln.8 is found in manpage for: [[../ld.so (8),][ld.so (8),]]