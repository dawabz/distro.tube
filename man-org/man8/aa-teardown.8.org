#+TITLE: Manpages - aa-teardown.8
#+DESCRIPTION: Linux manpage for aa-teardown.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
aa-teardown - unload all AppArmor profiles

* SYNOPSIS
*aa-teardown*

* DESCRIPTION
aa-teardown unloads all AppArmor profiles

* BUGS
If you find any bugs, please report them at
<https://gitlab.com/apparmor/apparmor/-/issues>.

* SEE ALSO
/apparmor/ (7), /apparmor.d/ (5), and <https://wiki.apparmor.net>.
