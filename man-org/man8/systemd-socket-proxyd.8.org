#+TITLE: Manpages - systemd-socket-proxyd.8
#+DESCRIPTION: Linux manpage for systemd-socket-proxyd.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd-socket-proxyd - Bidirectionally proxy local sockets to another
(possibly remote) socket

* SYNOPSIS
*systemd-socket-proxyd* [/OPTIONS/...] /HOST/:/PORT/

*systemd-socket-proxyd* [/OPTIONS/...] /UNIX-DOMAIN-SOCKET-PATH/

* DESCRIPTION
*systemd-socket-proxyd* is a generic socket-activated network socket
forwarder proxy daemon for IPv4, IPv6 and UNIX stream sockets. It may be
used to bi-directionally forward traffic from a local listening socket
to a local or remote destination socket.

One use of this tool is to provide socket activation support for
services that do not natively support socket activation. On behalf of
the service to activate, the proxy inherits the socket from systemd,
accepts each client connection, opens a connection to a configured
server for each client, and then bidirectionally forwards data between
the two.

This utilitys behavior is similar to *socat*(1). The main differences
for *systemd-socket-proxyd* are support for socket activation with
"Accept=no" and an event-driven design that scales better with the
number of connections.

* OPTIONS
The following options are understood:

*-h*, *--help*

#+begin_quote
  Print a short help text and exit.
#+end_quote

*--version*

#+begin_quote
  Print a short version string and exit.
#+end_quote

*--connections-max=*, *-c*

#+begin_quote
  Sets the maximum number of simultaneous connections, defaults to 256.
  If the limit of concurrent connections is reached further connections
  will be refused.
#+end_quote

*--exit-idle-time=*

#+begin_quote
  Sets the time before exiting when there are no connections, defaults
  to *infinity*. Takes a unit-less value in seconds, or a time span
  value such as "5min 20s".
#+end_quote

* EXIT STATUS
On success, 0 is returned, a non-zero failure code otherwise.

* EXAMPLES
** Simple Example
Use two services with a dependency and no namespace isolation.

*Example 1. proxy-to-nginx.socket*

#+begin_quote
  #+begin_example
    [Socket]
    ListenStream=80

    [Install]
    WantedBy=sockets.target
  #+end_example
#+end_quote

*Example 2. proxy-to-nginx.service*

#+begin_quote
  #+begin_example
    [Unit]
    Requires=nginx.service
    After=nginx.service
    Requires=proxy-to-nginx.socket
    After=proxy-to-nginx.socket

    [Service]
    ExecStart=/usr/lib/systemd/systemd-socket-proxyd /run/nginx/socket
    PrivateTmp=yes
    PrivateNetwork=yes
  #+end_example
#+end_quote

*Example 3. nginx.conf*

#+begin_quote
  #+begin_example
    [...]
    server {
        listen       unix:/run/nginx/socket;
        [...]
  #+end_example
#+end_quote

*Example 4. Enabling the proxy*

#+begin_quote
  #+begin_example
    # systemctl enable --now proxy-to-nginx.socket
    $ curl http://localhost:80/
  #+end_example
#+end_quote

If nginx.service has /StopWhenUnneeded=/ set, then passing
*--exit-idle-time=* to *systemd-socket-proxyd* allows both services to
stop during idle periods.

** Namespace Example
Similar as above, but runs the socket proxy and the main service in the
same private namespace, assuming that nginx.service has /PrivateTmp=/
and /PrivateNetwork=/ set, too.

*Example 5. proxy-to-nginx.socket*

#+begin_quote
  #+begin_example
    [Socket]
    ListenStream=80

    [Install]
    WantedBy=sockets.target
  #+end_example
#+end_quote

*Example 6. proxy-to-nginx.service*

#+begin_quote
  #+begin_example
    [Unit]
    Requires=nginx.service
    After=nginx.service
    Requires=proxy-to-nginx.socket
    After=proxy-to-nginx.socket
    JoinsNamespaceOf=nginx.service

    [Service]
    ExecStart=/usr/lib/systemd/systemd-socket-proxyd 127.0.0.1:8080
    PrivateTmp=yes
    PrivateNetwork=yes
  #+end_example
#+end_quote

*Example 7. nginx.conf*

#+begin_quote
  #+begin_example
    [...]
    server {
        listen       8080;
        [...]
  #+end_example
#+end_quote

*Example 8. Enabling the proxy*

#+begin_quote
  #+begin_example
    # systemctl enable --now proxy-to-nginx.socket
    $ curl http://localhost:80/
  #+end_example
#+end_quote

* SEE ALSO
*systemd*(1), *systemd.socket*(5), *systemd.service*(5), *systemctl*(1),
*socat*(1), *nginx*(1), *curl*(1)
