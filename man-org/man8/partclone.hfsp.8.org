#+TITLE: Manpages - partclone.hfsp.8
#+DESCRIPTION: Linux manpage for partclone.hfsp.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about partclone.hfsp.8 is found in manpage for: [[../man8/partclone.8][man8/partclone.8]]