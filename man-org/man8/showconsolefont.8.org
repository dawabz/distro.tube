#+TITLE: Manpages - showconsolefont.8
#+DESCRIPTION: Linux manpage for showconsolefont.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
showconsolefont - Show the current EGA/VGA console screen font

* SYNOPSIS
*showconsolefont* [ *-V | --version* ] [ *-v* ] [ *-i* ] [ *-C*
/console/ ]

* DESCRIPTION
The *showconsolefont* command outputs the current console font to
stdout. The option *-v* prints additional information, while the option
*-V* prints the program version number. The option *-i* doesn't print
out the font table, just shows ROWSxCOLSxCOUNT and exits. On Linux 2.6.1
and later, the option *-C* allows one to indicate the console involved.
Its argument is a pathname.

* SEE ALSO
*setfont*(8)
