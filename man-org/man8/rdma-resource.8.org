#+TITLE: Manpages - rdma-resource.8
#+DESCRIPTION: Linux manpage for rdma-resource.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
rdma-resource - rdma resource configuration

* SYNOPSIS
*rdma* [ /OPTIONS/ ] /RESOURCE/ { /COMMAND/ | *help* }

/RESOURCE/ := { *cm_id* | *cq* | *mr* | *pd* | *qp* | *ctx* | *srq* }

/OPTIONS/ := { *-j*[/son/] | *-d*[/etails/] }

*rdma resource show* [ /DEV/PORT_INDEX/ ]

*rdma resource help*

* DESCRIPTION
** rdma resource show - display rdma resource tracking information
/DEV/PORT_INDEX/ - specifies the RDMA link to show. If this argument is
omitted all links are listed.

* EXAMPLES
rdma resource show

#+begin_quote
  Shows summary for all devices on the system.
#+end_quote

rdma resource show mlx5_2

#+begin_quote
  Shows the state of specified rdma device.
#+end_quote

rdma res show qp link mlx5_4

#+begin_quote
  Get all QPs for the specific device.
#+end_quote

rdma res show qp link mlx5_4/1

#+begin_quote
  Get QPs of specific port.
#+end_quote

rdma res show qp link mlx5_4/0

#+begin_quote
  Provide illegal port number (0 is illegal).
#+end_quote

rdma res show qp link mlx5_4/-

#+begin_quote
  Get QPs which have not assigned port yet.
#+end_quote

rdma res show qp link mlx5_4/- -d

#+begin_quote
  Detailed view.
#+end_quote

rdma res show qp link mlx5_4/- -dd

#+begin_quote
  Detailed view including driver-specific details.
#+end_quote

rdma res show qp link mlx5_4/1 lqpn 0-6

#+begin_quote
  Limit to specific Local QPNs.
#+end_quote

rdma res show qp link mlx5_4/1 lqpn 6 -r

#+begin_quote
  Driver specific details in raw format.
#+end_quote

rdma resource show cm_id dst-port 7174

#+begin_quote
  Show CM_IDs with destination ip port of 7174.
#+end_quote

rdma resource show cm_id src-addr 172.16.0.100

#+begin_quote
  Show CM_IDs bound to local ip address 172.16.0.100
#+end_quote

rdma resource show cq pid 30489

#+begin_quote
  Show CQs belonging to pid 30489
#+end_quote

rdma resource show ctx ctxn 1

#+begin_quote
  Show contexts that have index equal to 1.
#+end_quote

rdma resource show srq lqpn 5-7

#+begin_quote
  Show SRQs that the QPs with lqpn 5-7 are associated with.
#+end_quote

* SEE ALSO
*rdma*(8), *rdma-dev*(8), *rdma-link*(8), *rdma-statistic*(8),\\

* AUTHOR
Leon Romanovsky <leonro@mellanox.com>
