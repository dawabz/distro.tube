#+TITLE: Manpages - ld-linux.8
#+DESCRIPTION: Linux manpage for ld-linux.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ld-linux.8 is found in manpage for: [[../man8/ld.so.8][man8/ld.so.8]]