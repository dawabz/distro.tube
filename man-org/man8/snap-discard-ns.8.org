#+TITLE: Manpages - snap-discard-ns.8
#+DESCRIPTION: Linux manpage for snap-discard-ns.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
snap-discard-ns - internal tool for discarding preserved namespaces of
snappy applications

* SYNOPSIS

#+begin_quote

  #+begin_quote
    snap-discard-ns [--from-snap-confine] SNAP_INSTANCE_NAME
  #+end_quote
#+end_quote

* DESCRIPTION
The /snap-discard-ns/ is a program used internally by /snapd/ to discard
a preserved mount namespace of a particular snap.

* OPTIONS
The --from-snap-confine option is used internally by snap-confine to
tell snap-discard-ns that it is invoked from snap-confine and can
disable locking.

* ENVIRONMENT
/snap-discard-ns/ responds to the following environment variables

#+begin_quote
  - */SNAP_CONFINE_DEBUG/:* :: When defined the program will print
    additional diagnostic information about the actions being performed.
    All the output goes to stderr.
#+end_quote

* FILES
/snap-discard-ns/ uses the following files:

//run/snapd/ns/$SNAP_INSTNACE_NAME.mnt/:
//run/snapd/ns/$SNAP_INSTNACE_NAME.*.mnt/:

#+begin_quote

  #+begin_quote
    The preserved mount namespace that is unmounted and removed by
    /snap-discard-ns/. The second form is for the per-user mount
    namespace.
  #+end_quote
#+end_quote

//run/snapd/ns/snap.$SNAP_INSTNACE_NAME.fstab/:
//run/snapd/ns/snap.$SNAP_INSTNACE_NAME.*.user-fstab/:

#+begin_quote

  #+begin_quote
    The current mount profile of a preserved mount namespace that is
    removed by /snap-discard-ns/.
  #+end_quote
#+end_quote

* BUGS
Please report all bugs with /https://bugs.launchpad.net/snapd/+filebug/

* AUTHOR
zygmunt.krynicki@canonical.com

* COPYRIGHT
Canonical Ltd.
