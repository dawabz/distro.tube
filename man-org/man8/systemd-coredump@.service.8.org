#+TITLE: Manpages - systemd-coredump@.service.8
#+DESCRIPTION: Linux manpage for systemd-coredump@.service.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about systemd-coredump@.service.8 is found in manpage for: [[../systemd-coredump.8][systemd-coredump.8]]