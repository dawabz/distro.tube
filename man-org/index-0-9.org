#+TITLE: Linux Manpages - 0-9
#+DESCRIPTION: Linux manpages - 0-9
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"

* 0-9
#+begin_src bash :exports results
readarray -t starts_with_num < <(find ./man* -type f -iname "[0-9]*" | sort -t '/' -k 3)

for x in "${starts_with_num[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./man5/00-upstream-settings.5.org][00-upstream-settings.5]]               |
| [[file:./man8/30-systemd-environment-d-generator.8.org][30-systemd-environment-d-generator.8]] |
| [[file:./man1/411toppm.1.org][411toppm.1]]                           |
| [[file:./man1/7z.1.org][7z.1]]                                 |
| [[file:./man1/7za.1.org][7za.1]]                                |
| [[file:./man1/7zr.1.org][7zr.1]]                                |
