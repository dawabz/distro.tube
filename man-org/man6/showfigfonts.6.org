#+TITLE: Manpages - showfigfonts.6
#+DESCRIPTION: Linux manpage for showfigfonts.6
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
showfigfonts - prints a list of available figlet fonts

* SYNOPSIS
*showfigfonts* [ *-d* /directory/ ] [ /word/ ]

* DESCRIPTION
Prints a list of available figlet fonts, along with a sample of each
font. If directory is given, lists fonts in that directory; otherwise
uses the default font directory. If word is given, prints that word in
each font; otherwise prints the font name.

* EXAMPLES
To use *showfigfonts* with its default settings, simply type

#+begin_quote
  *example% showfigfonts*
#+end_quote

To print all the fonts in /usr/share/fonts/figlet

#+begin_quote
  *example% showfigfonts -d /usr/share/fonts/figlet*
#+end_quote

To print the word foo using all available fonts

#+begin_quote
  *example% showfigfonts foo*
#+end_quote

* AUTHORS
showfigfonts was written by Glenn Chappell <ggc@uiuc.edu>

This manual page was written by Jonathon Abbott for the Debian Project.

* SEE ALSO
*figlet*(6), *chkfont*(6), *figlist*(6)
