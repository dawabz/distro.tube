#+TITLE: Manpages - intro.6
#+DESCRIPTION: Linux manpage for intro.6
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
intro - introduction to games

* DESCRIPTION
Section 6 of the manual describes the games and funny little programs
available on the system.

* NOTES
** Authors and copyright conditions
Look at the header of the manual page source for the author(s) and
copyright conditions. Note that these can be different from page to
page!

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
