#+TITLE: Manpages - signal.h.0p
#+DESCRIPTION: Linux manpage for signal.h.0p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
signal.h --- signals

* SYNOPSIS
#+begin_example
  #include <signal.h>
#+end_example

* DESCRIPTION
Some of the functionality described on this reference page extends the
ISO C standard. Applications shall define the appropriate feature test
macro (see the System Interfaces volume of POSIX.1‐2017, /Section 2.2/,
/The Compilation Environment/) to enable the visibility of these symbols
in this header.

The /<signal.h>/ header shall define the following macros, which shall
expand to constant expressions with distinct values that have a type
compatible with the second argument to, and the return value of, the
/signal/() function, and whose values shall compare unequal to the
address of any declarable function.

- SIG_DFL :: Request for default signal handling.

- SIG_ERR :: Return value from /signal/() in case of error.

- SIG_HOLD :: Request that signal be held.

- SIG_IGN :: Request that signal be ignored.

The /<signal.h>/ header shall define the *pthread_t*, *size_t*, and
*uid_t* types as described in /<sys/types.h>/.

The /<signal.h>/ header shall define the *timespec* structure as
described in /<time.h>/.

The /<signal.h>/ header shall define the following data types:

- sig_atomic_t :: Possibly volatile-qualified integer type of an object
  that can be accessed as an atomic entity, even in the presence of
  asynchronous interrupts.

- sigset_t :: Integer or structure type of an object used to represent
  sets of signals.

- pid_t :: As described in /<sys/types.h>/.

The /<signal.h>/ header shall define the *pthread_attr_t* type as
described in /<sys/types.h>/.

The /<signal.h>/ header shall define the *sigevent* structure, which
shall include at least the following members:

#+begin_quote
  #+begin_example

    int              sigev_notify            Notification type.
    int              sigev_signo             Signal number.
    union sigval     sigev_value             Signal value.
    void           (*sigev_notify_function)(union sigval)
                                             Notification function.
    pthread_attr_t *sigev_notify_attributes  Notification attributes.
  #+end_example
#+end_quote

The /<signal.h>/ header shall define the following symbolic constants
for the values of /sigev_notify/:

- SIGEV_NONE :: No asynchronous notification is delivered when the event
  of interest occurs.

- SIGEV_SIGNAL :: A queued signal, with an application-defined value, is
  generated when the event of interest occurs.

- SIGEV_THREAD :: A notification function is called to perform
  notification.\\

The *sigval* union shall be defined as:

#+begin_quote
  #+begin_example

    int    sival_int    Integer signal value.
    void  *sival_ptr    Pointer signal value.
  #+end_example
#+end_quote

The /<signal.h>/ header shall declare the SIGRTMIN and SIGRTMAX macros,
which shall expand to positive integer expressions with type *int*, but
which need not be constant expressions. These macros specify a range of
signal numbers that are reserved for application use and for which the
realtime signal behavior specified in this volume of POSIX.1‐2017 is
supported. The signal numbers in this range do not overlap any of the
signals specified in the following table.

The range SIGRTMIN through SIGRTMAX inclusive shall include at least
{RTSIG_MAX} signal numbers.

It is implementation-defined whether realtime signal behavior is
supported for other signals.

The /<signal.h>/ header shall define the following macros that are used
to refer to the signals that occur in the system. Signals defined here
begin with the letters SIG followed by an uppercase letter. The macros
shall expand to positive integer constant expressions with type *int*
and distinct values. The value 0 is reserved for use as the null signal
(see /kill/()). Additional implementation-defined signals may occur in
the system.

The ISO C standard only requires the signal names SIGABRT, SIGFPE,
SIGILL, SIGINT, SIGSEGV, and SIGTERM to be defined. An implementation
need not generate any of these six signals, except as a result of
explicit use of interfaces that generate signals, such as /raise/(),
/kill/(), the General Terminal Interface (see /Section 11.1.9/, /Special
Characters/), and the /kill/ utility, unless otherwise stated (see, for
example, the System Interfaces volume of POSIX.1‐2017, /Section
2.8.3.3/, /Memory Protection/).

The following signals shall be supported on all implementations (default
actions are explained below the table):

TABLE

\\

The default actions are as follows:

20. Abnormal termination of the process.

21. Abnormal termination of the process with additional actions.

1. Ignore the signal.

19. Stop the process.

20. Continue the process, if it is stopped; otherwise, ignore the
    signal.

The effects on the process in each case are described in the System
Interfaces volume of POSIX.1‐2017, /Section 2.4.3/, /Signal Actions/.

The /<signal.h>/ header shall declare the *sigaction* structure, which
shall include at least the following members:

#+begin_quote
  #+begin_example

    void   (*sa_handler)(int)  Pointer to a signal-catching function
                               or one of the SIG_IGN or SIG_DFL.
    sigset_t sa_mask           Set of signals to be blocked during execution
                               of the signal handling function.
    int      sa_flags          Special flags.
    void   (*sa_sigaction)(int, siginfo_t *, void *)
                               Pointer to a signal-catching function.
  #+end_example
#+end_quote

The storage occupied by /sa_handler/ and /sa_sigaction/ may overlap, and
a conforming application shall not use both simultaneously.

The /<signal.h>/ header shall define the following macros which shall
expand to integer constant expressions that need not be usable in *#if*
preprocessing directives:

- SIG_BLOCK :: The resulting set is the union of the current set and the
  signal set pointed to by the argument /set/.

- SIG_UNBLOCK :: The resulting set is the intersection of the current
  set and the complement of the signal set pointed to by the argument
  /set/.

- SIG_SETMASK :: The resulting set is the signal set pointed to by the
  argument /set/.

The /<signal.h>/ header shall also define the following symbolic
constants:

- SA_NOCLDSTOP :: Do not generate SIGCHLD when children stop\\
  or stopped children continue.

- SA_ONSTACK :: Causes signal delivery to occur on an alternate stack.

- SA_RESETHAND :: Causes signal dispositions to be set to SIG_DFL on
  entry to signal handlers.

- SA_RESTART :: Causes certain functions to become restartable.

- SA_SIGINFO :: Causes extra information to be passed to signal handlers
  at the time of receipt of a signal.

- SA_NOCLDWAIT :: Causes implementations not to create zombie processes
  or status information on child termination. See //sigaction/ ( )/.

- SA_NODEFER :: Causes signal not to be automatically blocked on entry
  to signal handler.

- SS_ONSTACK :: Process is executing on an alternate signal stack.

- SS_DISABLE :: Alternate signal stack is disabled.

- MINSIGSTKSZ :: Minimum stack size for a signal handler.

- SIGSTKSZ :: Default size in bytes for the alternate signal stack.

The /<signal.h>/ header shall define the *mcontext_t* type through
*typedef*.

The /<signal.h>/ header shall define the *ucontext_t* type as a
structure that shall include at least the following members:

#+begin_quote
  #+begin_example

    ucontext_t *uc_link     Pointer to the context that is resumed
                            when this context returns.
    sigset_t    uc_sigmask  The set of signals that are blocked when this
                            context is active.
    stack_t     uc_stack    The stack used by this context.
    mcontext_t  uc_mcontext A machine-specific representation of the saved
                            context.
  #+end_example
#+end_quote

The /<signal.h>/ header shall define the *stack_t* type as a structure,
which shall include at least the following members:

#+begin_quote
  #+begin_example

    void     *ss_sp       Stack base or pointer.
    size_t    ss_size     Stack size.
    int       ss_flags    Flags.
  #+end_example
#+end_quote

The /<signal.h>/ header shall define the *siginfo_t* type as a
structure, which shall include at least the following members:

#+begin_quote
  #+begin_example

    int           si_signo  Signal number.
    int           si_code   Signal code.
    int           si_errno  If non-zero, an errno value associated with
                            this signal, as described in <errno.h>.
    pid_t         si_pid    Sending process ID.
    uid_t         si_uid    Real user ID of sending process.
    void         *si_addr   Address of faulting instruction.
    int           si_status Exit value or signal.
    long          si_band   Band event for SIGPOLL.
    union sigval  si_value  Signal value.
  #+end_example
#+end_quote

The /<signal.h>/ header shall define the symbolic constants in the
*Code* column of the following table for use as values of /si_code/ that
are signal-specific or non-signal-specific reasons why the signal was
generated.

TABLE

Implementations may support additional /si_code/ values not included in
this list, may generate values included in this list under circumstances
other than those described in this list, and may contain extensions or
limitations that prevent some values from being generated.
Implementations do not generate a different value from the ones
described in this list for circumstances described in this list.\\

In addition, the following signal-specific information shall be
available:

TABLE

For some implementations, the value of /si_addr/ may be inaccurate.

The following shall be declared as functions and may also be defined as
macros. Function prototypes shall be provided.

#+begin_quote
  #+begin_example

    int    kill(pid_t, int);
    int    killpg(pid_t, int);
    void   psiginfo(const siginfo_t *, const char *);
    void   psignal(int, const char *);
    int    pthread_kill(pthread_t, int);
    int    pthread_sigmask(int, const sigset_t *restrict,
               sigset_t *restrict);
    int    raise(int);
    int    sigaction(int, const struct sigaction *restrict,
               struct sigaction *restrict);
    int    sigaddset(sigset_t *, int);
    int    sigaltstack(const stack_t *restrict, stack_t *restrict);
    int    sigdelset(sigset_t *, int);
    int    sigemptyset(sigset_t *);
    int    sigfillset(sigset_t *);
    int    sighold(int);
    int    sigignore(int);
    int    siginterrupt(int, int);
    int    sigismember(const sigset_t *, int);
    void (*signal(int, void (*)(int)))(int);
    int    sigpause(int);
    int    sigpending(sigset_t *);
    int    sigprocmask(int, const sigset_t *restrict, sigset_t *restrict);
    int    sigqueue(pid_t, int, union sigval);
    int    sigrelse(int);
    void (*sigset(int, void (*)(int)))(int);
    int    sigsuspend(const sigset_t *);
    int    sigtimedwait(const sigset_t *restrict, siginfo_t *restrict,
               const struct timespec *restrict);
    int    sigwait(const sigset_t *restrict, int *restrict);
    int    sigwaitinfo(const sigset_t *restrict, siginfo_t *restrict);
  #+end_example
#+end_quote

Inclusion of the /<signal.h>/ header may make visible all symbols from
the /<time.h>/ header.

/The following sections are informative./

* APPLICATION USAGE
On systems not supporting the XSI option, the /si_pid/ and /si_uid/
members of *siginfo_t* are only required to be valid when /si_code/ is
SI_USER or SI_QUEUE. On XSI-conforming systems, they are also valid for
all /si_code/ values less than or equal to 0; however, it is unspecified
whether SI_USER and SI_QUEUE have values less than or equal to zero, and
therefore XSI applications should check whether /si_code/ has the value
SI_USER or SI_QUEUE or is less than or equal to 0 to tell whether
/si_pid/ and /si_uid/ are valid.

* RATIONALE
None.

* FUTURE DIRECTIONS
The SIGPOLL and SIGPROF signals may be removed in a future version.

* SEE ALSO
/*<errno.h>*/, /*<stropts.h>*/, /*<sys_types.h>*/, /*<time.h>*/

The System Interfaces volume of POSIX.1‐2017, /Section 2.2/, /The
Compilation Environment/, //alarm/ ( )/, //ioctl/ ( )/, //kill/ ( )/,
//killpg/ ( )/, //psiginfo/ ( )/, //pthread_kill/ ( )/,
//pthread_sigmask/ ( )/, //raise/ ( )/, //sigaction/ ( )/,
//sigaddset/ ( )/, //sigaltstack/ ( )/, //sigdelset/ ( )/,
//sigemptyset/ ( )/, //sigfillset/ ( )/, //sighold/ ( )/,
//siginterrupt/ ( )/, //sigismember/ ( )/, //signal/ ( )/,
//sigpending/ ( )/, //sigqueue/ ( )/, //sigsuspend/ ( )/,
//sigtimedwait/ ( )/, //sigwait/ ( )/, //timer_create/ ( )/,
//wait/ ( )/, //waitid/ ( )/

The Shell and Utilities volume of POSIX.1‐2017, //kill/ /

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
