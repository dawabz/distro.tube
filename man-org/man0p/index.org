#+TITLE: Man0p - index
#+DESCRIPTION: Man0p - index
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* man0p
Section 0p is for header files (POSIX).

#+begin_src bash :exports results
readarray -t orgfiles < <(find . -type f -iname "*" | sort)

for x in "${orgfiles[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./aio.h.0p.org][aio.h.0p]]          |
| [[file:./arpa_inet.h.0p.org][arpa_inet.h.0p]]    |
| [[file:./assert.h.0p.org][assert.h.0p]]       |
| [[file:./complex.h.0p.org][complex.h.0p]]      |
| [[file:./cpio.h.0p.org][cpio.h.0p]]         |
| [[file:./ctype.h.0p.org][ctype.h.0p]]        |
| [[file:./dirent.h.0p.org][dirent.h.0p]]       |
| [[file:./dlfcn.h.0p.org][dlfcn.h.0p]]        |
| [[file:./errno.h.0p.org][errno.h.0p]]        |
| [[file:./fcntl.h.0p.org][fcntl.h.0p]]        |
| [[file:./fenv.h.0p.org][fenv.h.0p]]         |
| [[file:./float.h.0p.org][float.h.0p]]        |
| [[file:./fmtmsg.h.0p.org][fmtmsg.h.0p]]       |
| [[file:./fnmatch.h.0p.org][fnmatch.h.0p]]      |
| [[file:./ftw.h.0p.org][ftw.h.0p]]          |
| [[file:./glob.h.0p.org][glob.h.0p]]         |
| [[file:./grp.h.0p.org][grp.h.0p]]          |
| [[file:./iconv.h.0p.org][iconv.h.0p]]        |
| [[file:./index.org][index]]             |
| [[file:./inttypes.h.0p.org][inttypes.h.0p]]     |
| [[file:./iso646.h.0p.org][iso646.h.0p]]       |
| [[file:./langinfo.h.0p.org][langinfo.h.0p]]     |
| [[file:./libgen.h.0p.org][libgen.h.0p]]       |
| [[file:./limits.h.0p.org][limits.h.0p]]       |
| [[file:./locale.h.0p.org][locale.h.0p]]       |
| [[file:./math.h.0p.org][math.h.0p]]         |
| [[file:./monetary.h.0p.org][monetary.h.0p]]     |
| [[file:./mqueue.h.0p.org][mqueue.h.0p]]       |
| [[file:./ndbm.h.0p.org][ndbm.h.0p]]         |
| [[file:./netdb.h.0p.org][netdb.h.0p]]        |
| [[file:./net_if.h.0p.org][net_if.h.0p]]       |
| [[file:./netinet_in.h.0p.org][netinet_in.h.0p]]   |
| [[file:./netinet_tcp.h.0p.org][netinet_tcp.h.0p]]  |
| [[file:./nl_types.h.0p.org][nl_types.h.0p]]     |
| [[file:./poll.h.0p.org][poll.h.0p]]         |
| [[file:./pthread.h.0p.org][pthread.h.0p]]      |
| [[file:./pwd.h.0p.org][pwd.h.0p]]          |
| [[file:./regex.h.0p.org][regex.h.0p]]        |
| [[file:./sched.h.0p.org][sched.h.0p]]        |
| [[file:./search.h.0p.org][search.h.0p]]       |
| [[file:./semaphore.h.0p.org][semaphore.h.0p]]    |
| [[file:./setjmp.h.0p.org][setjmp.h.0p]]       |
| [[file:./signal.h.0p.org][signal.h.0p]]       |
| [[file:./spawn.h.0p.org][spawn.h.0p]]        |
| [[file:./stdarg.h.0p.org][stdarg.h.0p]]       |
| [[file:./stdbool.h.0p.org][stdbool.h.0p]]      |
| [[file:./stddef.h.0p.org][stddef.h.0p]]       |
| [[file:./stdint.h.0p.org][stdint.h.0p]]       |
| [[file:./stdio.h.0p.org][stdio.h.0p]]        |
| [[file:./stdlib.h.0p.org][stdlib.h.0p]]       |
| [[file:./string.h.0p.org][string.h.0p]]       |
| [[file:./strings.h.0p.org][strings.h.0p]]      |
| [[file:./stropts.h.0p.org][stropts.h.0p]]      |
| [[file:./sys_ipc.h.0p.org][sys_ipc.h.0p]]      |
| [[file:./syslog.h.0p.org][syslog.h.0p]]       |
| [[file:./sys_mman.h.0p.org][sys_mman.h.0p]]     |
| [[file:./sys_msg.h.0p.org][sys_msg.h.0p]]      |
| [[file:./sys_resource.h.0p.org][sys_resource.h.0p]] |
| [[file:./sys_select.h.0p.org][sys_select.h.0p]]   |
| [[file:./sys_sem.h.0p.org][sys_sem.h.0p]]      |
| [[file:./sys_shm.h.0p.org][sys_shm.h.0p]]      |
| [[file:./sys_socket.h.0p.org][sys_socket.h.0p]]   |
| [[file:./sys_stat.h.0p.org][sys_stat.h.0p]]     |
| [[file:./sys_statvfs.h.0p.org][sys_statvfs.h.0p]]  |
| [[file:./sys_time.h.0p.org][sys_time.h.0p]]     |
| [[file:./sys_times.h.0p.org][sys_times.h.0p]]    |
| [[file:./sys_types.h.0p.org][sys_types.h.0p]]    |
| [[file:./sys_uio.h.0p.org][sys_uio.h.0p]]      |
| [[file:./sys_un.h.0p.org][sys_un.h.0p]]       |
| [[file:./sys_utsname.h.0p.org][sys_utsname.h.0p]]  |
| [[file:./sys_wait.h.0p.org][sys_wait.h.0p]]     |
| [[file:./tar.h.0p.org][tar.h.0p]]          |
| [[file:./termios.h.0p.org][termios.h.0p]]      |
| [[file:./tgmath.h.0p.org][tgmath.h.0p]]       |
| [[file:./time.h.0p.org][time.h.0p]]         |
| [[file:./trace.h.0p.org][trace.h.0p]]        |
| [[file:./ulimit.h.0p.org][ulimit.h.0p]]       |
| [[file:./unistd.h.0p.org][unistd.h.0p]]       |
| [[file:./utime.h.0p.org][utime.h.0p]]        |
| [[file:./utmpx.h.0p.org][utmpx.h.0p]]        |
| [[file:./wchar.h.0p.org][wchar.h.0p]]        |
| [[file:./wctype.h.0p.org][wctype.h.0p]]       |
| [[file:./wordexp.h.0p.org][wordexp.h.0p]]      |
