#+TITLE: Manpages - search.h.0p
#+DESCRIPTION: Linux manpage for search.h.0p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
search.h --- search tables

* SYNOPSIS
#+begin_example
  #include <search.h>
#+end_example

* DESCRIPTION
The /<search.h>/ header shall define the *ENTRY* type for structure
*entry* which shall include the following members:

#+begin_quote
  #+begin_example

    char    *key
    void    *data
  #+end_example
#+end_quote

and shall define *ACTION* and *VISIT* as enumeration data types through
type definitions as follows:

#+begin_quote
  #+begin_example

    enum { FIND, ENTER } ACTION;
    enum { preorder, postorder, endorder, leaf } VISIT;
  #+end_example
#+end_quote

The /<search.h>/ header shall define the *size_t* type as described in
/<sys/types.h>/.

The following shall be declared as functions and may also be defined as
macros. Function prototypes shall be provided.

#+begin_quote
  #+begin_example

    int    hcreate(size_t);
    void   hdestroy(void);
    ENTRY *hsearch(ENTRY, ACTION);
    void   insque(void *, void *);
    void  *lfind(const void *, const void *, size_t *,
              size_t, int (*)(const void *, const void *));
    void  *lsearch(const void *, void *, size_t *,
              size_t, int (*)(const void *, const void *));
    void   remque(void *);
    void  *tdelete(const void *restrict, void **restrict,
              int(*)(const void *, const void *));
    void  *tfind(const void *, void *const *,
              int(*)(const void *, const void *));
    void  *tsearch(const void *, void **,
              int(*)(const void *, const void *));
    void   twalk(const void *,
              void (*)(const void *, VISIT, int ));
  #+end_example
#+end_quote

/The following sections are informative./

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
/*<sys_types.h>*/

The System Interfaces volume of POSIX.1‐2017, //hcreate/ ( )/,
//insque/ ( )/, //lsearch/ ( )/, //tdelete/ ( )/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
