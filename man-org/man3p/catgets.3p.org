#+TITLE: Manpages - catgets.3p
#+DESCRIPTION: Linux manpage for catgets.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
catgets --- read a program message

* SYNOPSIS
#+begin_example
  #include <nl_types.h>
  char *catgets(nl_catd catd, int set_id, int msg_id, const char *s);
#+end_example

* DESCRIPTION
The /catgets/() function shall attempt to read message /msg_id/, in set
/set_id/, from the message catalog identified by /catd/. The /catd/
argument is a message catalog descriptor returned from an earlier call
to /catopen/(). The results are undefined if /catd/ is not a value
returned by /catopen/() for a message catalog still open in the process.
The /s/ argument points to a default message string which shall be
returned by /catgets/() if it cannot retrieve the identified message.

The /catgets/() function need not be thread-safe.

* RETURN VALUE
If the identified message is retrieved successfully, /catgets/() shall
return a pointer to an internal buffer area containing the
null-terminated message string. If the call is unsuccessful for any
reason, /s/ shall be returned and /errno/ shall be set to indicate the
error.

* ERRORS
The /catgets/() function shall fail if:

- *EINTR* :: The read operation was terminated due to the receipt of a
  signal, and no data was transferred.

- *ENOMSG* :: The message identified by /set_id/ and /msg_id/ is not in
  the message catalog.

The /catgets/() function may fail if:

- *EBADF* :: The /catd/ argument is not a valid message catalog
  descriptor open for reading.

- *EBADMSG* :: The message identified by /set_id/ and /msg_id/ in the
  specified message catalog did not satisfy implementation-defined
  security criteria.

- *EINVAL* :: The message catalog identified by /catd/ is corrupted.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//catclose/ ( )/, //catopen/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<nl_types.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
