#+TITLE: Manpages - killpg.3p
#+DESCRIPTION: Linux manpage for killpg.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
killpg --- send a signal to a process group

* SYNOPSIS
#+begin_example
  #include <signal.h>
  int killpg(pid_t pgrp, int sig);
#+end_example

* DESCRIPTION
The /killpg/() function shall send the signal specified by /sig/ to the
process group specified by /pgrp/.

If /pgrp/ is greater than 1, /killpg/(/pgrp/, /sig/) shall be equivalent
to /kill/(-/pgrp/, /sig/). If /pgrp/ is less than or equal to 1, the
behavior of /killpg/() is undefined.

* RETURN VALUE
Refer to //kill/ ( )/.

* ERRORS
Refer to //kill/ ( )/.

/The following sections are informative./

* EXAMPLES
** Sending a Signal to All Other Members of a Process Group
The following example shows how the calling process could send a signal
to all other members of its process group. To prevent itself from
receiving the signal it first makes itself immune to the signal by
ignoring it.

#+begin_quote
  #+begin_example

    #include <signal.h>
    #include <unistd.h>
    ...
        if (signal(SIGUSR1, SIG_IGN) == SIG_ERR)
            /* Handle error */;
        if (killpg(getpgrp(), SIGUSR1) == -1)
            /* Handle error */;"
  #+end_example
#+end_quote

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//getpgid/ ( )/, //getpid/ ( )/, //kill/ ( )/, //raise/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<signal.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
