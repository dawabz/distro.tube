#+TITLE: Manpages - atol.3p
#+DESCRIPTION: Linux manpage for atol.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
atol, atoll --- convert a string to a long integer

* SYNOPSIS
#+begin_example
  #include <stdlib.h>
  long atol(const char *nptr);
  long long atoll(const char *nptr);
#+end_example

* DESCRIPTION
The functionality described on this reference page is aligned with the
ISO C standard. Any conflict between the requirements described here and
the ISO C standard is unintentional. This volume of POSIX.1‐2017 defers
to the ISO C standard.

Except as noted below, the call /atol/(/nptr/) shall be equivalent to:

#+begin_quote
  #+begin_example

    strtol(nptr, (char **)NULL, 10)
  #+end_example
#+end_quote

Except as noted below, the call to /atoll/(/nptr/) shall be equivalent
to:

#+begin_quote
  #+begin_example

    strtoll(nptr, (char **)NULL, 10)
  #+end_example
#+end_quote

The handling of errors may differ. If the value cannot be represented,
the behavior is undefined.

* RETURN VALUE
These functions shall return the converted value if the value can be
represented.

* ERRORS
No errors are defined.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
If the number is not known to be in range, /strtol/() or /strtoll/()
should be used because /atol/() and /atoll/() are not required to
perform any error checking.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//strtol/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<stdlib.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
