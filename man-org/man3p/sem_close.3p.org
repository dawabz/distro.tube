#+TITLE: Manpages - sem_close.3p
#+DESCRIPTION: Linux manpage for sem_close.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
sem_close --- close a named semaphore

* SYNOPSIS
#+begin_example
  #include <semaphore.h>
  int sem_close(sem_t *sem);
#+end_example

* DESCRIPTION
The /sem_close/() function shall indicate that the calling process is
finished using the named semaphore indicated by /sem/. The effects of
calling /sem_close/() for an unnamed semaphore (one created by
/sem_init/()) are undefined. The /sem_close/() function shall deallocate
(that is, make available for reuse by a subsequent /sem_open/() by this
process) any system resources allocated by the system for use by this
process for this semaphore. The effect of subsequent use of the
semaphore indicated by /sem/ by this process is undefined. If any
threads in the calling process are currently blocked on the semaphore,
the behavior is undefined. If the semaphore has not been removed with a
successful call to /sem_unlink/(), then /sem_close/() has no effect on
the state of the semaphore. If the /sem_unlink/() function has been
successfully invoked for /name/ after the most recent call to
/sem_open/() with O_CREAT for this semaphore, then when all processes
that have opened the semaphore close it, the semaphore is no longer
accessible.

* RETURN VALUE
Upon successful completion, a value of zero shall be returned.
Otherwise, a value of -1 shall be returned and /errno/ set to indicate
the error.

* ERRORS
The /sem_close/() function may fail if:

- *EINVAL* :: The /sem/ argument is not a valid semaphore descriptor.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//semctl/ ( )/, //semget/ ( )/, //semop/ ( )/, //sem_init/ ( )/,
//sem_open/ ( )/, //sem_unlink/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<semaphore.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
