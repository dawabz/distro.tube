#+TITLE: Manpages - pthread_barrierattr_getpshared.3p
#+DESCRIPTION: Linux manpage for pthread_barrierattr_getpshared.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
pthread_barrierattr_getpshared, pthread_barrierattr_setpshared --- get
and set the process-shared attribute of the barrier attributes object

* SYNOPSIS
#+begin_example
  #include <pthread.h>
  int pthread_barrierattr_getpshared(const pthread_barrierattr_t
      *restrict attr, int *restrict pshared);
  int pthread_barrierattr_setpshared(pthread_barrierattr_t *attr,
      int pshared);
#+end_example

* DESCRIPTION
The /pthread_barrierattr_getpshared/() function shall obtain the value
of the /process-shared/ attribute from the attributes object referenced
by /attr/. The /pthread_barrierattr_setpshared/() function shall set the
/process-shared/ attribute in an initialized attributes object
referenced by /attr/.

The /process-shared/ attribute is set to PTHREAD_PROCESS_SHARED to
permit a barrier to be operated upon by any thread that has access to
the memory where the barrier is allocated. See /Section 2.9.9/,
/Synchronization Object Copies and Alternative Mappings/ for further
requirements. The default value of the attribute shall be
PTHREAD_PROCESS_PRIVATE. Both constants PTHREAD_PROCESS_SHARED and
PTHREAD_PROCESS_PRIVATE are defined in /<pthread.h>/.

Additional attributes, their default values, and the names of the
associated functions to get and set those attribute values are
implementation-defined.

The behavior is undefined if the value specified by the /attr/ argument
to /pthread_barrierattr_getpshared/() or
/pthread_barrierattr_setpshared/() does not refer to an initialized
barrier attributes object.

* RETURN VALUE
If successful, the /pthread_barrierattr_getpshared/() function shall
return zero and store the value of the /process-shared/ attribute of
/attr/ into the object referenced by the /pshared/ parameter. Otherwise,
an error number shall be returned to indicate the error.

If successful, the /pthread_barrierattr_setpshared/() function shall
return zero; otherwise, an error number shall be returned to indicate
the error.

* ERRORS
The /pthread_barrierattr_setpshared/() function may fail if:

- *EINVAL* :: The new value specified for the /process-shared/ attribute
  is not one of the legal values PTHREAD_PROCESS_SHARED or
  PTHREAD_PROCESS_PRIVATE.

These functions shall not return an error code of *[EINTR]*.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
The /pthread_barrierattr_getpshared/() and
/pthread_barrierattr_setpshared/() functions are part of the Thread
Process-Shared Synchronization option and need not be provided on all
implementations.

* RATIONALE
If an implementation detects that the value specified by the /attr/
argument to /pthread_barrierattr_getpshared/() or
/pthread_barrierattr_setpshared/() does not refer to an initialized
barrier attributes object, it is recommended that the function should
fail and report an *[EINVAL]* error.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//pthread_barrier_destroy/ ( )/, //pthread_barrierattr_destroy/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<pthread.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
