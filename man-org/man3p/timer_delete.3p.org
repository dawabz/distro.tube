#+TITLE: Manpages - timer_delete.3p
#+DESCRIPTION: Linux manpage for timer_delete.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
timer_delete --- delete a per-process timer

* SYNOPSIS
#+begin_example
  #include <time.h>
  int timer_delete(timer_t timerid);
#+end_example

* DESCRIPTION
The /timer_delete/() function deletes the specified timer, /timerid/,
previously created by the /timer_create/() function. If the timer is
armed when /timer_delete/() is called, the behavior shall be as if the
timer is automatically disarmed before removal. The disposition of
pending signals for the deleted timer is unspecified.

The behavior is undefined if the value specified by the /timerid/
argument to /timer_delete/() does not correspond to a timer ID returned
by /timer_create/() but not yet deleted by /timer_delete/().

* RETURN VALUE
If successful, the /timer_delete/() function shall return a value of
zero. Otherwise, the function shall return a value of -1 and set /errno/
to indicate the error.

* ERRORS
No errors are defined.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
If an implementation detects that the value specified by the /timerid/
argument to /timer_delete/() does not correspond to a timer ID returned
by /timer_create/() but not yet deleted by /timer_delete/(), it is
recommended that the function should fail and report an *[EINVAL]*
error.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//timer_create/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<time.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
