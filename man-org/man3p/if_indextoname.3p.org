#+TITLE: Manpages - if_indextoname.3p
#+DESCRIPTION: Linux manpage for if_indextoname.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
if_indextoname --- map a network interface index to its corresponding
name

* SYNOPSIS
#+begin_example
  #include <net/if.h>
  char *if_indextoname(unsigned ifindex, char *ifname);
#+end_example

* DESCRIPTION
The /if_indextoname/() function shall map an interface index to its
corresponding name.

When this function is called, /ifname/ shall point to a buffer of at
least {IF_NAMESIZE} bytes. The function shall place in this buffer the
name of the interface with index /ifindex/.

* RETURN VALUE
If /ifindex/ is an interface index, then the function shall return the
value supplied in /ifname/, which points to a buffer now containing the
interface name. Otherwise, the function shall return a null pointer and
set /errno/ to indicate the error.

* ERRORS
The /if_indextoname/() function shall fail if:

- *ENXIO* :: The interface does not exist.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//getsockopt/ ( )/, //if_freenameindex/ ( )/, //if_nameindex/ ( )/,
//if_nametoindex/ ( )/, //setsockopt/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<net_if.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
