#+TITLE: Manpages - getuid.3p
#+DESCRIPTION: Linux manpage for getuid.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
getuid --- get a real user ID

* SYNOPSIS
#+begin_example
  #include <unistd.h>
  uid_t getuid(void);
#+end_example

* DESCRIPTION
The /getuid/() function shall return the real user ID of the calling
process. The /getuid/() function shall not modify /errno/.

* RETURN VALUE
The /getuid/() function shall always be successful and no return value
is reserved to indicate the error.

* ERRORS
No errors are defined.

/The following sections are informative./

* EXAMPLES
** Setting the Effective User ID to the Real User ID
The following example sets the effective user ID of the calling process
to the real user ID.

#+begin_quote
  #+begin_example

    #include <unistd.h>
    ...
    seteuid(getuid());
  #+end_example
#+end_quote

* APPLICATION USAGE
None.

* RATIONALE
In a conforming environment, /getuid/() will always succeed. It is
possible for implementations to provide an extension where a process in
a non-conforming environment will not be associated with a user or group
ID. It is recommended that such implementations return ( *uid_t*)-1 and
set /errno/ to indicate such an environment; doing so does not violate
this standard, since such an environment is already an extension.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//getegid/ ( )/, //geteuid/ ( )/, //getgid/ ( )/, //setegid/ ( )/,
//seteuid/ ( )/, //setgid/ ( )/, //setregid/ ( )/, //setreuid/ ( )/,
//setuid/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<sys_types.h>*/,
/*<unistd.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
