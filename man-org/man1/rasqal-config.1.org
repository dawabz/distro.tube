#+TITLE: Man1 - rasqal-config.1
#+DESCRIPTION: Linux manpage for rasqal-config.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
rasqal-config - script to get information about the installed version of
the RDF query library

* SYNOPSIS
*rasqal-config* [--prefix/[=DIR]/] [--libs] [--libtool-libs] [--cflags]
[--version] [--version-decimal] [--help]

* DESCRIPTION
/rasqal-config/ is a tool that is used to determine the compile and
linker flags that should be used to compile and link programs that use
the Rasqal RDF query library.

* OPTIONS
/rasqal-config/ accepts the following options:

- *--version* :: Print the currently installed version of rasqal on the
  standard output.

- *--version-decimal* :: Print the currently installed version of rasqal
  as a decimal integer.

- *--libs* :: Print the linker flags that are necessary to link a
  program with Rasqal.

- *--libtool-libs* :: Print the path to the libtool file for rasqal.

- *--cflags* :: Print the compiler flags that are necessary to compile a
  program with the RDF query language.

- *--prefix=PREFIX* :: If specified, use PREFIX instead of the
  installation prefix that Rasqal was built with when computing the
  output for the --cflags and --libs options. This option must be
  specified before any --libs or --cflags options.

* SEE ALSO
*librasqal(3)*

* AUTHOR
Dave Beckett - [[http://www.dajobe.org/]]
