#+TITLE: Man1 - pbmtocmuwm.1
#+DESCRIPTION: Linux manpage for pbmtocmuwm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmtocmuwm - convert a PBM image into a CMU window manager bitmap

* SYNOPSIS
*pbmtocmuwm* [/pbmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmtocmuwm* reads a portable bitmap as input and produces a CMU window
manager bitmap as output.

* SEE ALSO
*cmuwmtopbm*(1) , *pbm*(5)

* AUTHOR
Copyright (C) 1989 by Jef Poskanzer.
