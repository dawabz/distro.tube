#+TITLE: Man1 - calibre-server.1
#+DESCRIPTION: Linux manpage for calibre-server.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
calibre-server - calibre-server

#+begin_quote

  #+begin_quote
    #+begin_example
      calibre-server [options] [path to library folder...]
    #+end_example
  #+end_quote
#+end_quote

Start the calibre Content server. The calibre Content server exposes
your calibre libraries over the internet. You can specify the path to
the library folders as arguments to *calibre-server*. If you do not
specify any paths, all the libraries that the main calibre program knows
about will be used.

Whenever you pass arguments to *calibre-server* that have spaces in
them, enclose the arguments in quotation marks. For example: "/some
path/with spaces"

* [OPTIONS]

#+begin_quote
  - *--access-log* :: Path to the access log file. This log contains
    information about clients connecting to the server and making
    requests. By default no access logging is done.
#+end_quote

#+begin_quote
  - *--ajax-timeout* :: Time (in seconds) to wait for a response from
    the server when making queries.
#+end_quote

#+begin_quote
  - *--auth-mode* :: Choose the type of authentication used. Set the
    HTTP authentication mode used by the server. Set to *"*basic*"* if
    you are putting this server behind an SSL proxy. Otherwise, leave it
    as *"*auto*"*, which will use *"*basic*"* if SSL is configured
    otherwise it will use *"*digest*"*.
#+end_quote

#+begin_quote
  - *--auto-reload* :: Automatically reload server when source code
    changes. Useful for development. You should also specify a small
    value for the shutdown timeout.
#+end_quote

#+begin_quote
  - *--ban-after* :: Number of login failures for ban. The number of
    login failures after which an IP address is banned
#+end_quote

#+begin_quote
  - *--ban-for* :: Ban IP addresses that have repeated login failures.
    Temporarily bans access for IP addresses that have repeated login
    failures for the specified number of minutes. Useful to prevent
    attempts at guessing passwords. If set to zero, no banning is done.
#+end_quote

#+begin_quote
  - *--book-list-mode* :: Choose the default book list mode. Set the
    default book list mode that will be used for new users. Individual
    users can override the default in their own settings. The default is
    to use a cover grid.
#+end_quote

#+begin_quote
  - *--compress-min-size* :: Minimum size for which responses use data
    compression (in bytes).
#+end_quote

#+begin_quote
  - *--custom-list-template* :: Path to a JSON file containing a
    template for the custom book list mode. The easiest way to create
    such a template file is to go to Preferences-> Sharing over the
    net-> Book list template in calibre, create the template and export
    it.
#+end_quote

#+begin_quote
  - *--daemonize* :: Run process in background as a daemon (Linux only).
#+end_quote

#+begin_quote
  - *--displayed-fields* :: Restrict displayed user-defined fields.
    Comma separated list of user-defined metadata fields that will be
    displayed by the Content server in the /opds and /mobile views. If
    you specify this option, any fields not in this list will not be
    displayed. For example: my_rating,my_tags
#+end_quote

#+begin_quote
  - *--enable-allow-socket-preallocation,
    --disable-allow-socket-preallocation* :: Socket pre-allocation, for
    example, with systemd socket activation. By default, this option is
    enabled.
#+end_quote

#+begin_quote
  - *--enable-auth, --disable-auth* :: Password based authentication to
    access the server. Normally, the server is unrestricted, allowing
    anyone to access it. You can restrict access to predefined users
    with this option. By default, this option is disabled.
#+end_quote

#+begin_quote
  - *--enable-fallback-to-detected-interface,
    --disable-fallback-to-detected-interface* :: Fallback to
    auto-detected interface. If for some reason the server is unable to
    bind to the interface specified in the listen_on option, then it
    will try to detect an interface that connects to the outside world
    and bind to that. By default, this option is enabled.
#+end_quote

#+begin_quote
  - *--enable-local-write, --disable-local-write* :: Allow
    un-authenticated local connections to make changes. Normally, if you
    do not turn on authentication, the server operates in read-only
    mode, so as to not allow anonymous users to make changes to your
    calibre libraries. This option allows anybody connecting from the
    same computer as the server is running on to make changes. This is
    useful if you want to run the server without authentication but
    still use calibredb to make changes to your calibre libraries. Note
    that turning on this option means any program running on the
    computer can make changes to your calibre libraries. By default,
    this option is disabled.
#+end_quote

#+begin_quote
  - *--enable-log-not-found, --disable-log-not-found* :: Log HTTP 404
    (Not Found) requests. Normally, the server logs all HTTP requests
    for resources that are not found. This can generate a lot of log
    spam, if your server is targeted by bots. Use this option to turn it
    off. By default, this option is enabled.
#+end_quote

#+begin_quote
  - *--enable-use-bonjour, --disable-use-bonjour* :: Advertise OPDS
    feeds via BonJour. Advertise the OPDS feeds via the BonJour service,
    so that OPDS based reading apps can detect and connect to the server
    automatically. By default, this option is enabled.
#+end_quote

#+begin_quote
  - *--enable-use-sendfile, --disable-use-sendfile* :: Zero copy file
    transfers for increased performance. This will use zero-copy
    in-kernel transfers when sending files over the network, increasing
    performance. However, it can cause corrupted file transfers on some
    broken filesystems. If you experience corrupted file transfers, turn
    it off. By default, this option is enabled.
#+end_quote

#+begin_quote
  - *--help, -h* :: show this help message and exit
#+end_quote

#+begin_quote
  - *--ignored-fields* :: Ignored user-defined metadata fields. Comma
    separated list of user-defined metadata fields that will not be
    displayed by the Content server in the /opds and /mobile views. For
    example: my_rating,my_tags
#+end_quote

#+begin_quote
  - *--listen-on* :: The interface on which to listen for connections.
    The default is to listen on all available IPv4 interfaces. You can
    change this to, for example, *"*127.0.0.1*"* to only listen for
    connections from the local machine, or to *"*::*"* to listen to all
    incoming IPv6 and IPv4 connections.
#+end_quote

#+begin_quote
  - *--log* :: Path to log file for server log. This log contains server
    information and errors, not access logs. By default it is written to
    stdout.
#+end_quote

#+begin_quote
  - *--manage-users* :: Manage the database of users allowed to connect
    to this server. You can use it in automated mode by adding a --. See
    calibre-server /--manage-users/ *--* help for details. See also the
    /--userdb/ option.
#+end_quote

#+begin_quote
  - *--max-header-line-size* :: Max. size of single HTTP header (in KB).
#+end_quote

#+begin_quote
  - *--max-job-time* :: Maximum time for worker processes. Maximum
    amount of time worker processes are allowed to run (in minutes). Set
    to zero for no limit.
#+end_quote

#+begin_quote
  - *--max-jobs* :: Maximum number of worker processes. Worker processes
    are launched as needed and used for large jobs such as preparing a
    book for viewing, adding books, converting, etc. Normally, the max.
    number of such processes is based on the number of CPU cores. You
    can control it by this setting.
#+end_quote

#+begin_quote
  - *--max-log-size* :: Max. log file size (in MB). The maximum size of
    log files, generated by the server. When the log becomes larger than
    this size, it is automatically rotated. Set to zero to disable log
    rotation.
#+end_quote

#+begin_quote
  - *--max-opds-items* :: Maximum number of books in OPDS feeds. The
    maximum number of books that the server will return in a single OPDS
    acquisition feed.
#+end_quote

#+begin_quote
  - *--max-opds-ungrouped-items* :: Maximum number of ungrouped items in
    OPDS feeds. Group items in categories such as author/tags by first
    letter when there are more than this number of items. Set to zero to
    disable.
#+end_quote

#+begin_quote
  - *--max-request-body-size* :: Max. allowed size for files uploaded to
    the server (in MB).
#+end_quote

#+begin_quote
  - *--num-per-page* :: Number of books to show in a single page. The
    number of books to show in a single page in the browser.
#+end_quote

#+begin_quote
  - *--pidfile* :: Write process PID to the specified file
#+end_quote

#+begin_quote
  - *--port* :: The port on which to listen for connections.
#+end_quote

#+begin_quote
  - *--search-the-net-urls* :: Path to a JSON file containing URLs for
    the *"*Search the internet*"* feature. The easiest way to create
    such a file is to go to Preferences-> Sharing over the net->Search
    the internet in calibre, create the URLs and export them.
#+end_quote

#+begin_quote
  - *--shutdown-timeout* :: Total time in seconds to wait for clean
    shutdown.
#+end_quote

#+begin_quote
  - *--ssl-certfile* :: Path to the SSL certificate file.
#+end_quote

#+begin_quote
  - *--ssl-keyfile* :: Path to the SSL private key file.
#+end_quote

#+begin_quote
  - *--timeout* :: Time (in seconds) after which an idle connection is
    closed.
#+end_quote

#+begin_quote
  - *--trusted-ips* :: Allow un-authenticated connections from specific
    IP addresses to make changes. Normally, if you do not turn on
    authentication, the server operates in read-only mode, so as to not
    allow anonymous users to make changes to your calibre libraries.
    This option allows anybody connecting from the specified IP
    addresses to make changes. Must be a comma separated list of address
    or network specifications. This is useful if you want to run the
    server without authentication but still use calibredb to make
    changes to your calibre libraries. Note that turning on this option
    means anyone connecting from the specified IP addresses can make
    changes to your calibre libraries.
#+end_quote

#+begin_quote
  - *--url-prefix* :: A prefix to prepend to all URLs. Useful if you
    wish to run this server behind a reverse proxy. For example use,
    /calibre as the URL prefix.
#+end_quote

#+begin_quote
  - *--userdb* :: Path to the user database to use for authentication.
    The database is a SQLite file. To create it use /--manage-users/.
    You can read more about managing users at:
    /https://manual.calibre-ebook.com/server.html#managing-user-accounts-from-the-command-line-only/
#+end_quote

#+begin_quote
  - *--version* :: show program*'*s version number and exit
#+end_quote

#+begin_quote
  - *--worker-count* :: Number of worker threads used to process
    requests.
#+end_quote

* AUTHOR
Kovid Goyal

* COPYRIGHT
Kovid Goyal
