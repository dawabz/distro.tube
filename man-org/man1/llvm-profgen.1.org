#+TITLE: Man1 - llvm-profgen.1
#+DESCRIPTION: Linux manpage for llvm-profgen.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
llvm-profgen - LLVM SPGO profile generation tool

* SYNOPSIS
*llvm-profgen* [/commands/] [/options/]

* DESCRIPTION
The *llvm-profgen* utility generates a profile data file from given perf
script data files for sample-based profile guided optimization(SPGO).

* COMMANDS
At least one of the following commands are required:

#+begin_quote
  - *--perfscript=<string[,string,...]>* :: Path of perf-script trace
    created by Linux perf tool with /script/ command(the raw perf.data
    should be profiled with -b).
#+end_quote

#+begin_quote
  - *--binary=<string[,string,...]>* :: Path of the input profiled
    binary files.
#+end_quote

#+begin_quote
  - *--output=<string>* :: Path of the output profile file.
#+end_quote

* OPTIONS
*llvm-profgen* supports the following options:

#+begin_quote
  - *--format=[text|binary|extbinary|compbinary|gcc]* :: Specify the
    format of the generated profile. Supported <format> are /text/,
    /binary/, /extbinary/, /compbinary/, /gcc/, see /llvm-profdata/ for
    more descriptions of the format.
#+end_quote

#+begin_quote
  - *--show-mmap-events* :: Print mmap events.
#+end_quote

#+begin_quote
  - *--show-disassembly* :: Print disassembled code.
#+end_quote

#+begin_quote
  - *--x86-asm-syntax=[att|intel]* :: Specify whether to print assembly
    code in AT&T syntax (the default) or Intel syntax.
#+end_quote

* AUTHOR
Maintained by the LLVM Team (https://llvm.org/).

* COPYRIGHT
2003-2021, LLVM Project
