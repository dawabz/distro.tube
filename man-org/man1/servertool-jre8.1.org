#+TITLE: Man1 - servertool-jre8.1
#+DESCRIPTION: Linux manpage for servertool-jre8.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
servertool - Provides an easy-to-use interface for developers to
register, unregister, start up, and shut down a persistent server.

* SYNOPSIS
#+begin_example

  servertool -ORBInitialPort nameserverport [ options ] [ commands ]
#+end_example

- /options/ :: The command-line options. See Options.

- commands :: The command-line commands. See Commands.

* DESCRIPTION
The servertool command provides the command-line interface for
developers to register, unregister, start up, and shut down a persistent
server. Command-line commands let you obtain various statistical
information about the server. See Commands.

* OPTIONS
- -ORBInitialHost /nameserverhost/ :: \\
  This options is required. It specifies the host machine on which the
  name server runs and listens for incoming requests. The nameserverhost
  value must specify the port on which the orb is running and listening
  for requests. The value defaults to localhost when this option is not
  specified. If orbd and servertool are running on different machines,
  then you must specify the name or IP address of the host on which orbd
  is running.

/Note:/ On Oracle Solaris, you must become a root user to start a
process on a port below 1024. Oracle recommends that you use a port
number above or equal to 1024 for the nameserverport value.

- -J/option/ :: \\
  Passes option to the Java Virtual Machine, where option is one of the
  options described on the reference page for the Java application
  launcher. For example, -J-Xms48m sets the startup memory to 48 MB. See
  java(1).

* COMMANDS
You can start the servertool command with or without a command-line
command.

- · :: If you did not specify a command when you started servertool,
  then the command-line tool displays the servertool prompt where you
  can enter commands: servertool >.

- · :: If you specify a command when you start servertool, then the Java
  IDL Server Tool starts, executes the command, and exits.

register -server /server-class-name/ -classpath /classpath-to-server/ [
-applicationName /application-name/ -args /args-to-server/ -vmargs
/flags-for-JVM/ ] Registers a new persistent server with the Object
Request Broker Daemon (ORBD). If the server is not already registered,
then it is registered and activated. This command causes an installation
method to be called in the main class of the server identified by the
-server option. The installation method must be public static void
install(org.omg.CORBA.ORB). The install method is optional and lets
developers provide their own server installation behavior, such as
creating a database schema.

unregister -serverid /server-id/ | -applicationName /application-name/
Unregisters a server from the ORBD with either its server ID or its
application name. This command causes an uninstallation method to be
called in the main class of the server identified by the -server option.
The uninstall method must be public static void
uninstall(org.omg.CORBA.ORB). The uninstall method is optional and lets
developers provide their own server uninstallation behavior, such as
undoing the behavior of the install method.

- getserverid -applicationName /application-name/ :: Returns the server
  ID that corresponds to the application-name value.

- list :: Lists information about all persistent servers registered with
  the ORBD.

- listappnames :: Lists the application names for all servers currently
  registered with the ORBD.

- listactive :: Lists information about all persistent servers that were
  started by the ORBD and are currently running.

locate -serverid /server-id/ | -applicationName /application-name/ [
-endpointType /endpointType/ ] Locates the endpoints (ports) of a
specific type for all ORBs created by a registered server. If a server
is not already running, then it is activated. If an endpointType value
is not specified, then the plain/non-protected endpoint associated with
each ORB in a server is returned.

locateperorb -serverid /server-id/ | -applicationName /application-name/
[ -orbid /ORB-name/ ] Locates all the endpoints (ports) registered by a
specific Object Request Broker (ORB) of registered server. If a server
is not already running, then it is activated. If an orbid is not
specified, then the default value of "" is assigned to the orbid. If any
ORBs are created with an orbid of empty string, then all ports
registered by it are returned.

- orblist -serverid /server-id/ | -applicationName
  /application-name/ :: Lists the ORBId of the ORBs defined on a server.
  An ORBId is the string name for the ORB created by the server. If the
  server is not already running, then it is activated.

- shutdown -serverid /server-id/ | -applicationName
  application-name :: Shut down an active server that is registered with
  ORBD. During execution of this command, the shutdown method defined in
  the class specified by either the -serverid or -applicationName
  parameter is also called to shut down the server process.

- startup -serverid /server-id/ | -applicationName
  application-name :: Starts up or activate a server that is registered
  with ORBD. If the server is not running, then this command starts the
  server. If the server is already running, then an error message is
  displayed.

- help :: Lists all the commands available to the server through the
  servertool command.

- quit :: Exits the servertool command.

* SEE ALSO
- · :: orbd(1)

\\
