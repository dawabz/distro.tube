#+TITLE: Man1 - djvmcvt.1
#+DESCRIPTION: Linux manpage for djvmcvt.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
djvmcvt - Convert multi-page DjVu documents.

* SYNOPSIS
*    $*
*djvmcvt -b[undled] */docin.djvu/* */docout.djvu/

*    $*
*djvmcvt -i[ndirect] */docin.djvu/* */dir/* */index.djvu/

* DESCRIPTION
This program converts any multi-page DjVu document to either the bundled
or indirect multi-page format. The input file /docin.djvu/ must be
either the file name of a bundled document or the index file of an
indirect document.

* OPTIONS
- *-b[undled]* :: Create a bundled multi-page DjVu document named
  /docout.djvu/.

- *-i[ndirect]* :: Create an indirect multi-page DjVu document. All the
  files composing the indirect document will be stored into directory
  /dir/. The index file will be named /index.djvu/.

* CREDITS
This program was initially written by Andrei Erofeev
<andrew_erofeev@yahoo.com> and was improved by Bill Riemers
<docbill@sourceforge.net> and many others.

* SEE ALSO
*djvu*(1), *djvmcvt*(1)
