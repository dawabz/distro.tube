#+TITLE: Man1 - jack_simple_client.1
#+DESCRIPTION: Linux manpage for jack_simple_client.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
jack_simple_client - The JACK Audio Connection Kit example client

* SYNOPSIS
*jack_simple_client* client-name

The client-name must be a yet unused client name.

* DESCRIPTION
*jack_simple_client* is an example client for the JACK Audio Connection
Kit. It creates two ports (client-name:input and client-name:output)
that pass the data unmodified.

* EXAMPLE
jack_simple_client in_process_test

* AUTHOR
Jeremy Hall

This manpage was written by Robert Jordens <jordens@debian.org> for
Debian.
