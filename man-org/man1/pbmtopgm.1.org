#+TITLE: Man1 - pbmtopgm.1
#+DESCRIPTION: Linux manpage for pbmtopgm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmtopgm - convert PBM image to PGM by averaging areas

* SYNOPSIS
*pbmtopgm * /width/ /height/ [/pbmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmtopgm* reads a PBM image as input. It outputs a PGM image in which
each pixel's gray level is the average of the surrounding black and
white input pixels. The surrounding area is a rectangle of /width/ by
/height/ pixels.

In other words, this is a convolution. *pbmtopgm* is similar to a
special case of *pnmconvol*.

You may need a *pnmsmooth* step after *pbmtopgm*.

*pbmtopgm* has the effect of anti-aliasing bitmaps which contain
distinct line features.

*pbmtopgm* works best with odd sample width and heights.

You don't need *pbmtopgm* just to use a PGM program on a PBM image. Any
PGM program (assuming it uses the Netpbm libraries to read the PGM
input) takes PBM input as if it were PGM, with only the minimum and
maximum gray levels. So unless your convolution rectangle is bigger than
one pixel, you're not gaining anything with a *pbmtopgm* step.

The opposite transformation (which would turn a PGM into a PBM) is
dithering. See *pamditherbw*.

* SEE ALSO
*pamditherbw*(1) , *pnmconvol*(1) , *pbm*(5) , *pgm*(5)

* AUTHOR
Copyright (C) 1990 by Angus Duggan.

Copyright (C) 1989 by Jef Poskanzer.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation. This software is provided 'as is' without
express or implied warranty.
