#+TITLE: Man1 - lightdm.1
#+DESCRIPTION: Linux manpage for lightdm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lightdm - a display manager

* SYNOPSIS
*lightdm* [ /OPTION/ ]

* DESCRIPTION
*lightdm* is a display manager.

* OPTIONS
- *-h, --help* :: Show help options

- *-c, --config=FILE* :: Use configuration file

- *-d, --debug* :: Print debugging messages

- *--test-mode* :: Run as unprivileged user, skipping things that
  require root access

- *--pid-file=FILE* :: File to write PID into

- *--xsessions-dir=DIRECTORY* :: Directory to load X sessions from

- *--xgreeters-dir=DIRECTORY* :: Directory to load X greeters from

- *--log-dir=DIRECTORY* :: Directory to write logs to

- *--run-dir=DIRECTORY* :: Directory to store running state

- *--cache-dir=DIRECTORY* :: Directory to cached information

- *-v, --version* :: Show release version

* FILES
- */etc/lightdm/lightdm.conf* :: Configuration

- */etc/lightdm/users.conf* :: User list configuration (if not using
  Accounts Service)

- */etc/lightdm/keys.conf* :: XDMCP keys

* SEE ALSO
*dm-tool*(1)
