#+TITLE: Man1 - ebook-meta.1
#+DESCRIPTION: Linux manpage for ebook-meta.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ebook-meta - ebook-meta

#+begin_quote

  #+begin_quote
    #+begin_example
      ebook-meta ebook_file [options]
    #+end_example
  #+end_quote
#+end_quote

Read/Write metadata from/to e-book files.

Supported formats for reading metadata: azw, azw1, azw3, azw4, cb7, cbr,
cbz, chm, docx, epub, fb2, fbz, html, htmlz, imp, lit, lrf, lrx, mobi,
odt, oebzip, opf, pdb, pdf, pml, pmlz, pobi, prc, rar, rb, rtf, snb,
tpz, txt, txtz, updb, zip

Supported formats for writing metadata: azw, azw1, azw3, azw4, docx,
epub, fb2, fbz, htmlz, lrf, mobi, odt, pdb, pdf, prc, rtf, tpz, txtz

Different file types support different kinds of metadata. If you try to
set some metadata on a file type that does not support it, the metadata
will be silently ignored.

Whenever you pass arguments to *ebook-meta* that have spaces in them,
enclose the arguments in quotation marks. For example: "/some path/with
spaces"

* [OPTIONS]

#+begin_quote
  - *--author-sort* :: String to be used when sorting by author. If
    unspecified, and the author(s) are specified, it will be
    auto-generated from the author(s).
#+end_quote

#+begin_quote
  - *--authors, -a* :: Set the authors. Multiple authors should be
    separated by the & character. Author names should be in the order
    Firstname Lastname.
#+end_quote

#+begin_quote
  - *--book-producer, -k* :: Set the book producer.
#+end_quote

#+begin_quote
  - *--category* :: Set the book category.
#+end_quote

#+begin_quote
  - *--comments, -c* :: Set the e-book description.
#+end_quote

#+begin_quote
  - *--cover* :: Set the cover to the specified file.
#+end_quote

#+begin_quote
  - *--date, -d* :: Set the published date.
#+end_quote

#+begin_quote
  - *--from-opf* :: Read metadata from the specified OPF file and use it
    to set metadata in the e-book. Metadata specified on the command
    line will override metadata read from the OPF file
#+end_quote

#+begin_quote
  - *--get-cover* :: Get the cover from the e-book and save it at as the
    specified file.
#+end_quote

#+begin_quote
  - *--help, -h* :: show this help message and exit
#+end_quote

#+begin_quote
  - *--identifier* :: Set the identifiers for the book, can be specified
    multiple times. For example: /--identifier/ uri:https://acme.com
    /--identifier/ isbn:12345 To remove an identifier, specify no value,
    /--identifier/ isbn: Note that for EPUB files, an identifier marked
    as the package identifier cannot be removed.
#+end_quote

#+begin_quote
  - *--index, -i* :: Set the index of the book in this series.
#+end_quote

#+begin_quote
  - *--isbn* :: Set the ISBN of the book.
#+end_quote

#+begin_quote
  - *--language, -l* :: Set the language.
#+end_quote

#+begin_quote
  - *--lrf-bookid* :: Set the BookID in LRF files
#+end_quote

#+begin_quote
  - *--publisher, -p* :: Set the e-book publisher.
#+end_quote

#+begin_quote
  - *--rating, -r* :: Set the rating. Should be a number between 1
    and 5.
#+end_quote

#+begin_quote
  - *--series, -s* :: Set the series this e-book belongs to.
#+end_quote

#+begin_quote
  - *--tags* :: Set the tags for the book. Should be a comma separated
    list.
#+end_quote

#+begin_quote
  - *--title, -t* :: Set the title.
#+end_quote

#+begin_quote
  - *--title-sort* :: The version of the title to be used for sorting.
    If unspecified, and the title is specified, it will be
    auto-generated from the title.
#+end_quote

#+begin_quote
  - *--to-opf* :: Specify the name of an OPF file. The metadata will be
    written to the OPF file.
#+end_quote

#+begin_quote
  - *--version* :: show program*'*s version number and exit
#+end_quote

* AUTHOR
Kovid Goyal

* COPYRIGHT
Kovid Goyal
