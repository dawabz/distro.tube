#+TITLE: Man1 - pg_config.1
#+DESCRIPTION: Linux manpage for pg_config.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pg_config - retrieve information about the installed version of
PostgreSQL

* SYNOPSIS
*pg_config* [/option/...]

* DESCRIPTION
The pg_config utility prints configuration parameters of the currently
installed version of PostgreSQL. It is intended, for example, to be used
by software packages that want to interface to PostgreSQL to facilitate
finding the required header files and libraries.

* OPTIONS
To use pg_config, supply one or more of the following options:

*--bindir*

#+begin_quote
  Print the location of user executables. Use this, for example, to find
  the *psql* program. This is normally also the location where the
  pg_config program resides.
#+end_quote

*--docdir*

#+begin_quote
  Print the location of documentation files.
#+end_quote

*--htmldir*

#+begin_quote
  Print the location of HTML documentation files.
#+end_quote

*--includedir*

#+begin_quote
  Print the location of C header files of the client interfaces.
#+end_quote

*--pkgincludedir*

#+begin_quote
  Print the location of other C header files.
#+end_quote

*--includedir-server*

#+begin_quote
  Print the location of C header files for server programming.
#+end_quote

*--libdir*

#+begin_quote
  Print the location of object code libraries.
#+end_quote

*--pkglibdir*

#+begin_quote
  Print the location of dynamically loadable modules, or where the
  server would search for them. (Other architecture-dependent data files
  might also be installed in this directory.)
#+end_quote

*--localedir*

#+begin_quote
  Print the location of locale support files. (This will be an empty
  string if locale support was not configured when PostgreSQL was
  built.)
#+end_quote

*--mandir*

#+begin_quote
  Print the location of manual pages.
#+end_quote

*--sharedir*

#+begin_quote
  Print the location of architecture-independent support files.
#+end_quote

*--sysconfdir*

#+begin_quote
  Print the location of system-wide configuration files.
#+end_quote

*--pgxs*

#+begin_quote
  Print the location of extension makefiles.
#+end_quote

*--configure*

#+begin_quote
  Print the options that were given to the configure script when
  PostgreSQL was configured for building. This can be used to reproduce
  the identical configuration, or to find out with what options a binary
  package was built. (Note however that binary packages often contain
  vendor-specific custom patches.) See also the examples below.
#+end_quote

*--cc*

#+begin_quote
  Print the value of the /CC/ variable that was used for building
  PostgreSQL. This shows the C compiler used.
#+end_quote

*--cppflags*

#+begin_quote
  Print the value of the /CPPFLAGS/ variable that was used for building
  PostgreSQL. This shows C compiler switches needed at preprocessing
  time (typically, -I switches).
#+end_quote

*--cflags*

#+begin_quote
  Print the value of the /CFLAGS/ variable that was used for building
  PostgreSQL. This shows C compiler switches.
#+end_quote

*--cflags_sl*

#+begin_quote
  Print the value of the /CFLAGS_SL/ variable that was used for building
  PostgreSQL. This shows extra C compiler switches used for building
  shared libraries.
#+end_quote

*--ldflags*

#+begin_quote
  Print the value of the /LDFLAGS/ variable that was used for building
  PostgreSQL. This shows linker switches.
#+end_quote

*--ldflags_ex*

#+begin_quote
  Print the value of the /LDFLAGS_EX/ variable that was used for
  building PostgreSQL. This shows linker switches used for building
  executables only.
#+end_quote

*--ldflags_sl*

#+begin_quote
  Print the value of the /LDFLAGS_SL/ variable that was used for
  building PostgreSQL. This shows linker switches used for building
  shared libraries only.
#+end_quote

*--libs*

#+begin_quote
  Print the value of the /LIBS/ variable that was used for building
  PostgreSQL. This normally contains -l switches for external libraries
  linked into PostgreSQL.
#+end_quote

*--version*

#+begin_quote
  Print the version of PostgreSQL.
#+end_quote

*-?*\\
*--help*

#+begin_quote
  Show help about pg_config command line arguments, and exit.
#+end_quote

If more than one option is given, the information is printed in that
order, one item per line. If no options are given, all available
information is printed, with labels.

* NOTES
The options *--docdir*, *--pkgincludedir*, *--localedir*, *--mandir*,
*--sharedir*, *--sysconfdir*, *--cc*, *--cppflags*, *--cflags*,
*--cflags_sl*, *--ldflags*, *--ldflags_sl*, and *--libs* were added in
PostgreSQL 8.1. The option *--htmldir* was added in PostgreSQL 8.4. The
option *--ldflags_ex* was added in PostgreSQL 9.0.

* EXAMPLE
To reproduce the build configuration of the current PostgreSQL
installation, run the following command:

#+begin_quote
  #+begin_example
    eval ./configure `pg_config --configure`
  #+end_example
#+end_quote

The output of pg_config --configure contains shell quotation marks so
arguments with spaces are represented correctly. Therefore, using eval
is required for proper results.
