#+TITLE: Man1 - autorandr.1
#+DESCRIPTION: Linux manpage for autorandr.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
autorandr - automatically select a display configuration based on
connected devices

* SYNOPSIS
*autorandr* [/OPTION/] [/PROFILE/]

* DESCRIPTION
This program automatically detects connected display hardware and then
loads an appropriate X11 setup using xrandr. It also supports the use of
display profiles for different hardware setups.

Autorandr also includes several virtual configurations including *off*,
*common*, *clone-largest*, *horizontal*, and *vertical*. See the
documentation for explanation of each.

* OPTIONS
- *-h*, *--help* :: Display help text and exit

- *-c*, *--change* :: Automatically load the first detected profile

- *-d*, *--default */PROFILE/ :: Make profile /PROFILE/ the default
  profile. The default profile is used if no suitable profile can be
  identified. Else, the current configuration is kept.

- *-l*, *--load */PROFILE/ :: Load profile /PROFILE/

- *-s*, *--save */PROFILE/ :: Save the current setup to profile
  /PROFILE/

- *-r*, *--remove */PROFILE/ :: Remove profile /PROFILE/

- *--batch* :: Run autorandr for all users with active X11 sessions

- *--current* :: List only the current (active) configuration(s)

- *--config* :: Dump the variable values of your current xrandr setup

- *--cycle* :: Cycle through all detected profiles

- *--debug* :: Enable verbose output

- *--detected* :: List only the detected (i.e. available)
  configuration(s)

- *--dry-run* :: Don't change anything, only print the xrandr commands

- *--fingerprint* :: Fingerprint the current hardware setup

- *--match-edid* :: Match displays based on edid instead of name

- *--force* :: Force loading or reloading of a profile

- *--list* :: List all profiles

- *--skip-options [*/OPTION/] ... :: Set a comma-separated list of
  xrandr arguments to skip both in change detection and profile
  application. See *xrandr(1)* for xrandr arguments.

- *--version* :: Show version information and exit

* FILES
Configuration files are searched for in the /autorandr/ directory in the
colon separated list of paths in /$XDG_CONFIG_DIRS/ - or in //etc/xdg/
if that var is not set. They are then looked for in /~/.autorandr/ and
if that doesn't exist, in /$XDG_CONFIG_HOME/autorandr/ or in
/~/.config/autorandr/ if that var is unset.

In each of those directories it looks for directories with /config/ and
/setup/ in them. It is best to manage these files with the *autorandr*
utility.

* AUTHOR
Phillip Berndt <phillip.berndt@googlemail.com>\\
See https://github.com/phillipberndt/autorandr for a full list of
contributors.

* REPORTING BUGS
Report issues upstream on GitHub:
https://github.com/phillipberndt/autorandr/issues\\
Please attach the output of *xrandr --verbose* to your bug report if
appropriate.

* SEE ALSO
For examples, advanced usage (including predefined per-profile & global
hooks and wildcard EDID matching), and full documentation, see
https://github.com/phillipberndt/autorandr.
