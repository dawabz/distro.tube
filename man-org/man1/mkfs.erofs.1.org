#+TITLE: Man1 - mkfs.erofs.1
#+DESCRIPTION: Linux manpage for mkfs.erofs.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
mkfs.erofs - tool to create an EROFS filesystem

* SYNOPSIS
*mkfs.erofs* [/OPTIONS/] /DESTINATION/ /SOURCE/

* DESCRIPTION
EROFS is a new enhanced lightweight linux read-only filesystem with
modern designs (eg. no buffer head, reduced metadata, inline
xattrs/data, etc.) for scenarios which need high-performance read-only
requirements, e.g. Android OS for smartphones and LIVECDs.

It also provides fixed-sized output compression support, which improves
storage density, keeps relatively higher compression ratios, which is
more useful to achieve high performance for embedded devices with
limited memory since it has unnoticable memory overhead and page cache
thrashing.

mkfs.erofs is used to create such EROFS filesystem /DESTINATION/ image
file from /SOURCE/ directory.

* OPTIONS
- *-z */compression-algorithm/* [*/,#/*]* :: Set an algorithm for file
  compression, which can be set with an optional compression level
  separated by a comma.

- *-C */max-pcluster-size/ :: Specify the maximum size of compress
  physical cluster in bytes. It may enable big pcluster feature if
  needed (Linux v5.13+).

- *-d */#/ :: Specify the level of debugging messages. The default is 2,
  which shows basic warning messages.

- *-x */#/ :: Specify the upper limit of an xattr which is still
  inlined. The default is 2. Disable storing xattrs if < 0.

- *-E */extended-option/* [,...]* :: Set extended options for the
  filesystem. Extended options are comma separated, and may take an
  argument using the equals ('=') sign. The following extended options
  are supported:

  - *legacy-compress* :: Disable "decompression in-place" and "compacted
    indexes" support, which is used when generating EROFS images for
    kernel version < 5.3.

  - *force-inode-compact* :: Forcely generate compact inodes (32-byte
    inodes) to output.

  - *force-inode-extended* :: Forcely generate extended inodes (64-byte
    inodes) to output.

  - *noinline_data* :: Don't inline regular files for FSDAX support
    (Linux v5.15+).

  - *force-inode-blockmap* :: Forcely generate inode chunk format in
    4-byte block address array.

  - *force-chunk-indexes* :: Forcely generate inode chunk format in
    8-byte chunk indexes (with device id).

- *-T */#/ :: Set all files to the given UNIX timestamp. Reproducible
  builds requires setting all to a specific one.

- *-U */UUID/ :: Set the universally unique identifier (UUID) of the
  filesystem to /UUID/. The format of the UUID is a series of hex digits
  separated by hyphens, like this:
  "c1b9d5a2-f162-11cf-9ece-0020afc76f16".

- *--all-root* :: Make all files owned by root.

- *--blobdev */file/ :: Specify another extra blob device to store
  chunk-based data.

- *--chunksize */#/ :: Generate chunk-based files with #-byte chunks.

- *--compress-hints */file/ :: If the optional *--compress-hints */file/
  argument is given, *mkfs.erofs* uses it to apply the per-file
  compression strategy. Each line is defined by tokens separated by
  spaces in the following form:

  <pcluster-in-bytes> <match-pattern>

- *--exclude-path=*/path/ :: Ignore file that matches the exact literal
  path. You may give multiple `--exclude-path' options.

- *--exclude-regex=*/regex/ :: Ignore files that match the given regular
  expression. You may give multiple `--exclude-regex` options.

- *--file-contexts=*/file/ :: Specify a /file_contexts/ file to setup /
  override selinux labels.

- *--force-uid=*/UID/ :: Set all file uids to /UID/.

- *--force-gid=*/GID/ :: Set all file gids to /GID/.

- *--help* :: Display this help and exit.

- *--max-extent-bytes */#/ :: Specify maximum decompressed extent size #
  in bytes.

* AUTHOR
This version of *mkfs.erofs* is written by Li Guifu
<blucerlee@gmail.com>, Miao Xie <miaoxie@huawei.com> and Gao Xiang
<xiang@kernel.org> with continuously improvements from others.

This manual page was written by Gao Xiang <xiang@kernel.org>.

* AVAILABILITY
*mkfs.erofs* is part of erofs-utils package and is available from
git://git.kernel.org/pub/scm/linux/kernel/git/xiang/erofs-utils.git.

* SEE ALSO
*mkfs*(8).
