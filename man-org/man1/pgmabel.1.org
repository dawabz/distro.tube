#+TITLE: Man1 - pgmabel.1
#+DESCRIPTION: Linux manpage for pgmabel.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pgmabel - create cross section using Abel Integration for Deconvolution

* SYNOPSIS
*pgmabel* [*-help*] [*-axis* /axis/] [*-factor* /factor/] [*-pixsize*
/pixsize/] [*-left* | *-right*] [*-verbose*] [/filespec/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pgmabel* reads as input a PGM image, which it assumes to be an image of
a rotational symmetric transparent object. The image must have a
vertical symmetry axis. *pgmabel* produces as output an image of a
cross-section of the image.

*pgmabel* does the calculation by performing the Abel Integration for
Deconvolution of an axial-symmetrical image by solving the system of
linear equations.

After integration, *pgmabel* weights all gray-values of one side by the
surface area of the calculated ring in square pixels divided by
4*/factor/ multiplied by the size of one pixel (/pixsize/). With the
*-verbose* option, *pgmabel* prints the weighting factors.

Where the calculation generates a negative result, the output is black.

The computation is unstable against periodic structures with size 2 in
the vertical direction.

* OPTIONS
You can abbreviate any option to its shortest unique prefix.

- *-help* :: Prints a help message.

- *-axis* /axis/ :: Position of the axis of symmetry in the image in
  pixels from the left edge of the image. Default is the center of the
  image.

- *-factor* /factor/ :: User defined factor for enhancement of the
  output. Use a /factor/ less than 1 for decreasing gary values. Default
  is 1.0.

- *-pixsize* /pixsize/ :: The size of a pixel for getting scale
  invariant. Default is 0.1.

- *-left* :: Calculate only the left side of the image. You cannot
  specify both *left* and *right*.

- *-right* :: Analogous to *-left*.

- *-verbose* :: print information about the calculation.

* EXAMPLE
Rotate a PGM image to get an image with a vertical axis of symmetry,
then calculate the cross section:

#+begin_example
      pnmrotate 90 file.pgm | pgmabel -axis 140 >cross_section.pgm
#+end_example

* SEE ALSO
*pnmrotate*(1) , *pgm*(5) ,

* HISTORY
This program was added to Netpbm in Release 10.3 (June 2002).

* AUTHOR
Volker Schmidt (lefti@voyager.boerde.de)

Copyright (C) 1997-2002 German Aerospace research establishment
