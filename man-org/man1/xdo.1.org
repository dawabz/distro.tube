#+TITLE: Man1 - xdo.1
#+DESCRIPTION: Linux manpage for xdo.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xdo - Perform actions on windows

* SYNOPSIS
*xdo* /ACTION/ [/OPTIONS/] [/WID/ ...]

* DESCRIPTION
Apply the given action to the given windows.

If no window IDs and no options are given, the action applies to the
focused window.

* ACTIONS
*close*

#+begin_quote
  Close the window.
#+end_quote

*kill*

#+begin_quote
  Kill the client.
#+end_quote

*hide*

#+begin_quote
  Unmap the window.
#+end_quote

*show*

#+begin_quote
  Map the window.
#+end_quote

*raise*

#+begin_quote
  Raise the window.
#+end_quote

*lower*

#+begin_quote
  Lower the window.
#+end_quote

*below*

#+begin_quote
  Put the window below the target (see *-t*).
#+end_quote

*above*

#+begin_quote
  Put the window above the target (see *-t*).
#+end_quote

*move*

#+begin_quote
  Move the window.
#+end_quote

*resize*

#+begin_quote
  Resize the window.
#+end_quote

*activate*

#+begin_quote
  Activate the window.
#+end_quote

*id*

#+begin_quote
  Print the window's ID.
#+end_quote

*pid*

#+begin_quote
  Print the window's pid.
#+end_quote

*key_press*, *key_release*

#+begin_quote
  Simulate a key press/release event.
#+end_quote

*button_press*, *button_release*

#+begin_quote
  Simulate a button press/release event.
#+end_quote

*pointer_motion*

#+begin_quote
  Simulate a pointer motion event.
#+end_quote

*-h*

#+begin_quote
  Print the synopsis and exit.
#+end_quote

*-v*

#+begin_quote
  Print the version and exit.
#+end_quote

* OPTIONS
When options are provided, the action applies to all the children of the
root window that match the comparisons implied by the options in
relation to the focused window.

*-r*

#+begin_quote
  Distinct ID.
#+end_quote

*-c*

#+begin_quote
  Same class.
#+end_quote

*-C*

#+begin_quote
  Distinct class.
#+end_quote

*-d*

#+begin_quote
  Same desktop.
#+end_quote

*-D*

#+begin_quote
  Distinct desktop.
#+end_quote

*-n* /INSTANCE_NAME/

#+begin_quote
  The window has the given instance name.
#+end_quote

*-N* /CLASS_NAME/

#+begin_quote
  The window has the given class name.
#+end_quote

*-a* /WM_NAME/

#+begin_quote
  The window has the given wm name.
#+end_quote

*-t* /WID/

#+begin_quote
  The target window for the *below* and *above* actions.
#+end_quote

*-p* /PID/

#+begin_quote
  The window has the given pid.
#+end_quote

*-k* /CODE/

#+begin_quote
  Use the given code for the *key_press*, *key_release*, *button_press*
  and *button_release* actions.
#+end_quote

*-x* /[±]PIXELS/

#+begin_quote
  Window x coordinate (or delta) for the *move* and *pointer_motion*
  action.
#+end_quote

*-y* /[±]PIXELS/

#+begin_quote
  Window y coordinate (or delta) for the *move* and *pointer_motion*
  action.
#+end_quote

*-w* /[±]PIXELS/

#+begin_quote
  Window width (or delta) for the *resize* action.
#+end_quote

*-h* /[±]PIXELS/

#+begin_quote
  Window height (or delta) for the *resize* action.
#+end_quote

*-m*

#+begin_quote
  Wait for the existence of a matching window.
#+end_quote

*-s*

#+begin_quote
  Handle symbolic desktop numbers.
#+end_quote

* EXAMPLES
Close the focused window:

#+begin_quote
  #+begin_example
    xdo close
  #+end_example
#+end_quote

Close all the windows having the same class as the focused window:

#+begin_quote
  #+begin_example
    xdo close -c
  #+end_example
#+end_quote

Hide all the windows of the current desktop except the focused window:

#+begin_quote
  #+begin_example
    xdo hide -dr
  #+end_example
#+end_quote

Activate the window which ID is 0x00800109:

#+begin_quote
  #+begin_example
    xdo activate 0x00800109
  #+end_example
#+end_quote

Send fake key press/release events with keycode 46 to the focused
window:

#+begin_quote
  #+begin_example
    xdo key_press -k 46; sleep 0.2; xdo key_release -k 46
  #+end_example
#+end_quote
