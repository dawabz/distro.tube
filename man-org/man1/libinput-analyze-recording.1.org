#+TITLE: Man1 - libinput-analyze-recording.1
#+DESCRIPTION: Linux manpage for libinput-analyze-recording.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libinput-analyze-recording - analyze a device recording

* SYNOPSIS
*libinput analyze recording [--help] [options] /recording.yml/*

* DESCRIPTION
The *libinput analyze recording* tool analyzes a recording made with
*libinput record* and prints a tabular summary of the events in that
recording.

This is a debugging tool only, its output may change at any time. Do not
rely on the output.

* OPTIONS
- *--help* :: Print help

* OUTPUT
An example output for a tablet sequence is below.

#+begin_example
  Time    |      X |      Y | PRESSURE | DISTANCE |   MISC | SERIAL
  -----------------------------------------------------------------
    0.000 |   9717 |   6266 |          |       63 |  0x862 | 0x9a805597 | BTN_TOOL_PEN
    0.005 |   9709 |        |          |          |        | 0x9a805597 | BTN_TOOL_PEN
    0.012 |   9701 |        |          |          |        | 0x9a805597 | BTN_TOOL_PEN
    0.020 |   9692 |   6269 |          |          |        | 0x9a805597 | BTN_TOOL_PEN
    0.028 |   9680 |   6277 |          |          |        | 0x9a805597 | BTN_TOOL_PEN
    0.034 |   9668 |   6279 |          |          |        | 0x9a805597 | BTN_TOOL_PEN
    0.042 |   9654 |   6282 |          |          |        | 0x9a805597 | BTN_TOOL_PEN
#+end_example

* LIBINPUT
Part of the *libinput(1)* suite
