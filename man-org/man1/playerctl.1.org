#+TITLE: Man1 - playerctl.1
#+DESCRIPTION: Linux manpage for playerctl.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The

utility controls MPRIS-enabled media players. In addition to offering
play, pause and stop control,

also offers previous and next track support, the ability to seek
backwards and forwards in a track, and volume control.

also supports displaying metadata

for the current track, and showing the status of the player.

Players that can be controlled using

include

and

However, any player that implements the MPRIS interface specification
can be controlled using

including web browsers.

Playerctl also comes with a daemon called

which keeps track of media player activity. When

is running,

commands will act on the media player with the most recent activity. Run
the command

to start the daemon.

The options are as follows:

Apply command to all available players.

Block and output the updated query when it changes.

Set the output of the current command to

See

Print this help, then exit.

Ignore the specific player

Multiple players can be specified in a comma-separated list.

List the names of running players that can be controlled.

Control the specific player

Multiple players can be specified in a comma-separated list. Defaults to
the first available player. The name "name" matches both "name" and
"name.{INSTANCE}". Additionally, the name "%any" matches any player.

Silence some diagnostic and error messages.

Print version number, then exit.

The commands are as follows:

Get the current status of the player.

Command the player to play.

Command the player to pause.

Command the player to toggle between play and pause.

Command the player to stop.

Command the player to skip to the next track.

Command the player to skip to the previous track.

Print the position of the current track in seconds. With

specified, seek to

seconds from the start of the current track. With the optional

appended, seek forward or backward

seconds from the current position.

Print the player's volume scaled from 0.0

to 1.0

With

specified, set the player's volume to

With the optional

appended, increase or decrease the player's volume by

Print all metadata properties for the current track set by the current
player. If

is specified only the value of

is printed.

Open

in the player.

may be the name of a file or an external URL.

Print the shuffle status of the player. With the shuffle status
specified, set the shuffle status to either

,

, or

Print the loop status of the player. With the loop status specified, set
the loop status to

or

The output of the

and

commands can be controlled using a format string. Variables set by these
commands can be included in the format string by enclosing them in curly
braces:

These will then be expanded on output.

Each command has access to the following variables:

The name of the current player.

The time position of the current track, in microseconds.

The status of the current player.

The player's volume scaled from 0.0

to 1.0

Each property listed in the

command are also set as variables. It is recommended to check this list
for each player, as different players may not set the same properties.
See the for a list of all properties in the MPRIS specification. The
most common properties are as follows:

The album of the current track.

The artist of the current track.

The title of the current track.

Helper functions are also available to transform expanded variables into
other representations. They are called in the form

The helper functions are as follows:

Convert string

to lowercase.

Convert string

to uppercase.

Escape XML characters in string

Print

if set, else print

Reformat the microsecond timestamp

in the form

Can only be called with

or

Try to convert the value for

to an emoji representation. Currently implemented for

and

Truncate

to a maximum of

characters, adding an ellipsis (...) if necessary.

The template language is also able to perform basic math operations.

References to unknown functions will cause

to exit with an error. References to unknown variables will be expanded
to empty strings. Text not enclosed in braces will be printed verbatim.

Print the player name, playback status in lowercase, and position and
length in human readable form:

$ playerctl metadata --format '{{playerName}}: {{lc(status)}} '\
'{{duration(position)}}|{{duration(mpris:length)}}'

The

utility is maintained by

and is made available under the GNU Lesser General Public License 3.0.

This reference was written by

for the Debian GNU/Linux project. It was later updated and expanded by
