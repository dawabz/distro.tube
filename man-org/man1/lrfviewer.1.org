#+TITLE: Man1 - lrfviewer.1
#+DESCRIPTION: Linux manpage for lrfviewer.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lrfviewer - lrfviewer

#+begin_quote

  #+begin_quote
    #+begin_example
      lrfviewer [options] book.lrf
    #+end_example
  #+end_quote
#+end_quote

Read the LRF e-book book.lrf

Whenever you pass arguments to *lrfviewer* that have spaces in them,
enclose the arguments in quotation marks. For example: "/some path/with
spaces"

* [OPTIONS]

#+begin_quote
  - *--disable-hyphenation* :: Disable hyphenation. Should significantly
    speed up rendering.
#+end_quote

#+begin_quote
  - *--help, -h* :: show this help message and exit
#+end_quote

#+begin_quote
  - *--profile* :: Profile the LRF renderer
#+end_quote

#+begin_quote
  - *--verbose* :: Print more information about the rendering process
#+end_quote

#+begin_quote
  - *--version* :: show program*'*s version number and exit
#+end_quote

#+begin_quote
  - *--visual-debug* :: Turn on visual aids to debugging the rendering
    engine
#+end_quote

#+begin_quote
  - *--white-background* :: By default the background is off white as I
    find this easier on the eyes. Use this option to make the background
    pure white.
#+end_quote

* AUTHOR
Kovid Goyal

* COPYRIGHT
Kovid Goyal
