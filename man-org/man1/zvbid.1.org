#+TITLE: Man1 - zvbid.1
#+DESCRIPTION: Linux manpage for zvbid.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zvbid - VBI proxy daemon

* SYNOPSIS
*zvbid [ options ]*

* DESCRIPTION
*zvbid* is a proxy for VBI devices, i.e. it forwards one or more VBI
data streams to one or more connected clients and manages channel change
requests.

* OPTIONS
- *-dev* path :: Path of a device from which to read data. This argument
  can be given several times with different devices.

- *-buffers* count :: Number of buffers to allocate for capturing VBI
  raw data from devices which support streaming (currently only
  video4linux, rev. 2) A higher number of buffers can prevent data loss
  in case of high latency. The downside is higher memory consumption
  (typically 65kB per buffer.) Default count is 8, maximum is 32.

- *-nodetach* :: Daemon process remains connected to the terminal from
  which it was started (e.g. so that you can stop it by pressing
  Control-C keys). Intended for trouble shooting only.

- *-kill* :: Terminates a proxy daemon running for the given device.

- *-debug* level :: Enables debug output: 0= off(default); 1= general
  messages; In addition 2, 4, 8, ... can be added to enable debug output
  for various categories.

- *-syslog* level :: Enables syslog output.

- *-loglevel* level :: Log file level

- *-logfile* path :: Path to the log file.

- *-maxclients* count :: Max. number of clients which are allowed to
  connect simultaneously.

- *-help* :: Print a short description of all command line options.

* SEE ALSO
zvbi-chains(1), v4l-conf(8)\\
http://zapping.sourceforge.net/ (homepage)

* AUTHOR
Tom Zoerner (tomzo AT users.sourceforge.net)

* COPYRIGHT
Copyright (C) 2003,2004 Tom Zoerner

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
675 Mass Ave, Cambridge, MA 02139, USA.
