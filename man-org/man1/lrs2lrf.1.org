#+TITLE: Man1 - lrs2lrf.1
#+DESCRIPTION: Linux manpage for lrs2lrf.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lrs2lrf - lrs2lrf

#+begin_quote

  #+begin_quote
    #+begin_example
      lrs2lrf [options] file.lrs
    #+end_example
  #+end_quote
#+end_quote

Compile an LRS file into an LRF file.

Whenever you pass arguments to *lrs2lrf* that have spaces in them,
enclose the arguments in quotation marks. For example: "/some path/with
spaces"

* [OPTIONS]

#+begin_quote
  - *--help, -h* :: show this help message and exit
#+end_quote

#+begin_quote
  - *--lrs* :: Convert LRS to LRS, useful for debugging.
#+end_quote

#+begin_quote
  - *--output, -o* :: Path to output file
#+end_quote

#+begin_quote
  - *--verbose* :: Verbose processing
#+end_quote

#+begin_quote
  - *--version* :: show program*'*s version number and exit
#+end_quote

* AUTHOR
Kovid Goyal

* COPYRIGHT
Kovid Goyal
