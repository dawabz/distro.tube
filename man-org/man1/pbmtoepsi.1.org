#+TITLE: Man1 - pbmtoepsi.1
#+DESCRIPTION: Linux manpage for pbmtoepsi.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmtoepsi - convert a PBM image to an encapsulated PostScript style
preview bitmap

* SYNOPSIS
*pbmtoepsi* [*-dpi=*/N/[*x*/N/]] [*-bbonly*] [/pbmfile/]

All options can be abbreviated to their shortest unique prefix. You may
use two hyphens instead of one. You may separate an option name and its
value with white space instead of an equals sign.

* DESCRIPTION
This program is part of *Netpbm*(1)

Reads a PBM image as input. Produces an encapsulated Postscript style
bitmap as output. The output is not a stand alone postscript file, it is
only a preview bitmap, which can be included in an encapsulated
PostScript file.

*pbmtoepsi* assumes the PBM input describes a whole output page, with
one pixel on the page corresponding to one PBM pixel. It detects white
borders in the image and generates Postscript output that contains a
Bounding Box statement to describe the location of the principal image
(the image excluding the white borders) on the page and thus does not
include the borders in the raster part of the Postscript output.

There is no *epsitopbm* tool - this transformation is one way.

* OPTIONS
- *-dpi=*/N/[*x*/N/] :: This option specifies the resolution in dots per
  inch of the ultimate output device. You must specify this because the
  Bounding Box statement defines the bounding box in absolute distances,
  not in pixels. *pbmtoepsi* assumes in calculating the bounding box
  that each PBM pixel will become one dot on the output device, and
  applies your *dpi* specification to calculate the size and location on
  the page of the bounding box.

If you specify /N/*x*N, the first number is the horizontal resolution
and the second number is the vertical resolution. If you specify just a
single number /N/, that is the resolution in both directions.

The default is 72 dots per inch in both directions.

This option was new In Netpbm 10.3 (June 2002). Before that, *pbmtoepsi*
always assumed 72 dots per inch in both directions.

- *-bbonly* :: Only create a boundary box, don't fill it with the image.

* SEE ALSO
*pbm*(5) , *pnmtops*(1) , *pstopnm*(1) , *psidtopgm*(1) , *pbmtolps*(1)
,

Postscript language documentation

* AUTHOR
Copyright (C) 1988 Jef Poskanzer, modified by Doug Crabill 1992
