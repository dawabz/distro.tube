#+TITLE: Man1 - cargo-tree.1
#+DESCRIPTION: Linux manpage for cargo-tree.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cargo-tree - Display a tree visualization of a dependency graph

* SYNOPSIS
*cargo tree* [/options/]

* DESCRIPTION
This command will display a tree of dependencies to the terminal. An
example of a simple project that depends on the "rand" package:

#+begin_quote
  #+begin_example
    myproject v0.1.0 (/myproject)
    `-- rand v0.7.3
        |-- getrandom v0.1.14
        |   |-- cfg-if v0.1.10
        |   `-- libc v0.2.68
        |-- libc v0.2.68 (*)
        |-- rand_chacha v0.2.2
        |   |-- ppv-lite86 v0.2.6
        |   `-- rand_core v0.5.1
        |       `-- getrandom v0.1.14 (*)
        `-- rand_core v0.5.1 (*)
    [build-dependencies]
    `-- cc v1.0.50
  #+end_example
#+end_quote

Packages marked with *(*)* have been "de-duplicated". The dependencies
for the package have already been shown elsewhere in the graph, and so
are not repeated. Use the *--no-dedupe* option to repeat the duplicates.

The *-e* flag can be used to select the dependency kinds to display. The
"features" kind changes the output to display the features enabled by
each dependency. For example, *cargo tree -e features*:

#+begin_quote
  #+begin_example
    myproject v0.1.0 (/myproject)
    `-- log feature "serde"
        `-- log v0.4.8
            |-- serde v1.0.106
            `-- cfg-if feature "default"
                `-- cfg-if v0.1.10
  #+end_example
#+end_quote

In this tree, *myproject* depends on *log* with the *serde* feature.
*log* in turn depends on *cfg-if* with "default" features. When using
*-e features* it can be helpful to use *-i* flag to show how the
features flow into a package. See the examples below for more detail.

* OPTIONS
** Tree Options
*-i* /spec/, *--invert* /spec/

#+begin_quote
  Show the reverse dependencies for the given package. This flag will
  invert the tree and display the packages that depend on the given
  package.

  Note that in a workspace, by default it will only display the
  package's reverse dependencies inside the tree of the workspace member
  in the current directory. The *--workspace* flag can be used to extend
  it so that it will show the package's reverse dependencies across the
  entire workspace. The *-p* flag can be used to display the package's
  reverse dependencies only with the subtree of the package given to
  *-p*.
#+end_quote

*--prune* /spec/

#+begin_quote
  Prune the given package from the display of the dependency tree.
#+end_quote

*--depth* /depth/

#+begin_quote
  Maximum display depth of the dependency tree. A depth of 1 displays
  the direct dependencies, for example.
#+end_quote

*--no-dedupe*

#+begin_quote
  Do not de-duplicate repeated dependencies. Usually, when a package has
  already displayed its dependencies, further occurrences will not
  re-display its dependencies, and will include a *(*)* to indicate it
  has already been shown. This flag will cause those duplicates to be
  repeated.
#+end_quote

*-d*, *--duplicates*

#+begin_quote
  Show only dependencies which come in multiple versions (implies
  *--invert*). When used with the *-p* flag, only shows duplicates
  within the subtree of the given package.

  It can be beneficial for build times and executable sizes to avoid
  building that same package multiple times. This flag can help identify
  the offending packages. You can then investigate if the package that
  depends on the duplicate with the older version can be updated to the
  newer version so that only one instance is built.
#+end_quote

*-e* /kinds/, *--edges* /kinds/

#+begin_quote
  The dependency kinds to display. Takes a comma separated list of
  values:

  #+begin_quote
    ·*all* --- Show all edge kinds.
  #+end_quote

  #+begin_quote
    ·*normal* --- Show normal dependencies.
  #+end_quote

  #+begin_quote
    ·*build* --- Show build dependencies.
  #+end_quote

  #+begin_quote
    ·*dev* --- Show development dependencies.
  #+end_quote

  #+begin_quote
    ·*features* --- Show features enabled by each dependency. If this is
    the only kind given, then it will automatically include the other
    dependency kinds.
  #+end_quote

  #+begin_quote
    ·*no-normal* --- Do not include normal dependencies.
  #+end_quote

  #+begin_quote
    ·*no-build* --- Do not include build dependencies.
  #+end_quote

  #+begin_quote
    ·*no-dev* --- Do not include development dependencies.
  #+end_quote

  #+begin_quote
    ·*no-proc-macro* --- Do not include procedural macro dependencies.
  #+end_quote

  The *normal*, *build*, *dev*, and *all* dependency kinds cannot be
  mixed with *no-normal*, *no-build*, or *no-dev* dependency kinds.

  The default is *normal,build,dev*.
#+end_quote

*--target* /triple/

#+begin_quote
  Filter dependencies matching the given target-triple. The default is
  the host platform. Use the value *all* to include /all/ targets.
#+end_quote

** Tree Formatting Options
*--charset* /charset/

#+begin_quote
  Chooses the character set to use for the tree. Valid values are "utf8"
  or "ascii". Default is "utf8".
#+end_quote

*-f* /format/, *--format* /format/

#+begin_quote
  Set the format string for each package. The default is "{p}".

  This is an arbitrary string which will be used to display each
  package. The following strings will be replaced with the corresponding
  value:

  #+begin_quote
    ·*{p}* --- The package name.
  #+end_quote

  #+begin_quote
    ·*{l}* --- The package license.
  #+end_quote

  #+begin_quote
    ·*{r}* --- The package repository URL.
  #+end_quote

  #+begin_quote
    ·*{f}* --- Comma-separated list of package features that are
    enabled.
  #+end_quote

  #+begin_quote
    ·*{lib}* --- The name, as used in a *use* statement, of the
    package's library.
  #+end_quote
#+end_quote

*--prefix* /prefix/

#+begin_quote
  Sets how each line is displayed. The /prefix/ value can be one of:

  #+begin_quote
    ·*indent* (default) --- Shows each line indented as a tree.
  #+end_quote

  #+begin_quote
    ·*depth* --- Show as a list, with the numeric depth printed before
    each entry.
  #+end_quote

  #+begin_quote
    ·*none* --- Show as a flat list.
  #+end_quote
#+end_quote

** Package Selection
By default, when no package selection options are given, the packages
selected depend on the selected manifest file (based on the current
working directory if *--manifest-path* is not given). If the manifest is
the root of a workspace then the workspaces default members are
selected, otherwise only the package defined by the manifest will be
selected.

The default members of a workspace can be set explicitly with the
*workspace.default-members* key in the root manifest. If this is not
set, a virtual workspace will include all workspace members (equivalent
to passing *--workspace*), and a non-virtual workspace will include only
the root crate itself.

*-p* /spec/..., *--package* /spec/...

#+begin_quote
  Display only the specified packages. See *cargo-pkgid*(1) for the SPEC
  format. This flag may be specified multiple times and supports common
  Unix glob patterns like ***, *?* and *[]*. However, to avoid your
  shell accidentally expanding glob patterns before Cargo handles them,
  you must use single quotes or double quotes around each pattern.
#+end_quote

*--workspace*

#+begin_quote
  Display all members in the workspace.
#+end_quote

*--exclude* /SPEC/...

#+begin_quote
  Exclude the specified packages. Must be used in conjunction with the
  *--workspace* flag. This flag may be specified multiple times and
  supports common Unix glob patterns like ***, *?* and *[]*. However, to
  avoid your shell accidentally expanding glob patterns before Cargo
  handles them, you must use single quotes or double quotes around each
  pattern.
#+end_quote

** Manifest Options
*--manifest-path* /path/

#+begin_quote
  Path to the *Cargo.toml* file. By default, Cargo searches for the
  *Cargo.toml* file in the current directory or any parent directory.
#+end_quote

*--frozen*, *--locked*

#+begin_quote
  Either of these flags requires that the *Cargo.lock* file is
  up-to-date. If the lock file is missing, or it needs to be updated,
  Cargo will exit with an error. The *--frozen* flag also prevents Cargo
  from attempting to access the network to determine if it is
  out-of-date.

  These may be used in environments where you want to assert that the
  *Cargo.lock* file is up-to-date (such as a CI build) or want to avoid
  network access.
#+end_quote

*--offline*

#+begin_quote
  Prevents Cargo from accessing the network for any reason. Without this
  flag, Cargo will stop with an error if it needs to access the network
  and the network is not available. With this flag, Cargo will attempt
  to proceed without the network if possible.

  Beware that this may result in different dependency resolution than
  online mode. Cargo will restrict itself to crates that are downloaded
  locally, even if there might be a newer version as indicated in the
  local copy of the index. See the *cargo-fetch*(1) command to download
  dependencies before going offline.

  May also be specified with the *net.offline* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

** Feature Selection
The feature flags allow you to control which features are enabled. When
no feature options are given, the *default* feature is activated for
every selected package.

See /the features documentation/
<https://doc.rust-lang.org/cargo/reference/features.html#command-line-feature-options>
for more details.

*--features* /features/

#+begin_quote
  Space or comma separated list of features to activate. Features of
  workspace members may be enabled with *package-name/feature-name*
  syntax. This flag may be specified multiple times, which enables all
  specified features.
#+end_quote

*--all-features*

#+begin_quote
  Activate all available features of all selected packages.
#+end_quote

*--no-default-features*

#+begin_quote
  Do not activate the *default* feature of the selected packages.
#+end_quote

** Display Options
*-v*, *--verbose*

#+begin_quote
  Use verbose output. May be specified twice for "very verbose" output
  which includes extra output such as dependency warnings and build
  script output. May also be specified with the *term.verbose* /config
  value/ <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

*-q*, *--quiet*

#+begin_quote
  No output printed to stdout.
#+end_quote

*--color* /when/

#+begin_quote
  Control when colored output is used. Valid values:

  #+begin_quote
    ·*auto* (default): Automatically detect if color support is
    available on the terminal.
  #+end_quote

  #+begin_quote
    ·*always*: Always display colors.
  #+end_quote

  #+begin_quote
    ·*never*: Never display colors.
  #+end_quote

  May also be specified with the *term.color* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

** Common Options
*+*/toolchain/

#+begin_quote
  If Cargo has been installed with rustup, and the first argument to
  *cargo* begins with *+*, it will be interpreted as a rustup toolchain
  name (such as *+stable* or *+nightly*). See the /rustup documentation/
  <https://rust-lang.github.io/rustup/overrides.html> for more
  information about how toolchain overrides work.
#+end_quote

*-h*, *--help*

#+begin_quote
  Prints help information.
#+end_quote

*-Z* /flag/

#+begin_quote
  Unstable (nightly-only) flags to Cargo. Run *cargo -Z help* for
  details.
#+end_quote

* ENVIRONMENT
See /the reference/
<https://doc.rust-lang.org/cargo/reference/environment-variables.html>
for details on environment variables that Cargo reads.

* EXIT STATUS

#+begin_quote
  ·*0*: Cargo succeeded.
#+end_quote

#+begin_quote
  ·*101*: Cargo failed to complete.
#+end_quote

* EXAMPLES

#+begin_quote
  1.Display the tree for the package in the current directory:

  #+begin_quote
    #+begin_example
      cargo tree
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  2.Display all the packages that depend on the *syn* package:

  #+begin_quote
    #+begin_example
      cargo tree -i syn
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  3.Show the features enabled on each package:

  #+begin_quote
    #+begin_example
      cargo tree --format "{p} {f}"
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  4.Show all packages that are built multiple times. This can happen if
  multiple semver-incompatible versions appear in the tree (like 1.0.0
  and 2.0.0).

  #+begin_quote
    #+begin_example
      cargo tree -d
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  5.Explain why features are enabled for the *syn* package:

  #+begin_quote
    #+begin_example
      cargo tree -e features -i syn
    #+end_example
  #+end_quote

  The *-e features* flag is used to show features. The *-i* flag is used
  to invert the graph so that it displays the packages that depend on
  *syn*. An example of what this would display:

  #+begin_quote
    #+begin_example
      syn v1.0.17
      |-- syn feature "clone-impls"
      |   `-- syn feature "default"
      |       `-- rustversion v1.0.2
      |           `-- rustversion feature "default"
      |               `-- myproject v0.1.0 (/myproject)
      |                   `-- myproject feature "default" (command-line)
      |-- syn feature "default" (*)
      |-- syn feature "derive"
      |   `-- syn feature "default" (*)
      |-- syn feature "full"
      |   `-- rustversion v1.0.2 (*)
      |-- syn feature "parsing"
      |   `-- syn feature "default" (*)
      |-- syn feature "printing"
      |   `-- syn feature "default" (*)
      |-- syn feature "proc-macro"
      |   `-- syn feature "default" (*)
      `-- syn feature "quote"
          |-- syn feature "printing" (*)
          `-- syn feature "proc-macro" (*)
    #+end_example
  #+end_quote

  To read this graph, you can follow the chain for each feature from the
  root to see why it is included. For example, the "full" feature is
  added by the *rustversion* crate which is included from *myproject*
  (with the default features), and *myproject* is the package selected
  on the command-line. All of the other *syn* features are added by the
  "default" feature ("quote" is added by "printing" and "proc-macro",
  both of which are default features).

  If you're having difficulty cross-referencing the de-duplicated *(*)*
  entries, try with the *--no-dedupe* flag to get the full output.
#+end_quote

* SEE ALSO
*cargo*(1), *cargo-metadata*(1)
