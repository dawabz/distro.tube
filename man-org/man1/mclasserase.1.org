#+TITLE: Man1 - mclasserase.1
#+DESCRIPTION: Linux manpage for mclasserase.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* Name
mclasserase - erase memory cards

* Note of warning
This manpage has been automatically generated from mtools's texinfo
documentation, and may not be entirely accurate or complete. See the end
of this man page for details.

* Description
The =mclasserase= command is used to wipe memory cards by overwriting it
three times: first with =0xff=, then with =0x00=, then with =0xff=
again. The command uses the following syntax:

#+begin_example
  mclasserase [-d] msdosdrive
#+end_example

MS-DOS drive is optional, if none is specified, use =A:=. If more than
one drive are specified, all but the last are ignored.

=Mclasserase= accepts the following command line options:

- =d=  :: Stop after each erase cycle, for testing purposes

- =p=  :: Not yet implemented

=Mclasserase= returns 0 on success or -1 on failure.

* See Also
Mtools' texinfo doc

* Viewing the texi doc
This manpage has been automatically generated from mtools's texinfo
documentation. However, this process is only approximative, and some
items, such as crossreferences, footnotes and indices are lost in this
translation process. Indeed, these items have no appropriate
representation in the manpage format. Moreover, not all information has
been translated into the manpage version. Thus I strongly advise you to
use the original texinfo doc. See the end of this manpage for
instructions how to view the texinfo doc.

- *  :: To generate a printable copy from the texinfo doc, run the
  following commands:

#+begin_example
      ./configure; make dvi; dvips mtools.dvi
#+end_example

- *  :: To generate a html copy, run:

#+begin_example
      ./configure; make html
#+end_example

A premade html can be found at
=∞http://www.gnu.org/software/mtools/manual/mtools.html∫=

- *  :: To generate an info copy (browsable using emacs' info mode),
  run:

#+begin_example
      ./configure; make info
#+end_example

The texinfo doc looks most pretty when printed or as html. Indeed, in
the info version certain examples are difficult to read due to the
quoting conventions used in info.
