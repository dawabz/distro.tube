#+TITLE: Man1 - eu-readelf.1
#+DESCRIPTION: Linux manpage for eu-readelf.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
eu-readelf - Displays information about ELF files.

* SYNOPSIS
eu-readelf [*-a*|*--all*] [*-h*|*--file-header*]
[*-l*|*--program-headers*|*--segments*]
[*-S*|*--section-headers*|*--sections*] [*-g*|*--section-groups*]
[*-e*|*--exception*] [*-s*|*--symbols*] [section name] ] [*--dyn-syms*]
[*-n*|*--notes* [section name] ] [*-r*|*--relocs*] [*-d*|*--dynamic*]
[*-V*|*--version-info*] [*-A*|*--arch-specific*] [*-x* <number or
name>|*--hex-dump=*<number or name>] [*-p* <number or
name>|*--string-dump=*<number or name>] [*-z*|*--decompress*]
[*-c*|*--archive-index*] [*--dwarf-skeleton* <file> ] [*--elf-section*
[section] ] [*-w*|
*--debug-dump*[=line,=decodedline,=info,=info+,=abbrev,=pubnames,=aranges,=macro,=frames,=str,=loc,=ranges,=gdb_index,=addr]]
[*-I*|*--histogram*] [*-v*|*--version*] [*-W*|*--wide*] [*-H*|*--help*]
/elffile/...

* DESCRIPTION
*eu-readelf* displays information about one or more ELF format object
files. The options control what particular information to display.

/elffile/... are the object files to be examined. 32-bit and 64-bit ELF
files are supported, as are archives containing ELF files.

This program performs a similar function to *objdump* but it goes into
more detail and it exists independently of the BFD library, so if there
is a bug in BFD then readelf will not be affected.

* OPTIONS
The long and short forms of options, shown here as alternatives, are
equivalent. At least one option in addition to *-v* or *-H* must be
given.

** ELF Input Selection
- --dwarf-skeleton <file> :: 

Used with -w to find the skeleton Compile Units in FILE associated with
the Split Compile units in a .dwo input file.

- --elf-section [section] :: 

Use the named SECTION (default .gnu_debugdata) as (compressed) ELF input
data

** ELF Output Selection
- -a :: 

- --all :: 

Equivalent to specifying *--file-header*, *--program-headers*,
*--sections*, *--symbols*, *--relocs*, *--dynamic*, *--notes*,
*--version-info*, *--arch-specific*, *--section-groups* and
*--histogram*.

- -h :: 

- --file-header :: 

Displays the information contained in the ELF header at the start of the
file.

- -l :: 

- --program-headers :: 

- --segments :: 

Displays the information contained in the file's segment headers, if it
has any.

- -S :: 

- --sections :: 

- --section-headers :: 

Displays the information contained in the file's section headers, if it
has any.

- -g :: 

- --section-groups :: 

Displays the information contained in the file's section groups, if it
has any.

- -I :: 

- --histogram :: 

Display a histogram of bucket list lengths when displaying the contents
of the symbol tables.

- -s :: 

- --symbols [section name] :: 

Displays the entries in symbol table section of the file, if it has one.
If a symbol has version information associated with it then this is
displayed as well. The version string is displayed as a suffix to the
symbol name, preceded by an @ character. For example *foo@VER_1*. If the
version is the default version to be used when resolving unversioned
references to the symbol then it is displayed as a suffix preceded by
two @ characters. For example *foo@@VER_2*.

- --dyn-syms :: 

Display (only) the dynamic symbol table.

- -e :: 

- --exception :: 

Display sections for exception handling.

- -n :: 

- --notes [section name] :: 

Displays the contents of the NOTE segments and/or sections, if any.

- -r :: 

- --relocs :: 

Displays the contents of the file's relocation section, if it has one.

- -d :: 

- --dynamic :: 

Displays the contents of the file's dynamic section, if it has one.

- -V :: 

- --version-info :: 

Displays the contents of the version sections in the file, it they
exist.

- -A :: 

- --arch-specific :: 

Displays architecture-specific information in the file, if there is any.

** Additional output selection
- -x <name> :: 

- --hex-dump=<name> :: 

Displays the contents of the indicated section name as a hexadecimal
bytes.

- -w :: 

- --debug-dump[=decodedline,=info,=info+,=abbrev,=pubnames,=aranges,=macro,=frames,=str,=loc,=ranges,=gdb_index,=addr] :: 

Displays the contents of the DWARF debug sections in the file, if any
are present. Compressed debug sections are automatically decompressed
(temporarily) before they are displayed. If one or more of the optional
letters or words follows the switch then only those type(s) of data will
be dumped. The letters and words refer to the following information:

#+begin_quote
  - "=abbrev" :: 

  Displays the contents of the *.debug_abbrev* section.

  - "=addr" :: 

  Displays the contents of the *.debug_addr* section.

  - "=frames" :: 

  Display the raw contents of a *.debug_frame* section.

  - "=gdb_index" :: 

  Displays the contents of the *.gdb_index* and/or *.debug_names*
  sections.

  - "=info" :: 

  Displays the contents of the *.debug_info* section.

  - "=info+" :: 

  Displays the contents of the *.debug_info* section, plus any skeleton
  unit will be immediately followed by the corresponding split compile
  unit (from the .dwo file). To show the difference between "regular"
  CUs and split CUs print offsets and references between { and } instead
  of [ and ].

  - "=decodedline" :: 

  Displays the interpreted contents of the *.debug_line* section.

  - "=macro" :: 

  Displays the contents of the *.debug_macro* and/or *.debug_macinfo*
  sections.

  - "=loc" :: 

  Displays the contents of the *.debug_loc* and/or *.debug_loclists*
  sections.

  - "=pubnames" :: 

  Displays the contents of the *.debug_pubnames* and/or
  *.debug_gnu_pubnames* sections.

  - "=aranges" :: 

  Displays the contents of the *.debug_aranges* section.

  - "=ranges" :: 

  Displays the contents of the *.debug_ranges* and/or *.debug_rnglists*
  sections.

  - "=str" :: 

  Displays the contents of the *.debug_str*, *.debug_line_str* and/or
  *.debug_str_offsets* sections.

  #+begin_quote
    Note: displaying the contents of *.debug_static_funcs*,
    *.debug_static_vars* and *debug_weaknames* sections is not currently
    supported.
  #+end_quote

  - -p <number or name> :: 

  - --string-dump=<number or name> :: 

  Displays the contents of the indicated section as printable strings. A
  number identifies a particular section by index in the section table;
  any other string identifies all sections with that name in the object
  file.

  - -c :: 

  - --archive-index :: 

  Displays the file symbol index information contained in the header
  part of binary archives. Performs the same function as the *t* command
  to *ar*, but without using the BFD library.
#+end_quote

** Output control
- -z :: 

- --decompress :: 

Requests that the section(s) being dumped by *x*, *R* or *p* options are
decompressed before being displayed. If the section(s) are not
compressed then they are displayed as is.

- -v :: 

- --version :: 

Display the version number of eu-readelf.

- -W :: 

- --wide :: 

Ignored for compatibility (lines always wide).

- -H :: 

- --help :: 

Display the command line options understood by *eu-readelf*.

1. Read command-line options from /file/. The options read are inserted
   in place of the original @/file/ option. If /file/ does not exist, or
   cannot be read, then the option will be treated literally, and not
   removed. Options in /file/ are separated by whitespace. A whitespace
   character may be included in an option by surrounding the entire
   option in either single or double quotes. Any character (including a
   backslash) may be included by prefixing the character to be included
   with a backslash. The /file/ may itself contain additional @/file/
   options; any such options will be processed recursively.

* SEE ALSO
/objdump/ (1), /readelf/ (1) and the Info entries for /binutils/.

* COPYRIGHT
Copyright (c) 1991-2018 Free Software Foundation, Inc.

Copyright (c) 2019 Red Hat Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts, and with no Back-Cover
Texts. A copy of the license is included in the section entitled GNU
Free Documentation License.
