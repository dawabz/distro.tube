#+TITLE: Man1 - kateenc.1
#+DESCRIPTION: Linux manpage for kateenc.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
kateenc - create Kate streams from text input

* SYNOPSIS
*kateenc* *[-hVM]* *[-o */outfile/*]* *[-l */language/*]* *[-c
*/category/*]* *[-s */serial/*]* *[-C */tag=value/*]* *[-R
*/threshold/*]* *[-K */threshold/*]* *-t */filetype/ [ infile ]

* DESCRIPTION
*kateenc* creates Kate streams from input files (SubRip SRT subtitles,
LRC lyrics, and libkate's custom description language). When saved to an
Ogg stream (the default behavior), a Kate stream may then be merged with
other Ogg streams (eg, a Theora video) using the oggz tools.

* OPTIONS
- *-h* :: Show command line help.

- *-V* :: Show version information.

- *-M* :: Allow simple HTML-like markup in SRT files. If this parameter
  is used, strings such as "<i>" will be interpreted to mean italics,
  rather than appear as such. Note that various players interpret a
  slightly different set of HTML tags, and some do not.

- *-o* outfile :: Write the output Kate stream to the given file name
  (writes to stdout if not specified).

- *-l* language :: Sets the language for the stream (default is the
  language specified in the input, or none).

- *-c* category :: Sets the category for the stream (default is the
  category specified in the input, or none).

- *-s* serial :: Sets the serial number for the output Kate stream (does
  not apply to raw streams).

- *-C* tag=value :: Adds a comment to encode into the output Kate
  stream. The comment must be of the form tag=value, and comply with the
  Vorbis comment rules (eg, tag is composed of a particular subset of
  ASCII characters, and value is valid UTF-8). Note that you might have
  to escape or quote the comment for it to be seen as a single command
  line parameter.

- *-t* filetype :: Specifies the type of the input (srt, lrc, kate)

- *-R* threshold :: Use repeat packets, with the given threshold (in
  seconds). Data packets will be repeated at intervals of roughly
  threshold seconds, while active.

- *-K* threshold :: Use keepalive packets, with the given threshold (in
  seconds). Keepalive packets will be emitted when no other packet has
  been emitted for roughly threshold seconds.

* EXAMPLES
Create a Kate stream for Welsh subtitles from a SubRip file:

kateenc -t srt -l cy -c SUB -o output.ogg input.srt

* SEE ALSO
*katedec*(1), *katalyzer*(1)
