#+TITLE: Man1 - svn.1
#+DESCRIPTION: Linux manpage for svn.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
svn - Subversion command line client tool

* SYNOPSIS
- *svn* /command/ [/options/] [/args/] :: 

* OVERVIEW
Subversion is a version control system, which allows you to keep old
versions of files and directories (usually source code), keep a log of
who, when, and why changes occurred, etc., like CVS, RCS or SCCS.
*Subversion* keeps a single copy of the master sources. This copy is
called the source ``repository''; it contains all the information to
permit extracting previous versions of those files at any time.

For more information about the Subversion project, visit
http://subversion.apache.org.

Documentation for Subversion and its tools, including detailed usage
explanations of the *svn*, *svnadmin*, *svnserve* and *svnlook*
programs, historical background, philosophical approaches and
reasonings, etc., can be found at http://svnbook.red-bean.com/.

Run `svn help' to access the built-in tool documentation.
