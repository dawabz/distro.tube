#+TITLE: Manpages - systemd-umount.1
#+DESCRIPTION: Linux manpage for systemd-umount.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about systemd-umount.1 is found in manpage for: [[../systemd-mount.1][systemd-mount.1]]