#+TITLE: Man1 - wildmidi.1
#+DESCRIPTION: Linux manpage for wildmidi.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
wildmidi - example player for libWildMidi

* LIBRARY
*libWildMidi*

* FILES
*/etc/wildmidi/wildmidi.cfg*

* SYNOPSIS
*wildmidi [-bhlvwnst] [-c /config-file/*]* [-d /audiodev/*]* [-m
/volume-level/*]* [-o /wav-file/*]* [-f /frequency-Hz(MUS)/*]* [-r
/sample-rate/*]* [-g /convert-xmi-type/*]* /midifile/ ...*

* DESCRIPTION
This is a demonstration program to show the capabilities of libWildMidi.

/midifile can be a MIDI type file in the HMI, HMP, MIDI, MUS or XMI
formats and is processed by libWildMidi and the resulting audio is
output by the player./

You can have more than one /midifile on the command line and
/*wildmidi*/ will pass them to libWildMidi for processing, one after the
other. You can also use wildcards, for example: /*wildmidi *.mid*

* OPTIONS
- -b | --reverb :: Turns on an 8 point reverb engine that adds depth to
  the final mix.

- -c config-file | --config config-file :: Uses the configuration file
  stated by /config-file instead of /etc/wildmidi/wildmidi.cfg/

- -d audiodev | --device=audiodev :: Send the audio to /audiodev/*
  instead of the default. ALSA defaults to the system "default" while
  OSS defaults to "/dev/dsp". Other environments do not support this
  option.*

- -h | --help :: Displays command line options.

- -f | --frequency :: Use frequency F Hz for playback (MUS).

- -g | --convert :: Convert XMI: 0 - No Conversion (default) 1 - MT32 to
  GM 2 - MT32 to GS

- -l | --log_vol :: Some MIDI files have been recorded on hardware that
  uses a volume curve, making them sound really badly mixed on other
  MIDI devices. Use this option to use volume curves.

- -m volume-level | --mastervol=volume-level :: Set the overall volume
  level to /volume-level/*. The minimum is 0 and the maximum is 127,
  with the default being 100.*

- -o wav-file | --wavout=wav-file :: Records the audio in wav format to
  /wav-file/*.*

- -r sndrate | --rate=sndrate :: Set the audio output rate to
  /sndrate/*. The default rate is 32072.*

- -n | --roundtempo :: Round tempo to nearest whole number.

- -s | --skipsilentstart :: Skips any silence at the start of playback.

- -v | --version :: Display version and copyright information.

- -x | --tomidi :: Convert a MUS or an XMI file to midi and save to
  file.

* TEST OPTIONS
These options are not designed for general use. Instead these options
are designed to make it easier to listen to specific sound samples.

Note: These options are not displayed by *-h | --help*

- -k N | --test_bank=N :: Set the test bank to /N/*. Range is 0 to 127.*

- -p N | --test_patch=N :: Set the test patch to /N/*. Range is 0
  to 127.*

- -t | --test_midi :: Plays the built in test midi which plays all 127
  notes.

* USER INTERFACE
The player accepts limited user input that allows some interaction while
playing midi files.

- Turns the master volume up.

- Turns the master volume down.

5. Turns enhanced resampling on and off.

12. Turns volume curves on and off.

18. Turns the final mix reverb on and off.

14. Play the next midi on the command line.

13. Save the currently playing file to a midi file. NOTE: This saves to
    the current directory.

16. Pause the playback. Note: since the audio is buffered it will stop
    when the audio buffer is empty.

- . :: Seek forward 1 second. Note: Clears active midi events and will
  only play midi events from after the new position.

- , :: Seek backwards 1 second. Note: Clears active midi events and will
  only play midi events from after the new position.

17. Quit wildmidi.

* SEE ALSO
*wildmidi.cfg*(5)

* AUTHOR
Chris Ison <chrisisonwildcode@gmail.com> Bret Curtis <psi29a@gmail.com>

* COPYRIGHT
Copyright (C) WildMidi Developers 2001-2016

This file is part of WildMIDI.

WildMIDI is free software: you can redistribute and/or modify the player
under the terms of the GNU General Public License and you can
redistribute and/or modify the library under the terms of the GNU Lesser
General Public License as published by the Free Software Foundation,
either version 3 of the licenses, or(at your option) any later version.

WildMIDI is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License and
the GNU Lesser General Public License for more details.

You should have received a copy of the GNU General Public License and
the GNU Lesser General Public License along with WildMIDI. If not, see
<http://www.gnu.org/licenses/>.

This manpage is licensed under the Creative Commons AttributionShare
Alike 3.0 Unported License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/3.0/ or send a letter to
Creative Commons, 171 Second Street, Suite 300, San Francisco,
California, 94105, USA.
