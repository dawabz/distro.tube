#+TITLE: Man1 - preparetips5.1
#+DESCRIPTION: Linux manpage for preparetips5.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
*preparetips5*

* NAME
preparetips5 - extract text from tips file

* SYNOPSIS
*preparetips5*

* DESCRIPTION
*preparetips5* is a script to extract the text from a tips file. It
outputs the text so *xgettext* can add the tips to a PO file. PO files
provide a human-readable string format used for translations.

*preparetips5* looks for /data/tips/ as tips file.

* SEE ALSO
*kf5options*(7)

* BUGS
Please use *KDEs bugtracker*[1] to report bugs, do not mail the author
directly.

* AUTHOR
*Matthias Kiefer* <matthias.kiefer@gmx.de>\\

#+begin_quote
  Author.
#+end_quote

* NOTES
-  1. :: KDE's bugtracker

  https://bugs.kde.org
