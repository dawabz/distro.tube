#+TITLE: Man1 - memdiskfind.1
#+DESCRIPTION: Linux manpage for memdiskfind.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
memdiskfind --- Simple utility to find a resident *memdisk* instance.

* SYNOPSIS
*memdiskfind*

* DESCRIPTION
The *memdiskfind* utility searches memory for a *memdisk* instance, and,
if found, outputs the parameters needed to use the hram driver in Linux
to map it.
