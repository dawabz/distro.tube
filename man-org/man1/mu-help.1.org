#+TITLE: Man1 - mu-help.1
#+DESCRIPTION: Linux manpage for mu-help.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*mu help* is a *mu* command that gives help information about mu
commands.

* SYNOPSIS
*mu help <command>*

* DESCRIPTION
*mu help* provides help information about mu commands.

* BUGS
Please report bugs if you find them: *https://github.com/djcb/mu/issues*

* AUTHOR
Dirk-Jan C. Binnema <djcb@djcbsoftware.nl>

* SEE ALSO
*mu-index*(1), *mu-find*(1), *mu-cfind*(1), *mu-mkdir*(1), *mu-view*(1),
*mu-extract*(1), *mu-easy*(1), *mu-bookmarks*(5)
