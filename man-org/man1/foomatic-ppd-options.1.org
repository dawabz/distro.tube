#+TITLE: Man1 - foomatic-ppd-options.1
#+DESCRIPTION: Linux manpage for foomatic-ppd-options.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
foomatic-ppd-options - show the PPD options

* SYNOPSIS
*foomatic-ppd-options [-d=debuglevel] [ ppdfiles ]*

* DESCRIPTION
The *foomatic-ppd-options* program reads the specified PPD files or
stdin and decodes the PPD file to produce a list of options. This is
really just a text based version of other print configuration tools.

The output format consists of a set of lines, each starting with a
keyword, followed by a set of options and the default value. For
example:

#+begin_quote
  #+begin_example

    makemodel = Tektronix Phaser 350 Foomatic/Postscript (recommended)
    name=PageSize; default=Letter; options=Letter (US Letter), \
        A4 (A4), 11x17 (11x17), A3 (A3), A5 (A5), B5 (B5 \(JIS\)),...
    name=PageRegion; default=Letter; options=Letter (US Letter), A4 (A4), ...
  #+end_example
#+end_quote

Each option starts with the *name* tag, followed by a default value (if
any), followed by the set of options and a comment describing them.
Punctuation in the comment is escaped.

** Options
- *-d*/debuglevel/ :: Set the debug level. 0 disables debug messages.

- *[ files ]* :: The list of PPD files to read for options. A '-' causes
  STDIN to be read.

* EXIT STATUS
*foomatic-ppd-options* exits with a 0 status if successful, and nonzero
if an error. An error message is also printed on standard error
(STDERR).

* AUTHOR
Patrick Powell <papowell at astart.com>

* BUGS
Probably. But it has been tested.
