#+TITLE: Man1 - cargo-clean.1
#+DESCRIPTION: Linux manpage for cargo-clean.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cargo-clean - Remove generated artifacts

* SYNOPSIS
*cargo clean* [/options/]

* DESCRIPTION
Remove artifacts from the target directory that Cargo has generated in
the past.

With no options, *cargo clean* will delete the entire target directory.

* OPTIONS
** Package Selection
When no packages are selected, all packages and all dependencies in the
workspace are cleaned.

*-p* /spec/..., *--package* /spec/...

#+begin_quote
  Clean only the specified packages. This flag may be specified multiple
  times. See *cargo-pkgid*(1) for the SPEC format.
#+end_quote

** Clean Options
*--doc*

#+begin_quote
  This option will cause *cargo clean* to remove only the *doc*
  directory in the target directory.
#+end_quote

*--release*

#+begin_quote
  Remove all artifacts in the *release* directory.
#+end_quote

*--profile* /name/

#+begin_quote
  Remove all artifacts in the directory with the given profile name.
#+end_quote

*--target-dir* /directory/

#+begin_quote
  Directory for all generated artifacts and intermediate files. May also
  be specified with the *CARGO_TARGET_DIR* environment variable, or the
  *build.target-dir* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>. Defaults to
  *target* in the root of the workspace.
#+end_quote

*--target* /triple/

#+begin_quote
  Clean for the given architecture. The default is the host
  architecture. The general format of the triple is
  *<arch><sub>-<vendor>-<sys>-<abi>*. Run *rustc --print target-list*
  for a list of supported targets.

  This may also be specified with the *build.target* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.

  Note that specifying this flag makes Cargo run in a different mode
  where the target artifacts are placed in a separate directory. See the
  /build cache/ <https://doc.rust-lang.org/cargo/guide/build-cache.html>
  documentation for more details.
#+end_quote

** Display Options
*-v*, *--verbose*

#+begin_quote
  Use verbose output. May be specified twice for "very verbose" output
  which includes extra output such as dependency warnings and build
  script output. May also be specified with the *term.verbose* /config
  value/ <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

*-q*, *--quiet*

#+begin_quote
  No output printed to stdout.
#+end_quote

*--color* /when/

#+begin_quote
  Control when colored output is used. Valid values:

  #+begin_quote
    ·*auto* (default): Automatically detect if color support is
    available on the terminal.
  #+end_quote

  #+begin_quote
    ·*always*: Always display colors.
  #+end_quote

  #+begin_quote
    ·*never*: Never display colors.
  #+end_quote

  May also be specified with the *term.color* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

** Manifest Options
*--manifest-path* /path/

#+begin_quote
  Path to the *Cargo.toml* file. By default, Cargo searches for the
  *Cargo.toml* file in the current directory or any parent directory.
#+end_quote

*--frozen*, *--locked*

#+begin_quote
  Either of these flags requires that the *Cargo.lock* file is
  up-to-date. If the lock file is missing, or it needs to be updated,
  Cargo will exit with an error. The *--frozen* flag also prevents Cargo
  from attempting to access the network to determine if it is
  out-of-date.

  These may be used in environments where you want to assert that the
  *Cargo.lock* file is up-to-date (such as a CI build) or want to avoid
  network access.
#+end_quote

*--offline*

#+begin_quote
  Prevents Cargo from accessing the network for any reason. Without this
  flag, Cargo will stop with an error if it needs to access the network
  and the network is not available. With this flag, Cargo will attempt
  to proceed without the network if possible.

  Beware that this may result in different dependency resolution than
  online mode. Cargo will restrict itself to crates that are downloaded
  locally, even if there might be a newer version as indicated in the
  local copy of the index. See the *cargo-fetch*(1) command to download
  dependencies before going offline.

  May also be specified with the *net.offline* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

** Common Options
*+*/toolchain/

#+begin_quote
  If Cargo has been installed with rustup, and the first argument to
  *cargo* begins with *+*, it will be interpreted as a rustup toolchain
  name (such as *+stable* or *+nightly*). See the /rustup documentation/
  <https://rust-lang.github.io/rustup/overrides.html> for more
  information about how toolchain overrides work.
#+end_quote

*-h*, *--help*

#+begin_quote
  Prints help information.
#+end_quote

*-Z* /flag/

#+begin_quote
  Unstable (nightly-only) flags to Cargo. Run *cargo -Z help* for
  details.
#+end_quote

* ENVIRONMENT
See /the reference/
<https://doc.rust-lang.org/cargo/reference/environment-variables.html>
for details on environment variables that Cargo reads.

* EXIT STATUS

#+begin_quote
  ·*0*: Cargo succeeded.
#+end_quote

#+begin_quote
  ·*101*: Cargo failed to complete.
#+end_quote

* EXAMPLES

#+begin_quote
  1.Remove the entire target directory:

  #+begin_quote
    #+begin_example
      cargo clean
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  2.Remove only the release artifacts:

  #+begin_quote
    #+begin_example
      cargo clean --release
    #+end_example
  #+end_quote
#+end_quote

* SEE ALSO
*cargo*(1), *cargo-build*(1)
