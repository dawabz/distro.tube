#+TITLE: Man1 - git-citool.1
#+DESCRIPTION: Linux manpage for git-citool.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
git-citool - Graphical alternative to git-commit

* SYNOPSIS
#+begin_example
  git citool
#+end_example

* DESCRIPTION
A Tcl/Tk based graphical interface to review modified files, stage them
into the index, enter a commit message and record the new commit onto
the current branch. This interface is an alternative to the less
interactive /git commit/ program.

/git citool/ is actually a standard alias for *git gui citool*. See
*git-gui*(1) for more details.

* GIT
Part of the *git*(1) suite
