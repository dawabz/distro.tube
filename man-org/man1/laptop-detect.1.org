#+TITLE: Man1 - laptop-detect.1
#+DESCRIPTION: Linux manpage for laptop-detect.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
laptop-detect - attempt to detect a laptop

* SYNOPSIS
laptop-detect
[/-h/|/--help/|/-v/|/--verbose/|/-V/|/--version/|/-D/|/--debug/]

* DESCRIPTION
Laptop-detect attempts to determine whether it is being run on a laptop
or a desktop and appraises its caller of this.

* OPTIONS
- *-h* *--help* :: Output help information and exit.

- *-v* *--verbose* :: be verbose (messages go to STDOUT)

- *-V* *--version* :: Output version information and exit.

- *-D* *--debug* :: Debug mode.

* EXIT STATUS
\\

#+begin_example
  0	most likely running on a laptop
  1	most likely NOT running on a laptop
  2	usage error (arguments supplied)
#+end_example

* FILES
//proc/acpi/battery/\\
//proc/pmu/info/\\
//dev/mem/\\
//sys/devices/virtual/dmi/id/chassis_type/\\
//sys/class/power_supply/*/type/\\
//proc/apm/

* SEE ALSO
dmidecode(8)
