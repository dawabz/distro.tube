#+TITLE: Man1 - xkbcli-interactive-evdev.1
#+DESCRIPTION: Linux manpage for xkbcli-interactive-evdev.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
is a commandline tool to interactively debug XKB keymaps by listening to

evdev devices.

requires permission to open the evdev device nodes. This usually
requires being the

user or belonging to the

group.

Press the

key to exit.

This is a debugging tool, its behavior or output is not guaranteed to be
stable.

Print help and exit

The XKB ruleset

The XKB model

The XKB layout

The XKB layout variant

The XKB options

Specify a keymap path. This option is mutually exclusive with the RMLVO
options.

Report changes to the keyboard state

Enable Compose functionality

Set the consumed modifiers mode (default: xkb)

Don't add an offset of 8 when converting an evdev keycode to an XKB
keycode. You probably don't want this option.
