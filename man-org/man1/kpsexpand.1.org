#+TITLE: Manpages - kpsexpand.1
#+DESCRIPTION: Linux manpage for kpsexpand.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about kpsexpand.1 is found in manpage for: [[../man1/kpsetool.1][man1/kpsetool.1]]