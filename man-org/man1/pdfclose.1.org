#+TITLE: Manpages - pdfclose.1
#+DESCRIPTION: Linux manpage for pdfclose.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about pdfclose.1 is found in manpage for: [[../man1/pdfopen.1][man1/pdfopen.1]]