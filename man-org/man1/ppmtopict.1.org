#+TITLE: Man1 - ppmtopict.1
#+DESCRIPTION: Linux manpage for ppmtopict.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
ppmtopict - convert a PPM image to a Macintosh PICT file

* SYNOPSIS
*ppmtopict*

[/ppmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*ppmtopict* reads a PPM image as input and produces a Macintosh PICT
file as output.

The generated file is only the data fork of a picture. You will need a
program such as /mcvert/ to generate a Macbinary or a BinHex file that
contains the necessary information to identify the file as a PICT file
to MacOS.

Even though PICT can have 2 and 4 bits per pixel, *ppmtopict* always
generates an 8 bits per pixel file.

* LIMITATIONS
The picture size field is correct only if the output is to a file since
writing into this field requires seeking backwards on a file. However
the PICT documentation seems to suggest that this field is not critical
anyway since it is only the lower 16 bits of the picture size.

* SEE ALSO
*picttoppm*(1) , *ppm*(5) , *mcvert*

* AUTHOR
Copyright (C) 1990 by Ken Yap </ken@cs.rocester.edu/>.
