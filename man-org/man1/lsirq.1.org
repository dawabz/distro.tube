#+TITLE: Man1 - lsirq.1
#+DESCRIPTION: Linux manpage for lsirq.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lsirq - utility to display kernel interrupt information

* SYNOPSIS
*lsirq* [options]

* DESCRIPTION
Display kernel interrupt counter information.

The default output is subject to change. So whenever possible, you
should avoid using default outputs in your scripts. Always explicitly
define expected columns by using *--output*.

* OPTIONS
*-n*, *--noheadings*

#+begin_quote
  Don't print headings.
#+end_quote

*-o*, *--output* /list/

#+begin_quote
  Specify which output columns to print. Use *--help* to get a list of
  all supported columns. The default list of columns may be extended if
  list is specified in the format /+list/.
#+end_quote

*-s*, *--sort* /column/

#+begin_quote
  Specify sort criteria by column name. See *--help* output to get
  column names.
#+end_quote

*-J*, *--json*

#+begin_quote
  Use JSON output format.
#+end_quote

*-P*, *--pairs*

#+begin_quote
  Produce output in the form of key="value" pairs. All potentially
  unsafe characters are hex-escaped (\x<code>).
#+end_quote

*-S*, *--softirq*

#+begin_quote
  Show softirqs information.
#+end_quote

*-V*, *--version*

#+begin_quote
  Display version information and exit.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help text and exit.
#+end_quote

* AUTHORS
* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *lsirq* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
