#+TITLE: Man1 - lv2info.1
#+DESCRIPTION: Linux manpage for lv2info.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*lv2info - print information about an installed LV2 plugin*

* SYNOPSIS
*lv2info PLUGIN_URI*

* OPTIONS
- *-p FILE* :: Write Turtle description of plugin to FILE

- *-m FILE* :: Add record of plugin to manifest FILE

- *--help* :: Display help and exit

- *--version* :: Display version information and exit

* SEE ALSO
*lilv(3),* *lv2ls(1)*

* AUTHOR
lv2info was written by David Robillard <d@drobilla.net>

This manual page was written by Jaromír Mikes <mira.mikes@seznam.cz> and
David Robillard <d@drobilla.net>
