#+TITLE: Man1 - pbmto10x.1
#+DESCRIPTION: Linux manpage for pbmto10x.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmto10x - convert a PBM image into Gemini 10X printer graphics

* SYNOPSIS
*pbmto10x* [*-h*] [/pbmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmto10x* reads a PBM image as input and produces a file of Gemini 10X
printer graphics as output. The 10x's printer codes are alleged to be
similar to the Epson codes.

Note that there is no 10xtopbm tool - this transformation is one way.

* OPTIONS
The resolution is normally 60H by 72V. If you specify the *-h* option,
resolution is 120H by 144V. You may find it useful to rotate landscape
images before printing.

* SEE ALSO
*pbm*(5)

* AUTHOR
Copyright (C) 1990 by Ken Yap
