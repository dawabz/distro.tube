#+TITLE: Man1 - xfce4-notifyd-config.1
#+DESCRIPTION: Linux manpage for xfce4-notifyd-config.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xfce4-notifyd-config - configuration GUI for xfce4-notifyd

* SYNOPSIS
- *xfce4-notifyd-config* [*-?*] [*-V*] [*-s* /SOCKET_ID/]
  [*--display*=/DISPLAY/] :: 

* DESCRIPTION
*xfce4-notifyd-config* will launch the configuration GUI for
xfce4-notifyd.

* OPTIONS
- *-?*, *--help* :: Show help

- *-V*, *--version* :: Display version information

- *-s*, *--socket-id*=/SOCKET_ID/ :: Settings manager socket

- *--display*=/DISPLAY/ :: X display to use

* AUTHORS
*xfce4-notifyd-config* was written by Brian Tarricone
<bjt23@cornell.edu>.\\
This manual page was written by Evgeni Golov <sargentd@die-welt.net> for
the Debian Project but may be used by everyone under the terms of
GPL-2+.
