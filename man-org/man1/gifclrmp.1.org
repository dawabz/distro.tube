#+TITLE: Man1 - gifclrmp.1
#+DESCRIPTION: Linux manpage for gifclrmp.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gifclrmp - extract colormaps from GIF images

* SYNOPSIS
*gifclrmp* [-v] [-s] [-l /mapfile/] [-t /trans/] [-g /gamma/] [-i
/image/] [-h] [/gif-file/]

* DESCRIPTION
A program to modify GIF image colormaps. Any local colormap in a GIF
file can be modified at a time, or the global screen one.

* OPTIONS
-v

#+begin_quote
  Verbose mode (show progress). Enables printout of running scan lines.
#+end_quote

-s

#+begin_quote
  Select the global screen color map.
#+end_quote

-l mapfile

#+begin_quote
  Load color map from this file instead of selected color map.
#+end_quote

-t trans

#+begin_quote
  Change color index values. The change is made to both the selected
  color table and the raster bits of the selected image. A translation
  file is a list of pairs of `before and `after index values. At
  present, the `before index values must be in ascending order starting
  from 0.
#+end_quote

-g gamma

#+begin_quote
  Apply gamma correction to selected color map.
#+end_quote

-i image

#+begin_quote
  Select the color map of the numbered image.
#+end_quote

-h

#+begin_quote
  Print one command line help, similar to Usage above.
#+end_quote

If no GIF file is given, gifclip will try to read a GIF file from stdin.

* NOTES

#+begin_quote
  ·

  The default operation is to dump out the selected color map in text
  format.
#+end_quote

#+begin_quote
  ·

  The file to load/dump is simply one color map entry per line. Each
  such entry line has four integers: "ColorIndex Red Green Blue", where
  color index is in ascending order starting from 1.
#+end_quote

* AUTHOR
Gershon Elber.
