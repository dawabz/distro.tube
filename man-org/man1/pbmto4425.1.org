#+TITLE: Man1 - pbmto4425.1
#+DESCRIPTION: Linux manpage for pbmto4425.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmto4425 - Display PBM images on an AT&T 4425 terminal

* SYNOPSIS
*pbmto4425* [/pbmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmto4425* displays PBM format images on an AT&T 4425 ASCII terminal
using that terminal's mosaic graphics character set. The program should
also work with other VT100-like terminals with mosaic graphics character
sets such as the C. Itoh CIT-101, but it has not yet been tested on
terminals other than the 4425.

*Pbmto4425* puts the terminal into 132 column mode to achieve the
maximum resolution of the terminal. In this mode the terminal has a
resolution of 264 columns by 69 rows. The pixels have an aspect ratio of
1:2.6, therefore an image should be processed before being displayed in
a manner such as this:

#+begin_example
  % pamscale -xscale 2.6 pamfile \
      | pamscale -xysize 264 69 \
      | ppmtopgm \
      | pamditherbw \
      | pbmto4425
#+end_example

* SEE ALSO
*pbmtoascii*(1) , *ppmtoterm*(1) , *pbm*(5)

* AUTHOR
Copyright (C) 1993 by Robert Perlberg
