#+TITLE: Man1 - mtoolstest.1
#+DESCRIPTION: Linux manpage for mtoolstest.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* Name
mtoolstest - tests and displays the configuration

* Note of warning
This manpage has been automatically generated from mtools's texinfo
documentation, and may not be entirely accurate or complete. See the end
of this man page for details.

* Description
The =mtoolstest= command is used to tests the mtools configuration
files. To invoke it, just type =mtoolstest= without any arguments.
=Mtoolstest= reads the mtools configuration files, and prints the
cumulative configuration to =stdout=. The output can be used as a
configuration file itself (although you might want to remove redundant
clauses). You may use this program to convert old-style configuration
files into new style configuration files.

* See Also
Mtools' texinfo doc

* Viewing the texi doc
This manpage has been automatically generated from mtools's texinfo
documentation. However, this process is only approximative, and some
items, such as crossreferences, footnotes and indices are lost in this
translation process. Indeed, these items have no appropriate
representation in the manpage format. Moreover, not all information has
been translated into the manpage version. Thus I strongly advise you to
use the original texinfo doc. See the end of this manpage for
instructions how to view the texinfo doc.

- *  :: To generate a printable copy from the texinfo doc, run the
  following commands:

#+begin_example
      ./configure; make dvi; dvips mtools.dvi
#+end_example

- *  :: To generate a html copy, run:

#+begin_example
      ./configure; make html
#+end_example

A premade html can be found at
=∞http://www.gnu.org/software/mtools/manual/mtools.html∫=

- *  :: To generate an info copy (browsable using emacs' info mode),
  run:

#+begin_example
      ./configure; make info
#+end_example

The texinfo doc looks most pretty when printed or as html. Indeed, in
the info version certain examples are difficult to read due to the
quoting conventions used in info.
