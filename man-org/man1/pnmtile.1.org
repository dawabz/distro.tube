#+TITLE: Man1 - pnmtile.1
#+DESCRIPTION: Linux manpage for pnmtile.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pnmtile - replicate an image to fill a specified region

* SYNOPSIS
*pnmtile* /width/ /height/ [/pnmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pnmtile* reads a PNM image as input. It replicates it to fill an area
of the specified dimensions and produces an image in the same format as
output.

You can do pretty much the reverse with *pamdice*.

You can explicitly concatenate an image to itself (or anything else)
with *pnmcat*.

If you're trying to tile multiple images into a superimage, see
*pamundice* or (for a thumbnail sheet) *pnmindex*.

* SEE ALSO
*pamdice*(1) , *pnmcat*(1) , *pamdice*(1) , *pnmindex*(1) , *pampop9*(1)
, *pnm*(5)

* AUTHOR
Copyright (C) 1989 by Jef Poskanzer.
