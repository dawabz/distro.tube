#+TITLE: Man1 - pipewire.1
#+DESCRIPTION: Linux manpage for pipewire.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pipewire - The PipeWire media server

* SYNOPSIS
#+begin_example
  pipewire [options]
#+end_example

* DESCRIPTION
PipeWire is a service that facilitates sharing of multimedia content
between devices and applications.

The *pipewire* daemon reads a config file that is further documented in
*pipewire.conf(5)* manual page.

* OPTIONS

#+begin_quote
  - *-h | --help* :: Show help.

  - *-v | --verbose* :: Increase the verbosity by one level. This option
    may be specified multiple times.
#+end_quote

#+begin_quote
  - *--version* :: Show version information.
#+end_quote

#+begin_quote
  - *-c | --config=FILE* :: Load the given config file (Default:
    pipewire.conf).
#+end_quote

* AUTHORS
The PipeWire Developers
</https://gitlab.freedesktop.org/pipewire/pipewire/issues/>; PipeWire is
available from /https://pipewire.org/

* SEE ALSO
*pw-mon(1)*, *pw-cat(1)*, *pw-cli(1)*,
