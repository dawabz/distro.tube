#+TITLE: Man1 - pdfetex.1
#+DESCRIPTION: Linux manpage for pdfetex.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pdfetex - PDF output from e-TeX

* SYNOPSIS
*pdfetex* [/options/] [/*&*format/] [/file/|/*\*commands/]

* DESCRIPTION
pdfeTeX is another name for pdfTeX; they behave identically. See
pdftex(1) for all information.

* SEE ALSO
*pdftex*(1), http://pdftex.org, http://tug.org/web2c.
