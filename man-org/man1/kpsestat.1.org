#+TITLE: Man1 - kpsestat.1
#+DESCRIPTION: Linux manpage for kpsestat.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
kpsestat - compute octal mode from mode of existing file

* SYNOPSIS
*kpsestat* /mode/ /file/

* DESCRIPTION
*kpsestat* prints the octal permission of /file/ modified according to
/mode/ on standard output. The /mode/ parameter accepts a subset of the
symbolic permissions accepted by *chmod*(1). Use *=* as the mode to
obtain the unchanged permissions.

* OPTIONS
*kpsestat* accepts the following options:

- *--help* :: Print help message and exit.

- *--version* :: Print version information and exit.

* SEE ALSO
*chmod*(1).
