#+TITLE: Man1 - ts_verify.1
#+DESCRIPTION: Linux manpage for ts_verify.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ts_verify - A basic test routine for tslib's API.

* SYNOPSIS
*ts_print_mt [OPTION]*

* DESCRIPTION
ts_verify runs through tslib's API and prints test results.

*-i, --idev*

#+begin_quote
  Explicitly choose the original input event device for tslib to use.
  Default: the environment variable *TSLIB_TSDEVICE*'s value.

  *-h, --help*

  #+begin_quote
    Print usage help and exit.
  #+end_quote
#+end_quote

* SEE ALSO
ts.conf (5), ts_test (1), ts_test_mt (1), ts_calibrate (1)
