#+TITLE: Man1 - mesg.1
#+DESCRIPTION: Linux manpage for mesg.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
mesg - display (or do not display) messages from other users

* SYNOPSIS
*mesg* [/option/] [*n*|*y*]

* DESCRIPTION
The *mesg* utility is invoked by a user to control write access others
have to the terminal device associated with standard error output. If
write access is allowed, then programs such as *talk*(1) and *write*(1)
may display messages on the terminal.

Traditionally, write access is allowed by default. However, as users
become more conscious of various security risks, there is a trend to
remove write access by default, at least for the primary login shell. To
make sure your ttys are set the way you want them to be set, *mesg*
should be executed in your login scripts.

The *mesg* utility silently exits with error status 2 if not executed on
terminal. In this case execute *mesg* is pointless. The command line
option *--verbose* forces mesg to print a warning in this situation.
This behaviour has been introduced in version 2.33.

* ARGUMENTS
*n*

#+begin_quote
  Disallow messages.
#+end_quote

*y*

#+begin_quote
  Allow messages to be displayed.
#+end_quote

If no arguments are given, *mesg* shows the current message status on
standard error output.

* OPTIONS
*-v*, *--verbose*

#+begin_quote
  Explain what is being done.
#+end_quote

*-V*, *--version*

#+begin_quote
  Display version information and exit.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help text and exit.
#+end_quote

* EXIT STATUS
The *mesg* utility exits with one of the following values:

*0*

#+begin_quote
  Messages are allowed.
#+end_quote

*1*

#+begin_quote
  Messages are not allowed.
#+end_quote

*>1*

#+begin_quote
  An error has occurred.
#+end_quote

* FILES
//dev/[pt]ty[pq]?/

* HISTORY
A *mesg* command appeared in Version 6 AT&T UNIX.

* SEE ALSO
*login*(1), *talk*(1), *write*(1), *wall*(1), *xterm*(1)

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *mesg* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
