#+TITLE: Man1 - spottopgm.1
#+DESCRIPTION: Linux manpage for spottopgm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
spottopgm - convert SPOT satellite images to a PGM image

* SYNTAX
*spottopgm* [*-1*|*-2*|*-3*] [/Firstcol/ /Firstline/ /Lastcol/
/Lastline/] /inputfile/

* OPTIONS
- *-1* :: 

- *-2* :: 

- *-3* :: Extract the given color from the SPOT image. The colors are
  infrared, visible light, and ultraviolet, although I don't know which
  corresponds to which number. If the image is in color, *spottopgm*
  announces this on Standard Error. The default color is 1.

* PARAMETERS
- /Firstcol Firstline Lastcol Lastline/ :: Extract the specified
  rectangle from the SPOT image. Most SPOT images are 3000 lines long
  and 3000 or more columns wide. Unfortunately, the SPOT format only
  gives the width and not the length. The width is printed on standard
  error. The default rectangle is the width of the input image by 3000
  lines.

* DESCRIPTION
This program is part of *Netpbm*(1)

*spottopgm* converts the named *inputfile* to PGM format, defaulting to
the first color and the whole SPOT image unless you specify otherwise
with the options.

* LIMITATIONS
*spottopgm* doesn't determine the length of the input file; this would
involve two passes over the input file. It defaults to 3000 lines
instead.

*spottopgm* could extract a three-color image (as a PPM), but I didn't
feel like making the program more complicated than it is now. Besides,
there is no one-to-one correspondence between red, green, blue and
infrared, visible and ultraviolet.

I've had only a limited number of SPOT images to play with, and
therefore wouldn't guarantee that this will work on any other images.

* AUTHOR
Warren Toomey /wkt@csadfa.cs.adfa.oz.au/

* SEE ALSO
*pgm*(5)
