#+TITLE: Man1 - tiffsplit.1
#+DESCRIPTION: Linux manpage for tiffsplit.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tiffsplit - split a multi-image

into single-image

files

* SYNOPSIS
*tiffsplit* /src.tif/ [ /prefix/ ]

* DESCRIPTION
/tiffsplit/ takes a multi-directory (page)

file and creates one or more single-directory (page)

files from it. The output files are given names created by concatenating
a prefix, a lexically ordered suffix in the range [/aaa/-/zzz/], the
suffix /.tif/ (e.g. /xaaa.tif/, /xaab.tif/, /.../, /xzzz.tif/). If a
prefix is not specified on the command line, the default prefix of /x/
is used.

* OPTIONS
None.

* EXIT STATUS
/tiffsplit/ exits with one of the following values:

- *0* :: Success

- *1* :: An error occurred either reading the input or writing results.

* BUGS
Only a select set of ``known tags'' is copied when splitting.

* SEE ALSO
*tiffcp*(1), *tiffinfo*(1), *libtiff*(3TIFF)

Libtiff library home page: *http://www.simplesystems.org/libtiff/*
