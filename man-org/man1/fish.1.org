#+TITLE: Man1 - fish.1
#+DESCRIPTION: Linux manpage for fish.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fish - the friendly interactive shell

* SYNOPSIS

#+begin_quote

  #+begin_quote
    #+begin_example
      fish [OPTIONS] [-c command] [FILE] [ARGUMENTS...]
    #+end_example
  #+end_quote
#+end_quote

* DESCRIPTION
fish is a command-line shell written mainly with interactive use in
mind. This page briefly describes the options for invoking fish. The
full manual is available in HTML by using the help command from inside
fish, and in the /fish-doc(1)/ man page. The tutorial is available as
HTML via *help tutorial* or in /fish-tutorial(1)/.

The following options are available:

#+begin_quote

  - *-c* or *--command=COMMANDS* evaluate the specified commands instead
    of reading from the commandline, passing any additional positional
    arguments via $argv. Note that, unlike other shells, the first
    argument is /not/ the name of the program (*$0*), but simply the
    first normal argument.

  - *-C* or *--init-command=COMMANDS* evaluate the specified commands
    after reading the configuration, before running the command
    specified by *-c* or reading interactive input

  - *-d* or *--debug=DEBUG_CATEGORIES* enable debug output and specify a
    pattern for matching debug categories. See /Debugging/ below for
    details.

  - *-o* or *--debug-output=DEBUG_FILE* specify a file path to receive
    the debug output, including categories and *fish_trace*. The default
    is stderr.

  - *-i* or *--interactive* specify that fish is to run in interactive
    mode

  - *-l* or *--login* specify that fish is to run as a login shell

  - *-n* or *--no-execute* do not execute any commands, only perform
    syntax checking

  - *-p* or *--profile=PROFILE_FILE* when fish exits, output timing
    information on all executed commands to the specified file. This
    excludes time spent starting up and reading the configuration.

  - *--profile-startup=PROFILE_FILE* will write timing information for
    fish's startup to the specified file. This is useful to profile your
    configuration.

  - *-P* or *--private* enables private mode, so fish will not access
    old or store new history.

  - *--print-rusage-self* when fish exits, output stats from getrusage

  - *--print-debug-categories* outputs the list of debug categories, and
    then exits.

  - *-v* or *--version* display version and exit

  - *-f* or *--features=FEATURES* enables one or more feature flags
    (separated by a comma). These are how fish stages changes that might
    break scripts.
#+end_quote

The fish exit status is generally the exit status of the last foreground
command.

* DEBUGGING
While fish provides extensive support for debugging fish scripts, it is
also possible to debug and instrument its internals. Debugging can be
enabled by passing the *--debug* option. For example, the following
command turns on debugging for background IO thread events, in addition
to the default categories, i.e. /debug/, /error/, /warning/, and
/warning-path/:

#+begin_quote

  #+begin_quote
    #+begin_example
      > fish --debug=iothread
    #+end_example
  #+end_quote
#+end_quote

Available categories are listed by *fish --print-debug-categories*. The
*--debug* option accepts a comma-separated list of categories, and
supports glob syntax. The following command turns on debugging for
/complete/, /history/, /history-file/, and /profile-history/, as well as
the default categories:

#+begin_quote

  #+begin_quote
    #+begin_example
      > fish --debug='complete,*history*'
    #+end_example
  #+end_quote
#+end_quote

Debug messages output to stderr by default. Note that if *fish_trace* is
set, execution tracing also outputs to stderr by default. You can output
to a file using the *--debug-output* option:

#+begin_quote

  #+begin_quote
    #+begin_example
      > fish --debug='complete,*history*' --debug-output=/tmp/fish.log --init-command='set fish_trace on'
    #+end_example
  #+end_quote
#+end_quote

These options can also be changed via the $FISH_DEBUG and
$FISH_DEBUG_OUTPUT variables. The categories enabled via *--debug* are
/added/ to the ones enabled by $FISH_DEBUG, so they can be disabled by
prefixing them with *-* (*reader-*,-ast** enables reader debugging and
disables ast debugging).

The file given in *--debug-output* takes precedence over the file in
$FISH_DEBUG_OUTPUT.

* COPYRIGHT
2021, fish-shell developers
