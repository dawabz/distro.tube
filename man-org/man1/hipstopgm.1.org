#+TITLE: Man1 - hipstopgm.1
#+DESCRIPTION: Linux manpage for hipstopgm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
hipstopgm - convert a HIPS file into a PGM image

* SYNOPSIS
*hipstopgm* [/hipsfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*hipstopgm* reads a HIPS file as input and produces a PGM image as
output.

If the HIPS file contains more than one frame in sequence, *hipstopgm*
will concatenate all the frames vertically.

HIPS is a format developed at the Human Information Processing
Laboratory, NYU.

* SEE ALSO
*pgm*(5)

* AUTHOR
Copyright (C) 1989 by Jef Poskanzer.
