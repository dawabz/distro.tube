#+TITLE: Man1 - serdi.1
#+DESCRIPTION: Linux manpage for serdi.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*serdi - Read and write RDF syntax*

* SYNOPSIS
serdi [/OPTION/]... /INPUT/ /BASE_URI/

* OPTIONS
- *-a* :: Write ASCII output if possible.

- *-b* :: Fast bulk output for large serialisations.

- *-c* */PREFIX/* :: Chop /PREFIX/ from matching blank node IDs.

- *-e* :: Eat input one character at a time, rather than a page at a
  time which is the default. This is useful when reading from a pipe
  since output will be generated immediately as input arrives, rather
  than waiting until an entire page of input has arrived. With this
  option serdi uses one page less memory, but will likely be
  significantly slower.

- *-f* :: Keep full URIs in input (don't qualify).

- *-h* :: Print the command line options.

- *-i* */SYNTAX/* :: Read input as /SYNTAX/. Valid values
  (case-insensitive): “turtle”, “ntriples”, “trig”, “nquads”.

- *-l* :: Lax (non-strict) parsing.

- *-o* */SYNTAX/* :: Write output as /SYNTAX/. Valid values
  (case-insensitive): “turtle”, “ntriples”, “trig”, “nquads”.

- *-p* */PREFIX/* :: Add /PREFIX/ to blank node IDs.

- *-q* :: Suppress all output except data.

- *-r* */ROOT_URI/* :: Keep relative URIs within /ROOT_URI/.

- *-s* */INPUT/* :: Parse /INPUT/ as a string (terminates options).

- *-v* :: Display version information and exit.

* AUTHOR
Serdi was written by David Robillard <d@drobilla.net>

* COPYRIGHT
Copyright © 2011-2020 David Robillard.\\
License: <http://www.opensource.org/licenses/isc>\\
This is free software; you are free to change and redistribute it.\\
There is NO WARRANTY, to the extent permitted by law.

* SEE ALSO
<http://drobilla.net/software/serd>
