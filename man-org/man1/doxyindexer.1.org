#+TITLE: Man1 - doxyindexer.1
#+DESCRIPTION: Linux manpage for doxyindexer.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
doxyindexer - creates a search index from raw search data

* SYNOPSIS
*doxyindexer* [/-o output_dir/] /searchdata.xml /[/searchdata2.xml/...]

* DESCRIPTION
Generates a search index called *doxysearch.db* from one or more search
data files produced by doxygen. Use doxysearch.cgi as a CGI program to
search the data indexed by doxyindexer.

* OPTIONS
- *-o* <output_dir> :: The directory where to write the doxysearch.db
  to. If omitted the current directory is used.

* SEE ALSO
doxygen(1), doxysearch(1), doxywizard(1).
