#+TITLE: Man1 - lldb-tblgen.1
#+DESCRIPTION: Linux manpage for lldb-tblgen.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lldb-tblgen - Description to C++ Code for LLDB

* SYNOPSIS
*lldb-tblgen* [/options/] [/filename/]

* DESCRIPTION
*lldb-tblgen* is a program that translates compiler-related target
description (*.td*) files into C++ code and other output formats. Most
users of LLVM will not need to use this program. It is used only for
writing parts of the compiler.

Please see tblgen - Description to C++ Code for a description of the
/filename/ argument and options, including the options common to all
**-tblgen* programs.

* AUTHOR
Maintained by the LLVM Team (https://llvm.org/).

* COPYRIGHT
2003-2021, LLVM Project
