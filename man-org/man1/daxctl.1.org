#+TITLE: Man1 - daxctl.1
#+DESCRIPTION: Linux manpage for daxctl.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
daxctl - Provides enumeration and provisioning commands for the Linux
kernel Device-DAX facility

* SYNOPSIS
#+begin_example
  daxctl [--version] [--help] COMMAND [ARGS]
#+end_example

\\

* OPTIONS
-v, --version

#+begin_quote
  Display daxctl version.
#+end_quote

-h, --help

#+begin_quote
  Run daxctl help command.
#+end_quote

* DESCRIPTION
The daxctl utility provides enumeration and provisioning commands for
the Linux kernel Device-DAX facility. This facility enables DAX mappings
of performance / feature differentiated memory without need of a
filesystem.

* COPYRIGHT
Copyright © 2016 - 2020, Intel Corporation. License GPLv2: GNU GPL
version 2 <http://gnu.org/licenses/gpl.html>. This is free software: you
are free to change and redistribute it. There is NO WARRANTY, to the
extent permitted by law.

* SEE ALSO
ndctl-create-namespace(1), ndctl-list(1)
