#+TITLE: Manpages - odvitype.1
#+DESCRIPTION: Linux manpage for odvitype.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about odvitype.1 is found in manpage for: [[../man1/dvitype.1][man1/dvitype.1]]