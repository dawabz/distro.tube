#+TITLE: Man1 - xkbwatch.1
#+DESCRIPTION: Linux manpage for xkbwatch.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xkbwatch - XKB extension user utility

* DESCRIPTION
This program reports changes in the fundamental components of the XKB
keyboard state plus the effective compatibility state.

* OPTIONS
/Xkbwatch/ accepts all of the standard X Toolkit command line options
along with the additional options listed below:

- *-version* :: This option indicates that the program version should be
  printed, after which the program exits.

* SEE ALSO
X(7)
