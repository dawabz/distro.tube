#+TITLE: Man1 - ebook-edit.1
#+DESCRIPTION: Linux manpage for ebook-edit.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ebook-edit - ebook-edit

#+begin_quote

  #+begin_quote
    #+begin_example
      ebook-edit [opts] [path_to_ebook] [name_of_file_inside_book ...]
    #+end_example
  #+end_quote
#+end_quote

Launch the calibre Edit book tool. You can optionally also specify the
names of files inside the book which will be opened for editing
automatically.

Whenever you pass arguments to *ebook-edit* that have spaces in them,
enclose the arguments in quotation marks. For example: "/some path/with
spaces"

* [OPTIONS]

#+begin_quote
  - *--detach* :: Detach from the controlling terminal, if any (Linux
    only)
#+end_quote

#+begin_quote
  - *--help, -h* :: show this help message and exit
#+end_quote

#+begin_quote
  - *--select-text* :: The text to select in the book when it is opened
    for editing
#+end_quote

#+begin_quote
  - *--version* :: show program*'*s version number and exit
#+end_quote

* AUTHOR
Kovid Goyal

* COPYRIGHT
Kovid Goyal
