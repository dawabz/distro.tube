#+TITLE: Man1 - djvuextract.1
#+DESCRIPTION: Linux manpage for djvuextract.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
djvuextract - Extract chunks from DjVu image files.

* SYNOPSIS
*djvuextract [-page=*/pagenum/*] */djvufile/*
[*/chkid/*=*/filename/*]...*

* DESCRIPTION
Program *djvuextract* extracts raw chunk data from a DjVu file
/djvufile/. These chunks can then be re-assembled into DjVu files using
program *djvumake*.

Option *-page* can be used to specify a particular page. Otherwise the
first page of the document is assumed. Each remaining argument specifies
that the raw data associated with all the chunks named /chkid/ will be
concatenated into the file named /filename/. Chunks named *BG44* and
*FG44* are handled slightly differently: the program generates legal

files instead of simply saving the raw data.

See the man page *djvumake*(1) for related information.

* CREDITS
This program was written by Léon Bottou <leonb@users.sourceforge.net>
and was then improved by Andrei Erofeev <andrew_erofeev@yahoo.com>, Bill
Riemers <docbill@sourceforge.net> and many others.

* SEE ALSO
*djvu*(1), *djvumake*(1)
