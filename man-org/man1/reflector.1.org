#+TITLE: Man1 - reflector.1
#+DESCRIPTION: Linux manpage for reflector.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
reflector - retrieve and filter the latest Pacman mirrorlist

* SYNOPSIS
=reflector [arguments]=

* DESCRIPTION
reflector is a Python script and associated Python module that can
retrieve up-to-date Arch Linux mirror data from the Mirror Status
(https://www.archlinux.org/mirrors/status/) web interface.

* ARGUMENTS
See =reflector --help=.

* EXAMPLES
Print the latest mirrorlist to STDOUT:

#+begin_example
  reflector
#+end_example

Sort the five most recently synchronized mirrors by download speed and
overwrite the local mirrorlist:

#+begin_example
  reflector --latest 5 --sort rate --save /etc/pacman.d/mirrorlist
#+end_example

Select the 200 most recently synchronized HTTP or HTTPS mirrors, sort
them by download speed, and overwrite the file /etc/pacman.d/mirrorlist:

#+begin_example
  reflector --latest 200 --protocol http,https --sort rate --save /etc/pacman.d/mirrorlist
#+end_example

Select the HTTPS mirrors synchronized within the last 12 hours and
located in either France or Germany, sort them by download speed, and
overwrite the file =/etc/pacman.d/mirrorlist= with the results:

#+begin_example
  reflector --country France,Germany --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
#+end_example

* SYSTEMD INTEGRATION
Reflector includes systemd service and timer units that can be used to
automatically update Pacman's mirrorlist. To use the timer, edit the
configuration file at =/etc/xdg/reflector/reflector.conf= and then
enable the timer with

#+begin_example
  systemctl enable reflector.timer
  systemctl start reflector.timer
#+end_example

Check that status with =systemctl list-timers=. To update the mirrorlist
immediately instead of waiting for the scheduled operation, run
=systemctl start reflector.service=.

* SEE ALSO
=pacman(8)=

* AUTHORS
Xyne.
