#+TITLE: Man1 - rumakeindex.1
#+DESCRIPTION: Linux manpage for rumakeindex.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
rumakeindex - process a LaTeX index using Russian Cyrillic characters

* SYNOPSIS
*rumakeindex* /basename/

* DESCRIPTION
*rumakeindex* is used to run *makeindex*(1) on =idx= files that use
Russian characters. It is part of the T2 package.

*rumakeindex* runs *sed*(1) on the =idx= file to convert Cyrillic
character commands to 8-bit ASCII characters; pipes the results through
*makeindex*(1); and then through *tr*(1) to do some final conversions.
The results are placed in a KOI8-R encoded =ind= file.

* FILES
- basename.idx :: LaTeX index entry file

- basename.ilg :: LaTeX index log file

- basename.ind :: LaTeX processed index file

* BUGS
None known, but report any bugs found to the authors.

* COPYRIGHT
The T2 package is Copyright 1997-1999 Werner Lemberg, Vladimir Volovich
and any individual authors listed elsewhere in package files.

It may be distributed under the conditions of the LaTeX Project Public
License, either version 1.1 of this license or (at your option) any
later version.

* SEE ALSO
*makeindex*(1), *sed*(1), *tr*(1), <ftp://ftp.vsu.ru/pub/tex/T2/README>.

* AUTHOR
Werner Lemberg <wl@gnu.org>, Vladimir Volovich <TeX@vvv.vsu.ru>.

This manual page was written by C.M. Connelly <c@eskimo.com>, for the
Debian GNU/Linux system. It may be used by other distributions without
contacting the author. Any mistakes or omissions in the manual page are
my fault; inquiries about or corrections to this manual page should be
directed to me (and not to the primary author).
