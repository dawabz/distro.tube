#+TITLE: Man1 - grub-menulst2cfg.1
#+DESCRIPTION: Linux manpage for grub-menulst2cfg.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
grub-menulst2cfg - transform legacy menu.lst into grub.cfg

* SYNOPSIS
*grub-menulst2cfg* [/INFILE /[/OUTFILE/]]

* DESCRIPTION
* SEE ALSO
*grub-mkconfig*(8)

The full documentation for *grub-menulst2cfg* is maintained as a Texinfo
manual. If the *info* and *grub-menulst2cfg* programs are properly
installed at your site, the command

#+begin_quote
  *info grub-menulst2cfg*
#+end_quote

should give you access to the complete manual.
