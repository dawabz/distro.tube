#+TITLE: Man1 - i3-nagbar.1
#+DESCRIPTION: Linux manpage for i3-nagbar.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
i3-nagbar - displays an error bar on top of your screen

* SYNOPSIS
i3-nagbar [-m <message>] [-b <button> <action>] [-B <button> <action>]
[-t warning|error] [-f <font>] [-v] [-p]

* OPTIONS
*-v, --version*

#+begin_quote
  Display version number and exit.
#+end_quote

*-h, --help*

#+begin_quote
  Display a short help-message and exit.
#+end_quote

*-t, --type* /type/

#+begin_quote
  Display either a warning or error message. This only changes the color
  scheme for the i3-nagbar. Default: error.
#+end_quote

*-m, --message* /message/

#+begin_quote
  Display /message/ as text on the left of the i3-nagbar.
#+end_quote

*-f, --font* /font/

#+begin_quote
  Select font that is being used.
#+end_quote

*-b, --button* /button/ /action/

#+begin_quote
  Create a button with text /button/. The /action/ are the shell
  commands that will be executed by this button. Multiple buttons can be
  defined. Will launch the shell commands inside a terminal emulator,
  using i3-sensible-terminal.
#+end_quote

*-B, --button-no-terminal* /button/ /action/

#+begin_quote
  Same as above, but will execute the shell commands directly, without
  launching a terminal emulator.
#+end_quote

*-p, --primary*

#+begin_quote
  Always opens the i3-nagbar on the primary monitor. By default it opens
  on the focused monitor.
#+end_quote

* DESCRIPTION
i3-nagbar is used by i3 to tell you about errors in your configuration
file (for example). While these errors are logged to the logfile (if
any), the past has proven that users are either not aware of their
logfile or do not check it after modifying the configuration file.

* EXAMPLE

#+begin_quote
  #+begin_example
    i3-nagbar -m You have an error in your i3 config file! \
    -b edit config i3-sensible-editor ~/.config/i3/config
  #+end_example
#+end_quote

* SEE ALSO
i3(1)

* AUTHOR
Michael Stapelberg and contributors
