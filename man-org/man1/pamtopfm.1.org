#+TITLE: Man1 - pamtopfm.1
#+DESCRIPTION: Linux manpage for pamtopfm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pamtopfm - Convert Netpbm image to PFM (Portable Float Map)

* SYNOPSIS
*pamtopfm* [*-endian=*{*big*|*little*}] [*-scale=*/float/] [/imagefile/]

All options can be abbreviated to their shortest unique prefix. You may
use two hyphens instead of one. You may separate an option name and its
value with white space instead of an equals sign.

* DESCRIPTION
This program is part of *Netpbm*(1)

*pamtopfm* reads a Netpbm image (PNM or PAM) and converts it to a PFM
(Portable Float Map) image.

The PFM (Portable Float Map) image format is a lot like PPM, but uses
floating point numbers with no maxval to achieve a High Dynamic Range
(HDR) format. That means it doesn't have a concept of absolute color and
it can represent generic light intensity information rather than just
visual information like PPM does. For example, two pixels that are so
close in intensity that the human eye cannot tell them apart are not
visually distinct, so a visual image format such as PPM would have no
reason to use different sample values for them. But an HDR format would.

There are details of the PFM format in the *PFM* Format Description (5)

[[http://www.debevec.org/HDRShop][USC's HDRShop program]] and a program
called Lefty use it.

*pamtopfm* creates a color PFM image if its input is RGB (PPM) and a
non-color PFM otherwise.

Use **pfmtopam**(1) to convert a PFM image to Netpbm format.

* OPTIONS
- *-scale=*/float/ :: This specifies the scale factor of the PFM image.
  Scale factor is a component of the PFM format. Default is 1.0.

- *-endian=*{*big*|*little*} :: This specifies the endianness of the PFM
  image. The samples in the raster of a PFM image are 4 byte IEEE
  floating point numbers. A parameter of the IEEE format, and therefore
  the PFM format, is endianness, i.e. whether the specified bytes are
  ordered from low addresses to high addresses or vice versa.

*big* means big endian -- the natural ordering; *little* means
little-endian, the Intel-friendly ordering.

Default is whichever endianness the machine on which *pamtopfm* runs
uses internally, which results in the faster execution.

* SEE ALSO
*Netpbm*(1) , *pfmtopam*(1) , *pam*(5)

* HISTORY
*pamtopfm* was added to Netpbm in Release 10.22 (April 2004).
