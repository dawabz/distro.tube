#+TITLE: Man1 - mu-view.1
#+DESCRIPTION: Linux manpage for mu-view.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
mu view- display an e-mail message file

* SYNOPSIS
*mu view [options] <file> [<files>]*

* DESCRIPTION
*mu view* is the *mu* command for displaying e-mail message files. It
works on message files and does /not/ require the message to be indexed
in the database.

The command shows some common headers (From:, To:, Cc:, Bcc:, Subject:
and Date:), the list of attachments and the plain-text body of the
message (if any).

* OPTIONS
- *--summary-len*=/<number>/ :: instead of displaying the full message,
  output a summary based upon the first /<number>/ lines of the message.

- *--terminate* :: terminate messages with \f (/form-feed/) characters
  when displaying them. This is useful when you want to further process
  them.

- *--decrypt* :: attempt to decrypt encrypted message bodies. This is
  only possible if *mu* was built with crypto-support. Users are
  strongly recommended to use *gpg-agent*; however, if needed, *mu* will
  request the user password from the console.

* BUGS
Please report bugs if you find them: *https://github.com/djcb/mu/issues*

* AUTHOR
Dirk-Jan C. Binnema <djcb@djcbsoftware.nl>

* SEE ALSO
*mu*(1), *mu-index*(1), *gpg*(1), *gpg-agent*(1)
