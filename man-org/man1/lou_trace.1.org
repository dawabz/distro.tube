#+TITLE: Man1 - lou_trace.1
#+DESCRIPTION: Linux manpage for lou_trace.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lou_trace - A tool to list all the rules that were used for a Braille
translation

* SYNOPSIS
*lou_trace* [/OPTIONS/] /TABLE/[/,TABLE,/...]

* DESCRIPTION
Examine and debug Braille translation tables. This program allows you to
inspect liblouis translation tables by printing out the list of applied
translation rules for a given input.

- *-h*, *--help* :: display this help and exit

- *-v*, *--version* :: display version information and exit

- *-f*, *--forward* :: forward translation using the given table

- *-b*, *--backward* :: backward translation using the given table

- *--noContractions* :: Use no contractions

- *--dotsIO* :: Display dot patterns

- *--ucBrl* :: Use Unicode Braille patterns

- *--noUndefined* :: Disable output of undefined dot numbers during
  back-translation

- *--partialTrans* :: Use partial back-translation If neither *-f* nor
  *-b* are specified forward translation is assumed

* AUTHOR
Written by Bert Frees.

* REPORTING BUGS
Report bugs to liblouis-liblouisxml@freelists.org.\\
Liblouis home page: <http://www.liblouis.org>

* COPYRIGHT
Copyright © 2019 Swiss Library for the Blind, Visually Impaired and
Print Disabled. License GPLv3+: GNU GPL version 3 or later
<https://gnu.org/licenses/gpl.html>.\\
This is free software: you are free to change and redistribute it. There
is NO WARRANTY, to the extent permitted by law.

* SEE ALSO
The full documentation for *lou_trace* is maintained as a Texinfo
manual. If the *info* and *lou_trace* programs are properly installed at
your site, the command

#+begin_quote
  *info liblouis*
#+end_quote

should give you access to the complete manual.
