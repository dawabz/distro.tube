#+TITLE: Man1 - imgcmp.1
#+DESCRIPTION: Linux manpage for imgcmp.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
imgcmp - Image comparison utility

* SYNOPSIS
*imgcmp* [/options/]

* DESCRIPTION
The *imgcmp* command compares two images of the same geometry with
respect to a given metric. Please use the --help command line switch and
the JasPer Software Reference Manual for more information.

* SEE ALSO
/jasper/(1)

* AUTHOR
Michael D. Adams <mdadams@ieee.org> This manpage was initially written
by Roland Stigge <stigge@antcom.de> for the Debian Project.
