#+TITLE: Man1 - xapian-pos.1
#+DESCRIPTION: Linux manpage for xapian-pos.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xapian-pos - Debug positional data in a Xapian database

* SYNOPSIS
*xapian-pos* [/OPTIONS/] /DATABASE/

* DESCRIPTION
xapian-pos - Debug positional data in a Xapian database

* OPTIONS
- *-d*, *--doc*=/DOCID/ :: Show positions for document DOCID

- *-s*, *--start*=/POS/ :: Specifies the first position to show

- *-e*, *--end*=/POS/ :: Specifies the last position to show

- *--help* :: display this help and exit

- *--version* :: output version information and exit
