#+TITLE: Man1 - clusterdb.1
#+DESCRIPTION: Linux manpage for clusterdb.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
clusterdb - cluster a PostgreSQL database

* SYNOPSIS
*clusterdb* [/connection-option/...] [*--verbose* | *-v*] [ *--table* |
*-t* /table/ ]... [/dbname/]

*clusterdb* [/connection-option/...] [*--verbose* | *-v*] *--all* | *-a*

* DESCRIPTION
clusterdb is a utility for reclustering tables in a PostgreSQL database.
It finds tables that have previously been clustered, and clusters them
again on the same index that was last used. Tables that have never been
clustered are not affected.

clusterdb is a wrapper around the SQL command *CLUSTER*(7). There is no
effective difference between clustering databases via this utility and
via other methods for accessing the server.

* OPTIONS
clusterdb accepts the following command-line arguments:

*-a*\\
*--all*

#+begin_quote
  Cluster all databases.
#+end_quote

*[-d] */dbname/\\
*[--dbname=]*/dbname/

#+begin_quote
  Specifies the name of the database to be clustered, when *-a*/*--all*
  is not used. If this is not specified, the database name is read from
  the environment variable *PGDATABASE*. If that is not set, the user
  name specified for the connection is used. The /dbname/ can be a
  connection string. If so, connection string parameters will override
  any conflicting command line options.
#+end_quote

*-e*\\
*--echo*

#+begin_quote
  Echo the commands that clusterdb generates and sends to the server.
#+end_quote

*-q*\\
*--quiet*

#+begin_quote
  Do not display progress messages.
#+end_quote

*-t */table/\\
*--table=*/table/

#+begin_quote
  Cluster /table/ only. Multiple tables can be clustered by writing
  multiple *-t* switches.
#+end_quote

*-v*\\
*--verbose*

#+begin_quote
  Print detailed information during processing.
#+end_quote

*-V*\\
*--version*

#+begin_quote
  Print the clusterdb version and exit.
#+end_quote

*-?*\\
*--help*

#+begin_quote
  Show help about clusterdb command line arguments, and exit.
#+end_quote

clusterdb also accepts the following command-line arguments for
connection parameters:

*-h */host/\\
*--host=*/host/

#+begin_quote
  Specifies the host name of the machine on which the server is running.
  If the value begins with a slash, it is used as the directory for the
  Unix domain socket.
#+end_quote

*-p */port/\\
*--port=*/port/

#+begin_quote
  Specifies the TCP port or local Unix domain socket file extension on
  which the server is listening for connections.
#+end_quote

*-U */username/\\
*--username=*/username/

#+begin_quote
  User name to connect as.
#+end_quote

*-w*\\
*--no-password*

#+begin_quote
  Never issue a password prompt. If the server requires password
  authentication and a password is not available by other means such as
  a .pgpass file, the connection attempt will fail. This option can be
  useful in batch jobs and scripts where no user is present to enter a
  password.
#+end_quote

*-W*\\
*--password*

#+begin_quote
  Force clusterdb to prompt for a password before connecting to a
  database.

  This option is never essential, since clusterdb will automatically
  prompt for a password if the server demands password authentication.
  However, clusterdb will waste a connection attempt finding out that
  the server wants a password. In some cases it is worth typing *-W* to
  avoid the extra connection attempt.
#+end_quote

*--maintenance-db=*/dbname/

#+begin_quote
  Specifies the name of the database to connect to to discover which
  databases should be clustered, when *-a*/*--all* is used. If not
  specified, the postgres database will be used, or if that does not
  exist, template1 will be used. This can be a connection string. If so,
  connection string parameters will override any conflicting command
  line options. Also, connection string parameters other than the
  database name itself will be re-used when connecting to other
  databases.
#+end_quote

* ENVIRONMENT
*PGDATABASE*\\
*PGHOST*\\
*PGPORT*\\
*PGUSER*

#+begin_quote
  Default connection parameters
#+end_quote

*PG_COLOR*

#+begin_quote
  Specifies whether to use color in diagnostic messages. Possible values
  are always, auto and never.
#+end_quote

This utility, like most other PostgreSQL utilities, also uses the
environment variables supported by libpq (see Section 33.14).

* DIAGNOSTICS
In case of difficulty, see *CLUSTER*(7) and *psql*(1) for discussions of
potential problems and error messages. The database server must be
running at the targeted host. Also, any default connection settings and
environment variables used by the libpq front-end library will apply.

* EXAMPLES
To cluster the database test:

#+begin_quote
  #+begin_example
    $ clusterdb test
  #+end_example
#+end_quote

To cluster a single table foo in a database named xyzzy:

#+begin_quote
  #+begin_example
    $ clusterdb --table=foo xyzzy
  #+end_example
#+end_quote

* SEE ALSO
*CLUSTER*(7)
