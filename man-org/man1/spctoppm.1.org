#+TITLE: Man1 - spctoppm.1
#+DESCRIPTION: Linux manpage for spctoppm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
spctoppm - convert an Atari compressed Spectrum file to a PPM

* SYNOPSIS
*spctoppm*

[/spcfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*spctoppm* reads an Atari compressed Spectrum file as input and produces
a PPM image as output.

* SEE ALSO
*sputoppm*(1) , *ppm*(5)

* AUTHOR
Copyright (C) 1991 by Steve Belczyk (/seb3@gte.com/) and Jef Poskanzer.
