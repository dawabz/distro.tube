#+TITLE: Man1 - melt-7.1
#+DESCRIPTION: Linux manpage for melt-7.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
melt - author, play, and encode multitrack audio/video compositions

* SYNOPSIS
*melt* [/options/] [/producer /[/name=value/]/* /]/+/

* OPTIONS
- *-attach* filter[:arg] [name=value]* :: Attach a filter to the output

- *-attach-cut* filter[:arg] [name=value]* :: Attach a filter to a cut

*-attach-track* filter[:arg] [name=value]* Attach a filter to a track

- *-attach-clip* filter[:arg] [name=value]* :: Attach a filter to a
  producer

- *-audio-track* | *-hide-video* :: Add an audio-only track

- *-blank* frames :: Add blank silence to a track

- *-chain* id[:arg] [name=value]* :: Add a producer as a chain

- *-consumer* id[:arg] [name=value]* :: Set the consumer (sink)

- *-debug* :: Set the logging level to debug

- *-filter* filter[:arg] [name=value]* :: Add a filter to the current
  track

- *-getc* :: Get keyboard input using getc

- *-group* [name=value]* :: Apply properties repeatedly

- *-help* :: Show this message

- *-jack* :: Enable JACK transport synchronization

- *-join* clips :: Join multiple clips into one cut

- *-link* id[:arg] [name=value]* :: Add a link to a chain

- *-mix* length :: Add a mix between the last two cuts

- *-mixer* transition :: Add a transition to the mix

- *-null-track* | *-hide-track* :: Add a hidden track

- *-profile* name :: Set the processing settings

- *-progress* :: Display progress along with position

- *-query* :: List all of the registered services

- *-query* "consumers" | "consumer"=id :: List consumers or show info
  about one

- *-query* "filters" | "filter"=id :: List filters or show info about
  one

- *-query* "producers" | "producer"=id :: List producers or show info
  about one

- *-query* "transitions" | "transition"=id :: List transitions, show
  info about one

- *-query* "profiles" | "profile"=id :: List profiles, show info about
  one

- *-query* "presets" | "preset"=id :: List presets, show info about one

- *-query* "formats" :: List audio/video formats

- *-query* "audio_codecs" :: List audio codecs

- *-query* "video_codecs" :: List video codecs

- *-quiet* :: Set the logging level to quiet

- *-remove* :: Remove the most recent cut

- *-repeat* times :: Repeat the last cut

- *-repository* path :: Set the directory of MLT modules

- *-serialise* [filename] :: Write the commands to a text file

- *-silent* :: Do not display position/transport

- *-split* relative-frame :: Split the last cut into two cuts

- *-swap* :: Rearrange the last two cuts

- *-track* :: Add a track

- *-transition* id[:arg] [name=value]* :: Add a transition

- *-verbose* :: Set the logging level to verbose

- *-timings* :: Set the logging level to timings

- *-version* :: Show the version and copyright

- *-video-track* | *-hide-audio* :: Add a video-only track

For more help: <https://www.mltframework.org/>

* COPYRIGHT
Copyright © 2002-2020 Meltytech, LLC <https://www.mltframework.org/>\\
This is free software; see the source for copying conditions. There is
NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.
