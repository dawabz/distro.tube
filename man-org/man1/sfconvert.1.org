#+TITLE: Man1 - sfconvert.1
#+DESCRIPTION: Linux manpage for sfconvert.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sfconvert - convert audio files

* SYNOPSIS
*sfconvert* /infile/ /outfile/ [/output options/]

* DESCRIPTION
The sfconvert command converts an audio file to another file format or
data format.

* OPTIONS
The following keywords specify the format of the output sound file:

format /f/

#+begin_quote
  Output file format. /f/ must be one of the following:

  | aiff  | Audio Interchange File Format         |
  | aifc  | AIFF-C                                |
  | next  | NeXT/Sun .snd/.au Format              |
  | wave  | MS RIFF WAVE                          |
  | bicsf | Berkeley/IRCAM/CARL Sound File Format |
  | smp   | Sample Vision Format                  |
  | voc   | Creative Voice File Format            |
  | nist  | NIST SPHERE Format                    |
  | caf   | Core Audio Format                     |
  | flac  | FLAC                                  |
#+end_quote

compression /c/

#+begin_quote
  | ulaw    | G.711 mu-law               |
  | alaw    | G.711 A-law                |
  | ima     | IMA ADPCM                  |
  | msadpcm | Microsoft ADPCM            |
  | flac    | FLAC                       |
  | alac    | Apple Lossless Audio Codec |
#+end_quote

byteorder /e/

#+begin_quote
  Output byte order. /e/ may be either big or little.
#+end_quote

channels /n/

#+begin_quote
  Number of output channels. /n/ is 1 for mono, and 2 for stereo.
#+end_quote

integer /n/ /s/

#+begin_quote
  Convert to integer audio data. /n/ indicates the output sample width
  in bits. /s/ specifies the sample format and may be either 2scomp for
  2's complement signed data or unsigned for unsigned data.
#+end_quote

float /m/

#+begin_quote
  Convert to floating-point audio data with a maximum amplitude of /m/
  (usually 1.0). The integer and float options are mutually exclusive.
#+end_quote

The following options take no additional arguments:

--help, -h

#+begin_quote
  Print help message.
#+end_quote

--version, -v

#+begin_quote
  Print version information.
#+end_quote

* SEE ALSO
*sfinfo*(1)

* AUTHOR
Michael Pruett <michael@68k.org>
