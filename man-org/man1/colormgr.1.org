#+TITLE: Man1 - colormgr.1
#+DESCRIPTION: Linux manpage for colormgr.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
colormgr - Color Manager Testing Tool

* SYNOPSIS
*colormgr* [*--verbose*] [*--version*]

* DESCRIPTION
This manual page documents briefly the *colormgr* command.

*colormgr* allows an administrator to view and change color profile to
device mappings.

* OPTIONS
This program follows the usual GNU command line syntax, with long
options starting with two dashes (‘-'). A summary of options is included
below.

*--help*

#+begin_quote
  Show summary of options.
#+end_quote

* COMMANDS
This program takes commands with a variable number of arguments.

*create-device* /device_id/ /scope/

#+begin_quote
  Create a device
#+end_quote

*create-profile* /profile_id/ /scope/

#+begin_quote
  Create a profile
#+end_quote

*delete-device* /device/

#+begin_quote
  Deletes a device
#+end_quote

*delete-profile* /profile/

#+begin_quote
  Deletes a profile
#+end_quote

*device-add-profile* /device/ /profile/

#+begin_quote
  Add a profile to a device that already exists
#+end_quote

*device-get-default-profile* /device/

#+begin_quote
  Gets the default profile for a device
#+end_quote

*device-get-profile-for-qualifier* /device/ /qualifier/

#+begin_quote
  Returns all the profiles that match a qualifier
#+end_quote

*device-inhibit* /device/ /timeout/

#+begin_quote
  Inhibits colour profiles for this device
#+end_quote

*device-make-profile-default* /device/ /profile/

#+begin_quote
  Makes a profile default for a device
#+end_quote

*device-set-enabled*

#+begin_quote
  Enables or disables the device
#+end_quote

*device-set-kind* /device/ /kind/

#+begin_quote
  Sets the device kind
#+end_quote

*device-set-model* /device/ /model/

#+begin_quote
  Sets the device model
#+end_quote

*device-set-serial* /device/ /serial/

#+begin_quote
  Sets the device serial
#+end_quote

*device-set-vendor* /device/ /vendor/

#+begin_quote
  Sets the device vendor
#+end_quote

*find-device* /device_id/

#+begin_quote
  Find a device from the device ID
#+end_quote

*find-device-by-property* /key/ /value/

#+begin_quote
  Find a device with a given property value
#+end_quote

*find-profile* /profile_id/

#+begin_quote
  Find a profile from the profile ID
#+end_quote

*find-profile-by-filename* /filename/

#+begin_quote
  Find a profile by filename
#+end_quote

*get-devices*

#+begin_quote
  Gets all the color managed devices
#+end_quote

*get-devices-by-kind* /kind/

#+begin_quote
  Gets all the color managed devices of a specific kind
#+end_quote

*get-profiles*

#+begin_quote
  Gets all the available color profiles
#+end_quote

*get-sensor-reading* /display_type/

#+begin_quote
  Gets a reading from a sensor
#+end_quote

*get-sensors*

#+begin_quote
  Gets all the available colour sensors
#+end_quote

*get-standard-space* /standard_space/

#+begin_quote
  Get a standard colourspace
#+end_quote

*import-profile* /filename/

#+begin_quote
  Copies an .icc file into the correct per-user directory and shows
  information about the imported profile.
#+end_quote

*profile-set-property* /profile/ /name/ /value/

#+begin_quote
  Sets extra properties on the profile
#+end_quote

*sensor-lock*

#+begin_quote
  Locks the colour sensor
#+end_quote

*sensor-set-options* /name/ /value/

#+begin_quote
  Sets one or more sensor options
#+end_quote

* AUTHOR
This manual page was written by Richard Hughes <richard@hughsie.com>.
