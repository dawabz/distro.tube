#+TITLE: Man1 - lispmtopgm.1
#+DESCRIPTION: Linux manpage for lispmtopgm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
lispmtopgm - convert a Lisp Machine bitmap file to PGM

* SYNOPSIS
*lispmtopgm* [/lispmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*listpmfile* reads a Lisp Machine bitmap as input and produces a PGM
image as output.

This is the file format written by the tv:write-bit-array-file function
on TI Explorer and Symbolics lisp machines.

Multi-plane bitmaps on lisp machines are color; but the Lispm image file
format does not include a color map, so we must treat it as a monochrome
instead and produce PGM. This is unfortunate.

* SEE ALSO
*pgmtolispm*(1) , *pgm*(5)

* LIMITATIONS
The Lispm bitmap file format is a bit quirky; Usually the image in the
file has its width rounded up to the next higher multiple of 32, but not
always. If the width is not a multiple of 32, we don't deal with it
properly, but because of the Lispm microcode, such arrays are probably
not image data anyway.

Also, the Lispm code for saving bitmaps has a bug, in that if you are
writing a bitmap which is not mod32 across, the file may be up to 7 bits
too short! They round down instead of up, and we don't handle this bug
gracefully.

* AUTHOR
Copyright (C) 1991 by Jamie Zawinski and Jef Poskanzer.
