#+TITLE: Man1 - appstreamcli-compose.1
#+DESCRIPTION: Linux manpage for appstreamcli-compose.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
appstreamcli-compose - Compose AppStream collection metadata from
directory trees

* SYNOPSIS
*appstreamcli compose* [*COMMAND*]

* DESCRIPTION
This manual page documents briefly the *appstreamcli compose* command.

*appstreamcli compose* is a tool to construct AppStream collection
metadata from directory trees. The tool will also perform many related
metadata generation actions, like resizing icons and screenshots and
merging in data from referenced desktop-entry files as well as
translation status information. Therefore, the tool provides a fast way
to test how the final processed metadata for an application that is
shipped to users may look like. It also provides a way to easily
generate AppStream data for projects which do not need a more complex
data generator like *appstream-generator*.

In order for the *appstreamcli compose* command to be available, you may
need to install the optional compose module for *appstreamcli* first.

For more information about the AppStream project and the other
components which are part of it, take a look at the AppStream pages at
*Freedesktop.org*[1].

* OPTIONS
/SOURCE DIRECTORIES/

#+begin_quote
  A list of directories to process needs to be provided as positional
  parameters. Data from all directories will be combined into one output
  namespace.
#+end_quote

*--origin */NAME/

#+begin_quote
  Set the AppStream data origin identifier. This can be a value like
  "debian-unstable-main" or "flathub".
#+end_quote

*--result-root */DIR/

#+begin_quote
  Sets the directory where all generated output that is deployed to a
  users machine is exported to. If this parameter is not set and we only
  have one directory to process, we use that directory as default output
  path.

  If both *--data-dir* and *--icons-dir* are set, *--result-root* is not
  necessary and no data will be written to that directory.
#+end_quote

*--data-dir */DIR/

#+begin_quote
  Override the directory where the generated AppStream collection
  metadata will be written to. Data will be written directly to this
  directory, and no supdirectories will be created (unlike when using
  *--result-root* to set an output location).
#+end_quote

*--icons-dir */DIR/

#+begin_quote
  Override the directory where the cached icons are exported to.
#+end_quote

*--hints-dir */DIR/

#+begin_quote
  Set a directory where hints reported generated during metadata
  processing are saved to. If this parameter is not set, no HTML/YAML
  hint reports will be saved.
#+end_quote

*--media-dir */DIR/

#+begin_quote
  If set, creates a directory with media content (icons, screenshots,
  ...) that can be served via a webserver. The metadata will be extended
  to include information about these remote media.
#+end_quote

*--media-baseurl */URL/

#+begin_quote
  The URL under which the contents of a directory set via *--media-dir*
  will be served. This value must be set if a media directory is
  created.
#+end_quote

*--prefix */DIR/

#+begin_quote
  Set the default prefix that is used in the processed directories. If
  none is set explicitly, /usr is assumed.
#+end_quote

*--print-report */MODE/

#+begin_quote
  Print the issue hints report (that gets exported as HTML and YAML
  document when *--hints-dir* was set) to the console in text form.

  Various print modes are supported: /on-error/ only prints a short
  report if the run failed (default), /short/ generates an abridged
  report that is always printed and /full/ results in a detailed report
  to be printed.
#+end_quote

*--components */COMPONENT-IDs/

#+begin_quote
  Set a comma-separated list of AppStream component IDs that should be
  considered for the generated metadata. All components that exist in
  the input data but are not mentioned in this list will be ignored for
  the generated output.
#+end_quote

*--no-color*

#+begin_quote
  Dont print colored output.
#+end_quote

*--verbose*

#+begin_quote
  Display extra debugging information
#+end_quote

*--version*

#+begin_quote
  Display the version number of appstreamcli compose
#+end_quote

* SEE ALSO
appstreamcli(1), appstream-generator(1).

* AUTHOR
This manual page was written by Matthias Klumpp <matthias@tenstral.net>.

* COPYRIGHT
\\
Copyright © 2020-2021 Matthias Klumpp\\

* NOTES
-  1. :: Freedesktop.org

  https://www.freedesktop.org/wiki/Distributions/AppStream/
