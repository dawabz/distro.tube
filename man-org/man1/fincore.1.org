#+TITLE: Man1 - fincore.1
#+DESCRIPTION: Linux manpage for fincore.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fincore - count pages of file contents in core

* SYNOPSIS
*fincore* [options] /file/...

* DESCRIPTION
*fincore* counts pages of file contents being resident in memory (in
core), and reports the numbers. If an error occurs during counting, then
an error message is printed to the stderr and *fincore* continues
processing the rest of files listed in a command line.

The default output is subject to change. So whenever possible, you
should avoid using default outputs in your scripts. Always explicitly
define expected columns by using *--output* /columns-list/ in
environments where a stable output is required.

* OPTIONS
*-n*, *--noheadings*

#+begin_quote
  Do not print a header line in status output.
#+end_quote

*-b*, *--bytes*

#+begin_quote
  Print the SIZE column in bytes rather than in a human-readable format.
#+end_quote

*-o*, *--output* /list/

#+begin_quote
  Define output columns. See the *--help* output to get a list of the
  currently supported columns. The default list of columns may be
  extended if /list/ is specified in the format /+list/.
#+end_quote

*-r*, *--raw*

#+begin_quote
  Produce output in raw format. All potentially unsafe characters are
  hex-escaped (\x<code>).
#+end_quote

*-J*, *--json*

#+begin_quote
  Use JSON output format.
#+end_quote

*-V*, *--version*

#+begin_quote
  Display version information and exit.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help text and exit.
#+end_quote

* AUTHORS
* SEE ALSO
*mincore*(2), *getpagesize*(2), *getconf*(1p)

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *fincore* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
