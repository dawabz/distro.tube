#+TITLE: Man1 - pw-mon.1
#+DESCRIPTION: Linux manpage for pw-mon.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pw-mon - The PipeWire monitor

* SYNOPSIS
#+begin_example
  pw-mon [options]
#+end_example

* DESCRIPTION
Monitor objects on the PipeWire instance.

* OPTIONS

#+begin_quote
  - *-r | --remote=NAME* :: The name the /remote/ instance to monitor.
    If left unspecified, a connection is made to the default PipeWire
    instance.

  - *-h | --help* :: Show help.
#+end_quote

#+begin_quote
  - *--version* :: Show version information.
#+end_quote

#+begin_quote
  - *-N | --color=WHEN* :: Whether to use color, one of 'never',
    'always', or 'auto'. The default is 'auto'. *-N* is equivalent to
    *--color=never*.
#+end_quote

* AUTHORS
The PipeWire Developers
</https://gitlab.freedesktop.org/pipewire/pipewire/issues/>; PipeWire is
available from /https://pipewire.org/

* SEE ALSO
*pipewire(1)*,
