#+TITLE: Manpages - opt.1
#+DESCRIPTION: Linux manpage for opt.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
opt - LLVM optimizer

* SYNOPSIS
*opt* [/options/] [/filename/]

* DESCRIPTION
The *opt* command is the modular LLVM optimizer and analyzer. It takes
LLVM source files as input, runs the specified optimizations or analyses
on it, and then outputs the optimized file or the analysis results. The
function of *opt* depends on whether the /-analyze/ option is given.

When /-analyze/ is specified, *opt* performs various analyses of the
input source. It will usually print the results on standard output, but
in a few cases, it will print output to standard error or generate a
file with the analysis output, which is usually done when the output is
meant for another program.

While /-analyze/ is /not/ given, *opt* attempts to produce an optimized
output file. The optimizations available via *opt* depend upon what
libraries were linked into it as well as any additional libraries that
have been loaded with the /-load/ option. Use the /-help/ option to
determine what optimizations you can use.

If *filename* is omitted from the command line or is "*-*", *opt* reads
its input from standard input. Inputs can be in either the LLVM assembly
language format (*.ll*) or the LLVM bitcode format (*.bc*).

If an output filename is not specified with the /-o/ option, *opt*
writes its output to the standard output.

* OPTIONS

#+begin_quote
  - *-f* :: Enable binary output on terminals. Normally, *opt* will
    refuse to write raw bitcode output if the output stream is a
    terminal. With this option, *opt* will write raw bitcode regardless
    of the output device.
#+end_quote

#+begin_quote
  - *-help* :: Print a summary of command line options.
#+end_quote

#+begin_quote
  - *-o <filename>* :: Specify the output filename.
#+end_quote

#+begin_quote
  - *-S* :: Write output in LLVM intermediate language (instead of
    bitcode).
#+end_quote

#+begin_quote
  - *-{passname}* :: *opt* provides the ability to run any of LLVM's
    optimization or analysis passes in any order. The /-help/ option
    lists all the passes available. The order in which the options occur
    on the command line are the order in which they are executed (within
    pass constraints).
#+end_quote

#+begin_quote
  - *-disable-inlining* :: This option simply removes the inlining pass
    from the standard list.
#+end_quote

#+begin_quote
  - *-disable-opt* :: This option is only meaningful when
    /-std-link-opts/ is given. It disables most passes.
#+end_quote

#+begin_quote
  - *-strip-debug* :: This option causes opt to strip debug information
    from the module before applying other optimizations. It is
    essentially the same as /-strip/ but it ensures that stripping of
    debug information is done first.
#+end_quote

#+begin_quote
  - *-verify-each* :: This option causes opt to add a verify pass after
    every pass otherwise specified on the command line (including
    /-verify/). This is useful for cases where it is suspected that a
    pass is creating an invalid module but it is not clear which pass is
    doing it.
#+end_quote

#+begin_quote
  - *-stats* :: Print statistics.
#+end_quote

#+begin_quote
  - *-time-passes* :: Record the amount of time needed for each pass and
    print it to standard error.
#+end_quote

#+begin_quote
  - *-debug* :: If this is a debug build, this option will enable debug
    printouts from passes which use the *LLVM_DEBUG()* macro. See the
    /LLVM Programmer's Manual/, section *#DEBUG* for more information.
#+end_quote

#+begin_quote
  - *-load=<plugin>* :: Load the dynamic object *plugin*. This object
    should register new optimization or analysis passes. Once loaded,
    the object will add new command line options to enable various
    optimizations or analyses. To see the new complete list of
    optimizations, use the /-help/ and /-load/ options together. For
    example:

    #+begin_quote
      #+begin_example
        opt -load=plugin.so -help
      #+end_example
    #+end_quote
#+end_quote

#+begin_quote
  - *-p* :: Print module after each transformation.
#+end_quote

* EXIT STATUS
If *opt* succeeds, it will exit with 0. Otherwise, if an error occurs,
it will exit with a non-zero value.

* AUTHOR
Maintained by the LLVM Team (https://llvm.org/).

* COPYRIGHT
2003-2021, LLVM Project

Information about opt.1 is found in manpage for: [[../\-load=plugin.so \-help][\-load=plugin.so \-help]]