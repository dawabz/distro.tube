#+TITLE: Manpages - djvuxmlparser.1
#+DESCRIPTION: Linux manpage for djvuxmlparser.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about djvuxmlparser.1 is found in manpage for: [[../man1/djvuxml.1][man1/djvuxml.1]]