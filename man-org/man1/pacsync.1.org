#+TITLE: Man1 - pacsync.1
#+DESCRIPTION: Linux manpage for pacsync.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
pacsync - update sync databases

* SYNOPSIS
pacsync [options] [<syncdb>]... pacsync (--help|--version)

* DESCRIPTION
Update sync databases. If no /syncdb/ names are provided all databases
will by updated.

* OPTIONS
- --config=path :: Set an alternate configuration file path.

- --dbext=extension :: Set an alternate sync database extension.

- --dbpath=path :: Set an alternate database path.

- --logfile=path :: Set an alternate log file path.

- --no-timeout :: Disable low-speed timeouts for downloads.

- --root=path :: Set an alternate installation root.

- --sysroot=path :: Set an alternate system root. See
  *pacutils-sysroot* (7).

- --debug :: Display additional debugging information.

- --updated :: Return true only if a database was actually updated.

- --force :: Update databases even if already up-to-date.

- --help :: Display usage information and exit.

- --version :: Display version information and exit.
