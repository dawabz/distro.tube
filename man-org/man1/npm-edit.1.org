#+TITLE: Man1 - npm-edit.1
#+DESCRIPTION: Linux manpage for npm-edit.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*npm-edit* - Edit an installed package

** Synopsis

#+begin_quote
  #+begin_example
    npm edit <pkg>
  #+end_example
#+end_quote

Note: This command is unaware of workspaces.

** Description
Selects a dependency in the current project and opens the package folder
in the default editor (or whatever you've configured as the npm *editor*
config -- see *npm-config* /npm-config)./

After it has been edited, the package is rebuilt so as to pick up any
changes in compiled packages.

For instance, you can do *npm install connect* to install connect into
your package, and then *npm edit connect* to make a few changes to your
locally installed copy.

** Configuration
<!-- AUTOGENERATED CONFIG DESCRIPTIONS START --> <!-- automatically
generated, do not edit manually --> <!-- see
lib/utils/config/definitions.js -->

** *editor*

#+begin_quote

  - Default: The EDITOR or VISUAL environment variables, or
    'notepad.exe' on Windows, or 'vim' on Unix systems

  - Type: String
#+end_quote

The command to run for *npm edit* and *npm config edit* . <!--
automatically generated, do not edit manually --> <!-- see
lib/utils/config/definitions.js -->

<!-- AUTOGENERATED CONFIG DESCRIPTIONS END -->

** See Also

#+begin_quote

  - npm help folders

  - npm help explore

  - npm help install

  - npm help config

  - npm help npmrc
#+end_quote
