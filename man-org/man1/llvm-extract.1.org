#+TITLE: Man1 - llvm-extract.1
#+DESCRIPTION: Linux manpage for llvm-extract.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
llvm-extract - extract a function from an LLVM module

* SYNOPSIS
*llvm-extract* [/options/] *--func* /function-name/ [/filename/]

* DESCRIPTION
The *llvm-extract* command takes the name of a function and extracts it
from the specified LLVM bitcode file. It is primarily used as a
debugging tool to reduce test cases from larger programs that are
triggering a bug.

In addition to extracting the bitcode of the specified function,
*llvm-extract* will also remove unreachable global variables,
prototypes, and unused types.

The *llvm-extract* command reads its input from standard input if
filename is omitted or if filename is *-*. The output is always written
to standard output, unless the *-o* option is specified (see below).

* OPTIONS
*--alias* /alias-name/

#+begin_quote

  #+begin_quote
    Extract the alias named /function-name/ from the LLVM bitcode. May
    be specified multiple times to extract multiple alias at once.
  #+end_quote
#+end_quote

*--ralias* /alias-regular-expr/

#+begin_quote

  #+begin_quote
    Extract the alias matching /alias-regular-expr/ from the LLVM
    bitcode. All alias matching the regular expression will be
    extracted. May be specified multiple times.
  #+end_quote
#+end_quote

*--bb* /basic-block-specifier/

#+begin_quote

  #+begin_quote
    Extract basic blocks(s) specified in /basic-block-specifier/. May be
    specified multiple times. Each <function:bb[;bb]> specifier pair
    will create a function. If multiple basic blocks are specified in
    one pair, the first block in the sequence should dominate the rest.
  #+end_quote
#+end_quote

*--delete*

#+begin_quote

  #+begin_quote
    Delete specified Globals from Module.
  #+end_quote
#+end_quote

*-f*

#+begin_quote

  #+begin_quote
    Enable binary output on terminals. Normally, *llvm-extract* will
    refuse to write raw bitcode output if the output stream is a
    terminal. With this option, *llvm-extract* will write raw bitcode
    regardless of the output device.
  #+end_quote
#+end_quote

*--func* /function-name/

#+begin_quote

  #+begin_quote
    Extract the function named /function-name/ from the LLVM bitcode.
    May be specified multiple times to extract multiple functions at
    once.
  #+end_quote
#+end_quote

*--rfunc* /function-regular-expr/

#+begin_quote

  #+begin_quote
    Extract the function(s) matching /function-regular-expr/ from the
    LLVM bitcode. All functions matching the regular expression will be
    extracted. May be specified multiple times.
  #+end_quote
#+end_quote

*--glob* /global-name/

#+begin_quote

  #+begin_quote
    Extract the global variable named /global-name/ from the LLVM
    bitcode. May be specified multiple times to extract multiple global
    variables at once.
  #+end_quote
#+end_quote

*--rglob* /glob-regular-expr/

#+begin_quote

  #+begin_quote
    Extract the global variable(s) matching /global-regular-expr/ from
    the LLVM bitcode. All global variables matching the regular
    expression will be extracted. May be specified multiple times.
  #+end_quote
#+end_quote

*--keep-const-init*

#+begin_quote

  #+begin_quote
    Preserve the values of constant globals.
  #+end_quote
#+end_quote

*--recursive*

#+begin_quote

  #+begin_quote
    Recursively extract all called functions
  #+end_quote
#+end_quote

*-help*

#+begin_quote

  #+begin_quote
    Print a summary of command line options.
  #+end_quote
#+end_quote

*-o* /filename/

#+begin_quote

  #+begin_quote
    Specify the output filename. If filename is "-" (the default), then
    *llvm-extract* sends its output to standard output.
  #+end_quote
#+end_quote

*-S*

#+begin_quote

  #+begin_quote
    Write output in LLVM intermediate language (instead of bitcode).
  #+end_quote
#+end_quote

* EXIT STATUS
If *llvm-extract* succeeds, it will exit with 0. Otherwise, if an error
occurs, it will exit with a non-zero value.

* SEE ALSO
*bugpoint(1)*

* AUTHOR
Maintained by the LLVM Team (https://llvm.org/).

* COPYRIGHT
2003-2021, LLVM Project
