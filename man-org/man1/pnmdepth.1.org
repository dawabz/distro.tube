#+TITLE: Man1 - pnmdepth.1
#+DESCRIPTION: Linux manpage for pnmdepth.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pnmdepth - change the maxval in a PNM image

* DESCRIPTION
This program is part of *Netpbm*(1)

Starting with Netpbm 10.32 (February 2006), *pnmdepth* is obsolete. Use
**pamdepth**(1) instead.

*pamdepth* is backward compatible with *pnmdepth*. You can use the
*pamdepth* manual for *pnmdepth* as long as you ignore features that
were added after Netpbm 10.31.

For backward compatibility, the name 'pnmdepth' continues to exist as an
alias for 'pamdepth'. But because of a bug, that name doesn't work in
Netpbm 10.32. You have to fix the symbolic link.
