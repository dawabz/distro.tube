#+TITLE: Man1 - GraphicsMagickWand-config.1
#+DESCRIPTION: Linux manpage for GraphicsMagickWand-config.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
GraphicsMagickWand-config - get information about the installed version
of GraphicsMagick

* SYNOPSIS
*GraphicsMagickWand-config* *[--cflags]* *[--cppflags]*
*[--exec-prefix]* *[--ldflags]* *[--libs]* *[--prefix]* *[--version]*

* DESCRIPTION
*GraphicsMagickWand-config* prints the compiler and linker flags
required to compile and link programs that use the *GraphicsMagick Wand*
Application Programmer Interface.

* EXAMPLES
To print the version of the installed distribution of *GraphicsMagick*,
use:

#+begin_example
    GraphicsMagickWand-config --version
#+end_example

To compile a program that calls the *GraphicsMagick Wand* Application
Programmer Interface, use:

#+begin_example
    cc `GraphicsMagickWand-config --cflags --cppflags --ldflags --libs` program.c
#+end_example

* OPTIONS
- *--cflags* :: Print the compiler flags that were used to compile
  *libGraphicsMagick*.

- *--cppflags* :: Print the preprocessor flags that are needed to find
  the *GraphicsMagick* C include files and defines to ensure that the
  GraphicsMagick data structures match between your program and the
  installed libraries.

- *--exec-prefix* :: Print the directory under which target specific
  binaries and executables are installed.

- *--ldflags* :: Print the linker flags that are needed to link with the
  *GraphicsMagickWand* library.

- *--libs* :: Print the linker flags that are needed to link a program
  with *libMagick*.

- *--version* :: Print the version of the *GraphicsMagick* distribution
  to standard output.

* COPYRIGHT
Copyright (C) 2003 GraphicsMagick Group

Copyright (C) 2002 ImageMagick Studio

* AUTHORS
Bob Friesenhahn
