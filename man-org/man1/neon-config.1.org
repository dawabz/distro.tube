#+TITLE: Man1 - neon-config.1
#+DESCRIPTION: Linux manpage for neon-config.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
neon-config - script providing information about installed copy of neon
library

* SYNOPSIS
*neon-config* [*--prefix*] [[*--cflags*] | [*--libs*] | [*--la-file*] |
[*--support* /feature/] | [*--help*] | [*--version*]]

* DESCRIPTION
The *neon-config* script provides information about an installed copy of
the neon library. The *--cflags* and *--libs* options instruct how to
compile and link an application against the library; the *--version* and
*--support* options can help determine whether the library meets the
applications requirements.

* OPTIONS
*--cflags*

#+begin_quote
  Print the flags which should be passed to the C compiler when
  compiling object files, when the object files use neon header files.
#+end_quote

*--libs*

#+begin_quote
  Print the flags which should be passed to the linker when linking an
  application which uses the neon library
#+end_quote

*--la-file*

#+begin_quote
  Print the location of the libtool library script, libneon.la, which
  can be used to link against neon by applications using libtool.
#+end_quote

*--version*

#+begin_quote
  Print the version of the library
#+end_quote

*--prefix* /dir/

#+begin_quote
  If /dir/ is given; relocate output of *--cflags* and *--libs* as if
  neon was installed in given prefix directory. Otherwise, print the
  installation prefix of the library.
#+end_quote

*--support* /feature/

#+begin_quote
  The script exits with success if /feature/ is supported by the
  library.
#+end_quote

*--help*

#+begin_quote
  Print help message; includes list of known features and whether they
  are supported or not.
#+end_quote

* EXAMPLE
Below is a Makefile fragment which could be used to build an application
against an installed neon library, when the *neon-config* script can be
found in *$PATH*.

#+begin_quote
  #+begin_example
    CFLAGS = `neon-config --cflags`
    LIBS = `neon-config --libs`
    OBJECTS = myapp.o
    TARGET = myapp

    $(TARGET): $(OBJECTS)
    	$(CC) $(LDFLAGS) -o $(TARGET) $(OBJECTS) $(LIBS)

    myapp.o: myapp.c
    	$(CC) $(CFLAGS) -c myapp.c -o myapp.o
  #+end_example
#+end_quote

* AUTHOR
*Joe Orton* <neon@lists.manyfish.co.uk>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
