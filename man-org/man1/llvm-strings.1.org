#+TITLE: Man1 - llvm-strings.1
#+DESCRIPTION: Linux manpage for llvm-strings.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
llvm-strings - print strings

* SYNOPSIS
*llvm-strings* [/options/] [/input.../]

* DESCRIPTION
*llvm-strings* is a tool intended as a drop-in replacement for GNU's
*strings*, which looks for printable strings in files and writes them to
the standard output stream. A printable string is any sequence of four
(by default) or more printable ASCII characters. The end of the file, or
any other byte, terminates the current sequence.

*llvm-strings* looks for strings in each *input* file specified. Unlike
GNU *strings* it looks in the entire input file, regardless of file
format, rather than restricting the search to certain sections of object
files. If "*-*" is specified as an *input*, or no *input* is specified,
the program reads from the standard input stream.

* EXAMPLE

#+begin_quote

  #+begin_quote
    #+begin_example
      $ cat input.txt
      bars
      foo
      wibble blob
      $ llvm-strings input.txt
      bars
      wibble blob
    #+end_example
  #+end_quote
#+end_quote

* OPTIONS

#+begin_quote
  - *--all, -a* :: Silently ignored. Present for GNU *strings*
    compatibility.
#+end_quote

#+begin_quote
  - *--bytes=<length>, -n* :: Set the minimum number of printable ASCII
    characters required for a sequence of bytes to be considered a
    string. The default value is 4.
#+end_quote

#+begin_quote
  - *--help, -h* :: Display a summary of command line options.
#+end_quote

#+begin_quote
  - *--print-file-name, -f* :: Display the name of the containing file
    before each string.

  Example:

  #+begin_quote

    #+begin_quote
      #+begin_example
        $ llvm-strings --print-file-name test.o test.elf
        test.o: _Z5hellov
        test.o: some_bss
        test.o: test.cpp
        test.o: main
        test.elf: test.cpp
        test.elf: test2.cpp
        test.elf: _Z5hellov
        test.elf: main
        test.elf: some_bss
      #+end_example
    #+end_quote
  #+end_quote
#+end_quote

#+begin_quote
  - *--radix=<radix>, -t* :: Display the offset within the file of each
    string, before the string and using the specified radix. Valid
    *<radix>* values are *o*, *d* and *x* for octal, decimal and
    hexadecimal respectively.

  Example:

  #+begin_quote

    #+begin_quote
      #+begin_example
        $ llvm-strings --radix=o test.o
            1054 _Z5hellov
            1066 .rela.text
            1101 .comment
            1112 some_bss
            1123 .bss
            1130 test.cpp
            1141 main
        $ llvm-strings --radix=d test.o
            556 _Z5hellov
            566 .rela.text
            577 .comment
            586 some_bss
            595 .bss
            600 test.cpp
            609 main
        $ llvm-strings -t x test.o
            22c _Z5hellov
            236 .rela.text
            241 .comment
            24a some_bss
            253 .bss
            258 test.cpp
            261 main
      #+end_example
    #+end_quote
  #+end_quote
#+end_quote

#+begin_quote
  - *--version* :: Display the version of the *llvm-strings* executable.
#+end_quote

#+begin_quote
  - *@<FILE>* :: Read command-line options from response file *<FILE>*.
#+end_quote

* EXIT STATUS
*llvm-strings* exits with a non-zero exit code if there is an error.
Otherwise, it exits with code 0.

* BUGS
To report bugs, please visit </https://bugs.llvm.org//>.

* AUTHOR
Maintained by the LLVM Team (https://llvm.org/).

* COPYRIGHT
2003-2021, LLVM Project
