#+TITLE: Man1 - storeutl.1ssl
#+DESCRIPTION: Linux manpage for storeutl.1ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
openssl-storeutl, storeutl - STORE utility

* SYNOPSIS
*openssl* *storeutl* [*-help*] [*-out file*] [*-noout*] [*-passin arg*]
[*-text arg*] [*-engine id*] [*-r*] [*-certs*] [*-keys*] [*-crls*]
[*-subject arg*] [*-issuer arg*] [*-serial arg*] [*-alias arg*]
[*-fingerprint arg*] [*-/digest/*] *uri* ...

* DESCRIPTION
The *storeutl* command can be used to display the contents (after
decryption as the case may be) fetched from the given URIs.

* OPTIONS
- -help :: Print out a usage message.

- -out filename :: specifies the output filename to write to or standard
  output by default.

- -noout :: this option prevents output of the PEM data.

- -passin arg :: the key password source. For more information about the
  format of *arg* see Pass Phrase Options in *openssl* (1).

- -text :: Prints out the objects in text form, similarly to the *-text*
  output from *openssl x509*, *openssl pkey*, etc.

- -engine id :: specifying an engine (by its unique *id* string) will
  cause *storeutl* to attempt to obtain a functional reference to the
  specified engine, thus initialising it if needed. The engine will then
  be set as the default for all available algorithms.

- -r :: Fetch objects recursively when possible.

- -certs :: 

- -keys :: 

- -crls :: 

Only select the certificates, keys or CRLs from the given URI. However,
if this URI would return a set of names (URIs), those are always
returned.

- -subject arg :: Search for an object having the subject name *arg*.
  The arg must be formatted as //type0=value0/type1=value1/type2=.../.
  Keyword characters may be escaped by \ (backslash), and whitespace is
  retained. Empty values are permitted but are ignored for the search.
  That is, a search with an empty value will have the same effect as not
  specifying the type at all.

- -issuer arg :: 

- -serial arg :: 

Search for an object having the given issuer name and serial number.
These two options /must/ be used together. The issuer arg must be
formatted as //type0=value0/type1=value1/type2=.../, characters may be
escaped by \ (backslash), no spaces are skipped. The serial arg may be
specified as a decimal value or a hex value if preceded by *0x*.

- -alias arg :: Search for an object having the given alias.

- -fingerprint arg :: Search for an object having the given fingerprint.

- -digest :: The digest that was used to compute the fingerprint given
  with *-fingerprint*.

* SEE ALSO
*openssl* (1)

* HISTORY
The *openssl* *storeutl* app was added in OpenSSL 1.1.1.

* COPYRIGHT
Copyright 2016-2021 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
