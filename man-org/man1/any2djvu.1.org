#+TITLE: Man1 - any2djvu.1
#+DESCRIPTION: Linux manpage for any2djvu.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
any2djvu - Convert .ps/.ps.gz/.pdf to .djvu

* SYNOPSIS
*any2djvu* /url {filename(s)}/

* DESCRIPTION
Converts files from .ps/.ps.gz/.pdf to .djvu by running them through a
web server willing to perform this task.

Invoke with -h switch for usage information.

* ENVIRONMENT
Non-empty value of DJVU_ONLINE_ACK acknowledges transmission of the
documents to the server (so that no warning dialog is displayed).

* EXAMPLES
any2djvu http://barak.pearlmutter.net/papers mesh-preprint.ps.gz

any2djvu localfile.pdf

* AUTHORS
David Kreil, Barak A. Pearlmutter, Yaroslav O. Halchenko

* BUGS
Using a web-based encoder server is a stop-gap measure until better
encoders enjoy wide free distribution.

There is a security issue in operating on documents not intended for
widespread distribution, which could be partially although not
completely ameliorated by using a secure web connection.

* SEE ALSO
The entire djvu suite, eg djvu(1), djview(1), and djvuserver(1).
