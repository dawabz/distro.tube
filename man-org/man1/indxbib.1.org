#+TITLE: Man1 - indxbib.1
#+DESCRIPTION: Linux manpage for indxbib.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
indxbib - make inverted index for bibliographic databases

* SYNOPSIS
*indxbib* [ *-w* ] [ *-c* file ] [ *-d* dir ] [ *-f* file ] [ *-h* n ] [
*-i* string ] [ *-k* n ] [ *-l* n ] [ *-n* n ] [ *-o* file ] [ *-t* n ]
[/filename/ . . .] *indxbib* *--help* *indxbib* *-v* *indxbib*
*--version*

* DESCRIPTION
*indxbib* makes an inverted index for the bibliographic databases in
/filename/ . . . for use with *refer*(1), *lookbib*(1), and *lkbib*(1).
The index will be named filename/.i/; the index is written to a
temporary file which is then renamed to this. If no filenames are given
on the command line because the *-f* option has been used, and no *-o*
option is given, the index will be named /Ind.i/.

Bibliographic databases are divided into records by blank lines. Within
a record, each fields starts with a *%* character at the beginning of a
line. Fields have a one letter name which follows the *%* character.

The values set by the *-c*, *-n*, *-l*, and *-t* options are stored in
the index; when the index is searched, keys will be discarded and
truncated in a manner appropriate to these options; the original keys
will be used for verifying that any record found using the index
actually contains the keys. This means that a user of an index need not
know whether these options were used in the creation of the index,
provided that not all the keys to be searched for would have been
discarded during indexing and that the user supplies at least the part
of each key that would have remained after being truncated during
indexing. The value set by the *-i* option is also stored in the index
and will be used in verifying records found using the index.

* OPTIONS
Whitespace is permitted between a command-line option and its argument.

- *-v* :: Print the version number.

- *-w* :: Index whole files. Each file is a separate record.

- *-c*/file/ :: Read the list of common words from /file/ instead of
  //usr/share/groff/1.22.4/eign/.

- *-d*/dir/ :: Use /dir/ as the pathname of the current working
  directory to store in the index, instead of the path printed by
  *pwd*(1). Usually /dir/ will be a symbolic link that points to the
  directory printed by *pwd*(1).

- *-f*/file/ :: Read the files to be indexed from /file/. If /file/ is
  *-*, files will be read from the standard input. The *-f* option can
  be given at most once.

- *-i*/string/ :: Don't index the contents of fields whose names are in
  /string/. Initially /string/ is *XYZ*.

- *-h*/n/ :: Use the first prime greater than or equal to /n/ for the
  size of the hash table. Larger values of /n/ will usually make
  searching faster, but will make the index larger and *indxbib* use
  more memory. Initially /n/ is 997.

- *-k*/n/ :: Use at most /n/ keys per input record. Initially /n/
  is 100.

- *-l*/n/ :: Discard keys that are shorter than /n/. Initially /n/ is 3.

- *-n*/n/ :: Discard the /n/ most common words. Initially /n/ is 100.

- *-o*/basename/ :: The index should be named basename/.i/.

- *-t*/n/ :: Truncate keys to /n/. Initially /n/ is 6.

* FILES
- filename/.i/ :: Index.

- /Ind.i/ :: Default index name.

- //usr/share/groff/1.22.4/eign/ :: List of common words.

- /indxbib/XXXXXX :: Temporary file.

* SEE ALSO
*refer*(1), *lkbib*(1), *lookbib*(1)
