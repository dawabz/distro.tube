#+TITLE: Manpages - systemd-firstboot.service.1
#+DESCRIPTION: Linux manpage for systemd-firstboot.service.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about systemd-firstboot.service.1 is found in manpage for: [[../systemd-firstboot.1][systemd-firstboot.1]]