#+TITLE: Man1 - llvm-cxxfilt.1
#+DESCRIPTION: Linux manpage for llvm-cxxfilt.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
llvm-cxxfilt - LLVM symbol name demangler

* SYNOPSIS
*llvm-cxxfilt* [/options/] [/mangled names.../]

* DESCRIPTION
*llvm-cxxfilt* is a symbol demangler that can be used as a replacement
for the GNU *c++filt* tool. It takes a series of symbol names and prints
their demangled form on the standard output stream. If a name cannot be
demangled, it is simply printed as is.

If no names are specified on the command-line, names are read
interactively from the standard input stream. When reading names from
standard input, each input line is split on characters that are not part
of valid Itanium name manglings, i.e. characters that are not
alphanumeric, '.', '$', or '_'. Separators between names are copied to
the output as is.

* EXAMPLE

#+begin_quote

  #+begin_quote
    #+begin_example
      $ llvm-cxxfilt _Z3foov _Z3bari not_mangled
      foo()
      bar(int)
      not_mangled
      $ cat input.txt
      | _Z3foov *** _Z3bari *** not_mangled |
      $ llvm-cxxfilt < input.txt
      | foo() *** bar(int) *** not_mangled |
    #+end_example
  #+end_quote
#+end_quote

* OPTIONS

#+begin_quote
  - *--format=<value>, -s* :: Mangling scheme to assume. Valid values
    are *auto* (default, auto-detect the style) and *gnu* (assume
    GNU/Itanium style).
#+end_quote

#+begin_quote
  - *--help, -h* :: Print a summary of command line options.
#+end_quote

#+begin_quote
  - *--no-strip-underscore, -n* :: Do not strip a leading underscore.
    This is the default for all platforms except Mach-O based hosts.
#+end_quote

#+begin_quote
  - *--strip-underscore, -_* :: Strip a single leading underscore, if
    present, from each input name before demangling. On by default on
    Mach-O based platforms.
#+end_quote

#+begin_quote
  - *--types, -t* :: Attempt to demangle names as type names as well as
    function names.
#+end_quote

#+begin_quote
  - *--version* :: Display the version of the *llvm-cxxfilt* executable.
#+end_quote

#+begin_quote
  - *@<FILE>* :: Read command-line options from response file /<FILE>/.
#+end_quote

* EXIT STATUS
*llvm-cxxfilt* returns 0 unless it encounters a usage error, in which
case a non-zero exit code is returned.

* SEE ALSO
*llvm-nm(1)*

* AUTHOR
Maintained by the LLVM Team (https://llvm.org/).

* COPYRIGHT
2003-2021, LLVM Project
