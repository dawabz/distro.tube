#+TITLE: Man1 - i3-sensible-terminal.1
#+DESCRIPTION: Linux manpage for i3-sensible-terminal.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
i3-sensible-terminal - launches $TERMINAL with fallbacks

* SYNOPSIS
i3-sensible-terminal [arguments]

* DESCRIPTION
i3-sensible-terminal is invoked in the i3 default config to start a
terminal. This wrapper script is necessary since there is no
distribution-independent terminal launcher (but for example Debian has
x-terminal-emulator). Distribution packagers are responsible for
shipping this script in a way which is appropriate for the distribution.

It tries to start one of the following (in that order):

#+begin_quote
  ·

  $TERMINAL (this is a non-standard variable)
#+end_quote

#+begin_quote
  ·

  x-terminal-emulator (only present on Debian and derivatives)
#+end_quote

#+begin_quote
  ·

  mate-terminal
#+end_quote

#+begin_quote
  ·

  gnome-terminal
#+end_quote

#+begin_quote
  ·

  terminator
#+end_quote

#+begin_quote
  ·

  xfce4-terminal
#+end_quote

#+begin_quote
  ·

  urxvt
#+end_quote

#+begin_quote
  ·

  rxvt
#+end_quote

#+begin_quote
  ·

  termit
#+end_quote

#+begin_quote
  ·

  Eterm
#+end_quote

#+begin_quote
  ·

  aterm
#+end_quote

#+begin_quote
  ·

  uxterm
#+end_quote

#+begin_quote
  ·

  xterm
#+end_quote

#+begin_quote
  ·

  roxterm
#+end_quote

#+begin_quote
  ·

  termite
#+end_quote

#+begin_quote
  ·

  lxterminal
#+end_quote

#+begin_quote
  ·

  terminology
#+end_quote

#+begin_quote
  ·

  st
#+end_quote

#+begin_quote
  ·

  qterminal
#+end_quote

#+begin_quote
  ·

  lilyterm
#+end_quote

#+begin_quote
  ·

  tilix
#+end_quote

#+begin_quote
  ·

  terminix
#+end_quote

#+begin_quote
  ·

  konsole
#+end_quote

#+begin_quote
  ·

  kitty
#+end_quote

#+begin_quote
  ·

  guake
#+end_quote

#+begin_quote
  ·

  tilda
#+end_quote

#+begin_quote
  ·

  alacritty
#+end_quote

#+begin_quote
  ·

  hyper
#+end_quote

Please don't complain about the order: If the user has any preference,
they will have $TERMINAL set or modified their i3 configuration file.

* SEE ALSO
i3(1)

* AUTHOR
Michael Stapelberg and contributors
