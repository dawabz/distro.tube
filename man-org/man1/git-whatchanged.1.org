#+TITLE: Man1 - git-whatchanged.1
#+DESCRIPTION: Linux manpage for git-whatchanged.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
git-whatchanged - Show logs with difference each commit introduces

* SYNOPSIS
#+begin_example
  git whatchanged <option>...
#+end_example

* DESCRIPTION
Shows commit logs and diff output each commit introduces.

New users are encouraged to use *git-log*(1) instead. The *whatchanged*
command is essentially the same as *git-log*(1) but defaults to show the
raw format diff output and to skip merges.

The command is kept primarily for historical reasons; fingers of many
people who learned Git long before *git log* was invented by reading
Linux kernel mailing list are trained to type it.

* EXAMPLES
*git whatchanged -p v2.6.12.. include/scsi drivers/scsi*

#+begin_quote
  Show as patches the commits since version /v2.6.12/ that changed any
  file in the include/scsi or drivers/scsi subdirectories
#+end_quote

*git whatchanged --since="2 weeks ago" -- gitk*

#+begin_quote
  Show the changes during the last two weeks to the file /gitk/. The
  "--" is necessary to avoid confusion with the *branch* named /gitk/
#+end_quote

* GIT
Part of the *git*(1) suite
