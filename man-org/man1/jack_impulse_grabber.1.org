#+TITLE: Man1 - jack_impulse_grabber.1
#+DESCRIPTION: Linux manpage for jack_impulse_grabber.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
jack_impulse_grabber - JACK toolkit client to grab an impulse (response)

* SYNOPSIS
*jack_impulse_grabber* *-d* /duration/ [/-f/ (C|gnuplot)]

* DESCRIPTION
*jack_impulse_grabber* is a JACK example client for collecting impulses
recordings from JACK ports.
