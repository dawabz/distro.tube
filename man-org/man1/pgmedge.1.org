#+TITLE: Man1 - pgmedge.1
#+DESCRIPTION: Linux manpage for pgmedge.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
*pgmedge* - replaced by pamedge

* DESCRIPTION
This program is part of *Netpbm*(1)

*pgmedge* was replaced in Netpbm 10.14 (March 2002) by *pamedge*(1)

*pamedge* is backward compatible with *pgmedge*, but works on color
images too.
