#+TITLE: Man1 - G
#+DESCRIPTION: Man1 - G
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* G
#+begin_src bash :exports results
readarray -t starts_with_g < <(find . -type f -iname "g*" | sort)

for x in "${starts_with_g[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./g++.1.org][g++.1]]                          |
| [[file:./g3topbm.1.org][g3topbm.1]]                      |
| [[file:./gamma4scanimage.1.org][gamma4scanimage.1]]              |
| [[file:./gapplication.1.org][gapplication.1]]                 |
| [[file:./gawk.1.org][gawk.1]]                         |
| [[file:./gc.1.org][gc.1]]                           |
| [[file:./gcc.1.org][gcc.1]]                          |
| [[file:./gconftool-2.1.org][gconftool-2.1]]                  |
| [[file:./gcov.1.org][gcov.1]]                         |
| [[file:./gcov-dump.1.org][gcov-dump.1]]                    |
| [[file:./gcov-tool.1.org][gcov-tool.1]]                    |
| [[file:./gdbm_dump.1.org][gdbm_dump.1]]                    |
| [[file:./gdbm_load.1.org][gdbm_load.1]]                    |
| [[file:./gdbmtool.1.org][gdbmtool.1]]                     |
| [[file:./gdbus.1.org][gdbus.1]]                        |
| [[file:./gdiffmk.1.org][gdiffmk.1]]                      |
| [[file:./gdk-pixbuf-csource.1.org][gdk-pixbuf-csource.1]]           |
| [[file:./gdk-pixbuf-query-loaders.1.org][gdk-pixbuf-query-loaders.1]]     |
| [[file:./gemtopbm.1.org][gemtopbm.1]]                     |
| [[file:./gemtopnm.1.org][gemtopnm.1]]                     |
| [[file:./genbrk.1.org][genbrk.1]]                       |
| [[file:./gencfu.1.org][gencfu.1]]                       |
| [[file:./gencnval.1.org][gencnval.1]]                     |
| [[file:./gendict.1.org][gendict.1]]                      |
| [[file:./gendsa.1ssl.org][gendsa.1ssl]]                    |
| [[file:./genpkey.1ssl.org][genpkey.1ssl]]                   |
| [[file:./genrb.1.org][genrb.1]]                        |
| [[file:./genrsa.1ssl.org][genrsa.1ssl]]                    |
| [[file:./geoiplookup.1.org][geoiplookup.1]]                  |
| [[file:./geoiplookup6.1.org][geoiplookup6.1]]                 |
| [[file:./GET.1p.org][GET.1p]]                         |
| [[file:./getcifsacl.1.org][getcifsacl.1]]                   |
| [[file:./getent.1.org][getent.1]]                       |
| [[file:./getfacl.1.org][getfacl.1]]                      |
| [[file:./getfattr.1.org][getfattr.1]]                     |
| [[file:./gethostip.1.org][gethostip.1]]                    |
| [[file:./getopt.1.org][getopt.1]]                       |
| [[file:./gettext.1.org][gettext.1]]                      |
| [[file:./gettextize.1.org][gettextize.1]]                   |
| [[file:./gftodvi.1.org][gftodvi.1]]                      |
| [[file:./gftopk.1.org][gftopk.1]]                       |
| [[file:./gftype.1.org][gftype.1]]                       |
| [[file:./ghc.1.org][ghc.1]]                          |
| [[file:./gif2rgb.1.org][gif2rgb.1]]                      |
| [[file:./gif2webp.1.org][gif2webp.1]]                     |
| [[file:./gifbg.1.org][gifbg.1]]                        |
| [[file:./gifbuild.1.org][gifbuild.1]]                     |
| [[file:./gifclrmp.1.org][gifclrmp.1]]                     |
| [[file:./gifcolor.1.org][gifcolor.1]]                     |
| [[file:./gifecho.1.org][gifecho.1]]                      |
| [[file:./giffix.1.org][giffix.1]]                       |
| [[file:./gifhisto.1.org][gifhisto.1]]                     |
| [[file:./gifinto.1.org][gifinto.1]]                      |
| [[file:./giflib.1.org][giflib.1]]                       |
| [[file:./giftext.1.org][giftext.1]]                      |
| [[file:./giftool.1.org][giftool.1]]                      |
| [[file:./giftopnm.1.org][giftopnm.1]]                     |
| [[file:./gifwedge.1.org][gifwedge.1]]                     |
| [[file:./gig2mono.1.org][gig2mono.1]]                     |
| [[file:./gig2stereo.1.org][gig2stereo.1]]                   |
| [[file:./gigdump.1.org][gigdump.1]]                      |
| [[file:./gigextract.1.org][gigextract.1]]                   |
| [[file:./gigmerge.1.org][gigmerge.1]]                     |
| [[file:./gimp-2.10.1.org][gimp-2.10.1]]                    |
| [[file:./gimptool-2.0.1.org][gimptool-2.0.1]]                 |
| [[file:./gio.1.org][gio.1]]                          |
| [[file:./gio-querymodules.1.org][gio-querymodules.1]]             |
| [[file:./g-ir-compiler.1.org][g-ir-compiler.1]]                |
| [[file:./g-ir-doc-tool.1.org][g-ir-doc-tool.1]]                |
| [[file:./g-ir-generate.1.org][g-ir-generate.1]]                |
| [[file:./g-ir-scanner.1.org][g-ir-scanner.1]]                 |
| [[file:./git.1.org][git.1]]                          |
| [[file:./git-add.1.org][git-add.1]]                      |
| [[file:./git-am.1.org][git-am.1]]                       |
| [[file:./git-annotate.1.org][git-annotate.1]]                 |
| [[file:./git-apply.1.org][git-apply.1]]                    |
| [[file:./git-archimport.1.org][git-archimport.1]]               |
| [[file:./git-archive.1.org][git-archive.1]]                  |
| [[file:./git-bisect.1.org][git-bisect.1]]                   |
| [[file:./git-blame.1.org][git-blame.1]]                    |
| [[file:./git-branch.1.org][git-branch.1]]                   |
| [[file:./git-bugreport.1.org][git-bugreport.1]]                |
| [[file:./git-bundle.1.org][git-bundle.1]]                   |
| [[file:./git-cat-file.1.org][git-cat-file.1]]                 |
| [[file:./git-check-attr.1.org][git-check-attr.1]]               |
| [[file:./git-check-ignore.1.org][git-check-ignore.1]]             |
| [[file:./git-check-mailmap.1.org][git-check-mailmap.1]]            |
| [[file:./git-checkout.1.org][git-checkout.1]]                 |
| [[file:./git-checkout-index.1.org][git-checkout-index.1]]           |
| [[file:./git-check-ref-format.1.org][git-check-ref-format.1]]         |
| [[file:./git-cherry.1.org][git-cherry.1]]                   |
| [[file:./git-cherry-pick.1.org][git-cherry-pick.1]]              |
| [[file:./git-citool.1.org][git-citool.1]]                   |
| [[file:./git-clean.1.org][git-clean.1]]                    |
| [[file:./git-clone.1.org][git-clone.1]]                    |
| [[file:./git-column.1.org][git-column.1]]                   |
| [[file:./git-commit.1.org][git-commit.1]]                   |
| [[file:./git-commit-graph.1.org][git-commit-graph.1]]             |
| [[file:./git-commit-tree.1.org][git-commit-tree.1]]              |
| [[file:./git-config.1.org][git-config.1]]                   |
| [[file:./git-count-objects.1.org][git-count-objects.1]]            |
| [[file:./git-credential.1.org][git-credential.1]]               |
| [[file:./git-credential-cache.1.org][git-credential-cache.1]]         |
| [[file:./git-credential-cache--daemon.1.org][git-credential-cache--daemon.1]] |
| [[file:./git-credential-store.1.org][git-credential-store.1]]         |
| [[file:./git-cvsexportcommit.1.org][git-cvsexportcommit.1]]          |
| [[file:./git-cvsimport.1.org][git-cvsimport.1]]                |
| [[file:./git-cvsserver.1.org][git-cvsserver.1]]                |
| [[file:./git-daemon.1.org][git-daemon.1]]                   |
| [[file:./git-describe.1.org][git-describe.1]]                 |
| [[file:./git-diff.1.org][git-diff.1]]                     |
| [[file:./git-diff-files.1.org][git-diff-files.1]]               |
| [[file:./git-diff-index.1.org][git-diff-index.1]]               |
| [[file:./git-difftool.1.org][git-difftool.1]]                 |
| [[file:./git-diff-tree.1.org][git-diff-tree.1]]                |
| [[file:./git-fast-export.1.org][git-fast-export.1]]              |
| [[file:./git-fast-import.1.org][git-fast-import.1]]              |
| [[file:./git-fetch.1.org][git-fetch.1]]                    |
| [[file:./git-fetch-pack.1.org][git-fetch-pack.1]]               |
| [[file:./git-filter-branch.1.org][git-filter-branch.1]]            |
| [[file:./git-fmt-merge-msg.1.org][git-fmt-merge-msg.1]]            |
| [[file:./git-for-each-ref.1.org][git-for-each-ref.1]]             |
| [[file:./git-for-each-repo.1.org][git-for-each-repo.1]]            |
| [[file:./git-format-patch.1.org][git-format-patch.1]]             |
| [[file:./git-fsck.1.org][git-fsck.1]]                     |
| [[file:./git-fsck-objects.1.org][git-fsck-objects.1]]             |
| [[file:./git-gc.1.org][git-gc.1]]                       |
| [[file:./git-get-tar-commit-id.1.org][git-get-tar-commit-id.1]]        |
| [[file:./git-grep.1.org][git-grep.1]]                     |
| [[file:./git-gui.1.org][git-gui.1]]                      |
| [[file:./git-hash-object.1.org][git-hash-object.1]]              |
| [[file:./git-help.1.org][git-help.1]]                     |
| [[file:./git-http-backend.1.org][git-http-backend.1]]             |
| [[file:./git-http-fetch.1.org][git-http-fetch.1]]               |
| [[file:./git-http-push.1.org][git-http-push.1]]                |
| [[file:./git-imap-send.1.org][git-imap-send.1]]                |
| [[file:./git-index-pack.1.org][git-index-pack.1]]               |
| [[file:./git-init.1.org][git-init.1]]                     |
| [[file:./git-init-db.1.org][git-init-db.1]]                  |
| [[file:./git-instaweb.1.org][git-instaweb.1]]                 |
| [[file:./git-interpret-trailers.1.org][git-interpret-trailers.1]]       |
| [[file:./gitk.1.org][gitk.1]]                         |
| [[file:./git-log.1.org][git-log.1]]                      |
| [[file:./git-ls-files.1.org][git-ls-files.1]]                 |
| [[file:./git-ls-remote.1.org][git-ls-remote.1]]                |
| [[file:./git-ls-tree.1.org][git-ls-tree.1]]                  |
| [[file:./git-mailinfo.1.org][git-mailinfo.1]]                 |
| [[file:./git-mailsplit.1.org][git-mailsplit.1]]                |
| [[file:./git-maintenance.1.org][git-maintenance.1]]              |
| [[file:./git-merge.1.org][git-merge.1]]                    |
| [[file:./git-merge-base.1.org][git-merge-base.1]]               |
| [[file:./git-merge-file.1.org][git-merge-file.1]]               |
| [[file:./git-merge-index.1.org][git-merge-index.1]]              |
| [[file:./git-merge-one-file.1.org][git-merge-one-file.1]]           |
| [[file:./git-mergetool.1.org][git-mergetool.1]]                |
| [[file:./git-mergetool--lib.1.org][git-mergetool--lib.1]]           |
| [[file:./git-merge-tree.1.org][git-merge-tree.1]]               |
| [[file:./git-mktag.1.org][git-mktag.1]]                    |
| [[file:./git-mktree.1.org][git-mktree.1]]                   |
| [[file:./git-multi-pack-index.1.org][git-multi-pack-index.1]]         |
| [[file:./git-mv.1.org][git-mv.1]]                       |
| [[file:./git-name-rev.1.org][git-name-rev.1]]                 |
| [[file:./git-notes.1.org][git-notes.1]]                    |
| [[file:./git-p4.1.org][git-p4.1]]                       |
| [[file:./git-pack-objects.1.org][git-pack-objects.1]]             |
| [[file:./git-pack-redundant.1.org][git-pack-redundant.1]]           |
| [[file:./git-pack-refs.1.org][git-pack-refs.1]]                |
| [[file:./git-patch-id.1.org][git-patch-id.1]]                 |
| [[file:./git-prune.1.org][git-prune.1]]                    |
| [[file:./git-prune-packed.1.org][git-prune-packed.1]]             |
| [[file:./git-pull.1.org][git-pull.1]]                     |
| [[file:./git-push.1.org][git-push.1]]                     |
| [[file:./git-quiltimport.1.org][git-quiltimport.1]]              |
| [[file:./git-range-diff.1.org][git-range-diff.1]]               |
| [[file:./git-read-tree.1.org][git-read-tree.1]]                |
| [[file:./git-rebase.1.org][git-rebase.1]]                   |
| [[file:./git-receive-pack.1.org][git-receive-pack.1]]             |
| [[file:./git-reflog.1.org][git-reflog.1]]                   |
| [[file:./git-remote.1.org][git-remote.1]]                   |
| [[file:./git-remote-bzr.1.org][git-remote-bzr.1]]               |
| [[file:./git-remote-ext.1.org][git-remote-ext.1]]               |
| [[file:./git-remote-fd.1.org][git-remote-fd.1]]                |
| [[file:./git-repack.1.org][git-repack.1]]                   |
| [[file:./git-replace.1.org][git-replace.1]]                  |
| [[file:./git-request-pull.1.org][git-request-pull.1]]             |
| [[file:./git-rerere.1.org][git-rerere.1]]                   |
| [[file:./git-reset.1.org][git-reset.1]]                    |
| [[file:./git-restore.1.org][git-restore.1]]                  |
| [[file:./git-revert.1.org][git-revert.1]]                   |
| [[file:./git-rev-list.1.org][git-rev-list.1]]                 |
| [[file:./git-rev-parse.1.org][git-rev-parse.1]]                |
| [[file:./git-rm.1.org][git-rm.1]]                       |
| [[file:./git-send-email.1.org][git-send-email.1]]               |
| [[file:./git-send-pack.1.org][git-send-pack.1]]                |
| [[file:./git-shell.1.org][git-shell.1]]                    |
| [[file:./git-sh-i18n.1.org][git-sh-i18n.1]]                  |
| [[file:./git-sh-i18n--envsubst.1.org][git-sh-i18n--envsubst.1]]        |
| [[file:./git-shortlog.1.org][git-shortlog.1]]                 |
| [[file:./git-show.1.org][git-show.1]]                     |
| [[file:./git-show-branch.1.org][git-show-branch.1]]              |
| [[file:./git-show-index.1.org][git-show-index.1]]               |
| [[file:./git-show-ref.1.org][git-show-ref.1]]                 |
| [[file:./git-sh-setup.1.org][git-sh-setup.1]]                 |
| [[file:./git-sparse-checkout.1.org][git-sparse-checkout.1]]          |
| [[file:./git-stage.1.org][git-stage.1]]                    |
| [[file:./git-stash.1.org][git-stash.1]]                    |
| [[file:./git-status.1.org][git-status.1]]                   |
| [[file:./git-stripspace.1.org][git-stripspace.1]]               |
| [[file:./git-submodule.1.org][git-submodule.1]]                |
| [[file:./git-subtree.1.org][git-subtree.1]]                  |
| [[file:./git-svn.1.org][git-svn.1]]                      |
| [[file:./git-switch.1.org][git-switch.1]]                   |
| [[file:./git-symbolic-ref.1.org][git-symbolic-ref.1]]             |
| [[file:./git-tag.1.org][git-tag.1]]                      |
| [[file:./git-unpack-file.1.org][git-unpack-file.1]]              |
| [[file:./git-unpack-objects.1.org][git-unpack-objects.1]]           |
| [[file:./git-update-index.1.org][git-update-index.1]]             |
| [[file:./git-update-ref.1.org][git-update-ref.1]]               |
| [[file:./git-update-server-info.1.org][git-update-server-info.1]]       |
| [[file:./git-upload-archive.1.org][git-upload-archive.1]]           |
| [[file:./git-upload-pack.1.org][git-upload-pack.1]]              |
| [[file:./git-var.1.org][git-var.1]]                      |
| [[file:./git-verify-commit.1.org][git-verify-commit.1]]            |
| [[file:./git-verify-pack.1.org][git-verify-pack.1]]              |
| [[file:./git-verify-tag.1.org][git-verify-tag.1]]               |
| [[file:./git-version.1.org][git-version.1]]                  |
| [[file:./gitweb.1.org][gitweb.1]]                       |
| [[file:./git-web--browse.1.org][git-web--browse.1]]              |
| [[file:./git-whatchanged.1.org][git-whatchanged.1]]              |
| [[file:./git-worktree.1.org][git-worktree.1]]                 |
| [[file:./git-write-tree.1.org][git-write-tree.1]]               |
| [[file:./gksu.1.org][gksu.1]]                         |
| [[file:./gksu-properties.1.org][gksu-properties.1]]              |
| [[file:./glib-compile-resources.1.org][glib-compile-resources.1]]       |
| [[file:./glib-compile-schemas.1.org][glib-compile-schemas.1]]         |
| [[file:./glib-gettextize.1.org][glib-gettextize.1]]              |
| [[file:./glib-mkenums.1.org][glib-mkenums.1]]                 |
| [[file:./glilypond.1.org][glilypond.1]]                    |
| [[file:./glslc.1.org][glslc.1]]                        |
| [[file:./gm.1.org][gm.1]]                           |
| [[file:./gml2gv.1.org][gml2gv.1]]                       |
| [[file:./gmrun.1.org][gmrun.1]]                        |
| [[file:./gnome-disk-image-mounter.1.org][gnome-disk-image-mounter.1]]     |
| [[file:./gnome-disks.1.org][gnome-disks.1]]                  |
| [[file:./gnome-keyring.1.org][gnome-keyring.1]]                |
| [[file:./gnome-keyring-3.1.org][gnome-keyring-3.1]]              |
| [[file:./gnome-keyring-daemon.1.org][gnome-keyring-daemon.1]]         |
| [[file:./gnutls-cli.1.org][gnutls-cli.1]]                   |
| [[file:./gnutls-cli-debug.1.org][gnutls-cli-debug.1]]             |
| [[file:./gnutls-serv.1.org][gnutls-serv.1]]                  |
| [[file:./gobject-query.1.org][gobject-query.1]]                |
| [[file:./gouldtoppm.1.org][gouldtoppm.1]]                   |
| [[file:./gpasswd.1.org][gpasswd.1]]                      |
| [[file:./gperl.1.org][gperl.1]]                        |
| [[file:./gpg.1.org][gpg.1]]                          |
| [[file:./gpg-agent.1.org][gpg-agent.1]]                    |
| [[file:./gpg-check-pattern.1.org][gpg-check-pattern.1]]            |
| [[file:./gpgconf.1.org][gpgconf.1]]                      |
| [[file:./gpg-connect-agent.1.org][gpg-connect-agent.1]]            |
| [[file:./gpgparsemail.1.org][gpgparsemail.1]]                 |
| [[file:./gpg-preset-passphrase.1.org][gpg-preset-passphrase.1]]        |
| [[file:./gpgrt-config.1.org][gpgrt-config.1]]                 |
| [[file:./gpgsm.1.org][gpgsm.1]]                        |
| [[file:./gpgtar.1.org][gpgtar.1]]                       |
| [[file:./gpgv.1.org][gpgv.1]]                         |
| [[file:./gpg-wks-client.1.org][gpg-wks-client.1]]               |
| [[file:./gpg-wks-server.1.org][gpg-wks-server.1]]               |
| [[file:./gpinyin.1.org][gpinyin.1]]                      |
| [[file:./gpm-root.1.org][gpm-root.1]]                     |
| [[file:./gprof.1.org][gprof.1]]                        |
| [[file:./grab_color_image.1.org][grab_color_image.1]]             |
| [[file:./grab_gray_image.1.org][grab_gray_image.1]]              |
| [[file:./grab_partial_image.1.org][grab_partial_image.1]]           |
| [[file:./grap2graph.1.org][grap2graph.1]]                   |
| [[file:./GraphicsMagick++-config.1.org][GraphicsMagick++-config.1]]      |
| [[file:./GraphicsMagick-config.1.org][GraphicsMagick-config.1]]        |
| [[file:./GraphicsMagickWand-config.1.org][GraphicsMagickWand-config.1]]    |
| [[file:./graphml2gv.1.org][graphml2gv.1]]                   |
| [[file:./grep.1.org][grep.1]]                         |
| [[file:./gresource.1.org][gresource.1]]                    |
| [[file:./grn.1.org][grn.1]]                          |
| [[file:./grodvi.1.org][grodvi.1]]                       |
| [[file:./groff.1.org][groff.1]]                        |
| [[file:./groffer.1.org][groffer.1]]                      |
| [[file:./grog.1.org][grog.1]]                         |
| [[file:./grohtml.1.org][grohtml.1]]                      |
| [[file:./grolj4.1.org][grolj4.1]]                       |
| [[file:./gropdf.1.org][gropdf.1]]                       |
| [[file:./grops.1.org][grops.1]]                        |
| [[file:./grotty.1.org][grotty.1]]                       |
| [[file:./groups.1.org][groups.1]]                       |
| [[file:./growisofs.1.org][growisofs.1]]                    |
| [[file:./grub-customizer.1.org][grub-customizer.1]]              |
| [[file:./grub-editenv.1.org][grub-editenv.1]]                 |
| [[file:./grub-file.1.org][grub-file.1]]                    |
| [[file:./grub-fstest.1.org][grub-fstest.1]]                  |
| [[file:./grub-glue-efi.1.org][grub-glue-efi.1]]                |
| [[file:./grub-kbdcomp.1.org][grub-kbdcomp.1]]                 |
| [[file:./grub-menulst2cfg.1.org][grub-menulst2cfg.1]]             |
| [[file:./grub-mkfont.1.org][grub-mkfont.1]]                  |
| [[file:./grub-mkimage.1.org][grub-mkimage.1]]                 |
| [[file:./grub-mklayout.1.org][grub-mklayout.1]]                |
| [[file:./grub-mknetdir.1.org][grub-mknetdir.1]]                |
| [[file:./grub-mkpasswd-pbkdf2.1.org][grub-mkpasswd-pbkdf2.1]]         |
| [[file:./grub-mkrelpath.1.org][grub-mkrelpath.1]]               |
| [[file:./grub-mkrescue.1.org][grub-mkrescue.1]]                |
| [[file:./grub-mkstandalone.1.org][grub-mkstandalone.1]]            |
| [[file:./grub-mount.1.org][grub-mount.1]]                   |
| [[file:./grub-render-label.1.org][grub-render-label.1]]            |
| [[file:./grub-script-check.1.org][grub-script-check.1]]            |
| [[file:./grub-syslinux2cfg.1.org][grub-syslinux2cfg.1]]            |
| [[file:./gs.1.org][gs.1]]                           |
| [[file:./gsettings.1.org][gsettings.1]]                    |
| [[file:./gsettings-data-convert.1.org][gsettings-data-convert.1]]       |
| [[file:./gsettings-schema-convert.1.org][gsettings-schema-convert.1]]     |
| [[file:./gsf.1.org][gsf.1]]                          |
| [[file:./gsf-office-thumbnailer.1.org][gsf-office-thumbnailer.1]]       |
| [[file:./gsftopk.1.org][gsftopk.1]]                      |
| [[file:./gsf-vba-dump.1.org][gsf-vba-dump.1]]                 |
| [[file:./gsl-config.1.org][gsl-config.1]]                   |
| [[file:./gsl-histogram.1.org][gsl-histogram.1]]                |
| [[file:./gslp.1.org][gslp.1]]                         |
| [[file:./gsl-randist.1.org][gsl-randist.1]]                  |
| [[file:./gsnd.1.org][gsnd.1]]                         |
| [[file:./gst-device-monitor-1.0.1.org][gst-device-monitor-1.0.1]]       |
| [[file:./gst-discoverer-1.0.1.org][gst-discoverer-1.0.1]]           |
| [[file:./gst-inspect-1.0.1.org][gst-inspect-1.0.1]]              |
| [[file:./gst-launch-1.0.1.org][gst-launch-1.0.1]]               |
| [[file:./gst-play-1.0.1.org][gst-play-1.0.1]]                 |
| [[file:./gst-stats-1.0.1.org][gst-stats-1.0.1]]                |
| [[file:./gst-typefind-1.0.1.org][gst-typefind-1.0.1]]             |
| [[file:./gtester.1.org][gtester.1]]                      |
| [[file:./gtester-report.1.org][gtester-report.1]]               |
| [[file:./gtf.1.org][gtf.1]]                          |
| [[file:./gtk4-broadwayd.1.org][gtk4-broadwayd.1]]               |
| [[file:./gtk4-builder-tool.1.org][gtk4-builder-tool.1]]            |
| [[file:./gtk4-encode-symbolic-svg.1.org][gtk4-encode-symbolic-svg.1]]     |
| [[file:./gtk4-launch.1.org][gtk4-launch.1]]                  |
| [[file:./gtk4-query-settings.1.org][gtk4-query-settings.1]]          |
| [[file:./gtk4-update-icon-cache.1.org][gtk4-update-icon-cache.1]]       |
| [[file:./gtk-builder-tool.1.org][gtk-builder-tool.1]]             |
| [[file:./gtk-encode-symbolic-svg.1.org][gtk-encode-symbolic-svg.1]]      |
| [[file:./gtk-launch.1.org][gtk-launch.1]]                   |
| [[file:./gtk-query-immodules-3.0.1.org][gtk-query-immodules-3.0.1]]      |
| [[file:./gtk-query-settings.1.org][gtk-query-settings.1]]           |
| [[file:./gts2dxf.1.org][gts2dxf.1]]                      |
| [[file:./gts2oogl.1.org][gts2oogl.1]]                     |
| [[file:./gts2stl.1.org][gts2stl.1]]                      |
| [[file:./gtscheck.1.org][gtscheck.1]]                     |
| [[file:./gtscompare.1.org][gtscompare.1]]                   |
| [[file:./gts-config.1.org][gts-config.1]]                   |
| [[file:./gtstemplate.1.org][gtstemplate.1]]                  |
| [[file:./guile.1.org][guile.1]]                        |
| [[file:./gunzip.1.org][gunzip.1]]                       |
| [[file:./gupnp-binding-tool-1.2.1.org][gupnp-binding-tool-1.2.1]]       |
| [[file:./guvcview.1.org][guvcview.1]]                     |
| [[file:./gvcolor.1.org][gvcolor.1]]                      |
| [[file:./gvedit.1.org][gvedit.1]]                       |
| [[file:./gvfsd.1.org][gvfsd.1]]                        |
| [[file:./gvfsd-fuse.1.org][gvfsd-fuse.1]]                   |
| [[file:./gvfsd-metadata.1.org][gvfsd-metadata.1]]               |
| [[file:./gvgen.1.org][gvgen.1]]                        |
| [[file:./gvmap.1.org][gvmap.1]]                        |
| [[file:./gvmap.sh.1.org][gvmap.sh.1]]                     |
| [[file:./gvnccapture.1.org][gvnccapture.1]]                  |
| [[file:./gvpack.1.org][gvpack.1]]                       |
| [[file:./gvpr.1.org][gvpr.1]]                         |
| [[file:./gxditview.1.org][gxditview.1]]                    |
| [[file:./gxl2gv.1.org][gxl2gv.1]]                       |
| [[file:./gxmessage.1.org][gxmessage.1]]                    |
| [[file:./gzexe.1.org][gzexe.1]]                        |
| [[file:./gzip.1.org][gzip.1]]                         |
