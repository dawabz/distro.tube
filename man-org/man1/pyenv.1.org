#+TITLE: Man1 - pyenv.1
#+DESCRIPTION: Linux manpage for pyenv.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pyenv - Simple Python version management

* SYNOPSIS
*pyenv* /<command> /[/<args>/]

* DESCRIPTION
pyenv lets you easily switch between multiple versions of Python. It's
simple, unobtrusive, and follows the UNIX tradition of single-purpose
tools that do one thing well.

To start using pyenv

1. *Append* the following to *$HOME/.bashrc*

#+begin_quote
  source /usr/share/pyenv/pyenv_user_setup.bash
#+end_quote

#+begin_quote
  Appending this line enables shims. Please make sure this line is
  placed toward the end of the shell configuration file since it
  manipulates *PATH* during the initialization.

  - *Debian note:* :: Modify only your *~/.bashrc* file instead of
    creating\\
    *~/.bash_profile*

  #+begin_quote
    *Zsh note*: Modify your *~/.zshrc* file instead of *~/.bashrc*

    *Warning*: If you configured your system so that *BASH_ENV* variable
    points to *.bashrc*. You should almost certainly put the above
    mentioned line into *.bash_profile*, and *not* into *.bashrc*.
    Otherwise you may observe strange behaviour, such as *pyenv* getting
    into an infinite loop. See #264
    [[https://github.com/pyenv/pyenv/issues/264][]] for details.
  #+end_quote
#+end_quote

2. *Restart your shell so the path changes take effect.* You can now
   begin using pyenv.

#+begin_quote
  exec "$SHELL"
#+end_quote

3. *Install Python versions into $(pyenv root)/versions*. For example,
   to download and install Python 3.6.12, run:

#+begin_quote
  *pyenv install 3.6.12*
#+end_quote

*NOTE:* If you need to pass configure option to build, please use
*CONFIGURE_OPTS* environment variable. If you are having trouble
installing a python version, please visit the wiki page about Common
Build Problems
[[https://github.com/pyenv/pyenv/wiki/Common-build-problems][]]

*Proxy note*: If you use a proxy, export *HTTP_PROXY* and *HTTPS_PROXY*
environment variables.

** Stop using pyenv
The simplicity of pyenv makes it easy to temporarily disable it, or
uninstall from the system. To *disable* pyenv managing your Python
versions, simply remove the *pyenv init* line from your shell startup
configuration. This will remove pyenv shims directory from PATH, and
future invocations like *python* will execute the system Python version,
as before pyenv.

-  :: 

*pyenv* will still be accessible on the command line, but your Python
apps won't be affected by version switching.

-  :: 

* COMMAND LINE OPTIONS
Like *git*, the *pyenv* command delegates to subcommands based on its
first argument.

** Some useful pyenv commands are:
- *commands* :: List all available pyenv commands

- *exec* :: Run an executable with the selected Python version

- *global* :: Set or show the global Python version(s)

- *help* :: Display help for a command

- *hooks* :: List hook scripts for a given pyenv command

- *init* :: Configure the shell environment for pyenv

- *install* :: Install a Python version using python-build

- *local* :: Set or show the local application-specific Python
  version(s)

- *prefix* :: Display prefix for a Python version

- *rehash* :: Rehash pyenv shims (run this after installing executables)

- *root* :: Display the root directory where versions and shims are kept

- *shell* :: Set or show the shell-specific Python version

- *shims* :: List existing pyenv shims

- *uninstall* :: Uninstall a specific Python version

- *version* :: Show the current Python version(s) and its origin

- *version-file* :: Detect the file that sets the current pyenv version

- *version-name* :: Show the current Python version

- *version-origin* :: Explain how the current Python version is set

- *versions* :: List all Python versions available to pyenv

- *whence* :: List all Python versions that contain the given executable

- *which* :: Display the full path to an executable

See `pyenv help <command>' for information on a specific command. For
full documentation, see *COMMAND REFERENCE* section

* OPTIONS
- *-h, --help* :: Show summary of options.

- *-v, --version* :: Show version of program.

* COMPARISON
*"pyenv does . . .*

- Let you *change the global Python version* on a per-user basis.

- Provide support for *per-project Python versions*.

- Allow you to *override the Python version* with an environment
  variable.

- Search commands from *multiple versions of Python at a time*. This may
  be helpful to test across Python versions with tox

-  :: 

*In contrast with pythonbrew and pythonz, pyenv does not . . .*

- *Depend on Python itself.* pyenv was made from pure shell scripts.
  There is no bootstrap problem of Python.

- *Need to be loaded into your shell.* Instead, pyenv's shim approach
  works by adding a directory to your *$PATH*.

- *Manage virtualenv.* Of course, you can create virtualenv yourself, or
  pyenv-virtualenv to automate the process.

* How It Works
At a high level, pyenv intercepts Python commands using shim executables
injected into your *PATH*, determines which Python version has been
specified by your application, and passes your commands along to the
correct Python installation.

** Understanding PATH
When you run a command like *python* or *pip*, your operating system
searches through a list of directories to find an executable file with
that name. This list of directories lives in an environment variable
called *PATH*, with each directory in the list separated by a colon:

-  :: 

#+begin_example
  /usr/local/bin:/usr/bin:/bin
#+end_example

-  :: 

Directories in *PATH* are searched from left to right, so a matching
executable in a directory at the beginning of the list takes precedence
over another one at the end. In this example, the */usr/local/bin*
directory will be searched first, then */usr/bin*, then */bin*.

** Understanding Shims
pyenv works by inserting a directory of /shims/ at the front of your
*PATH*:

-  :: 

#+begin_example
  $(pyenv root)/shims:/usr/local/bin:/usr/bin:/bin
#+end_example

-  :: 

Through a process called /rehashing/, pyenv maintains shims in that
directory to match every Python command (*python*,*pip*,etc...) across
every installed version of Python

Shims are lightweight executables that simply pass your command along to
pyenv. So with pyenv installed, when you run, say, *pip*, your operating
system will do the following:

- Search your *PATH* for an executable file named *pip*

- Find the pyenv shim named *pip* at the beginning of your *PATH*

- Run the shim named *pip*, which in turn passes the command along to
  pyenv

-  :: 

** Choosing the Python Version
When you execute a shim, pyenv determines which Python version to use by
reading it from the following sources, in this order:

1. The *PYENV_VERSION* environment variable (if specified). You can use
   the *pyenv shell* command to set this environment variable in your
   current shell session.

2. The application-specific *.python-version* file in the current
   directory (if present). You can modify the current directory's
   *.python-version* file with the *pyenv local* command.

3. The first *.python-version* file found (if any) by searching each
   parent directory, until reaching the root of your filesystem.

4. The global *$(pyenv root)/version* file. You can modify this file
   using the *pyenv global* command. If the global version file is not
   present, pyenv assumes you want to use the "system" Python. (In other
   words, whatever version would run if pyenv weren't in your *PATH*.)

-  :: 

*NOTE:* You can activate multiple versions at the same time, including
multiple versions of Python2 or Python3 simultaneously. This allows for
parallel usage of Python2 and Python3, and is required with tools like
*tox*. For example, to set your path to first use your *system* Python
and Python3 (set to 2.7.9 and 3.4.2 in this example), but also have
Python 3.3.6, 3.2, and 2.5 available on your *PATH*, one would first
*pyenv install* the missing versions, then set *pyenv global system
3.3.6 3.2 2.5*. At this point, one should be able to find the full
executable path to each of these using *pyenv which*, e.g. *pyenv which
python2.5* (should display *$(pyenv root)/versions/2.5/bin/python2.5*),
or *pyenv which python3.4* (should display path to system Python3). You
can also specify multiple versions in a *.python-version* file,
separated by newlines or any whitespace. hy

** Locating the Python Installation
Once pyenv has determined which version of Python your application has
specified, it passes the command along to the corresponding Python
installation.

Each Python version is installed into its own directory under

#+begin_example
  $(pyenv root)/versions.
#+end_example

For example, you might have these versions installed:

- *$(pyenv root)/versions/2.7.8/*

- *$(pyenv root)/versions/3.4.2/*

- *$(pyenv root)/versions/pypy-2.4.0/*

-  :: 

As far as pyenv is concerned, version names are simply the directories
in *$(pyenv root)/versions*.

** Managing Virtual Environments
There is a pyenv plugin named pyenv-virtualenv which comes with various
features to help pyenv users to manage virtual environments created by
virtualenv or Anaconda. Because the *activate* script of those virtual
environments are relying on mutating *$PATH* variable of user's
interactive shell, it will intercept pyenv's shim style command
execution hooks. We'd recommend to install pyenv-virtualenv as well if
you have some plan to play with those virtual environments.

* Advanced Configuration
Skip this section unless you must know what every line in your shell
profile is doing.

*pyenv init* is the only command that crosses the line of loading extra
commands into your shell. Coming from rvm, some of you might be opposed
to this idea. Here's what *pyenv init* actually does:

1. *Sets up your shims path.* This is the only requirement for pyenv to
   function properly. You can do this by hand by prepending *$(pyenv
   root)/shims* to your *$PATH*.

2. *Rehashes shims.* From time to time you'll need to rebuild your shim
   files. Doing this on init makes sure everything is up to date. You
   can always run *pyenv rehash* manually.

3. *Installs the sh dispatcher.* This bit is also optional, but allows
   pyenv and plugins to change variables in your current shell, making
   commands like *pyenv shell* possible. The sh dispatcher doesn't do
   anything crazy like override *cd* or hack your shell prompt, but if
   for some reason you need *pyenv* to be a real script rather than a
   shell function, you can safely skip it.

-  :: 

To see exactly what happens under the hood for yourself, run *"pyenv
init -"*.

* Uninstalling Python Versions
As time goes on, you will accumulate Python versions in your *$(pyenv
root)/versions* directory.

To remove old Python versions, *pyenv uninstall* command to automate the
removal process.

Alternatively, simply *rm -rf* the directory of the version you want to
remove. You can find the directory of a particular Python version with
the *pyenv prefix* command,

e.g. *pyenv prefix 2.6.8*.

* Command Reference
The most common subcommands are:

** pyenv commands
Lists all available pyenv commands.

** pyenv local
Sets a local application-specific Python version by writing the version
name to a *.python-version* file in the current directory. This version
overrides the global version, and can be overridden itself by setting
the *PYENV_VERSION* environment variable or with the *pyenv shell*
command.

-  :: 

#+begin_example
  $ pyenv local 2.7.6
#+end_example

-  :: 

When run without a version number, *pyenv local* reports the currently
configured local version. You can also unset the local version:

-  :: 

#+begin_example
  $ pyenv local --unset
#+end_example

-  :: 

Previous versions of pyenv stored local version specifications in a file
named *.pyenv-version*. For backwards compatibility, pyenv will read a
local version specified in an *.pyenv-version* file, but a
*.python-version* file in the same directory will take precedence.

You can specify multiple versions as local Python at once.

Let's say if you have two versions of 2.7.6 and 3.3.3. If you prefer
2.7.6 over 3.3.3,

-  :: 

#+begin_example
  $ pyenv local 2.7.6 3.3.3
  $ pyenv versions
    system
  * 2.7.6 (set by /Users/yyuu/path/to/project/.python-version)
  * 3.3.3 (set by /Users/yyuu/path/to/project/.python-version)
  $ python --version
  Python 2.7.6
  $ python2.7 --version
  Python 2.7.6
  $ python3.3 --version
  Python 3.3.3
#+end_example

-  :: 

or, if you prefer 3.3.3 over 2.7.6,

-  :: 

#+begin_example
  $ pyenv local 3.3.3 2.7.6
  $ pyenv versions
    system
  * 2.7.6 (set by /Users/yyuu/path/to/project/.python-version)
  * 3.3.3 (set by /Users/yyuu/path/to/project/.python-version)
    venv27
  $ python --version
  Python 3.3.3
  $ python2.7 --version
  Python 2.7.6
  $ python3.3 --version
  Python 3.3.3
#+end_example

-  :: 

** pyenv global
Sets the global version of Python to be used in all shells by writing
the version name to the *~/.pyenv/version* file. This version can be
overridden by an application-specific *.python-version* file, or by
setting the *PYENV_VERSION* environment variable.

-  :: 

#+begin_example
  $ pyenv global 2.7.6
#+end_example

-  :: 

The special version name *system* tells pyenv to use the system Python
(detected by searching your *$PATH*).

When run without a version number, *pyenv global* reports the currently
configured global version.

You can specify multiple versions as global Python at once.

Let's say if you have two versions of 2.7.6 and 3.3.3. If you prefer
2.7.6 over 3.3.3,

-  :: 

#+begin_example
  $ pyenv global 2.7.6 3.3.3
  $ pyenv versions
    system
  * 2.7.6 (set by /Users/yyuu/.pyenv/version)
  * 3.3.3 (set by /Users/yyuu/.pyenv/version)
  $ python --version
  Python 2.7.6
  $ python2.7 --version
  Python 2.7.6
  $ python3.3 --version
  Python 3.3.3
#+end_example

-  :: 

or, if you prefer 3.3.3 over 2.7.6,

-  :: 

#+begin_example
  $ pyenv global 3.3.3 2.7.6
  $ pyenv versions
    system
  * 2.7.6 (set by /Users/yyuu/.pyenv/version)
  * 3.3.3 (set by /Users/yyuu/.pyenv/version)
    venv27
  $ python --version
  Python 3.3.3
  $ python2.7 --version
  Python 2.7.6
  $ python3.3 --version
  Python 3.3.3
#+end_example

-  :: 

** pyenv shell
Sets a shell-specific Python version by setting the *PYENV_VERSION*
environment variable in your shell. This version overrides
application-specific versions and the global version.

-  :: 

#+begin_example
  $ pyenv shell pypy-2.2.1
#+end_example

-  :: 

When run without a version number, *pyenv shell* reports the current
value of *PYENV_VERSION*. You can also unset the shell version:

-  :: 

#+begin_example
  $ pyenv shell --unset
#+end_example

-  :: 

Note that you'll need pyenv's shell integration enabled (step 3 of the
installation instructions) in order to use this command. If you prefer
not to use shell integration, you may simply set the *PYENV_VERSION*
variable yourself:

-  :: 

#+begin_example
  $ export PYENV_VERSION=pypy-2.2.1
#+end_example

-  :: 

You can specify multiple versions via *PYENV_VERSION* at once.

Let's say if you have two versions of 2.7.6 and 3.3.3. If you prefer
2.7.6 over 3.3.3,

-  :: 

#+begin_example
  $ pyenv shell 2.7.6 3.3.3
  $ pyenv versions
    system
  * 2.7.6 (set by PYENV_VERSION environment variable)
  * 3.3.3 (set by PYENV_VERSION environment variable)
  $ python --version
  Python 2.7.6
  $ python2.7 --version
  Python 2.7.6
  $ python3.3 --version
  Python 3.3.3
#+end_example

-  :: 

or, if you prefer 3.3.3 over 2.7.6,

-  :: 

#+begin_example
  $ pyenv shell 3.3.3 2.7.6
  $ pyenv versions
    system
  * 2.7.6 (set by PYENV_VERSION environment variable)
  * 3.3.3 (set by PYENV_VERSION environment variable)
    venv27
  $ python --version
  Python 3.3.3
  $ python2.7 --version
  Python 2.7.6
  $ python3.3 --version
  Python 3.3.3
#+end_example

-  :: 

** pyenv install
Install a Python version

-  :: 

#+begin_example
  Usage: pyenv install [-f] [-kvp] <version>
         pyenv install [-f] [-kvp] <definition-file>
         pyenv install -l|--list

    -l, --list             List all available versions
    -f, --force            Install even if the version appears to be installed 
    					already
    -s, --skip-existing    Skip the installation if the version appears to be
    					installed already

    python-build options:

    -k, --keep        Keep source tree in $PYENV_BUILD_ROOT after installation
                      (defaults to $PYENV_ROOT/sources)
    -v, --verbose     Verbose mode: print compilation status to stdout
    -p, --patch       Apply a patch from stdin before building
    -g, --debug       Build a debug version
#+end_example

-  :: 

To list the all available versions of Python, including Anaconda,
Jython, pypy, and stackless, use:

-  :: 

#+begin_example
  $ pyenv install --list
#+end_example

-  :: 

Then install the desired versions:

-  :: 

#+begin_example
  $ pyenv install 2.7.6
  $ pyenv install 2.6.8
  $ pyenv versions
    system
    2.6.8
  * 2.7.6 (set by /home/yyuu/.pyenv/version)
#+end_example

-  :: 

** pyenv uninstall
Uninstall a specific Python version.

-  :: 

#+begin_example
  Usage: pyenv uninstall [-f|--force] <version>

     -f  Attempt to remove the specified version without prompting
         for confirmation. If the version does not exist, do not
         display an error message.
#+end_example

-  :: 

** pyenv rehash
Installs shims for all Python binaries known to pyenv (i.e.,
*~/.pyenv/versions/*/bin/**). Run this command after you install a new
version of Python, or install a package that provides binaries.

-  :: 

#+begin_example
  $ pyenv rehash
#+end_example

-  :: 

** pyenv version
Displays the currently active Python version, along with information on
how it was set.

-  :: 

#+begin_example
  $ pyenv version
  2.7.6 (set by /home/yyuu/.pyenv/version)
#+end_example

-  :: 

** pyenv versions
Lists all Python versions known to pyenv, and shows an asterisk next to
the currently active version.

-  :: 

#+begin_example
  $ pyenv versions
    2.5.6
    2.6.8
  * 2.7.6 (set by /home/yyuu/.pyenv/version)
    3.3.3
    jython-2.5.3
    pypy-2.2.1
#+end_example

-  :: 

** pyenv which
Displays the full path to the executable that pyenv will invoke when you
run the given command.

-  :: 

#+begin_example
  $ pyenv which python3.3
  /home/yyuu/.pyenv/versions/3.3.3/bin/python3.3
#+end_example

-  :: 

** pyenv whence
Lists all Python versions with the given command installed.

-  :: 

#+begin_example
  $ pyenv whence 2to3
  2.6.8
  2.7.6
  3.3.3
#+end_example

-  :: 

* Environment variables
You can affect how pyenv operates with the following settings:

- *name (default)* :: *description*

- *PYENV_VERSION* :: Specifies the Python version to be used. Also see
  *pyenv shell*

- *PYENV_ROOT (*~/.pyenv*)* :: Defines the directory under which Python
  versions and shims reside. Also see *pyenv root*

- *PYENV_DEBUG* :: Outputs debug information.\\
  Also as: *pyenv --debug <subcommand>*

- *PYENV_HOOK_PATH* :: Colon-separated list of paths searched for pyenv
  hooks.

- *PYENV_DIR (*$PWD*)* :: Directory to start searching for
  *.python-version* files.

- *HTTP_PROXY,HTTPS_PROXY* :: Proxy Variables

- *CONFIGURE_OPTS* :: Pass configure options to build.

- *PYTHON_BUILD_ARIA2_OPTS* :: Used to pass additional parameters to
  *aria2* [[https://aria2.github.io/][]] If the *aria2c* binary is
  available on PATH, pyenv uses *aria2c* instead of *curl* or *wget* to
  download the Python Source code. If you have an unstable internet
  connection, you can use this variable to instruct *aria2* to
  accelerate the download. In most cases, you will only need to use *-x
  10 -k 1M* as value to *PYTHON_BUILD_ARIA2_OPTS* environment variable

* License
The *MIT* License
