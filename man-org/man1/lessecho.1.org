#+TITLE: Man1 - lessecho.1
#+DESCRIPTION: Linux manpage for lessecho.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lessecho - expand metacharacters

* SYNOPSIS
*lessecho* /[-ox] [-cx] [-pn] [-dn] [-mx] [-nn] [-ex] [-a] file .../

* DESCRIPTION
/lessecho/ is a program that simply echos its arguments on standard
output. But any metacharacter in the output is preceded by an "escape"
character, which by default is a backslash.

* OPTIONS
A summary of options is included below.

- *-ex* :: Specifies "x", rather than backslash, to be the escape char
  for metachars. If x is "-", no escape char is used and arguments
  containing metachars are surrounded by quotes instead.

- *-ox* :: Specifies "x", rather than double-quote, to be the open quote
  character, which is used if the -e- option is specified.

- *-cx* :: Specifies "x" to be the close quote character.

- *-pn* :: Specifies "n" to be the open quote character, as an integer.

- *-dn* :: Specifies "n" to be the close quote character, as an integer.

- *-mx* :: Specifies "x" to be a metachar. By default, no characters are
  considered metachars.

- *-nn* :: Specifies "n" to be a metachar, as an integer.

- *-fn* :: Specifies "n" to be the escape char for metachars, as an
  integer.

- *-a* :: Specifies that all arguments are to be quoted. The default is
  that only arguments containing metacharacters are quoted.

* SEE ALSO
*less*(1)

* AUTHOR
This manual page was written by Thomas Schoepf <schoepf@debian.org>, for
the Debian GNU/Linux system (but may be used by others).

Report bugs at https://github.com/gwsw/less/issues.
