#+TITLE: Man1 - gifcolor.1
#+DESCRIPTION: Linux manpage for gifcolor.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gifcolor - generate color test-pattern GIFs

* SYNOPSIS
*gifcolor* [-v] [-b /background/] [-h] [/colormap-file/]

* DESCRIPTION
A program to generate color test patterns. Feed it a color map file (as
generated, say, by the -s option of gifclrmp) and it will generate a GIF
containing lines of the form.

#+begin_quote
  #+begin_example
    Color %-3d: [%-3d, %-3d, %-3d]:
  #+end_example
#+end_quote

where the first number is the zero-based color index, and the triple is
the indexs [Red, Green, Blue] value. There will be one such line for
each color. Each line will be set in a simple 8x8 font in the color it
describes; thus, any lines corresponding to the GIFs background color
will be blank.

* OPTIONS
-v

#+begin_quote
  Verbose mode (show progress). Enables printout of running scan lines.
#+end_quote

-b

#+begin_quote
  Set the images backround color to a given numeric index.
#+end_quote

-h

#+begin_quote
  Print one line of command line help, similar to Usage above.
#+end_quote

If no colormap file is specified, the color map will be read from stdin.

* AUTHOR
Gershon Elber.
