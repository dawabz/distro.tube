#+TITLE: Man1 - lss16toppm.1
#+DESCRIPTION: Linux manpage for lss16toppm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lss16toppm --- Convert an LSS-16 image to PPM

* SYNOPSIS
*lss16toppm* [*-map*] [< file.lss] [> file.ppm]

* DESCRIPTION
This manual page documents briefly the *lss16toppm* command.

The *lss16toppm* utility converts an LSS-16 image to a PPM image.

* OPTIONS
A summary of options is included below.

- -map :: Output the color map to standard error.

* SEE ALSO
*ppmtolss16*(1)

* AUTHOR
This manual page was compiled by dann frazier <dannf@debian.org> for the
*Debian GNU/Linux* system (but may be used by others).
