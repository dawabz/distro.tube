#+TITLE: Man1 - texindy.1
#+DESCRIPTION: Linux manpage for texindy.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
texindy - create sorted and tagged index from raw LaTeX index

* SYNOPSIS
texindy [-V?h] [-qv] [-iglr] [-d magic] [-o outfile.ind] [-t log] \ [-L
lang] [-C codepage] [-M module] [idx0 idx1 ...]

** GNU-Style Long Options for Short Options:
-V / --version -? / -h / --help -q / --quiet -v / --verbose -i / --stdin
-g / --german -l / --letter-ordering -r / --no-ranges -d / --debug
(multiple times) -o / --out-file -t / --log-file -L / --language -C /
--codepage -M / --module (multiple times) -I / --input-markup
(supported: latex, xelatex, omega)

* DESCRIPTION
*texindy* is the LaTeX-specific command of xindy, the flexible indexing
system. It takes a raw index as input, and produces a merged, sorted and
tagged index. Merging, sorting, and tagging is controlled by xindy
modules, with a convenient set already preloaded.

Files with the raw index are passed as arguments. If no arguments are
passed, the raw index will be read from standard input.

A good introductionary description of *texindy* appears in the indexing
chapter of the LaTeX Companion (2nd ed.)

If you want to produce an index for LaTeX documents with special index
markup, the command /xindy/ (1) is probably more of interest for you.

*texindy* is an approach to merge support for the /make-rules/
framework, own xindy modules (e.g., for special LaTeX commands in the
index), and a reasonable level of MakeIndex compatibility.

* OPTIONS
- "--version" / -V :: output version numbers of all relevant components
  and exit.

- "--help" / -h / -? :: output usage message with options explanation.

- "--quiet" / -q :: Don't output progress messages. Output only error
  messages.

- "--verbose" / -v :: Output verbose progress messages.

- "--debug" magic / -d magic :: Output debug messages, this option may
  be specified multiple times. /magic/ determines what is output: magic
  remark ------------------------------------------------------------
  script internal progress messages of driver scripts keep_tmpfiles dont
  discard temporary files markup output markup trace, as explained in
  xindy manual level=n log level, n is 0 (default), 1, 2, or 3

- "--out-file" outfile.ind / -o outfile.ind :: Output index to file
  /outfile.ind/. If this option is not passed, the name of the output
  file is the base name of the first argument and the file extension
  /ind/. If the raw index is read from standard input, this option is
  mandatory.

- "--log-file" log.ilg / -t log.ilg :: Output log messages to file
  /log.ilg/. These log messages are independent from the progress
  messages that you can influence with =--debug= or =--verbose=.

- "--language" lang / -L lang :: The index is sorted according to the
  rules of language /lang/. These rules are encoded in a xindy module
  created by /make-rules/. If no input encoding is specified via
  =--codepage= or enforced by input markup, a xindy module for that
  language is searched with a latin, a cp, an iso, ascii, or utf8
  encoding, in that order.

- "--codepage" enc / -C enc :: There are two different situations and
  use cases for this option.

  1. Input markup is =latex= (the default). Then *texindy*'s raw input
     is assumed to be encoded in LaTeX Internal Character Representation
     (LICR). I.e., non-ASCII characters are encoded as command
     sequences. This option tells xindy the encoding it shall use for
     letter group headings. (Additionally it specifies the encoding used
     internally for sorting Ω- but that doesn't matter for the result.)
     Only LICR encodings for Latin script alphabets are supported; more
     precisely characters that are in LaTeX latin1, latin2, and latin3
     LICR encodings. Even when you specify =utf8= as codepage, only
     these characters will be known. But if you use non-Latin alphabets,
     you probably use (or should use) XeLaTeX or LuaLaTeX and then you
     have a different input markup.

  2. Input markup is =xelatex= or =omega=. Then this option is ignored;
     codepage =utf8= is enforced. *texindy*'s raw input is assumed to be
     UTF-8 encoded, LICR is not used.

- "--module" module / -M module :: Load the xindy module /module.xdy/.
  This option may be specified multiple times. The modules are searched
  in the xindy search path that can be changed with the environment
  variable =XINDY_SEARCHPATH=.

- "--input-markup" input / -I input :: Specifies the input markup of the
  raw index. Supported values for /input/ are =latex=, =xelatex=, and
  =omega=. =latex= input markup is the one that is emitted by default
  from the LaTeX kernel, or by the =index= macro package of David Jones,
  when used with standard LaTeX or pdfLaTeX. ^^-notation of single byte
  characters is supported. Usage of LaTeX's /inputenc/ package is
  assumed as well, i.e., raw input is encoded in LICR. =xelatex= input
  markup is like =latex=, but without /inputenc/ usage. Raw input is
  encoded in UTF-8. LuaLaTeX has the same input markup, there's no
  special option value for it. =omega= input markup is like =latex=
  input markup, but with Omega's ^^-notation as encoding for non-ASCII
  characters. LICR encoding is not used then, and =utf8= is enforced to
  be the codepage for sorting and for output of letter group headings.

* SUPPORTED LANGUAGES / CODEPAGES
The following languages are supported:

** Latin scripts
albanian gypsy portuguese croatian hausa romanian czech hungarian
russian-iso danish icelandic slovak-small english italian slovak-large
esperanto kurdish-bedirxan slovenian estonian kurdish-turkish
spanish-modern finnish latin spanish-traditional french latvian swedish
general lithuanian turkish german-din lower-sorbian upper-sorbian
german-duden norwegian vietnamese greek-iso polish

German recognizes two different sorting schemes to handle umlauts:
normally, =a\k:.= is sorted like =ae=, but in phone books or
dictionaries, it is sorted like =a=. The first scheme is known as /DIN
order/, the second as /Duden order/.

=*-iso= language names assume that the raw index entries are in ISO
8859-9 encoding.

=gypsy= is a northern Russian dialect.

** Cyrillic scripts
belarusian mongolian serbian bulgarian russian ukrainian macedonian

** Other scripts
greek klingon

** Available Codepages
This is not yet written. You can look them up in your xindy
distribution, in the /modules/lang/language// directory (where
/language/ is your language). They are named
/variant-codepage-lang.xdy/, where /variant-/ is most often empty (for
german, it's =din5007= and =duden=; for spanish, it's =modern= and
=traditional=, etc.)

< Describe available codepages for each language > < Describe relevance
of codepages (as internal representation) for LaTeX inputenc >

* TEXINDY STANDARD MODULES
There is a set of *texindy* standard modules that help to process LaTeX
index files. Some of them are automatically loaded. Some of them are
loaded by default, this can be turned off with a *texindy* option.
Others may be specified as =--module= argument to achieve a specific
effect.

xindy Module Category Description

** Sorting
word-order Default A space comes before any letter in the alphabet:
``index style is listed before ``indexing. Turn it off with option -l.
letter-order Add-on Spaces are ignored: ``index style is sorted after
``indexing. keep-blanks Add-on Leading and trailing white space (blanks
and tabs) are not ignored; intermediate white space is not changed.
ignore-hyphen Add-on Hyphens are ignored: ``ad-hoc is sorted as ``adhoc.
ignore-punctuation Add-on All kinds of punctuation characters are
ignored: hyphens, periods, commas, slashes, parentheses, and so on.
numeric-sort Auto Numbers are sorted numerically, not like characters:
``V64 appears before ``V128.

** Page Numbers
page-ranges Default Appearances on more than two consecutive pages are
listed as a range: ``1--4. Turn it off with option -r. ff-ranges Add-on
Uses implicit ``ff notation for ranges of three pages, and explicit
ranges thereafter: 2f, 2ff, 2--6. ff-ranges-only Add-on Uses only
implicit ranges: 2f, 2ff. book-order Add-on Sorts page numbers with
common book numbering scheme correctly -- Roman numerals first, then
Arabic numbers, then others: i, 1, A.

** Markup and Layout
tex Auto Handles basic TeX conventions. latex-loc-fmts Auto Provides
LaTeX formatting commands for page number encapsulation. latex Auto
Handles LaTeX conventions, both in raw index entries and output markup;
implies tex. makeindex Auto Emulates the default MakeIndex input syntax
and quoting behavior. latin-lettergroups Auto Layout contains a single
Latin letter above each group of words starting with the same letter.
german-sty Add-on Handles umlaut markup of babels german and ngerman
options.

* COMPATIBILITY TO MAKEINDEX
*xindy* does not claim to be completely compatible with MakeIndex, that
would prevent some of its enhancements. That said, we strive to deliver
as much compatibility as possible. The most important incompatibilities
are

- For raw index entries in LaTeX syntax, =\index{aaa|bbb}= is
  interpreted differently. For MakeIndex =bbb= is markup that is output
  as a LaTeX tag for this page number. For *xindy*, this is a location
  attribute, an abstract identifier that will be later associated with
  markup that should be output for that attribute. For straight-forward
  usage, when =bbb= is =textbf= or similar, we supply location attribute
  definitions that mimic MakeIndex's behaviour. For more complex usage,
  when =bbb= is not an identifier, no such compatibility definitions
  exist and may also not been created with current *xindy*. In
  particular, this means that by default the LaTeX package =hyperref=
  will create raw index files that cannot be processed with *xindy*.
  This is not a bug, this is the unfortunate result of an intented
  incompatibility. It is currently not possible to get both hyperref's
  index links and use *xindy*. A similar situation is reported to exist
  for the =memoir= LaTeX class. Programmers who know Common Lisp and Lex
  and want to work on a remedy should please contact the author.

- If you have an index rage and a location attribute, e.g.,
  =\index{key\(attr}= starts the range, one needs (1) to specify that
  attribute in the range closing entry as well (i.e., as
  =\index{key\)attr}=) and (2) one needs to declare the index attribute
  in an *xindy* style file. MakeIndex will output the markup
  =\attr{page1--page2}= for such a construct. This is not possible to
  achieve in *xindy*, output will be
  =\attrMarkup{page1}--\attrMarkup{page2}=. (This is actually considered
  a bug, but not a high priority one.) The difference between MakeIndex
  page number tags and *xindy* location attributes was already explained
  in the previous item.

- The MakeIndex compatibility definitions support only the default raw
  index syntax and markup definition. It is not possible to configure
  raw index parsing or use a MakeIndex style file to describe output
  markup.

* ENVIRONMENT
- "TEXINDY_AUTO_MODULE" :: This is the name of the xindy module that
  loads all auto-loaded modules. The default is =texindy=.

* AUTHOR
Joachim Schrod

* LEGALESE
Copyright (C) 2004-2014 by Joachim Schrod.

*texindy* is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.
