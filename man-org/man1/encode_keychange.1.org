#+TITLE: Man1 - encode_keychange.1
#+DESCRIPTION: Linux manpage for encode_keychange.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
encode_keychange - produce the KeyChange string for SNMPv3

* SYNOPSIS
*encode_keychange* -t md5|sha1 [/OPTIONS/]

* DESCRIPTION
*encode_keychange* produces a KeyChange string using the old and new
passphrases as described in Section 5 of RFC 2274 "User-based Security
Model (USM) for version 3 of the Simple Network Management Protocol
(SNMPv3)". *-t* option is mandatory and specifies the hash transform
type to use.

The transform is used to convert passphrase to master key for a given
user (Ku), convert master key to the localized key (Kul), and to hash
the old Kul with the random bits.

Passphrases are obtained by examining a number of sources until success
(in order listed):

#+begin_quote
  command line options (see *-N* and *-O* options below);

  the file *$HOME/.snmp/passphrase.ek* which should only contain two
  lines with old and new passphrase;

  standard input *-or-* user input from the terminal.
#+end_quote

* OPTIONS
- *-E* [0x]</engineID/> EngineID used for Kul
  generation. :: </engineID/> is intepreted as a hex string when
  preceded by 0x, otherwise it is treated as a text string. If no
  </engineID/> is specified, it is constructed from the first IP address
  for the local host.

- *-f* :: Force passphrases to be read from standard input.

- *-h* :: Display the help message.

- *-N* "</new_passphrase/>" :: Passphrase used to generate the new Ku.

- *-O* "</old_passphrase/>" :: Passphrase used to generate the old Ku.

- *-P* :: Turn off the prompt for passphrases when getting data from
  standard input.

- *-v* :: Be verbose.

- *-V* :: Echo passphrases to terminal.

* SEE ALSO
The localized key method is defined in RFC 2274, Sections 2.6 and A.2,
and originally documented in

#+begin_quote
  U. Blumenthal, N. C. Hien, B. Wijnen, "Key Derivation for Network
  Management Applications", IEEE Network Magazine, April/May
  issue, 1997.
#+end_quote
