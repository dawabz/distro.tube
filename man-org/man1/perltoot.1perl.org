#+TITLE: Man1 - perltoot.1perl
#+DESCRIPTION: Linux manpage for perltoot.1perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
perltoot - Links to information on object-oriented programming in Perl

* DESCRIPTION
For information on OO programming with Perl, please see perlootut and
perlobj.

(The above documents supersede the tutorial that was formerly here in
perltoot.)
