#+TITLE: Man1 - cargo-yank.1
#+DESCRIPTION: Linux manpage for cargo-yank.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cargo-yank - Remove a pushed crate from the index

* SYNOPSIS
*cargo yank* [/options/] *--vers* /version/ [/crate/]

* DESCRIPTION
The yank command removes a previously published crate's version from the
server's index. This command does not delete any data, and the crate
will still be available for download via the registry's download link.

Note that existing crates locked to a yanked version will still be able
to download the yanked version to use it. Cargo will, however, not allow
any new crates to be locked to any yanked version.

This command requires you to be authenticated with either the *--token*
option or using *cargo-login*(1).

If the crate name is not specified, it will use the package name from
the current directory.

* OPTIONS
** Yank Options
*--vers* /version/

#+begin_quote
  The version to yank or un-yank.
#+end_quote

*--undo*

#+begin_quote
  Undo a yank, putting a version back into the index.
#+end_quote

*--token* /token/

#+begin_quote
  API token to use when authenticating. This overrides the token stored
  in the credentials file (which is created by *cargo-login*(1)).

  /Cargo config/ <https://doc.rust-lang.org/cargo/reference/config.html>
  environment variables can be used to override the tokens stored in the
  credentials file. The token for crates.io may be specified with the
  *CARGO_REGISTRY_TOKEN* environment variable. Tokens for other
  registries may be specified with environment variables of the form
  *CARGO_REGISTRIES_NAME_TOKEN* where *NAME* is the name of the registry
  in all capital letters.
#+end_quote

*--index* /index/

#+begin_quote
  The URL of the registry index to use.
#+end_quote

*--registry* /registry/

#+begin_quote
  Name of the registry to use. Registry names are defined in /Cargo
  config/ files <https://doc.rust-lang.org/cargo/reference/config.html>.
  If not specified, the default registry is used, which is defined by
  the *registry.default* config key which defaults to *crates-io*.
#+end_quote

** Display Options
*-v*, *--verbose*

#+begin_quote
  Use verbose output. May be specified twice for "very verbose" output
  which includes extra output such as dependency warnings and build
  script output. May also be specified with the *term.verbose* /config
  value/ <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

*-q*, *--quiet*

#+begin_quote
  No output printed to stdout.
#+end_quote

*--color* /when/

#+begin_quote
  Control when colored output is used. Valid values:

  #+begin_quote
    ·*auto* (default): Automatically detect if color support is
    available on the terminal.
  #+end_quote

  #+begin_quote
    ·*always*: Always display colors.
  #+end_quote

  #+begin_quote
    ·*never*: Never display colors.
  #+end_quote

  May also be specified with the *term.color* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

** Common Options
*+*/toolchain/

#+begin_quote
  If Cargo has been installed with rustup, and the first argument to
  *cargo* begins with *+*, it will be interpreted as a rustup toolchain
  name (such as *+stable* or *+nightly*). See the /rustup documentation/
  <https://rust-lang.github.io/rustup/overrides.html> for more
  information about how toolchain overrides work.
#+end_quote

*-h*, *--help*

#+begin_quote
  Prints help information.
#+end_quote

*-Z* /flag/

#+begin_quote
  Unstable (nightly-only) flags to Cargo. Run *cargo -Z help* for
  details.
#+end_quote

* ENVIRONMENT
See /the reference/
<https://doc.rust-lang.org/cargo/reference/environment-variables.html>
for details on environment variables that Cargo reads.

* EXIT STATUS

#+begin_quote
  ·*0*: Cargo succeeded.
#+end_quote

#+begin_quote
  ·*101*: Cargo failed to complete.
#+end_quote

* EXAMPLES

#+begin_quote
  1.Yank a crate from the index:

  #+begin_quote
    #+begin_example
      cargo yank --vers 1.0.7 foo
    #+end_example
  #+end_quote
#+end_quote

* SEE ALSO
*cargo*(1), *cargo-login*(1), *cargo-publish*(1)
