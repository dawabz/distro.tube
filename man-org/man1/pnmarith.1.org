#+TITLE: Man1 - pnmarith.1
#+DESCRIPTION: Linux manpage for pnmarith.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pnmarith - perform arithmetic on two PNM images

* DESCRIPTION
This program is part of *Netpbm*(1)

Starting with Netpbm 10.3, *pnmarith* is obsolete. Use **pamarith**(1)
instead.

*pamarith* is backward compatible with *pnmarith* except where your
input images have different depths. *pnmarith* would convert the one
with the lesser depth to have the higher depth before doing the
arithmetic. With *pamarith*, you must do that step separately, using
*pgmtoppm*.
