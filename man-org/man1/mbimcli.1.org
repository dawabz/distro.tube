#+TITLE: Man1 - mbimcli.1
#+DESCRIPTION: Linux manpage for mbimcli.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
mbimcli - Control MBIM devices

* DESCRIPTION
** Usage:

#+begin_quote
  mbimcli [OPTION?] - Control MBIM devices
#+end_quote

** Help Options:
- *-h*, *--help* :: Show help options

- *--help-all* :: Show all help options

- *--help-basic-connect* :: Show Basic Connect Service options

- *--help-phonebook* :: Show Phonebook Service options

- *--help-dss* :: Show Device Service Stream options

- *--help-ms-firmware-id* :: Show Microsoft Firmware ID Service options

- *--help-ms-host-shutdown* :: Show Microsoft Host Shutdown Service
  options

- *--help-ms-sar* :: Show Microsoft SAR Service options

- *--help-atds* :: Show AT&T Device Service options

- *--help-intel-firmware-update* :: Show Intel Firmware Update Service
  options

- *--help-ms-basic-connect-extensions* :: Show Microsoft Basic Connect
  Extensions Service options

- *--help-quectel* :: Show Quectel Service options

- *--help-link-management* :: Show link management specific options

** Basic Connect options:
- *--query-device-caps* :: Query device capabilities

- *--query-subscriber-ready-status* :: Query subscriber ready status

- *--query-radio-state* :: Query radio state

- *--set-radio-state*=/[/(on|off)] :: Set radio state

- *--query-device-services* :: Query device services

- *--query-pin-state* :: Query PIN state

- *--enter-pin*=/[/(PIN type),(current PIN)] :: Enter PIN (PIN type is
  optional, defaults to PIN1, allowed options:
  (pin1,network-pin,network-subset-pin,service-provider-pin,corporate-pin)

- *--change-pin*=/[/(current PIN),(new PIN)] :: Change PIN

- *--enable-pin*=/[/(current PIN)] :: Enable PIN

- *--disable-pin*=/[/(PIN type),(current PIN)] :: Disable PIN (PIN type
  is optional, see enter-pin for details)

- *--enter-puk*=/[/(PUK type),(PUK),(new PIN)] :: Enter PUK (PUK type is
  optional, defaults to PUK1, allowed options:
  (puk1,network-puk,network-subset-puk,service-provider-puk,corporate-puk)

- *--query-pin-list* :: Query PIN list

- *--query-home-provider* :: Query home provider

- *--query-preferred-providers* :: Query preferred providers

- *--query-visible-providers* :: Query visible providers

- *--query-registration-state* :: Query registration state

- *--register-automatic* :: Launch automatic registration

- *--query-signal-state* :: Query signal state

- *--query-packet-service-state* :: Query packet service state

- *--attach-packet-service* :: Attach to the packet service

- *--detach-packet-service* :: Detach from the packet service

- *--query-connection-state*=/[SessionID]/ :: Query connection state
  (SessionID is optional, defaults to 0)

- *--connect*=/[/"key=value,..."] :: Connect (allowed keys: session-id,
  apn, ip-type (ipv4|ipv6|ipv4v6), auth (PAP|CHAP|MSCHAPV2), username,
  password)

- *--query-ip-configuration*=/[SessionID]/ :: Query IP configuration
  (SessionID is optional, defaults to 0)

- *--disconnect*=/[SessionID]/ :: Disconnect (SessionID is optional,
  defaults to 0)

- *--query-packet-statistics* :: Query packet statistics

- *--query-ip-packet-filters*=/[SessionID]/ :: Query IP packet filters
  (SessionID is optional, defaults to 0)

- *--query-provisioned-contexts* :: Query provisioned contexts

** Phonebook options:
- *--phonebook-query-configuration* :: Query the phonebook configuration

- *--phonebook-read*=/[/(Phonebook index)] :: Read phonebook entry with
  given index

- *--phonebook-read-all* :: Read all phonebook entries

- *--phonebook-write*=/[/(Name),(Number)[,(Index)]] :: Add new phonebook
  entry or update an existing one

- *--phonebook-delete*=/[/(Phonebook index)] :: Delete phonebook entry
  with given index

- *--phonebook-delete-all* :: Delete all phonebook entries

** Device Service Stream options:
- *--dss-connect*=/[/(UUID),(Session ID)] :: Connect DSS session

- *--dss-disconnect*=/[/(UUID),(Session ID)] :: Disconnect DSS session

** Microsoft Firmware ID options:
- *--ms-query-firmware-id* :: Query firmware ID

** Microsoft Host Shutdown options:
- *--ms-notify-host-shutdown* :: Notify that host is shutting down

** Microsoft SAR options:
- *--ms-set-sar-config*=/[/(device|os),(enabled|disabled)[,[{antenna_index,backoff_index}...]]] :: Set
  SAR config

- *--ms-query-sar-config* :: Query SAR config

- *--ms-set-transmission-status*=/[/(enabled|disabled),(timer)] :: Set
  transmission status and hysteresis timer (in seconds)

- *--ms-query-transmission-status* :: Query transmission status

** AT&T Device Service options:
- *--atds-query-signal* :: Query signal info

- *--atds-query-location* :: Query cell location

** Intel Firmware Update Service options:
- *--intel-modem-reboot* :: Reboot modem

** Microsoft Basic Connect Extensions options:
- *--ms-query-pco*=/[SessionID]/ :: Query PCO value (SessionID is
  optional, defaults to 0)

- *--ms-query-lte-attach-configuration* :: Query LTE attach
  configuration

- *--ms-query-lte-attach-info* :: Query LTE attach status information

- *--ms-query-sys-caps* :: Query system capabilities

- *--ms-query-device-caps* :: Query device capabilities

- *--ms-query-slot-info-status*=/[SlotIndex]/ :: Query slot information
  status

- *--ms-set-device-slot-mappings*=/[/(SlotIndex)[,(SlotIndex)[,...]]] :: Set
  device slot mappings for each executor

- *--ms-query-device-slot-mappings* :: Query device slot mappings

** Quectel options:
- *--quectel-query-radio-state* :: Query radio state

- *--quectel-set-radio-state*=/[/(on)] :: Set radio state

** Link management options:
- *--link-list*=/[IFACE]/ :: List links created from a given interface

- *--link-add*=/[iface=IFACE/,prefix=PREFIX[,session-id=N]] :: Create
  new network interface link

- *--link-delete*=/IFACE/ :: Delete a given network interface link

- *--link-delete-all*=/[IFACE]/ :: Delete all network interface links
  from the given interface

** Application Options:
- *-d*, *--device*=/[PATH]/ :: Specify device path

- *-p*, *--device-open-proxy* :: Request to use the 'mbim-proxy' proxy

- *--no-open*=/[Transaction/ ID] :: Do not explicitly open the MBIM
  device before running the command

- *--no-close* :: Do not close the MBIM device after running the command

- *--noop* :: Don't run any command

- *-v*, *--verbose* :: Run action with verbose logs, including the debug
  ones

- *--silent* :: Run action with no logs; not even the error/warning ones

- *-V*, *--version* :: Print version

* COPYRIGHT
Copyright © 2013-2021 Aleksander Morgado License GPLv2+: GNU GPL version
2 or later <http://gnu.org/licenses/gpl-2.0.html>\\
This is free software: you are free to change and redistribute it. There
is NO WARRANTY, to the extent permitted by law.

* SEE ALSO
The full documentation for *mbimcli* is maintained as a Texinfo manual.
If the *info* and *mbimcli* programs are properly installed at your
site, the command

#+begin_quote
  *info mbimcli*
#+end_quote

should give you access to the complete manual.
