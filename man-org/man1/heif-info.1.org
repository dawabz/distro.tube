#+TITLE: Man1 - heif-info.1
#+DESCRIPTION: Linux manpage for heif-info.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
heif-info - show information on HEIC/HEIF file

* SYNOPSIS
*heif-info* [*-d*|*--dump-boxes*] [*-h*|*--help*] /filename/

* DESCRIPTION
*heif-info* Show information on HEIC/HEIF file.

* OPTIONS
- *-d*, *--dump-boxes* :: Show a low-level dump of all MP4 file boxes.

- *-help*, *--help* :: Show help.

* EXIT STATUS
*0*

#+begin_quote
  Success
#+end_quote

*1*

#+begin_quote
  Failure (syntax or usage error; error while loading image).
#+end_quote

* BUGS
Please reports bugs or issues at https://github.com/strukturag/libheif

* AUTHORS
Dirk Farin, struktur AG

* COPYRIGHT
Copyright © 2017 struktur AG
