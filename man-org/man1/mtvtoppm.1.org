#+TITLE: Man1 - mtvtoppm.1
#+DESCRIPTION: Linux manpage for mtvtoppm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
mtvtoppm - convert output from an MTV or PRT ray tracer into a PPM

* SYNOPSIS
*mtvtoppm* [/mtvfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*mtvtoppm* reads an input file from Mark VanDeWettering's MTV ray tracer
and produces a PPM image as output.

The PRT raytracer also produces this format.

* SEE ALSO
*ppm*(5)

* AUTHOR
Copyright (C) 1989 by Jef Poskanzer.
