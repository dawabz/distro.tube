#+TITLE: Man1 - omxregister-bellagio.1
#+DESCRIPTION: Linux manpage for omxregister-bellagio.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
omxregister-bellagio - register

components

* SYNOPSIS
*omxregister-bellagio [-v] [-h] [* /componentspath/ *]...*

* DESCRIPTION
Before using any

component, they *must* be registered with the *omxregister-bellagio*
command.

By default, this will create a file named /.omxregister/ under the home
*XDG* data directory with all the components found in
/${libdir}/bellagio./

* OPTIONS
- -v :: Display a verbose output, listing all the components registered.

- -h :: Display help.

- componentspath :: Location of the

components (by default /${libdir}/bellagio)./

* FILES
/$XDG_DATA_HOME/libomxil-bellagio/registry/ or /$OMX_BELLAGIO_REGISTRY./

#+begin_quote
  The

  component registry file.
#+end_quote

* ENVIRONMENT
- BELLAGIO_SEARCH_PATH :: 

- OMX_BELLAGIO_REGISTRY :: 

  Location of the registry.

- XDG_DATA_HOME :: 

  Location of the home data directory where registry is saved (if
  /$OMX_BELLAGIO_REGISTRY/ is unset).

* AUTHORS
Bhattacharyya Sourya, Melpignano Diego, Niemimuukko Ukri, Sen Pankaj,
Siorpaes David and Urlini Giulio.

Man page written by Marc-Andre Lureau.
