#+TITLE: Man1 - git-bugreport.1
#+DESCRIPTION: Linux manpage for git-bugreport.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
git-bugreport - Collect information for user to file a bug report

* SYNOPSIS
#+begin_example
  git bugreport [(-o | --output-directory) <path>] [(-s | --suffix) <format>]
#+end_example

* DESCRIPTION
Captures information about the user's machine, Git client, and
repository state, as well as a form requesting information about the
behavior the user observed, into a single text file which the user can
then share, for example to the Git mailing list, in order to report an
observed bug.

The following information is requested from the user:

#+begin_quote
  ·

  Reproduction steps
#+end_quote

#+begin_quote
  ·

  Expected behavior
#+end_quote

#+begin_quote
  ·

  Actual behavior
#+end_quote

The following information is captured automatically:

#+begin_quote
  ·

  /git version --build-options/
#+end_quote

#+begin_quote
  ·

  uname sysname, release, version, and machine strings
#+end_quote

#+begin_quote
  ·

  Compiler-specific info string
#+end_quote

#+begin_quote
  ·

  A list of enabled hooks
#+end_quote

#+begin_quote
  ·

  $SHELL
#+end_quote

This tool is invoked via the typical Git setup process, which means that
in some cases, it might not be able to launch - for example, if a
relevant config file is unreadable. In this kind of scenario, it may be
helpful to manually gather the kind of information listed above when
manually asking for help.

* OPTIONS
-o <path>, --output-directory <path>

#+begin_quote
  Place the resulting bug report file in *<path>* instead of the current
  directory.
#+end_quote

-s <format>, --suffix <format>

#+begin_quote
  Specify an alternate suffix for the bugreport name, to create a file
  named /git-bugreport-<formatted suffix>/. This should take the form of
  a strftime(3) format string; the current local time will be used.
#+end_quote

* GIT
Part of the *git*(1) suite
