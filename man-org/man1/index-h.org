#+TITLE: Man1 - H
#+DESCRIPTION: Man1 - H
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* H
#+begin_src bash :exports results
readarray -t starts_with_h < <(find . -type f -iname "h*" | sort)

for x in "${starts_with_h[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./h2ph.1perl.org][h2ph.1perl]]              |
| [[file:./h2xs.1perl.org][h2xs.1perl]]              |
| [[file:./happrox.1.org][happrox.1]]               |
| [[file:./hardinfo.1.org][hardinfo.1]]              |
| [[file:./hardlink.1.org][hardlink.1]]              |
| [[file:./hash-to-efi-sig-list.1.org][hash-to-efi-sig-list.1]]  |
| [[file:./hbf2gf.1.org][hbf2gf.1]]                |
| [[file:./hdifftopam.1.org][hdifftopam.1]]            |
| [[file:./head.1.org][head.1]]                  |
| [[file:./HEAD.1p.org][HEAD.1p]]                 |
| [[file:./heif-convert.1.org][heif-convert.1]]          |
| [[file:./heif-enc.1.org][heif-enc.1]]              |
| [[file:./heif-info.1.org][heif-info.1]]             |
| [[file:./heif-thumbnailer.1.org][heif-thumbnailer.1]]      |
| [[file:./hello_xr.1.org][hello_xr.1]]              |
| [[file:./herbstclient.1.org][herbstclient.1]]          |
| [[file:./herbstluftwm.1.org][herbstluftwm.1]]          |
| [[file:./hexdump.1.org][hexdump.1]]               |
| [[file:./hg.1.org][hg.1]]                    |
| [[file:./hipstopgm.1.org][hipstopgm.1]]             |
| [[file:./hmac256.1.org][hmac256.1]]               |
| [[file:./homectl.1.org][homectl.1]]               |
| [[file:./host.1.org][host.1]]                  |
| [[file:./hostid.1.org][hostid.1]]                |
| [[file:./hostname.1.org][hostname.1]]              |
| [[file:./hostnamectl.1.org][hostnamectl.1]]           |
| [[file:./hpcdtoppm.1.org][hpcdtoppm.1]]             |
| [[file:./hpftodit.1.org][hpftodit.1]]              |
| [[file:./htop.1.org][htop.1]]                  |
| [[file:./hunspell.1.org][hunspell.1]]              |
| [[file:./hunzip.1.org][hunzip.1]]                |
| [[file:./hwloc-annotate.1.org][hwloc-annotate.1]]        |
| [[file:./hwloc-bind.1.org][hwloc-bind.1]]            |
| [[file:./hwloc-calc.1.org][hwloc-calc.1]]            |
| [[file:./hwloc-compress-dir.1.org][hwloc-compress-dir.1]]    |
| [[file:./hwloc-diff.1.org][hwloc-diff.1]]            |
| [[file:./hwloc-distrib.1.org][hwloc-distrib.1]]         |
| [[file:./hwloc-dump-hwdata.1.org][hwloc-dump-hwdata.1]]     |
| [[file:./hwloc-gather-cpuid.1.org][hwloc-gather-cpuid.1]]    |
| [[file:./hwloc-gather-topology.1.org][hwloc-gather-topology.1]] |
| [[file:./hwloc-info.1.org][hwloc-info.1]]            |
| [[file:./hwloc-patch.1.org][hwloc-patch.1]]           |
| [[file:./hwloc-ps.1.org][hwloc-ps.1]]              |
| [[file:./hzip.1.org][hzip.1]]                  |
