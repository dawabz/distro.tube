#+TITLE: Man1 - pmxab.1
#+DESCRIPTION: Linux manpage for pmxab.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pmxab - a MusiXTeX preprocessor

* SYNOPSIS
*pmxab* basename/[.pmx]/

* DESCRIPTION
pmxab is a preprocessor for MusiXTeX. It's usually invoked by a frontend
musixtex(1). To use it to its full benefit you should have installed
MusiXTeX Version 1.21 or higher, and TeX itself. The goal of PMX is to
faciliate the efficient typesetting of scores and parts that have an
almost professional appearance. To learn more about typesetting with
PMX, please read the PMX Manual.

When invoked directly, pmxab produces tex output utilizing the MusiXTeX
macro set.

* SEE ALSO
scor2prt(1), musixtex(1)

* AUTHORS
PMX was written by Don Simons <dsimons@roadrunner.com>. This manual page
was written by Roland Stigge <stigge@antcom.de> for the Debian project
and revised by Bob Tennent <rdt@cs.queensu.ca>.
