#+TITLE: Man1 - ompi-clean.1
#+DESCRIPTION: Linux manpage for ompi-clean.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*orte-clean* - Cleans up any stale processes and files leftover from
Open MPI jobs.

* SYNOPSIS
#+begin_example
  orte-clean [--verbose]


  mpirun --pernode [--host | --hostfile file] orte-clean [--verbose]
#+end_example

* OPTIONS
[-v | --verbose] This argument will run the command in verbose mode and
print out the universes that are getting cleaned up as well as processes
that are being killed.

* DESCRIPTION
/orte-clean/ attempts to clean up any processes and files left over from
Open MPI jobs that were run in the past as well as any currently running
jobs. This includes OMPI infrastructure and helper commands, any
processes that were spawned as part of the job, and any temporary files.
orte-clean will only act upon processes and files that belong to the
user running the orte-clean command. If run as root, it will kill off
processes belonging to any users.

When run from the command line, orte-clean will attempt to clean up the
local node it is run from. When launched via mpirun, it will clean up
the nodes selected by mpirun.

* EXAMPLES
Example 1: Clean up local node only.

#+begin_example
  example% orte-clean
#+end_example

Example 2: To clean up on a specific set of nodes specified on command
line, it is recommended to use the pernode option. This will run one
orte-clean for each node.

#+begin_example
  example% mpirun --pernode --host node1,node2,node3 orte-clean
#+end_example

To clean up on a specific set of nodes from a file.

#+begin_example
  example% mpirun --pernode --hostfile nodes_file orte-clean
#+end_example

Example 3: Within a resource managed environment like N1GE, SLURM, or
Torque. The following example is from N1GE.

First, we see that we have two nodes with two CPUs each.

#+begin_example
  example% qsh -pe orte 4


  example% mpirun -np 4 hostname


  node1


  node1


  node2


  node2
#+end_example

Clean up all the nodes in the cluster.

#+begin_example
  example% mpirun --pernode orte-clean
#+end_example

Clean up a subset of the nodes in the cluster.

#+begin_example
  example% mpirun --pernode --host node1 orte-clean
#+end_example

* SEE ALSO
orterun(1), orte-ps(1)
