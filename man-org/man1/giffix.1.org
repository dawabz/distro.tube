#+TITLE: Man1 - giffix.1
#+DESCRIPTION: Linux manpage for giffix.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
giffix - attempt to fix up broken GIFs

* SYNOPSIS
*giffix* [-v] [-h] [/gif-file/]

* DESCRIPTION
A program that attempts to fix broken GIF images. Currently will "fix"
images terminated prematurely by filling the rest of the image with the
darkest color found in the image.

If no GIF file is given, giffix will try to read a GIF file from stdin.
The fixed file is dumped to stdout.

* OPTIONS
-t

#+begin_quote
  Verbose mode (show progress). Enables printout of running scan lines.
#+end_quote

-h

#+begin_quote
  Print one line of command line help, similar to Usage above.
#+end_quote

* AUTHOR
Gershon Elber.
