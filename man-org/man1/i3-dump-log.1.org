#+TITLE: Man1 - i3-dump-log.1
#+DESCRIPTION: Linux manpage for i3-dump-log.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
i3-dump-log - dumps the i3 SHM log

* SYNOPSIS
i3-dump-log [-s <socketpath>] [-f]

* DESCRIPTION
Debug versions of i3 automatically use 1% of your RAM (but 25 MiB max)
to store full debug log output. This is extremely helpful for bugreports
and figuring out what is going on, without permanently logging to a
file.

With i3-dump-log, you can dump the SHM log to stdout.

The -f flag works like tail -f, i.e. the process does not terminate
after dumping the log, but prints new lines as they appear.

* EXAMPLE
i3-dump-log | gzip -9 > /tmp/i3-log.gz

* SEE ALSO
i3(1)

* AUTHOR
Michael Stapelberg and contributors
