#+TITLE: Man1 - sf2extract.1
#+DESCRIPTION: Linux manpage for sf2extract.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sf2extract - Extract samples from SoundFont version 2 (.sf2) files.

* SYNOPSIS
*sf2extract* [ -v ] SF2FILE DESTDIR [SAMPLENR] [ [SAMPLENR] ... ]

* DESCRIPTION
Extract samples from SoundFont version 2 (.sf2) files. All extracted
samples will be written in .wav format. You must at least supply name of
the .sf2 input file and an output path where all extracted samples
should be written to. By default *sf2extract* extracts all samples
contained in the SoundFont file, even if they are not linked by any
instrument in the .sf2 file, but you can also extract only particular
samples by supplying a list of samples indices at the end of the command
line. You can use the *sf2dump*(1) tool to see the list of available
samples and their sample indices of a SoundFont file.

*Stereo Samples:* Note that stereo samples are always stored as pair of
separate two mono samples with Sound Font files. Keep that in mind when
selecting individual samples by index. *sf2dump*(1) shows you which
(mono) sample is linked to which other (mono) sample to form a stereo
sample pair. *sf2extract* automatically detects if one of the samples is
part of a stereo pair and automatically extracts the two as combined
interleaved stereo sample data to the output .wav file. Hence it is
sufficient to pass the index of one of the two (mono) samples of each
stereo pair you want to extract.

* OPTIONS
- * SF2FILE* :: filename of the input SoundFont file

- * DESTDIR* :: output path where all samples should be extracted to

- * SAMPLENR* :: optional index of sample(s) to be exclusively extracted

- * -v* :: print version and exit

* SEE ALSO
*sf2dump(1),* *gigextract(1),* *gigdump(1)*

* BUGS
Check and report bugs at http://bugs.linuxsampler.org

* Author
Application and manual page written by Christian Schoenebeck
<cuse@users.sf.net>
