#+TITLE: Man1 - doxygen.1
#+DESCRIPTION: Linux manpage for doxygen.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
doxygen - documentation system for various programming languages

* DESCRIPTION
Doxygen is a documentation system for C++, C, Java, Objective-C, IDL
(Corba and Microsoft flavors), Fortran, Python, VHDL and to some extent
PHP, C#, and D.

You can use doxygen in a number of ways:

- 1) Use doxygen to generate a template configuration file: :: 

  doxygen [-s] *-g* [configName]

- 2) Use doxygen to update an old configuration file: :: 

  doxygen [-s] *-u* [configName]

- 3) Use doxygen to generate documentation using an existing
  configuration file: :: 

  doxygen [configName]

- 4) Use doxygen to generate a template file controlling the layout of
  the generated documentation: :: 

  doxygen -l [layoutFileName]

  In case layoutFileName is omitted layoutFileName.xml will be used as
  filename. If - is used for layoutFileName doxygen will write to
  standard output.

- 5) Use doxygen to generate a template style sheet file for RTF, HTML
  or Latex. :: 

  RTF: doxygen *-w* rtf styleSheetFile

  HTML: doxygen *-w* html headerFile footerFile styleSheetFile
  [configFile]

  LaTeX: doxygen *-w* latex headerFile footerFile styleSheetFile
  [configFile]

- 6) Use doxygen to generate an rtf extensions file :: 

  RTF: doxygen *-e* rtf extensionsFile

  If - is used for extensionsFile doxygen will write to standard output.

- 7) Use doxygen to compare the used configuration file with the
  template configuration file :: 

  doxygen *-x* [configFile]

- 8) Use doxygen to show a list of built-in emojis. :: 

  doxygen *-f* emoji outputFileName

  If - is used for outputFileName doxygen will write to standard output.

If *-s* is specified the comments in the config file will be omitted. If
configName is omitted `Doxyfile' will be used as a default. If - is used
for configFile doxygen will write / read the configuration to /from
standard output / input.

* AUTHOR
Doxygen version 1.9.2, Copyright Dimitri van Heesch 1997-2019

* SEE ALSO
doxywizard(1).
