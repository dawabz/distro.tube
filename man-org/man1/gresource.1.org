#+TITLE: Man1 - gresource.1
#+DESCRIPTION: Linux manpage for gresource.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gresource - GResource tool

* SYNOPSIS
*gresource* [--section /SECTION/] list /FILE/ [/PATH/]

*gresource* [--section /SECTION/] details /FILE/ [/PATH/]

*gresource* [--section /SECTION/] extract /FILE/ /PATH/

*gresource* sections /FILE/

*gresource* help [/COMMAND/]

* DESCRIPTION
*gresource* offers a simple commandline interface to *GResource*. It
lets you list and extract resources that have been compiled into a
resource file or included in an elf file (a binary or a shared library).

The file to operate on is specified by the /FILE/ argument.

If an elf file includes multiple sections with resources, it is possible
to select which one to operate on with the --section option. Use the
sections command to find available sections.

* COMMANDS
*list*

#+begin_quote
  Lists resources. If /SECTION/ is given, only list resources in this
  section. If /PATH/ is given, only list matching resources.
#+end_quote

*details*

#+begin_quote
  Lists resources with details. If /SECTION/ is given, only list
  resources in this section. If /PATH/ is given, only list matching
  resources. Details include the section, size and compression of each
  resource.
#+end_quote

*extract*

#+begin_quote
  Extracts the resource named by /PATH/ to stdout. Note that resources
  may contain binary data.
#+end_quote

*sections*

#+begin_quote
  Lists sections containing resources. This is only interesting if
  /FILE/ is an elf file.
#+end_quote

*help*

#+begin_quote
  Prints help and exits.
#+end_quote
