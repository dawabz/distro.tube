#+TITLE: Manpages - dvigif.1
#+DESCRIPTION: Linux manpage for dvigif.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about dvigif.1 is found in manpage for: [[../man1/dvipng.1][man1/dvipng.1]]