#+TITLE: Man1 - chsh.1
#+DESCRIPTION: Linux manpage for chsh.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
chsh - change your login shell

* SYNOPSIS
*chsh* [*-s* /shell/] [*-l*] [*-h*] [*-v*] [/username/]

* DESCRIPTION
*chsh* is used to change your login shell. If a shell is not given on
the command line, *chsh* prompts for one.

*chsh* supports non-local entries (kerberos, LDAP, etc.) if linked with
libuser, otherwise use *ypchsh*(1), *lchsh*(1) or any other
implementation for non-local entries.

* OPTIONS
*-s*, *--shell* /shell/

#+begin_quote
  Specify your login shell.
#+end_quote

*-l*, *--list-shells*

#+begin_quote
  Print the list of shells listed in //etc/shells/ and exit.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help text and exit.
#+end_quote

*-v*, *--version*

#+begin_quote
  Display version information and exit.
#+end_quote

* VALID SHELLS
*chsh* will accept the full pathname of any executable file on the
system.

The default behavior for non-root users is to accept only shells listed
in the //etc/shells/ file, and issue a warning for root user. It can
also be configured at compile-time to only issue a warning for all
users.

* EXIT STATUS
Returns 0 if operation was successful, 1 if operation failed or command
syntax was not valid.

* AUTHORS
* SEE ALSO
*login*(1), *login.defs*(5), *passwd*(5), *shells*(5)

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *chsh* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
