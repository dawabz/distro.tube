#+TITLE: Man1 - cargo-update.1
#+DESCRIPTION: Linux manpage for cargo-update.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cargo-update - Update dependencies as recorded in the local lock file

* SYNOPSIS
*cargo update* [/options/]

* DESCRIPTION
This command will update dependencies in the *Cargo.lock* file to the
latest version. If the *Cargo.lock* file does not exist, it will be
created with the latest available versions.

* OPTIONS
** Update Options
*-p* /spec/..., *--package* /spec/...

#+begin_quote
  Update only the specified packages. This flag may be specified
  multiple times. See *cargo-pkgid*(1) for the SPEC format.

  If packages are specified with the *-p* flag, then a conservative
  update of the lockfile will be performed. This means that only the
  dependency specified by SPEC will be updated. Its transitive
  dependencies will be updated only if SPEC cannot be updated without
  updating dependencies. All other dependencies will remain locked at
  their currently recorded versions.

  If *-p* is not specified, all dependencies are updated.
#+end_quote

*--aggressive*

#+begin_quote
  When used with *-p*, dependencies of /spec/ are forced to update as
  well. Cannot be used with *--precise*.
#+end_quote

*--precise* /precise/

#+begin_quote
  When used with *-p*, allows you to specify a specific version number
  to set the package to. If the package comes from a git repository,
  this can be a git revision (such as a SHA hash or tag).
#+end_quote

*-w*, *--workspace*

#+begin_quote
  Attempt to update only packages defined in the workspace. Other
  packages are updated only if they don't already exist in the lockfile.
  This option is useful for updating *Cargo.lock* after you've changed
  version numbers in *Cargo.toml*.
#+end_quote

*--dry-run*

#+begin_quote
  Displays what would be updated, but doesn't actually write the
  lockfile.
#+end_quote

** Display Options
*-v*, *--verbose*

#+begin_quote
  Use verbose output. May be specified twice for "very verbose" output
  which includes extra output such as dependency warnings and build
  script output. May also be specified with the *term.verbose* /config
  value/ <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

*-q*, *--quiet*

#+begin_quote
  No output printed to stdout.
#+end_quote

*--color* /when/

#+begin_quote
  Control when colored output is used. Valid values:

  #+begin_quote
    ·*auto* (default): Automatically detect if color support is
    available on the terminal.
  #+end_quote

  #+begin_quote
    ·*always*: Always display colors.
  #+end_quote

  #+begin_quote
    ·*never*: Never display colors.
  #+end_quote

  May also be specified with the *term.color* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

** Manifest Options
*--manifest-path* /path/

#+begin_quote
  Path to the *Cargo.toml* file. By default, Cargo searches for the
  *Cargo.toml* file in the current directory or any parent directory.
#+end_quote

*--frozen*, *--locked*

#+begin_quote
  Either of these flags requires that the *Cargo.lock* file is
  up-to-date. If the lock file is missing, or it needs to be updated,
  Cargo will exit with an error. The *--frozen* flag also prevents Cargo
  from attempting to access the network to determine if it is
  out-of-date.

  These may be used in environments where you want to assert that the
  *Cargo.lock* file is up-to-date (such as a CI build) or want to avoid
  network access.
#+end_quote

*--offline*

#+begin_quote
  Prevents Cargo from accessing the network for any reason. Without this
  flag, Cargo will stop with an error if it needs to access the network
  and the network is not available. With this flag, Cargo will attempt
  to proceed without the network if possible.

  Beware that this may result in different dependency resolution than
  online mode. Cargo will restrict itself to crates that are downloaded
  locally, even if there might be a newer version as indicated in the
  local copy of the index. See the *cargo-fetch*(1) command to download
  dependencies before going offline.

  May also be specified with the *net.offline* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

** Common Options
*+*/toolchain/

#+begin_quote
  If Cargo has been installed with rustup, and the first argument to
  *cargo* begins with *+*, it will be interpreted as a rustup toolchain
  name (such as *+stable* or *+nightly*). See the /rustup documentation/
  <https://rust-lang.github.io/rustup/overrides.html> for more
  information about how toolchain overrides work.
#+end_quote

*-h*, *--help*

#+begin_quote
  Prints help information.
#+end_quote

*-Z* /flag/

#+begin_quote
  Unstable (nightly-only) flags to Cargo. Run *cargo -Z help* for
  details.
#+end_quote

* ENVIRONMENT
See /the reference/
<https://doc.rust-lang.org/cargo/reference/environment-variables.html>
for details on environment variables that Cargo reads.

* EXIT STATUS

#+begin_quote
  ·*0*: Cargo succeeded.
#+end_quote

#+begin_quote
  ·*101*: Cargo failed to complete.
#+end_quote

* EXAMPLES

#+begin_quote
  1.Update all dependencies in the lockfile:

  #+begin_quote
    #+begin_example
      cargo update
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  2.Update only specific dependencies:

  #+begin_quote
    #+begin_example
      cargo update -p foo -p bar
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  3.Set a specific dependency to a specific version:

  #+begin_quote
    #+begin_example
      cargo update -p foo --precise 1.2.3
    #+end_example
  #+end_quote
#+end_quote

* SEE ALSO
*cargo*(1), *cargo-generate-lockfile*(1)
