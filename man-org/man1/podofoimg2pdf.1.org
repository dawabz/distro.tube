#+TITLE: Man1 - podofoimg2pdf.1
#+DESCRIPTION: Linux manpage for podofoimg2pdf.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
podofoimg2pdf - Convert images to PDF files

* SYNOPSIS
*podofoimg2pdf* [output.pdf] [-useimgsize] [image1 image2 image3 ...]

* DESCRIPTION
*podofoimg2pdf* is one of the command line tools from the PoDoFo library
that provide several useful operations to work with PDF files. This tool
will combine any number of images into a single PDF. This is useful for
creating a document from scanned images. Large pages will be scaled to
fit the page and images smaller than the defined page size will be
centered.

Supported image formats:

#+begin_quote
  JPEG\\
  PNG\\
  TIFF
#+end_quote

* OPTIONS
*-useimgsize*

#+begin_quote
  Use the image size as page size instead of A4
#+end_quote

* SEE ALSO
*podofobox*(1), *podofocolor*(1), *podofocountpages*(1),
*podofocrop*(1), *podofoencrypt*(1), *podofogc*(1),
*podofoimgextract*(1), *podofoincrementalupdates*(1), *podofoimpose*(1),
*podofomerge*(1), *podofopages*(1), *podofopdfinfo*(1),
*podofotxt2pdf*(1), *podofotxtextract*(1), *podofouncompress*(1),
*podofoxmp*(1)

* AUTHORS
PoDoFo is written by Dominik Seichter <domseichter@web.de> and others.

This manual page was written by Oleksandr Moskalenko <malex@debian.org>
for the Debian Project (but may be used by others).
