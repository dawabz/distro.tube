#+TITLE: Man1 - grub-customizer.1
#+DESCRIPTION: Linux manpage for grub-customizer.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
grub-customizer - a graphical grub2/burg editor

* SYNOPSIS
*grub-customizer*

* DESCRIPTION
Grub Customizer is an application which provides an easy way to edit the
configuration of Grub 2 or BURG. The base functionality of Grub
Customizer is to rename, reorder or delete the menu entries - but you
can also configure settings like default entry, timeouts, backgrounds
and much more.

Grub Customizer also helps you restoring your bootloader by providing an
easy way to access the configuration of other partitions.

* AUTHOR
Daniel Richter (2007-2011) <danielrichter2007@web.de>.

* COPYRIGHT
Copyright (C) 2011 Daniel Richter <danielrichter2007@web.de>.
