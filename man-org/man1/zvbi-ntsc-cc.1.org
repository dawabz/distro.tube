#+TITLE: Man1 - zvbi-ntsc-cc.1
#+DESCRIPTION: Linux manpage for zvbi-ntsc-cc.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zvbi-ntsc-cc - closed caption decoder

* SYNOPSIS
*zvbi-ntsc-cc [ options ]*

* DESCRIPTION
*zvbi-ntsc-cc* reads vbi data from /dev/vbi and decodes the enclosed cc
data. Start it with '-h' to get a list of cmd line options.

* AUTHORS
timecop@japan.co.jp\\
Mike Baker <mbm@linux.com>\\
Adam <adam@cfar.umd.edu>

* COPYRIGHT
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
675 Mass Ave, Cambridge, MA 02139, USA.
