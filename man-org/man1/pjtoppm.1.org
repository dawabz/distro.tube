#+TITLE: Man1 - pjtoppm.1
#+DESCRIPTION: Linux manpage for pjtoppm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pjtoppm - convert an HP PaintJet file to a PPM image

* SYNOPSIS
*pjtoppm*

[/paintjet/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pjtoppm* reads an HP PaintJet file as input and converts it into a PPM
image. This was a quick hack to save some trees, and it only handles a
small subset of the paintjet commands. In particular, it will only
handle enough commands to convert most raster image files.

* REFERENCES
HP PaintJet XL Color Graphics Printer User's Guide

* SEE ALSO
*ppmtopj*(1)

* AUTHOR
Copyright (C) 1991 by Christos Zoulas.
