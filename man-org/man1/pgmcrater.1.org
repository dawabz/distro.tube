#+TITLE: Man1 - pgmcrater.1
#+DESCRIPTION: Linux manpage for pgmcrater.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pgmcrater - create cratered terrain by fractal forgery

* SYNOPSIS
*pgmcrater*

[*-number* /n/]

[*-height*|*-ysize* /s/]

[*-width*|*-xsize* /s/]

[*-gamma* /g/]

[*-randomseed=*/integer/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pgmcrater* creates a PGM image which mimics cratered terrain. The PGM
image is created by simulating the impact of a given number of craters
with random position and size, then rendering the resulting terrain
elevations based on a light source shining from one side of the screen.
The size distribution of the craters is based on a power law which
results in many more small craters than large ones. The number of
craters of a given size varies as the reciprocal of the area as
described on pages 31 and 32 of Peitgen and Saupe[1]; cratered bodies in
the Solar System are observed to obey this relationship. The formula
used to obtain crater radii governed by this law from a uniformly
distributed pseudorandom sequence was developed by Rudy Rucker.

High resolution images with large numbers of craters often benefit from
being piped through *pnmsmooth*. The averaging performed by this process
eliminates some of the jagged pixels and lends a mellow ``telescopic
image'' feel to the overall picture.

*pgmcrater* simulates only small craters, which are hemispherical in
shape (regardless of the incidence angle of the impacting body, as long
as the velocity is sufficiently high). Large craters, such as Copernicus
and Tycho on the Moon, have a ``walled plain'' shape with a
cross-section more like:

#+begin_example
                  /\                            /\
            _____/  \____________/\____________/  \_____
#+end_example

Larger craters should really use this profile, including the central
peak, and totally obliterate the pre-existing terrain.

The randomness in the image is limited before Netpbm 10.37
(December 2006) -- if you run the program twice in the same second, you
may get identical output.

* OPTIONS
All options can be abbreviated to their shortest unique prefix.

- *-number* /n/ :: Causes /n/ craters to be generated. If no *-number*
  specification is given, 50000 craters will be generated. Don't expect
  to see them all! For every large crater there are many, many more tiny
  ones which tend simply to erode the landscape. In general, the more
  craters you specify the more realistic the result; ideally you want
  the entire terrain to have been extensively turned over again and
  again by cratering. High resolution images containing five to ten
  million craters are stunning but take quite a while to create.

- *-height* /height/ :: Sets the height of the generated image to
  /height/ pixels. The default height is 256 pixels.

- *-width* /width/ :: Sets the width of the generated image to /width/
  pixels. The default width is 256 pixels.

- *-xsize* /width/ :: Sets the width of the generated image to /width/
  pixels. The default width is 256 pixels.

- *-ysize* /height/ :: Sets the height of the generated image to
  /height/ pixels. The default height is 256 pixels.

- *-gamma* /factor/ :: The specified /factor/ is used to gamma adjust
  the image in the same manner as performed by *pnmgamma*. The default
  value is 1.0, which results in a medium contrast image. Values larger
  than 1 lighten the image and reduce contrast, while values less than 1
  darken the image, increasing contrast.

Note that this is separate from the gamma correction that is part of the
definition of the PGM format. The image *pnmgamma* generates is a
genuine, gamma-corrected PGM image in any case. This option simply
changes the contrast and may compensate for a display device that does
not correctly render PGM images.

- *-randomseed=*/integer/ :: This is the seed for the random number
  generator that generates the pixels.

Use this to ensure you get the same image on separate invocations.

By default, *pgmnoise* uses a seed derived from the time of day and
process ID, which gives you fairly uncorrelated results in multiple
invocations.

This option was new in Netpbm 10.61 (December 2012).

* DESIGN NOTES
The*-gamma* option isn't really necessary since you can achieve the same
effect by piping the output from *pgmcrater* through *pnmgamma*.
However, *pgmcrater* performs an internal gamma map anyway in the
process of rendering the elevation array into the PGM format, so there's
no additional overhead in allowing an additional gamma adjustment.

Real craters have two distinct morphologies.

* SEE ALSO
*pnmgamma*(1) , *pnmsmooth*(1)

*pgm*(5) ,

- [1] :: Peitgen, H.-O., and Saupe, D. eds., The Science Of Fractal
  Images, New York: Springer Verlag, 1988.

* AUTHOR
#+begin_example
  John Walker
  Autodesk SA
  Avenue des Champs-Montants 14b
  CH-2074 MARIN
  Suisse/Schweiz/Svizzera/Svizra/Switzerland
      Usenet:kelvin@Autodesk.com
      Fax:038/33 88 15
      Voice:038/33 76 33
#+end_example

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted, without
any conditions or restrictions. This software is provided 'as is'
without express or implied warranty.

* HISTORY
The original 1991 version of this manual contains the following:

** PLUGWARE!
If you like this kind of stuff, you may also enjoy 'James Gleick's
Chaos--The Software' for MS-DOS, available for $59.95 from your local
software store or directly from Autodesk, Inc., Attn: Science Series,
2320 Marinship Way, Sausalito, CA 94965, USA. Telephone: (800) 688-2344
toll-free or, outside the U.S. (415) 332-2344 Ext 4886. Fax: (415)
289-4718. 'Chaos--The Software' includes a more comprehensive fractal
forgery generator which creates three-dimensional landscapes as well as
clouds and planets, plus five more modules which explore other aspects
of Chaos. The user guide of more than 200 pages includes an introduction
by James Gleick and detailed explanations by Rudy Rucker of the
mathematics and algorithms used by each program.
