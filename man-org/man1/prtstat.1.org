#+TITLE: Man1 - prtstat.1
#+DESCRIPTION: Linux manpage for prtstat.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
prtstat - print statistics of a process

* SYNOPSIS
*prtstat* [*-r*|*--raw*] /pid/\\
*prtstat* *-V*|*--version*

* DESCRIPTION
*prtstat* prints the statistics of the specified process. This
information comes from the */proc/*/pid/*/stat* file.

* OPTIONS
- *-r*,* --raw* :: Print the information in raw format.

- *-V*,* --version* :: Show the version information for *prtstat*.

* FILES
- */proc/*/pid/*/stat* :: source of the information *prtstat* uses.
