#+TITLE: Man1 - pnmalias.1
#+DESCRIPTION: Linux manpage for pnmalias.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pnmalias - antialias a PNM image

* SYNOPSIS
*pnmalias*

[*-bgcolor* /color/]

[*-fgcolor* /color/]

[*-bonly*]

[*-fonly*]

[*-balias*]

[*-falias*]

[*-weight* /w/]

[/pnmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pnmalias* reads a PNM image as input, and applies anti-aliasing to
background and foreground pixels. If the input file is a PBM, *pnmalias*
promotes the output anti-aliased image to a PGM, and prints a message
informing the user of the change in format.

* OPTIONS
*-bgcolor* /colorb/ sets the background color the /colorb/.

*-fgcolor* /colorf/ sets the foreground color to /colorf/.

Pixels with these values will be anti-aliased. By default, *pnmalias*
takes the background color to be black, and foreground color to be
white.

Specify the color (/color/) as described for the
[[file:libppm.html#colorname][argument of the *ppm_parsecolor()* library
routine]] .

Note that even when dealing with PGMs, background and foreground colors
need to be specified in the fashion described above. In this case,
*pnmalias* takes the background and foreground pixel values to be the
value of the red component for the specified color.

*-bonly* says to apply anti-aliasing only to the background pixels.

*-fonly* says to apply anti-aliasing only to the foreground pixels.

*-balias* says to apply anti-aliasing to all pixels surrounding
background pixels.

*-falias* says to apply anti-aliasing to all pixels surrounding
foreground pixels.

If you specify neither *-balias* nor *-falias*, *pnmalias* applies
anti-aliasing only among neighboring background and foreground pixels.

*-weight* /w/ says to use /w/ as the central weight for the aliasing
filter. /w/ must be a real number in the range 0 < /w/ < 1. The lower
the value of /w/ is, the 'blurrier' the output image is. The default is
w = 1/3.

* SEE ALSO
*pbmtext*(1) , *pnmsmooth*(1) , *pnm*(5)

* AUTHOR
Copyright (C) 1992 by Alberto Accomazzi, Smithsonian Astrophysical
Observatory.
