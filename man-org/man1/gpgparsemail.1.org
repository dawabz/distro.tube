#+TITLE: Man1 - gpgparsemail.1
#+DESCRIPTION: Linux manpage for gpgparsemail.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*gpgparsemail* - Parse a mail message into an annotated format

* SYNOPSIS
*gpgparsemail* [/options/] [/file/]

* DESCRIPTION
The *gpgparsemail* is a utility currently only useful for debugging. Run
it with *--help* for usage information.
