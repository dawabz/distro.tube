#+TITLE: Man1 - smbget.1
#+DESCRIPTION: Linux manpage for smbget.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
smbget - wget-like utility for download files over SMB

* SYNOPSIS
smbget [-a, --guest] [-r, --resume] [-R, --recursive] [-U,
--user=STRING] [-w, --workgroup=STRING] [-n, --nonprompt] [-d,
--debuglevel=INT] [-D, --dots] [-o, --outputfile] [-f, --rcfile] [-q,
--quiet] [-v, --verbose] [-b, --blocksize] [-O, --stdout] [-u, --update]
[-?, --help] [--usage] {smb://host/share/path/to/file} [smb://url2/]
[...]

* DESCRIPTION
This tool is part of the *samba*(7) suite.

smbget is a simple utility with wget-like semantics, that can download
files from SMB servers. You can specify the files you would like to
download on the command-line.

The files should be in the smb-URL standard, e.g. use
smb://host/share/file for the UNC path /\\\\HOST\\SHARE\\file/.

* OPTIONS
-a, --guest

#+begin_quote
  Work as user guest
#+end_quote

-r, --resume

#+begin_quote
  Automatically resume aborted files
#+end_quote

-R, --recursive

#+begin_quote
  Recursively download files
#+end_quote

-U, --user=/username[%password]/

#+begin_quote
  Username (and password) to use
#+end_quote

-w, --workgroup=STRING

#+begin_quote
  Workgroup to use (optional)
#+end_quote

-n, --nonprompt

#+begin_quote
  Dont ask anything (non-interactive)
#+end_quote

-d, --debuglevel=INT

#+begin_quote
  Debuglevel to use
#+end_quote

-D, --dots

#+begin_quote
  Show dots as progress indication
#+end_quote

-o, --outputfile

#+begin_quote
  Write the file that is being downloaded to the specified file. Can not
  be used together with -R.
#+end_quote

-O, --stdout

#+begin_quote
  Write the file that is being downloaded to standard output.
#+end_quote

-f, --rcfile

#+begin_quote
  Use specified rcfile. This will be loaded in the order it was
  specified - e.g. if you specify any options before this one, they
  might get overridden by the contents of the rcfile.
#+end_quote

-q, --quiet

#+begin_quote
  Be quiet
#+end_quote

-v, --verbose

#+begin_quote
  Be verbose
#+end_quote

-b, --blocksize

#+begin_quote
  Number of bytes to download in a block. Defaults to 64000.
#+end_quote

-?, --help

#+begin_quote
  Show help message
#+end_quote

--usage

#+begin_quote
  Display brief usage message
#+end_quote

-u, --update

#+begin_quote
  Download only when remote file is newer than local file or local file
  is missing.
#+end_quote

* SMB URLS
SMB URLs should be specified in the following format:

#+begin_quote
  #+begin_example
    smb://[[[domain;]user[:password@]]server[/share[/path[/file]]]]
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    smb:// means all the workgroups
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    smb://name/ means, if name is a workgroup, all the servers in this workgroup, or if name is a server, all the shares on this server.
  #+end_example
#+end_quote

* EXAMPLES

#+begin_quote
  #+begin_example
    # Recursively download src directory
    smbget -R smb://rhonwyn/jelmer/src
    # Download FreeBSD ISO and enable resuming
    smbget -r smb://rhonwyn/isos/FreeBSD5.1.iso
    # Recursively download all ISOs
    smbget -Rr smb://rhonwyn/isos
    # Backup my data on rhonwyn
    smbget -Rr smb://rhonwyn/
  #+end_example
#+end_quote

* BUGS
Permission denied is returned in some cases where the cause of the error
is unknown (such as an illegally formatted smb:// url or trying to get a
directory without -R turned on).

* VERSION
This man page is part of version 4.15.3 of the Samba suite.

* AUTHOR
The original Samba software and related utilities were created by Andrew
Tridgell. Samba is now developed by the Samba Team as an Open Source
project similar to the way the Linux kernel is developed.

The smbget manpage was written by Jelmer Vernooij.
