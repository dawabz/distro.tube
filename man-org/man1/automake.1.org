#+TITLE: Manpages - automake.1
#+DESCRIPTION: Linux manpage for automake.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about automake.1 is found in manpage for: [[../man1/automake-1.16.1][man1/automake-1.16.1]]