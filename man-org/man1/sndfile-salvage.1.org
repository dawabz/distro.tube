#+TITLE: Man1 - sndfile-salvage.1
#+DESCRIPTION: Linux manpage for sndfile-salvage.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Audio files using the WAV file container are inherently limited to 4G of
data size fields in the WAV header being stored as unsigned 32bit
integers. Many applications have trouble with these WAV files that are
more the 4G in size.

rewrites the WAV file into a W64 file with the same audio content. This
file is overwritten if it already exists.
