#+TITLE: Man1 - katedec.1
#+DESCRIPTION: Linux manpage for katedec.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
katedec - decodes Kate streams to a textual description

* SYNOPSIS
*katedec* *[-hVv]* *[-o */outfile/*]* *[-t */type/*]* [ infile ]

* DESCRIPTION
*katedec* decodes Kate streams to libkate's custom description language.

* OPTIONS
- *-h* :: Show command line help.

- *-V* :: Show version information.

- *-v* :: Increase verbosity.

- *-o* outfile :: Write the output description file to the given file
  name (writes to stdout if not specified). It is possible to write to
  multiple files (eg, if the input contains several Kate streams) by
  passing printf style formatting tags in the filename. Formatting tags
  are strings starting with a '%' character, followed by the tag itself.
  Known tags are: % replaced by a verbatim % character l replaced by the
  stream language c replaced by the stream category s replaced by the
  stream serial number, in hexadecimal format i replaced by the index of
  the stream in the input file, starting from 0

- *-t* type :: Set the output format type: kate: Kate description format
  srt: SubRip format (non text information will be lost)

* EXAMPLES
Decodes a Kate stream to a textual description:

katedec -o output.kate input.ogg

Decodes several multiplexed Kate streams to SubRip files:

katedec -t srt -o output.%l.kate input.ogg

* SEE ALSO
*kateenc*(1), *katalyzer*(1)
