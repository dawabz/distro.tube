#+TITLE: Man1 - kpsereadlink.1
#+DESCRIPTION: Linux manpage for kpsereadlink.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
kpsereadlink - print contents of symbolic link

* SYNOPSIS
*kpsereadlink* /file/

* DESCRIPTION
If /file/ is a symbolic link, print its contents (what it links to), and
exit successfully. Exit with a failure otherwise.

On systems that do not support symbolic links, *kpsereadlink* will
always fail.

* OPTIONS
*kpsereadlink* accepts the following options:

- *--help* :: Print help message and exit.

- *--version* :: Print version information and exit.

* SEE ALSO
*readlink*(2)
