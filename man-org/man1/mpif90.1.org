#+TITLE: Man1 - mpif90.1
#+DESCRIPTION: Linux manpage for mpif90.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
mpif77, mpif90 -- Deprecated Open MPI Fortran wrapper compilers

* SYNTAX
mpif90 ...

* DESCRIPTION
The /mpif77/ and /mpif90/ wrapper compiler names are deprecated, and
will disappear in a future version of Open MPI. You should use the
/mpifort/ wrapper compiler, instead. While they are deprecated, /mpif77/
and /mpif90/ accept all the same parameters as /mpifort/, and behaves
the same as /mpifort/.

With /mpifort/, you can compile any Fortran program that uses the
"mpif.h", "use mpi", and/or "use mpi_f08" MPI Fortran interfaces.

See mpifort(1) for more details.

* SEE ALSO
mpifort(1)
