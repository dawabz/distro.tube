#+TITLE: Man1 - pnmcut.1
#+DESCRIPTION: Linux manpage for pnmcut.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
*pnmcut* - replaced by pamcut

<H2 >DESCRIPTION</H2>

This program is part of *Netpbm*(1)

*pnmcut* was obsoleted by **pamcut**(1) , introduced with Netpbm 9.20
(May 2001). *pamcut* is backward compatible with *pnmcut*, plus adds
many additional functions, including the ability to process PAM images.

*pnmcut* remained in the Netpbm package until Netpbm 10.46 (March 2009)
because of hopes that it had fewer bugs than *pamcut* because of its
age. But now it would just be clutter.

In Netpbm before 9.20, use the manual for *pamcut* with *pnmcut*.
Features that are in *pamcut* but not *pnmcut* are indicated by
statements that they didn't exist before 9.20.
