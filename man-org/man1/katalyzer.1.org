#+TITLE: Man1 - katalyzer.1
#+DESCRIPTION: Linux manpage for katalyzer.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
katalyzer - analyzes Kate streams and displays various information about
them.

* SYNOPSIS
*katalyzer* *[-hV]* *[-l */types/*]* *[-l!]* [ infile ]

* DESCRIPTION
*katalyzer* analyzes Kate streams and displays information about them.
The type of information to select is selectable on the command line.
Several multiplexed Kate streams may be analyzed at the same time.

* OPTIONS
- *-h* :: Show command line help.

- *-V* :: Show version information.

- *-l* types :: Select the types of information to log, from the list
  below. These characters may be concatenated in a single -l option, as,
  eg, *ls* does (eg, *katalyzer* -l Ttv). Refer to the *katalyzer*
  command line help for a fully up to date list of types. e: errors p:
  packet c: container specific information T: Timing t: text v: event d:
  dump page and packets S: statistics -: miscellaneous information that
  doesn't belong in any other type

- *-l!* :: Shorthand to select all information to log.

* EXAMPLES
Analyzes any Kate stream in an Ogg file:

katalyzer input.ogg

Analyzes any Kate stream in an Ogg file, displaying only errors:

katalyzer -l e input.ogg

* SEE ALSO
*katedec*(1), *kateenc*(1)
