#+TITLE: Man1 - git-patch-id.1
#+DESCRIPTION: Linux manpage for git-patch-id.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
git-patch-id - Compute unique ID for a patch

* SYNOPSIS
#+begin_example
  git patch-id [--stable | --unstable]
#+end_example

* DESCRIPTION
Read a patch from the standard input and compute the patch ID for it.

A "patch ID" is nothing but a sum of SHA-1 of the file diffs associated
with a patch, with whitespace and line numbers ignored. As such, it's
"reasonably stable", but at the same time also reasonably unique, i.e.,
two patches that have the same "patch ID" are almost guaranteed to be
the same thing.

IOW, you can use this thing to look for likely duplicate commits.

When dealing with /git diff-tree/ output, it takes advantage of the fact
that the patch is prefixed with the object name of the commit, and
outputs two 40-byte hexadecimal strings. The first string is the patch
ID, and the second string is the commit ID. This can be used to make a
mapping from patch ID to commit ID.

* OPTIONS
--stable

#+begin_quote
  Use a "stable" sum of hashes as the patch ID. With this option:

  #+begin_quote
    ·

    Reordering file diffs that make up a patch does not affect the ID.
    In particular, two patches produced by comparing the same two trees
    with two different settings for "-O<orderfile>" result in the same
    patch ID signature, thereby allowing the computed result to be used
    as a key to index some meta-information about the change between the
    two trees;
  #+end_quote

  #+begin_quote
    ·

    Result is different from the value produced by git 1.9 and older or
    produced when an "unstable" hash (see --unstable below) is
    configured - even when used on a diff output taken without any use
    of "-O<orderfile>", thereby making existing databases storing such
    "unstable" or historical patch-ids unusable.

    #+begin_quote
      #+begin_example
        This is the default if patchid.stable is set to true.
      #+end_example
    #+end_quote
  #+end_quote
#+end_quote

--unstable

#+begin_quote
  Use an "unstable" hash as the patch ID. With this option, the result
  produced is compatible with the patch-id value produced by git 1.9 and
  older. Users with pre-existing databases storing patch-ids produced by
  git 1.9 and older (who do not deal with reordered patches) may want to
  use this option.

  #+begin_quote
    #+begin_example
      This is the default.
    #+end_example
  #+end_quote
#+end_quote

* GIT
Part of the *git*(1) suite
