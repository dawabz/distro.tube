#+TITLE: Man1 - icontopbm.1
#+DESCRIPTION: Linux manpage for icontopbm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
icontopbm - convert a Sun icon image to PBM

* SYNOPSIS
*icontopbm* [/iconfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*icontopbm* was obsoleted by **sunicontopnm**(1) , introduced with
Netpbm 10.53 (December 2010). *sunicontopnm* is backward compatible with
*icontopbm*, plus adds additional functions, including the ability to
convert a Depth=8 Sun icon, producing a PGM image.

Starting in Release 10.53, *icontopbm* is just an alias for
*sunicontopnm*.

The point of the name change is that there are many kinds of icons in
the world besides Sun icons, and in 2010, the Sun variety isn't even
common.

In releases before 10.53, you can use the *sunicontopnm* documentation
for *icontopbm*, as long as you recognize that any change the
*sunicontopnm* manual says happened in or after Netpbm 10.53 doesn't
apply to *icontopbm*.
