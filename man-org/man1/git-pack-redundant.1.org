#+TITLE: Man1 - git-pack-redundant.1
#+DESCRIPTION: Linux manpage for git-pack-redundant.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
git-pack-redundant - Find redundant pack files

* SYNOPSIS
#+begin_example
  git pack-redundant [ --verbose ] [ --alt-odb ] < --all | .pack filename ... >
#+end_example

* DESCRIPTION
This program computes which packs in your repository are redundant. The
output is suitable for piping to *xargs rm* if you are in the root of
the repository.

/git pack-redundant/ accepts a list of objects on standard input. Any
objects given will be ignored when checking which packs are required.
This makes the following command useful when wanting to remove packs
which contain unreachable objects.

git fsck --full --unreachable | cut -d -f3 | \ git pack-redundant --all
| xargs rm

* OPTIONS
--all

#+begin_quote
  Processes all packs. Any filenames on the command line are ignored.
#+end_quote

--alt-odb

#+begin_quote
  Don't require objects present in packs from alternate object
  directories to be present in local packs.
#+end_quote

--verbose

#+begin_quote
  Outputs some statistics to stderr. Has a small performance penalty.
#+end_quote

* SEE ALSO
*git-pack-objects*(1) *git-repack*(1) *git-prune-packed*(1)

* GIT
Part of the *git*(1) suite
