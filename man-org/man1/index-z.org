#+TITLE: Man1 - Z
#+DESCRIPTION: Man1 - Z
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* Z
#+begin_src bash :exports results
readarray -t starts_with_z < <(find . -type f -iname "z*" | sort)

for x in "${starts_with_z[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./zbarcam.1.org][zbarcam.1]]        |
| [[file:./zbarimg.1.org][zbarimg.1]]        |
| [[file:./zcat.1.org][zcat.1]]           |
| [[file:./zcmp.1.org][zcmp.1]]           |
| [[file:./zdiff.1.org][zdiff.1]]          |
| [[file:./zeisstopnm.1.org][zeisstopnm.1]]     |
| [[file:./zenity.1.org][zenity.1]]         |
| [[file:./zforce.1.org][zforce.1]]         |
| [[file:./zgrep.1.org][zgrep.1]]          |
| [[file:./zip.1.org][zip.1]]            |
| [[file:./zipcloak.1.org][zipcloak.1]]       |
| [[file:./zipcmp.1.org][zipcmp.1]]         |
| [[file:./zipdetails.1perl.org][zipdetails.1perl]] |
| [[file:./zipgrep.1.org][zipgrep.1]]        |
| [[file:./zipinfo.1.org][zipinfo.1]]        |
| [[file:./zipmerge.1.org][zipmerge.1]]       |
| [[file:./zipnote.1.org][zipnote.1]]        |
| [[file:./zipsplit.1.org][zipsplit.1]]       |
| [[file:./ziptool.1.org][ziptool.1]]        |
| [[file:./zless.1.org][zless.1]]          |
| [[file:./zlib-flate.1.org][zlib-flate.1]]     |
| [[file:./zmore.1.org][zmore.1]]          |
| [[file:./znew.1.org][znew.1]]           |
| [[file:./zresample.1.org][zresample.1]]      |
| [[file:./zretune.1.org][zretune.1]]        |
| [[file:./zsh.1.org][zsh.1]]            |
| [[file:./zshall.1.org][zshall.1]]         |
| [[file:./zshbuiltins.1.org][zshbuiltins.1]]    |
| [[file:./zshcalsys.1.org][zshcalsys.1]]      |
| [[file:./zshcompctl.1.org][zshcompctl.1]]     |
| [[file:./zshcompsys.1.org][zshcompsys.1]]     |
| [[file:./zshcompwid.1.org][zshcompwid.1]]     |
| [[file:./zshcontrib.1.org][zshcontrib.1]]     |
| [[file:./zshexpn.1.org][zshexpn.1]]        |
| [[file:./zshmisc.1.org][zshmisc.1]]        |
| [[file:./zshmodules.1.org][zshmodules.1]]     |
| [[file:./zshoptions.1.org][zshoptions.1]]     |
| [[file:./zshparam.1.org][zshparam.1]]       |
| [[file:./zshroadmap.1.org][zshroadmap.1]]     |
| [[file:./zshtcpsys.1.org][zshtcpsys.1]]      |
| [[file:./zshzftpsys.1.org][zshzftpsys.1]]     |
| [[file:./zshzle.1.org][zshzle.1]]         |
| [[file:./zsoelim.1.org][zsoelim.1]]        |
| [[file:./zstd.1.org][zstd.1]]           |
| [[file:./zstdgrep.1.org][zstdgrep.1]]       |
| [[file:./zstdless.1.org][zstdless.1]]       |
| [[file:./zvbi-atsc-cc.1.org][zvbi-atsc-cc.1]]   |
| [[file:./zvbi-chains.1.org][zvbi-chains.1]]    |
| [[file:./zvbid.1.org][zvbid.1]]          |
| [[file:./zvbi-ntsc-cc.1.org][zvbi-ntsc-cc.1]]   |
