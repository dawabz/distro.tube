#+TITLE: Man1 - pbmtoybm.1
#+DESCRIPTION: Linux manpage for pbmtoybm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pgmtoybm - convert a PBM image into a Bennet Yee 'face' file

* SYNOPSIS
*pbmtoybm*

[/pbmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmtoybm* reads a PBM image as input and produces as output a file
acceptable to the *face* and *xbm* programs by Bennet Yee
(/bsy+@cs.cmu.edu/).

* SEE ALSO
*ybmtopbm*(1) , *pbm*(5) ,

* AUTHOR
Copyright (C) 1991 by Jamie Zawinski and Jef Poskanzer.
