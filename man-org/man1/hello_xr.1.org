#+TITLE: Man1 - hello_xr.1
#+DESCRIPTION: Linux manpage for hello_xr.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
is a sample application written using the

API.

The arguments are as follows:

Show brief usage instructions.

specify the graphics API to use. (Note that not that not all graphics
APIs are necessarily available on all systems.) The parameter

must be one of the following (case-insensitive):

Direct3D 11 (Windows-only)

Direct3D 12 (Windows-only)

Specify the form factor to use. (Note that you need a suitable XR system
and a runtime supporting a given form factor for it to work.) The
parameter

must be one of the following (case-insensitive):

Head-mounted display (default)

Specify the view configuration to use. (Note that you need a suitable XR
system and a runtime supporting a given view configuration for it to
work.) The parameter

must be one of the following (case-insensitive):

(default)

Specify the environment blend mode to use. (Note that you need a
suitable XR system and a runtime supporting a given environment blend
mode for it to work.) The parameter

must be one of the following (case-insensitive):

Specify the space to use. The parameter

must be one of the following (case-insensitive):

Enable verbose logging output from the

application itself.

https://www.khronos.org/registry/OpenXR/ ,
https://github.com/KhronosGroup/OpenXR-SDK-Source/tree/master/src/tests/hello_xr
