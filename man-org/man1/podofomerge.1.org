#+TITLE: Man1 - podofomerge.1
#+DESCRIPTION: Linux manpage for podofomerge.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
podofomerge - merge several PDF files

* SYNOPSIS
*podofomerge* [inputfile1] [inputfile2] [outputfile]

* DESCRIPTION
*podofomerge* is one of the command line tools from the PoDoFo library
that provide several useful operations to work with PDF files. It can
merge several PDF files.

* SEE ALSO
*podofobox*(1), *podofocolor*(1), *podofocountpages*(1),
*podofocrop*(1), *podofoencrypt*(1), *podofogc*(1), *podofoimg2pdf*(1),
*podofoimgextract*(1), *podofoimpose*(1), *podofoincrementalupdates*(1),
*podofopages*(1), *podofopdfinfo*(1), *podofotxt2pdf*(1),
*podofotxtextract*(1), *podofouncompress*(1), *podofoxmp*(1)

* AUTHORS
PoDoFo is written by Dominik Seichter <domseichter@web.de> and others.

This manual page was written by Oleksandr Moskalenko <malex@debian.org>
for the Debian Project (but may be used by others).
