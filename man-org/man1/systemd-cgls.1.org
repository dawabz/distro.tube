#+TITLE: Man1 - systemd-cgls.1
#+DESCRIPTION: Linux manpage for systemd-cgls.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd-cgls - Recursively show control group contents

* SYNOPSIS
*systemd-cgls* [OPTIONS...] [CGROUP...]

*systemd-cgls* [OPTIONS...] *--unit*|*--user-unit* [UNIT...]

* DESCRIPTION
*systemd-cgls* recursively shows the contents of the selected Linux
control group hierarchy in a tree. If arguments are specified, shows all
member processes of the specified control groups plus all their
subgroups and their members. The control groups may either be specified
by their full file paths or are assumed in the systemd control group
hierarchy. If no argument is specified and the current working directory
is beneath the control group mount point /sys/fs/cgroup/, shows the
contents of the control group the working directory refers to.
Otherwise, the full systemd control group hierarchy is shown.

By default, empty control groups are not shown.

* OPTIONS
The following options are understood:

*--all*

#+begin_quote
  Do not hide empty control groups in the output.
#+end_quote

*-l*, *--full*

#+begin_quote
  Do not ellipsize process tree members.
#+end_quote

*-u*, *--unit*

#+begin_quote
  Show cgroup subtrees for the specified units.
#+end_quote

*--user-unit*

#+begin_quote
  Show cgroup subtrees for the specified user units.
#+end_quote

*-k*

#+begin_quote
  Include kernel threads in output.
#+end_quote

*-M */MACHINE/, *--machine=*/MACHINE/

#+begin_quote
  Limit control groups shown to the part corresponding to the container
  /MACHINE/.
#+end_quote

*-h*, *--help*

#+begin_quote
  Print a short help text and exit.
#+end_quote

*--version*

#+begin_quote
  Print a short version string and exit.
#+end_quote

*--no-pager*

#+begin_quote
  Do not pipe output into a pager.
#+end_quote

* EXIT STATUS
On success, 0 is returned, a non-zero failure code otherwise.

* SEE ALSO
*systemd*(1), *systemctl*(1), *systemd-cgtop*(1), *systemd-nspawn*(1),
*ps*(1)
