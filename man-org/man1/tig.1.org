#+TITLE: Man1 - tig.1
#+DESCRIPTION: Linux manpage for tig.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tig - text-mode interface for Git

* SYNOPSIS
#+begin_example
  tig        [options] [revisions] [--] [paths]
  tig log    [options] [revisions] [--] [paths]
  tig show   [options] [revisions] [--] [paths]
  tig reflog [options] [revisions]
  tig blame  [options] [rev] [--] path
  tig grep   [options] [pattern]
  tig refs   [options]
  tig stash  [options]
  tig status
  tig <      [Git command output]
#+end_example

* DESCRIPTION
Tig is an ncurses-based text-mode interface for git(1). It functions
mainly as a Git repository browser, but can also assist in staging
changes for commit at chunk level and act as a pager for output from
various Git commands.

* OPTIONS
Command line options recognized by Tig include all valid git-log(1) and
git-diff(1) options, as well as the following subcommands and
Tig-specific options. The first command line parameter not starting with
"-" is interpreted as being either a revision specification or a path
and will end the option parsing. All additional options will be passed
to the underlying Git command.

show

#+begin_quote
  Open diff view using the given git-show(1) options.
#+end_quote

blame

#+begin_quote
  Show given file annotated by commits. Takes zero or more git-blame(1)
  options. Optionally limited from given revision.
#+end_quote

status

#+begin_quote
  Start up in status view.
#+end_quote

log

#+begin_quote
  Start up in log view, displaying git-log(1) output.
#+end_quote

reflog

#+begin_quote
  Start up in reflog view.
#+end_quote

refs

#+begin_quote
  Start up in refs view.
#+end_quote

stash

#+begin_quote
  Start up in stash view.
#+end_quote

grep

#+begin_quote
  Open the grep view. Supports the same options as git-grep(1).
#+end_quote

+<number>

#+begin_quote
  Show the first view with line <number> visible and selected.
#+end_quote

-v, --version

#+begin_quote
  Show version and exit.
#+end_quote

-h, --help

#+begin_quote
  Show help message and exit.
#+end_quote

-C <path>

#+begin_quote
  Run as if Tig was started in <path> instead of the current working
  directory.
#+end_quote

* PAGER MODE
Tig enters pager mode when input is provided via stdin and supports the
following subcommands and options:

#+begin_quote
  ·

  When the /show/ subcommand is specified and the /--stdin/ option is
  given, stdin is assumed to be a list of commit IDs and will be
  forwarded to the diff view's underlying git-show(1) command. For
  example:
#+end_quote

#+begin_quote
  #+begin_example
    $ git rev-list --author=vivien HEAD | tig show --stdin
  #+end_example
#+end_quote

#+begin_quote
  ·

  When /--stdin/ is given, stdin is assumed to be a list of commit IDs
  and will be forwarded to the main view's underlying git-log(1)
  command. For example:
#+end_quote

#+begin_quote
  #+begin_example
    $ tig --no-walk --stdin < cherry-picks.txt
  #+end_example
#+end_quote

#+begin_quote
  ·

  When /--pretty=raw/ is given, stdin is assumed to be a "pretty=raw"
  formatted output similar to that of git-log(1). For example:
#+end_quote

#+begin_quote
  #+begin_example
    $ git reflog --pretty=raw | tig --pretty=raw
  #+end_example
#+end_quote

When no subcommands nor options are given, the pager view will be used
for displaying the Git command input given on stdin. The pager view
assumes the input is either from git-log(1) or git-diff(1) and will
highlight it similar to the log and diff views. For example:

#+begin_quote
  #+begin_example
    $ git log -Schange -p --raw | tig
  #+end_example
#+end_quote

* EXAMPLES
Display the list of commits for the current branch:

#+begin_quote
  #+begin_example
    $ tig
  #+end_example
#+end_quote

Display commits from one or more branches:

#+begin_quote
  #+begin_example
    $ tig test master
  #+end_example
#+end_quote

Pretend as if all the refs in refs/ are listed on the command line:

#+begin_quote
  #+begin_example
    $ tig --all
  #+end_example
#+end_quote

Display differences between two branches:

#+begin_quote
  #+begin_example
    $ tig test..master
  #+end_example
#+end_quote

Display changes for sub-module versions:

#+begin_quote
  #+begin_example
    $ tig --submodule
  #+end_example
#+end_quote

Display changes for a single file:

#+begin_quote
  #+begin_example
    $ tig -- README
  #+end_example
#+end_quote

Display contents of the README file in a specific revision:

#+begin_quote
  #+begin_example
    $ tig show tig-0.8:README
  #+end_example
#+end_quote

Display revisions between two dates for a specific file:

#+begin_quote
  #+begin_example
    $ tig --after="2004-01-01" --before="2006-05-16" -- README
  #+end_example
#+end_quote

Blame file with copy detection enabled:

#+begin_quote
  #+begin_example
    $ tig blame -C README
  #+end_example
#+end_quote

Display the list of stashes:

#+begin_quote
  #+begin_example
    $ tig stash
  #+end_example
#+end_quote

Grep all files for lines containing DEFINE_ENUM:

#+begin_quote
  #+begin_example
    $ tig grep -p DEFINE_ENUM
  #+end_example
#+end_quote

Show references (branches, remotes and tags):

#+begin_quote
  #+begin_example
    $ tig refs
  #+end_example
#+end_quote

Use word diff in the diff view:

#+begin_quote
  #+begin_example
    $ tig --word-diff=plain
  #+end_example
#+end_quote

* ENVIRONMENT VARIABLES
In addition to environment variables used by Git (e.g. GIT_DIR), Tig
defines the ones below. The command related environment variables have
access to the internal state of Tig via replacement variables, such as
%(commit) and %(blob). See *tigrc*(5) for a full list.

TIGRC_USER

#+begin_quote
  Path of the user configuration file (defaults to ~/.tigrc or
  $XDG_CONFIG_HOME/tig/config).
#+end_quote

TIGRC_SYSTEM

#+begin_quote
  Path of the system wide configuration file (defaults to
  {sysconfdir}/tigrc). Define to empty string to use built-in
  configuration.
#+end_quote

TIG_LS_REMOTE

#+begin_quote
  Command for retrieving all repository references. The command should
  output data in the same format as git-ls-remote(1).
#+end_quote

TIG_DIFF_OPTS

#+begin_quote
  The diff options to use in the diff view. The diff view uses
  git-show(1) for formatting and always passes --patch-with-stat. You
  may also set the diff-options setting in the configuration file.
#+end_quote

TIG_TRACE

#+begin_quote
  Path for trace file where information about Git commands are logged.
#+end_quote

TIG_SCRIPT

#+begin_quote
  Path to script that should be executed automatically on startup. If
  this environment variable is defined to the empty string, the script
  is read from stdin. The script is interpreted line-by-line and can
  contain prompt commands and key mappings.

  #+begin_quote
    #+begin_example
      E.g. TIG_SCRIPT=<(echo :set main-view-commit-title-graph = no) tig
    #+end_example
  #+end_quote
#+end_quote

TIG_NO_DISPLAY

#+begin_quote
  Open Tig without rendering anything to the terminal. This force
  Ncurses to write to /dev/null. The main use is for automated testing
  of Tig.
#+end_quote

TIG_EDITOR

#+begin_quote
  The editor command to use when visiting files. This environment
  variable overrides $GIT_EDITOR, $EDITOR and $VISUAL, so it allows to
  use a different editor from the one Git uses.
#+end_quote

* FILES
/$XDG_CONFIG_HOME/tig/config/, /~/.config/tig/config/, /~/.tigrc/

#+begin_quote
  The Tig user configuration file is loaded in the following way. If
  $XDG_CONFIG_HOME is set, read user configuration from
  $XDG_CONFIG_HOME/tig/config. If $XDG_CONFIG_HOME is empty or
  undefined, read user configuration from ~/.config/tig/config if it
  exists and fall back to ~/.tigrc if it does not exist. See *tigrc*(5)
  for examples.
#+end_quote

//etc/tigrc/

#+begin_quote
  System wide configuration file.
#+end_quote

/$GIT_DIR/config/, /~/.gitconfig/, //etc/gitconfig/

#+begin_quote
  Git configuration files. Read on start-up with the help of
  git-config(1).
#+end_quote

/$XDG_DATA_HOME/tig/history/, /~/.local/share/tig/history/,
/~/.tig_history/

#+begin_quote
  When compiled with readline support, Tig writes a persistent command
  and search history. The location of the history file is determined in
  the following way. If $XDG_DATA_HOME is set and $XDG_DATA_HOME/tig/
  exists, store history to $XDG_DATA_HOME/tig/history. If $XDG_DATA_HOME
  is empty or undefined, store history to ~/.local/share/tig/history if
  the directory ~/.local/share/tig/ exists, and fall back to
  ~/.tig_history if it does not exist.
#+end_quote

* BUGS
Please visit Tig's *home page*[1] or *main Git repository*[2] for
information about new releases and how to report bugs or feature
request.

* COPYRIGHT
Copyright (c) 2006-2014 Jonas Fonseca <*jonas.fonseca@gmail.com*[3]>

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

* SEE ALSO
*tigrc*(5), *tigmanual*(7), git(7)

* NOTES
-  1. :: home page

  https://jonas.github.io/tig

-  2. :: main Git repository

  https://github.com/jonas/tig

-  3. :: jonas.fonseca@gmail.com

  mailto:jonas.fonseca@gmail.com
