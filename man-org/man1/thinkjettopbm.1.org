#+TITLE: Man1 - thinkjettopbm.1
#+DESCRIPTION: Linux manpage for thinkjettopbm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
thinkjettopbm - convert HP ThinkJet printer commands file to PBM

* SYNOPSIS
*thinkjettopbm*

[*-d*]

[/thinkjet_file/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*thinkjettopbm* reads HP ThinkJet printer commands from the standard
input, or /thinkjet_file/ if specified, and writes a PBM image to
Standard Output.

*thinkjettopbm* silently ignores text and non-graphics command
sequences.

The *-d* option turns on debugging messages which are written to the
standard error stream.

* LIMITATIONS
The program handles only a small subset of ThinkJet command sequences,
but enough to convert screen images from older HP test equipment.

* SEE ALSO
*pnmtopclx.html*(1) , *pbmtolj.html*(1) , *ppmtopj*(1) , *ppmtopj*(1) ,
*thinkjettopbm*(1) , *pbm*(5) , *pjtoppm*(1)

* AUTHOR
Copyright (C) 2001 by W. Eric Norum
