#+TITLE: Man1 - networkctl.1
#+DESCRIPTION: Linux manpage for networkctl.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
networkctl - Query the status of network links

* SYNOPSIS
*networkctl* [OPTIONS...] COMMAND [LINK...]

* DESCRIPTION
*networkctl* may be used to introspect the state of the network links as
seen by *systemd-networkd*. Please refer to
*systemd-networkd.service*(8) for an introduction to the basic concepts,
functionality, and configuration syntax.

* COMMANDS
The following commands are understood:

*list* [/PATTERN.../]

#+begin_quote
  Show a list of existing links and their status. If one ore more
  /PATTERN/s are specified, only links matching one of them are shown.
  If no further arguments are specified shows all links, otherwise just
  the specified links. Produces output similar to:

  #+begin_quote
    #+begin_example
      IDX LINK         TYPE     OPERATIONAL SETUP
        1 lo           loopback carrier     unmanaged
        2 eth0         ether    routable    configured
        3 virbr0       ether    no-carrier  unmanaged
        4 virbr0-nic   ether    off         unmanaged

      4 links listed.
    #+end_example
  #+end_quote

  The operational status is one of the following:

  missing

  #+begin_quote
    the device is missing
  #+end_quote

  off

  #+begin_quote
    the device is powered down
  #+end_quote

  no-carrier

  #+begin_quote
    the device is powered up, but it does not yet have a carrier
  #+end_quote

  dormant

  #+begin_quote
    the device has a carrier, but is not yet ready for normal traffic
  #+end_quote

  degraded-carrier

  #+begin_quote
    for bond or bridge master, one of the bonding or bridge slave
    network interfaces is in off, no-carrier, or dormant state
  #+end_quote

  carrier

  #+begin_quote
    the link has a carrier, or for bond or bridge master, all bonding or
    bridge slave network interfaces are enslaved to the master
  #+end_quote

  degraded

  #+begin_quote
    the link has carrier and addresses valid on the local link
    configured
  #+end_quote

  enslaved

  #+begin_quote
    the link has carrier and is enslaved to bond or bridge master
    network interface
  #+end_quote

  routable

  #+begin_quote
    the link has carrier and routable address configured
  #+end_quote

  The setup status is one of the following:

  pending

  #+begin_quote
    udev is still processing the link, we dont yet know if we will
    manage it
  #+end_quote

  failed

  #+begin_quote
    networkd failed to manage the link
  #+end_quote

  configuring

  #+begin_quote
    in the process of retrieving configuration or configuring the link
  #+end_quote

  configured

  #+begin_quote
    link configured successfully
  #+end_quote

  unmanaged

  #+begin_quote
    networkd is not handling the link
  #+end_quote

  linger

  #+begin_quote
    the link is gone, but has not yet been dropped by networkd
  #+end_quote
#+end_quote

*status* [/PATTERN.../]

#+begin_quote
  Show information about the specified links: type, state, kernel module
  driver, hardware and IP address, configured DNS servers, etc. If one
  ore more /PATTERN/s are specified, only links matching one of them are
  shown.

  When no links are specified, an overall network status is shown. Also
  see the option *--all*.

  Produces output similar to:

  #+begin_quote
    #+begin_example
      ●        State: routable
        Online state: online
             Address: 10.193.76.5 on eth0
                      192.168.122.1 on virbr0
                      169.254.190.105 on eth0
                      fe80::5054:aa:bbbb:cccc on eth0
             Gateway: 10.193.11.1 (CISCO SYSTEMS, INC.) on eth0
                 DNS: 8.8.8.8
                      8.8.4.4
    #+end_example
  #+end_quote

  In the overall network status, the online state depends on the
  individual online state of all required links. Managed links are
  required for online by default. In this case, the online state is one
  of the following:

  unknown

  #+begin_quote
    all links have unknown online status (i.e. there are no required
    links)
  #+end_quote

  offline

  #+begin_quote
    all required links are offline
  #+end_quote

  partial

  #+begin_quote
    some, but not all, required links are online
  #+end_quote

  online

  #+begin_quote
    all required links are online
  #+end_quote
#+end_quote

*lldp* [/PATTERN.../]

#+begin_quote
  Show discovered LLDP (Link Layer Discovery Protocol) neighbors. If one
  or more /PATTERN/s are specified only neighbors on those interfaces
  are shown. Otherwise shows discovered neighbors on all interfaces.
  Note that for this feature to work, /LLDP=/ must be turned on for the
  specific interface, see *systemd.network*(5) for details.

  Produces output similar to:

  #+begin_quote
    #+begin_example
      LINK             CHASSIS ID        SYSTEM NAME      CAPS        PORT ID           PORT DESCRIPTION
      enp0s25          00:e0:4c:00:00:00 GS1900           ..b........ 2                 Port #2

      Capability Flags:
      o - Other; p - Repeater;  b - Bridge; w - WLAN Access Point; r - Router;
      t - Telephone; d - DOCSIS cable device; a - Station; c - Customer VLAN;
      s - Service VLAN, m - Two-port MAC Relay (TPMR)

      1 neighbors listed.
    #+end_example
  #+end_quote
#+end_quote

*label*

#+begin_quote
  Show numerical address labels that can be used for address selection.
  This is the same information that *ip-addrlabel*(8) shows. See *RFC
  3484*[1] for a discussion of address labels.

  Produces output similar to:

  #+begin_quote
    #+begin_example
      Prefix/Prefixlen                          Label
              ::/0                                  1
          fc00::/7                                  5
          fec0::/10                                11
          2002::/16                                 2
          3ffe::/16                                12
       2001:10::/28                                 7
          2001::/32                                 6
      ::ffff:0.0.0.0/96                             4
              ::/96                                 3
             ::1/128                                0
    #+end_example
  #+end_quote
#+end_quote

*delete* /DEVICE.../

#+begin_quote
  Deletes virtual netdevs. Takes interface name or index number.
#+end_quote

*up* /DEVICE.../

#+begin_quote
  Bring devices up. Takes interface name or index number.
#+end_quote

*down* /DEVICE.../

#+begin_quote
  Bring devices down. Takes interface name or index number.
#+end_quote

*renew* /DEVICE.../

#+begin_quote
  Renew dynamic configurations e.g. addresses received from DHCP server.
  Takes interface name or index number.
#+end_quote

*forcerenew* /DEVICE.../

#+begin_quote
  Send a FORCERENEW message to all connected clients, triggering DHCP
  reconfiguration. Takes interface name or index number.
#+end_quote

*reconfigure* /DEVICE.../

#+begin_quote
  Reconfigure network interfaces. Takes interface name or index number.
  Note that this does not reload .netdev or .network corresponding to
  the specified interface. So, if you edit config files, it is necessary
  to call *networkctl reload* first to apply new settings.
#+end_quote

*reload*

#+begin_quote
  Reload .netdev and .network files. If a new .netdev file is found,
  then the corresponding netdev is created. Note that even if an
  existing .netdev is modified or removed, *systemd-networkd* does not
  update or remove the netdev. If a new, modified or removed .network
  file is found, then all interfaces which match the file are
  reconfigured.
#+end_quote

* OPTIONS
The following options are understood:

*-a* *--all*

#+begin_quote
  Show all links with *status*.
#+end_quote

*-s* *--stats*

#+begin_quote
  Show link statistics with *status*.
#+end_quote

*-l*, *--full*

#+begin_quote
  Do not ellipsize the output.
#+end_quote

*-n*, *--lines=*

#+begin_quote
  When used with *status*, controls the number of journal lines to show,
  counting from the most recent ones. Takes a positive integer argument.
  Defaults to 10.
#+end_quote

*--json=*/MODE/

#+begin_quote
  Shows output formatted as JSON. Expects one of "short" (for the
  shortest possible output without any redundant whitespace or line
  breaks), "pretty" (for a pretty version of the same, with indentation
  and line breaks) or "off" (to turn off JSON output, the default).
#+end_quote

*-h*, *--help*

#+begin_quote
  Print a short help text and exit.
#+end_quote

*--version*

#+begin_quote
  Print a short version string and exit.
#+end_quote

*--no-legend*

#+begin_quote
  Do not print the legend, i.e. column headers and the footer with
  hints.
#+end_quote

*--no-pager*

#+begin_quote
  Do not pipe output into a pager.
#+end_quote

* EXIT STATUS
On success, 0 is returned, a non-zero failure code otherwise.

* SEE ALSO
*systemd-networkd.service*(8), *systemd.network*(5),
*systemd.netdev*(5), *ip*(8)

* NOTES
-  1. :: RFC 3484

  https://tools.ietf.org/html/rfc3484
