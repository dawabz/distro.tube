#+TITLE: Man1 - mpc.1
#+DESCRIPTION: Linux manpage for mpc.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
mpc - mpc Documentation

* DESCRIPTION
mpc is a command-line client for the /Music Player Daemon/ (MPD). It
connects to a MPD and controls it according to commands and arguments
passed to it. If no command is given, the current status is printed
(same as "*mpc status*").

* SYNOPSIS

#+begin_quote

  #+begin_quote
    mpc [options] <command> [<arguments>]
  #+end_quote
#+end_quote

* OPTIONS

#+begin_quote
  - *-f, --format* :: Configure the format used to display songs.

  The metadata delimiters are:

  | _              |                                                                                                                                                                                                                                                   |
  | Name           | Description                                                                                                                                                                                                                                       |
  | _              |                                                                                                                                                                                                                                                   |
  | %name%         | A name for this song. This is not the song title. The exact meaning of this tag is not well-defined. It is often used by badly configured internet radio stations with broken tags to squeeze both the artist name and the song title in one tag. |
  | _              |                                                                                                                                                                                                                                                   |
  | %artist%       | Artist file tag                                                                                                                                                                                                                                   |
  | _              |                                                                                                                                                                                                                                                   |
  | %album%        | Album file tag                                                                                                                                                                                                                                    |
  | _              |                                                                                                                                                                                                                                                   |
  | %albumartist%  | Album Artist file tag                                                                                                                                                                                                                             |
  | _              |                                                                                                                                                                                                                                                   |
  | %comment%      | Comment file tag (not enabled by default in *mpd.conf*'s metadata_to_use)                                                                                                                                                                         |
  | _              |                                                                                                                                                                                                                                                   |
  | %composer%     | Composer file tag                                                                                                                                                                                                                                 |
  | _              |                                                                                                                                                                                                                                                   |
  | %date%         | Date file tag                                                                                                                                                                                                                                     |
  | _              |                                                                                                                                                                                                                                                   |
  | %originaldate% | Original Date file tag                                                                                                                                                                                                                            |
  | _              |                                                                                                                                                                                                                                                   |
  | %disc%         | Disc file tag                                                                                                                                                                                                                                     |
  | _              |                                                                                                                                                                                                                                                   |
  | %genre%        | Genre file tag                                                                                                                                                                                                                                    |
  | _              |                                                                                                                                                                                                                                                   |
  | %performer%    | Performer file tag                                                                                                                                                                                                                                |
  | _              |                                                                                                                                                                                                                                                   |
  | %title%        | Title file tag                                                                                                                                                                                                                                    |
  | _              |                                                                                                                                                                                                                                                   |
  | %track%        | Track file tag                                                                                                                                                                                                                                    |
  | _              |                                                                                                                                                                                                                                                   |
  | %time%         | Duration of file                                                                                                                                                                                                                                  |
  | _              |                                                                                                                                                                                                                                                   |
  | %file%         | Path of file, relative to MPD's *music_directory* variable                                                                                                                                                                                        |
  | _              |                                                                                                                                                                                                                                                   |
  | %position%     | Queue track number                                                                                                                                                                                                                                |
  | _              |                                                                                                                                                                                                                                                   |
  | %id%           | Queue track id number                                                                                                                                                                                                                             |
  | _              |                                                                                                                                                                                                                                                   |
  | %prio%         | Priority in the (random) queue.                                                                                                                                                                                                                   |
  | _              |                                                                                                                                                                                                                                                   |
  | %mtime%        | Date and time of last file modification                                                                                                                                                                                                           |
  | _              |                                                                                                                                                                                                                                                   |
  | %mdate%        | Date of last file modification                                                                                                                                                                                                                    |
  | _              |                                                                                                                                                                                                                                                   |

  The *[]* operator is used to group output such that if no metadata
  delimiters are found or matched between *[* and *]*, then none of the
  characters between *[* and *]* are output. *&* and *|* are logical
  operators for and and or. *#* is used to escape characters. Some
  useful examples for format are: "*%file%*" and "*[[%artist% -
  ]%title%]|[%file%]*". This command also takes the following defined
  escape sequences:

  | _  |                 |
  | \  | backslash       |
  | _  |                 |
  | \[ | left bracket    |
  | _  |                 |
  | \] | right bracket   |
  | _  |                 |
  | \a | alert           |
  | _  |                 |
  | \b | backspace       |
  | _  |                 |
  | \e | escape          |
  | _  |                 |
  | \t | tab             |
  | _  |                 |
  | \n | newline         |
  | _  |                 |
  | \v | vertical tab    |
  | _  |                 |
  | \f | form-feed       |
  | _  |                 |
  | \r | carriage return |
  | _  |                 |

  If not given, the value of the environment variable /MPC_FORMAT/ is
  used.
#+end_quote

#+begin_quote
  - *--wait* :: Wait for operation to finish (e.g. database update).
#+end_quote

#+begin_quote
  - *--range=[START]:[END]* :: Operate on a range (e.g. when loading a
    playlist). START is the first index of the range, END is the first
    index after the range (i.e. excluding). START and END may be
    omitted, making the range open to that end. Indexes start with zero.
#+end_quote

#+begin_quote
  - *-q, --quiet, --no-status* :: Prevents the current song status from
    being printed on completion of some of the commands.
#+end_quote

#+begin_quote
  - *--verbose* :: Verbose output.
#+end_quote

#+begin_quote
  - *--host=HOST* :: The MPD server to connect to. This can be a
    hostname, IPv4/IPv6 address, an absolute path (i.e. local socket) or
    a name starting with *@* (i.e. an abstract socket, Linux only).

  To use a password, provide a value of the form "*password@host*".

  If not given, the value of the environment variable /MPD_HOST/ is
  used.
#+end_quote

#+begin_quote
  - *--port=PORT, -p PORT* :: The TCP port of the MPD server to connect
    to.

  If not given, the value of the environment variable /MPD_PORT/ is
  used.
#+end_quote

* COMMANDS
Commands can be used from the least unambiguous prefix (e.g insert or
ins).

** Player Commands

#+begin_quote
  - *consume <on|off>* - Toggle consume mode if state (*on* :: or *off*)
    is not specified.

  - *crossfade [<seconds>]* - Gets and sets the current amount
    of :: crossfading between songs (*0* disables crossfading).

  - *current [--wait]* - Show the currently playing song.
    With :: /--wait/, mpc waits until the song changes (or until
    playback is started/stopped) before it queries the current song from
    the server.
#+end_quote

*queued* - Show the currently queued (next) song.

#+begin_quote
  - *mixrampdb [<db>]* - Gets and sets the volume level at
    which :: songs with MixRamp tags will be overlapped. This disables
    the fading of the crossfade command and simply mixes the songs.
    *-50.0* will effectively remove any gaps, *0.0* will mash tracks
    together. The amount of overlap is limited by the audio_buffer_size
    MPD configuration parameter.

  - *mixrampdelay [<seconds>]* - Gets and sets the current amount :: of
    extra delay added to the value computed from the MixRamp tags. (A
    negative value disables overlapping with MixRamp tags and restores
    the previous value of crossfade).
#+end_quote

*next* - Starts playing next song on queue.

*pause* - Pauses playing.

#+begin_quote
  - *play <position>* - Starts playing the song-number :: specified. If
    none is specified, plays number 1.
#+end_quote

*prev* - Starts playing previous song.

#+begin_quote
  - *random <on|off>* - Toggle random mode if state (*on* :: or *off*)
    is not specified.

  - *repeat <on|off>* - Toggle repeat mode if state (*on* :: or *off*)
    is not specified.

  - *replaygain [<off|track|album>]* - Sets the replay gain
    mode. :: Without arguments, it prints the replay gain mode.

  - *single <on|once|off>* - Toggle single mode if state
    (*on*, :: *once*, or *off*) is not specified. *once* toggles to
    *off*.

  - *seek [+-][<HH:MM:SS>] or <[+-]<0-100>%>* - Seeks by hour, :: minute
    or seconds, hours or minutes can be omitted. If seeking by
    percentage, seeks within the current song in the specified manner.
    If a *+* or *-* is used, the seek is done relative to the current
    song position. Absolute seeking by default.

  - *seekthrough [+-][<HH:MM:SS>]* - Seeks by hour, :: minute or
    seconds, hours or minutes can be omitted, relatively to the current
    position. If the duration exceeds the limit of the current song, the
    seek command proceeds to seek through the playlist until the
    duration is reached. If a *+* is used, the seek is forward. If a *-*
    is used, the seek is backward. Forward seeking by default.
#+end_quote

*stop* - Stops playing.

#+begin_quote
  - *toggle* - Toggles between play and pause. If stopped
    starts :: playing. Does not support start playing at song number
    (use play).
#+end_quote

** Queue Commands

#+begin_quote
  - *add <file>* - Adds a song from the music database to the :: queue.
    Can also read input from pipes. Use "*mpc add /*" to add all files
    to the queue.

  - *insert <file>* - The insert command works similarly to :: *add*
    except it adds song(s) after the currently playing one, rather than
    at the end. When random mode is enabled, the new song is queued
    after the current song.
#+end_quote

*clear* - Empties the queue.

#+begin_quote
  - *crop* - Remove all songs except for the currently playing :: song.

  - *del <songpos>* - Removes a queue number from the queue. Can :: also
    read input from pipes (*0* deletes the current playing song).

  - *mv, move <from> <to>* - Moves song at position <from> to
    the :: position <to> in the queue.

  - *searchplay <type> <query> [<type> <query>]...* - Search
    the :: queue for a matching song and play it.
#+end_quote

*shuffle* - Shuffles all songs on the queue.

** Playlist Commands

#+begin_quote
  - *load <file>* - Loads <file> as queue. The option :: /--range/ may
    be used to load only a portion of the file
#+end_quote

*lsplaylists*: - Lists available playlists.

#+begin_quote
  - *playlist [<playlist>]* - Lists all songs in <playlist>. If :: no
    <playlist> is specified, lists all songs in the current queue.
#+end_quote

*rm <file>* - Deletes a specific playlist.

*save <file>* - Saves playlist as <file>.

** Database Commands

#+begin_quote
  - *listall [<file>]* - Lists <file> from database. If no :: *file* is
    specified, lists all songs in the database.

  - *ls [<directory>]* - Lists all files/folders in :: *directory*. If
    no *directory* is specified, lists all files in music directory.

  - *search <type> <query> [<type> <query>]...* - Searches
    for :: substrings in song tags. Any number of tag type and query
    combinations can be specified. Possible tag types are: artist,
    album, title, track, name, genre, date, composer, performer,
    comment, disc, filename, or any (to match any tag).

  - *search <expression>* - Searches with a filter expression, :: 

    #+begin_quote
      e.g.:

      #+begin_quote

        #+begin_quote
          #+begin_example
            mpc search '((artist == "Kraftwerk") AND (title == "Metall auf
            Metall"))'
          #+end_example
        #+end_quote
      #+end_quote
    #+end_quote

  Check the /MPD protocol documentation/ for details. This syntax can be
  used with *find* and *findadd* as well. (Requires libmpdclient 2.16
  and MPD 0.21)

  - *find <type> <query> [<type> <query>]...* - Same as :: *search*, but
    tag values must match *query* exactly instead of doing a substring
    match.

  - *findadd <type> <query> [<type> <query>]...* - Same as :: *find*,
    but add the result to the current queue instead of printing them.

  - *list <type> [<type> <query>]... [group <type>]...* - Return :: a
    list of all tags of given tag *type*. Optional search *type*/*query*
    limit results in a way similar to search. Results can be grouped by
    one or more tags. Example:

    #+begin_quote
      #+begin_example
        mpc list album group artist
      #+end_example
    #+end_quote
#+end_quote

*stats* - Displays statistics about MPD.

#+begin_quote
  - *update [--wait] [<path>]* - Scans for updated files in the :: music
    directory. The optional parameter *path* (relative to the music
    directory) may limit the scope of the update.

  With /--wait/, mpc waits until MPD has finished the update.

  - *rescan [--wait] [<path>]* - Like update, but also
    rescans :: unmodified files.
#+end_quote

** File Commands

#+begin_quote
  - *albumart <file>* - Download album art for the given song
    and :: write it to stdout.

  - *readpicture <file>* - Download a picture embedded in the :: given
    song and write it to stdout.
#+end_quote

** Mount Commands
*mount* - Lists all mounts.

#+begin_quote
  - *mount <mount-path> <storage-uri>* - Create a new mount: :: mounts a
    storage on the given path. Example:

    #+begin_quote
      #+begin_example
        mpc mount server nfs://10.0.0.5/mp3
        mpc mount stick udisks://by-id-ata-FOO-part2
      #+end_example
    #+end_quote
#+end_quote

*unmount <mount-path>* - Remove a mount.

#+begin_quote
  - *listneighbors* - Print a list of "neighors" :: (i.e. automatically
    detected storages which can be mounted). This requires /enabling at
    least one neighbor plugin/ in *mpd.conf*. Example:

    #+begin_quote
      #+begin_example
        $ mpc listneighbors
        upnp://uuid:01234567-89ab-cdef-0123-456789abcdef/urn:schemas-upnp-org:service:ContentDirectory:1
        udisks://by-id-dm-name-_dev_sdb3
        udisks://by-id-ata-FOO-part2
      #+end_example
    #+end_quote
#+end_quote

** Sticker Commands
The *sticker* command allows you to get and set song stickers.

#+begin_quote
  - *sticker <file> set <key> <value>* - Set the value of a
    song :: sticker.

  - *sticker <file> get <key>* - Print the value of a song :: sticker.
#+end_quote

*sticker <file> list* - List all stickers of a song.

*sticker <file> delete <key>* - Delete a song sticker.

#+begin_quote
  - *sticker <dir> find <key>* - Search for stickers with
    the :: specified name, below the specified directory.
#+end_quote

** Output Commands

#+begin_quote
  - *volume [+-]<num>* - Sets the volume to <num> (0-100). If :: *+* or
    *-* is used, then it adjusts the volume relative to the current
    volume.
#+end_quote

*outputs* - Lists all available outputs

#+begin_quote
  - *disable [only] <output # or name> [...]* - Disables
    the :: output(s); a list of one or more names or numbers is
    required. If *only* is the first argument, all other outputs are
    enabled.

  - *enable [only] <output # or name> [...]* - Enables the :: output(s);
    a list of one or more names or numbers is required. If *only* is the
    first argument, all other outputs are disabled.

  - *toggleoutput <output # or name> [...]* - Changes the :: status for
    the given output(s); a list of one or more names or numbers is
    required.
#+end_quote

** Client-to-client Commands

#+begin_quote
  - *channels* - List the channels that other clients have :: subscribed
    to.

  - *sendmessage <channel> <message>* - Send a message to
    the :: specified channel.

  - *waitmessage <channel>* - Wait for at least one message on :: the
    specified channel.

  - *subscribe <channel>* - Subscribe to the specified channel :: and
    continuously receive messages.
#+end_quote

** Other Commands

#+begin_quote
  - *idle [events]* - Waits until an event occurs. Prints a list :: of
    event names, one per line. See the MPD protocol documentation for
    further information.

  If you specify a list of events, only these events are considered.

  - *idleloop [events]* - Similar to *idle*, but :: re-enters "idle"
    state after events have been printed.

  If you specify a list of events, only these events are considered.

  - *status [format]* - Without an argument print a three line
    status :: output equivalent to "mpc" with no arguments. If a format
    string is given then the delimiters are processed exactly as how
    they are for metadata. See the '-f' option in /Options/

  | _             |                                                                        |
  | Name          | Description                                                            |
  | _             |                                                                        |
  | %totaltime%   | The total duration of the song.                                        |
  | _             |                                                                        |
  | %currenttime% | The time that the client is currently at.                              |
  | _             |                                                                        |
  | %percenttime% | The percentage of time elapsed for the current song.                   |
  | _             |                                                                        |
  | %songpos%     | The position of the current song within the playlist.                  |
  | _             |                                                                        |
  | %length%      | The number of songs within the playlist                                |
  | _             |                                                                        |
  | %state%       | Either 'playing' or 'paused'                                           |
  | _             |                                                                        |
  | %volume%      | The current volume spaced out to 4 characters including a percent sign |
  | _             |                                                                        |
  | %random%      | Current status of random mode. 'on' or 'off'                           |
  | _             |                                                                        |
  | %repeat%      | Current status of repeat mode. 'on' or 'off'                           |
  | _             |                                                                        |
  | %single%      | Current status of single mode. 'on', 'once', or 'off'                  |
  | _             |                                                                        |
  | %consume%     | Current status of consume mode. 'on' or 'off'                          |
  | _             |                                                                        |

  - *version* - Reports the version of the protocol spoken, not the
    real :: version of the daemon.
#+end_quote

* ENVIRONMENT VARIABLES
All environment variables are overridden by any values specified via
command line switches.

#+begin_quote
  - *MPC_FORMAT* :: Configure the format used to display songs. See
    option /--format/.
#+end_quote

#+begin_quote
  - *MPD_HOST* :: The MPD server to connect to. See option /--host/.
#+end_quote

#+begin_quote
  - *MPD_PORT* :: The TCP port of the MPD server to connect to. See
    option /--port/.
#+end_quote

* BUGS
Report bugs on /https://github.com/MusicPlayerDaemon/mpc/issues/

Since MPD uses UTF-8, mpc needs to convert characters to the charset
used by the local system. If you get character conversion errors when
you're running mpc you probably need to set up your locale. This is done
by setting any of the LC_CTYPE, LANG or LC_ALL environment variables
(LC_CTYPE only affects character handling).

* SEE ALSO
*mpd(1)*

* AUTHOR
See
/https://raw.githubusercontent.com/MusicPlayerDaemon/mpc/master/AUTHORS/

* AUTHOR
Max Kellermann

* COPYRIGHT
Copyright 2003-2021 The Music Player Daemon Project
