#+TITLE: Man1 - gvnccapture.1
#+DESCRIPTION: Linux manpage for gvnccapture.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
gvnccapture - VNC screenshot capture

* SYNOPSIS
gvnccapture [OPTION]... [HOST][:DISPLAY] FILENAME

* DESCRIPTION
Capture a screenshot of the VNC desktop at HOST:DISPLAY saving to the
image file FILENAME. If HOST is omitted it defaults to localhost, if
:DISPLAY is omitted, it defaults to :1. FILENAME must end in a known
image format extension (eg .png, .jpeg). Supported options are

- --help, -? :: Display command line help information

- --quiet, -q :: Do not display information on the console when
  capturing the screenshot, with the exception of any password prompt.

- --debug, -d :: Display verbose debugging information on the console

* EXIT STATUS
The exit status is 0 upon successful screen capture, otherwise it is a
non-zero integer

* EXAMPLES
# gvnccapture localhost:1 desktop.png Password: Connected to localhost:1
Saved display to desktop.png

* AUTHORS
Daniel P. Berrange <dan@berrange.com>

* COPYRIGHT
Copyright (C) 2010 Daniel P. Berrange <dan@berrange.com>.

License LGPLv2+: GNU Lesser GPL version 2 or later
<http://gnu.org/licenses/gpl.html>.

This is free software: you are free to change and redistribute it. There
is NO WARRANTY, to the extent permitted by law.

* SEE ALSO
*vinagre* (1)
