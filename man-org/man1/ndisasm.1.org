#+TITLE: Man1 - ndisasm.1
#+DESCRIPTION: Linux manpage for ndisasm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ndisasm - the Netwide Disassembler, an 80x86 binary file disassembler

* SYNOPSIS
*ndisasm* [ *-o* origin ] [ *-s* sync-point [...]] [ *-a* | *-i* ] [
*-b* bits ] [ *-u* ] [ *-e* hdrlen ] [ *-p* vendor ] [ *-k*
offset,length [...]] infile

* DESCRIPTION
The *ndisasm* command generates a disassembly listing of the binary file
infile and directs it to stdout.

* OPTIONS
*-h*

#+begin_quote
  Causes *ndisasm* to exit immediately, after giving a summary of its
  invocation options.
#+end_quote

*-r*|*-v*

#+begin_quote
  Causes *ndisasm* to exit immediately, after displaying its version
  number.
#+end_quote

*-o* /origin/

#+begin_quote
  Specifies the notional load address for the file. This option causes
  *ndisasm* to get the addresses it lists down the left hand margin, and
  the target addresses of PC-relative jumps and calls, right.
#+end_quote

*-s* /sync-point/

#+begin_quote
  Manually specifies a synchronisation address, such that *ndisasm* will
  not output any machine instruction which encompasses bytes on both
  sides of the address. Hence the instruction which starts at that
  address will be correctly disassembled.
#+end_quote

*-e* /hdrlen/

#+begin_quote
  Specifies a number of bytes to discard from the beginning of the file
  before starting disassembly. This does not count towards the
  calculation of the disassembly offset: the first /disassembled/
  instruction will be shown starting at the given load address.
#+end_quote

*-k* /offset,length/

#+begin_quote
  Specifies that /length/ bytes, starting from disassembly offset
  /offset/, should be skipped over without generating any output. The
  skipped bytes still count towards the calculation of the disassembly
  offset.
#+end_quote

*-a*|*-i*

#+begin_quote
  Enables automatic (or intelligent) sync mode, in which *ndisasm* will
  attempt to guess where synchronisation should be performed, by means
  of examining the target addresses of the relative jumps and calls it
  disassembles.
#+end_quote

*-b* /bits/

#+begin_quote
  Specifies 16-, 32- or 64-bit mode. The default is 16-bit mode.
#+end_quote

*-u*

#+begin_quote
  Specifies 32-bit mode, more compactly than using ‘-b 32'.
#+end_quote

*-p* /vendor/

#+begin_quote
  Prefers instructions as defined by /vendor/ in case of a conflict.
  Known /vendor/ names include *intel*, *amd*, *cyrix*, and *idt*. The
  default is *intel*.
#+end_quote

* RESTRICTIONS
*ndisasm* only disassembles binary files: it has no understanding of the
header information present in object or executable files. If you want to
disassemble an object file, you should probably be using *objdump*(1).

Auto-sync mode won't necessarily cure all your synchronisation problems:
a sync marker can only be placed automatically if a jump or call
instruction is found to refer to it /before/ *ndisasm* actually
disassembles that part of the code. Also, if spurious jumps or calls
result from disassembling non-machine-code data, sync markers may get
placed in strange places. Feel free to turn auto-sync off and go back to
doing it manually if necessary.

* SEE ALSO
*objdump*(1)
