#+TITLE: Man1 - aec.1
#+DESCRIPTION: Linux manpage for aec.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
aec - compress or expand files

* SYNOPSIS
*aec* [*-3*] [*-b* /BYTES/] [*-d*] [*-j* /SAMPLES/] [*-m*] [*-n* /BITS/]
[*-N*] [*-p*] [*-r* /BLOCKS/] [*-s*] [*-t*] /infile/ /outfile/

* DESCRIPTION
/Aec/ performs lossless compression and decompression with Golomb-Rice
coding as defined in the Space Data System recommended standard
121.0-B-3.

* OPTIONS
- * -3* :: 24 bit samples are stored in 3 bytes

- * -b* /BYTES/ :: internal buffer size in bytes

- * -d* :: decompress /infile/; if option -d is not used then compress
  /infile/

- * -j* /SAMPLES/ :: block size in samples

- * -m* :: samples are MSB first; default is LSB first

- * -n* /BITS/ :: bits per sample

- * -N* :: disable pre/post processing

- * -p* :: pad RSI to byte boundary

- * -r* /BLOCKS/ :: reference sample interval in blocks

- * -s* :: samples are signed; default is unsigned

- * -t* :: use restricted set of code options
