#+TITLE: Man1 - ts_test.1
#+DESCRIPTION: Linux manpage for ts_test.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ts_test - A basic test routine for tslib.

* SYNOPSIS
*ts_test*

* DESCRIPTION
ts_test is used to manually test *tslib* using ts_read(). It draws a
crosshair and mode buttons to the framebuffer and lets you test the
behaviour of input samples with your configured filters applied, see
ts.conf (5). Since it uses ts_read(), it supports single touch only.

*-r, --rotate [value]*

#+begin_quote
  Rotate the screen. value is 0 for 0 degree, 1 for 90 degrees (CW), 2
  for 180 degrees (upside down) and 3 for 270 degrees (CCW).
#+end_quote

* SEE ALSO
ts.conf (5), ts_test_mt (1), ts_calibrate (1)
