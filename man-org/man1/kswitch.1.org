#+TITLE: Man1 - kswitch.1
#+DESCRIPTION: Linux manpage for kswitch.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
kswitch - switch primary ticket cache

* SYNOPSIS
*kswitch* {*-c* /cachename/|*-p* /principal/}

* DESCRIPTION
kswitch makes the specified credential cache the primary cache for the
collection, if a cache collection is available.

* OPTIONS

#+begin_quote
  - *-c* /cachename/ :: Directly specifies the credential cache to be
    made primary.

  - *-p* /principal/ :: Causes the cache collection to be searched for a
    cache containing credentials for /principal/. If one is found, that
    collection is made primary.
#+end_quote

* ENVIRONMENT
See kerberos(7) for a description of Kerberos environment variables.

* FILES

#+begin_quote
  - **FILE:/tmp/krb5cc_%{uid}** :: Default location of Kerberos 5
    credentials cache
#+end_quote

* SEE ALSO
kinit(1), kdestroy(1), klist(1), kerberos(7)

* AUTHOR
MIT

* COPYRIGHT
1985-2021, MIT
