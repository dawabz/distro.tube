#+TITLE: Man1 - djvudump.1
#+DESCRIPTION: Linux manpage for djvudump.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
djvudump - Display internal structure of DjVu files.

* SYNOPSIS
*djvudump [-o */outputfile/*] */djvufiles/*...*

* DESCRIPTION
Program *djvudump* prints an indented representation of the chunk
structure of any DjVu files. Each line represent contains a chunk ID
followed by the chunk size. Lines are indented in order to reflect the
hierarchical structure of the IFF files. The page identifier is printed
between curly braces when a bundled multi-page DjVu document is
recognized. Additional information about each chunk is provided when
*djvudump* recognizes the chunk name and knows how to summarize the
chunk data.

* REMARKS
This program is in fact able to describe any file complying with the
Electronic Arts IFF 85 specification. This includes a number of
graphical and sound file formats.

* CREDITS
This program was written by Léon Bottou <leonb@users.sourceforge.net>
and was then improved by Andrei Erofeev <andrew_erofeev@yahoo.com>, Bill
Riemers <docbill@sourceforge.net> and many others.

* SEE ALSO
*djvu*(1)
