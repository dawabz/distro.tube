#+TITLE: Man1 - lxsession-default-terminal.1
#+DESCRIPTION: Linux manpage for lxsession-default-terminal.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lxsession-default-terminal - Launching default terminal set by LXSession

* SYNOPSIS
*lxsession-default-terminal*

* DESCRIPTION
*lxsession-default-terminal* is a wrapper around LXSession D-Bus
interface, which launch default terminal set by LXsession. It's
equivalent to lxsession-default terminal, but it can be used if an
application can't use lxsesison-default.

Applications launched will be the one define for
terminal_manager/command set in desktop.conf.

* AUTHORS
Julien Lavergne <gilir@ubuntu.com>

Man page written to conform with Debian by Julien Lavergne.
