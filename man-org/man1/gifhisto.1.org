#+TITLE: Man1 - gifhisto.1
#+DESCRIPTION: Linux manpage for gifhisto.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gifhisto - make a color histogram from GIF colr frequencies

* SYNOPSIS
*gifhisto* [-v] [-t] [-s /width/ /height/] [-n /image-number/] [-b] [-h]
[/gif-file/]

* DESCRIPTION
A program to create histogram of number of pixels using each color. The
output can be formatted into a GIF histogram file, or as text file -
both go to stdout.

If no GIF file is given, gifhisto will try to read a GIF file from
stdin.

* OPTIONS
-v

#+begin_quote
  Verbose mode (show progress). Enables printout of running scan lines.
#+end_quote

-t

#+begin_quote
  Force output to be text file of the following form: (colormap size)
  lines each containing two integers: number of times color appeared,
  and color index. Lines are in increasing color index order. This
  output can be fed directly to a sort program if ordering by color
  frequency is desired.

  The colormap picked is the one to be used for the image to generate
  histogram for, as defined in GIF format.
#+end_quote

-s width height

#+begin_quote
  Size of GIF histogram file. The height of the histogram should be
  power of 2 dividable by number of colors in colormap.

  Width sets the resolution (accuracy if you like) of the histogram as
  the maximum histogram bar is scaled to fit it.
#+end_quote

-n image-number

#+begin_quote
  Image number to test. Default is one.
#+end_quote

-b

#+begin_quote
  Zeros the background color count. As only linear scale bars are
  supported and usually the background appears much more often then
  other colors, deleting the background count will improve the scaling
  of other colors.
#+end_quote

-h

#+begin_quote
  Print one line of command line help, similar to Usage above.
#+end_quote

* AUTHOR
Gershon Elber
