#+TITLE: Man1 - npm-shrinkwrap.1
#+DESCRIPTION: Linux manpage for npm-shrinkwrap.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*npm-shrinkwrap* - Lock down dependency versions for publication

** Synopsis

#+begin_quote
  #+begin_example
    npm shrinkwrap
  #+end_example
#+end_quote

Note: This command is unaware of workspaces.

** Description
This command repurposes *package-lock.json* into a publishable
*npm-shrinkwrap.json* or simply creates a new one. The file created and
updated by this command will then take precedence over any other
existing or future *package-lock.json* files. For a detailed explanation
of the design and purpose of package locks in npm, see npm help
package-lock-json.

** See Also

#+begin_quote

  - npm help install

  - npm help run-script

  - npm help scripts

  - npm help package.json

  - npm help package-lock.json

  - npm help npm-shrinkwrap.json

  - npm help ls
#+end_quote
