#+TITLE: Man1 - jack_lsp.1
#+DESCRIPTION: Linux manpage for jack_lsp.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
jack_lsp - JACK toolkit client to list information on ports

* SYNOPSIS
*jack_lsp* [ /-s/ | /--server/ servername ] [ /-AclLptvh/ ]

* DESCRIPTION
*jack_lsp* lists all known ports associated with a JACK server. It can
also optionally list various kinds of information about each port.

* OPTIONS
- *-s*, *--server* /servername/ :: \\
  Connect to the jack server named /servername/

- *-A*, *--aliases* :: \\
  List aliases for each port

- *-c*, *--connections* :: \\
  List connections to/from each port

- *-l*, *--latency* :: \\
  Display per-port latency in frames at each port

- *-L*, /--latency/ :: \\
  Display total latency in frames at each port

- *-p*, *--properties* :: \\
  Display port properties. Output may include input|output, can-monitor,
  physical, terminal

- *-t*, *--type* :: \\
  Display port type

- *-h*, *--help* :: \\
  Display help/usage message

- *-v*, *--version* :: \\
  Output version information and exit
