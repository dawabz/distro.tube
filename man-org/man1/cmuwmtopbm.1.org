#+TITLE: Man1 - cmuwmtopbm.1
#+DESCRIPTION: Linux manpage for cmuwmtopbm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
cmuwmtopbm - convert a CMU window manager bitmap into a PBM image

* SYNOPSIS
*cmuwmtopbm* [/cmuwmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*cmuwmtopbm* reads a CMU window manager bitmap as input. and produces a
PBM image as output.

* SEE ALSO
*pbmtocmuwm*(1) , *pbm*(5)

* AUTHOR
Copyright (C) 1989 by Jef Poskanzer.
