#+TITLE: Man1 - named-rrchecker.1
#+DESCRIPTION: Linux manpage for named-rrchecker.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
named-rrchecker - syntax checker for individual DNS resource records

* SYNOPSIS
*named-rrchecker* [*-h*] [*-o* origin] [*-p*] [*-u*] [*-C*] [*-T*]
[*-P*]

* DESCRIPTION
*named-rrchecker* reads a individual DNS resource record from standard
input and checks whether it is syntactically correct.

* OPTIONS

#+begin_quote
  - **-h** :: This option prints out the help menu.

  - **-o* origin* :: This option specifies the origin to be used when
    interpreting the record.

  - **-p** :: This option prints out the resulting record in canonical
    form. If there is no canonical form defined, the record is printed
    in unknown record format.

  - **-u** :: This option prints out the resulting record in unknown
    record form.

  - **-C*, *-T*, and *-P** :: These options print out the known class,
    standard type, and private type mnemonics, respectively.
#+end_quote

* SEE ALSO
/RFC 1034/, /RFC 1035/, *named(8)*.

* AUTHOR
Internet Systems Consortium

* COPYRIGHT
2021, Internet Systems Consortium
