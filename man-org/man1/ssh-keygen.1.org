#+TITLE: Manpages - ssh-keygen.1
#+DESCRIPTION: Linux manpage for ssh-keygen.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
generates, manages and converts authentication keys for

can create keys for use by SSH protocol version 2.

The type of key to be generated is specified with the

option. If invoked without any arguments,

will generate an RSA key.

is also used to generate groups for use in Diffie-Hellman group exchange
(DH-GEX). See the

section for details.

Finally,

can be used to generate and update Key Revocation Lists, and to test
whether given keys have been revoked by one. See the

section for details.

Normally each user wishing to use SSH with public key authentication
runs this once to create the authentication key in

or

Additionally, the system administrator may use this to generate host
keys, as seen in

Normally this program generates the key and asks for a file in which to
store the private key. The public key is stored in a file with the same
name but

appended. The program also asks for a passphrase. The passphrase may be
empty to indicate no passphrase (host keys must have an empty
passphrase), or it may be a string of arbitrary length. A passphrase is
similar to a password, except it can be a phrase with a series of words,
punctuation, numbers, whitespace, or any string of characters you want.
Good passphrases are 10-30 characters long, are not simple sentences or
otherwise easily guessable (English prose has only 1-2 bits of entropy
per character, and provides very bad passphrases), and contain a mix of
upper and lowercase letters, numbers, and non-alphanumeric characters.
The passphrase can be changed later by using the

option.

There is no way to recover a lost passphrase. If the passphrase is lost
or forgotten, a new key must be generated and the corresponding public
key copied to other machines.

will by default write keys in an OpenSSH-specific format. This format is
preferred as it offers better protection for keys at rest as well as
allowing storage of key comments within the private key file itself. The
key comment may be useful to help identify the key. The comment is
initialized to

when the key is created, but can be changed using the

option.

It is still possible for

to write the previously-used PEM format private keys using the

flag. This may be used when generating new keys, and existing new-format
keys may be converted using this option in conjunction with the

(change passphrase) flag.

After a key is generated,

will ask where the keys should be placed to be activated.

The options are as follows:

For each of the key types (rsa, dsa, ecdsa and ed25519) for which host
keys do not exist, generate the host keys with the default key file
path, an empty passphrase, default bits for the key type, and default
comment. If

has also been specified, its argument is used as a prefix to the default
path for the resulting host key files. This is used by

to generate new host keys.

When saving a private key, this option specifies the number of KDF (key
derivation function, currently

rounds used. Higher numbers result in slower passphrase verification and
increased resistance to brute-force password cracking (should the keys
be stolen). The default is 16 rounds.

Show the bubblebabble digest of specified private or public key file.

Specifies the number of bits in the key to create. For RSA keys, the
minimum size is 1024 bits and the default is 3072 bits. Generally, 3072
bits is considered sufficient. DSA keys must be exactly 1024 bits as
specified by FIPS 186-2. For ECDSA keys, the

flag determines the key length by selecting from one of three elliptic
curve sizes: 256, 384 or 521 bits. Attempting to use bit lengths other
than these three values for ECDSA keys will fail. ECDSA-SK, Ed25519 and
Ed25519-SK keys have a fixed length and the

flag will be ignored.

Provides a new comment.

Requests changing the comment in the private and public key files. The
program will prompt for the file containing the private keys, for the
passphrase if the key has one, and for the new comment.

Download the public keys provided by the PKCS#11 shared library

When used in combination with

this option indicates that a CA key resides in a PKCS#11 token (see the

section for details).

Specifies the hash algorithm used when displaying key fingerprints.
Valid options are:

and

The default is

This option will read a private or public OpenSSH key file and print to
stdout a public key in one of the formats specified by the

option. The default export format is

This option allows exporting OpenSSH keys for use by other programs,
including several commercial SSH implementations.

Search for the specified

(with optional port number) in a

file, listing any occurrences found. This option is useful to find
hashed host names or addresses and may also be used in conjunction with
the

option to print found keys in a hashed format.

Specifies the filename of the key file.

Use generic DNS format when printing fingerprint resource records using
the

command.

Hash a

file. This replaces all hostnames and addresses with hashed
representations within the specified file; the original content is moved
to a file with a .old suffix. These hashes may be used normally by

and

but they do not reveal identifying information should the file's
contents be disclosed. This option will not modify existing hashed
hostnames and is therefore safe to use on files that mix hashed and
non-hashed names.

When signing a key, create a host certificate instead of a user
certificate. Please see the

section for details.

Specify the key identity when signing a public key. Please see the

section for details.

This option will read an unencrypted private (or public) key file in the
format specified by the

option and print an OpenSSH compatible private (or public) key to
stdout. This option allows importing keys from other software, including
several commercial SSH implementations. The default import format is

Download resident keys from a FIDO authenticator. Public and private key
files will be written to the current directory for each downloaded key.
If multiple FIDO authenticators are attached, keys will be downloaded
from the first touched authenticator.

Generate a KRL file. In this mode,

will generate a KRL file at the location specified via the

flag that revokes every key or certificate presented on the command
line. Keys/certificates to be revoked may be specified by public key
file or using the format described in the

section.

Prints the contents of one or more certificates.

Show fingerprint of specified public key file. For RSA and DSA keys

tries to find the matching public key file and prints its fingerprint.
If combined with

a visual ASCII art representation of the key is supplied with the
fingerprint.

Generate candidate Diffie-Hellman Group Exchange (DH-GEX) parameters for
eventual use by the

key exchange methods. The numbers generated by this operation must be
further screened before use. See the

section for more information.

Screen candidate parameters for Diffie-Hellman Group Exchange. This will
accept a list of candidate numbers and test that they are safe (Sophie
Germain) primes with acceptable group generators. The results of this
operation may be added to the

file. See the

section for more information.

Specify a key format for key generation, the

(import),

(export) conversion options, and the

change passphrase operation. The latter may be used to convert between
OpenSSH private key and PEM private key formats. The supported key
formats are:

(RFC 4716/SSH2 public or private key),

(PKCS8 public or private key) or

(PEM public key). By default OpenSSH will write newly-generated private
keys in its own format, but when converting public keys for export the
default format is

Setting a format of

when generating or updating a supported private key type will cause the
key to be stored in the legacy PEM private key format.

Provides the new passphrase.

Specify one or more principals (user or host names) to be included in a
certificate when signing a key. Multiple principals may be specified,
separated by commas. Please see the

section for details.

Specify a key/value option. These are specific to the operation that

has been requested to perform.

When signing certificates, one of the options listed in the

section may be specified here.

When performing moduli generation or screening, one of the options
listed in the

section may be specified.

When generating a key that will be hosted on a FIDO authenticator, this
flag may be used to specify key-specific options. Those supported at
present are:

Override the default FIDO application/origin string of

This may be useful when generating host or domain-specific resident
keys. The specified application string must begin with

Specifies a path to a challenge string that will be passed to the FIDO
token during key generation. The challenge string may be used as part of
an out-of-band protocol for key enrollment (a random challenge is used
by default).

Explicitly specify a

device to use, rather than letting the token middleware select one.

Indicate that the generated private key should not require touch events
(user presence) when making signatures. Note that

will refuse such signatures by default, unless overridden via an
authorized_keys option.

Indicate that the key should be stored on the FIDO authenticator itself.
Resident keys may be supported on FIDO2 tokens and typically require
that a PIN be set on the token prior to generation. Resident keys may be
loaded off the token using

A username to be associated with a resident key, overriding the empty
default username. Specifying a username may be useful when generating
multiple resident keys for the same application name.

Indicate that this private key should require user verification for each
signature. Not all FIDO tokens support this option. Currently PIN
authentication is the only supported verification method, but other
methods may be supported in the future.

May be used at key generation time to record the attestation data
returned from FIDO tokens during key generation. Please note that this
information is potentially sensitive. By default, this information is
discarded.

When performing signature-related options using the

flag, the following options are accepted:

Print the full public key to standard output after signature
verification.

Specifies a time to use when validating signatures instead of the
current time. The time may be specified as a date in YYYYMMDD format or
a time in YYYYMMDDHHMM[SS] format.

The

option may be specified multiple times.

Provides the (old) passphrase.

Requests changing the passphrase of a private key file instead of
creating a new private key. The program will prompt for the file
containing the private key, for the old passphrase, and twice for the
new passphrase.

Test whether keys have been revoked in a KRL. If the

option is also specified then the contents of the KRL will be printed.

Silence

Removes all keys belonging to the specified

(with optional port number) from a

file. This option is useful to delete hashed hosts (see the

option above).

Print the SSHFP fingerprint resource record named

for the specified public key file.

Certify (sign) a public key using the specified CA key. Please see the

section for details.

When generating a KRL,

specifies a path to a CA public key file used to revoke certificates
directly by key ID or serial number. See the

section for details.

Specifies the type of key to create. The possible values are

or

This flag may also be used to specify the desired signature type when
signing certificates using an RSA CA key. The available RSA signature
variants are

(SHA1 signatures, not recommended),

and

(the default).

When used in combination with

this option indicates that a CA key resides in a

See the

section for more information.

Update a KRL. When specified with

keys listed via the command line are added to the existing KRL rather
than a new KRL being created.

Specify a validity interval when signing a certificate. A validity
interval may consist of a single time, indicating that the certificate
is valid beginning now and expiring at that time, or may consist of two
times separated by a colon to indicate an explicit time interval.

The start time may be specified as the string

to indicate the certificate has no specified start time, a date in
YYYYMMDD format, a time in YYYYMMDDHHMM[SS] format, a relative time (to
the current time) consisting of a minus sign followed by an interval in
the format described in the TIME FORMATS section of

The end time may be specified as a YYYYMMDD date, a YYYYMMDDHHMM[SS]
time, a relative time starting with a plus character or the string

to indicate that the certificate has no expiry date.

For example:

(valid from now to 52 weeks and one day from now),

(valid from four weeks ago to four weeks from now),

(valid from 12:30 PM, January 1st, 2010 to 12:30 PM, January 1st, 2011),

(valid from yesterday to midnight, January 1st, 2011),

(valid from one minute ago and never expiring).

Verbose mode. Causes

to print debugging messages about its progress. This is helpful for
debugging moduli generation. Multiple

options increase the verbosity. The maximum is 3.

Specifies a path to a library that will be used when creating FIDO
authenticator-hosted keys, overriding the default of using the internal
USB HID support.

Find the principal(s) associated with the public key of a signature,
provided using the

flag in an authorized signers file provided using the

flag. The format of the allowed signers file is documented in the

section below. If one or more matching principals are found, they are
returned on standard output.

Checks that a signature generated using

has a valid structure. This does not validate if a signature comes from
an authorized signer. When testing a signature,

accepts a message on standard input and a signature namespace using

A file containing the corresponding signature must also be supplied
using the

flag. Successful testing of the signature is signalled by

returning a zero exit status.

Cryptographically sign a file or some data using a SSH key. When
signing,

accepts zero or more files to sign on the command-line - if no files are
specified then

will sign data presented on standard input. Signatures are written to
the path of the input file with

appended, or to standard output if the message to be signed was read
from standard input.

The key used for signing is specified using the

option and may refer to either a private key, or a public key with the
private half available via

An additional signature namespace, used to prevent signature confusion
across different domains of use (e.g. file signing vs email signing)
must be provided via the

flag. Namespaces are arbitrary strings, and may include:

for file signing,

for email signing. For custom uses, it is recommended to use names
following a NAMESPACE@YOUR.DOMAIN pattern to generate unambiguous
namespaces.

Request to verify a signature generated using

as described above. When verifying a signature,

accepts a message on standard input and a signature namespace using

A file containing the corresponding signature must also be supplied
using the

flag, along with the identity of the signer using

and a list of allowed signers via the

flag. The format of the allowed signers file is documented in the

section below. A file containing revoked keys can be passed using the

flag. The revocation file may be a KRL or a one-per-line list of public
keys. Successful verification by an authorized signer is signalled by

returning a zero exit status.

This option will read a private OpenSSH format file and print an OpenSSH
public key to stdout.

Specifies the cipher to use for encryption when writing an
OpenSSH-format private key file. The list of available ciphers may be
obtained using

The default is

Specifies a serial number to be embedded in the certificate to
distinguish this certificate from others from the same CA. If the

is prefixed with a

character, then the serial number will be incremented for each
certificate signed on a single command-line. The default serial number
is zero.

When generating a KRL, the

flag is used to specify a KRL version number.

may be used to generate groups for the Diffie-Hellman Group Exchange
(DH-GEX) protocol. Generating these groups is a two-step process: first,
candidate primes are generated using a fast, but memory intensive
process. These candidate primes are then tested for suitability (a
CPU-intensive process).

Generation of primes is performed using the

option. The desired length of the primes may be specified by the

option. For example:

By default, the search for primes begins at a random point in the
desired length range. This may be overridden using the

option, which specifies a different start point (in hex).

Once a set of candidates have been generated, they must be screened for
suitability. This may be performed using the

option. In this mode

will read candidates from standard input (or a file specified using the

option). For example:

By default, each candidate will be subjected to 100 primality tests.
This may be overridden using the

option. The DH generator value will be chosen automatically for the
prime under consideration. If a specific generator is desired, it may be
requested using the

option. Valid generator values are 2, 3, and 5.

Screened DH groups may be installed in

It is important that this file contains moduli of a range of bit
lengths.

A number of options are available for moduli generation and screening
via the

flag:

Exit after screening the specified number of lines while performing DH
candidate screening.

Start screening at the specified line number while performing DH
candidate screening.

Write the last line processed to the specified file while performing DH
candidate screening. This will be used to skip lines in the input file
that have already been processed if the job is restarted.

Specify the amount of memory to use (in megabytes) when generating
candidate moduli for DH-GEX.

Specify start point (in hex) when generating candidate moduli for
DH-GEX.

Specify desired generator (in decimal) when testing candidate moduli for
DH-GEX.

supports signing of keys to produce certificates that may be used for
user or host authentication. Certificates consist of a public key, some
identity information, zero or more principal (user or host) names and a
set of options that are signed by a Certification Authority (CA) key.
Clients or servers may then trust only the CA key and verify its
signature on a certificate rather than trusting many user/host keys.
Note that OpenSSH certificates are a different, and much simpler, format
to the X.509 certificates used in

supports two types of certificates: user and host. User certificates
authenticate users to servers, whereas host certificates authenticate
server hosts to users. To generate a user certificate:

The resultant certificate will be placed in

A host certificate requires the

option:

The host certificate will be output to

It is possible to sign using a CA key stored in a PKCS#11 token by
providing the token library using

and identifying the CA key by providing its public half as an argument
to

Similarly, it is possible for the CA key to be hosted in a

This is indicated by the

flag and, again, the CA key must be identified by its public half.

In all cases,

is a "key identifier" that is logged by the server when the certificate
is used for authentication.

Certificates may be limited to be valid for a set of principal
(user/host) names. By default, generated certificates are valid for all
users or hosts. To generate a certificate for a specified set of
principals:

Additional limitations on the validity and use of user certificates may
be specified through certificate options. A certificate option may
disable features of the SSH session, may be valid only when presented
from particular source addresses or may force the use of a specific
command.

The options that are valid for user certificates are:

Clear all enabled permissions. This is useful for clearing the default
set of permissions so permissions may be added individually.

Includes an arbitrary certificate critical option or extension. The
specified

should include a domain suffix, e.g.

If

is specified then it is included as the contents of the extension/option
encoded as a string, otherwise the extension/option is created with no
contents (usually indicating a flag). Extensions may be ignored by a
client or server that does not recognise them, whereas unknown critical
options will cause the certificate to be refused.

Forces the execution of

instead of any shell or command specified by the user when the
certificate is used for authentication.

Disable

forwarding (permitted by default).

Disable port forwarding (permitted by default).

Disable PTY allocation (permitted by default).

Disable execution of

by

(permitted by default).

Disable X11 forwarding (permitted by default).

Allows

forwarding.

Allows port forwarding.

Allows PTY allocation.

Allows execution of

by

Allows X11 forwarding.

Do not require signatures made using this key include demonstration of
user presence (e.g. by having the user touch the authenticator). This
option only makes sense for the FIDO authenticator algorithms

and

Restrict the source addresses from which the certificate is considered
valid. The

is a comma-separated list of one or more address/netmask pairs in CIDR
format.

Require signatures made using this key indicate that the user was first
verified. This option only makes sense for the FIDO authenticator
algorithms

and

Currently PIN authentication is the only supported verification method,
but other methods may be supported in the future.

At present, no standard options are valid for host keys.

Finally, certificates may be defined with a validity lifetime. The

option allows specification of certificate start and end times. A
certificate that is presented at a time outside this range will not be
considered valid. By default, certificates are valid from the

Epoch to the distant future.

For certificates to be used for user or host authentication, the CA
public key must be trusted by

or

Please refer to those manual pages for details.

is able to manage OpenSSH format Key Revocation Lists (KRLs). These
binary files specify keys or certificates to be revoked using a compact
format, taking as little as one bit per certificate if they are being
revoked by serial number.

KRLs may be generated using the

flag. This option reads one or more files from the command line and
generates a new KRL. The files may either contain a KRL specification
(see below) or public keys, listed one per line. Plain public keys are
revoked by listing their hash or contents in the KRL and certificates
revoked by serial number or key ID (if the serial is zero or not
available).

Revoking keys using a KRL specification offers explicit control over the
types of record used to revoke keys and may be used to directly revoke
certificates by serial number or key ID without having the complete
original certificate on hand. A KRL specification consists of lines
containing one of the following directives followed by a colon and some
directive-specific information.

Revokes a certificate with the specified serial number. Serial numbers
are 64-bit values, not including zero and may be expressed in decimal,
hex or octal. If two serial numbers are specified separated by a hyphen,
then the range of serial numbers including and between each is revoked.
The CA key must have been specified on the

command line using the

option.

Revokes a certificate with the specified key ID string. The CA key must
have been specified on the

command line using the

option.

Revokes the specified key. If a certificate is listed, then it is
revoked as a plain public key.

Revokes the specified key by including its SHA1 hash in the KRL.

Revokes the specified key by including its SHA256 hash in the KRL. KRLs
that revoke keys by SHA256 hash are not supported by OpenSSH versions
prior to 7.9.

Revokes a key using a fingerprint hash, as obtained from a

authentication log message or the

flag. Only SHA256 fingerprints are supported here and resultant KRLs are
not supported by OpenSSH versions prior to 7.9.

KRLs may be updated using the

flag in addition to

When this option is specified, keys listed via the command line are
merged into the KRL, adding to those already there.

It is also possible, given a KRL, to test whether it revokes a
particular key (or keys). The

flag will query an existing KRL, testing each key specified on the
command line. If any key listed on the command line has been revoked (or
an error encountered) then

will exit with a non-zero exit status. A zero exit status will only be
returned if no key was revoked.

When verifying signatures,

uses a simple list of identities and keys to determine whether a
signature comes from an authorized source. This "allowed signers" file
uses a format patterned after the AUTHORIZED_KEYS FILE FORMAT described
in

Each line of the file contains the following space-separated fields:
principals, options, keytype, base64-encoded key. Empty lines and lines
starting with a

are ignored as comments.

The principals field is a pattern-list (see PATTERNS in

consisting of one or more comma-separated USER@DOMAIN identity patterns
that are accepted for signing. When verifying, the identity presented
via the

option must match a principals pattern in order for the corresponding
key to be considered acceptable for verification.

The options (if present) consist of comma-separated option
specifications. No spaces are permitted, except within double quotes.
The following option specifications are supported (note that option
keywords are case-insensitive):

Indicates that this key is accepted as a certificate authority (CA) and
that certificates signed by this CA may be accepted for verification.

Specifies a pattern-list of namespaces that are accepted for this key.
If this option is present, the signature namespace embedded in the
signature object and presented on the verification command-line must
match the specified list before the key will be considered acceptable.

Indicates that the key is valid for use at or after the specified
timestamp, which may be a date in YYYYMMDD format or a time in
YYYYMMDDHHMM[SS] format.

Indicates that the key is valid for use at or before the specified
timestamp.

When verifying signatures made by certificates, the expected principal
name must match both the principals pattern in the allowed signers file
and the principals embedded in the certificate itself.

An example allowed signers file:

# Comments allowed at start of line user1@example.com,user2@example.com
ssh-rsa AAAAX1... # A certificate authority, trusted for all principals
in a domain. *@example.com cert-authority ssh-ed25519 AAAB4... # A key
that is accepted only for file signing. user2@example.com
namespaces="file" ssh-ed25519 AAA41...

Specifies a path to a library that will be used when loading any FIDO
authenticator-hosted keys, overriding the default of using the built-in
USB HID support.

Contains the DSA, ECDSA, authenticator-hosted ECDSA, Ed25519,
authenticator-hosted Ed25519 or RSA authentication identity of the user.
This file should not be readable by anyone but the user. It is possible
to specify a passphrase when generating the key; that passphrase will be
used to encrypt the private part of this file using 128-bit AES. This
file is not automatically accessed by

but it is offered as the default file for the private key.

will read this file when a login attempt is made.

Contains the DSA, ECDSA, authenticator-hosted ECDSA, Ed25519,
authenticator-hosted Ed25519 or RSA public key for authentication. The
contents of this file should be added to

on all machines where the user wishes to log in using public key
authentication. There is no need to keep the contents of this file
secret.

Contains Diffie-Hellman groups used for DH-GEX. The file format is
described in

OpenSSH is a derivative of the original and free ssh 1.2.12 release by
Tatu Ylonen. Aaron Campbell, Bob Beck, Markus Friedl, Niels Provos, Theo
de Raadt and Dug Song removed many bugs, re-added newer features and
created OpenSSH. Markus Friedl contributed the support for SSH protocol
versions 1.5 and 2.0.

Information about ssh-keygen.1 is found in manpage for: [[../$ ssh-keygen -s ca_key.pub -D libpkcs11.so -I key_id user_key.pub][$ ssh-keygen -s ca_key.pub -D libpkcs11.so -I key_id user_key.pub]]