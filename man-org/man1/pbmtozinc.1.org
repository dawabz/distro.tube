#+TITLE: Man1 - pbmtozinc.1
#+DESCRIPTION: Linux manpage for pbmtozinc.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmtozinc - convert a PBM image into a Zinc bitmap

* SYNOPSIS
*pbmtozinc*

[/pbmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmtozinc* reads a PBM image as input and produces a bitmap in the
format used by the Zinc Interface Library (ZIL) Version 1.0 as output.

* SEE ALSO
*pbm*(5)

* AUTHOR
Copyright (C) 1988 by James Darrell McCauley
(/jdm5548@diamond.tamu.edu/) and Jef Poskanzer.
