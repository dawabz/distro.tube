#+TITLE: Man1 - hardlink.1
#+DESCRIPTION: Linux manpage for hardlink.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hardlink - link multiple copies of a file

* SYNOPSIS
*hardlink* [options] [/directory/|/file/]...

* DESCRIPTION
*hardlink* is a tool which replaces copies of a file with hardlinks,
therefore saving space.

* OPTIONS
*-h*, *--help*

#+begin_quote
  print quick usage details to the screen.
#+end_quote

*-v*, *--verbose*

#+begin_quote
  More verbose output. If specified once, every hardlinked file is
  displayed, if specified twice, it also shows every comparison.
#+end_quote

*-q*, *--quiet*

#+begin_quote
  Quiet mode, don't print anything.
#+end_quote

*-n*, *--dry-run*

#+begin_quote
  Do not act, just print what would happen.
#+end_quote

*-f*, *--respect-name*

#+begin_quote
  Only try to link files with the same (basename). It's strongly
  recommended to use long options rather than *-f* which is interpreted
  in a different way by others *hardlink* implementations.
#+end_quote

*-p*, *--ignore-mode*

#+begin_quote
  Link/compare files even if their mode is different. This may be a bit
  unpredictable.
#+end_quote

*-o*, *--ignore-owner*

#+begin_quote
  Link/compare files even if their owner (user and group) is different.
  It is not predictable.
#+end_quote

*-t*, *--ignore-time*

#+begin_quote
  Link/compare files even if their time of modification is different.
  You almost always want this.
#+end_quote

*-X*, *--respect-xattrs*

#+begin_quote
  Only try to link files with the same extended attributes.
#+end_quote

*-m*, *--maximize*

#+begin_quote
  Among equal files, keep the file with the highest link count.
#+end_quote

*-M*, *--minimize*

#+begin_quote
  Among equal files, keep the file with the lowest link count.
#+end_quote

*-O*, *--keep-oldest*

#+begin_quote
  Among equal files, keep the oldest file (least recent modification
  time). By default, the newest file is kept. If *--maximize* or
  *--minimize* is specified, the link count has a higher precedence than
  the time of modification.
#+end_quote

*-x*, *--exclude* /regex/

#+begin_quote
  A regular expression which excludes files from being compared and
  linked.
#+end_quote

*-i*, *--include* /regex/

#+begin_quote
  A regular expression to include files. If the option *--exclude* has
  been given, this option re-includes files which would otherwise be
  excluded. If the option is used without *--exclude*, only files
  matched by the pattern are included.
#+end_quote

*-s*, *--minimum-size* /size/

#+begin_quote
  The minimum size to consider. By default this is 1, so empty files
  will not be linked. The /size/ argument may be followed by the
  multiplicative suffixes KiB (=1024), MiB (=1024*1024), and so on for
  GiB, TiB, PiB, EiB, ZiB and YiB (the "iB" is optional, e.g., "K" has
  the same meaning as "KiB").
#+end_quote

* ARGUMENTS
*hardlink* takes one or more directories which will be searched for
files to be linked.

* BUGS
The original *hardlink* implementation uses the option *-f* to force
hardlinks creation between filesystem. This very rarely usable feature
is no more supported by the current hardlink.

*hardlink* assumes that the trees it operates on do not change during
operation. If a tree does change, the result is undefined and
potentially dangerous. For example, if a regular file is replaced by a
device, hardlink may start reading from the device. If a component of a
path is replaced by a symbolic link or file permissions change, security
may be compromised. Do not run hardlink on a changing tree or on a tree
controlled by another user.

* AUTHOR
There are multiple *hardlink* implementations. The very first
implementation is from Jakub Jelinek for Fedora distribution, this
implementation has been used in util-linux between versions v2.34 to
v2.36. The current implementations is based on Debian version from
Julian Andres Klode.

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *hardlink* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
