#+TITLE: Man1 - calibre.1
#+DESCRIPTION: Linux manpage for calibre.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
calibre - calibre

#+begin_quote

  #+begin_quote
    #+begin_example
      calibre [options] [path_to_ebook or calibre url ...]
    #+end_example
  #+end_quote
#+end_quote

Launch the main *calibre* Graphical User Interface and optionally add
the e-book at path_to_ebook to the database. You can also specify
*calibre* URLs to perform various different actions, than just adding
books. For example:

*calibre*://view-book/test_library/1842/epub

Will open the book with id 1842 in the EPUB format from the library
"test_library" in the *calibre* E-book viewer. Library names are the
folder names of the libraries with spaces replaced by underscores. A
full description of the various URL based actions is in the User Manual.

Whenever you pass arguments to *calibre* that have spaces in them,
enclose the arguments in quotation marks. For example: "/some path/with
spaces"

* [OPTIONS]

#+begin_quote
  - *--detach* :: Detach from the controlling terminal, if any (Linux
    only)
#+end_quote

#+begin_quote
  - *--help, -h* :: show this help message and exit
#+end_quote

#+begin_quote
  - *--ignore-plugins* :: Ignore custom plugins, useful if you installed
    a plugin that is preventing calibre from starting
#+end_quote

#+begin_quote
  - *--no-update-check* :: Do not check for updates
#+end_quote

#+begin_quote
  - *--shutdown-running-calibre, -s* :: Cause a running calibre
    instance, if any, to be shutdown. Note that if there are running
    jobs, they will be silently aborted, so use with care.
#+end_quote

#+begin_quote
  - *--start-in-tray* :: Start minimized to system tray.
#+end_quote

#+begin_quote
  - *--verbose, -v* :: Ignored, do not use. Present only for legacy
    reasons
#+end_quote

#+begin_quote
  - *--version* :: show program*'*s version number and exit
#+end_quote

#+begin_quote
  - *--with-library* :: Use the library located at the specified path.
#+end_quote

* AUTHOR
Kovid Goyal

* COPYRIGHT
Kovid Goyal
