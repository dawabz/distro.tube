#+TITLE: Man1 - cargo-search.1
#+DESCRIPTION: Linux manpage for cargo-search.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cargo-search - Search packages in crates.io

* SYNOPSIS
*cargo search* [/options/] [/query/...]

* DESCRIPTION
This performs a textual search for crates on <https://crates.io>. The
matching crates will be displayed along with their description in TOML
format suitable for copying into a *Cargo.toml* manifest.

* OPTIONS
** Search Options
*--limit* /limit/

#+begin_quote
  Limit the number of results (default: 10, max: 100).
#+end_quote

*--index* /index/

#+begin_quote
  The URL of the registry index to use.
#+end_quote

*--registry* /registry/

#+begin_quote
  Name of the registry to use. Registry names are defined in /Cargo
  config/ files <https://doc.rust-lang.org/cargo/reference/config.html>.
  If not specified, the default registry is used, which is defined by
  the *registry.default* config key which defaults to *crates-io*.
#+end_quote

** Display Options
*-v*, *--verbose*

#+begin_quote
  Use verbose output. May be specified twice for "very verbose" output
  which includes extra output such as dependency warnings and build
  script output. May also be specified with the *term.verbose* /config
  value/ <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

*-q*, *--quiet*

#+begin_quote
  No output printed to stdout.
#+end_quote

*--color* /when/

#+begin_quote
  Control when colored output is used. Valid values:

  #+begin_quote
    ·*auto* (default): Automatically detect if color support is
    available on the terminal.
  #+end_quote

  #+begin_quote
    ·*always*: Always display colors.
  #+end_quote

  #+begin_quote
    ·*never*: Never display colors.
  #+end_quote

  May also be specified with the *term.color* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

** Common Options
*+*/toolchain/

#+begin_quote
  If Cargo has been installed with rustup, and the first argument to
  *cargo* begins with *+*, it will be interpreted as a rustup toolchain
  name (such as *+stable* or *+nightly*). See the /rustup documentation/
  <https://rust-lang.github.io/rustup/overrides.html> for more
  information about how toolchain overrides work.
#+end_quote

*-h*, *--help*

#+begin_quote
  Prints help information.
#+end_quote

*-Z* /flag/

#+begin_quote
  Unstable (nightly-only) flags to Cargo. Run *cargo -Z help* for
  details.
#+end_quote

* ENVIRONMENT
See /the reference/
<https://doc.rust-lang.org/cargo/reference/environment-variables.html>
for details on environment variables that Cargo reads.

* EXIT STATUS

#+begin_quote
  ·*0*: Cargo succeeded.
#+end_quote

#+begin_quote
  ·*101*: Cargo failed to complete.
#+end_quote

* EXAMPLES

#+begin_quote
  1.Search for a package from crates.io:

  #+begin_quote
    #+begin_example
      cargo search serde
    #+end_example
  #+end_quote
#+end_quote

* SEE ALSO
*cargo*(1), *cargo-install*(1), *cargo-publish*(1)
