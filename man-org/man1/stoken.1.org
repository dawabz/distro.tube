#+TITLE: Man1 - stoken.1
#+DESCRIPTION: Linux manpage for stoken.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
stoken - software token for cryptographic authentication

* SYNOPSIS
*stoken* [*tokencode*] [*--stdin*] [*--force*] [*--next*] [/opts/]

*stoken* *import* {*--file=*/file/ | *--token=*/token_string/}
[*--force*] [/opts/]

*stoken* *setpin* [/opts/]

*stoken* *setpass* [/opts/]

*stoken* *show* [*--seed*] [/opts/]

*stoken* *export* [{*--blocks* | *--iphone* | *--android* | *--v3* |
*--sdtid* | *--qr=*/file.png/ | *--show-qr*}] [/opts/]

*stoken* *issue* [--*template*=/file/]

*stoken* *help*

*stoken* *version*

* DESCRIPTION
*stoken* is a software token compatible with RSA SecurID 128-bit (AES)
tokens. The command-line interface provides facilities for importing new
tokens, displaying the current tokencode, encrypting the seed with a
user-specified password, storing the user's PIN alongside the token, and
viewing or exporting the token data.

* BASIC USAGE
Use *stoken import* to decode a token string and write it into
/~/.stokenrc/. This may prompt for a device ID and/or password,
depending on what options your administrator used to create the token.
The token string can be provided on the command line, or read from a
text file.

*stoken* will autodetect the following types of token strings:

- *286510182209303756117707012447003320623006...* :: 

- *29658-21098-45467-64675-65731-01441-11337...* :: 

Pure numeric (81-digit) "ctf" (compressed token format) strings, with or
without dashes. These may have been furnished as-is, or they could have
been derived from an /sdtid/ file by the RSA /TokenConverter/ program.

- *com.rsa.securid.iphone://ctf?ctfData=229639330774927764401...* :: iPhone-compatible
  token strings.

- *http://127.0.0.1/securid/ctf?ctfData=250494932146245277466...* :: 

- *http://127.0.0.1/securid/ctf?ctfData=AwAAfBc3QSopPxxjLGnxf...* :: Android-compatible
  token strings.

- *<?xml version=...* :: RSA /sdtid/-formatted XML files. These should
  be imported from a file: *stoken import --file=FILE.SDTID*.

Tokens supplied as QR codes can be converted back to standard URIs by
running *zbarimg*(1) on the image file.

The device ID, if used, can be viewed in the "about" menu for the RSA
soft token app on the phone. Numeric ctf strings and smartphone tokens
bound to a device ID contain a seed that is encrypted using the device
ID, so the ID must be furnished before stoken can successfully import
the token. /sdtid/ files can be imported without knowledge of the device
ID, as long as the password (if any) is known.

By default, *stoken import* will refuse to overwrite an existing token
in /~/.stokenrc/. The *--force* switch overrides this check.

*stoken import* will normally prompt for a new password, which is used
to encrypt the seed before storing it in /~/.stokenrc/. This can be
bypassed by entering an empty password, or specifying
*--new-password=''* on the command line. It is recommended to choose a
longer, hard-to-guess passphrase for this purpose.

After a token has been imported, running *stoken* with no arguments will
prompt for any required password or PIN, then display the current
tokencode.

Tokencodes are computed from the raw (decrypted) seed data, the current
time of day, and the PIN. If the same seed is installed on multiple
devices, they should all produce identical tokencodes. If they do not,
double-check the timezone setting and consider using NTP to synchronize
the system time to a known good source.

*stoken setpin* can be used to save the PIN in /~/.stokenrc/. Not all
tokens will require a PIN; this can be configured by the SecurID
administrator when generating new tokens. Setting an empty PIN will
remove the PIN from /~/.stokenrc/ so that the user will be prompted
every time it is required. See the *SECURITY CONSIDERATIONS* section
below for additional details.

*stoken setpass* encrypts the seed and PIN (if present) in /~/.stokenrc/
with a user-selectable password or passphrase. If an empty password is
entered, the password will be removed. See the *SECURITY CONSIDERATIONS*
section below for additional details.

* VIEWING TOKENS
*stoken show* displays information about the current token, typically
read from /~/.stokenrc/. The *--seed* option displays the encrypted and
decrypted seed bytes (which should be treated as sensitive data, as they
can be used to derive tokencodes).

*stoken export* translates the current token into a format suitable for
importation to another device.

*stoken issue* generates a new software token in XML /sdtid/ format. A
template file, itself in /sdtid/ format, may be provided to override
some or all of the human-readable fields. This would permit appropriate
serial numbers, expiration dates, usernames, etc. to be specified. If
Secret, Seed, or MAC fields are present in the template file, they will
be ignored.

* GLOBAL OPTIONS
- *--rcfile=*/file/ :: Use an alternate /.stokenrc/* configuration file.
  This is typically* used to support multiple tokens on the same user's
  UNIX account. Note that the /.stokenrc/* file stores additional data
  (such as the PIN), so it* cannot be parsed as a "raw" token string by
  *stoken --file.*

- *--password=*/password/*, -p */password/ :: Use a password supplied
  from the command line, instead of prompting the user. See notes in
  *SECURITY CONSIDERATIONS below.*

- *--pin=*/pin/*, -n */pin/ :: Use a PIN supplied from the command line,
  instead of prompting the user. See notes in *SECURITY CONSIDERATIONS
  below. If you save your PIN* in /~/.stokenrc/*, note that --pin=0000
  is often required when* activating a new soft token for the first
  time.

- *--devid=*/devid/ :: Use a device ID supplied from the command line to
  decrypt the token. A token can be bound to a class GUID device ID
  (i.e. a certain type of device, such as "iPhone" or "Android"), a
  unique device ID (one specific unit), or nothing. *stoken will attempt
  to autodetect matches with a class GUID,* but on rare occasions this
  results in false positives due to hash collisions. In these cases, the
  bound device ID should be specified on the command line to override
  autodetection.

* EXPORT OPTIONS
- *--new-password=*/password/ :: Supply the encryption password from the
  command line for operations that write out a token string or
  /.stokenrc/* file: import, export,* *setpass, and issue. See notes in
  SECURITY CONSIDERATIONS* below.

- *--keep-password* :: If the token in the /.stokenrc/* file is
  protected with a password, retain* the same password when exporting
  the token. By default, the *export* operation will not encrypt the
  token with a password; note that it may not be possible to enter all
  possible passwords on devices with limited text input capabilities
  (such as feature phones).

- *--new-pin=*/pin/ :: Supply a new PIN from the command line for the
  *setpin operation.* See notes in *SECURITY CONSIDERATIONS below.*

- *--new-devid=*/devid/ :: Used with the *export or issue command to
  encrypt the new token* with a specific device ID. This is only used
  for testing purposes.

- *--blocks, --iphone, --android, --v3* :: Used with the *export command
  to select the output format. See examples* in *BASIC USAGE. By
  default, the export command will print an* unformatted 81-digit string
  to standard output.

- *--sdtid, --xml* :: These options are synonyms. Both export a token to
  standard output in RSA's /sdtid/* XML format.*

- *--qr=*/file.png/ :: Encode the token as a QR code and write it to
  /file.png/*. This requires* the *qrencode program to be installed.*

- *--show-qr* :: Encode the token as a QR code and immediately display
  it on the screen. This requires the *qrencode program to be installed.
  If the* *QR_VIEWER environment variable is set, stoken will use that*
  program as the preferred viewer. Otherwise it will try to execute a
  few common Linux image viewers, and give up if none of them exist.

- *--template=*/file/ :: Used with the *export or issue commands to
  override fields in* the XML output. The template file should look like
  any standard /sdtid/ file, but all fields are optional and will
  default to reasonably sane values if omitted. This can be used to
  force the output XML to use a specific serial number, user name,
  expiration date, etc. Correct MAC checksums will be (re)computed on
  the provided values. See the /examples/* directory in the source
  distribution for more information.*

* OTHER OPTIONS
- *--use-time={*/unix_time/*|+*/offset/*|-*/offset/*}* :: Instead of
  generating a tokencode based on the current time of day, force a
  specific time, or adjust the current time based on a positive or
  negative offset (specified in seconds). This is only used for testing
  purposes.

- *--next* :: Generate the next tokencode instead of the current
  tokencode. For a 60-second token, this is equivalent to
  *--use-time=+60.*

- *--stdin, -s* :: When generating a tokencode that requires /either/* a
  password or PIN,* read the password or PIN as single line from
  standard input. This is intended to allow external programs to call
  /stoken/* to generate* single-use passwords without user intervention;
  see *NON-INTERACTIVE USE* below.

- *--force, -f* :: Override token expiration date checks (for
  *tokencode) or token* overwrite checks (for *import).*

- *--batch, -b* :: Abort with an error exit code if any user input is
  required. Intended for automated operation and testing.

- *--file=*/file/ :: Read a ctf string, an Android/iPhone URI, or an XML
  /sdtid/* token from* /file/* instead of the */.stokenrc/*
  configuration. Most stoken* commands accept this flag, but it is
  expected that the typical user will save his token in /~/.stokenrc/*
  instead of supplying it by* hand on every invocation. Typically
  *--file and --token* are only used for the *import command.*

- *--token=*/token_string/ :: Use a token from the command line instead
  of the /.stokenrc/* file. See* above notes on *--file.*

- *--random* :: Generate a random token on the fly. Used for testing or
  demonstrations only. These tokens should *not be used for real
  authentication.*

- *--help, -h* :: Display basic usage information.

- *--version, -v* :: Display version information.

* SECURITY CONSIDERATIONS
Software tokens, unlike hardware tokens, are relatively easy to
replicate. Systems that store soft token seeds should be carefully
guarded to prevent unauthorized disclosure. The use of whole-disk
encryption, such as TrueCrypt, is strongly recommended for laptops and
other portable devices that are easily lost or stolen.

*stoken permits users to store their PIN in */~/.stokenrc/* to* allow
for automated (scriptable) generation of tokencodes, but the risks of
this approach should be carefully weighed against the benefits.

Using the *setpass command to encrypt the seed and PIN in*
/~/.stokenrc/* provides some degree of protection against unauthorized*
access, but does not necessarily cover all possible attack vectors. A
host that is already compromised (e.g. running a keylogger) will not
provide adequate protection for any seed(s) stored on it.

*stoken encryption passwords may be up to 40 characters long.* A longer
passphrase constructed from several random words can provide more
protection from brute-force attacks than a shorter password.

Entering a password or PIN on the command line is generally unsafe on
multiuser systems, as other users may be able to view the command line
arguments in *ps or similar utilities. The command line could* also be
cached in shell history files.

Encoding QR tokens may expose the seed data through *ps, and* the
*--show-qr option writes temporary PNG files in /tmp.*

*stoken attempts to lock pages to prevent swapping out to disk, but*
does not scrub secrets from process memory.

* NON-INTERACTIVE USE
Other applications, such as VPN clients, may want to invoke *stoken*
non-interactively to generate single-use passwords. Three usage modes
are supported, depending on the level of security (and/or convenience)
that is required:

** No password or PIN
The user configures *stoken to print a tokencode immediately upon*
invocation, with no prompts, by using *setpin to store the PIN in*
/~/.stokenrc/* and using setpass to set an empty password.* The other
application can then invoke *stoken --batch and read* the tokencode
through a pipe from standard output.

This provides no security for the seed, but may be useful in
applications where (re-)authentication is frequent or unattended
operation is required.

** Save the PIN and set a password
The user configures *stoken to encrypt the */~/.stokenrc/* secrets* with
a password using *setpass, then saves the PIN with setpin.* The PIN and
the seed will both be encrypted with the password. The other application
will request the password from the user, then call *stoken --stdin,
write the password to stoken's standard input* through a pipe, and read
back a tokencode from *stoken's standard* output.

** No password; prompt for the PIN
Similar to above, but set an empty password using *setpass, do not* save
the PIN in /~/.stokenrc/*, and pass the PIN to stoken --stdin* via
standard input.

* BUGS/TODO
/sdtid/* support is still new and may choke on unexpected input.* As a
short-term workaround you can try commenting out the sanity checks in
*sdtid_decrypt() to see if the problem goes away.*

Features under development include: hardware token seeds (and the
*stoken split command needed to work with* them), and support for
non-Linux hosts.

Patches are always welcome.

* SEE ALSO
*stoken-gui(1).*

* FILES
- ~/.stokenrc :: Default configuration file.

* AUTHOR
Kevin Cernekee <cernekee@gmail.com>
