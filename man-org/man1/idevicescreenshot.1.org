#+TITLE: Man1 - idevicescreenshot.1
#+DESCRIPTION: Linux manpage for idevicescreenshot.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
idevicescreenshot - Gets a screenshot from the connected device.

* SYNOPSIS
*idevicescreenshot* [OPTIONS] [FILE]

* DESCRIPTION
Gets a screenshot from the connected device.

The screenshot is saved as a TIFF image with the given FILE name, where
the default name is "screenshot-DATE.tiff", e.g.:
./screenshot-2013-12-31-23-59-59.tiff

NOTE: A mounted developer disk image is required on the device,
otherwise the screenshotr service is not available.

* OPTIONS
- *-u, --udid UDID* :: target specific device by UDID.

- *-n, --network* :: connect to network device.

- *-d, --debug* :: enable communication debugging.

- *-h, --help* :: prints usage information

- *-v, --version* :: prints version information.

* AUTHOR
Nikias Bassen

Man page written to conform with Debian by Julien Lavergne.

* ON THE WEB
https://libimobiledevice.org

https://github.com/libimobiledevice/libimobiledevice
