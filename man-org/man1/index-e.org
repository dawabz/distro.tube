#+TITLE: Man1 - E
#+DESCRIPTION: Man1 - E
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* E
#+begin_src bash :exports results
readarray -t starts_with_e < <(find . -type f -iname "e*" | sort)

for x in "${starts_with_e[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./e2pall.1.org][e2pall.1]]             |
| [[file:./ebb.1.org][ebb.1]]                |
| [[file:./ebook-convert.1.org][ebook-convert.1]]      |
| [[file:./ebook-edit.1.org][ebook-edit.1]]         |
| [[file:./ebook-meta.1.org][ebook-meta.1]]         |
| [[file:./ebook-polish.1.org][ebook-polish.1]]       |
| [[file:./ebook-viewer.1.org][ebook-viewer.1]]       |
| [[file:./ebrowse.1.org][ebrowse.1]]            |
| [[file:./ec.1ssl.org][ec.1ssl]]              |
| [[file:./echo.1.org][echo.1]]               |
| [[file:./ecparam.1ssl.org][ecparam.1ssl]]         |
| [[file:./ed.1.org][ed.1]]                 |
| [[file:./edgepaint.1.org][edgepaint.1]]          |
| [[file:./efi-readvar.1.org][efi-readvar.1]]        |
| [[file:./efi-updatevar.1.org][efi-updatevar.1]]      |
| [[file:./efivar.1.org][efivar.1]]             |
| [[file:./egrep.1.org][egrep.1]]              |
| [[file:./eject.1.org][eject.1]]              |
| [[file:./elfedit.1.org][elfedit.1]]            |
| [[file:./emacs.1.org][emacs.1]]              |
| [[file:./emacsclient.1.org][emacsclient.1]]        |
| [[file:./enc.1ssl.org][enc.1ssl]]             |
| [[file:./enc2xs.1perl.org][enc2xs.1perl]]         |
| [[file:./encguess.1perl.org][encguess.1perl]]       |
| [[file:./enchant-2.1.org][enchant-2.1]]          |
| [[file:./enchant-lsmod-2.1.org][enchant-lsmod-2.1]]    |
| [[file:./encodedv.1.org][encodedv.1]]           |
| [[file:./encode_keychange.1.org][encode_keychange.1]]   |
| [[file:./engine.1ssl.org][engine.1ssl]]          |
| [[file:./env.1.org][env.1]]                |
| [[file:./envsubst.1.org][envsubst.1]]           |
| [[file:./eptex.1.org][eptex.1]]              |
| [[file:./eqn.1.org][eqn.1]]                |
| [[file:./eqn2graph.1.org][eqn2graph.1]]          |
| [[file:./errstr.1ssl.org][errstr.1ssl]]          |
| [[file:./escp2topbm.1.org][escp2topbm.1]]         |
| [[file:./escputil.1.org][escputil.1]]           |
| [[file:./etags.1.org][etags.1]]              |
| [[file:./eu-elfclassify.1.org][eu-elfclassify.1]]     |
| [[file:./euptex.1.org][euptex.1]]             |
| [[file:./eu-readelf.1.org][eu-readelf.1]]         |
| [[file:./eutp.1.org][eutp.1]]               |
| [[file:./evim.1.org][evim.1]]               |
| [[file:./evince.1.org][evince.1]]             |
| [[file:./evince-previewer.1.org][evince-previewer.1]]   |
| [[file:./evince-thumbnailer.1.org][evince-thumbnailer.1]] |
| [[file:./ex.1.org][ex.1]]                 |
| [[file:./exa.1.org][exa.1]]                |
| [[file:./exiv2.1.org][exiv2.1]]              |
| [[file:./exo-open.1.org][exo-open.1]]           |
| [[file:./expac.1.org][expac.1]]              |
| [[file:./expand.1.org][expand.1]]             |
| [[file:./expiry.1.org][expiry.1]]             |
| [[file:./expr.1.org][expr.1]]               |
| [[file:./extcheck-zulu-8.1.org][extcheck-zulu-8.1]]    |
| [[file:./extconv.1.org][extconv.1]]            |
| [[file:./extlinux.1.org][extlinux.1]]           |
| [[file:./extraclangtools.1.org][extraclangtools.1]]    |
| [[file:./extract_a52.1.org][extract_a52.1]]        |
| [[file:./extractbb.1.org][extractbb.1]]          |
| [[file:./extract_dca.1.org][extract_dca.1]]        |
| [[file:./extract_mpeg2.1.org][extract_mpeg2.1]]      |
| [[file:./eyuvtoppm.1.org][eyuvtoppm.1]]          |
