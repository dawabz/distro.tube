#+TITLE: Manpages - eptex.1
#+DESCRIPTION: Linux manpage for eptex.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about eptex.1 is found in manpage for: [[../man1/ptex.1][man1/ptex.1]]