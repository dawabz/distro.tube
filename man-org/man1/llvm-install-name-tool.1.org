#+TITLE: Man1 - llvm-install-name-tool.1
#+DESCRIPTION: Linux manpage for llvm-install-name-tool.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
llvm-install-name-tool - LLVM tool for manipulating install-names and
rpaths

* SYNOPSIS
*llvm-install-name-tool* [/options/] /input/

* DESCRIPTION
*llvm-install-name-tool* is a tool to manipulate dynamic shared library
install names and rpaths listed in a Mach-O binary.

For most scenarios, it works as a drop-in replacement for Apple's
*install_name_tool*.

* OPTIONS
At least one of the following options are required, and some options can
be combined with other options. Options /-add_rpath/, /-delete_rpath/,
and /-rpath/ can be combined in an invocation only if they do not share
the same /<rpath>/ value.

#+begin_quote
  - *-add_rpath <rpath>* :: Add an rpath named *<rpath>* to the
    specified binary. Can be specified multiple times to add multiple
    rpaths. Throws an error if *<rpath>* is already listed in the
    binary.
#+end_quote

#+begin_quote
  - *-change <old_install_name> <new_install_name>* :: Change an install
    name *<old_install_name>* to *<new_install_name>* in the specified
    binary. Can be specified multiple times to change multiple dependent
    shared library install names. Option is ignored if
    *<old_install_name>* is not listed in the specified binary.
#+end_quote

#+begin_quote
  - *-delete_rpath <rpath>* :: Delete an rpath named *<rpath>* from the
    specified binary. Can be specified multiple times to delete multiple
    rpaths. Throws an error if *<rpath>* is not listed in the binary.
#+end_quote

#+begin_quote
  - *-delete_all_rpaths* :: Deletes all rpaths from the binary.
#+end_quote

#+begin_quote
  - *--help, -h* :: Print a summary of command line options.
#+end_quote

#+begin_quote
  - *-id <name>* :: Change shared library's identification name under
    LC_ID_DYLIB to *<name>* in the specified binary. If specified
    multiple times, only the last /-id/ option is selected. Option is
    ignored if the specified Mach-O binary is not a dynamic shared
    library.
#+end_quote

#+begin_quote
  - *-rpath <old_rpath> <new_rpath>* :: Change an rpath named
    *<old_rpath>* to *<new_rpath>* in the specified binary. Can be
    specified multiple times to change multiple rpaths. Throws an error
    if *<old_rpath>* is not listed in the binary or *<new_rpath>* is
    already listed in the binary.
#+end_quote

#+begin_quote
  - *--version, -V* :: Display the version of the
    *llvm-install-name-tool* executable.
#+end_quote

* EXIT STATUS
*llvm-install-name-tool* exits with a non-zero exit code if there is an
error. Otherwise, it exits with code 0.

* BUGS
To report bugs, please visit </https://bugs.llvm.org//>.

* SEE ALSO
*llvm-objcopy(1)*

* AUTHOR
Maintained by the LLVM Team (https://llvm.org/).

* COPYRIGHT
2003-2021, LLVM Project
