#+TITLE: Man1 - ppmtoleaf.1
#+DESCRIPTION: Linux manpage for ppmtoleaf.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
ppmtoleaf - convert PPM image to Interleaf image format

* SYNOPSIS
*ppmtoleaf*

[/ppmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*ppmtoleaf* reads an Interleaf image file as input and generates a PPM
image as output.

Interleaf is a now-defunct (actually purchased ca. 2000 by BroadVision)
technical publishing software company.

* SEE ALSO
*ppm*(5) , *pnmquant*(1)

* AUTHOR
The program is copyright (C) 1994 by Bill O'Donnell.
