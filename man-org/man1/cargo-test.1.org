#+TITLE: Man1 - cargo-test.1
#+DESCRIPTION: Linux manpage for cargo-test.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cargo-test - Execute unit and integration tests of a package

* SYNOPSIS
*cargo test* [/options/] [/testname/] [*--* /test-options/]

* DESCRIPTION
Compile and execute unit and integration tests.

The test filtering argument *TESTNAME* and all the arguments following
the two dashes (*--*) are passed to the test binaries and thus to
/libtest/ (rustc's built in unit-test and micro-benchmarking framework).
If you're passing arguments to both Cargo and the binary, the ones after
*--* go to the binary, the ones before go to Cargo. For details about
libtest's arguments see the output of *cargo test -- --help* and check
out the rustc book's chapter on how tests work at
<https://doc.rust-lang.org/rustc/tests/index.html>.

As an example, this will filter for tests with *foo* in their name and
run them on 3 threads in parallel:

#+begin_quote
  #+begin_example
    cargo test foo -- --test-threads 3
  #+end_example
#+end_quote

Tests are built with the *--test* option to *rustc* which creates an
executable with a *main* function that automatically runs all functions
annotated with the *#[test]* attribute in multiple threads. *#[bench]*
annotated functions will also be run with one iteration to verify that
they are functional.

The libtest harness may be disabled by setting *harness = false* in the
target manifest settings, in which case your code will need to provide
its own *main* function to handle running tests.

Documentation tests are also run by default, which is handled by
*rustdoc*. It extracts code samples from documentation comments and
executes them. See the /rustdoc book/
<https://doc.rust-lang.org/rustdoc/> for more information on writing doc
tests.

* OPTIONS
** Test Options
*--no-run*

#+begin_quote
  Compile, but don't run tests.
#+end_quote

*--no-fail-fast*

#+begin_quote
  Run all tests regardless of failure. Without this flag, Cargo will
  exit after the first executable fails. The Rust test harness will run
  all tests within the executable to completion, this flag only applies
  to the executable as a whole.
#+end_quote

** Package Selection
By default, when no package selection options are given, the packages
selected depend on the selected manifest file (based on the current
working directory if *--manifest-path* is not given). If the manifest is
the root of a workspace then the workspaces default members are
selected, otherwise only the package defined by the manifest will be
selected.

The default members of a workspace can be set explicitly with the
*workspace.default-members* key in the root manifest. If this is not
set, a virtual workspace will include all workspace members (equivalent
to passing *--workspace*), and a non-virtual workspace will include only
the root crate itself.

*-p* /spec/..., *--package* /spec/...

#+begin_quote
  Test only the specified packages. See *cargo-pkgid*(1) for the SPEC
  format. This flag may be specified multiple times and supports common
  Unix glob patterns like ***, *?* and *[]*. However, to avoid your
  shell accidentally expanding glob patterns before Cargo handles them,
  you must use single quotes or double quotes around each pattern.
#+end_quote

*--workspace*

#+begin_quote
  Test all members in the workspace.
#+end_quote

*--all*

#+begin_quote
  Deprecated alias for *--workspace*.
#+end_quote

*--exclude* /SPEC/...

#+begin_quote
  Exclude the specified packages. Must be used in conjunction with the
  *--workspace* flag. This flag may be specified multiple times and
  supports common Unix glob patterns like ***, *?* and *[]*. However, to
  avoid your shell accidentally expanding glob patterns before Cargo
  handles them, you must use single quotes or double quotes around each
  pattern.
#+end_quote

** Target Selection
When no target selection options are given, *cargo test* will build the
following targets of the selected packages:

#+begin_quote
  ·lib --- used to link with binaries, examples, integration tests, and
  doc tests
#+end_quote

#+begin_quote
  ·bins (only if integration tests are built and required features are
  available)
#+end_quote

#+begin_quote
  ·examples --- to ensure they compile
#+end_quote

#+begin_quote
  ·lib as a unit test
#+end_quote

#+begin_quote
  ·bins as unit tests
#+end_quote

#+begin_quote
  ·integration tests
#+end_quote

#+begin_quote
  ·doc tests for the lib target
#+end_quote

The default behavior can be changed by setting the *test* flag for the
target in the manifest settings. Setting examples to *test = true* will
build and run the example as a test. Setting targets to *test = false*
will stop them from being tested by default. Target selection options
that take a target by name ignore the *test* flag and will always test
the given target.

Doc tests for libraries may be disabled by setting *doctest = false* for
the library in the manifest.

Binary targets are automatically built if there is an integration test
or benchmark. This allows an integration test to execute the binary to
exercise and test its behavior. The *CARGO_BIN_EXE_<name>* /environment
variable/
<https://doc.rust-lang.org/cargo/reference/environment-variables.html#environment-variables-cargo-sets-for-crates>
is set when the integration test is built so that it can use the /*env*
macro/ <https://doc.rust-lang.org/std/macro.env.html> to locate the
executable.

Passing target selection flags will test only the specified targets.

Note that *--bin*, *--example*, *--test* and *--bench* flags also
support common Unix glob patterns like ***, *?* and *[]*. However, to
avoid your shell accidentally expanding glob patterns before Cargo
handles them, you must use single quotes or double quotes around each
glob pattern.

*--lib*

#+begin_quote
  Test the package's library.
#+end_quote

*--bin* /name/...

#+begin_quote
  Test the specified binary. This flag may be specified multiple times
  and supports common Unix glob patterns.
#+end_quote

*--bins*

#+begin_quote
  Test all binary targets.
#+end_quote

*--example* /name/...

#+begin_quote
  Test the specified example. This flag may be specified multiple times
  and supports common Unix glob patterns.
#+end_quote

*--examples*

#+begin_quote
  Test all example targets.
#+end_quote

*--test* /name/...

#+begin_quote
  Test the specified integration test. This flag may be specified
  multiple times and supports common Unix glob patterns.
#+end_quote

*--tests*

#+begin_quote
  Test all targets in test mode that have the *test = true* manifest
  flag set. By default this includes the library and binaries built as
  unittests, and integration tests. Be aware that this will also build
  any required dependencies, so the lib target may be built twice (once
  as a unittest, and once as a dependency for binaries, integration
  tests, etc.). Targets may be enabled or disabled by setting the *test*
  flag in the manifest settings for the target.
#+end_quote

*--bench* /name/...

#+begin_quote
  Test the specified benchmark. This flag may be specified multiple
  times and supports common Unix glob patterns.
#+end_quote

*--benches*

#+begin_quote
  Test all targets in benchmark mode that have the *bench = true*
  manifest flag set. By default this includes the library and binaries
  built as benchmarks, and bench targets. Be aware that this will also
  build any required dependencies, so the lib target may be built twice
  (once as a benchmark, and once as a dependency for binaries,
  benchmarks, etc.). Targets may be enabled or disabled by setting the
  *bench* flag in the manifest settings for the target.
#+end_quote

*--all-targets*

#+begin_quote
  Test all targets. This is equivalent to specifying *--lib --bins
  --tests --benches --examples*.
#+end_quote

*--doc*

#+begin_quote
  Test only the library's documentation. This cannot be mixed with other
  target options.
#+end_quote

** Feature Selection
The feature flags allow you to control which features are enabled. When
no feature options are given, the *default* feature is activated for
every selected package.

See /the features documentation/
<https://doc.rust-lang.org/cargo/reference/features.html#command-line-feature-options>
for more details.

*--features* /features/

#+begin_quote
  Space or comma separated list of features to activate. Features of
  workspace members may be enabled with *package-name/feature-name*
  syntax. This flag may be specified multiple times, which enables all
  specified features.
#+end_quote

*--all-features*

#+begin_quote
  Activate all available features of all selected packages.
#+end_quote

*--no-default-features*

#+begin_quote
  Do not activate the *default* feature of the selected packages.
#+end_quote

** Compilation Options
*--target* /triple/

#+begin_quote
  Test for the given architecture. The default is the host architecture.
  The general format of the triple is
  *<arch><sub>-<vendor>-<sys>-<abi>*. Run *rustc --print target-list*
  for a list of supported targets.

  This may also be specified with the *build.target* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.

  Note that specifying this flag makes Cargo run in a different mode
  where the target artifacts are placed in a separate directory. See the
  /build cache/ <https://doc.rust-lang.org/cargo/guide/build-cache.html>
  documentation for more details.
#+end_quote

*--release*

#+begin_quote
  Test optimized artifacts with the *release* profile. See also the
  *--profile* option for choosing a specific profile by name.
#+end_quote

*--profile* /name/

#+begin_quote
  Test with the given profile. See the /the reference/
  <https://doc.rust-lang.org/cargo/reference/profiles.html> for more
  details on profiles.
#+end_quote

*--ignore-rust-version*

#+begin_quote
  Test the target even if the selected Rust compiler is older than the
  required Rust version as configured in the project's *rust-version*
  field.
#+end_quote

** Output Options
*--target-dir* /directory/

#+begin_quote
  Directory for all generated artifacts and intermediate files. May also
  be specified with the *CARGO_TARGET_DIR* environment variable, or the
  *build.target-dir* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>. Defaults to
  *target* in the root of the workspace.
#+end_quote

** Display Options
By default the Rust test harness hides output from test execution to
keep results readable. Test output can be recovered (e.g., for
debugging) by passing *--nocapture* to the test binaries:

#+begin_quote
  #+begin_example
    cargo test -- --nocapture
  #+end_example
#+end_quote

*-v*, *--verbose*

#+begin_quote
  Use verbose output. May be specified twice for "very verbose" output
  which includes extra output such as dependency warnings and build
  script output. May also be specified with the *term.verbose* /config
  value/ <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

*-q*, *--quiet*

#+begin_quote
  No output printed to stdout.
#+end_quote

*--color* /when/

#+begin_quote
  Control when colored output is used. Valid values:

  #+begin_quote
    ·*auto* (default): Automatically detect if color support is
    available on the terminal.
  #+end_quote

  #+begin_quote
    ·*always*: Always display colors.
  #+end_quote

  #+begin_quote
    ·*never*: Never display colors.
  #+end_quote

  May also be specified with the *term.color* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

*--message-format* /fmt/

#+begin_quote
  The output format for diagnostic messages. Can be specified multiple
  times and consists of comma-separated values. Valid values:

  #+begin_quote
    ·*human* (default): Display in a human-readable text format.
    Conflicts with *short* and *json*.
  #+end_quote

  #+begin_quote
    ·*short*: Emit shorter, human-readable text messages. Conflicts with
    *human* and *json*.
  #+end_quote

  #+begin_quote
    ·*json*: Emit JSON messages to stdout. See /the reference/
    <https://doc.rust-lang.org/cargo/reference/external-tools.html#json-messages>
    for more details. Conflicts with *human* and *short*.
  #+end_quote

  #+begin_quote
    ·*json-diagnostic-short*: Ensure the *rendered* field of JSON
    messages contains the "short" rendering from rustc. Cannot be used
    with *human* or *short*.
  #+end_quote

  #+begin_quote
    ·*json-diagnostic-rendered-ansi*: Ensure the *rendered* field of
    JSON messages contains embedded ANSI color codes for respecting
    rustc's default color scheme. Cannot be used with *human* or
    *short*.
  #+end_quote

  #+begin_quote
    ·*json-render-diagnostics*: Instruct Cargo to not include rustc
    diagnostics in in JSON messages printed, but instead Cargo itself
    should render the JSON diagnostics coming from rustc. Cargo's own
    JSON diagnostics and others coming from rustc are still emitted.
    Cannot be used with *human* or *short*.
  #+end_quote
#+end_quote

** Manifest Options
*--manifest-path* /path/

#+begin_quote
  Path to the *Cargo.toml* file. By default, Cargo searches for the
  *Cargo.toml* file in the current directory or any parent directory.
#+end_quote

*--frozen*, *--locked*

#+begin_quote
  Either of these flags requires that the *Cargo.lock* file is
  up-to-date. If the lock file is missing, or it needs to be updated,
  Cargo will exit with an error. The *--frozen* flag also prevents Cargo
  from attempting to access the network to determine if it is
  out-of-date.

  These may be used in environments where you want to assert that the
  *Cargo.lock* file is up-to-date (such as a CI build) or want to avoid
  network access.
#+end_quote

*--offline*

#+begin_quote
  Prevents Cargo from accessing the network for any reason. Without this
  flag, Cargo will stop with an error if it needs to access the network
  and the network is not available. With this flag, Cargo will attempt
  to proceed without the network if possible.

  Beware that this may result in different dependency resolution than
  online mode. Cargo will restrict itself to crates that are downloaded
  locally, even if there might be a newer version as indicated in the
  local copy of the index. See the *cargo-fetch*(1) command to download
  dependencies before going offline.

  May also be specified with the *net.offline* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

** Common Options
*+*/toolchain/

#+begin_quote
  If Cargo has been installed with rustup, and the first argument to
  *cargo* begins with *+*, it will be interpreted as a rustup toolchain
  name (such as *+stable* or *+nightly*). See the /rustup documentation/
  <https://rust-lang.github.io/rustup/overrides.html> for more
  information about how toolchain overrides work.
#+end_quote

*-h*, *--help*

#+begin_quote
  Prints help information.
#+end_quote

*-Z* /flag/

#+begin_quote
  Unstable (nightly-only) flags to Cargo. Run *cargo -Z help* for
  details.
#+end_quote

** Miscellaneous Options
The *--jobs* argument affects the building of the test executable but
does not affect how many threads are used when running the tests. The
Rust test harness includes an option to control the number of threads
used:

#+begin_quote
  #+begin_example
    cargo test -j 2 -- --test-threads=2
  #+end_example
#+end_quote

*-j* /N/, *--jobs* /N/

#+begin_quote
  Number of parallel jobs to run. May also be specified with the
  *build.jobs* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>. Defaults to
  the number of CPUs.
#+end_quote

* ENVIRONMENT
See /the reference/
<https://doc.rust-lang.org/cargo/reference/environment-variables.html>
for details on environment variables that Cargo reads.

* EXIT STATUS

#+begin_quote
  ·*0*: Cargo succeeded.
#+end_quote

#+begin_quote
  ·*101*: Cargo failed to complete.
#+end_quote

* EXAMPLES

#+begin_quote
  1.Execute all the unit and integration tests of the current package:

  #+begin_quote
    #+begin_example
      cargo test
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  2.Run only tests whose names match against a filter string:

  #+begin_quote
    #+begin_example
      cargo test name_filter
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  3.Run only a specific test within a specific integration test:

  #+begin_quote
    #+begin_example
      cargo test --test int_test_name -- modname::test_name
    #+end_example
  #+end_quote
#+end_quote

* SEE ALSO
*cargo*(1), *cargo-bench*(1)
