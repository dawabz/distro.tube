#+TITLE: Man1 - mdconvert.1
#+DESCRIPTION: Linux manpage for mdconvert.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
mdconvert - Maildir mailbox UID storage scheme converter

* SYNOPSIS
*mdconvert* [/options/ ...] /mailbox/ ...

* DESCRIPTION
*mdconvert* converts Maildir mailboxes between the two UID storage
schemes supported by *mbsync*. See *mbsync*'s manual page for details on
these schemes.

* OPTIONS
- *-a*, *--alt* :: Convert to the *alternative* (Berkeley DB based) UID
  storage scheme.

- *-n*, *--native* :: Convert to the *native* (file name based) UID
  storage scheme. This is the default.

- *-h*, *--help* :: Displays a summary of command line options.

- *-v*, *--version* :: Displays version information.

* SEE ALSO
mbsync(1)

* AUTHOR
Written and maintained by Oswald Buddenhagen <ossi@users.sf.net>.
