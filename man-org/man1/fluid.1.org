#+TITLE: Man1 - fluid.1
#+DESCRIPTION: Linux manpage for fluid.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fluid - the fast light user-interface designer

* SYNOPSIS
fluid [ -c [ -o /code-filename/ -h /header-filename/ ] ] [ /filename.fl/
]

* DESCRIPTION
/fluid/ is an interactive GUI designer for FLTK. When run with no
arguments or with a filename, /fluid/ will display the GUI hierarchy and
any windows defined in the file. Functions, classes, windows, and GUI
components can be manipulated as needed.

When used with the /-c/ option, /fluid/ will create the necessary C++
header and code files in the current directory. You can override the
default extensions, filenames, and directories using the /-o/ and /-h/
options.

* SEE ALSO
fltk-config(1), fltk(3)\\
FLTK Programming Manual, Chapter 9\\
FLTK Web Site, http://www.fltk.org/

* AUTHORS
Bill Spitzak and others.
