#+TITLE: Man1 - idevicedebug.1
#+DESCRIPTION: Linux manpage for idevicedebug.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
idevicedebug - Interact with the debugserver service of a device.

* SYNOPSIS
*idevicedebug* [OPTIONS] COMMAND

* DESCRIPTION
Interact with the debug service of a device. Currently the only
implemented command is "run" and allows execution of developer apps and
watch the stdout/stderr of the process.

* OPTIONS
- *-u, --udid UDID* :: target specific device by UDID.

- *-n, --network* :: connect to network device.

- *-e, --env NAME=VALUE* :: set environment variable NAME to VALUE.

- *-d, --debug* :: enable communication debugging.

- *-h, --help* :: prints usage information.

- *-v, --version* :: prints version information.

* COMMANDS
- *run BUNDLEID [ARGS...]* :: run app with BUNDLEID and optional ARGS on
  device.

* AUTHORS
Martin Szulecki

* ON THE WEB
https://libimobiledevice.org

https://github.com/libimobiledevice/libimobiledevice
