#+TITLE: Man1 - toc2cddb.1
#+DESCRIPTION: Linux manpage for toc2cddb.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
toc2cddb - translates a TOC file of /cdrdao(1)/ into a cddb file and
prints it to stdout

* SYNOPSIS
*toc2cddb* /toc_file/

*toc2cddb* *[ *-h* | *-V* ]*

* DESCRIPTION
*toc2cddb* translates a TOC file of /cdrdao(1)/ into a cddb file and
prints it to stdout.

* OPTIONS
- *-h* :: Shows a short help message.

- *-V* :: Prints the version of toc2cddb.

* SEE ALSO
/cdrdao(1)/

* AUTHOR
*toc2cddb* was written by Giuseppe "Cowo" Corbelli <cowo@lugbs.linux.it>
and is part of /cdrdao(1)/ written by Andreas Mueller
<andreas@daneb.de>.

This manual page was written by Francois Wendling <frwendling@free.fr>
and revised by Daniel Baumann <daniel@debian.org>, for the Debian
project (but may be used by others).
