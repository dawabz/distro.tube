#+TITLE: Man1 - grub-mkrelpath.1
#+DESCRIPTION: Linux manpage for grub-mkrelpath.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
grub-mkrelpath - make a system path relative to its root

* SYNOPSIS
*grub-mkrelpath* [/OPTION/...] /PATH/

* DESCRIPTION
Transform a system filename into GRUB one.

- -?, *--help* :: give this help list

- *--usage* :: give a short usage message

- *-V*, *--version* :: print program version

* REPORTING BUGS
Report bugs to <bug-grub@gnu.org>.

* SEE ALSO
*grub-probe*(8)

The full documentation for *grub-mkrelpath* is maintained as a Texinfo
manual. If the *info* and *grub-mkrelpath* programs are properly
installed at your site, the command

#+begin_quote
  *info grub-mkrelpath*
#+end_quote

should give you access to the complete manual.
