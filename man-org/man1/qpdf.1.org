#+TITLE: Man1 - qpdf.1
#+DESCRIPTION: Linux manpage for qpdf.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
qpdf - PDF transformation software

* SYNOPSIS
*qpdf* [ /options/ ] /infilename/ [ /outfilename/ ]

* DESCRIPTION
The qpdf program is used to convert one PDF file to another equivalent
PDF file. It is capable of performing a variety of transformations such
as linearization (also known as web optimization or fast web viewing),
encryption, and decryption of PDF files. It also has many options for
inspecting or checking PDF files, some of which are useful primarily to
PDF developers.

For a summary of qpdf's options, please run *qpdf --help*. A complete
manual can be found in /usr/share/doc/qpdf/qpdf-manual.html or
/usr/share/doc/qpdf/qpdf-manual.pdf.
