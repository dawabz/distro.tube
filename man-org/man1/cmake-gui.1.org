#+TITLE: Man1 - cmake-gui.1
#+DESCRIPTION: Linux manpage for cmake-gui.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cmake-gui - CMake GUI Command-Line Reference

* SYNOPSIS

#+begin_quote

  #+begin_quote
    #+begin_example
      cmake-gui [<options>]
      cmake-gui [<options>] {<path-to-source> | <path-to-existing-build>}
      cmake-gui [<options>] -S <path-to-source> -B <path-to-build>
      cmake-gui [<options>] --browse-manual
    #+end_example
  #+end_quote
#+end_quote

* DESCRIPTION
The *cmake-gui* executable is the CMake GUI. Project configuration
settings may be specified interactively. Brief instructions are provided
at the bottom of the window when the program is running.

CMake is a cross-platform build system generator. Projects specify their
build process with platform-independent CMake listfiles included in each
directory of a source tree with the name *CMakeLists.txt*. Users build a
project by using CMake to generate a build system for a native tool on
their platform.

* OPTIONS

#+begin_quote
  - **-S* <path-to-source>* :: Path to root directory of the CMake
    project to build.

  - **-B* <path-to-build>* :: Path to directory which CMake will use as
    the root of build directory.

  If the directory doesn't already exist CMake will make it.

  - **--preset=<preset-name>** :: Name of the preset to use from the
    project's *presets* files, if it has them.

  - **--browse-manual** :: Open the CMake reference manual in a browser
    and immediately exit.
#+end_quote

#+begin_quote
  - **--help,-help,-usage,-h,-H,/?** :: Print usage information and
    exit.

  Usage describes the basic command line interface and its options.

  - **--version,-version,/V* [<f>]* :: Show program name/version banner
    and exit.

  If a file is specified, the version is written into it. The help is
  printed to a named <f>ile if given.

  - **--help-full* [<f>]* :: Print all help manuals and exit.

  All manuals are printed in a human-readable text format. The help is
  printed to a named <f>ile if given.

  - **--help-manual* <man> [<f>]* :: Print one help manual and exit.

  The specified manual is printed in a human-readable text format. The
  help is printed to a named <f>ile if given.

  - **--help-manual-list* [<f>]* :: List help manuals available and
    exit.

  The list contains all manuals for which help may be obtained by using
  the *--help-manual* option followed by a manual name. The help is
  printed to a named <f>ile if given.

  - **--help-command* <cmd> [<f>]* :: Print help for one command and
    exit.

  The *cmake-commands(7)* manual entry for *<cmd>* is printed in a
  human-readable text format. The help is printed to a named <f>ile if
  given.

  - **--help-command-list* [<f>]* :: List commands with help available
    and exit.

  The list contains all commands for which help may be obtained by using
  the *--help-command* option followed by a command name. The help is
  printed to a named <f>ile if given.

  - **--help-commands* [<f>]* :: Print cmake-commands manual and exit.

  The *cmake-commands(7)* manual is printed in a human-readable text
  format. The help is printed to a named <f>ile if given.

  - **--help-module* <mod> [<f>]* :: Print help for one module and exit.

  The *cmake-modules(7)* manual entry for *<mod>* is printed in a
  human-readable text format. The help is printed to a named <f>ile if
  given.

  - **--help-module-list* [<f>]* :: List modules with help available and
    exit.

  The list contains all modules for which help may be obtained by using
  the *--help-module* option followed by a module name. The help is
  printed to a named <f>ile if given.

  - **--help-modules* [<f>]* :: Print cmake-modules manual and exit.

  The *cmake-modules(7)* manual is printed in a human-readable text
  format. The help is printed to a named <f>ile if given.

  - **--help-policy* <cmp> [<f>]* :: Print help for one policy and exit.

  The *cmake-policies(7)* manual entry for *<cmp>* is printed in a
  human-readable text format. The help is printed to a named <f>ile if
  given.

  - **--help-policy-list* [<f>]* :: List policies with help available
    and exit.

  The list contains all policies for which help may be obtained by using
  the *--help-policy* option followed by a policy name. The help is
  printed to a named <f>ile if given.

  - **--help-policies* [<f>]* :: Print cmake-policies manual and exit.

  The *cmake-policies(7)* manual is printed in a human-readable text
  format. The help is printed to a named <f>ile if given.

  - **--help-property* <prop> [<f>]* :: Print help for one property and
    exit.

  The *cmake-properties(7)* manual entries for *<prop>* are printed in a
  human-readable text format. The help is printed to a named <f>ile if
  given.

  - **--help-property-list* [<f>]* :: List properties with help
    available and exit.

  The list contains all properties for which help may be obtained by
  using the *--help-property* option followed by a property name. The
  help is printed to a named <f>ile if given.

  - **--help-properties* [<f>]* :: Print cmake-properties manual and
    exit.

  The *cmake-properties(7)* manual is printed in a human-readable text
  format. The help is printed to a named <f>ile if given.

  - **--help-variable* <var> [<f>]* :: Print help for one variable and
    exit.

  The *cmake-variables(7)* manual entry for *<var>* is printed in a
  human-readable text format. The help is printed to a named <f>ile if
  given.

  - **--help-variable-list* [<f>]* :: List variables with help available
    and exit.

  The list contains all variables for which help may be obtained by
  using the *--help-variable* option followed by a variable name. The
  help is printed to a named <f>ile if given.

  - **--help-variables* [<f>]* :: Print cmake-variables manual and exit.

  The *cmake-variables(7)* manual is printed in a human-readable text
  format. The help is printed to a named <f>ile if given.
#+end_quote

* SEE ALSO
The following resources are available to get help using CMake:

#+begin_quote
  - *Home Page* :: /https://cmake.org/

  The primary starting point for learning about CMake.

  - *Online Documentation and Community
    Resources* :: /https://cmake.org/documentation/

  Links to available documentation and community resources may be found
  on this web page.

  - *Discourse Forum* :: /https://discourse.cmake.org/

  The Discourse Forum hosts discussion and questions about CMake.
#+end_quote

* COPYRIGHT
2000-2021 Kitware, Inc. and Contributors
