#+TITLE: Man1 - pbmtogo.1
#+DESCRIPTION: Linux manpage for pbmtogo.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmtogo - convert a PBM image into compressed GraphOn graphics

* SYNOPSIS
*pbmtogo* [/pbmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmtogo* reads a PBM image as input and produces 2D compressed GraphOn
graphics as output.

Be sure to set up your GraphOn with the following modes: 8 bits / no
parity; obeys no XON/XOFF; NULs are accepted. These are all on the Comm
menu. Also, remember to turn off tty post processing. Note that there is
no gotopbm tool.

* SEE ALSO
*pbm*(5)

* AUTHOR
Copyright (C) 1988, 1989 by Jef Poskanzer, Michael Haberler, and Bo
Thide'.
