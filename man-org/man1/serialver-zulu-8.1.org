#+TITLE: Man1 - serialver-zulu-8.1
#+DESCRIPTION: Linux manpage for serialver-zulu-8.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
serialver - Returns the serial version UID for specified classes.

* SYNOPSIS
#+begin_example

  serialver [ options ] [ classnames ]
#+end_example

- /options/ :: The command-line options. See Options.

- /classnames/ :: The classes for which the serialVersionUID is to be
  returned.

* DESCRIPTION
The serialver command returns the serialVersionUID for one or more
classes in a form suitable for copying into an evolving class. When
called with no arguments, the serialver command prints a usage line.

* OPTIONS
- -classpath /path-files/ :: \\
  Sets the search path for application classes and resources. Separate
  classes and resources with a colon (:).

- -show :: \\
  Displays a simple user interface. Enter the full class name and press
  either the /Enter/ key or the /Show/ button to display the
  serialVersionUID.

- -J/option/ :: \\
  Passes option to the Java Virtual Machine, where option is one of the
  options described on the reference page for the Java application
  launcher. For example, -J-Xms48m sets the startup memory to 48 MB. See
  java(1).

* NOTES
The serialver command loads and initializes the specified classes in its
virtual machine, and by default, it does not set a security manager. If
the serialver command is to be run with untrusted classes, then a
security manager can be set with the following option:

#+begin_example
  -J-Djava.security.manager
#+end_example

#+begin_example
#+end_example

When necessary, a security policy can be specified with the following
option:

#+begin_example
  -J-Djava.security.policy=<policy file>
#+end_example

#+begin_example
#+end_example

* SEE ALSO
- · :: policytool(1)

- · :: The java.io.ObjectStream class description at
  http://docs.oracle.com/javase/8/docs/api/java/io/ObjectStreamClass.html

\\
