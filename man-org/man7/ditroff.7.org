#+TITLE: Manpages - ditroff.7
#+DESCRIPTION: Linux manpage for ditroff.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ditroff - classical device-independent roff

* DESCRIPTION
The name /ditroff/ refers to a historical development stage of the
/roff/(7) text processing system. In /roff/ systems extant today, the
name /troff/ is a synonym for /ditroff/.

Early versions of /roff/ by Joe Ossanna generated two programs from the
same sources, using conditional compilation to distinguish them. /nroff/
produced text-oriented TTY output, while /troff/ generated graphical
output for exactly one output device, the Wang Graphic Systems CAT
phototypesetter.

In 1979, Brian Kernighan rewrote /troff/ to support more devices by
creating an intermediate output format for /troff/ that could be fed
into postprocessor programs which actually do the printout on the
device. Kernighan's version marks what is known as “classical troff”
today. In order to distinguish it from Ossanna's original version, it
was called /ditroff/ (/d/evice /i/ndependent /t/roff) on some systems,
though this naming isn't mentioned in the classical documentation.

Today, all existing /roff/ systems are based on Kernighan's multi-device
/troff/. The distinction between /troff/ and /ditroff/ is no longer
necessary; each modern /troff/ provides the complete functionality of
/ditroff/.

The easiest way to use /ditroff/ is via the GNU /roff/ system, /groff/.
The /groff/(1) program is a wrapper around /(di)troff/ that
automatically handles device postprocessing.

* AUTHORS
This document was written by
[[mailto:groff-bernd.warken-72@web.de][Bernd Warken]].

* SEE ALSO
- CSTR #54 :: refers to the 1992 revision of the /Nroff/Troff User's
  Manual/ by J. F. Ossanna and Brian Kernighan.

- CSTR #97 :: refers to /A Typesetter-independent TROFF/, by Brian
  Kernighan and is the original documentation of the first multi-device
  /troff/ (/ditroff/).

- /roff/(7) :: provides a history and conceptual overview of /roff/
  systems.

- /troff/(1) :: describes the GNU implementation of /(di)troff/.

- /groff/(1) :: documents the GNU /roff/ program and includes pointers
  to further documentation about /groff/.

- /groff_out/(5) :: describes the /groff/ version of the intermediate
  output language, the basis for multi-device output.
