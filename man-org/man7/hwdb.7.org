#+TITLE: Manpages - hwdb.7
#+DESCRIPTION: Linux manpage for hwdb.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwdb - Hardware Database

* DESCRIPTION
The hardware database is a key-value store for associating modalias-like
keys to udev-property-like values. It is used primarily by udev to add
the relevant properties to matching devices, but it can also be queried
directly.

* HARDWARE DATABASE FILES
The hwdb files are read from the files located in the system hwdb
directory /usr/lib/udev/hwdb.d and the local administration directory
/etc/udev/hwdb.d. All hwdb files are collectively sorted and processed
in lexical order, regardless of the directories in which they live.
However, files with identical filenames replace each other. Files in
/etc/ have the highest priority and take precedence over files with the
same name in /usr/lib/. This can be used to override a system-supplied
hwdb file with a local file if needed; a symlink in /etc/ with the same
name as a hwdb file in /usr/lib/, pointing to /dev/null, disables that
hwdb file entirely. hwdb files must have the extension .hwdb; other
extensions are ignored.

Each hwdb file contains data records consisting of matches and
associated key-value pairs. Every record in the hwdb starts with one or
more match strings, specifying a shell glob to compare the lookup string
against. Multiple match lines are specified in consecutive lines. Every
match line is compared individually, and they are combined by OR. Every
match line must start at the first character of the line.

Match patterns consist of literal characters, and shell-style wildcards:

#+begin_quote
  ·

  Asterisk "*" matches any number of characters
#+end_quote

#+begin_quote
  ·

  Question mark "?" matches a single character
#+end_quote

#+begin_quote
  ·

  Character list "[/chars/]" matches one of the characters /chars/
  listed between "[" and "]". A range may be specified as with a dash as
  "[/first/-/last/]". The match may be inverted with a caret "[^...]".
#+end_quote

The match lines are followed by one or more key-value pair lines, which
are recognized by a leading space character. The key name and value are
separated by "=". An empty line signifies the end of a record. Lines
beginning with "#" are ignored.

In case multiple records match a given lookup string, the key-value
pairs from all records are combined. If a key is specified multiple
times, the value from the record with the highest priority is used (each
key can have only a single value). The priority is higher when the
record is in a file that sorts later lexicographically, and in case of
records in the same file, later records have higher priority.

The content of all hwdb files is read by *systemd-hwdb*(8) and compiled
to a binary database located at /etc/udev/hwdb.bin, or alternatively
/usr/lib/udev/hwdb.bin if you want ship the compiled database in an
immutable image. During runtime, only the binary database is used.

* EXAMPLES
*Example 1. General syntax of hwdb files*

#+begin_quote
  #+begin_example
    # /usr/lib/udev/hwdb.d/example.hwdb
    # Comments can be placed before any records. This is a good spot
    # to describe what that file is used for, what kind of properties
    # it defines, and the ordering convention.

    # A record with three matches and one property
    mouse:*:name:*Trackball*:*
    mouse:*:name:*trackball*:*
    mouse:*:name:*TrackBall*:*
     ID_INPUT_TRACKBALL=1

    # The rule above could be also be written in a form that
    # matches Tb, tb, TB, tB:
    mouse:*:name:*[tT]rack[bB]all*:*
     ID_INPUT_TRACKBALL=1

    # A record with a single match and five properties
    mouse:usb:v046dp4041:name:Logitech MX Master:*
     MOUSE_DPI=1000@166
     MOUSE_WHEEL_CLICK_ANGLE=15
     MOUSE_WHEEL_CLICK_ANGLE_HORIZONTAL=26
     MOUSE_WHEEL_CLICK_COUNT=24
     MOUSE_WHEEL_CLICK_COUNT_HORIZONTAL=14
  #+end_example
#+end_quote

*Example 2. Overriding of properties*

#+begin_quote
  #+begin_example
    # /usr/lib/udev/hwdb.d/60-keyboard.hwdb
    evdev:atkbd:dmi:bvn*:bvr*:bd*:svnAcer*:pn*:*
     KEYBOARD_KEY_a1=help
     KEYBOARD_KEY_a2=setup
     KEYBOARD_KEY_a3=battery

    # Match vendor name "Acer" and any product name starting with "X123"
    evdev:atkbd:dmi:bvn*:bvr*:bd*:svnAcer:pnX123*:*
     KEYBOARD_KEY_a2=wlan

    # /etc/udev/hwdb.d/70-keyboard.hwdb
    # disable wlan key on all at keyboards
    evdev:atkbd:*
     KEYBOARD_KEY_a2=reserved
     PROPERTY_WITH_SPACES=some string
  #+end_example
#+end_quote

If the hwdb consists of those two files, a keyboard with the lookup
string "evdev:atkbd:dmi:bvnAcer:bdXXXXX:bd08/05/2010:svnAcer:pnX123"
will match all three records, and end up with the following properties:

#+begin_quote
  #+begin_example
    KEYBOARD_KEY_a1=help
    KEYBOARD_KEY_a2=reserved
    KEYBOARD_KEY_a3=battery
    PROPERTY_WITH_SPACES=some string
  #+end_example
#+end_quote

* SEE ALSO
*systemd-hwdb*(8)
