#+TITLE: Manpages - ompi_crcp.7
#+DESCRIPTION: Linux manpage for ompi_crcp.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
OMPI_CRCP - Open MPI MCA Checkpoint/Restart Coordination Protocol (CRCP)
Framework: Overview of Open MPI's CRCP framework, and selected modules.
Open MPI 4.1.2

* DESCRIPTION
The CRCP Framework is used by Open MPI for the encapsulation of various
Checkpoint/Restart Coordination Protocols (e.g., Coordinated,
Uncoordinated, Message/Communication Induced, ...).

* GENERAL PROCESS REQUIREMENTS
In order for a process to use the Open MPI CRCP components it must
adhear to a few programmatic requirements.

First, the program must call /MPI_INIT/ early in its execution.

The program must call /MPI_FINALIZE/ before termination.

A user may initiate a checkpoint of a parallel application by using the
ompi-checkpoint(1) and ompi-restart(1) commands.

* AVAILABLE COMPONENTS
Open MPI currently ships with one CRCP component: /coord/.

The following MCA parameters apply to all components:

- crcp_base_verbose :: Set the verbosity level for all components.
  Default is 0, or silent except on error.

** coord CRCP Component
The /coord/ component implements a Coordinated Checkpoint/Restart
Coordination Protocol similar to the one implemented in LAM/MPI.

The /coord/ component has the following MCA parameters:

- crcp_coord_priority :: The component's priority to use when selecting
  the most appropriate component for a run.

- crcp_coord_verbose :: Set the verbosity level for this component.
  Default is 0, or silent except on error.

** none CRCP Component
The /none/ component simply selects no CRCP component. All of the CRCP
function calls return immediately with OMPI_SUCCESS.

This component is the last component to be selected by default. This
means that if another component is available, and the /none/ component
was not explicity requested then Open MPI will attempt to activate all
of the available components before falling back to this component.

* SEE ALSO
ompi-checkpoint(1), ompi-restart(1), opal-checkpoint(1),
opal-restart(1), orte_snapc(7), orte_filem(7), opal_crs(7)
