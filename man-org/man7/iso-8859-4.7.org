#+TITLE: Manpages - iso-8859-4.7
#+DESCRIPTION: Linux manpage for iso-8859-4.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about iso-8859-4.7 is found in manpage for: [[../man7/iso_8859-4.7][man7/iso_8859-4.7]]