#+TITLE: Manpages - zmq_tipc.7
#+DESCRIPTION: Linux manpage for zmq_tipc.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_tipc - 0MQ unicast transport using TIPC

* SYNOPSIS
TIPC is a cluster IPC protocol with a location transparent addressing
scheme.

* ADDRESSING
A 0MQ endpoint is a string consisting of a /transport/:// followed by an
/address/. The /transport/ specifies the underlying protocol to use. The
/address/ specifies the transport-specific address to connect to.

For the TIPC transport, the transport is tipc, and the meaning of the
/address/ part is defined below.

** Assigning a port name to a socket
When assigning a port name to a socket using /zmq_bind()/ with the
/tipc/ transport, the /endpoint/ is defined in the form: {type, lower,
upper}

#+begin_quote
  ·

  Type is the numerical (u32) ID of your service.
#+end_quote

#+begin_quote
  ·

  Lower and Upper specify a range for your service.
#+end_quote

Publishing the same service with overlapping lower/upper ID's will cause
connection requests to be distributed over these in a round-robin
manner.

** Connecting a socket
When connecting a socket to a peer address using /zmq_connect()/ with
the /tipc/ transport, the /endpoint/ shall be interpreted as a service
ID, followed by a comma and the instance ID.

The instance ID must be within the lower/upper range of a published port
name for the endpoint to be valid.

* EXAMPLES
*Assigning a local address to a socket*.

#+begin_quote
  #+begin_example
    //  Publish TIPC service ID 5555
    rc = zmq_bind(socket, "tipc://{5555,0,0}");
    assert (rc == 0);
    //  Publish TIPC service ID 5555 with a service range of 0-100
    rc = zmq_bind(socket, "tipc://{5555,0,100}");
    assert (rc == 0);
  #+end_example
#+end_quote

*Connecting a socket*.

#+begin_quote
  #+begin_example
    //  Connect to service 5555 instance id 50
    rc = zmq_connect(socket, "tipc://{5555,50}");
    assert (rc == 0);
  #+end_example
#+end_quote

* SEE ALSO
*zmq_bind*(3) *zmq_connect*(3) *zmq_tcp*(7) *zmq_pgm*(7) *zmq_ipc*(7)
*zmq_inproc*(7) *zmq_vmci*(7) *zmq*(7)

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
