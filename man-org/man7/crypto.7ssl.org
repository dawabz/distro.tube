#+TITLE: Manpages - crypto.7ssl
#+DESCRIPTION: Linux manpage for crypto.7ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
crypto - OpenSSL cryptographic library

* SYNOPSIS
See the individual manual pages for details.

* DESCRIPTION
The OpenSSL *crypto* library implements a wide range of cryptographic
algorithms used in various Internet standards. The services provided by
this library are used by the OpenSSL implementations of SSL, TLS and
S/MIME, and they have also been used to implement SSH, OpenPGP, and
other cryptographic standards.

*libcrypto* consists of a number of sub-libraries that implement the
individual algorithms.

The functionality includes symmetric encryption, public key cryptography
and key agreement, certificate handling, cryptographic hash functions,
cryptographic pseudo-random number generator, and various utilities.

* NOTES
Some of the newer functions follow a naming convention using the numbers
*0* and *1*. For example the functions:

int X509_CRL_add0_revoked(X509_CRL *crl, X509_REVOKED *rev); int
X509_add1_trust_object(X509 *x, const ASN1_OBJECT *obj);

The *0* version uses the supplied structure pointer directly in the
parent and it will be freed up when the parent is freed. In the above
example *crl* would be freed but *rev* would not.

The *1* function uses a copy of the supplied structure pointer (or in
some cases increases its link count) in the parent and so both (*x* and
*obj* above) should be freed up.

* RETURN VALUES
See the individual manual pages for details.

* SEE ALSO
*openssl* (1), *ssl* (7)

* COPYRIGHT
Copyright 2000-2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
