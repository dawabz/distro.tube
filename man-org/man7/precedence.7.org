#+TITLE: Manpages - precedence.7
#+DESCRIPTION: Linux manpage for precedence.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about precedence.7 is found in manpage for: [[../man7/operator.7][man7/operator.7]]