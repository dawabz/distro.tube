#+TITLE: Manpages - operator.7
#+DESCRIPTION: Linux manpage for operator.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
operator - C operator precedence and order of evaluation

* DESCRIPTION
This manual page lists C operators and their precedence in evaluation.

| Operator                          | Associativity | Notes |
| () [] -> . ++ --                  | left to right | [1]   |
| ! ~ ++ -- + - (type) * & sizeof   | right to left | [2]   |
| * / %                             | left to right |       |
| + -                               | left to right |       |
| << >>                             | left to right |       |
| < <= > >=                         | left to right |       |
| == !=                             | left to right |       |
| &                                 | left to right |       |
| ^                                 | left to right |       |
| |                                 | left to right |       |
| &&                                | left to right |       |
| ||                                | left to right |       |
| ?:                                | right to left |       |
| = += -= *= /= %= <<= >>= &= ^= |= | right to left |       |
| ,                                 | left to right |       |

The following notes provide further information to the above table:

- [1] :: The ++ and -- operators at this precedence level are the
  postfix flavors of the operators.

- [2] :: The ++ and -- operators at this precedence level are the prefix
  flavors of the operators.

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
