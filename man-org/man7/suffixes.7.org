#+TITLE: Manpages - suffixes.7
#+DESCRIPTION: Linux manpage for suffixes.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
suffixes - list of file suffixes

* DESCRIPTION
It is customary to indicate the contents of a file with the file suffix,
which (typically) consists of a period, followed by one or more letters.
Many standard utilities, such as compilers, use this to recognize the
type of file they are dealing with. The *make*(1) utility is driven by
rules based on file suffix.

Following is a list of suffixes which are likely to be found on a Linux
system.

TABLE

* CONFORMING TO
General UNIX conventions.

* BUGS
This list is not exhaustive.

* SEE ALSO
*file*(1), *make*(1)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
