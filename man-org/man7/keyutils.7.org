#+TITLE: Manpages - keyutils.7
#+DESCRIPTION: Linux manpage for keyutils.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
keyutils - in-kernel key management utilities

* DESCRIPTION
The *keyutils* package is a library and a set of utilities for accessing
the kernel *keyrings* facility.

A header file is supplied to provide the definitions and declarations
required to access the library:

#+begin_quote
  *#include <keyutils.h>*
#+end_quote

To link with the library, the following:

#+begin_quote
  *-lkeyutils*
#+end_quote

should be specified to the linker.

Three system calls are provided:

- *add_key*(2) :: Supply a new key to the kernel.

- *request_key*(2) :: Find an existing key for use, or, optionally,
  create one if one does not exist.

- *keyctl*(2) :: Control a key in various ways. The library provides a
  variety of wrappers around this system call and those should be used
  rather than calling it directly.

See the *add_key*(2), *request_key*(2), and *keyctl*(2) manual pages for
more information.

The *keyctl*() wrappers are listed on the *keyctl*(3) manual page.

* UTILITIES
A program is provided to interact with the kernel facility by a number
of subcommands, e.g.:

#+begin_quote
  *keyctl add user foo bar @s*
#+end_quote

See the *keyctl*(1) manual page for information on that.

The kernel has the ability to upcall to userspace to fabricate new keys.
This can be triggered by *request_key*(), but userspace is better off
using *add_key*() instead if it possibly can.

The upcalling mechanism is usually routed via the *request-key*(8)
program. What this does with any particular key is configurable in:

#+begin_quote
  //etc/request-key.conf/\\
  //etc/request-key.d//
#+end_quote

See the *request-key.conf*(5) and the *request-key*(8) manual pages for
more information.

* SEE ALSO
*keyctl*(1), *keyctl*(3), *keyrings*(7), *persistent-keyring*(7),
*process-keyring*(7), *session-keyring*(7), *thread-keyring*(7),
*user-keyring*(7), *user-session-keyring*(7), *pam_keyinit*(8)
