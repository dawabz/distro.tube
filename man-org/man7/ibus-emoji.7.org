#+TITLE: Manpages - ibus-emoji.7
#+DESCRIPTION: Linux manpage for ibus-emoji.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*ibus-emoji* - Call the IBus emoji utility by *IBus Emojier*

* SYNOPSIS
*/usr/libexec/ibus-ui-emojier* [/OPTION/]...

* DESCRIPTION
IBus Emojier provides a GUI to select an emoji by typing an emoji
annotation or choosing a character with mouse click and it's designed to
work as an extended IBus lookup window using Space, Enter, and Arrow
keys. The text entry accepts an emoji annotation or Unicode points. If
IBus panel is launched, IBus panel calls IBus Emojier with a shortcut
key or the right click menu and output the selected emoji on the
previous focused text on an application. If IBus panel is not available
likes GNOME desktop, You can still use *ibus emoji* command to launch
IBus Emojier and it saves the selected emoji in your clipboard and you
can paste the emoji by a paste key likes Control-v.

Refer *emoji* section in *ibus (1)* for the command line options.

Homepage: https://github.com/ibus/ibus/wiki

* SETTINGS
- *ANNOTATION LANGUAGE* :: You can choose a language of emoji
  annotations with *ibus-setup (1).*

- *FONT* :: You can choose an emoji font with *ibus-setup (1).* E.g.
  "Noto Color Emoji", "Android Emoji" font.

* KEYBOARD OPERATIONS
- *Control-Shift-e* :: Launch IBus Emojier. The shortcut key can be
  customized by *ibus-setup (1).*

- *Left, Right arrow, Control-b or Control-f* :: Select the next left or
  right category or emoji on the list if an annotation is not typed.
  Otherwise move the cursor to the immdediate left or right in the typed
  annotation.

- *Down, Up arrow, Control-n or Control-p* :: Select the next top or
  down category or emoji on the list

- *Enter* :: Commit the selected emoji.

- *Escape* :: Go back to the category list from emoji list, erase the
  typed annotation, and close IBus Emojier.

- *Space* :: Select the next left category or emoji on the list. If
  Shift-Space is typed, a space chararacter can be inserted between
  words in an annotation or Unicode points. E.g. 1f466 Shift-Space 1f3fb

- *PageDown, PageUp, Control-Shift-n or Control-Shift-p* :: Move to the
  next or previous page in the category or emoji list.

- *Head, End, Control-h or Control-e* :: Select the first or last emoji
  on the list if an annotation is not typed. Otherwise move the cursor
  to the head or end in the typed annotation.

- *Control-u* :: Erase the typed annotation.

- *Control-x or Control-v or Control-c* :: Cut the selected annotation
  to the clipboard with Control-x. Paste the contents of the clipboard
  into the annotation entry with Control-v. Copy the selected annotation
  to the clipboard with Control-c.

- *Control-Shift-c* :: Copy the selected emoji to the clipboard.

* BUGS
If you find a bug, please report it at
https://github.com/ibus/ibus/issues
