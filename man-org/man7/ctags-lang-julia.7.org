#+TITLE: Manpages - ctags-lang-julia.7
#+DESCRIPTION: Linux manpage for ctags-lang-julia.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ctags-lang-julia - Random notes about tagging Julia source code with
Universal-ctags

* SYNOPSIS
#+begin_example
  ctags ... --languages=+Julia ...
  ctags ... --language-force=Julia ...
  ctags ... --map-Julia=+.jl ...
#+end_example

* DESCRIPTION
This man page gathers random notes about tagging Julia source code.

* TAGGING IMPORT AND USING EXPRESSIONS
** Summary
/using X/

#+begin_quote

  #+begin_quote
    | _    |        |      |                         |
    | name | kind   | role | other noticeable fields |
    | _    |        |      |                         |
    | X    | module | used | N/A                     |
    | _    |        |      |                         |
  #+end_quote
#+end_quote

/using X: a, b/

#+begin_quote

  #+begin_quote
    | _    |         |           |                         |
    | name | kind    | role      | other noticeable fields |
    | _    |         |           |                         |
    | X    | module  | namespace | N/A                     |
    | _    |         |           |                         |
    | a, b | unknown | used      | scope:module:X          |
    | _    |         |           |                         |
  #+end_quote
#+end_quote

/import X/

#+begin_quote

  #+begin_quote
    | _    |        |          |                         |
    | name | kind   | role     | other noticeable fields |
    | _    |        |          |                         |
    | X    | module | imported | N/A                     |
    | _    |        |          |                         |
  #+end_quote
#+end_quote

/import X.a, Y.b/

#+begin_quote

  #+begin_quote
    | _    |         |           |                         |
    | name | kind    | role      | other noticeable fields |
    | _    |         |           |                         |
    | X, Y | module  | namespace | N/A                     |
    | _    |         |           |                         |
    | a    | unknown | imported  | scope:module:X          |
    | _    |         |           |                         |
    | b    | unknown | imported  | scope:module:Y          |
    | _    |         |           |                         |
  #+end_quote
#+end_quote

/import X: a, b/

#+begin_quote

  #+begin_quote
    | _    |         |           |                         |
    | name | kind    | role      | other noticeable fields |
    | _    |         |           |                         |
    | X    | module  | namespace | N/A                     |
    | _    |         |           |                         |
    | a,b  | unknown | imported  | scope:module:X          |
    | _    |         |           |                         |
  #+end_quote
#+end_quote

** Examples
"input.jl"

#+begin_quote

  #+begin_quote
    #+begin_example
      using X0
    #+end_example
  #+end_quote
#+end_quote

"output.tags" with "--options=NONE -o - --extras=+r --fields=+rzK
input.jl"

#+begin_quote

  #+begin_quote
    #+begin_example
      X0      input.jl        /^using X0$/;"  kind:module     roles:used
    #+end_example
  #+end_quote
#+end_quote

*--extras=+r* (or *--extras=+{reference}*) option is needed for this
tag, since it's a reference tag. This is because module *X* is not
defined here. It is defined in another file. Enable *roles:* field with
*--fields=+r* is for recording that the module is "used", i.e., loaded
by *using*.

"input.jl"

#+begin_quote

  #+begin_quote
    #+begin_example
      import X1.a, X2.b, X3
    #+end_example
  #+end_quote
#+end_quote

"output.tags" with "--options=NONE -o - --extras=+r --fields=+rzKZ
input.jl"

#+begin_quote

  #+begin_quote
    #+begin_example
      X1      input.jl        /^import X1.a, X2.b, X3$/;"     kind:module     roles:namespace
      X2      input.jl        /^import X1.a, X2.b, X3$/;"     kind:module     roles:namespace
      X3      input.jl        /^import X1.a, X2.b, X3$/;"     kind:module     roles:imported
      a       input.jl        /^import X1.a, X2.b, X3$/;"     kind:unknown    scope:module:X1 roles:imported
      b       input.jl        /^import X1.a, X2.b, X3$/;"     kind:unknown    scope:module:X2 roles:imported
    #+end_example
  #+end_quote
#+end_quote

Why *X1* and *X2* have role "namespace", while *X3* have role
"imported"? It's because the symbol *a* in module *X1*, and *b* in
module *X2* are brought to the current scope, but *X1* and *X2*
themselves are not. We use "namespace" role for such modules.

*X3* is different. The symbol *X3*, together with all exported symbols
in *X3*, is brought to current scope. For such modules, we use
"imported" or "used" role depending whether they are loaded by *import*
or *using*.

Also, notice that *a* and *b* have the "unknown" kind. This is because
we cannot know whether it's a function, constant, or macro, etc.

* SEE ALSO
ctags(1), ctags-client-tools(7)
