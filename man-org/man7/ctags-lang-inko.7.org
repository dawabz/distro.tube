#+TITLE: Manpages - ctags-lang-inko.7
#+DESCRIPTION: Linux manpage for ctags-lang-inko.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ctags-lang-inko -

* SYNOPSIS
#+begin_example
  ctags ... --languages=+Inko ...
  ctags ... --language-force=Inko ...
  ctags ... --map-Inko=+.inko ...
#+end_example

* DESCRIPTION
This man page describes the Inko parser for Universal Ctags.

The input file is expected to be valid Inko source code, otherwise the
output of ctags is undefined.

Tags are generated for objects, traits, methods, attributes, and
constants. String literals are ignored.

* SEE ALSO
ctags(1), ctags-client-tools(7)
