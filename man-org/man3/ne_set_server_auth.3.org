#+TITLE: Manpages - ne_set_server_auth.3
#+DESCRIPTION: Linux manpage for ne_set_server_auth.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ne_set_server_auth, ne_set_proxy_auth, ne_forget_auth - register
authentication callbacks

* SYNOPSIS
#+begin_example
  #include <ne_auth.h>
#+end_example

*typedef int (*ne_auth_creds)(void **/userdata/*, const char **/realm/*,
int */attempt/*, char **/username/*, char **/password/*);*

*void ne_set_server_auth(ne_session **/session/*, ne_auth_creds
*/callback/*, void **/userdata/*);*

*void ne_set_proxy_auth(ne_session **/session/*, ne_auth_creds
*/callback/*, void **/userdata/*);*

*void ne_forget_auth(ne_session **/session/*);*

* DESCRIPTION
The *ne_auth_creds* function type defines a callback which is invoked
when a server or proxy server requires user authentication for a
particular request. The /realm/ string is supplied by the server. The
/attempt/ is a counter giving the number of times the request has been
retried with different authentication credentials. The first time the
callback is invoked for a particular request, /attempt/ will be zero.

To retry the request using new authentication credentials, the callback
should return zero, and the /username/ and /password/ buffers must
contain NUL-terminated strings. The NE_ABUFSIZ constant gives the size
of these buffers.

#+begin_quote
  \\

  *Tip*

  \\

  If you only wish to allow the user one attempt to enter credentials,
  use the value of the /attempt/ parameter as the return value of the
  callback.
#+end_quote

To abort the request, the callback should return a non-zero value; in
which case the contents of the /username/ and /password/ buffers are
ignored.

The *ne_forget_auth* function can be used to discard the cached
authentication credentials.

* EXAMPLES

#+begin_quote
  #+begin_example
    /* Function which prompts for a line of user input: */
    extern char *prompt_for(const char *prompt);

    static int
    my_auth(void *userdata, const char *realm, int attempts,
            char *username, char *password)
    {
       strncpy(username, prompt_for("Username: "), NE_ABUFSIZ);
       strncpy(password, prompt_for("Password: "), NE_ABUFSIZ);
       return attempts;
    }

    int main(...)
    {
       ne_session *sess = ne_session_create(...);

       ne_set_server_auth(sess, my_auth, NULL);

       /* ... */
    }
  #+end_example
#+end_quote

* AUTHOR
*Joe Orton* <neon@lists.manyfish.co.uk>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
