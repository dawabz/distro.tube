#+TITLE: Manpages - XISetClientPointer.3
#+DESCRIPTION: Linux manpage for XISetClientPointer.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XISetClientPointer, XIGetClientPointer - set or get the ClientPointer
device.

* SYNOPSIS
#+begin_example
  #include <X11/extensions/XInput2.h>
#+end_example

#+begin_example
  XISetClientPointer( Display *display,
                      Window win,
                      int deviceid);
#+end_example

#+begin_example
  Bool XIGetClientPointer( Display *display,
                           Window win,
                           int *device);
#+end_example

#+begin_example
  display
         Specifies the connection to the X server.
#+end_example

#+begin_example
  win
         Specifies a window belonging to the client. May be None.
#+end_example

#+begin_example
  deviceid
         Specifies the ClientPointer device.
#+end_example

* DESCRIPTION

#+begin_quote
  #+begin_example
    The ClientPointer is the device that is perceived to be the
    core pointer for non-XI protocol requests and replies. Each
    time a protocol message needs device-dependent data and the
    device is not explicitly given, the ClientPointer device is
    used to obtain the data. For example, a XQueryPointer request
    will return the coordinates of the ClientPointer.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XISetClientPointer request sets the ClientPointer device for
    the client that owns the given window. If win is None, the
    requesting clients ClientPointer is set to the device
    specified with deviceid. Only master pointer devices can be set
    as ClientPointer.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XISetClientPointer and can generate a BadDevice and a BadWindow
    error.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The XIGetClientPointer request returns the ClientPointers
    device ID for the client that owns the given window. If win is
    None, the requesting clients ClientPointer is returned.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    win may be a client ID instead of a window.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIGetClientPointer can generate a BadWindow error.
  #+end_example
#+end_quote

* DIAGNOSTICS

#+begin_quote
  #+begin_example
    BadDevice
           An invalid device was specified. The device does not
           exist or is not a master pointer device.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadWindow
           A value for a Window argument does not name a defined
           window.
  #+end_example
#+end_quote
