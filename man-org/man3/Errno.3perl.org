#+TITLE: Manpages - Errno.3perl
#+DESCRIPTION: Linux manpage for Errno.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Errno - System errno constants

* SYNOPSIS
use Errno qw(EINTR EIO :POSIX);

* DESCRIPTION
=Errno= defines and conditionally exports all the error constants
defined in your system /errno.h/ include file. It has a single export
tag, =:POSIX=, which will export all POSIX defined error numbers.

On Windows, =Errno= also defines and conditionally exports all the
Winsock error constants defined in your system /WinError.h/ include
file. These are included in a second export tag, =:WINSOCK=.

=Errno= also makes =%!= magic such that each element of =%!= has a
non-zero value only if =$!= is set to that value. For example:

my $fh; unless (open($fh, "<", "/fangorn/spouse")) { if ($!{ENOENT}) {
warn "Get a wife!\n"; } else { warn "This path is barred: $!"; } }

If a specified constant =EFOO= does not exist on the system, =$!{EFOO}=
returns =""=. You may use =exists $!{EFOO}= to check whether the
constant is available on the system.

Perl automatically loads =Errno= the first time you use =%!=, so you
don't need an explicit =use=.

* CAVEATS
Importing a particular constant may not be very portable, because the
import will fail on platforms that do not have that constant. A more
portable way to set =$!= to a valid value is to use:

if (exists &Errno::EFOO) { $! = &Errno::EFOO; }

* AUTHOR
Graham Barr <gbarr@pobox.com>

* COPYRIGHT
Copyright (c) 1997-8 Graham Barr. All rights reserved. This program is
free software; you can redistribute it and/or modify it under the same
terms as Perl itself.
