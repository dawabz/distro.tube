#+TITLE: Manpages - FcUtf8ToUcs4.3
#+DESCRIPTION: Linux manpage for FcUtf8ToUcs4.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcUtf8ToUcs4 - convert UTF-8 to UCS4

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

int FcUtf8ToUcs4 (FcChar8 */src/*, FcChar32 **/dst/*, int */len/*);*

* DESCRIPTION
Converts the next Unicode char from /src/ into /dst/ and returns the
number of bytes containing the char. /src/ must be at least /len/ bytes
long.
