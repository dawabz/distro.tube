#+TITLE: Manpages - XkbFindOverlayForKey.3
#+DESCRIPTION: Linux manpage for XkbFindOverlayForKey.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbFindOverlayForKey - Find the alternate name by using the primary name
for a key that is part of an overlay

* SYNOPSIS
*char * XkbFindOverlayForKey* *( XkbGeometryPtr */geom/* ,*
*XkbSectionPtr */section/* ,* *char **/under/* );*

* ARGUMENTS
- /- geom/ :: geometry that contains the section

- /- section/ :: section to be searched for matching keys

- /- under/ :: primary name of the key to be considered

* DESCRIPTION
Keys that can generate multiple keycodes may be associated with multiple
names. Such keys have a primary name and an alternate name.

/XkbFindOverlayForKey/ uses the primary name of the key, /under,/ to
look up the alternate name, which it returns.
