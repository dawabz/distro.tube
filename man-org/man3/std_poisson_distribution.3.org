#+TITLE: Manpages - std_poisson_distribution.3
#+DESCRIPTION: Linux manpage for std_poisson_distribution.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::poisson_distribution< _IntType > - A discrete Poisson random number
distribution.

* SYNOPSIS
\\

=#include <random.h>=

** Classes
struct *param_type*\\

** Public Types
typedef _IntType *result_type*\\

** Public Member Functions
*poisson_distribution* (const *param_type* &__p)\\

*poisson_distribution* (double __mean)\\

template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void *__generate* (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator &__urng)\\

template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void *__generate* (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator &__urng, const
*param_type* &__p)\\

template<typename _UniformRandomNumberGenerator > void *__generate*
(*result_type* *__f, *result_type* *__t, _UniformRandomNumberGenerator
&__urng, const *param_type* &__p)\\

*result_type* *max* () const\\
Returns the least upper bound value of the distribution.

double *mean* () const\\
Returns the distribution parameter =mean=.

*result_type* *min* () const\\
Returns the greatest lower bound value of the distribution.

template<typename _UniformRandomNumberGenerator > *result_type*
*operator()* (_UniformRandomNumberGenerator &__urng)\\
Generating functions.

template<typename _UniformRandomNumberGenerator > *result_type*
*operator()* (_UniformRandomNumberGenerator &__urng, const *param_type*
&__p)\\

*param_type* *param* () const\\
Returns the parameter set of the distribution.

void *param* (const *param_type* &__param)\\
Sets the parameter set of the distribution.

void *reset* ()\\
Resets the distribution state.

** Friends
template<typename _IntType1 , typename _CharT , typename _Traits >
*std::basic_ostream*< _CharT, _Traits > & *operator<<*
(*std::basic_ostream*< _CharT, _Traits > &__os, const
*std::poisson_distribution*< _IntType1 > &__x)\\
Inserts a poisson_distribution random number distribution =__x= into the
output stream =__os=.

bool *operator==* (const *poisson_distribution* &__d1, const
*poisson_distribution* &__d2)\\
Return true if two Poisson distributions have the same parameters and
the sequences that would be generated are equal.

template<typename _IntType1 , typename _CharT , typename _Traits >
*std::basic_istream*< _CharT, _Traits > & *operator>>*
(*std::basic_istream*< _CharT, _Traits > &__is,
*std::poisson_distribution*< _IntType1 > &__x)\\
Extracts a poisson_distribution random number distribution =__x= from
the input stream =__is=.

* Detailed Description
** "template<typename _IntType = int>
\\
class std::poisson_distribution< _IntType >"A discrete Poisson random
number distribution.

The formula for the Poisson probability density function is $p(i|) =
ac{^i}{i!} e^{-}$ where $$ is the parameter of the distribution.

Definition at line *4421* of file *random.h*.

* Member Typedef Documentation
** template<typename _IntType = int> typedef _IntType
*std::poisson_distribution*< _IntType >::*result_type*
The type of the range of the distribution.

Definition at line *4428* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _IntType = int> *std::poisson_distribution*<
_IntType >::*poisson_distribution* ()= [inline]=
Definition at line *4473* of file *random.h*.

** template<typename _IntType = int> *std::poisson_distribution*<
_IntType >::*poisson_distribution* (double __mean)= [inline]=,
= [explicit]=
Definition at line *4476* of file *random.h*.

** template<typename _IntType = int> *std::poisson_distribution*<
_IntType >::*poisson_distribution* (const *param_type* &
__p)= [inline]=, = [explicit]=
Definition at line *4481* of file *random.h*.

* Member Function Documentation
** template<typename _IntType = int> template<typename _ForwardIterator
, typename _UniformRandomNumberGenerator > void
*std::poisson_distribution*< _IntType >::__generate (_ForwardIterator
__f, _ForwardIterator __t, _UniformRandomNumberGenerator &
__urng)= [inline]=
Definition at line *4544* of file *random.h*.

** template<typename _IntType = int> template<typename _ForwardIterator
, typename _UniformRandomNumberGenerator > void
*std::poisson_distribution*< _IntType >::__generate (_ForwardIterator
__f, _ForwardIterator __t, _UniformRandomNumberGenerator & __urng, const
*param_type* & __p)= [inline]=
Definition at line *4551* of file *random.h*.

** template<typename _IntType = int> template<typename
_UniformRandomNumberGenerator > void *std::poisson_distribution*<
_IntType >::__generate (*result_type* * __f, *result_type* * __t,
_UniformRandomNumberGenerator & __urng, const *param_type* &
__p)= [inline]=
Definition at line *4558* of file *random.h*.

** template<typename _IntType = int> *result_type*
*std::poisson_distribution*< _IntType >::max () const= [inline]=
Returns the least upper bound value of the distribution.

Definition at line *4525* of file *random.h*.

References *std::numeric_limits< _Tp >::max()*.

** template<typename _IntType = int> double *std::poisson_distribution*<
_IntType >::mean () const= [inline]=
Returns the distribution parameter =mean=.

Definition at line *4496* of file *random.h*.

** template<typename _IntType = int> *result_type*
*std::poisson_distribution*< _IntType >::min () const= [inline]=
Returns the greatest lower bound value of the distribution.

Definition at line *4518* of file *random.h*.

** template<typename _IntType = int> template<typename
_UniformRandomNumberGenerator > *result_type*
*std::poisson_distribution*< _IntType >::operator()
(_UniformRandomNumberGenerator & __urng)= [inline]=
Generating functions.

Definition at line *4533* of file *random.h*.

References *std::poisson_distribution< _IntType >::operator()()*.

Referenced by *std::poisson_distribution< _IntType >::operator()()*.

** template<typename _IntType > template<typename
_UniformRandomNumberGenerator > *poisson_distribution*< _IntType
>::*result_type* *std::poisson_distribution*< _IntType >::operator()
(_UniformRandomNumberGenerator & __urng, const *param_type* & __param)
A rejection algorithm when mean >= 12 and a simple method based upon the
multiplication of uniform random variates otherwise. NB: The former is
available only if _GLIBCXX_USE_C99_MATH_TR1 is defined.

Reference: Devroye, L. Non-Uniform Random Variates Generation.
Springer-Verlag, New York, 1986, Ch. X, Sects. 3.3 & 3.4 (+ Errata!).

Definition at line *1302* of file *bits/random.tcc*.

References *std::numeric_limits< _Tp >::epsilon()*, and
*std::numeric_limits< _Tp >::max()*.

** template<typename _IntType = int> *param_type*
*std::poisson_distribution*< _IntType >::param () const= [inline]=
Returns the parameter set of the distribution.

Definition at line *4503* of file *random.h*.

** template<typename _IntType = int> void *std::poisson_distribution*<
_IntType >::param (const *param_type* & __param)= [inline]=
Sets the parameter set of the distribution.

*Parameters*

#+begin_quote
  /__param/ The new parameter set of the distribution.
#+end_quote

Definition at line *4511* of file *random.h*.

** template<typename _IntType = int> void *std::poisson_distribution*<
_IntType >::reset ()= [inline]=
Resets the distribution state.

Definition at line *4489* of file *random.h*.

* Friends And Related Function Documentation
** template<typename _IntType = int> template<typename _IntType1 ,
typename _CharT , typename _Traits > *std::basic_ostream*< _CharT,
_Traits > & operator<< (*std::basic_ostream*< _CharT, _Traits > & __os,
const *std::poisson_distribution*< _IntType1 > & __x)= [friend]=
Inserts a poisson_distribution random number distribution =__x= into the
output stream =__os=.

*Parameters*

#+begin_quote
  /__os/ An output stream.\\
  /__x/ A poisson_distribution random number distribution.
#+end_quote

*Returns*

#+begin_quote
  The output stream with the state of =__x= inserted or in an error
  state.
#+end_quote

** template<typename _IntType = int> bool operator== (const
*poisson_distribution*< _IntType > & __d1, const *poisson_distribution*<
_IntType > & __d2)= [friend]=
Return true if two Poisson distributions have the same parameters and
the sequences that would be generated are equal.

Definition at line *4569* of file *random.h*.

** template<typename _IntType = int> template<typename _IntType1 ,
typename _CharT , typename _Traits > *std::basic_istream*< _CharT,
_Traits > & operator>> (*std::basic_istream*< _CharT, _Traits > & __is,
*std::poisson_distribution*< _IntType1 > & __x)= [friend]=
Extracts a poisson_distribution random number distribution =__x= from
the input stream =__is=.

*Parameters*

#+begin_quote
  /__is/ An input stream.\\
  /__x/ A poisson_distribution random number generator engine.
#+end_quote

*Returns*

#+begin_quote
  The input stream with =__x= extracted or in an error state.
#+end_quote

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
