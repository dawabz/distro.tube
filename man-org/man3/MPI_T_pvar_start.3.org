#+TITLE: Manpages - MPI_T_pvar_start.3
#+DESCRIPTION: Linux manpage for MPI_T_pvar_start.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_T_pvar_start*, *MPI_T_pvar_stop* - Start/stop a performance
variable

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_T_pvar_start(MPI_T_pvar_session session, MPI_T_pvar_handle handle)

  int MPI_T_pvar_stop(MPI_T_pvar_session session, MPI_T_pvar_handle handle)
#+end_example

* INPUT PARAMETERS
- session :: Performance experiment session.

- handle :: Performance variable handle.

* DESCRIPTION
MPI_T_pvar_start starts the performance variable with the handle
specified in /handle/. The special value MPI_T_PVAR_ALL_HANDLES can be
passed in /handle/ to start all non-continuous handles in the session
specified in /session/.

MPI_T_pvar_stop stops the performance variable with the handle specified
in /handle/. The special value MPI_T_PVAR_ALL_HANDLES can be passed in
/handle/ to stop all non-continuous handles in the session specified in
/session/.

Continuous performance variables can neither be started nor stopped.

* ERRORS
MPI_T_pvar_start() and MPI_T_pvar_stop() will fail if:

- [MPI_T_ERR_NOT_INITIALIZED] :: The MPI Tools interface not initialized

- [MPI_T_ERR_INVALID_SESSION] :: Session parameter is not a valid
  session

- [MPI_T_ERR_INVALID_HANDLE] :: Invalid handle or handle not associated
  with the session

- [MPI_T_ERR_PVAR_NO_STARTSTOP] :: The variable cannot be started or
  stopped

* SEE ALSO
#+begin_example
  MPI_T_pvar_get_info
#+end_example
