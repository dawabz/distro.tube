#+TITLE: Manpages - calloc.3
#+DESCRIPTION: Linux manpage for calloc.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about calloc.3 is found in manpage for: [[../man3/malloc.3][man3/malloc.3]]