#+TITLE: Manpages - XtWarning.3
#+DESCRIPTION: Linux manpage for XtWarning.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtWarning.3 is found in manpage for: [[../man3/XtError.3][man3/XtError.3]]