#+TITLE: Manpages - FcCharSetFirstPage.3
#+DESCRIPTION: Linux manpage for FcCharSetFirstPage.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcCharSetFirstPage - Start enumerating charset contents

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcChar32 FcCharSetFirstPage (const FcCharSet */a/*,
FcChar32[FC_CHARSET_MAP_SIZE] */map/*, FcChar32 **/next/*);*

* DESCRIPTION
Builds an array of bits in /map/ marking the first page of Unicode
coverage of /a/. /*next/ is set to contains the base code point for the
next page in /a/. Returns the base code point for the page, or
FC_CHARSET_DONE if /a/ contains no pages. As an example, if
*FcCharSetFirstPage* returns 0x300 and fills /map/ with

#+begin_example
  0xffffffff 0xffffffff 0x01000008 0x44300002 0xffffd7f0 0xfffffffb 0xffff7fff 0xffff0003
#+end_example

Then the page contains code points 0x300 through 0x33f (the first 64
code points on the page) because /map[0]/ and /map[1]/ both have all
their bits set. It also contains code points 0x343 (/0x300 + 32*2/ +
(4-1)) and 0x35e (/0x300 +/ 32*2 + (31-1)) because /map[2]/ has the 4th
and 31st bits set. The code points represented by map[3] and later are
left as an exercise for the reader ;).
