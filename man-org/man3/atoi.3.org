#+TITLE: Manpages - atoi.3
#+DESCRIPTION: Linux manpage for atoi.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
atoi, atol, atoll - convert a string to an integer

* SYNOPSIS
#+begin_example
  #include <stdlib.h>

  int atoi(const char *nptr);
  long atol(const char *nptr);
  long long atoll(const char *nptr);
#+end_example

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*atoll*():

#+begin_example
      _ISOC99_SOURCE
          || /* Glibc <= 2.19: */ _BSD_SOURCE || _SVID_SOURCE
#+end_example

* DESCRIPTION
The *atoi*() function converts the initial portion of the string pointed
to by /nptr/ to /int/. The behavior is the same as

#+begin_example
  strtol(nptr, NULL, 10);
#+end_example

except that *atoi*() does not detect errors.

The *atol*() and *atoll*() functions behave the same as *atoi*(), except
that they convert the initial portion of the string to their return type
of /long/ or /long long/.

* RETURN VALUE
The converted value or 0 on error.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                     | Attribute     | Value          |
| *atoi*(), *atol*(), *atoll*() | Thread safety | MT-Safe locale |

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD. C89 and POSIX.1-1996
include the functions *atoi*() and *atol*() only.

* NOTES
POSIX.1 leaves the return value of *atoi*() on error unspecified. On
glibc, musl libc, and uClibc, 0 is returned on error.

* BUGS
/errno/ is not set on error so there is no way to distinguish between 0
as an error and as the converted value. No checks for overflow or
underflow are done. Only base-10 input can be converted. It is
recommended to instead use the *strtol*() and *strtoul*() family of
functions in new programs.

* SEE ALSO
*atof*(3), *strtod*(3), *strtol*(3), *strtoul*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
