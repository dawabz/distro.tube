#+TITLE: Manpages - std_greater.3
#+DESCRIPTION: Linux manpage for std_greater.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::greater< _Tp > - One of the *comparison functors*.

* SYNOPSIS
\\

=#include <stl_function.h>=

Inherits *std::binary_function< _Tp, _Tp, bool >*.

** Public Types
typedef _Tp *first_argument_type*\\
=first_argument_type= is the type of the first argument

typedef bool *result_type*\\
=result_type= is the return type

typedef _Tp *second_argument_type*\\
=second_argument_type= is the type of the second argument

** Public Member Functions
constexpr bool *operator()* (const _Tp &__x, const _Tp &__y) const\\

* Detailed Description
** "template<typename _Tp>
\\
struct std::greater< _Tp >"One of the *comparison functors*.

Definition at line *371* of file *stl_function.h*.

* Member Typedef Documentation
** typedef _Tp *std::binary_function*< _Tp , _Tp , bool
>::*first_argument_type*= [inherited]=
=first_argument_type= is the type of the first argument

Definition at line *121* of file *stl_function.h*.

** typedef bool *std::binary_function*< _Tp , _Tp , bool
>::*result_type*= [inherited]=
=result_type= is the return type

Definition at line *127* of file *stl_function.h*.

** typedef _Tp *std::binary_function*< _Tp , _Tp , bool
>::*second_argument_type*= [inherited]=
=second_argument_type= is the type of the second argument

Definition at line *124* of file *stl_function.h*.

* Member Function Documentation
** template<typename _Tp > constexpr bool *std::greater*< _Tp
>::operator() (const _Tp & __x, const _Tp & __y) const= [inline]=,
= [constexpr]=
Definition at line *375* of file *stl_function.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
