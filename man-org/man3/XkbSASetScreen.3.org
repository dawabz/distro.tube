#+TITLE: Manpages - XkbSASetScreen.3
#+DESCRIPTION: Linux manpage for XkbSASetScreen.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbSASetScreen - Sets the screenXXX field of act from s

* SYNOPSIS
*void XkbSASetScreen* *( XkbSwitchScreenAction */act/* ,* *int */s/* );*

* ARGUMENTS
- /- act/ :: action in which to set screenXXX

- /- s/ :: value to set in screenXXX

* DESCRIPTION
Actions associated with the XkbSwitchScreen action structure change the
active screen on a multiscreen display.

The /type/ field of the XkbSwitchScreenAction structure should always be
XkbSA_SwitchScreen.

The /flags/ field is composed of the bitwise inclusive OR of the masks
shown in Table 1.

TABLE

The /screenXXX/ field is a signed character value that represents either
the relative or absolute screen index, depending on the state of the
XkbSA_SwitchAbsolute bit in the /flags/ field. Xkb provides the
following macros to convert between the integer and signed character
value for screen numbers in XkbSwitchScreenAction structures.

* STRUCTURES
#+begin_example

      typedef struct _XkbSwitchScreenAction {
          unsigned char    type;        /* XkbSA_SwitchScreen */
          unsigned char    flags;       /* controls screen switching */
          char             screenXXX;   /* screen number or delta */
      } XkbSwitchScreenAction;
#+end_example

* NOTES
This action is optional. Servers are free to ignore the action or any of
its flags if they do not support the requested behavior. If the action
is ignored, it behaves like XkbSA_NoAction. Otherwise, key press and key
release events do not generate an event.
