#+TITLE: Manpages - curs_insch.3x
#+DESCRIPTION: Linux manpage for curs_insch.3x
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*insch*, *winsch*, *mvinsch*, *mvwinsch* - insert a character before
cursor in a *curses* window

* SYNOPSIS
*#include <curses.h>*

*int insch(chtype */ch/*);*\\
*int winsch(WINDOW **/win/*, chtype */ch/*);*

*int mvinsch(int */y/*, int */x/*, chtype */ch/*);*\\
*int mvwinsch(WINDOW **/win/*, int */y/*, int */x/*, chtype */ch/*);*\\

* DESCRIPTION
These routines insert the character /ch/ before the character under the
cursor. All characters to the right of the cursor are moved one space to
the right, with the possibility of the rightmost character on the line
being lost. The insertion operation does not change the cursor position.

* RETURN VALUE
All routines that return an integer return *ERR* upon failure and *OK*
(SVr4 specifies only "an integer value other than *ERR*") upon
successful completion, unless otherwise noted in the preceding routine
descriptions.

Functions with a mv prefix first perform a cursor movement using
*wmove*, and return an error if the position is outside the window, or
if the window pointer is null.

* NOTES
These routines do not necessarily imply use of a hardware insert
character feature.

Note that *insch*, *mvinsch*, and *mvwinsch* may be macros.

* PORTABILITY
These functions are described in the XSI Curses standard, Issue 4.

* SEE ALSO
*curses*(3X).

Comparable functions in the wide-character (ncursesw) library are
described in *curs_ins_wch*(3X).
