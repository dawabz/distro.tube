#+TITLE: Manpages - PerlIO_encoding.3perl
#+DESCRIPTION: Linux manpage for PerlIO_encoding.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
PerlIO::encoding - encoding layer

* SYNOPSIS
use PerlIO::encoding; open($f, "<:encoding(foo)", "infoo"); open($f,
">:encoding(bar)", "outbar"); use Encode qw(:fallbacks);
$PerlIO::encoding::fallback = FB_PERLQQ;

* DESCRIPTION
This PerlIO layer opens a filehandle with a transparent encoding filter.

On input, it converts the bytes expected to be in the specified
character set and encoding to Perl string data (Unicode and Perl's
internal Unicode encoding, UTF-8). On output, it converts Perl string
data into the specified character set and encoding.

When the layer is pushed, the current value of
=$PerlIO::encoding::fallback= is saved and used as the CHECK argument
when calling the Encode methods *encode()* and *decode()*.

* SEE ALSO
open, Encode, binmode in perlfunc, perluniintro
