#+TITLE: Manpages - XcmsRGB.3
#+DESCRIPTION: Linux manpage for XcmsRGB.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XcmsRGB.3 is found in manpage for: [[../man3/XcmsColor.3][man3/XcmsColor.3]]