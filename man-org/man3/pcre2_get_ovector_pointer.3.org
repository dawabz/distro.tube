#+TITLE: Manpages - pcre2_get_ovector_pointer.3
#+DESCRIPTION: Linux manpage for pcre2_get_ovector_pointer.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  PCRE2_SIZE *pcre2_get_ovector_pointer(pcre2_match_data *match_data);
#+end_example

* DESCRIPTION
This function returns a pointer to the vector of offsets that forms part
of the given match data block. The number of pairs can be found by
calling *pcre2_get_ovector_count()*.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
