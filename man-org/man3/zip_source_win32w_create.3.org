#+TITLE: Manpages - zip_source_win32w_create.3
#+DESCRIPTION: Linux manpage for zip_source_win32w_create.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The functions

and

create a zip source on Windows using a Windows Unicode name. They open

and read

bytes from offset

from it. If

is 0 or -1, the whole file (starting from

is used.

If the file supports seek, the source can be used to open a zip archive
from.

The file is opened and read when the data from the source is used,
usually by

or

Upon successful completion, the created source is returned. Otherwise,

is returned and the error code in

or

is set to indicate the error.

and

fail if:

or

are invalid.

Required memory could not be allocated.

Opening

failed.

was added in libzip 1.0.

and
