#+TITLE: Manpages - gnutls_anon_set_server_dh_params.3
#+DESCRIPTION: Linux manpage for gnutls_anon_set_server_dh_params.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_anon_set_server_dh_params - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*void gnutls_anon_set_server_dh_params(gnutls_anon_server_credentials_t
*/res/*, gnutls_dh_params_t */dh_params/*);*

* ARGUMENTS
- gnutls_anon_server_credentials_t res :: is a
  gnutls_anon_server_credentials_t type

- gnutls_dh_params_t dh_params :: The Diffie-Hellman parameters.

* DESCRIPTION
This function will set the Diffie-Hellman parameters for an anonymous
server to use. These parameters will be used in Anonymous Diffie-Hellman
cipher suites.

* DEPRECATED
This function is unnecessary and discouraged on GnuTLS 3.6.0 or later.
Since 3.6.0, DH parameters are negotiated following RFC7919.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
