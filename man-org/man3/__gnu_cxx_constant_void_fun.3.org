#+TITLE: Manpages - __gnu_cxx_constant_void_fun.3
#+DESCRIPTION: Linux manpage for __gnu_cxx_constant_void_fun.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::constant_void_fun< _Result > - An *SGI extension *.

* SYNOPSIS
\\

Inherits __gnu_cxx::_Constant_void_fun< _Result >.

** Public Types
typedef _Result *result_type*\\

** Public Member Functions
*constant_void_fun* (const _Result &__v)\\

const result_type & *operator()* () const\\

** Public Attributes
result_type *_M_val*\\

* Detailed Description
** "template<class _Result>
\\
struct __gnu_cxx::constant_void_fun< _Result >"An *SGI extension *.

Definition at line *295* of file *ext/functional*.

* Member Typedef Documentation
** template<class _Result > typedef _Result
__gnu_cxx::_Constant_void_fun< _Result >::result_type= [inherited]=
Definition at line *240* of file *ext/functional*.

* Constructor & Destructor Documentation
** template<class _Result > *__gnu_cxx::constant_void_fun*< _Result
>::*constant_void_fun* (const _Result & __v)= [inline]=
Definition at line *298* of file *ext/functional*.

* Member Function Documentation
** template<class _Result > const result_type &
__gnu_cxx::_Constant_void_fun< _Result >::operator() ()
const= [inline]=, = [inherited]=
Definition at line *246* of file *ext/functional*.

* Member Data Documentation
** template<class _Result > result_type __gnu_cxx::_Constant_void_fun<
_Result >::_M_val= [inherited]=
Definition at line *241* of file *ext/functional*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
