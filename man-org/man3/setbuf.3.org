#+TITLE: Manpages - setbuf.3
#+DESCRIPTION: Linux manpage for setbuf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
setbuf, setbuffer, setlinebuf, setvbuf - stream buffering operations

* SYNOPSIS
#+begin_example
  #include <stdio.h>

  int setvbuf(FILE *restrict stream, char *restrict buf,
   int mode, size_t size);

  void setbuf(FILE *restrict stream, char *restrict buf);
  void setbuffer(FILE *restrict stream, char *restrict buf,
   size_t size);
  void setlinebuf(FILE *stream);
#+end_example

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*setbuffer*(), *setlinebuf*():

#+begin_example
      Since glibc 2.19:
          _DEFAULT_SOURCE
      Glibc 2.19 and earlier:
          _BSD_SOURCE
#+end_example

* DESCRIPTION
The three types of buffering available are unbuffered, block buffered,
and line buffered. When an output stream is unbuffered, information
appears on the destination file or terminal as soon as written; when it
is block buffered, many characters are saved up and written as a block;
when it is line buffered, characters are saved up until a newline is
output or input is read from any stream attached to a terminal device
(typically /stdin/). The function *fflush*(3) may be used to force the
block out early. (See *fclose*(3).)

Normally all files are block buffered. If a stream refers to a terminal
(as /stdout/ normally does), it is line buffered. The standard error
stream /stderr/ is always unbuffered by default.

The *setvbuf*() function may be used on any open stream to change its
buffer. The /mode/ argument must be one of the following three macros:

#+begin_quote
  - *_IONBF* :: unbuffered

  - *_IOLBF* :: line buffered

  - *_IOFBF* :: fully buffered
#+end_quote

Except for unbuffered files, the /buf/ argument should point to a buffer
at least /size/ bytes long; this buffer will be used instead of the
current buffer. If the argument /buf/ is NULL, only the mode is
affected; a new buffer will be allocated on the next read or write
operation. The *setvbuf*() function may be used only after opening a
stream and before any other operations have been performed on it.

The other three calls are, in effect, simply aliases for calls to
*setvbuf*(). The *setbuf*() function is exactly equivalent to the call

setvbuf(stream, buf, buf ? _IOFBF : _IONBF, BUFSIZ);

The *setbuffer*() function is the same, except that the size of the
buffer is up to the caller, rather than being determined by the default
*BUFSIZ*. The *setlinebuf*() function is exactly equivalent to the call:

setvbuf(stream, NULL, _IOLBF, 0);

* RETURN VALUE
The function *setvbuf*() returns 0 on success. It returns nonzero on
failure (/mode/ is invalid or the request cannot be honored). It may set
/errno/ on failure.

The other functions do not return a value.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                                              | Attribute     | Value   |
| *setbuf*(), *setbuffer*(), *setlinebuf*(), *setvbuf*() | Thread safety | MT-Safe |

* CONFORMING TO
The *setbuf*() and *setvbuf*() functions conform to C89 and C99.

* NOTES
POSIX notes that the value of /errno/ is unspecified after a call to
*setbuf*() and further notes that, since the value of /errno/ is not
required to be unchanged after a successful call to *setbuf*(),
applications should instead use *setvbuf*() in order to detect errors.

* BUGS
You must make sure that the space that /buf/ points to still exists by
the time /stream/ is closed, which also happens at program termination.
For example, the following is invalid:

#+begin_example
  #include <stdio.h>

  int
  main(void)
  {
      char buf[BUFSIZ];
      setbuf(stdout, buf);
      printf("Hello, world!\n");
      return 0;
  }
#+end_example

* SEE ALSO
*stdbuf*(1), *fclose*(3), *fflush*(3), *fopen*(3), *fread*(3),
*malloc*(3), *printf*(3), *puts*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
