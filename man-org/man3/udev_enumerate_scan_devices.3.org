#+TITLE: Manpages - udev_enumerate_scan_devices.3
#+DESCRIPTION: Linux manpage for udev_enumerate_scan_devices.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
udev_enumerate_scan_devices, udev_enumerate_scan_subsystems,
udev_enumerate_get_list_entry, udev_enumerate_add_syspath,
udev_enumerate_get_udev - Query or modify a udev enumerate object

* SYNOPSIS
#+begin_example
  #include <libudev.h>
#+end_example

*int udev_enumerate_scan_devices(struct udev_enumerate
**/udev_enumerate/*);*

*int udev_enumerate_scan_subsystems(struct udev_enumerate
**/udev_enumerate/*);*

*struct udev_list_entry *udev_enumerate_get_list_entry(struct
udev_enumerate **/udev_enumerate/*);*

*int udev_enumerate_add_syspath(struct udev_enumerate
**/udev_enumerate/*, const char **/syspath/*);*

*struct udev *udev_enumerate_get_udev(struct udev_enumerate
**/udev_enumerate/*);*

* RETURN VALUE
On success, *udev_enumerate_scan_devices()*,
*udev_enumerate_scan_subsystems()* and *udev_enumerate_add_syspath()*
return an integer greater than, or equal to, *0*.

On success, *udev_enumerate_get_list_entry()* returns a pointer to the
first entry in the list of found devices. If the list is empty, or on
failure, *NULL* is returned.

*udev_enumerate_get_udev()* always returns a pointer to the udev context
that this enumerated object is associated with.

* SEE ALSO
*udev_new*(3), *udev_device_new_from_syspath*(3),
*udev_enumerate_new*(3), *udev_enumerate_add_match_subsystem*(3),
*udev_monitor_new_from_netlink*(3), *udev_list_entry*(3), *systemd*(1),
