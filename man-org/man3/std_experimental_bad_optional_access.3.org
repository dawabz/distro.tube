#+TITLE: Manpages - std_experimental_bad_optional_access.3
#+DESCRIPTION: Linux manpage for std_experimental_bad_optional_access.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::experimental::bad_optional_access - Exception class thrown when a
disengaged optional object is dereferenced.

* SYNOPSIS
\\

Inherits *std::logic_error*.

** Public Member Functions
*bad_optional_access* (const char *__arg)\\

virtual const char * *what* () const noexcept\\

* Detailed Description
Exception class thrown when a disengaged optional object is
dereferenced.

Definition at line *104* of file *experimental/optional*.

* Constructor & Destructor Documentation
** std::experimental::bad_optional_access::bad_optional_access
()= [inline]=
Definition at line *107* of file *experimental/optional*.

** std::experimental::bad_optional_access::bad_optional_access (const
char * __arg)= [inline]=, = [explicit]=
Definition at line *110* of file *experimental/optional*.

* Member Function Documentation
** virtual const char * std::logic_error::what () const= [virtual]=,
= [noexcept]=, = [inherited]=
Returns a C-style character string describing the general cause of the
current error (the same string passed to the ctor).\\

Reimplemented from *std::exception*.

Reimplemented in *std::future_error*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
