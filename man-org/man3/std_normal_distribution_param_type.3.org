#+TITLE: Manpages - std_normal_distribution_param_type.3
#+DESCRIPTION: Linux manpage for std_normal_distribution_param_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::normal_distribution< _RealType >::param_type

* SYNOPSIS
\\

=#include <random.h>=

** Public Types
typedef *normal_distribution*< _RealType > *distribution_type*\\

** Public Member Functions
*param_type* (_RealType __mean, _RealType __stddev=_RealType(1))\\

_RealType *mean* () const\\

_RealType *stddev* () const\\

** Friends
bool *operator!=* (const *param_type* &__p1, const *param_type* &__p2)\\

bool *operator==* (const *param_type* &__p1, const *param_type* &__p2)\\

* Detailed Description
** "template<typename _RealType = double>
\\
struct std::normal_distribution< _RealType >::param_type"Parameter type.

Definition at line *1982* of file *random.h*.

* Member Typedef Documentation
** template<typename _RealType = double> typedef
*normal_distribution*<_RealType> *std::normal_distribution*< _RealType
>::*param_type::distribution_type*
Definition at line *1984* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _RealType = double> *std::normal_distribution*<
_RealType >::param_type::param_type ()= [inline]=
Definition at line *1986* of file *random.h*.

** template<typename _RealType = double> *std::normal_distribution*<
_RealType >::param_type::param_type (_RealType __mean, _RealType
__stddev = =_RealType(1)=)= [inline]=, = [explicit]=
Definition at line *1989* of file *random.h*.

* Member Function Documentation
** template<typename _RealType = double> _RealType
*std::normal_distribution*< _RealType >::param_type::mean ()
const= [inline]=
Definition at line *1996* of file *random.h*.

** template<typename _RealType = double> _RealType
*std::normal_distribution*< _RealType >::param_type::stddev ()
const= [inline]=
Definition at line *2000* of file *random.h*.

* Friends And Related Function Documentation
** template<typename _RealType = double> bool operator!= (const
*param_type* & __p1, const *param_type* & __p2)= [friend]=
Definition at line *2009* of file *random.h*.

** template<typename _RealType = double> bool operator== (const
*param_type* & __p1, const *param_type* & __p2)= [friend]=
Definition at line *2004* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
