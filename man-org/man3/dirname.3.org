#+TITLE: Manpages - dirname.3
#+DESCRIPTION: Linux manpage for dirname.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about dirname.3 is found in manpage for: [[../man3/basename.3][man3/basename.3]]