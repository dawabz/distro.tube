#+TITLE: Manpages - afSetVirtualByteOrder.3
#+DESCRIPTION: Linux manpage for afSetVirtualByteOrder.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
afSetVirtualByteOrder, afSetVirtualChannels, afSetVirtualPCMMapping,
afSetVirtualSampleFormat - set the virtual data format for a track in an
audio file

* SYNOPSIS
#+begin_example
  #include <audiofile.h>
#+end_example

#+begin_example
  int afSetVirtualByteOrder (AFfilehandle file, int track, int byteOrder);
#+end_example

#+begin_example
  int afSetVirtualChannels (AFfilehandle file, int track, int channels);
#+end_example

#+begin_example
  int afSetVirtualSampleFormat (AFfilehandle file, int track,
      int sampleFormat, int sampleWidth);
#+end_example

#+begin_example
  int afSetVirtualPCMMapping (AFfilehandle file, int track,
      double slope, double intercept, double minclip, double maxclip);
#+end_example

* PARAMETERS

#+begin_quote
  ·

  /file/ is an AFfilehandle which refers to an open audio file and is
  usually created by afOpenFile.
#+end_quote

#+begin_quote
  ·

  /track/ is an integer which identifies a particular track in an open
  audio file. The only valid track is AF_DEFAULT_TRACK for all currently
  supported file formats.
#+end_quote

#+begin_quote
  ·

  /sampleFormat/ is an integer which denotes a virtual sample format.
  Valid values are AF_SAMPFMT_TWOSCOMP, AF_SAMPFMT_UNSIGNED,
  AF_SAMPFMT_FLOAT, and AF_SAMPFMT_DOUBLE.
#+end_quote

#+begin_quote
  ·

  /sampleWidth/ is a positive integer which specifies the number of bits
  in a sample.
#+end_quote

#+begin_quote
  ·

  /channels/ is a positive integer which specifies the number of
  interleaved audio channels in the given audio track.
#+end_quote

#+begin_quote
  ·

  /byteOrder/ is an integer which specifies the virtual byte order of
  samples in the given audio track. /byteOrder/ can be either
  AF_BYTEORDER_BIGENDIAN or AF_BYTEORDER_LITTLEENDIAN.
#+end_quote

#+begin_quote
  ·

  /slope/ and /intercept/ are double-precision floating point values
  which indicate the audio data sample slope and zero-crossing value,
  respectively, for the given sample format.
#+end_quote

#+begin_quote
  ·

  /minclip/ and /maxclip/ are double-precision floating point values
  which indicates the minimum or maximum sample values to be returned.
  Any values less than /minclip/ will be set to /minclip/, and any
  values greater than /maxclip/ will be set to /maxclip/.
#+end_quote

* RETURN VALUE
These functions return 0 for success and -1 for failure.

* AUTHOR
Michael Pruett <michael@68k.org>
