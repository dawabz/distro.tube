#+TITLE: Manpages - getopt_long_only.3
#+DESCRIPTION: Linux manpage for getopt_long_only.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about getopt_long_only.3 is found in manpage for: [[../man3/getopt.3][man3/getopt.3]]