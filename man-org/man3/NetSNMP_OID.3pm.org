#+TITLE: Manpages - NetSNMP_OID.3pm
#+DESCRIPTION: Linux manpage for NetSNMP_OID.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
NetSNMP::OID - Perl extension for manipulating OIDs

* SYNOPSIS
use NetSNMP::OID; my $oid = new NetSNMP::OID(sysContact.0); if ($oid <
new NetSNMP::OID(ifTable)) { do_something(); } my @numarray =
$oid->to_array(); # appending oids $oid = new NetSNMP::OID(.1.3); $oid
+= ".6.1"; # -> .1.3.6.1 # appending index strings $oid2 = $oid +
"\"wes\""; # -> .1.3.6.1.3.119.101.115 $oid3 = $oid + "\wes\"; # ->
.1.3.6.1.119.101.115 $len = $oid3->length(); # -> 7 # retrieving indexes
from an oid: $arrayref = $tableoid->get_indexes()

* DESCRIPTION
The NetSNMP::OID module is a simple wrapper around a C-based net-snmp
oid (which is an array of unsigned integers). The OID is internally
stored as a C array of integers for speed purposes when doing
comparisons, etc.

The standard logical expression operators (<, >, ==, ...) are overloaded
such that lexographical comparisons may be done with them.

The + operator is overloaded to allow you to append stuff on to the end
of a OID, like index segments of a table, for example.

** EXPORT
int snmp_oid_compare(oid1, oid2) int compare(oid1, oid2)

* AUTHOR
Wes Hardaker, <hardaker@users.sourceforge.net>

* SEE ALSO
SNMP, perl.

* Copyright
Copyright (c) 2002 Networks Associates Technology, Inc. All Rights
Reserved. This program is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.
