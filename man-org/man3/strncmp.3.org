#+TITLE: Manpages - strncmp.3
#+DESCRIPTION: Linux manpage for strncmp.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about strncmp.3 is found in manpage for: [[../man3/strcmp.3][man3/strcmp.3]]