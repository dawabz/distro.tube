#+TITLE: Manpages - exp2l.3
#+DESCRIPTION: Linux manpage for exp2l.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about exp2l.3 is found in manpage for: [[../man3/exp2.3][man3/exp2.3]]