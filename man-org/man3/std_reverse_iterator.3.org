#+TITLE: Manpages - std_reverse_iterator.3
#+DESCRIPTION: Linux manpage for std_reverse_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::reverse_iterator< _Iterator >

* SYNOPSIS
\\

=#include <stl_iterator.h>=

Inherits *std::iterator< iterator_traits< _Iterator
>::iterator_category, iterator_traits< _Iterator >::value_type,
iterator_traits< _Iterator >::difference_type, iterator_traits<
_Iterator >::pointer, iterator_traits< _Iterator >::reference >*.

** Public Types
typedef __traits_type::difference_type *difference_type*\\

typedef *iterator_traits*< _Iterator >::*iterator_category*
*iterator_category*\\
One of the *tag types*.

typedef _Iterator *iterator_type*\\

typedef __traits_type::pointer *pointer*\\

typedef __traits_type::reference *reference*\\

typedef *iterator_traits*< _Iterator >::*value_type* *value_type*\\
The type 'pointed to' by the iterator.

** Public Member Functions
constexpr *reverse_iterator* ()\\

constexpr *reverse_iterator* (const *reverse_iterator* &__x)\\

template<typename _Iter > constexpr *reverse_iterator* (const
*reverse_iterator*< _Iter > &__x)\\

constexpr *reverse_iterator* (iterator_type __x)\\

constexpr iterator_type *base* () const\\

constexpr reference *operator** () const\\

constexpr *reverse_iterator* *operator+* (difference_type __n) const\\

constexpr *reverse_iterator* & *operator++* ()\\

constexpr *reverse_iterator* *operator++* (int)\\

constexpr *reverse_iterator* & *operator+=* (difference_type __n)\\

constexpr *reverse_iterator* *operator-* (difference_type __n) const\\

constexpr *reverse_iterator* & *operator--* ()\\

constexpr *reverse_iterator* *operator--* (int)\\

constexpr *reverse_iterator* & *operator-=* (difference_type __n)\\

constexpr pointer *operator->* () const\\

*reverse_iterator* & *operator=* (const *reverse_iterator* &)=default\\

template<typename _Iter > constexpr *reverse_iterator* & *operator=*
(const *reverse_iterator*< _Iter > &__x)\\

constexpr reference *operator[]* (difference_type __n) const\\

** Protected Types
typedef *iterator_traits*< _Iterator > *__traits_type*\\

** Protected Attributes
_Iterator *current*\\

* Detailed Description
** "template<typename _Iterator>
\\
class std::reverse_iterator< _Iterator >"Bidirectional and random access
iterators have corresponding reverse iterator adaptors that iterate
through the data structure in the opposite direction. They have the same
signatures as the corresponding iterators. The fundamental relation
between a reverse iterator and its corresponding iterator =i= is
established by the identity:

#+begin_example
  &*(reverse_iterator(i)) == &*(i - 1)
#+end_example

/This mapping is dictated by the fact that while there is always a
pointer past the end of an array, there might not be a valid pointer
before the beginning of an array./ [24.4.1]/1,2

Reverse iterators can be tricky and surprising at first. Their semantics
make sense, however, and the trickiness is a side effect of the
requirement that the iterators must be safe.

Definition at line *127* of file *bits/stl_iterator.h*.

* Member Typedef Documentation
** template<typename _Iterator > typedef *iterator_traits*<_Iterator>
*std::reverse_iterator*< _Iterator >::*__traits_type*= [protected]=
Definition at line *148* of file *bits/stl_iterator.h*.

** template<typename _Iterator > typedef __traits_type::difference_type
*std::reverse_iterator*< _Iterator >::difference_type
Definition at line *152* of file *bits/stl_iterator.h*.

** typedef *iterator_traits*< _Iterator >::*iterator_category*
*std::iterator*< *iterator_traits*< _Iterator >::*iterator_category* ,
*iterator_traits*< _Iterator >::*value_type* , *iterator_traits*<
_Iterator >::difference_type , *iterator_traits*< _Iterator >::pointer ,
*iterator_traits*< _Iterator >::reference
>::*iterator_category*= [inherited]=
One of the *tag types*.

Definition at line *130* of file *stl_iterator_base_types.h*.

** template<typename _Iterator > typedef _Iterator
*std::reverse_iterator*< _Iterator >::iterator_type
Definition at line *151* of file *bits/stl_iterator.h*.

** template<typename _Iterator > typedef __traits_type::pointer
*std::reverse_iterator*< _Iterator >::pointer
Definition at line *153* of file *bits/stl_iterator.h*.

** template<typename _Iterator > typedef __traits_type::reference
*std::reverse_iterator*< _Iterator >::reference
Definition at line *154* of file *bits/stl_iterator.h*.

** typedef *iterator_traits*< _Iterator >::*value_type* *std::iterator*<
*iterator_traits*< _Iterator >::*iterator_category* , *iterator_traits*<
_Iterator >::*value_type* , *iterator_traits*< _Iterator
>::difference_type , *iterator_traits*< _Iterator >::pointer ,
*iterator_traits*< _Iterator >::reference >::*value_type*= [inherited]=
The type 'pointed to' by the iterator.

Definition at line *132* of file *stl_iterator_base_types.h*.

* Constructor & Destructor Documentation
** template<typename _Iterator > constexpr *std::reverse_iterator*<
_Iterator >::*reverse_iterator* ()= [inline]=, = [constexpr]=
The default constructor value-initializes member =current=. If it is a
pointer, that means it is zero-initialized.

Definition at line *174* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *std::reverse_iterator*<
_Iterator >::*reverse_iterator* (iterator_type __x)= [inline]=,
= [explicit]=, = [constexpr]=
This iterator will move in the opposite direction that =x= does.

Definition at line *180* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *std::reverse_iterator*<
_Iterator >::*reverse_iterator* (const *reverse_iterator*< _Iterator > &
__x)= [inline]=, = [constexpr]=
The copy constructor is normal.

Definition at line *186* of file *bits/stl_iterator.h*.

** template<typename _Iterator > template<typename _Iter > constexpr
*std::reverse_iterator*< _Iterator >::*reverse_iterator* (const
*reverse_iterator*< _Iter > & __x)= [inline]=, = [constexpr]=
A reverse_iterator across other types can be copied if the underlying
iterator can be converted to the type of =current=.

Definition at line *202* of file *bits/stl_iterator.h*.

* Member Function Documentation
** template<typename _Iterator > constexpr iterator_type
*std::reverse_iterator*< _Iterator >::base () const= [inline]=,
= [constexpr]=
*Returns*

#+begin_quote
  =current=, the iterator used for underlying work.
#+end_quote

Definition at line *224* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr reference
*std::reverse_iterator*< _Iterator >::operator* () const= [inline]=,
= [constexpr]=
*Returns*

#+begin_quote
  A reference to the value at =--current=
#+end_quote

This requires that =--current= is dereferenceable.

*Warning*

#+begin_quote
  This implementation requires that for an iterator of the underlying
  iterator type, =x=, a reference obtained by =*x= remains valid after
  =x= has been modified or destroyed. This is a bug:
  http://gcc.gnu.org/PR51823
#+end_quote

Definition at line *238* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *reverse_iterator*
*std::reverse_iterator*< _Iterator >::operator+ (difference_type __n)
const= [inline]=, = [constexpr]=
*Returns*

#+begin_quote
  A reverse_iterator that refers to =current= - /__n/
#+end_quote

The underlying iterator must be a Random Access Iterator.

Definition at line *319* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *reverse_iterator* &
*std::reverse_iterator*< _Iterator >::operator++ ()= [inline]=,
= [constexpr]=
*Returns*

#+begin_quote
  =*this=
#+end_quote

Decrements the underlying iterator.

Definition at line *269* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *reverse_iterator*
*std::reverse_iterator*< _Iterator >::operator++ (int)= [inline]=,
= [constexpr]=
*Returns*

#+begin_quote
  The original value of =*this=
#+end_quote

Decrements the underlying iterator.

Definition at line *281* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *reverse_iterator* &
*std::reverse_iterator*< _Iterator >::operator+= (difference_type
__n)= [inline]=, = [constexpr]=
*Returns*

#+begin_quote
  *this
#+end_quote

Moves the underlying iterator backwards /__n/ steps. The underlying
iterator must be a Random Access Iterator.

Definition at line *329* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *reverse_iterator*
*std::reverse_iterator*< _Iterator >::operator- (difference_type __n)
const= [inline]=, = [constexpr]=
*Returns*

#+begin_quote
  A reverse_iterator that refers to =current= - /__n/
#+end_quote

The underlying iterator must be a Random Access Iterator.

Definition at line *341* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *reverse_iterator* &
*std::reverse_iterator*< _Iterator >::operator-- ()= [inline]=,
= [constexpr]=
*Returns*

#+begin_quote
  =*this=
#+end_quote

Increments the underlying iterator.

Definition at line *294* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *reverse_iterator*
*std::reverse_iterator*< _Iterator >::operator-- (int)= [inline]=,
= [constexpr]=
*Returns*

#+begin_quote
  A reverse_iterator with the previous value of =*this=
#+end_quote

Increments the underlying iterator.

Definition at line *306* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *reverse_iterator* &
*std::reverse_iterator*< _Iterator >::operator-= (difference_type
__n)= [inline]=, = [constexpr]=
*Returns*

#+begin_quote
  *this
#+end_quote

Moves the underlying iterator forwards /__n/ steps. The underlying
iterator must be a Random Access Iterator.

Definition at line *351* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr pointer
*std::reverse_iterator*< _Iterator >::operator-> () const= [inline]=,
= [constexpr]=
*Returns*

#+begin_quote
  A pointer to the value at =--current=
#+end_quote

This requires that =--current= is dereferenceable.

Definition at line *250* of file *bits/stl_iterator.h*.

** template<typename _Iterator > template<typename _Iter > constexpr
*reverse_iterator* & *std::reverse_iterator*< _Iterator >::operator=
(const *reverse_iterator*< _Iter > & __x)= [inline]=, = [constexpr]=
Definition at line *213* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr reference
*std::reverse_iterator*< _Iterator >::operator[] (difference_type __n)
const= [inline]=, = [constexpr]=
*Returns*

#+begin_quote
  The value at =current= - /__n/ - 1
#+end_quote

The underlying iterator must be a Random Access Iterator.

Definition at line *363* of file *bits/stl_iterator.h*.

* Friends And Related Function Documentation
** template<typename _Iterator > template<typename _Iter > friend class
*reverse_iterator*= [friend]=
Definition at line *135* of file *bits/stl_iterator.h*.

* Member Data Documentation
** template<typename _Iterator > _Iterator *std::reverse_iterator*<
_Iterator >::current= [protected]=
Definition at line *146* of file *bits/stl_iterator.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
