#+TITLE: Manpages - FcDirCacheDeleteUUID.3
#+DESCRIPTION: Linux manpage for FcDirCacheDeleteUUID.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcDirCacheDeleteUUID - Delete .uuid file

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcDirCacheDeleteUUID (const FcChar8 */dir/*, FcConfig
**/config/*);*

* DESCRIPTION
This is to delete .uuid file containing an UUID at a font directory of
/dir/.

* SINCE
version 2.13.1
