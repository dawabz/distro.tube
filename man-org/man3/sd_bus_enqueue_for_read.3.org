#+TITLE: Manpages - sd_bus_enqueue_for_read.3
#+DESCRIPTION: Linux manpage for sd_bus_enqueue_for_read.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_bus_enqueue_for_read - Re-enqueue a bus message on a bus connection,
for reading

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

*int sd_bus_enqueue_for_read(sd_bus **/bus/*, sd_bus_message
**/message/*);*

* DESCRIPTION
*sd_bus_enqueue_for_read()* may be used to re-enqueue an incoming bus
message on the local read queue, so that it is processed and dispatched
locally again, similar to how an incoming message from the peer is
processed. Takes a bus connection object and the message to enqueue. A
reference is taken of the message and the callers reference thus remains
in possession of the caller. The message is enqueued at the end of the
queue, thus will be dispatched after all other already queued messages
are dispatched.

This call is primarily useful for dealing with incoming method calls
that may be processed only after an additional asynchronous operation
completes. One example are PolicyKit authorization requests that are
determined to be necessary to authorize a newly incoming method call:
when the PolicyKit response is received the original method call may be
re-enqueued to process it again, this time with the authorization result
known.

* RETURN VALUE
On success, this function return 0 or a positive integer. On failure, it
returns a negative errno-style error code.

** Errors
Returned errors may indicate the following problems:

*-ECHILD*

#+begin_quote
  The bus connection has been created in a different process.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-bus*(3), *sd_bus_send*(3),
