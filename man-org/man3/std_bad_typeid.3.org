#+TITLE: Manpages - std_bad_typeid.3
#+DESCRIPTION: Linux manpage for std_bad_typeid.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::bad_typeid - Thrown when a NULL pointer in a =typeid= expression is
used.

* SYNOPSIS
\\

Inherits *std::exception*.

** Public Member Functions
virtual const char * *what* () const noexcept\\

* Detailed Description
Thrown when a NULL pointer in a =typeid= expression is used.

Definition at line *207* of file *typeinfo*.

* Constructor & Destructor Documentation
** std::bad_typeid::bad_typeid ()= [inline]=, = [noexcept]=
Definition at line *210* of file *typeinfo*.

* Member Function Documentation
** virtual const char * std::bad_typeid::what () const= [virtual]=,
= [noexcept]=
Returns a C-style character string describing the general cause of the
current error.\\

Reimplemented from *std::exception*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
