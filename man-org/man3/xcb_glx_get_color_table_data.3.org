#+TITLE: Manpages - xcb_glx_get_color_table_data.3
#+DESCRIPTION: Linux manpage for xcb_glx_get_color_table_data.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_glx_get_color_table_data.3 is found in manpage for: [[../man3/xcb_glx_get_color_table.3][man3/xcb_glx_get_color_table.3]]