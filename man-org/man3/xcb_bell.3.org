#+TITLE: Manpages - xcb_bell.3
#+DESCRIPTION: Linux manpage for xcb_bell.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_bell -

* SYNOPSIS
*#include <xcb/xproto.h>*

** Request function
xcb_void_cookie_t *xcb_bell*(xcb_connection_t */conn/, int8_t
/percent/);\\

* REQUEST ARGUMENTS
- conn :: The XCB connection to X11.

- percent :: TODO: NOT YET DOCUMENTED.

* DESCRIPTION
* RETURN VALUE
Returns an /xcb_void_cookie_t/. Errors (if any) have to be handled in
the event loop.

If you want to handle errors directly with /xcb_request_check/ instead,
use /xcb_bell_checked/. See *xcb-requests(3)* for details.

* ERRORS
This request does never generate any errors.

* SEE ALSO
* AUTHOR
Generated from xproto.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
