#+TITLE: Manpages - XcmsQueryGreen.3
#+DESCRIPTION: Linux manpage for XcmsQueryGreen.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XcmsQueryGreen.3 is found in manpage for: [[../man3/XcmsQueryBlack.3][man3/XcmsQueryBlack.3]]