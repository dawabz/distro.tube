#+TITLE: Manpages - xcb_x_print_print_get_page_dimensions_reply.3
#+DESCRIPTION: Linux manpage for xcb_x_print_print_get_page_dimensions_reply.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_x_print_print_get_page_dimensions_reply.3 is found in manpage for: [[../man3/xcb_x_print_print_get_page_dimensions.3][man3/xcb_x_print_print_get_page_dimensions.3]]