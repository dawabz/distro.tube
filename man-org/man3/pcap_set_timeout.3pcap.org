#+TITLE: Manpages - pcap_set_timeout.3pcap
#+DESCRIPTION: Linux manpage for pcap_set_timeout.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_set_timeout - set the packet buffer timeout for a not-yet-activated
capture handle

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  int pcap_set_timeout(pcap_t *p, int to_ms);
#+end_example

* DESCRIPTION
*pcap_set_timeout*() sets the packet buffer timeout that will be used on
a capture handle when the handle is activated to /to_ms/, which is in
units of milliseconds. (See *pcap*(3PCAP) for an explanation of the
packet buffer timeout.)

The behavior, if the timeout isn't specified, is undefined, as is the
behavior if the timeout is set to zero or to a negative value. We
recommend always setting the timeout to a non-zero value unless
immediate mode is set, in which case the timeout has no effect.

* RETURN VALUE
*pcap_set_timeout*() returns *0* on success or *PCAP_ERROR_ACTIVATED* if
called on a capture handle that has been activated.

* SEE ALSO
*pcap_create*(3PCAP), *pcap_activate*(3PCAP),
*pcap_set_immediate_mode*(3PCAP)
