#+TITLE: Manpages - xcb_render_create_cursor.3
#+DESCRIPTION: Linux manpage for xcb_render_create_cursor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_render_create_cursor -

* SYNOPSIS
*#include <xcb/render.h>*

** Request function
xcb_void_cookie_t *xcb_render_create_cursor*(xcb_connection_t */conn/,
xcb_cursor_t /cid/, xcb_render_picture_t /source/, uint16_t /x/,
uint16_t /y/);\\

* REQUEST ARGUMENTS
- conn :: The XCB connection to X11.

- cid :: TODO: NOT YET DOCUMENTED.

- source :: TODO: NOT YET DOCUMENTED.

24. TODO: NOT YET DOCUMENTED.

25. TODO: NOT YET DOCUMENTED.

* DESCRIPTION
* RETURN VALUE
Returns an /xcb_void_cookie_t/. Errors (if any) have to be handled in
the event loop.

If you want to handle errors directly with /xcb_request_check/ instead,
use /xcb_render_create_cursor_checked/. See *xcb-requests(3)* for
details.

* ERRORS
This request does never generate any errors.

* SEE ALSO
* AUTHOR
Generated from render.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
