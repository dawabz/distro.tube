#+TITLE: Manpages - gnutls_pubkey_verify_params.3
#+DESCRIPTION: Linux manpage for gnutls_pubkey_verify_params.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_pubkey_verify_params - API function

* SYNOPSIS
*#include <gnutls/abstract.h>*

*int gnutls_pubkey_verify_params(gnutls_pubkey_t */key/*);*

* ARGUMENTS
- gnutls_pubkey_t key :: should contain a *gnutls_pubkey_t* type

* DESCRIPTION
This function will verify the public key parameters.

* RETURNS
On success, *GNUTLS_E_SUCCESS* (0) is returned, otherwise a negative
error value.

* SINCE
3.3.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
