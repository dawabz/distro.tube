#+TITLE: Manpages - SSL_CTX_sess_set_cache_size.3ssl
#+DESCRIPTION: Linux manpage for SSL_CTX_sess_set_cache_size.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_CTX_sess_set_cache_size, SSL_CTX_sess_get_cache_size - manipulate
session cache size

* SYNOPSIS
#include <openssl/ssl.h> long SSL_CTX_sess_set_cache_size(SSL_CTX *ctx,
long t); long SSL_CTX_sess_get_cache_size(SSL_CTX *ctx);

* DESCRIPTION
*SSL_CTX_sess_set_cache_size()* sets the size of the internal session
cache of context *ctx* to *t*. This value is a hint and not an absolute;
see the notes below.

*SSL_CTX_sess_get_cache_size()* returns the currently valid session
cache size.

* NOTES
The internal session cache size is SSL_SESSION_CACHE_MAX_SIZE_DEFAULT,
currently 1024*20, so that up to 20000 sessions can be held. This size
can be modified using the *SSL_CTX_sess_set_cache_size()* call. A
special case is the size 0, which is used for unlimited size.

If adding the session makes the cache exceed its size, then unused
sessions are dropped from the end of the cache. Cache space may also be
reclaimed by calling *SSL_CTX_flush_sessions* (3) to remove expired
sessions.

If the size of the session cache is reduced and more sessions are
already in the session cache, old session will be removed at the next
time a session shall be added. This removal is not synchronized with the
expiration of sessions.

* RETURN VALUES
*SSL_CTX_sess_set_cache_size()* returns the previously valid size.

*SSL_CTX_sess_get_cache_size()* returns the currently valid size.

* SEE ALSO
*ssl* (7), *SSL_CTX_set_session_cache_mode* (3),
*SSL_CTX_sess_number* (3), *SSL_CTX_flush_sessions* (3)

* COPYRIGHT
Copyright 2001-2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
