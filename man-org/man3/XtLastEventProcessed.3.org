#+TITLE: Manpages - XtLastEventProcessed.3
#+DESCRIPTION: Linux manpage for XtLastEventProcessed.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtLastEventProcessed, XtLastTimestampProcessed - last event, last
timestamp processed

* SYNTAX
#include <X11/Intrinsic.h>

XEvent* XtLastEventProcessed(Display* /display/);

Time XtLastTimestampProcessed(Display* /display/);

* ARGUMENTS
- display :: Specifies the open display connection.

* DESCRIPTION
*XtLastEventProcessed* returns the last event passed to
*XtDispatchEvent* for the specified display and NULL if there has been
no event. The client must not modify the contents of the returned event.

*XtLastTimestampProcessed* returns the timestamp of the last *KeyPress*,
*KeyRelease*, *ButtonPress*, *ButtonRelease*, *MotionNotify*,
*EnterNotify*, *LeaveNotify*, *PropertyNotify*, or *SelectionClear*
event that has been passed to *XtDispatchEvent* for the specified
display and zero if there has been no such event.

* SEE ALSO
\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
