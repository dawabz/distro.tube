#+TITLE: Manpages - ne_buffer_ncreate.3
#+DESCRIPTION: Linux manpage for ne_buffer_ncreate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ne_buffer_ncreate.3 is found in manpage for: [[../ne_buffer_create.3][ne_buffer_create.3]]