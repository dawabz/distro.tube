#+TITLE: Manpages - pam_get_data.3
#+DESCRIPTION: Linux manpage for pam_get_data.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_get_data - get module internal data

* SYNOPSIS
#+begin_example
  #include <security/pam_modules.h>
#+end_example

*int pam_get_data(const pam_handle_t **/pamh/*, const char
**/module_data_name/*, const void ***/data/*);*

* DESCRIPTION
This function together with the *pam_set_data*(3) function is useful to
manage module-specific data meaningful only to the calling PAM module.

The *pam_get_data* function looks up the object associated with the
(hopefully) unique string /module_data_name/ in the PAM context
specified by the /pamh/ argument. A successful call to *pam_get_data*
will result in /data/ pointing to the object. Note, this data is /not/ a
copy and should be treated as /constant/ by the module.

* RETURN VALUES
PAM_SUCCESS

#+begin_quote
  Data was successful retrieved.
#+end_quote

PAM_SYSTEM_ERR

#+begin_quote
  A NULL pointer was submitted as PAM handle or the function was called
  by an application.
#+end_quote

PAM_NO_MODULE_DATA

#+begin_quote
  Module data not found or there is an entry, but it has the value NULL.
#+end_quote

* SEE ALSO
*pam_end*(3), *pam_set_data*(3), *pam_strerror*(3)
