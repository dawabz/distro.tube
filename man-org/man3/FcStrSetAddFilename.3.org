#+TITLE: Manpages - FcStrSetAddFilename.3
#+DESCRIPTION: Linux manpage for FcStrSetAddFilename.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcStrSetAddFilename - add a filename to a string set

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcStrSetAddFilename (FcStrSet */set/*, const FcChar8 **/s/*);*

* DESCRIPTION
Adds a copy /s/ to /set/, The copy is created with FcStrCopyFilename so
that leading '~' values are replaced with the value of the HOME
environment variable.
