#+TITLE: Manpages - ldns_rdf2wire.3
#+DESCRIPTION: Linux manpage for ldns_rdf2wire.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_rr2wire, ldns_pkt2wire, ldns_rdf2wire - conversion functions

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_status ldns_rr2wire(uint8_t **dest, const ldns_rr *rr, int section,
size_t *size);

ldns_status ldns_pkt2wire(uint8_t **dest, const ldns_pkt *p, size_t
*size);

ldns_status ldns_rdf2wire(uint8_t **dest, const ldns_rdf *rdf, size_t
*size);

* DESCRIPTION
/ldns_rr2wire/() Allocates an array of uint8_t at dest, and puts the
wireformat of the given rr in that array. The result_size value contains
the length of the array, if it succeeds, and 0 otherwise (in which case
the function also returns NULL)

If the section argument is LDNS_SECTION_QUESTION, data like ttl and
rdata are not put into the result

.br *dest*: pointer to the array of bytes to be created .br *rr*: the rr
to convert .br *section*: the rr section, determines how the rr is
written. .br *size*: the size of the converted result

/ldns_pkt2wire/() Allocates an array of uint8_t at dest, and puts the
wireformat of the given packet in that array. The result_size value
contains the length of the array, if it succeeds, and 0 otherwise (in
which case the function also returns NULL)

/ldns_rdf2wire/() Allocates an array of uint8_t at dest, and puts the
wireformat of the given rdf in that array. The result_size value
contains the length of the array, if it succeeds, and 0 otherwise (in
which case the function also returns NULL)

.br *dest*: pointer to the array of bytes to be created .br *rdf*: the
rdata field to convert .br *size*: the size of the converted result

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_wire2rr/, /ldns_wire2pkt/, /ldns_wire2rdf/. And *perldoc
Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*, *RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
