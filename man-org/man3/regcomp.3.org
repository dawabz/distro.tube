#+TITLE: Manpages - regcomp.3
#+DESCRIPTION: Linux manpage for regcomp.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about regcomp.3 is found in manpage for: [[../man3/regex.3][man3/regex.3]]