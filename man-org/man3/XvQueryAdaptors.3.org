#+TITLE: Manpages - XvQueryAdaptors.3
#+DESCRIPTION: Linux manpage for XvQueryAdaptors.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XvQueryAdaptors - return adaptor information for a screen

* SYNOPSIS
*#include <X11/extensions/Xvlib.h>*

#+begin_example
  int XvQueryAdaptors(Display *dpy, Window window,
   unsigned int *p_num_adaptors,
   XvAdaptorInfo **pp_adaptor_info);
#+end_example

* ARGUMENTS
- dpy :: Specifies the connection to the X server.

- window :: Specifies a window of the screen for which the adaptor
  information is desired.

- p_num_adaptors :: A pointer to where the number of adaptors for the
  specified screen is returned.

- pp_adaptor_info :: A pointer to where the list of returned adaptor
  information is returned.

* DESCRIPTION
*XvQueryAdaptors*(3) returns an video adaptor information for the screen
of the specified drawable. The XvAdaptorInfo structure has the following
organization:

#+begin_example

       typedef struct {
         XvPortID base_id;
         unsigned long num_ports;
         char type;
         char *name;
         unsigned long num_formats;
         XvFormat *formats;
         unsigned long num_adaptors;
       } XvAdaptorInfo;
#+end_example

- base_id :: The resource ID of the first adaptor port.

- num_ports :: The number of ports supported by the adaptor.

- type :: A bit mask with the value XvInputMask asserted if the adaptor
  supports video input, and value XvOutputMask asserted if the adaptor
  supports video output. In Xv protocol 2.2 and later, there are 3 new
  bits defined - XvVideoMask, XvStillMask and XvImageMask indicating
  that the adaptor is capable of video, still or image primitives
  respectively.

- name :: A vendor specific name that identifies the adaptor.

- num_formats :: The number of depth/visual id formats supported by the
  adaptor.

- formats :: A pointer to an array of XvFormat structures.

The XvFormat structure has the following organization:

#+begin_example

       typedef struct {
         char depth;
         unsigned long visual_id;
       } XvFormat;
#+end_example

- depth :: A drawable depth supported by the adaptor.

- visual_id :: A visual-id supported for the given depth by the adaptor.

* RETURN VALUES
- [Success] :: Returned if *XvQueryAdaptors*(3) completed successfully.

- [XvBadExtension] :: Returned if the Xv extension is unavailable.

- [XvBadAlloc] :: Returned if *XvQueryAdaptors*(3) failed to allocate
  memory to process the request.

* DIAGNOSTICS
- [Drawable] :: Returned if the requested drawable does not exist.

* SEE ALSO
*XvFreeAdaptorInfo*(3)
