#+TITLE: Manpages - dispatch_io_read.3
#+DESCRIPTION: Linux manpage for dispatch_io_read.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The dispatch I/O framework is an API for asynchronous read and write I/O
operations. It is an application of the ideas and idioms present in the

framework to device I/O. Dispatch I/O enables an application to more
easily avoid blocking I/O operations and allows it to more directly
express its I/O requirements than by using the raw POSIX file API.
Dispatch I/O will make a best effort to optimize how and when
asynchronous I/O operations are performed based on the capabilities of
the targeted device.

This page provides details on how to read from and write to dispatch I/O
channels. Creation and configuration of these channels is covered in the

page. The dispatch I/O framework also provides the convenience functions

and

for uses that do not require the full functionality provided by I/O
channels.

The

and

functions are used to perform asynchronous read and write operations on
dispatch I/O channels. They can be thought of as asynchronous versions
of the

and

functions in the standard C library.

The

function schedules an I/O read operation on the specified dispatch I/O

As results from the read operation become available, the provided

block will be submitted to the specified

The block will be passed a dispatch data object representing the data
that has been read since the handler's previous invocation.

The

parameter indicates where the read operation should begin. For a channel
of

type it is interpreted relative to the position of the file pointer when
the channel was created, for a channel of

type it is ignored and the read operation will begin at the current file
pointer position.

The

parameter indicates the number of bytes that should be read from the I/O
channel. Pass

to keep reading until EOF is encountered (for a channel created from a
disk-based file this happens when reading past the end of the physical
file).

The

function schedules an I/O write operation on the specified dispatch I/O

As the write operation progresses, the provided

block will be submitted to the specified

The block will be passed a dispatch data object representing the data
that remains to be written as part of this I/O operation.

The

parameter indicates where the write operation should begin. It is
interpreted as for read operations above.

The

parameter specifies the location and amount of data to be written,
encapsulated as a dispatch data object. The object is retained by the
system until the write operation is complete.

Dispatch I/O handler blocks submitted to a channel via the

or

functions will be executed one or more times depending on system load
and the channel's configuration settings (see

for details). The handler block need not be reentrant safe, no new I/O
handler instance is submitted until the previously enqueued handler
block has returned.

The dispatch

object passed to an I/O handler block will be released by the system
when the block returns, if access to the memory buffer it represents is
needed outside of the handler, the handler block must retain the data
object or create a new (e.g. concatenated) data object from it (see

for details).

Once an I/O handler block is invoked with the

flag set, the associated I/O operation is complete and that handler
block will not be run again. If an unrecoverable error occurs while
performing the I/O operation, the handler block will be submitted with
the

flag set and the appropriate POSIX error code in the

parameter. An invocation of a handler block with the

flag set, zero

and

set to

indicates that the I/O operation has encountered EOF.
