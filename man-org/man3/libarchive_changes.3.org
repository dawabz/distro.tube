#+TITLE: Manpages - libarchive_changes.3
#+DESCRIPTION: Linux manpage for libarchive_changes.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
This page describes user-visible changes in libarchive3, and lists
public functions and other symbols changed, deprecated or removed in
libarchive3, along with their replacements if any.

Libarchive2 permitted a single (input or output) filter active on an
archive. Libarchive3 extends this into a variable-length stack. Where

would replace any existing filter,

extends the write pipeline with another filter.

Libarchive2 assumed that the local platform uses

as the native

encoding, which is true on

modern

and a few other systems, but is certainly not universal. As a result,
pax format archives were written incorrectly on some systems, since pax
format requires

and libarchive 2 incorrectly assumed that

strings can be easily converted to

Libarchive3 uses the standard iconv library to convert between character
sets and is introducing the notion of a

To support this,

objects can now be bound to a particular archive when they are created.
The automatic character set conversions performed by

objects when reading and writing filenames, usernames, and other strings
will now use an appropriate default character set:

If the

object is bound to an archive, it will use the default character set for
that archive.

The platform default character encoding (as returned by

will be used if nothing else is specified.

Libarchive3 also introduces charset options to many of the archive
readers and writers to control the character set that will be used for
filenames written in those archives. When possible, this will be set
automatically based on information in the archive itself. Combining this
with the notion of a default character set for the archive should allow
you to configure libarchive to read archives from other platforms and
have the filenames and other information transparently converted to the
character encoding suitable for your application.

These changes break binary compatibility; libarchive3 has a new shared
library version to reflect these changes. The library now uses portable
wide types such as

instead of less-portable types such as

and

There are a few cases where these changes will affect your source code:

In some cases, libarchive's wider types will introduce the possibility
of truncation: for example, on a system with a 16-bit

be truncated to uid

which can cause serious security problems.

Typedef function pointer types will be incompatible. For example, if you
define custom skip callbacks, you may have to use code similar to the
following if you want to support building against libarchive2 and
libarchive3:

#if ARCHIVE_VERSION_NUMBER < 3000000 typedef off_t myoff_t; #else
typedef int64_t myoff_t; #endif

myoff_t my_skip_function(struct archive *a, void *v, myoff_t o) { ...
implementation ... }

Affected functions:

Where these functions or their arguments took or returned

or

they now take or return

or equivalent.

Symbols deprecated in libarchive3 will be removed in libarchive4. These
symbols, along with their replacements if any, are listed below:

These symbols, listed below along with their replacements if any, were
deprecated in libarchive2, and are not part of libarchive3.

or

or

or

or
