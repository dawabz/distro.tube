#+TITLE: Manpages - std_alignment_of.3
#+DESCRIPTION: Linux manpage for std_alignment_of.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::alignment_of< _Tp > - alignment_of

* SYNOPSIS
\\

Inherits *std::integral_constant< std::size_t, alignof(_Tp)>*.

** Public Types
typedef *integral_constant*< std::size_t, __v > *type*\\

typedef std::size_t *value_type*\\

** Public Member Functions
constexpr *operator value_type* () const noexcept\\

constexpr value_type *operator()* () const noexcept\\

** Static Public Attributes
static constexpr std::size_t *value*\\

* Detailed Description
** "template<typename _Tp>
\\
struct std::alignment_of< _Tp >"alignment_of

Definition at line *1310* of file *std/type_traits*.

* Member Typedef Documentation
** typedef *integral_constant*<std::size_t , __v>
*std::integral_constant*< std::size_t , __v >::*type*= [inherited]=
Definition at line *61* of file *std/type_traits*.

** typedef std::size_t *std::integral_constant*< std::size_t , __v
>::value_type= [inherited]=
Definition at line *60* of file *std/type_traits*.

* Member Function Documentation
** constexpr *std::integral_constant*< std::size_t , __v >::operator
value_type () const= [inline]=, = [constexpr]=, = [noexcept]=,
= [inherited]=
Definition at line *62* of file *std/type_traits*.

** constexpr value_type *std::integral_constant*< std::size_t , __v
>::operator() () const= [inline]=, = [constexpr]=, = [noexcept]=,
= [inherited]=
Definition at line *67* of file *std/type_traits*.

* Member Data Documentation
** constexpr std::size_t *std::integral_constant*< std::size_t , __v
>::value= [static]=, = [constexpr]=, = [inherited]=
Definition at line *59* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
