#+TITLE: Manpages - std_is_reference.3
#+DESCRIPTION: Linux manpage for std_is_reference.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::is_reference< _Tp > - is_reference

* SYNOPSIS
\\

Inherits __or_::type.

* Detailed Description
** "template<typename _Tp>
\\
struct std::is_reference< _Tp >"is_reference

Definition at line *530* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
