#+TITLE: Manpages - gnutls_pkcs11_privkey_init.3
#+DESCRIPTION: Linux manpage for gnutls_pkcs11_privkey_init.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_pkcs11_privkey_init - API function

* SYNOPSIS
*#include <gnutls/pkcs11.h>*

*int gnutls_pkcs11_privkey_init(gnutls_pkcs11_privkey_t * */key/*);*

* ARGUMENTS
- gnutls_pkcs11_privkey_t * key :: A pointer to the type to be
  initialized

* DESCRIPTION
This function will initialize an private key structure. This structure
can be used for accessing an underlying PKCS*11* object.

In versions of GnuTLS later than 3.5.11 the object is protected using
locks and a single *gnutls_pkcs11_privkey_t* can be re-used by many
threads. However, for performance it is recommended to utilize one
object per key per thread.

* RETURNS
On success, *GNUTLS_E_SUCCESS* (0) is returned, otherwise a negative
error value.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
