#+TITLE: Manpages - mitem_visible.3x
#+DESCRIPTION: Linux manpage for mitem_visible.3x
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*mitem_visible* - check visibility of a menu item

* SYNOPSIS
*#include <menu.h>*

*bool item_visible(const ITEM **/item/*);*\\

* DESCRIPTION
A menu item is visible when it is in the portion of a posted menu that
is mapped onto the screen (if the menu is scrollable, in particular,
this portion will be smaller than the whole menu).

* SEE ALSO
*curses*(3X), *menu*(3X).

* NOTES
The header file *<menu.h>* automatically includes the header file
*<curses.h>*.

* PORTABILITY
These routines emulate the System V menu library. They were not
supported on Version 7 or BSD versions.

* AUTHORS
Juergen Pfeifer. Manual pages and adaptation for new curses by Eric S.
Raymond.
