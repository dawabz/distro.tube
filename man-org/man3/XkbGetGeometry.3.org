#+TITLE: Manpages - XkbGetGeometry.3
#+DESCRIPTION: Linux manpage for XkbGetGeometry.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbGetGeometry - Loads a keyboard geometry if you already have the
keyboard description

* SYNOPSIS
*Status XkbGetGeometry* *( Display **/dpy/* ,* *XkbDescPtr */xkb/* );*

* ARGUMENTS
- /- dpy/ :: connection to the X server

- /- xkb/ :: keyboard description that contains the ID for the keyboard
  and into which the geometry should be loaded

* DESCRIPTION
You can load a keyboard geometry as part of the keyboard description
returned by /XkbGetKeyboard./ However, if a keyboard description has
been previously loaded, you can instead obtain the geometry by calling
the /XkbGetGeometry./ In this case, the geometry returned is the one
associated with the keyboard whose device ID is contained in the
keyboard description.

/XkbGetGeometry/ can return BadValue, BadImplementation, BadName,
BadAlloc, or BadLength errors or Success if it succeeds.

* DIAGNOSTICS
- *BadAlloc* :: Unable to allocate storage

- *BadImplementation* :: Invalid reply from server

- *BadLength* :: The length of a request is shorter or longer than that
  required to minimally contain the arguments

- *BadName* :: A font or color of the specified name does not exist

- *BadValue* :: An argument is out of range

* SEE ALSO
*XkbGetKeyboard*(3)
