#+TITLE: Manpages - FcPatternReference.3
#+DESCRIPTION: Linux manpage for FcPatternReference.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcPatternReference - Increment pattern reference count

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcPatternReference (FcPattern */p/*);*

* DESCRIPTION
Add another reference to /p/. Patterns are freed only when the reference
count reaches zero.
