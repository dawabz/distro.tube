#+TITLE: Manpages - CURLOPT_PROXY_SSLKEYTYPE.3
#+DESCRIPTION: Linux manpage for CURLOPT_PROXY_SSLKEYTYPE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_PROXY_SSLKEYTYPE - type of the proxy private key file

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_SSLKEYTYPE, char
*type);

* DESCRIPTION
This option is for connecting to an HTTPS proxy, not an HTTPS server.

Pass a pointer to a null-terminated string as parameter. The string
should be the format of your private key. Supported formats are "PEM",
"DER" and "ENG".

The application does not have to keep the string around after setting
this option.

* PROTOCOLS
Used with HTTPS proxy

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    curl_easy_setopt(curl, CURLOPT_PROXY, "https://proxy");
    curl_easy_setopt(curl, CURLOPT_PROXY_SSLCERT, "client.pem");
    curl_easy_setopt(curl, CURLOPT_PROXY_SSLKEY, "key.pem");
    curl_easy_setopt(curl, CURLOPT_PROXY_SSLKEYTYPE, "PEM");
    curl_easy_setopt(curl, CURLOPT_PROXY_KEYPASSWD, "s3cret");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.52.0

* RETURN VALUE
Returns CURLE_OK if TLS is supported, CURLE_UNKNOWN_OPTION if not, or
CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_PROXY_SSLKEY*(3), *CURLOPT_PROXY_SSLCERT*(3),
*CURLOPT_SSLKEYTYPE*(3),
