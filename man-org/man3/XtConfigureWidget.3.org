#+TITLE: Manpages - XtConfigureWidget.3
#+DESCRIPTION: Linux manpage for XtConfigureWidget.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtConfigureWidget, XtMoveWidget, XtResizeWidget - move and resize
widgets

* SYNTAX
#include <X11/Intrinsic.h>

void XtConfigureWidget(Widget /w/, Position /x/, Position /y/, Dimension
/width/, Dimension /height/, Dimension /border_width/);

void XtMoveWidget(Widget /w/, Position /x/, Position /y/);

void XtResizeWidget(Widget /w/, Dimension /width/, Dimension /height/,
Dimension /border_width/);

void XtResizeWindow(Widget /w/);

* ARGUMENTS
- width :: \\

- height :: \\

- border_width :: Specify the new widget size.

23. Specifies the widget.

24. \\

25. Specify the new widget x and y coordinates.

* DESCRIPTION
The *XtConfigureWidget* function returns immediately if the specified
geometry fields are the same as the old values. Otherwise,
*XtConfigureWidget* writes the new x, y, width, height, and border_width
values into the widget and, if the widget is realized, makes an Xlib
*XConfigureWindow* call on the widget's window.

If either the new width or height is different from its old value,
*XtConfigureWidget* calls the widget's resize procedure to notify it of
the size change; otherwise, it simply returns.

The *XtMoveWidget* function returns immediately if the specified
geometry fields are the same as the old values. Otherwise,
*XtMoveWidget* writes the new x and y values into the widget and, if the
widget is realized, issues an Xlib *XMoveWindow* call on the widget's
window.

The *XtResizeWidget* function returns immediately if the specified
geometry fields are the same as the old values. Otherwise,
*XtResizeWidget* writes the new width, height, and border_width values
into the widget and, if the widget is realized, issues an
*XConfigureWindow* call on the widget's window.

If the new width or height are different from the old values,
*XtResizeWidget* calls the widget's resize procedure to notify it of the
size change.

The *XtResizeWindow* function calls the *XConfigureWindow* Xlib function
to make the window of the specified widget match its width, height, and
border width. This request is done unconditionally because there is no
way to tell if these values match the current values. Note that the
widget's resize procedure is not called.

There are very few times to use *XtResizeWindow*; instead, you should
use *XtResizeWidget*.

* SEE ALSO
XtMakeGeometryRequest(3), XtQueryGeometry(3)\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
