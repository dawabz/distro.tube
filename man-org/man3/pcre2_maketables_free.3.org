#+TITLE: Manpages - pcre2_maketables_free.3
#+DESCRIPTION: Linux manpage for pcre2_maketables_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  void pcre2_maketables_free(pcre2_general_context *gcontext,
   const uint8_t *tables);
#+end_example

* DESCRIPTION
This function discards a set of character tables that were created by a
call to *pcre2_maketables()*.

The /gcontext/ parameter should match what was used in that call to
account for any custom allocators that might be in use; if it is NULL
the system *free()* is used.

There is a complete description of the PCRE2 native API in the
*pcre2api* page.
