#+TITLE: Manpages - FcObjectSetAdd.3
#+DESCRIPTION: Linux manpage for FcObjectSetAdd.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcObjectSetAdd - Add to an object set

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcObjectSetAdd (FcObjectSet */os/*, const char **/object/*);*

* DESCRIPTION
Adds a property name to the set. Returns FcFalse if the property name
cannot be inserted into the set (due to allocation failure). Otherwise
returns FcTrue.
