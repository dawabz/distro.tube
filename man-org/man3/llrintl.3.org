#+TITLE: Manpages - llrintl.3
#+DESCRIPTION: Linux manpage for llrintl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about llrintl.3 is found in manpage for: [[../man3/lrint.3][man3/lrint.3]]