#+TITLE: Manpages - psx_syscall6.3
#+DESCRIPTION: Linux manpage for psx_syscall6.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about psx_syscall6.3 is found in manpage for: [[../man3/libpsx.3][man3/libpsx.3]]