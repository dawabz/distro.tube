#+TITLE: Manpages - rpc_secure.3t
#+DESCRIPTION: Linux manpage for rpc_secure.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
These routines are part of the

library. They implement

Authentication. See

for further details about

The

is the first of two routines which interface to the

secure authentication system, known as

authentication. The second is

below.

Note: the keyserver daemon

must be running for the

authentication system to work.

The

function, used on the client side, returns an authentication handle that
will enable the use of the secure authentication system. The first
argument

is the network name, or

of the owner of the server process. This field usually represents a

derived from the utility routine

but could also represent a user name using

The second field is window on the validity of the client credential,
given in seconds. A small window is more secure than a large one, but
choosing too small of a window will increase the frequency of
resynchronizations because of clock drift. The third argument

is optional. If it is

then the authentication system will assume that the local clock is
always in sync with the server's clock, and will not attempt
resynchronizations. If an address is supplied, however, then the system
will use the address for consulting the remote time service whenever
resynchronization is required. This argument is usually the address of
the

server itself. The final argument

is also optional. If it is

then the authentication system will generate a random

key to be used for the encryption of credentials. If it is supplied,
however, then it will be used instead.

The

function is identical to

except that the public key needs to be provided at calling time and will
not looked up by this function itself.

The

function, the second of the two

authentication routines, is used on the server side for converting a

credential, which is operating system independent, into a

credential. This routine differs from utility routine

in that

pulls its information from a cache, and does not have to do a Yellow
Pages lookup every time it is called to get its information.

The

function installs the unique, operating-system independent netname of
the caller in the fixed-length array

Returns

if it succeeds and

if it fails.

The

function converts from a domain-specific hostname to an operating-system
independent netname. Returns

if it succeeds and

if it fails. Inverse of

The

function is an interface to the keyserver daemon, which is associated
with

secure authentication system

authentication). User programs rarely need to call it, or its associated
routines

and

System commands such as

and the

library are the main clients of these four routines.

The

function takes a server netname and a

key, and decrypts the key by using the public key of the server and the
secret key associated with the effective uid of the calling process. It
is the inverse of

The

function is a keyserver interface routine. It takes a server netname and
a des key, and encrypts it using the public key of the server and the
secret key associated with the effective uid of the calling process. It
is the inverse of

The

function is a keyserver interface routine. It is used to ask the
keyserver for a secure conversation key. Choosing one

is usually not good enough, because the common ways of choosing random
numbers, such as using the current time, are very easy to guess.

The

function is a keyserver interface routine. It is used to set the key for
the effective

of the calling process.

The

function converts from an operating-system independent netname to a
domain-specific hostname. Returns

if it succeeds and

if it fails. Inverse of

The

function converts from an operating-system independent netname to a
domain-specific user ID. Returns

if it succeeds and

if it fails. Inverse of

The

function converts from a domain-specific username to an operating-system
independent netname. Returns

if it succeeds and

if it fails. Inverse of

These functions are part of libtirpc.

The following manuals:
