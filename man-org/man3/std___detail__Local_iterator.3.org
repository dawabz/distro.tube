#+TITLE: Manpages - std___detail__Local_iterator.3
#+DESCRIPTION: Linux manpage for std___detail__Local_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Local_iterator< _Key, _Value, _ExtractKey, _Hash,
_RangeHash, _Unused, __constant_iterators, __cache > - local iterators

* SYNOPSIS
\\

=#include <hashtable_policy.h>=

Inherits *std::__detail::_Local_iterator_base< _Key, _Value,
_ExtractKey, _Hash, _RangeHash, _Unused, __cache >*.

** Public Types
typedef std::ptrdiff_t *difference_type*\\

typedef *std::forward_iterator_tag* *iterator_category*\\

typedef *std::conditional*< __constant_iterators, constvalue_type *,
value_type * >::type *pointer*\\

typedef *std::conditional*< __constant_iterators, constvalue_type &,
value_type & >::type *reference*\\

typedef _Value *value_type*\\

** Public Member Functions
*_Local_iterator* (const __hash_code_base &__base, *_Hash_node*< _Value,
__cache > *__n, std::size_t __bkt, std::size_t __bkt_count)\\

reference *operator** () const\\

*_Local_iterator* & *operator++* ()\\

*_Local_iterator* *operator++* (int)\\

pointer *operator->* () const\\

* Detailed Description
** "template<typename _Key, typename _Value, typename _ExtractKey,
typename _Hash, typename _RangeHash, typename _Unused, bool
__constant_iterators, bool __cache>
\\
struct std::__detail::_Local_iterator< _Key, _Value, _ExtractKey, _Hash,
_RangeHash, _Unused, __constant_iterators, __cache >"local iterators

Definition at line *1435* of file *hashtable_policy.h*.

* Member Typedef Documentation
** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Hash , typename _RangeHash , typename _Unused , bool
__constant_iterators, bool __cache> typedef std::ptrdiff_t
*std::__detail::_Local_iterator*< _Key, _Value, _ExtractKey, _Hash,
_RangeHash, _Unused, __constant_iterators, __cache >::difference_type
Definition at line *1452* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Hash , typename _RangeHash , typename _Unused , bool
__constant_iterators, bool __cache> typedef *std::forward_iterator_tag*
*std::__detail::_Local_iterator*< _Key, _Value, _ExtractKey, _Hash,
_RangeHash, _Unused, __constant_iterators, __cache
>::*iterator_category*
Definition at line *1453* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Hash , typename _RangeHash , typename _Unused , bool
__constant_iterators, bool __cache> typedef
*std::conditional*<__constant_iterators,constvalue_type*,value_type*>::type
*std::__detail::_Local_iterator*< _Key, _Value, _ExtractKey, _Hash,
_RangeHash, _Unused, __constant_iterators, __cache >::pointer
Definition at line *1448* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Hash , typename _RangeHash , typename _Unused , bool
__constant_iterators, bool __cache> typedef
*std::conditional*<__constant_iterators,constvalue_type&,value_type&>::type
*std::__detail::_Local_iterator*< _Key, _Value, _ExtractKey, _Hash,
_RangeHash, _Unused, __constant_iterators, __cache >::reference
Definition at line *1451* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Hash , typename _RangeHash , typename _Unused , bool
__constant_iterators, bool __cache> typedef _Value
*std::__detail::_Local_iterator*< _Key, _Value, _ExtractKey, _Hash,
_RangeHash, _Unused, __constant_iterators, __cache >::value_type
Definition at line *1445* of file *hashtable_policy.h*.

* Constructor & Destructor Documentation
** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Hash , typename _RangeHash , typename _Unused , bool
__constant_iterators, bool __cache> *std::__detail::_Local_iterator*<
_Key, _Value, _ExtractKey, _Hash, _RangeHash, _Unused,
__constant_iterators, __cache >::*_Local_iterator* (const
__hash_code_base & __base, *_Hash_node*< _Value, __cache > * __n,
std::size_t __bkt, std::size_t __bkt_count)= [inline]=
Definition at line *1457* of file *hashtable_policy.h*.

* Member Function Documentation
** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Hash , typename _RangeHash , typename _Unused , bool
__constant_iterators, bool __cache> reference
*std::__detail::_Local_iterator*< _Key, _Value, _ExtractKey, _Hash,
_RangeHash, _Unused, __constant_iterators, __cache >::operator* ()
const= [inline]=
Definition at line *1464* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Hash , typename _RangeHash , typename _Unused , bool
__constant_iterators, bool __cache> *_Local_iterator* &
*std::__detail::_Local_iterator*< _Key, _Value, _ExtractKey, _Hash,
_RangeHash, _Unused, __constant_iterators, __cache >::operator++
()= [inline]=
Definition at line *1472* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Hash , typename _RangeHash , typename _Unused , bool
__constant_iterators, bool __cache> *_Local_iterator*
*std::__detail::_Local_iterator*< _Key, _Value, _ExtractKey, _Hash,
_RangeHash, _Unused, __constant_iterators, __cache >::operator++
(int)= [inline]=
Definition at line *1479* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Hash , typename _RangeHash , typename _Unused , bool
__constant_iterators, bool __cache> pointer
*std::__detail::_Local_iterator*< _Key, _Value, _ExtractKey, _Hash,
_RangeHash, _Unused, __constant_iterators, __cache >::operator-> ()
const= [inline]=
Definition at line *1468* of file *hashtable_policy.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
