#+TITLE: Manpages - std_bad_optional_access.3
#+DESCRIPTION: Linux manpage for std_bad_optional_access.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::bad_optional_access - Exception class thrown when a disengaged
optional object is dereferenced.

* SYNOPSIS
\\

Inherits *std::exception*.

** Public Member Functions
const char * *what* () const noexcept override\\

* Detailed Description
Exception class thrown when a disengaged optional object is
dereferenced.

Definition at line *84* of file *std/optional*.

* Member Function Documentation
** const char * std::bad_optional_access::what () const= [inline]=,
= [override]=, = [virtual]=, = [noexcept]=
Returns a C-style character string describing the general cause of the
current error.\\

Reimplemented from *std::exception*.

Definition at line *90* of file *std/optional*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
