#+TITLE: Manpages - TIFFSetDirectory.3tiff
#+DESCRIPTION: Linux manpage for TIFFSetDirectory.3tiff
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
TIFFSetDirectory, TIFFSetSubDirectory - set the current directory for an
open

file

* SYNOPSIS
*#include <tiffio.h>*

*int TIFFSetDirectory(TIFF **/tif/*, tdir_t */dirnum/*)*\\
*int TIFFSetSubDirectory(TIFF **/tif/*, uint64_t */diroff/*)*

* DESCRIPTION
/TIFFSetDirectory/ changes the current directory and reads its contents
with /TIFFReadDirectory/. The parameter /dirnum/ specifies the
subfile/directory as an integer number, with the first directory
numbered zero.

/TIFFSetSubDirectory/ acts like /TIFFSetDirectory/, except the directory
is specified as a file offset instead of an index; this is required for
accessing subdirectories linked through a /SubIFD/ tag.

* RETURN VALUES
On successful return 1 is returned. Otherwise, 0 is returned if /dirnum/
or /diroff/ specifies a non-existent directory, or if an error was
encountered while reading the directory's contents.

* DIAGNOSTICS
All error messages are directed to the /TIFFError/(3TIFF) routine.

*%s: Error fetching directory count*. An error was encountered while
reading the ``directory count'' field.

*%s: Error fetching directory link*. An error was encountered while
reading the ``link value'' that points to the next directory in a file.

* SEE ALSO
/TIFFCurrentDirectory/(3TIFF), /TIFFOpen/(3TIFF),
/TIFFReadDirectory/(3TIFF), /TIFFWriteDirectory/(3TIFF),
/libtiff/(3TIFF)

Libtiff library home page: *http://www.simplesystems.org/libtiff/*
