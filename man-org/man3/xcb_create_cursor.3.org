#+TITLE: Manpages - xcb_create_cursor.3
#+DESCRIPTION: Linux manpage for xcb_create_cursor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_create_cursor -

* SYNOPSIS
*#include <xcb/xproto.h>*

** Request function
xcb_void_cookie_t *xcb_create_cursor*(xcb_connection_t */conn/,
xcb_cursor_t /cid/, xcb_pixmap_t /source/, xcb_pixmap_t /mask/, uint16_t
/fore_red/, uint16_t /fore_green/, uint16_t /fore_blue/, uint16_t
/back_red/, uint16_t /back_green/, uint16_t /back_blue/, uint16_t /x/,
uint16_t /y/);\\

* REQUEST ARGUMENTS
- conn :: The XCB connection to X11.

- cid :: TODO: NOT YET DOCUMENTED.

- source :: TODO: NOT YET DOCUMENTED.

- mask :: TODO: NOT YET DOCUMENTED.

- fore_red :: TODO: NOT YET DOCUMENTED.

- fore_green :: TODO: NOT YET DOCUMENTED.

- fore_blue :: TODO: NOT YET DOCUMENTED.

- back_red :: TODO: NOT YET DOCUMENTED.

- back_green :: TODO: NOT YET DOCUMENTED.

- back_blue :: TODO: NOT YET DOCUMENTED.

24. TODO: NOT YET DOCUMENTED.

25. TODO: NOT YET DOCUMENTED.

* DESCRIPTION
* RETURN VALUE
Returns an /xcb_void_cookie_t/. Errors (if any) have to be handled in
the event loop.

If you want to handle errors directly with /xcb_request_check/ instead,
use /xcb_create_cursor_checked/. See *xcb-requests(3)* for details.

* ERRORS
This request does never generate any errors.

* SEE ALSO
* AUTHOR
Generated from xproto.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
