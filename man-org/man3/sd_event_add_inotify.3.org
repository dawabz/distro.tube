#+TITLE: Manpages - sd_event_add_inotify.3
#+DESCRIPTION: Linux manpage for sd_event_add_inotify.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_event_add_inotify, sd_event_source_get_inotify_mask,
sd_event_inotify_handler_t - Add an "inotify" file system inode event
source to an event loop

* SYNOPSIS
#+begin_example
  #include <systemd/sd-event.h>
#+end_example

#+begin_example
  typedef struct sd_event_source sd_event_source;
#+end_example

*typedef int (*sd_event_inotify_handler_t)(sd_event_source **/s/*, const
struct inotify_event **/event/*, void **/userdata/*);*

*int sd_event_add_inotify(sd_event **/event/*, sd_event_source
***/source/*, const char **/path/*, uint32_t */mask/*,
sd_event_inotify_handler_t */handler/*, void **/userdata/*);*

*int sd_event_source_get_inotify_mask(sd_event_source **/source/*,
uint32_t **/mask/*);*

* DESCRIPTION
*sd_event_add_inotify()* adds a new *inotify*(7) file system inode event
source to an event loop. The event loop object is specified in the
/event/ parameter, the event source object is returned in the /source/
parameter. The /path/ parameter specifies the path of the file system
inode to watch. The /handler/ must reference a function to call when the
inode changes. The handler function will be passed the /userdata/
pointer, which may be chosen freely by the caller. The handler also
receives a pointer to a struct inotify_event structure containing
information about the inode event. The /mask/ parameter specifies which
types of inode events to watch specifically. It must contain an OR-ed
combination of *IN_ACCESS*, *IN_ATTRIB*, *IN_CLOSE_WRITE*, ... flags.
See *inotify*(7) for further information.

If multiple event sources are installed for the same inode the backing
inotify watch descriptor is automatically shared. The mask parameter may
contain any flag defined by the inotify API, with the exception of
*IN_MASK_ADD*.

The handler is enabled continuously (*SD_EVENT_ON*), but this may be
changed with *sd_event_source_set_enabled*(3). Alternatively, the
*IN_ONESHOT* mask flag may be used to request *SD_EVENT_ONESHOT* mode.
If the handler function returns a negative error code, it will be
disabled after the invocation, even if the *SD_EVENT_ON* mode was
requested before.

As a special limitation the priority of inotify event sources may only
be altered (see *sd_event_source_set_priority*(3)) in the time between
creation of the event source object with *sd_event_add_inotify()* and
the beginning of the next event loop iteration. Attempts of changing the
priority any later will be refused. Consider freeing and allocating a
new inotify event source to change the priority at that point.

To destroy an event source object use *sd_event_source_unref*(3), but
note that the event source is only removed from the event loop when all
references to the event source are dropped. To make sure an event source
does not fire anymore, even when theres still a reference to it kept,
consider disabling it with *sd_event_source_set_enabled*(3).

If the second parameter of *sd_event_add_inotify()* is passed as *NULL*
no reference to the event source object is returned. In this case the
event source is considered "floating", and will be destroyed implicitly
when the event loop itself is destroyed.

If the /handler/ parameter to *sd_event_add_inotify()* is *NULL*, and
the event source fires, this will be considered a request to exit the
event loop. In this case, the /userdata/ parameter, cast to an integer,
is passed as the exit code parameter to *sd_event_exit*(3).

*sd_event_source_get_inotify_mask()* retrieves the configured inotify
watch mask of an event source created previously with
*sd_event_add_inotify()*. It takes the event source object as the
/source/ parameter and a pointer to a *uint32_t* variable to return the
mask in.

* RETURN VALUE
On success, these functions return 0 or a positive integer. On failure,
they return a negative errno-style error code.

** Errors
Returned errors may indicate the following problems:

*-ENOMEM*

#+begin_quote
  Not enough memory to allocate an object.
#+end_quote

*-EINVAL*

#+begin_quote
  An invalid argument has been passed. This includes specifying a mask
  with *IN_MASK_ADD* set.
#+end_quote

*-ESTALE*

#+begin_quote
  The event loop is already terminated.
#+end_quote

*-ECHILD*

#+begin_quote
  The event loop has been created in a different process.
#+end_quote

*-EDOM*

#+begin_quote
  The passed event source is not an inotify process event source.
#+end_quote

* EXAMPLES
*Example 1. A simple program that uses inotify to monitor one or two
directories*

#+begin_quote
  #+begin_example
    #include <stdio.h>
    #include <string.h>
    #include <sys/inotify.h>

    #include <systemd/sd-event.h>

    #define _cleanup_(f) __attribute__((cleanup(f)))

    static int inotify_handler(sd_event_source *source,
                               const struct inotify_event *event,
                               void *userdata) {

      const char *desc = NULL;

      sd_event_source_get_description(source, &desc);

      if (event->mask & IN_Q_OVERFLOW)
        printf("inotify-handler <%s>: overflow\n", desc);
      else if (event->mask & IN_CREATE)
        printf("inotify-handler <%s>: create on %s\n", desc, event->name);
      else if (event->mask & IN_DELETE)
        printf("inotify-handler <%s>: delete on %s\n", desc, event->name);
      else if (event->mask & IN_MOVED_TO)
        printf("inotify-handler <%s>: moved-to on %s\n", desc, event->name);

      /* Terminate the program if an "exit" file appears */
      if ((event->mask & (IN_CREATE|IN_MOVED_TO)) &&
          strcmp(event->name, "exit") == 0)
        sd_event_exit(sd_event_source_get_event(source), 0);

      return 1;
    }

    int main(int argc, char **argv) {
      _cleanup_(sd_event_unrefp) sd_event *event = NULL;
      _cleanup_(sd_event_source_unrefp) sd_event_source *source1 = NULL, *source2 = NULL;

      const char *path1 = argc > 1 ? argv[1] : "/tmp";
      const char *path2 = argc > 2 ? argv[2] : NULL;

      /* Note: failure handling is omitted for brevity */

      sd_event_default(&event);

      sd_event_add_inotify(event, &source1, path1,
                           IN_CREATE | IN_DELETE | IN_MODIFY | IN_MOVED_TO,
                           inotify_handler, NULL);
      if (path2)
        sd_event_add_inotify(event, &source2, path2,
                             IN_CREATE | IN_DELETE | IN_MODIFY | IN_MOVED_TO,
                             inotify_handler, NULL);

      sd_event_loop(event);

      return 0;
    }
  #+end_example
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-event*(3), *sd_event_new*(3), *sd_event_now*(3),
*sd_event_add_io*(3), *sd_event_add_time*(3), *sd_event_add_signal*(3),
*sd_event_add_defer*(3), *sd_event_add_child*(3),
*sd_event_source_set_enabled*(3), *sd_event_source_set_priority*(3),
*sd_event_source_set_userdata*(3), *sd_event_source_set_description*(3),
*sd_event_source_set_floating*(3), *waitid*(2)
