#+TITLE: Manpages - __gnu_parallel___replace_if_selector.3
#+DESCRIPTION: Linux manpage for __gnu_parallel___replace_if_selector.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::__replace_if_selector< _It, _Op, _Tp > - std::replace()
selector.

* SYNOPSIS
\\

=#include <for_each_selectors.h>=

Inherits *__gnu_parallel::__generic_for_each_selector< _It >*.

** Public Member Functions
*__replace_if_selector* (const _Tp &*__new_val*)\\
Constructor.

bool *operator()* (_Op &__o, _It __i)\\
Functor execution.

** Public Attributes
const _Tp & *__new_val*\\
Value to replace with.

_It *_M_finish_iterator*\\
_Iterator on last element processed; needed for some algorithms (e. g.
std::transform()).

* Detailed Description
** "template<typename _It, typename _Op, typename _Tp>
\\
struct __gnu_parallel::__replace_if_selector< _It, _Op, _Tp
>"std::replace() selector.

Definition at line *156* of file *for_each_selectors.h*.

* Constructor & Destructor Documentation
** template<typename _It , typename _Op , typename _Tp >
*__gnu_parallel::__replace_if_selector*< _It, _Op, _Tp
>::*__replace_if_selector* (const _Tp & __new_val)= [inline]=,
= [explicit]=
Constructor.

*Parameters*

#+begin_quote
  /__new_val/ Value to replace with.
#+end_quote

Definition at line *164* of file *for_each_selectors.h*.

* Member Function Documentation
** template<typename _It , typename _Op , typename _Tp > bool
*__gnu_parallel::__replace_if_selector*< _It, _Op, _Tp >::operator()
(_Op & __o, _It __i)= [inline]=
Functor execution.

*Parameters*

#+begin_quote
  /__o/ Operator.\\
  /__i/ iterator referencing object.
#+end_quote

Definition at line *170* of file *for_each_selectors.h*.

References *__gnu_parallel::__replace_if_selector< _It, _Op, _Tp
>::__new_val*.

* Member Data Documentation
** template<typename _It , typename _Op , typename _Tp > const _Tp&
*__gnu_parallel::__replace_if_selector*< _It, _Op, _Tp >::__new_val
Value to replace with.

Definition at line *159* of file *for_each_selectors.h*.

Referenced by *__gnu_parallel::__replace_if_selector< _It, _Op, _Tp
>::operator()()*.

** template<typename _It > _It
*__gnu_parallel::__generic_for_each_selector*< _It
>::_M_finish_iterator= [inherited]=
_Iterator on last element processed; needed for some algorithms (e. g.
std::transform()).

Definition at line *47* of file *for_each_selectors.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
