#+TITLE: Manpages - SDL_PushEvent.3
#+DESCRIPTION: Linux manpage for SDL_PushEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_PushEvent - Pushes an event onto the event queue

* SYNOPSIS
*#include "SDL.h"*

*int SDL_PushEvent*(*SDL_Event *event*);

* DESCRIPTION
The event queue can actually be used as a two way communication channel.
Not only can events be read from the queue, but the user can also push
their own events onto it. *event* is a pointer to the event structure
you wish to push onto the queue.

#+begin_quote
  *Note: *

  Pushing device input events onto the queue doesn't modify the state of
  the device within SDL.
#+end_quote

* RETURN VALUE
Returns *0* on success or *-1* if the event couldn't be pushed.

* EXAMPLES
See *SDL_Event*.

* SEE ALSO
*SDL_PollEvent*, *SDL_PeepEvents*, *SDL_Event*
