#+TITLE: Manpages - ares_parse_srv_reply.3
#+DESCRIPTION: Linux manpage for ares_parse_srv_reply.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ares_parse_srv_reply - Parse a reply to a DNS query of type SRV

* SYNOPSIS
#+begin_example
  #include <ares.h>

  int ares_parse_srv_reply(const unsigned char* abuf, int alen,
  struct ares_srv_reply** srv_out);
#+end_example

* DESCRIPTION
The *ares_parse_srv_reply* function parses the response to a query of
type SRV into a linked list of /struct ares_srv_reply/ The parameters
/abuf/ and /alen/ give the contents of the response. The result is
stored in allocated memory and a pointer to it stored into the variable
pointed to by /srv_out/. It is the caller's responsibility to free the
resulting /srv_out/ structure when it is no longer needed using the
function *ares_free_data*

The structure /ares_srv_reply/ contains the following fields:

#+begin_example
  struct ares_srv_reply {
      struct ares_srv_reply  *next;
      unsigned short weight;
      unsigned short priority;
      unsigned short port;
      char *host;
  };
#+end_example

* RETURN VALUES
*ares_parse_srv_reply* can return any of the following values:

- *ARES_SUCCESS* :: The response was successfully parsed.

- *ARES_EBADRESP* :: The response was malformatted.

- *ARES_ENODATA* :: The response did not contain an answer to the query.

- *ARES_ENOMEM* :: Memory was exhausted.

* AVAILABILITY
This function was first introduced in c-ares version 1.7.0.

* SEE ALSO
*ares_query*(3) *ares_free_data*(3)

* AUTHOR
Written by Jakub Hrozek <jhrozek@redhat.com>, on behalf of Red Hat, Inc
http://www.redhat.com
