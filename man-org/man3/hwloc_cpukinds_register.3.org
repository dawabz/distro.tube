#+TITLE: Manpages - hwloc_cpukinds_register.3
#+DESCRIPTION: Linux manpage for hwloc_cpukinds_register.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about hwloc_cpukinds_register.3 is found in manpage for: [[../man3/hwlocality_cpukinds.3][man3/hwlocality_cpukinds.3]]