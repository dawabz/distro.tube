#+TITLE: Manpages - Pod_Perldoc_BaseTo.3perl
#+DESCRIPTION: Linux manpage for Pod_Perldoc_BaseTo.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Pod::Perldoc::BaseTo - Base for Pod::Perldoc formatters

* SYNOPSIS
package Pod::Perldoc::ToMyFormat; use parent qw( Pod::Perldoc::BaseTo );
...

* DESCRIPTION
This package is meant as a base of Pod::Perldoc formatters, like
Pod::Perldoc::ToText, Pod::Perldoc::ToMan, etc.

It provides default implementations for the methods

is_pageable write_with_binmode output_extension _perldoc_elem

The concrete formatter must implement

new parse_from_file

* SEE ALSO
perldoc

* COPYRIGHT AND DISCLAIMERS
Copyright (c) 2002-2007 Sean M. Burke.

This library is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

This program is distributed in the hope that it will be useful, but
without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose.

* AUTHOR
Current maintainer: Mark Allen =<mallen@cpan.org>=

Past contributions from: brian d foy =<bdfoy@cpan.org>= Adriano R.
Ferreira =<ferreira@cpan.org>=, Sean M. Burke =<sburke@cpan.org>=
