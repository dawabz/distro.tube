#+TITLE: Manpages - erf.3
#+DESCRIPTION: Linux manpage for erf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
erf, erff, erfl - error function

* SYNOPSIS
#+begin_example
  #include <math.h>

  double erf(double x);
  float erff(float x);
  long double erfl(long double x);
#+end_example

Link with /-lm/.

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*erf*():

#+begin_example
      _ISOC99_SOURCE || _POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE
          || /* Since glibc 2.19: */ _DEFAULT_SOURCE
          || /* Glibc <= 2.19: */ _BSD_SOURCE || _SVID_SOURCE
#+end_example

*erff*(), *erfl*():

#+begin_example
      _ISOC99_SOURCE || _POSIX_C_SOURCE >= 200112L
          || /* Since glibc 2.19: */ _DEFAULT_SOURCE
          || /* Glibc <= 2.19: */ _BSD_SOURCE || _SVID_SOURCE
#+end_example

* DESCRIPTION
These functions return the error function of /x/, defined as

-  erf(x) = 2/sqrt(pi) * integral from 0 to x of exp(-t*t) dt :: 

* RETURN VALUE
On success, these functions return the value of the error function of
/x/, a value in the range [-1, 1].

If /x/ is a NaN, a NaN is returned.

If /x/ is +0 (-0), +0 (-0) is returned.

If /x/ is positive infinity (negative infinity), +1 (-1) is returned.

If /x/ is subnormal, a range error occurs, and the return value is
2*x/sqrt(pi).

* ERRORS
See *math_error*(7) for information on how to determine whether an error
has occurred when calling these functions.

The following errors can occur:

- Range error: result underflow (/x/ is subnormal) :: An underflow
  floating-point exception (*FE_UNDERFLOW*) is raised.

These functions do not set /errno/.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                   | Attribute     | Value   |
| *erf*(), *erff*(), *erfl*() | Thread safety | MT-Safe |

* CONFORMING TO
C99, POSIX.1-2001, POSIX.1-2008.

The variant returning /double/ also conforms to SVr4, 4.3BSD.

* SEE ALSO
*cerf*(3), *erfc*(3), *exp*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
