#+TITLE: Manpages - std___detail__List_node_header.3
#+DESCRIPTION: Linux manpage for std___detail__List_node_header.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_List_node_header - The list node header.

* SYNOPSIS
\\

=#include <stl_list.h>=

Inherits *std::__detail::_List_node_base*.

** Public Member Functions
*_List_node_header* (*_List_node_header* &&__x) noexcept\\

void *_M_hook* (*_List_node_base* *const __position) noexcept\\

void *_M_init* () noexcept\\

void *_M_move_nodes* (*_List_node_header* &&__x)\\

void *_M_reverse* () noexcept\\

void *_M_transfer* (*_List_node_base* *const __first, *_List_node_base*
*const __last) noexcept\\

void *_M_unhook* () noexcept\\

** Static Public Member Functions
static void *swap* (*_List_node_base* &__x, *_List_node_base* &__y)
noexcept\\

** Public Attributes
*_List_node_base* * *_M_next*\\

*_List_node_base* * *_M_prev*\\

* Detailed Description
The list node header.

Definition at line *103* of file *stl_list.h*.

* Constructor & Destructor Documentation
** std::__detail::_List_node_header::_List_node_header ()= [inline]=,
= [noexcept]=
Definition at line *109* of file *stl_list.h*.

** std::__detail::_List_node_header::_List_node_header
(*_List_node_header* && __x)= [inline]=, = [noexcept]=
Definition at line *113* of file *stl_list.h*.

* Member Function Documentation
** void std::__detail::_List_node_header::_M_init ()= [inline]=,
= [noexcept]=
Definition at line *149* of file *stl_list.h*.

** void std::__detail::_List_node_header::_M_move_nodes
(*_List_node_header* && __x)= [inline]=
Definition at line *129* of file *stl_list.h*.

* Member Data Documentation
** *_List_node_base**
std::__detail::_List_node_base::_M_next= [inherited]=
Definition at line *82* of file *stl_list.h*.

** *_List_node_base**
std::__detail::_List_node_base::_M_prev= [inherited]=
Definition at line *83* of file *stl_list.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
