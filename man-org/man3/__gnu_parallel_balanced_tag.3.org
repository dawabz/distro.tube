#+TITLE: Manpages - __gnu_parallel_balanced_tag.3
#+DESCRIPTION: Linux manpage for __gnu_parallel_balanced_tag.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::balanced_tag - Recommends parallel execution using
dynamic load-balancing at compile time.

* SYNOPSIS
\\

=#include <tags.h>=

Inherits *__gnu_parallel::parallel_tag*.

** Public Member Functions
*_ThreadIndex* *__get_num_threads* ()\\
Find out desired number of threads.

void *set_num_threads* (*_ThreadIndex* __num_threads)\\
Set the desired number of threads.

* Detailed Description
Recommends parallel execution using dynamic load-balancing at compile
time.

Definition at line *88* of file *tags.h*.

* Member Function Documentation
** *_ThreadIndex* __gnu_parallel::parallel_tag::__get_num_threads
()= [inline]=, = [inherited]=
Find out desired number of threads.

*Returns*

#+begin_quote
  Desired number of threads.
#+end_quote

Definition at line *63* of file *tags.h*.

Referenced by *__gnu_parallel::__parallel_sort()*.

** void __gnu_parallel::parallel_tag::set_num_threads (*_ThreadIndex*
__num_threads)= [inline]=, = [inherited]=
Set the desired number of threads.

*Parameters*

#+begin_quote
  /__num_threads/ Desired number of threads.
#+end_quote

Definition at line *73* of file *tags.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
