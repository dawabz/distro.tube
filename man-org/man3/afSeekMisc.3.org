#+TITLE: Manpages - afSeekMisc.3
#+DESCRIPTION: Linux manpage for afSeekMisc.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about afSeekMisc.3 is found in manpage for: [[../afReadMisc.3][afReadMisc.3]]