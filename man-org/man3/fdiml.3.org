#+TITLE: Manpages - fdiml.3
#+DESCRIPTION: Linux manpage for fdiml.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about fdiml.3 is found in manpage for: [[../man3/fdim.3][man3/fdim.3]]