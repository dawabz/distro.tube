#+TITLE: Manpages - gnutls_record_get_max_early_data_size.3
#+DESCRIPTION: Linux manpage for gnutls_record_get_max_early_data_size.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_record_get_max_early_data_size - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*size_t gnutls_record_get_max_early_data_size(gnutls_session_t
*/session/*);*

* ARGUMENTS
- gnutls_session_t session :: is a *gnutls_session_t* type.

* DESCRIPTION
This function returns the maximum early data size in this connection.
This property can only be set to servers. The client may be provided
with the maximum allowed size through the "early_data" extension of the
NewSessionTicket handshake message.

* RETURNS
The maximum early data size in this connection.

* SINCE
3.6.5

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
