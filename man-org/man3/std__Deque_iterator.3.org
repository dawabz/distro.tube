#+TITLE: Manpages - std__Deque_iterator.3
#+DESCRIPTION: Linux manpage for std__Deque_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Deque_iterator< _Tp, _Ref, _Ptr > - A deque::iterator.

* SYNOPSIS
\\

=#include <stl_deque.h>=

** Public Types
typedef *__ptr_rebind*< _Ptr, _Tp > *_Elt_pointer*\\

typedef *__ptr_rebind*< _Ptr, _Elt_pointer > *_Map_pointer*\\

typedef *_Deque_iterator* *_Self*\\

typedef *__iter*< const _Tp > *const_iterator*\\

typedef ptrdiff_t *difference_type*\\

typedef *__iter*< _Tp > *iterator*\\

typedef *std::random_access_iterator_tag* *iterator_category*\\

typedef _Ptr *pointer*\\

typedef _Ref *reference*\\

typedef size_t *size_type*\\

typedef _Tp *value_type*\\

** Public Member Functions
*_Deque_iterator* (_Elt_pointer __x, _Map_pointer __y) noexcept\\

*_Deque_iterator* (const *_Deque_iterator* &__x) noexcept\\

template<typename _Iter , typename = _Require<is_same<_Self,
const_iterator>, is_same<_Iter, iterator>>> *_Deque_iterator* (const
_Iter &__x) noexcept\\

*iterator* *_M_const_cast* () const noexcept\\

void *_M_set_node* (_Map_pointer __new_node) noexcept\\

reference *operator** () const noexcept\\

*_Self* & *operator++* () noexcept\\

*_Self* *operator++* (int) noexcept\\

*_Self* & *operator+=* (difference_type __n) noexcept\\

*_Self* & *operator--* () noexcept\\

*_Self* *operator--* (int) noexcept\\

*_Self* & *operator-=* (difference_type __n) noexcept\\

pointer *operator->* () const noexcept\\

*_Deque_iterator* & *operator=* (const *_Deque_iterator* &)=default\\

reference *operator[]* (difference_type __n) const noexcept\\

** Static Public Member Functions
static size_t *_S_buffer_size* () noexcept\\

** Public Attributes
_Elt_pointer *_M_cur*\\

_Elt_pointer *_M_first*\\

_Elt_pointer *_M_last*\\

_Map_pointer *_M_node*\\

** Friends
template<typename _RefR , typename _PtrR > bool *operator!=* (const
*_Self* &__x, const *_Deque_iterator*< _Tp, _RefR, _PtrR > &__y)
noexcept\\

bool *operator!=* (const *_Self* &__x, const *_Self* &__y) noexcept\\

*_Self* *operator+* (const *_Self* &__x, difference_type __n) noexcept\\

*_Self* *operator+* (difference_type __n, const *_Self* &__x) noexcept\\

template<typename _RefR , typename _PtrR > difference_type *operator-*
(const *_Self* &__x, const *_Deque_iterator*< _Tp, _RefR, _PtrR > &__y)
noexcept\\

difference_type *operator-* (const *_Self* &__x, const *_Self* &__y)
noexcept\\

*_Self* *operator-* (const *_Self* &__x, difference_type __n) noexcept\\

template<typename _RefR , typename _PtrR > bool *operator<* (const
*_Self* &__x, const *_Deque_iterator*< _Tp, _RefR, _PtrR > &__y)
noexcept\\

bool *operator<* (const *_Self* &__x, const *_Self* &__y) noexcept\\

template<typename _RefR , typename _PtrR > bool *operator<=* (const
*_Self* &__x, const *_Deque_iterator*< _Tp, _RefR, _PtrR > &__y)
noexcept\\

bool *operator<=* (const *_Self* &__x, const *_Self* &__y) noexcept\\

template<typename _RefR , typename _PtrR > bool *operator==* (const
*_Self* &__x, const *_Deque_iterator*< _Tp, _RefR, _PtrR > &__y)
noexcept\\

bool *operator==* (const *_Self* &__x, const *_Self* &__y) noexcept\\

template<typename _RefR , typename _PtrR > bool *operator>* (const
*_Self* &__x, const *_Deque_iterator*< _Tp, _RefR, _PtrR > &__y)
noexcept\\

bool *operator>* (const *_Self* &__x, const *_Self* &__y) noexcept\\

template<typename _RefR , typename _PtrR > bool *operator>=* (const
*_Self* &__x, const *_Deque_iterator*< _Tp, _RefR, _PtrR > &__y)
noexcept\\

bool *operator>=* (const *_Self* &__x, const *_Self* &__y) noexcept\\

* Detailed Description
** "template<typename _Tp, typename _Ref, typename _Ptr>
\\
struct std::_Deque_iterator< _Tp, _Ref, _Ptr >"A deque::iterator.

Quite a bit of intelligence here. Much of the functionality of deque is
actually passed off to this class. A deque holds two of these
internally, marking its valid range. Access to elements is done as
offsets of either of those two, relying on operator overloading in this
class.

All the functions are op overloads except for _M_set_node.

Definition at line *113* of file *stl_deque.h*.

* Member Typedef Documentation
** template<typename _Tp , typename _Ref , typename _Ptr > typedef
*__ptr_rebind*<_Ptr, _Tp> *std::_Deque_iterator*< _Tp, _Ref, _Ptr
>::_Elt_pointer
Definition at line *127* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > typedef
*__ptr_rebind*<_Ptr, _Elt_pointer> *std::_Deque_iterator*< _Tp, _Ref,
_Ptr >::_Map_pointer
Definition at line *128* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > typedef
*_Deque_iterator* *std::_Deque_iterator*< _Tp, _Ref, _Ptr >::*_Self*
Definition at line *140* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > typedef
*__iter*<const _Tp> *std::_Deque_iterator*< _Tp, _Ref, _Ptr
>::*const_iterator*
Definition at line *126* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > typedef
ptrdiff_t *std::_Deque_iterator*< _Tp, _Ref, _Ptr >::difference_type
Definition at line *139* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > typedef
*__iter*<_Tp> *std::_Deque_iterator*< _Tp, _Ref, _Ptr >::*iterator*
Definition at line *125* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > typedef
*std::random_access_iterator_tag* *std::_Deque_iterator*< _Tp, _Ref,
_Ptr >::*iterator_category*
Definition at line *134* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > typedef _Ptr
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::pointer
Definition at line *136* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > typedef _Ref
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::reference
Definition at line *137* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > typedef
size_t *std::_Deque_iterator*< _Tp, _Ref, _Ptr >::size_type
Definition at line *138* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > typedef _Tp
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::value_type
Definition at line *135* of file *stl_deque.h*.

* Constructor & Destructor Documentation
** template<typename _Tp , typename _Ref , typename _Ptr >
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::*_Deque_iterator*
(_Elt_pointer __x, _Map_pointer __y)= [inline]=, = [noexcept]=
Definition at line *147* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr >
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::*_Deque_iterator*
()= [inline]=, = [noexcept]=
Definition at line *151* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr >
template<typename _Iter , typename = _Require<is_same<_Self,
const_iterator>, is_same<_Iter, iterator>>> *std::_Deque_iterator*< _Tp,
_Ref, _Ptr >::*_Deque_iterator* (const _Iter & __x)= [inline]=,
= [noexcept]=
Definition at line *164* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr >
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::*_Deque_iterator* (const
*_Deque_iterator*< _Tp, _Ref, _Ptr > & __x)= [inline]=, = [noexcept]=
Definition at line *168* of file *stl_deque.h*.

* Member Function Documentation
** template<typename _Tp , typename _Ref , typename _Ptr > *iterator*
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::_M_const_cast ()
const= [inline]=, = [noexcept]=
Definition at line *176* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > void
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::_M_set_node (_Map_pointer
__new_node)= [inline]=, = [noexcept]=
Prepares to traverse new_node. Sets everything except _M_cur, which
should therefore be set by the caller immediately afterwards, based on
_M_first and _M_last.

Definition at line *260* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > static size_t
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::_S_buffer_size ()= [inline]=,
= [static]=, = [noexcept]=
Definition at line *131* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > reference
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::operator* ()
const= [inline]=, = [noexcept]=
Definition at line *180* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > *_Self* &
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::operator++ ()= [inline]=,
= [noexcept]=
Definition at line *188* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > *_Self*
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::operator++ (int)= [inline]=,
= [noexcept]=
Definition at line *200* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > *_Self* &
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::operator+= (difference_type
__n)= [inline]=, = [noexcept]=
Definition at line *228* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > *_Self* &
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::operator-- ()= [inline]=,
= [noexcept]=
Definition at line *208* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > *_Self*
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::operator-- (int)= [inline]=,
= [noexcept]=
Definition at line *220* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > *_Self* &
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::operator-= (difference_type
__n)= [inline]=, = [noexcept]=
Definition at line *247* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > pointer
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::operator-> ()
const= [inline]=, = [noexcept]=
Definition at line *184* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > reference
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::operator[] (difference_type
__n) const= [inline]=, = [noexcept]=
Definition at line *251* of file *stl_deque.h*.

* Friends And Related Function Documentation
** template<typename _Tp , typename _Ref , typename _Ptr >
template<typename _RefR , typename _PtrR > bool operator!= (const
*_Self* & __x, const *_Deque_iterator*< _Tp, _RefR, _PtrR > &
__y)= [friend]=
Definition at line *296* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > bool
operator!= (const *_Self* & __x, const *_Self* & __y)= [friend]=
Definition at line *291* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > *_Self*
operator+ (const *_Self* & __x, difference_type __n)= [friend]=
Definition at line *377* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > *_Self*
operator+ (difference_type __n, const *_Self* & __x)= [friend]=
Definition at line *393* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr >
template<typename _RefR , typename _PtrR > difference_type operator-
(const *_Self* & __x, const *_Deque_iterator*< _Tp, _RefR, _PtrR > &
__y)= [friend]=
Definition at line *367* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr >
difference_type operator- (const *_Self* & __x, const *_Self* &
__y)= [friend]=
Definition at line *353* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > *_Self*
operator- (const *_Self* & __x, difference_type __n)= [friend]=
Definition at line *385* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr >
template<typename _RefR , typename _PtrR > bool operator< (const *_Self*
& __x, const *_Deque_iterator*< _Tp, _RefR, _PtrR > & __y)= [friend]=
Definition at line *309* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > bool
operator< (const *_Self* & __x, const *_Self* & __y)= [friend]=
Definition at line *301* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr >
template<typename _RefR , typename _PtrR > bool operator<= (const
*_Self* & __x, const *_Deque_iterator*< _Tp, _RefR, _PtrR > &
__y)= [friend]=
Definition at line *334* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > bool
operator<= (const *_Self* & __x, const *_Self* & __y)= [friend]=
Definition at line *329* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr >
template<typename _RefR , typename _PtrR > bool operator== (const
*_Self* & __x, const *_Deque_iterator*< _Tp, _RefR, _PtrR > &
__y)= [friend]=
Definition at line *276* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > bool
operator== (const *_Self* & __x, const *_Self* & __y)= [friend]=
Definition at line *268* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr >
template<typename _RefR , typename _PtrR > bool operator> (const *_Self*
& __x, const *_Deque_iterator*< _Tp, _RefR, _PtrR > & __y)= [friend]=
Definition at line *324* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > bool
operator> (const *_Self* & __x, const *_Self* & __y)= [friend]=
Definition at line *319* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr >
template<typename _RefR , typename _PtrR > bool operator>= (const
*_Self* & __x, const *_Deque_iterator*< _Tp, _RefR, _PtrR > &
__y)= [friend]=
Definition at line *346* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > bool
operator>= (const *_Self* & __x, const *_Self* & __y)= [friend]=
Definition at line *341* of file *stl_deque.h*.

* Member Data Documentation
** template<typename _Tp , typename _Ref , typename _Ptr > _Elt_pointer
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::_M_cur
Definition at line *142* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > _Elt_pointer
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::_M_first
Definition at line *143* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > _Elt_pointer
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::_M_last
Definition at line *144* of file *stl_deque.h*.

** template<typename _Tp , typename _Ref , typename _Ptr > _Map_pointer
*std::_Deque_iterator*< _Tp, _Ref, _Ptr >::_M_node
Definition at line *145* of file *stl_deque.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
