#+TITLE: Manpages - zip_errors.3
#+DESCRIPTION: Linux manpage for zip_errors.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The following error codes are used by libzip:

Entry has been changed.

Closing zip archive failed.

Compression method not supported.

Compressed data invalid.

CRC error.

Entry has been deleted.

Encryption method not supported.

Premature end of file.

File already exists.

Zip archive inconsistent.

Internal error.

Resource still in use.

Invalid argument.

Malloc failure.

Multi-disk zip archives not supported.

No such file.

No password provided.

Not a zip archive.

No error.

Can't open file.

Operation not supported.

Read-only archive.

Read error.

Can't remove file.

Renaming temporary file failed.

Seek error.

Tell error.

Failure to create temporary file.

Write error.

Wrong password provided.

Containing zip archive was closed.

Zlib error.

and
