#+TITLE: Manpages - std_ratio_less.3
#+DESCRIPTION: Linux manpage for std_ratio_less.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::ratio_less< _R1, _R2 > - ratio_less

* SYNOPSIS
\\

Inherits __ratio_less_impl::type.

* Detailed Description
** "template<typename _R1, typename _R2>
\\
struct std::ratio_less< _R1, _R2 >"ratio_less

Definition at line *397* of file *std/ratio*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
