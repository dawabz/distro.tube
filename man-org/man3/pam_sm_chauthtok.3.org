#+TITLE: Manpages - pam_sm_chauthtok.3
#+DESCRIPTION: Linux manpage for pam_sm_chauthtok.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_sm_chauthtok - PAM service function for authentication token
management

* SYNOPSIS
#+begin_example
  #include <security/pam_modules.h>
#+end_example

*int pam_sm_chauthtok(pam_handle_t **/pamh/*, int */flags/*, int
*/argc/*, const char ***/argv/*);*

* DESCRIPTION
The *pam_sm_chauthtok* function is the service modules implementation of
the *pam_chauthtok*(3) interface.

This function is used to (re-)set the authentication token of the user.

Valid flags, which may be logically ORd with /PAM_SILENT/, are:

PAM_SILENT

#+begin_quote
  Do not emit any messages.
#+end_quote

PAM_CHANGE_EXPIRED_AUTHTOK

#+begin_quote
  This argument indicates to the module that the users authentication
  token (password) should only be changed if it has expired. This flag
  is optional and /must/ be combined with one of the following two
  flags. Note, however, the following two options are /mutually
  exclusive/.
#+end_quote

PAM_PRELIM_CHECK

#+begin_quote
  This indicates that the modules are being probed as to their ready
  status for altering the users authentication token. If the module
  requires access to another system over some network it should attempt
  to verify it can connect to this system on receiving this flag. If a
  module cannot establish it is ready to update the users authentication
  token it should return *PAM_TRY_AGAIN*, this information will be
  passed back to the application.

  If the control value /sufficient/ is used in the password stack, the
  /PAM_PRELIM_CHECK/ section of the modules following that control value
  is not always executed.
#+end_quote

PAM_UPDATE_AUTHTOK

#+begin_quote
  This informs the module that this is the call it should change the
  authorization tokens. If the flag is logically ORd with
  *PAM_CHANGE_EXPIRED_AUTHTOK*, the token is only changed if it has
  actually expired.
#+end_quote

The PAM library calls this function twice in succession. The first time
with *PAM_PRELIM_CHECK* and then, if the module does not return
*PAM_TRY_AGAIN*, subsequently with *PAM_UPDATE_AUTHTOK*. It is only on
the second call that the authorization token is (possibly) changed.

* RETURN VALUES
PAM_AUTHTOK_ERR

#+begin_quote
  The module was unable to obtain the new authentication token.
#+end_quote

PAM_AUTHTOK_RECOVERY_ERR

#+begin_quote
  The module was unable to obtain the old authentication token.
#+end_quote

PAM_AUTHTOK_LOCK_BUSY

#+begin_quote
  Cannot change the authentication token since it is currently locked.
#+end_quote

PAM_AUTHTOK_DISABLE_AGING

#+begin_quote
  Authentication token aging has been disabled.
#+end_quote

PAM_PERM_DENIED

#+begin_quote
  Permission denied.
#+end_quote

PAM_TRY_AGAIN

#+begin_quote
  Preliminary check was unsuccessful. Signals an immediate return to the
  application is desired.
#+end_quote

PAM_SUCCESS

#+begin_quote
  The authentication token was successfully updated.
#+end_quote

PAM_USER_UNKNOWN

#+begin_quote
  User unknown to password service.
#+end_quote

* SEE ALSO
*pam*(3), *pam_chauthtok*(3), *pam_sm_chauthtok*(3), *pam_strerror*(3),
*PAM*(8)
