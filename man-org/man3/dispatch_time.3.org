#+TITLE: Manpages - dispatch_time.3
#+DESCRIPTION: Linux manpage for dispatch_time.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The

and

functions provide a simple mechanism for expressing temporal milestones
for use with dispatch functions that need timeouts or operate on a
schedule.

The

type is a semi-opaque integer, with only the special values

and

being externally defined. All other values are represented using an
internal format that is not safe for integer arithmetic or comparison.
The internal format is subject to change.

The

function returns a milestone relative to an existing milestone after
adding

nanoseconds. If the

parameter maps internally to a wall clock or is

then the returned value is relative to the wall clock. Otherwise, if

is

then the current time of the default host clock is used. On Apple
platforms, the value of the default host clock is obtained from

The

function is useful for creating a milestone relative to a fixed point in
time using the wall clock, as specified by the optional

parameter. If

is NULL, then the current time of the wall clock is used.

is equivalent to

The

and

functions detect overflow and underflow conditions when applying the

parameter.

Overflow causes

to be returned. When

is

then the

parameter is ignored.

Underflow causes the smallest representable value to be returned for a
given clock.

Create a milestone two seconds in the future, relative to the default
clock:

milestone = dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC);

Create a milestone two seconds in the future, in wall clock time:

milestone = dispatch_time(DISPATCH_WALLTIME_NOW, 2 * NSEC_PER_SEC);

Create a milestone for use as an infinite timeout:

milestone = DISPATCH_TIME_FOREVER;

Create a milestone on Tuesday, January 19, 2038:

struct timespec ts; ts.tv_sec = 0x7FFFFFFF; ts.tv_nsec = 0; milestone =
dispatch_walltime(&ts, 0);

Use a negative delta to create a milestone an hour before the one above:

milestone = dispatch_walltime(&ts, -60 * 60 * NSEC_PER_SEC);

These functions return an abstract value for use with

or
