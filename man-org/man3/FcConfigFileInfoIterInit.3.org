#+TITLE: Manpages - FcConfigFileInfoIterInit.3
#+DESCRIPTION: Linux manpage for FcConfigFileInfoIterInit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigFileInfoIterInit - Initialize the iterator

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcConfigFileInfoIterInit (FcConfig */config/*, FcConfigFileInfoIter
**/iter/*);*

* DESCRIPTION
Initialize 'iter' with the first iterator in the config file information
list.

This function isn't MT-safe. *FcConfigReference* must be called before
using this and then *FcConfigDestroy* when the relevant values are no
longer referenced.

* SINCE
version 2.12.91
