#+TITLE: Manpages - XdbeGetBackBufferAttributes.3
#+DESCRIPTION: Linux manpage for XdbeGetBackBufferAttributes.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XdbeGetBackBufferAttributes - returns attributes of a DBE buffer.

* SYNOPSIS
#include <X11/extensions/Xdbe.h>

XdbeBackBufferAttributes *XdbeGetBackBufferAttributes( Display *dpy,
XdbeBackBuffer buffer)

* DESCRIPTION
This function returns the attributes associated with the specified
buffer.

The /XdbeBackBufferAttributes/ structure has the following fields:

#+begin_quote
  Window window window that buffer belongs to
#+end_quote

If buffer is not a valid /XdbeBackBuffer,/ window returns None.

The returned /XdbeBackBufferAttributes/ structure can be freed with the
Xlib function *Xfree().*

* SEE ALSO
DBE, /XdbeAllocateBackBufferName(),/ /XdbeBeginIdiom(),/
/XdbeDeallocateBackBufferName(),/ /XdbeEndIdiom(),/
/XdbeFreeVisualInfo(),/ /XdbeGetVisualInfo(),/ /XdbeQueryExtension(),/
/XdbeSwapBuffers()./
