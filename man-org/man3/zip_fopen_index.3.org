#+TITLE: Manpages - zip_fopen_index.3
#+DESCRIPTION: Linux manpage for zip_fopen_index.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function opens the file name

in

The

argument specifies how the name lookup should be done, according to the
values are described in

Also, the following values may be

to it.

Read the compressed data. Otherwise the data is uncompressed by

Read the original data from the zip archive, ignoring any changes made
to the file.

The

function opens the file at position

If encrypted data is encountered, the functions call

or

respectively, using the default password set with

Upon successful completion, a

pointer is returned. Otherwise,

is returned and the error code in

is set to indicate the error.

The file data has been changed.

The compression method used is not supported.

The encryption method used is not supported.

Required memory could not be allocated.

The file is encrypted, but no password has been provided.

A file read error occurred.

A file seek error occurred.

The provided password does not match the password used for encryption.
Note that some incorrect passwords are not detected by the check done by

Initializing the zlib stream failed.

The function

may also fail and set

for any of the errors specified for the routine

The function

may also fail with

if

is invalid.

and

were added in libzip 1.0.

and
