#+TITLE: Manpages - __gnu_debug__Safe_sequence.3
#+DESCRIPTION: Linux manpage for __gnu_debug__Safe_sequence.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_debug::_Safe_sequence< _Sequence > - Base class for constructing a
/safe/ sequence type that tracks iterators that reference it.

* SYNOPSIS
\\

=#include <safe_sequence.h>=

Inherits *__gnu_debug::_Safe_sequence_base*.

Inherited by *__gnu_debug::_Safe_container< basic_string< _CharT,
std::char_traits< _CharT >, std::allocator< _CharT > >, std::allocator<
_CharT >, _Safe_sequence, bool(_GLIBCXX_USE_CXX11_ABI)>*,
*__gnu_debug::_Safe_container< deque< _Tp, std::allocator< _Tp > >,
std::allocator< _Tp >, __gnu_debug::_Safe_sequence >*,
*__gnu_debug::_Safe_container< vector< _Tp, std::allocator< _Tp > >,
std::allocator< _Tp >, __gnu_debug::_Safe_sequence >*, and
*__gnu_debug::_Safe_node_sequence< _Sequence >*.

** Public Member Functions
template<typename _Predicate > void *_M_invalidate_if* (_Predicate
__pred)\\

template<typename _Predicate > void *_M_transfer_from_if*
(*_Safe_sequence* &__from, _Predicate __pred)\\

** Public Attributes
*_Safe_iterator_base* * *_M_const_iterators*\\
The list of constant iterators that reference this container.

*_Safe_iterator_base* * *_M_iterators*\\
The list of mutable iterators that reference this container.

unsigned int *_M_version*\\
The container version number. This number may never be 0.

** Protected Member Functions
void *_M_detach_all* ()\\

void *_M_detach_singular* ()\\

__gnu_cxx::__mutex & *_M_get_mutex* () throw ()\\

void *_M_invalidate_all* () const\\

void *_M_revalidate_singular* ()\\

void *_M_swap* (*_Safe_sequence_base* &__x) noexcept\\

* Detailed Description
** "template<typename _Sequence>
\\
class __gnu_debug::_Safe_sequence< _Sequence >"Base class for
constructing a /safe/ sequence type that tracks iterators that reference
it.

The class template _Safe_sequence simplifies the construction of /safe/
sequences that track the iterators that reference the sequence, so that
the iterators are notified of changes in the sequence that may affect
their operation, e.g., if the container invalidates its iterators or is
destructed. This class template may only be used by deriving from it and
passing the name of the derived class as its template parameter via the
curiously recurring template pattern. The derived class must have
=iterator= and =const_iterator= types that are instantiations of class
template _Safe_iterator for this sequence. Iterators will then be
tracked automatically.

Definition at line *108* of file *safe_sequence.h*.

* Member Function Documentation
** void __gnu_debug::_Safe_sequence_base::_M_detach_all
()= [protected]=, = [inherited]=
Detach all iterators, leaving them singular.

Referenced by
*__gnu_debug::_Safe_sequence_base::~_Safe_sequence_base()*.

** void __gnu_debug::_Safe_sequence_base::_M_detach_singular
()= [protected]=, = [inherited]=
Detach all singular iterators.

*Postcondition*

#+begin_quote
  for all iterators i attached to this sequence, i->_M_version ==
  _M_version.
#+end_quote

** __gnu_cxx::__mutex & __gnu_debug::_Safe_sequence_base::_M_get_mutex
()= [protected]=, = [inherited]=
For use in _Safe_sequence.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** void __gnu_debug::_Safe_sequence_base::_M_invalidate_all ()
const= [inline]=, = [protected]=, = [inherited]=
Invalidates all iterators.

Definition at line *256* of file *safe_base.h*.

References *__gnu_debug::_Safe_sequence_base::_M_version*.

** template<typename _Sequence > template<typename _Predicate > void
*__gnu_debug::_Safe_sequence*< _Sequence >::_M_invalidate_if (_Predicate
__pred)
Invalidates all iterators =x= that reference this sequence, are not
singular, and for which =__pred(x)= returns =true=. =__pred= will be
invoked with the normal iterators nested in the safe ones.

Definition at line *37* of file *safe_sequence.tcc*.

** void __gnu_debug::_Safe_sequence_base::_M_revalidate_singular
()= [protected]=, = [inherited]=
Revalidates all attached singular iterators. This method may be used to
validate iterators that were invalidated before (but for some reason,
such as an exception, need to become valid again).

** void __gnu_debug::_Safe_sequence_base::_M_swap (*_Safe_sequence_base*
& __x)= [protected]=, = [noexcept]=, = [inherited]=
Swap this sequence with the given sequence. This operation also swaps
ownership of the iterators, so that when the operation is complete all
iterators that originally referenced one container now reference the
other container.

** template<typename _Sequence > template<typename _Predicate > void
*__gnu_debug::_Safe_sequence*< _Sequence >::_M_transfer_from_if
(*_Safe_sequence*< _Sequence > & __from, _Predicate __pred)
Transfers all iterators =x= that reference =from= sequence, are not
singular, and for which =__pred(x)= returns =true=. =__pred= will be
invoked with the normal iterators nested in the safe ones.

Definition at line *68* of file *safe_sequence.tcc*.

References *std::__addressof()*,
*__gnu_debug::_Safe_sequence_base::_M_const_iterators*,
*__gnu_debug::_Safe_iterator_base::_M_detach_single()*,
*__gnu_debug::_Safe_sequence_base::_M_get_mutex()*,
*__gnu_debug::_Safe_sequence_base::_M_iterators*,
*__gnu_debug::_Safe_iterator_base::_M_next*,
*__gnu_debug::_Safe_iterator_base::_M_prior*,
*__gnu_debug::_Safe_iterator_base::_M_sequence*, and
*__gnu_debug::_Safe_iterator_base::_M_version*.

* Member Data Documentation
** *_Safe_iterator_base**
__gnu_debug::_Safe_sequence_base::_M_const_iterators= [inherited]=
The list of constant iterators that reference this container.

Definition at line *197* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** *_Safe_iterator_base**
__gnu_debug::_Safe_sequence_base::_M_iterators= [inherited]=
The list of mutable iterators that reference this container.

Definition at line *194* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** unsigned int
__gnu_debug::_Safe_sequence_base::_M_version= [mutable]=, = [inherited]=
The container version number. This number may never be 0.

Definition at line *200* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence_base::_M_invalidate_all()*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
