#+TITLE: Manpages - XkbAllocGeomOverlayKeys.3
#+DESCRIPTION: Linux manpage for XkbAllocGeomOverlayKeys.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbAllocGeomOverlayKeys - Allocate keys in an overlay row

* SYNOPSIS
*Status XkbAllocGeomOverlayKeys* *( XkbOverlayRowPtr */row/* ,* *int
*/num_needed/* );*

* ARGUMENTS
- /- row/ :: section for which rows should be allocated

- /- num_needed/ :: number of new rows required

* DESCRIPTION
Xkb provides a number of functions to allocate and free subcomponents of
a keyboard geometry. Use these functions to create or modify keyboard
geometries. Note that these functions merely allocate space for the new
element(s), and it is up to you to fill in the values explicitly in your
code. These allocation functions increase /sz_*/ but never touch /num_*/
(unless there is an allocation failure, in which case they reset both
/sz_*/ and /num_*/ to zero). These functions return Success if they
succeed, BadAlloc if they are not able to allocate space, or BadValue if
a parameter is not as expected.

/XkbAllocGeomOverlayKeys/ allocates /num_needed/ keys and adds them to
the /row./ No initialization of the keys is done.

* DIAGNOSTICS
- *BadAlloc* :: Unable to allocate storage

- *BadValue* :: An argument is out of range
