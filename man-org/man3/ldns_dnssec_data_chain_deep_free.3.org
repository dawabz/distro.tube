#+TITLE: Manpages - ldns_dnssec_data_chain_deep_free.3
#+DESCRIPTION: Linux manpage for ldns_dnssec_data_chain_deep_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_dnssec_data_chain_new, ldns_dnssec_data_chain_free,
ldns_dnssec_data_chain_deep_free, ldns_dnssec_build_data_chain,
ldns_dnssec_data_chain_print - ldns_chain creation, destruction and
printing

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_dnssec_data_chain* ldns_dnssec_data_chain_new(void);

void ldns_dnssec_data_chain_free(ldns_dnssec_data_chain *chain);

void ldns_dnssec_data_chain_deep_free(ldns_dnssec_data_chain *chain);

ldns_dnssec_data_chain* ldns_dnssec_build_data_chain(ldns_resolver *res,
const uint16_t qflags, const ldns_rr_list *data_set, const ldns_pkt
*pkt, ldns_rr *orig_rr);

void ldns_dnssec_data_chain_print(FILE *out, const
ldns_dnssec_data_chain *chain);

* DESCRIPTION
/ldns_dnssec_data_chain_new/() Creates a new dnssec_chain structure .br
Returns ldns_dnssec_data_chain *

/ldns_dnssec_data_chain_free/() Frees a dnssec_data_chain structure

.br **chain*: The chain to free

/ldns_dnssec_data_chain_deep_free/() Frees a dnssec_data_chain
structure, and all data contained therein

.br **chain*: The dnssec_data_chain to free

/ldns_dnssec_build_data_chain/() Build an ldns_dnssec_data_chain, which
contains all DNSSEC data that is needed to derive the trust tree later

The data_set will be cloned

.br **res*: resolver structure for further needed queries .br *qflags*:
resolution flags .br **data_set*: The original rrset where the chain
ends .br **pkt*: optional, can contain the original packet (and hence
the sigs and maybe the key) .br **orig_rr*: The original Resource Record

.br Returns the DNSSEC data chain

/ldns_dnssec_data_chain_print/() Prints the dnssec_data_chain to the
given file stream

.br **out*: The file stream to print to .br **chain*: The
dnssec_data_chain to print

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_dnssec_data_chain/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*,
*RFC4033*, *RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
