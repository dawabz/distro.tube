#+TITLE: Manpages - xcb_xf86dri_get_drawable_info_back_clip_rects_length.3
#+DESCRIPTION: Linux manpage for xcb_xf86dri_get_drawable_info_back_clip_rects_length.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_xf86dri_get_drawable_info_back_clip_rects_length.3 is found in manpage for: [[../man3/xcb_xf86dri_get_drawable_info.3][man3/xcb_xf86dri_get_drawable_info.3]]