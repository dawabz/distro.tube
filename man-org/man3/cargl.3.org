#+TITLE: Manpages - cargl.3
#+DESCRIPTION: Linux manpage for cargl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about cargl.3 is found in manpage for: [[../man3/carg.3][man3/carg.3]]