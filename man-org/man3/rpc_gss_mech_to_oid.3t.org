#+TITLE: Manpages - rpc_gss_mech_to_oid.3t
#+DESCRIPTION: Linux manpage for rpc_gss_mech_to_oid.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
This function returns the GSS OID associated with the GSS_API mechanism
"mech".

The name of a GSS_API mechanism. "kerberos_v5" is currently the only
supported mechanism.

Buffer in which to place the returned OID

If the GSS_API mechanism name is recognized,

is returned. The corresponding GSS-API oid is returned in

Otherwise

is returned and

is left untouched.

The

function is part of libtirpc.

This manual page was written by
