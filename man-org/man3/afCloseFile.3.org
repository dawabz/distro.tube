#+TITLE: Manpages - afCloseFile.3
#+DESCRIPTION: Linux manpage for afCloseFile.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
afCloseFile - close an open audio file

* SYNOPSIS
#+begin_example
  #include <audiofile.h>
#+end_example

#+begin_example
  int afCloseFile(AFfilehandle file);
#+end_example

* PARAMETERS
file is a valid file handle created by *afOpenFile*(3).

* DESCRIPTION
afCloseFile closes the audio file /file/, updating the file if it was
opened for writing.

* RETURN VALUE
afCloseFile returns 0 if the file was closed properly and -1 if an error
occurred while closing the file.

* ERRORS
afCloseFile can generate these possible errors:

#+begin_quote
  ·

  AF_BAD_FILEHANDLE
#+end_quote

#+begin_quote
  ·

  AF_BAD_LSEEK
#+end_quote

#+begin_quote
  ·

  AF_BAD_WRITE
#+end_quote

* SEE ALSO
*afOpenFile*(3)

* AUTHOR
Michael Pruett <michael@68k.org>
