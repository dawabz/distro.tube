#+TITLE: Manpages - CURLOPT_TRAILERFUNCTION.3
#+DESCRIPTION: Linux manpage for CURLOPT_TRAILERFUNCTION.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_TRAILERFUNCTION - callback for sending trailing headers

* SYNOPSIS
#include <curl.h>

int curl_trailer_callback(struct curl_slist ** list, void *userdata);

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TRAILERFUNCTION,
curl_trailer_callback *func);

* DESCRIPTION
Pass a pointer to a callback function.

This callback function will be called once right before sending the
final CR LF in an HTTP chunked transfer to fill a list of trailing
headers to be sent before finishing the HTTP transfer.

You can set the userdata argument with the CURLOPT_TRAILERDATA option.

The trailing headers included in the linked list must not be
CRLF-terminated, because libcurl will add the appropriate line
termination characters after each header item.

If you use curl_slist_append to add trailing headers to the curl_slist
then libcurl will duplicate the strings, and will free the curl_slist
and the duplicates once the trailers have been sent.

If one of the trailing headers is not formatted correctly (i.e.
HeaderName: headerdata) it will be ignored and an info message will be
emitted.

The return value can either be CURL_TRAILERFUNC_OK or
CURL_TRAILERFUNC_ABORT which would respectively instruct libcurl to
either continue with sending the trailers or to abort the request.

If you set this option to NULL, then the transfer proceeds as usual
without any interruptions.

* DEFAULT
NULL

* PROTOCOLS
HTTP

* EXAMPLE
#include <curl/curl.h>

static int trailer_cb(struct curl_slist **tr, void *data) { /* libcurl
will free the list */ *tr = curl_slist_append(*tr,
"My-super-awesome-trailer: trailer-stuff"); return CURL_TRAILERFUNC_OK;
}

CURL *curl = curl_easy_init(); if(curl) { /* Set the URL of the request
*/ curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/"); /* Now
set it as a put */ curl_easy_setopt(curl, CURLOPT_PUT, 1L);

/* Assuming we have a function that will return the data to be pushed
Let that function be read_cb */ curl_easy_setopt(curl,
CURLOPT_READFUNCTION, read_cb);

struct curl_slist *headers = NULL; headers = curl_slist_append(headers,
"Trailer: My-super-awesome-trailer"); res = curl_easy_setopt(curl,
CURLOPT_HTTPHEADER, headers);

/* Set the trailers filling callback */ curl_easy_setopt(curl,
CURLOPT_TRAILERFUNCTION, trailer_cb);

/* Perform the request, res will get the return code */ res =
curl_easy_perform(curl);

curl_easy_cleanup(curl);

curl_slist_free_all(headers); }

* AVAILABILITY
This option was added in curl 7.64.0 and is present if HTTP support is
enabled

* RETURN VALUE
Returns CURLE_OK.

* SEE ALSO
*CURLOPT_TRAILERDATA*(3),
