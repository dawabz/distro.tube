#+TITLE: Manpages - FcFontSetDestroy.3
#+DESCRIPTION: Linux manpage for FcFontSetDestroy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcFontSetDestroy - Destroy a font set

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcFontSetDestroy (FcFontSet */s/*);*

* DESCRIPTION
Destroys a font set. Note that this destroys any referenced patterns as
well.
