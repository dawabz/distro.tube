#+TITLE: Manpages - FcConfigGetCurrent.3
#+DESCRIPTION: Linux manpage for FcConfigGetCurrent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigGetCurrent - Return current configuration

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcConfig * FcConfigGetCurrent (void*);*

* DESCRIPTION
Returns the current default configuration.
