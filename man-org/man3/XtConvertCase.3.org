#+TITLE: Manpages - XtConvertCase.3
#+DESCRIPTION: Linux manpage for XtConvertCase.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtConvertCase.3 is found in manpage for: [[../man3/XtSetKeyTranslator.3][man3/XtSetKeyTranslator.3]]