#+TITLE: Manpages - gnutls_privkey_export_x509.3
#+DESCRIPTION: Linux manpage for gnutls_privkey_export_x509.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_privkey_export_x509 - API function

* SYNOPSIS
*#include <gnutls/abstract.h>*

*int gnutls_privkey_export_x509(gnutls_privkey_t */pkey/*,
gnutls_x509_privkey_t * */key/*);*

* ARGUMENTS
- gnutls_privkey_t pkey :: The private key

- gnutls_x509_privkey_t * key :: Location for the key to be exported.

* DESCRIPTION
Converts the given abstract private key to a *gnutls_x509_privkey_t*
type. The abstract key must be of type *GNUTLS_PRIVKEY_X509*. The input
/key/ must not be initialized. The key returned in /key/ should be
deinitialized using *gnutls_x509_privkey_deinit()*.

* RETURNS
On success, *GNUTLS_E_SUCCESS* (0) is returned, otherwise a negative
error value.

* SINCE
3.4.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
