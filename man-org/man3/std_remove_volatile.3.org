#+TITLE: Manpages - std_remove_volatile.3
#+DESCRIPTION: Linux manpage for std_remove_volatile.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::remove_volatile< _Tp > - remove_volatile

* SYNOPSIS
\\

** Public Types
typedef _Tp *type*\\

* Detailed Description
** "template<typename _Tp>
\\
struct std::remove_volatile< _Tp >"remove_volatile

Definition at line *1478* of file *std/type_traits*.

* Member Typedef Documentation
** template<typename _Tp > typedef _Tp *std::remove_volatile*< _Tp
>::type
Definition at line *1479* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
