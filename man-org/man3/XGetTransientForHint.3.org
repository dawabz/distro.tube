#+TITLE: Manpages - XGetTransientForHint.3
#+DESCRIPTION: Linux manpage for XGetTransientForHint.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XGetTransientForHint.3 is found in manpage for: [[../man3/XSetTransientForHint.3][man3/XSetTransientForHint.3]]