#+TITLE: Manpages - zip_error_system_type.3
#+DESCRIPTION: Linux manpage for zip_error_system_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function returns the type of the system specific part for the zip_error

Currently, the following system types are defined:

System specific part of

is unused.

System specific part of

is an

System specific part of

is a

error.

was added in libzip 1.0.

and
