#+TITLE: Man3 - Y
#+DESCRIPTION: Man3 - Y
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* Y
#+begin_src bash :exports results
readarray -t starts_with_y < <(find . -type f -iname "y*" | sort)

for x in "${starts_with_y[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./y0.3.org][y0.3]]  |
| [[file:./y0f.3.org][y0f.3]] |
| [[file:./y0l.3.org][y0l.3]] |
| [[file:./y1.3.org][y1.3]]  |
| [[file:./y1f.3.org][y1f.3]] |
| [[file:./y1l.3.org][y1l.3]] |
| [[file:./yn.3.org][yn.3]]  |
| [[file:./ynf.3.org][ynf.3]] |
| [[file:./ynl.3.org][ynl.3]] |
