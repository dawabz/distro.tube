#+TITLE: Manpages - sincosl.3
#+DESCRIPTION: Linux manpage for sincosl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sincosl.3 is found in manpage for: [[../man3/sincos.3][man3/sincos.3]]