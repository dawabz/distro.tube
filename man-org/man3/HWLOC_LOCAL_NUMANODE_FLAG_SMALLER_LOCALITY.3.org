#+TITLE: Manpages - HWLOC_LOCAL_NUMANODE_FLAG_SMALLER_LOCALITY.3
#+DESCRIPTION: Linux manpage for HWLOC_LOCAL_NUMANODE_FLAG_SMALLER_LOCALITY.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about HWLOC_LOCAL_NUMANODE_FLAG_SMALLER_LOCALITY.3 is found in manpage for: [[../man3/hwlocality_memattrs.3][man3/hwlocality_memattrs.3]]