#+TITLE: Manpages - XQueryKeymap.3
#+DESCRIPTION: Linux manpage for XQueryKeymap.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XQueryKeymap.3 is found in manpage for: [[../man3/XChangeKeyboardControl.3][man3/XChangeKeyboardControl.3]]