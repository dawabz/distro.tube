#+TITLE: Manpages - xcb_record_get_context_reply.3
#+DESCRIPTION: Linux manpage for xcb_record_get_context_reply.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_record_get_context_reply.3 is found in manpage for: [[../man3/xcb_record_get_context.3][man3/xcb_record_get_context.3]]