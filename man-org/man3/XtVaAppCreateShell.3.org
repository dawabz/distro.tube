#+TITLE: Manpages - XtVaAppCreateShell.3
#+DESCRIPTION: Linux manpage for XtVaAppCreateShell.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtVaAppCreateShell.3 is found in manpage for: [[../man3/XtAppCreateShell.3][man3/XtAppCreateShell.3]]