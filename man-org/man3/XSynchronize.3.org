#+TITLE: Manpages - XSynchronize.3
#+DESCRIPTION: Linux manpage for XSynchronize.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XSynchronize, XSetAfterFunction - enable or disable synchronization

* SYNTAX
int (*XSynchronize ( Display */display/ , Bool /onoff/ ))();

int (*XSetAfterFunction ( Display */display/ , int
( * /procedure/ ) ()))();

* ARGUMENTS
- display :: Specifies the connection to the X server.

- procedure :: Specifies the procedure to be called.

- onoff :: Specifies a Boolean value that indicates whether to enable or
  disable synchronization.

* DESCRIPTION
The *XSynchronize* function returns the previous after function. If
onoff is *True*, *XSynchronize* turns on synchronous behavior. If onoff
is *False*, *XSynchronize* turns off synchronous behavior.

The specified procedure is called with only a display pointer.
*XSetAfterFunction* returns the previous after function.

* SEE ALSO
XSetErrorHandler(3)\\
/Xlib - C Language X Interface/
