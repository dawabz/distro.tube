#+TITLE: Manpages - hwloc_get_pcidev_by_busid.3
#+DESCRIPTION: Linux manpage for hwloc_get_pcidev_by_busid.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about hwloc_get_pcidev_by_busid.3 is found in manpage for: [[../man3/hwlocality_advanced_io.3][man3/hwlocality_advanced_io.3]]