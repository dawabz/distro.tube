#+TITLE: Manpages - gnutls_pkcs7_signature_info_deinit.3
#+DESCRIPTION: Linux manpage for gnutls_pkcs7_signature_info_deinit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_pkcs7_signature_info_deinit - API function

* SYNOPSIS
*#include <gnutls/pkcs7.h>*

*void gnutls_pkcs7_signature_info_deinit(gnutls_pkcs7_signature_info_st
* */info/*);*

* ARGUMENTS
- gnutls_pkcs7_signature_info_st * info :: should point to a
  *gnutls_pkcs7_signature_info_st* structure

* DESCRIPTION
This function will deinitialize any allocated value in the provided
*gnutls_pkcs7_signature_info_st*.

* SINCE
3.4.2

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
