#+TITLE: Manpages - strerrordesc_np.3
#+DESCRIPTION: Linux manpage for strerrordesc_np.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about strerrordesc_np.3 is found in manpage for: [[../man3/strerror.3][man3/strerror.3]]