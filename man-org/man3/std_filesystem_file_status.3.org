#+TITLE: Manpages - std_filesystem_file_status.3
#+DESCRIPTION: Linux manpage for std_filesystem_file_status.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::filesystem::file_status - Information about a file's type and
permissions.

* SYNOPSIS
\\

=#include <fs_dir.h>=

** Public Member Functions
*file_status* (const *file_status* &) noexcept=default\\

*file_status* (*file_status* &&) noexcept=default\\

*file_status* (file_type __ft, *perms* __prms=perms::unknown) noexcept\\

*file_status* & *operator=* (const *file_status* &) noexcept=default\\

*file_status* & *operator=* (*file_status* &&) noexcept=default\\

*perms* *permissions* () const noexcept\\

void *permissions* (*perms* __prms) noexcept\\

file_type *type* () const noexcept\\

void *type* (file_type __ft) noexcept\\

* Detailed Description
Information about a file's type and permissions.

Definition at line *54* of file *bits/fs_dir.h*.

* Constructor & Destructor Documentation
** std::filesystem::file_status::file_status ()= [inline]=,
= [noexcept]=
Definition at line *58* of file *bits/fs_dir.h*.

** std::filesystem::file_status::file_status (file_type __ft, *perms*
__prms = =perms::unknown=)= [inline]=, = [explicit]=, = [noexcept]=
Definition at line *61* of file *bits/fs_dir.h*.

* Member Function Documentation
** *perms* std::filesystem::file_status::permissions ()
const= [inline]=, = [noexcept]=
Definition at line *73* of file *bits/fs_dir.h*.

** void std::filesystem::file_status::permissions (*perms*
__prms)= [inline]=, = [noexcept]=
Definition at line *77* of file *bits/fs_dir.h*.

** file_type std::filesystem::file_status::type () const= [inline]=,
= [noexcept]=
Definition at line *72* of file *bits/fs_dir.h*.

** void std::filesystem::file_status::type (file_type __ft)= [inline]=,
= [noexcept]=
Definition at line *76* of file *bits/fs_dir.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
