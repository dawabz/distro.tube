#+TITLE: Manpages - SDL_GetMouseState.3
#+DESCRIPTION: Linux manpage for SDL_GetMouseState.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_GetMouseState - Retrieve the current state of the mouse

* SYNOPSIS
*#include "SDL.h"*

*Uint8 SDL_GetMouseState*(*int *x, int *y*);

* DESCRIPTION
The current button state is returned as a button bitmask, which can be
tested using the *SDL_BUTTON(X)* macros, and *x* and *y* are set to the
current mouse cursor position. You can pass *NULL* for either *x* or
*y*.

* EXAMPLE
#+begin_example
  SDL_PumpEvents();
  if(SDL_GetMouseState(NULL, NULL)&SDL_BUTTON(1))
    printf("Mouse Button 1(left) is pressed.
  ");
#+end_example

* SEE ALSO
*SDL_GetRelativeMouseState*, *SDL_PumpEvents*
