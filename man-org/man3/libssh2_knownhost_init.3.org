#+TITLE: Manpages - libssh2_knownhost_init.3
#+DESCRIPTION: Linux manpage for libssh2_knownhost_init.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_knownhost_init - init a collection of known hosts

* SYNOPSIS
#include <libssh2.h>

LIBSSH2_KNOWNHOSTS *libssh2_knownhost_init(LIBSSH2_SESSION *session);

* DESCRIPTION
Init a collection of known hosts for this session. Returns the handle to
an internal representation of a known host collection.

Call *libssh2_knownhost_free(3)* to free the collection again after
you're doing using it.

* RETURN VALUE
Returns a handle pointer or NULL if something went wrong. The returned
handle is used as input to all other known host related functions
libssh2 provides.

* AVAILABILITY
Added in libssh2 1.2

* SEE ALSO
*libssh2_knownhost_free(3)* *libssh2_knownhost_add(3)*
*libssh2_knownhost_check(3)*
