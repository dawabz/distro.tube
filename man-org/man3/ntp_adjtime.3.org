#+TITLE: Manpages - ntp_adjtime.3
#+DESCRIPTION: Linux manpage for ntp_adjtime.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ntp_adjtime.3 is found in manpage for: [[../man2/adjtimex.2][man2/adjtimex.2]]