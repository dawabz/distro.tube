#+TITLE: Manpages - XtGetSelectionTimeout.3
#+DESCRIPTION: Linux manpage for XtGetSelectionTimeout.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtGetSelectionTimeout, XtSetSelectionTimeout - set and obtain selection
timeout values

* SYNTAX
#include <X11/Intrinsic.h>

unsigned long XtGetSelectionTimeout(void);

void XtSetSelectionTimeout(unsigned long /timeout/);

* ARGUMENTS
- timeout :: Specifies the selection timeout in milliseconds.

* DESCRIPTION
The *XtGetSelectionTimeout* function has been superceded by
*XtAppGetSelectionTimeout*.

The *XtSetSelectionTimeout* function has been superceded by
*XtAppSetSelectionTimeout*.

* SEE ALSO
*XtAppGetSelectionTimeout*(3)\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
