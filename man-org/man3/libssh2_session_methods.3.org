#+TITLE: Manpages - libssh2_session_methods.3
#+DESCRIPTION: Linux manpage for libssh2_session_methods.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_session_methods - return the currently active algorithms

* SYNOPSIS
#include <libssh2.h>

const char * libssh2_session_methods(LIBSSH2_SESSION *session, int
method_type);

* DESCRIPTION
/session/ - Session instance as returned by *libssh2_session_init_ex(3)*

/method_type/ - one of the method type constants: LIBSSH2_METHOD_KEX,
LIBSSH2_METHOD_HOSTKEY, LIBSSH2_METHOD_CRYPT_CS,
LIBSSH2_METHOD_CRYPT_SC, LIBSSH2_METHOD_MAC_CS, LIBSSH2_METHOD_MAC_SC,
LIBSSH2_METHOD_COMP_CS, LIBSSH2_METHOD_COMP_SC, LIBSSH2_METHOD_LANG_CS,
LIBSSH2_METHOD_LANG_SC.

Returns the actual method negotiated for a particular transport
parameter.

* RETURN VALUE
Negotiated method or NULL if the session has not yet been started.

* ERRORS
/LIBSSH2_ERROR_INVAL/ - The requested method type was invalid.

/LIBSSH2_ERROR_METHOD_NONE/ - no method has been set

* SEE ALSO
*libssh2_session_init_ex(3)*
