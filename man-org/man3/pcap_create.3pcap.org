#+TITLE: Manpages - pcap_create.3pcap
#+DESCRIPTION: Linux manpage for pcap_create.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_create - create a live capture handle

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  char errbuf[PCAP_ERRBUF_SIZE];
  pcap_t *pcap_create(const char *source, char *errbuf);
#+end_example

* DESCRIPTION
*pcap_create*() is used to create a packet capture handle to look at
packets on the network. /source/ is a string that specifies the network
device to open; on Linux systems with 2.2 or later kernels, a /source/
argument of "any" or *NULL* can be used to capture packets from all
interfaces.

The returned handle must be activated with *pcap_activate*(3PCAP) before
packets can be captured with it; options for the capture, such as
promiscuous mode, can be set on the handle before activating it.

* RETURN VALUE
*pcap_create*() returns a /pcap_t */ on success and *NULL* on failure.
If *NULL* is returned, /errbuf/ is filled in with an appropriate error
message. /errbuf/ is assumed to be able to hold at least
*PCAP_ERRBUF_SIZE* chars.

* SEE ALSO
*pcap*(3PCAP)
