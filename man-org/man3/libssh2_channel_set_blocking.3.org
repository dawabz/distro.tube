#+TITLE: Manpages - libssh2_channel_set_blocking.3
#+DESCRIPTION: Linux manpage for libssh2_channel_set_blocking.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_channel_set_blocking - set or clear blocking mode on channel

* SYNOPSIS
#include <libssh2.h>

void libssh2_channel_set_blocking(LIBSSH2_CHANNEL *channel, int
blocking);

* DESCRIPTION
/channel/ - channel stream to set or clean blocking status on.

/blocking/ - Set to a non-zero value to make the channel block, or zero
to make it non-blocking.

Currently this is just a short cut call to
*libssh2_session_set_blocking(3)* and therefore will affect the session
and all channels.

* RETURN VALUE
None

* SEE ALSO
*libssh2_session_set_blocking(3)* *libssh2_channel_read_ex(3)*
*libssh2_channel_write_ex(3)*
