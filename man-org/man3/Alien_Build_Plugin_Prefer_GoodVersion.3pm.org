#+TITLE: Manpages - Alien_Build_Plugin_Prefer_GoodVersion.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Plugin_Prefer_GoodVersion.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Plugin::Prefer::GoodVersion - Plugin to filter known good
versions

* VERSION
version 2.44

* SYNOPSIS
use alienfile; plugin Prefer::GoodVersion => 1.2.3;

* DESCRIPTION
This plugin allows you to specify one or more good versions of a
library. This doesn't affect a system install at all. This plugin does
the opposite of the =Prefer::BadVersion= plugin. You need need a Prefer
plugin that filters and sorts files first. You may specify the filter in
one of three ways:

- as a string :: Filter any files that match the given version. use
  alienfile; plugin Prefer::GoodVersion => 1.2.3;

- as an array :: Filter all files that match any of the given versions.
  use alienfile; plugin Prefer::GoodVersion => [ 1.2.3, 1.2.4 ];

- as a code reference :: Filter any files return a true value. use
  alienfile; plugin Prefer::GoodVersion => sub { my($file) = @_;
  $file->{version} eq 1.2.3; # same as the string version above };

This plugin can also be used to filter known good versions of a library
on just one platform. For example, if you know that version 1.2.3 if
good on windows, but the default logic is fine on other platforms:

use alienfile; plugin Prefer::GoodVersion => 1.2.3 if $^O eq MSWin32;

* PROPERTIES
** filter
Filter entries that match the filter.

* CAVEATS
If you are using the string or array mode, then you need an existing
Prefer plugin that sets the version number for each file candidate, such
as Alien::Build::Plugin::Prefer::SortVersions.

Unless you want to exclude the latest version from a share install, this
plugin isn't really that useful. It has no effect on system installs,
which may not be obvious at first.

* SEE ALSO
- alienfile :: 

- Alien::Build :: 

- Alien::Build::Plugin::Prefer::SortVersions :: 

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
