#+TITLE: Manpages - ldns_buffer_read_at.3
#+DESCRIPTION: Linux manpage for ldns_buffer_read_at.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_buffer_write_at, ldns_buffer_write, ldns_buffer_write_string_at,
ldns_buffer_write_string, ldns_buffer_write_u8_at, ldns_buffer_write_u8,
ldns_buffer_write_u16_at, ldns_buffer_write_u16, ldns_buffer_read_at,
ldns_buffer_read, ldns_buffer_read_u8_at, ldns_buffer_read_u8,
ldns_buffer_read_u16_at, ldns_buffer_read_u16, ldns_buffer_read_u32_at,
ldns_buffer_read_u32, ldns_buffer_write_u32, ldns_buffer_write_u32_at -
reading and writing buffers

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

void ldns_buffer_write_at(ldns_buffer *buffer, size_t at, const void
*data, size_t count);

void ldns_buffer_write(ldns_buffer *buffer, const void *data, size_t
count);

void ldns_buffer_write_string_at(ldns_buffer *buffer, size_t at, const
char *str);

void ldns_buffer_write_string(ldns_buffer *buffer, const char *str);

void ldns_buffer_write_u8_at(ldns_buffer *buffer, size_t at, uint8_t
data);

void ldns_buffer_write_u8(ldns_buffer *buffer, uint8_t data);

void ldns_buffer_write_u16_at(ldns_buffer *buffer, size_t at, uint16_t
data);

void ldns_buffer_write_u16(ldns_buffer *buffer, uint16_t data);

void ldns_buffer_read_at(const ldns_buffer *buffer, size_t at, void
*data, size_t count);

void ldns_buffer_read(ldns_buffer *buffer, void *data, size_t count);

uint8_t ldns_buffer_read_u8_at(const ldns_buffer *buffer, size_t at);

uint8_t ldns_buffer_read_u8(ldns_buffer *buffer);

uint16_t ldns_buffer_read_u16_at(ldns_buffer *buffer, size_t at);

uint16_t ldns_buffer_read_u16(ldns_buffer *buffer);

uint32_t ldns_buffer_read_u32_at(ldns_buffer *buffer, size_t at);

uint32_t ldns_buffer_read_u32(ldns_buffer *buffer);

void ldns_buffer_write_u32(ldns_buffer *buffer, uint32_t data);

void ldns_buffer_write_u32_at(ldns_buffer *buffer, size_t at, uint32_t
data);

* DESCRIPTION
/ldns_buffer_write_at/() writes the given data to the buffer at the
specified position .br *buffer*: the buffer .br *at*: the position (in
number of bytes) to write the data at .br *data*: pointer to the data to
write to the buffer .br *count*: the number of bytes of data to write

/ldns_buffer_write/() writes count bytes of data to the current position
of the buffer .br *buffer*: the buffer .br *data*: the data to write .br
*count*: the length of the data to write

/ldns_buffer_write_string_at/() copies the given (null-delimited) string
to the specified position at the buffer .br *buffer*: the buffer .br
*at*: the position in the buffer .br *str*: the string to write

/ldns_buffer_write_string/() copies the given (null-delimited) string to
the current position at the buffer .br *buffer*: the buffer .br *str*:
the string to write

/ldns_buffer_write_u8_at/() writes the given byte of data at the given
position in the buffer .br *buffer*: the buffer .br *at*: the position
in the buffer .br *data*: the 8 bits to write

/ldns_buffer_write_u8/() writes the given byte of data at the current
position in the buffer .br *buffer*: the buffer .br *data*: the 8 bits
to write

/ldns_buffer_write_u16_at/() writes the given 2 byte integer at the
given position in the buffer .br *buffer*: the buffer .br *at*: the
position in the buffer .br *data*: the 16 bits to write

/ldns_buffer_write_u16/() writes the given 2 byte integer at the current
position in the buffer .br *buffer*: the buffer .br *data*: the 16 bits
to write

/ldns_buffer_read_at/() copies count bytes of data at the given position
to the given data-array .br *buffer*: the buffer .br *at*: the position
in the buffer to start .br *data*: buffer to copy to .br *count*: the
length of the data to copy

/ldns_buffer_read/() copies count bytes of data at the current position
to the given data-array .br *buffer*: the buffer .br *data*: buffer to
copy to .br *count*: the length of the data to copy

/ldns_buffer_read_u8_at/() returns the byte value at the given position
in the buffer .br *buffer*: the buffer .br *at*: the position in the
buffer .br Returns 1 byte integer

/ldns_buffer_read_u8/() returns the byte value at the current position
in the buffer .br *buffer*: the buffer .br Returns 1 byte integer

/ldns_buffer_read_u16_at/() returns the 2-byte integer value at the
given position in the buffer .br *buffer*: the buffer .br *at*: position
in the buffer .br Returns 2 byte integer

/ldns_buffer_read_u16/() returns the 2-byte integer value at the current
position in the buffer .br *buffer*: the buffer .br Returns 2 byte
integer

/ldns_buffer_read_u32_at/() returns the 4-byte integer value at the
given position in the buffer .br *buffer*: the buffer .br *at*: position
in the buffer .br Returns 4 byte integer

/ldns_buffer_read_u32/() returns the 4-byte integer value at the current
position in the buffer .br *buffer*: the buffer .br Returns 4 byte
integer

/ldns_buffer_write_u32/() writes the given 4 byte integer at the current
position in the buffer .br *buffer*: the buffer .br *data*: the 32 bits
to write

/ldns_buffer_write_u32_at/() writes the given 4 byte integer at the
given position in the buffer .br *buffer*: the buffer .br *at*: the
position in the buffer .br *data*: the 32 bits to write

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_buffer/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*,
*RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
