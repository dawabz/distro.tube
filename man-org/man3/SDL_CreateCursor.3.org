#+TITLE: Manpages - SDL_CreateCursor.3
#+DESCRIPTION: Linux manpage for SDL_CreateCursor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CreateCursor - Creates a new mouse cursor.

* SYNOPSIS
*#include "SDL.h"*

*SDL_Cursor *SDL_CreateCursor*(*Uint8 *data, Uint8 *mask, int w, int h,
int hot_x, int hot_y*);

* DESCRIPTION
Create a cursor using the specified *data* and *mask* (in MSB format).
The cursor width must be a multiple of 8 bits.

The cursor is created in black and white according to the following:

- *Data / Mask* :: *Resulting pixel on screen*

- 0 / 1 :: White

- 1 / 1 :: Black

- 0 / 0 :: Transparent

- 1 / 0 :: Inverted color if possible, black if not.

Cursors created with this function must be freed with /SDL_FreeCursor/.

* EXAMPLE
#+begin_example
  /* Stolen from the mailing list */
  /* Creates a new mouse cursor from an XPM */


  /* XPM */
  static const char *arrow[] = {
    /* width height num_colors chars_per_pixel */
    "    32    32        3            1",
    /* colors */
    "X c #000000",
    ". c #ffffff",
    "  c None",
    /* pixels */
    "X                               ",
    "XX                              ",
    "X.X                             ",
    "X..X                            ",
    "X...X                           ",
    "X....X                          ",
    "X.....X                         ",
    "X......X                        ",
    "X.......X                       ",
    "X........X                      ",
    "X.....XXXXX                     ",
    "X..X..X                         ",
    "X.X X..X                        ",
    "XX  X..X                        ",
    "X    X..X                       ",
    "     X..X                       ",
    "      X..X                      ",
    "      X..X                      ",
    "       XX                       ",
    "                                ",
    "                                ",
    "                                ",
    "                                ",
    "                                ",
    "                                ",
    "                                ",
    "                                ",
    "                                ",
    "                                ",
    "                                ",
    "                                ",
    "                                ",
    "0,0"
  };

  static SDL_Cursor *init_system_cursor(const char *image[])
  {
    int i, row, col;
    Uint8 data[4*32];
    Uint8 mask[4*32];
    int hot_x, hot_y;

    i = -1;
    for ( row=0; row<32; ++row ) {
      for ( col=0; col<32; ++col ) {
        if ( col % 8 ) {
          data[i] <<= 1;
          mask[i] <<= 1;
        } else {
          ++i;
          data[i] = mask[i] = 0;
        }
        switch (image[4+row][col]) {
          case 'X':
            data[i] |= 0x01;
            k[i] |= 0x01;
            break;
          case '.':
            mask[i] |= 0x01;
            break;
          case ' ':
            break;
        }
      }
    }
    sscanf(image[4+row], "%d,%d", &hot_x, &hot_y);
    return SDL_CreateCursor(data, mask, 32, 32, hot_x, hot_y);
  }
#+end_example

* SEE ALSO
*SDL_FreeCursor*, *SDL_SetCursor*, *SDL_ShowCursor*
