#+TITLE: Manpages - SPLAY_MAX.3bsd
#+DESCRIPTION: Linux manpage for SPLAY_MAX.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about SPLAY_MAX.3bsd is found in manpage for: [[../man3/tree.3bsd][man3/tree.3bsd]]