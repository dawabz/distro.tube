#+TITLE: Manpages - gnutls_certificate_set_x509_crl.3
#+DESCRIPTION: Linux manpage for gnutls_certificate_set_x509_crl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_certificate_set_x509_crl - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*int gnutls_certificate_set_x509_crl(gnutls_certificate_credentials_t
*/res/*, gnutls_x509_crl_t * */crl_list/*, int */crl_list_size/*);*

* ARGUMENTS
- gnutls_certificate_credentials_t res :: is a
  *gnutls_certificate_credentials_t* type.

- gnutls_x509_crl_t * crl_list :: is a list of trusted CRLs. They should
  have been verified before.

- int crl_list_size :: holds the size of the crl_list

* DESCRIPTION
This function adds the trusted CRLs in order to verify client or server
certificates. In case of a client this is not required to be called if
the certificates are not verified using
*gnutls_certificate_verify_peers2()*. This function may be called
multiple times.

* RETURNS
number of CRLs processed, or a negative error code on error.

* SINCE
2.4.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
