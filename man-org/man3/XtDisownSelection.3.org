#+TITLE: Manpages - XtDisownSelection.3
#+DESCRIPTION: Linux manpage for XtDisownSelection.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtDisownSelection.3 is found in manpage for: [[../man3/XtOwnSelection.3][man3/XtOwnSelection.3]]