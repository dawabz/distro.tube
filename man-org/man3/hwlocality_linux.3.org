#+TITLE: Manpages - hwlocality_linux.3
#+DESCRIPTION: Linux manpage for hwlocality_linux.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwlocality_linux - Linux-specific helpers

* SYNOPSIS
\\

** Functions
int *hwloc_linux_set_tid_cpubind* (*hwloc_topology_t* topology, pid_t
tid, *hwloc_const_cpuset_t* set)\\

int *hwloc_linux_get_tid_cpubind* (*hwloc_topology_t* topology, pid_t
tid, *hwloc_cpuset_t* set)\\

int *hwloc_linux_get_tid_last_cpu_location* (*hwloc_topology_t*
topology, pid_t tid, *hwloc_bitmap_t* set)\\

int *hwloc_linux_read_path_as_cpumask* (const char *path,
*hwloc_bitmap_t* set)\\

* Detailed Description
This includes helpers for manipulating Linux kernel cpumap files, and
hwloc equivalents of the Linux sched_setaffinity and sched_getaffinity
system calls.

* Function Documentation
** int hwloc_linux_get_tid_cpubind (*hwloc_topology_t* topology, pid_t
tid, *hwloc_cpuset_t* set)
Get the current binding of thread =tid=. The behavior is exactly the
same as the Linux sched_getaffinity system call, but uses a hwloc
cpuset.

*Note*

#+begin_quote
  This is equivalent to calling *hwloc_get_proc_cpubind()* with
  *HWLOC_CPUBIND_THREAD* as flags.
#+end_quote

** int hwloc_linux_get_tid_last_cpu_location (*hwloc_topology_t*
topology, pid_t tid, *hwloc_bitmap_t* set)
Get the last physical CPU where thread =tid= ran.

*Note*

#+begin_quote
  This is equivalent to calling *hwloc_get_proc_last_cpu_location()*
  with *HWLOC_CPUBIND_THREAD* as flags.
#+end_quote

** int hwloc_linux_read_path_as_cpumask (const char * path,
*hwloc_bitmap_t* set)
Convert a linux kernel cpumask file =path= into a hwloc bitmap =set=.
Might be used when reading CPU set from sysfs attributes such as
topology and caches for processors, or local_cpus for devices.

*Note*

#+begin_quote
  This function ignores the HWLOC_FSROOT environment variable.
#+end_quote

** int hwloc_linux_set_tid_cpubind (*hwloc_topology_t* topology, pid_t
tid, *hwloc_const_cpuset_t* set)
Bind a thread =tid= on cpus given in cpuset =set=. The behavior is
exactly the same as the Linux sched_setaffinity system call, but uses a
hwloc cpuset.

*Note*

#+begin_quote
  This is equivalent to calling *hwloc_set_proc_cpubind()* with
  HWLOC_CPUBIND_THREAD as flags.
#+end_quote

* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
