#+TITLE: Manpages - significand.3
#+DESCRIPTION: Linux manpage for significand.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
significand, significandf, significandl - get mantissa of floating-point
number

* SYNOPSIS
#+begin_example
  #include <math.h>

  double significand(double x);
  float significandf(float x);
  long double significandl(long double x);
#+end_example

Link with /-lm/.

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*significand*(), *significandf*(), *significandl*():

#+begin_example
      /* Since glibc 2.19: */ _DEFAULT_SOURCE
          || /* Glibc <= 2.19: */ _BSD_SOURCE || _SVID_SOURCE
#+end_example

* DESCRIPTION
These functions return the mantissa of /x/ scaled to the range [1,2).
They are equivalent to

#+begin_example
  scalb(x, (double) -ilogb(x))
#+end_example

This function exists mainly for use in certain standardized tests for
IEEE 754 conformance.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                                           | Attribute     | Value   |
| *significand*(), *significandf*(), *significandl*() | Thread safety | MT-Safe |

* CONFORMING TO
These functions are nonstandard; the /double/ version is available on a
number of other systems.

* SEE ALSO
*ilogb*(3), *scalb*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
