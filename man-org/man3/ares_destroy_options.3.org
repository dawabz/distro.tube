#+TITLE: Manpages - ares_destroy_options.3
#+DESCRIPTION: Linux manpage for ares_destroy_options.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ares_destroy_options - Destroy options initialized with
ares_save_options

* SYNOPSIS
#+begin_example
  #include <ares.h>

  void ares_destroy_options(struct ares_options *options)
#+end_example

* DESCRIPTION
The *ares_destroy_options(3)* function destroys the options struct
identified by Ioptions*, freeing all memory allocated by*
*ares_save_options(3).*

* SEE ALSO
*ares_save_options*(3), *ares_init_options*(3)

* AUTHOR
Brad House\\
Copyright 1998 by the Massachusetts Institute of Technology.
