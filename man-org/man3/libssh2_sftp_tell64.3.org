#+TITLE: Manpages - libssh2_sftp_tell64.3
#+DESCRIPTION: Linux manpage for libssh2_sftp_tell64.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_sftp_tell64 - get the current read/write position indicator for
a file

* SYNOPSIS
#include <libssh2.h> #include <libssh2_sftp.h>

libssh2_uint64_t libssh2_sftp_tell64(LIBSSH2_SFTP_HANDLE *handle);

* DESCRIPTION
/handle/ - SFTP File Handle as returned by *libssh2_sftp_open_ex(3)*

Identify the current offset of the file handle's internal pointer.

* RETURN VALUE
Current offset from beginning of file in bytes.

* AVAILABILITY
Added in libssh2 1.0

* SEE ALSO
*libssh2_sftp_open_ex(3),* *libssh2_sftp_tell(3)*
