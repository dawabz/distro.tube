#+TITLE: Manpages - std_make_signed.3
#+DESCRIPTION: Linux manpage for std_make_signed.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::make_signed< _Tp > - make_signed

* SYNOPSIS
\\

** Public Types
typedef __make_signed_selector< _Tp >::__type *type*\\

* Detailed Description
** "template<typename _Tp>
\\
struct std::make_signed< _Tp >"make_signed

Definition at line *1911* of file *std/type_traits*.

* Member Typedef Documentation
** template<typename _Tp > typedef __make_signed_selector<_Tp>::__type
*std::make_signed*< _Tp >::type
Definition at line *1912* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
