#+TITLE: Manpages - zmq_msg_send.3
#+DESCRIPTION: Linux manpage for zmq_msg_send.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_msg_send - send a message part on a socket

* SYNOPSIS
*int zmq_msg_send (zmq_msg_t */*msg/*, void */*socket/*, int
*/flags/*);*

* DESCRIPTION
The /zmq_msg_send()/ function is identical to *zmq_sendmsg*(3), which
shall be deprecated in future versions. /zmq_msg_send()/ is more
consistent with other message manipulation functions.

The /zmq_msg_send()/ function shall queue the message referenced by the
/msg/ argument to be sent to the socket referenced by the /socket/
argument. The /flags/ argument is a combination of the flags defined
below:

*ZMQ_DONTWAIT*

#+begin_quote
  For socket types (DEALER, PUSH) that block (either with ZMQ_IMMEDIATE
  option set and no peer available, or all peers having full high-water
  mark), specifies that the operation should be performed in
  non-blocking mode. If the message cannot be queued on the /socket/,
  the /zmq_msg_send()/ function shall fail with /errno/ set to EAGAIN.
#+end_quote

*ZMQ_SNDMORE*

#+begin_quote
  Specifies that the message being sent is a multi-part message, and
  that further message parts are to follow. Refer to the section
  regarding multi-part messages below for a detailed description.
#+end_quote

The /zmq_msg_t/ structure passed to /zmq_msg_send()/ is nullified on a
successful call. If you want to send the same message to multiple
sockets you have to copy it (e.g. using /zmq_msg_copy()/). If the call
fails, the /zmq_msg_t/ structure stays intact, and must be consumed by
another call to /zmq_msg_send()/ on the same or another socket, or
released using /zmq_msg_close()/ to avoid a memory leak.

#+begin_quote
  \\

  *Note*

  \\

  A successful invocation of /zmq_msg_send()/ does not indicate that the
  message has been transmitted to the network, only that it has been
  queued on the /socket/ and 0MQ has assumed responsibility for the
  message. You do not need to call /zmq_msg_close()/ after a successful
  /zmq_msg_send()/.
#+end_quote

** Multi-part messages
A 0MQ message is composed of 1 or more message parts. Each message part
is an independent /zmq_msg_t/ in its own right. 0MQ ensures atomic
delivery of messages: peers shall receive either all /message parts/ of
a message or none at all. The total number of message parts is unlimited
except by available memory.

An application that sends multi-part messages must use the /ZMQ_SNDMORE/
flag when sending each message part except the final one.

* RETURN VALUE
The /zmq_msg_send()/ function shall return number of bytes in the
message if successful (if number of bytes is higher than /MAX_INT/, the
function will return /MAX_INT/). Otherwise it shall return -1 and set
/errno/ to one of the values defined below.

* ERRORS
*EAGAIN*

#+begin_quote
  Non-blocking mode was requested and the message cannot be sent at the
  moment.
#+end_quote

*ENOTSUP*

#+begin_quote
  The /zmq_msg_send()/ operation is not supported by this socket type.
#+end_quote

*EINVAL*

#+begin_quote
  The sender tried to send multipart data, which the socket type does
  not allow.
#+end_quote

*EFSM*

#+begin_quote
  The /zmq_msg_send()/ operation cannot be performed on this socket at
  the moment due to the socket not being in the appropriate state. This
  error may occur with socket types that switch between several states,
  such as ZMQ_REP. See the /messaging patterns/ section of
  *zmq_socket*(3) for more information.
#+end_quote

*ETERM*

#+begin_quote
  The 0MQ /context/ associated with the specified /socket/ was
  terminated.
#+end_quote

*ENOTSOCK*

#+begin_quote
  The provided /socket/ was invalid.
#+end_quote

*EINTR*

#+begin_quote
  The operation was interrupted by delivery of a signal before the
  message was sent.
#+end_quote

*EFAULT*

#+begin_quote
  Invalid message.
#+end_quote

*EHOSTUNREACH*

#+begin_quote
  The message cannot be routed.
#+end_quote

* EXAMPLE
*Filling in a message and sending it to a socket*.

#+begin_quote
  #+begin_example
    /* Create a new message, allocating 6 bytes for message content */
    zmq_msg_t msg;
    int rc = zmq_msg_init_size (&msg, 6);
    assert (rc == 0);
    /* Fill in message content with AAAAAA */
    memset (zmq_msg_data (&msg), A, 6);
    /* Send the message to the socket */
    rc = zmq_msg_send (&msg, socket, 0);
    assert (rc == 6);
  #+end_example
#+end_quote

*Sending a multi-part message*.

#+begin_quote
  #+begin_example
    /* Send a multi-part message consisting of three parts to socket */
    rc = zmq_msg_send (&part1, socket, ZMQ_SNDMORE);
    rc = zmq_msg_send (&part2, socket, ZMQ_SNDMORE);
    /* Final part; no more parts to follow */
    rc = zmq_msg_send (&part3, socket, 0);
  #+end_example
#+end_quote

* SEE ALSO
*zmq_recv*(3) *zmq_send*(3) *zmq_msg_recv*(3) *zmq_socket*(7) *zmq*(7)

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
