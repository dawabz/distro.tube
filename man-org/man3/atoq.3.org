#+TITLE: Manpages - atoq.3
#+DESCRIPTION: Linux manpage for atoq.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about atoq.3 is found in manpage for: [[../man3/atoi.3][man3/atoi.3]]