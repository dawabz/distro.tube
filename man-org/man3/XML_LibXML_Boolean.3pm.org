#+TITLE: Manpages - XML_LibXML_Boolean.3pm
#+DESCRIPTION: Linux manpage for XML_LibXML_Boolean.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
XML::LibXML::Boolean - Boolean true/false values

* DESCRIPTION
XML::LibXML::Boolean objects implement simple boolean true/false
objects.

* API
** XML::LibXML::Boolean->True
Creates a new Boolean object with a true value.

** XML::LibXML::Boolean->False
Creates a new Boolean object with a false value.

** *value()*
Returns true or false.

** *to_literal()*
Returns the string true or false.
