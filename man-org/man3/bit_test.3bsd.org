#+TITLE: Manpages - bit_test.3bsd
#+DESCRIPTION: Linux manpage for bit_test.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about bit_test.3bsd is found in manpage for: [[../man3/bitstring.3bsd][man3/bitstring.3bsd]]