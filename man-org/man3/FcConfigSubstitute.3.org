#+TITLE: Manpages - FcConfigSubstitute.3
#+DESCRIPTION: Linux manpage for FcConfigSubstitute.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigSubstitute - Execute substitutions

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcConfigSubstitute (FcConfig */config/*, FcPattern **/p/*,
FcMatchKind */kind/*);*

* DESCRIPTION
Calls FcConfigSubstituteWithPat setting p_pat to NULL. Returns FcFalse
if the substitution cannot be performed (due to allocation failure).
Otherwise returns FcTrue. If /config/ is NULL, the current configuration
is used.
