#+TITLE: Manpages - std_piecewise_constant_distribution_param_type.3
#+DESCRIPTION: Linux manpage for std_piecewise_constant_distribution_param_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::piecewise_constant_distribution< _RealType >::param_type

* SYNOPSIS
\\

=#include <random.h>=

** Public Types
typedef *piecewise_constant_distribution*< _RealType >
*distribution_type*\\

** Public Member Functions
template<typename _InputIteratorB , typename _InputIteratorW >
*param_type* (_InputIteratorB __bfirst, _InputIteratorB __bend,
_InputIteratorW __wbegin)\\

*param_type* (const *param_type* &)=default\\

template<typename _Func > *param_type* (*initializer_list*< _RealType >
__bi, _Func __fw)\\

template<typename _Func > *param_type* (size_t __nw, _RealType __xmin,
_RealType __xmax, _Func __fw)\\

*std::vector*< double > *densities* () const\\

*std::vector*< _RealType > *intervals* () const\\

*param_type* & *operator=* (const *param_type* &)=default\\

** Friends
bool *operator!=* (const *param_type* &__p1, const *param_type* &__p2)\\

bool *operator==* (const *param_type* &__p1, const *param_type* &__p2)\\

class *piecewise_constant_distribution< _RealType >*\\

* Detailed Description
** "template<typename _RealType = double>
\\
struct std::piecewise_constant_distribution< _RealType
>::param_type"Parameter type.

Definition at line *5524* of file *random.h*.

* Member Typedef Documentation
** template<typename _RealType = double> typedef
*piecewise_constant_distribution*<_RealType>
*std::piecewise_constant_distribution*< _RealType
>::*param_type::distribution_type*
Definition at line *5526* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _RealType = double>
*std::piecewise_constant_distribution*< _RealType
>::param_type::param_type ()= [inline]=
Definition at line *5529* of file *random.h*.

** template<typename _RealType > template<typename _InputIteratorB ,
typename _InputIteratorW > *std::piecewise_constant_distribution*<
_RealType >::param_type::param_type (_InputIteratorB __bfirst,
_InputIteratorB __bend, _InputIteratorW __wbegin)
Definition at line *2850* of file *bits/random.tcc*.

** template<typename _RealType > template<typename _Func >
*std::piecewise_constant_distribution*< _RealType
>::param_type::param_type (*initializer_list*< _RealType > __bi, _Func
__fw)
Definition at line *2875* of file *bits/random.tcc*.

** template<typename _RealType > template<typename _Func >
*std::piecewise_constant_distribution*< _RealType
>::param_type::param_type (size_t __nw, _RealType __xmin, _RealType
__xmax, _Func __fw)
Definition at line *2892* of file *bits/random.tcc*.

* Member Function Documentation
** template<typename _RealType = double> *std::vector*< double >
*std::piecewise_constant_distribution*< _RealType
>::param_type::densities () const= [inline]=
Definition at line *5563* of file *random.h*.

** template<typename _RealType = double> *std::vector*< _RealType >
*std::piecewise_constant_distribution*< _RealType
>::param_type::intervals () const= [inline]=
Definition at line *5550* of file *random.h*.

* Friends And Related Function Documentation
** template<typename _RealType = double> bool operator!= (const
*param_type* & __p1, const *param_type* & __p2)= [friend]=
Definition at line *5571* of file *random.h*.

** template<typename _RealType = double> bool operator== (const
*param_type* & __p1, const *param_type* & __p2)= [friend]=
Definition at line *5567* of file *random.h*.

** template<typename _RealType = double> friend class
*piecewise_constant_distribution*< _RealType >= [friend]=
Definition at line *5526* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
