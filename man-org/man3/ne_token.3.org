#+TITLE: Manpages - ne_token.3
#+DESCRIPTION: Linux manpage for ne_token.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ne_token, ne_qtoken - string tokenizers

* SYNOPSIS
#+begin_example
  #include <ne_string.h>
#+end_example

*char *ne_token(char ***/str/*, char */sep/*);*

*char *ne_qtoken(char ***/str/*, char */sep/*, const char **/quotes/*);*

* DESCRIPTION
*ne_token* and *ne_qtoken* tokenize the string at the location stored in
the pointer /str/. Each time the function is called, it returns the next
token, and modifies the /str/ pointer to point to the remainder of the
string, or NULL if there are no more tokens in the string. A token is
delimited by the separator character /sep/; if *ne_qtoken* is used any
quoted segments of the string are skipped when searching for a
separator. A quoted segment is enclosed in a pair of one of the
characters given in the /quotes/ string.

The string being tokenized is modified each time the tokenizing function
is called; replacing the next separator character with a NUL terminator.

* EXAMPLES
The following function prints out each token in a comma-separated string
/list/, which is modified in-place:

#+begin_quote
  #+begin_example
    static void splitter(char *list)
    {
      do {
        printf("Token: %s\n", ne_token(&list, ,));
      while (list);
    }
  #+end_example
#+end_quote

* AUTHOR
*Joe Orton* <neon@lists.manyfish.co.uk>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
