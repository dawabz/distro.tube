#+TITLE: Manpages - rtcSetGeometryTopologyCount.3embree3
#+DESCRIPTION: Linux manpage for rtcSetGeometryTopologyCount.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcSetGeometryTopologyCount - sets the number of topologies of
    a subdivision geometry
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcSetGeometryTopologyCount(
    RTCGeometry geometry,
    unsigned int topologyCount
  );
#+end_example

** DESCRIPTION
The =rtcSetGeometryTopologyCount= function sets the number of topologies
(=topologyCount= parameter) for the specified subdivision geometry
(=geometry= parameter). The number of topologies of a subdivision
geometry must be greater or equal to 1.

To use multiple topologies, first the number of topologies must be
specified, then the individual topologies can be configured using
=rtcSetGeometrySubdivisionMode= and by setting an index buffer
(=RTC_BUFFER_TYPE_INDEX=) using the topology ID as the buffer slot.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[RTC_GEOMETRY_TYPE_SUBDIVISION], [rtcSetGeometrySubdivisionMode]
