#+TITLE: Manpages - hwlocality_distances_consult.3
#+DESCRIPTION: Linux manpage for hwlocality_distances_consult.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwlocality_distances_consult - Helpers for consulting distance matrices

* SYNOPSIS
\\

** Functions
static int *hwloc_distances_obj_index* (struct *hwloc_distances_s*
*distances, *hwloc_obj_t* obj)\\

static int *hwloc_distances_obj_pair_values* (struct *hwloc_distances_s*
*distances, *hwloc_obj_t* obj1, *hwloc_obj_t* obj2, hwloc_uint64_t
*value1to2, hwloc_uint64_t *value2to1)\\

* Detailed Description
* Function Documentation
** static int hwloc_distances_obj_index (struct *hwloc_distances_s* *
distances, *hwloc_obj_t* obj)= [inline]=, = [static]=
Find the index of an object in a distances structure.

*Returns*

#+begin_quote
  -1 if object =obj= is not involved in structure =distances=.
#+end_quote

** static int hwloc_distances_obj_pair_values (struct
*hwloc_distances_s* * distances, *hwloc_obj_t* obj1, *hwloc_obj_t* obj2,
hwloc_uint64_t * value1to2, hwloc_uint64_t * value2to1)= [inline]=,
= [static]=
Find the values between two objects in a distance matrices. The distance
from =obj1= to =obj2= is stored in the value pointed by =value1to2= and
reciprocally.

*Returns*

#+begin_quote
  -1 if object =obj1= or =obj2= is not involved in structure
  =distances=.
#+end_quote

* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
