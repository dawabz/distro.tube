#+TITLE: Manpages - std___debug_list.3
#+DESCRIPTION: Linux manpage for std___debug_list.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__debug::list< _Tp, _Allocator > - Class std::list with
safety/checking/debug instrumentation.

* SYNOPSIS
\\

Inherits *__gnu_debug::_Safe_container< list< _Tp, std::allocator< _Tp >
>, std::allocator< _Tp >, __gnu_debug::_Safe_node_sequence >*, and list<
_Tp, std::allocator< _Tp > >.

** Public Types
typedef _Allocator *allocator_type*\\

typedef *__gnu_debug::_Safe_iterator*< *_Base_const_iterator*, *list* >
*const_iterator*\\

typedef _Base::const_pointer *const_pointer*\\

typedef _Base::const_reference *const_reference*\\

typedef *std::reverse_iterator*< *const_iterator* >
*const_reverse_iterator*\\

typedef _Base::difference_type *difference_type*\\

typedef *__gnu_debug::_Safe_iterator*< *_Base_iterator*, *list* >
*iterator*\\

typedef _Base::pointer *pointer*\\

typedef _Base::reference *reference*\\

typedef *std::reverse_iterator*< *iterator* > *reverse_iterator*\\

typedef _Base::size_type *size_type*\\

typedef _Tp *value_type*\\

** Public Member Functions
*list* (_Base_ref __x)\\

template<class _InputIterator , typename =
std::_RequireInputIter<_InputIterator>> *list* (_InputIterator __first,
_InputIterator __last, const _Allocator &__a=_Allocator())\\

*list* (const _Allocator &__a) noexcept\\

*list* (const *list* &)=default\\

*list* (const *list* &__x, const allocator_type &__a)\\

*list* (*initializer_list*< value_type > __l, const allocator_type
&__a=allocator_type())\\

*list* (*list* &&)=default\\

*list* (*list* &&__x, const allocator_type &__a)
noexcept(*std::is_nothrow_constructible*< *_Base*, *_Base*, const
allocator_type & >::value)\\

*list* (size_type __n, const _Tp &__value, const _Allocator
&__a=_Allocator())\\

*list* (size_type __n, const allocator_type &__a=allocator_type())\\

const *_Base* & *_M_base* () const noexcept\\

*_Base* & *_M_base* () noexcept\\

template<typename _Predicate > void *_M_invalidate_if* (_Predicate
__pred)\\

void *_M_swap* (_Safe_container &__x) noexcept\\

template<typename _Predicate > void *_M_transfer_from_if*
(_Safe_sequence &__from, _Predicate __pred)\\

template<class _InputIterator , typename =
std::_RequireInputIter<_InputIterator>> void *assign* (_InputIterator
__first, _InputIterator __last)\\

void *assign* (*initializer_list*< value_type > __l)\\

void *assign* (size_type __n, const _Tp &__t)\\

const_reference *back* () const noexcept\\

reference *back* () noexcept\\

*const_iterator* *begin* () const noexcept\\

*iterator* *begin* () noexcept\\

*const_iterator* *cbegin* () const noexcept\\

*const_iterator* *cend* () const noexcept\\

void *clear* () noexcept\\

*const_reverse_iterator* *crbegin* () const noexcept\\

*const_reverse_iterator* *crend* () const noexcept\\

template<typename... _Args> *iterator* *emplace* (*const_iterator*
__position, _Args &&... __args)\\

*const_iterator* *end* () const noexcept\\

*iterator* *end* () noexcept\\

*iterator* *erase* (*const_iterator* __first, *const_iterator* __last)
noexcept\\

*iterator* *erase* (*const_iterator* __position) noexcept\\

const_reference *front* () const noexcept\\

reference *front* () noexcept\\

*iterator* *insert* (*const_iterator* __p, *initializer_list*<
value_type > __l)\\

template<class _InputIterator , typename =
std::_RequireInputIter<_InputIterator>> *iterator* *insert*
(*const_iterator* __position, _InputIterator __first, _InputIterator
__last)\\

*iterator* *insert* (*const_iterator* __position, _Tp &&__x)\\

*iterator* *insert* (*const_iterator* __position, const _Tp &__x)\\

*iterator* *insert* (*const_iterator* __position, size_type __n, const
_Tp &__x)\\

void *merge* (*list* &&__x)\\

template<class _Compare > void *merge* (*list* &&__x, _Compare __comp)\\

void *merge* (*list* &__x)\\

template<typename _Compare > void *merge* (*list* &__x, _Compare
__comp)\\

*list* & *operator=* (const *list* &)=default\\

*list* & *operator=* (*initializer_list*< value_type > __l)\\

*list* & *operator=* (*list* &&)=default\\

void *pop_back* () noexcept\\

void *pop_front* () noexcept\\

*const_reverse_iterator* *rbegin* () const noexcept\\

*reverse_iterator* *rbegin* () noexcept\\

__remove_return_type *remove* (const _Tp &__value)\\

template<class _Predicate > __remove_return_type *remove_if* (_Predicate
__pred)\\

*const_reverse_iterator* *rend* () const noexcept\\

*reverse_iterator* *rend* () noexcept\\

void *resize* (size_type __sz)\\

void *resize* (size_type __sz, const _Tp &__c)\\

void *sort* ()\\

template<typename _StrictWeakOrdering > void *sort* (_StrictWeakOrdering
__pred)\\

void *splice* (*const_iterator* __position, *list* &&__x) noexcept\\

void *splice* (*const_iterator* __position, *list* &&__x,
*const_iterator* __first, *const_iterator* __last) noexcept\\

void *splice* (*const_iterator* __position, *list* &&__x,
*const_iterator* __i) noexcept\\

void *splice* (*const_iterator* __position, *list* &__x) noexcept\\

void *splice* (*const_iterator* __position, *list* &__x,
*const_iterator* __first, *const_iterator* __last) noexcept\\

void *splice* (*const_iterator* __position, *list* &__x,
*const_iterator* __i) noexcept\\

void *swap* (*list* &__x) noexcept(/**conditional* */)\\

__remove_return_type *unique* ()\\

template<class _BinaryPredicate > __remove_return_type *unique*
(_BinaryPredicate __binary_pred)\\

** Public Attributes
_Safe_iterator_base * *_M_const_iterators*\\
The list of constant iterators that reference this container.

_Safe_iterator_base * *_M_iterators*\\
The list of mutable iterators that reference this container.

unsigned int *_M_version*\\
The container version number. This number may never be 0.

** Protected Member Functions
void *_M_detach_all* ()\\

void *_M_detach_singular* ()\\

__gnu_cxx::__mutex & *_M_get_mutex* () throw ()\\

void *_M_invalidate_all* ()\\

void *_M_invalidate_all* () const\\

void *_M_revalidate_singular* ()\\

_Safe_container & *_M_safe* () noexcept\\

void *_M_swap* (_Safe_sequence_base &__x) noexcept\\

** Friends
template<typename _ItT , typename _SeqT , typename _CatT > class
*::__gnu_debug::_Safe_iterator*\\

* Detailed Description
** "template<typename _Tp, typename _Allocator = std::allocator<_Tp>>
\\
class std::__debug::list< _Tp, _Allocator >"Class std::list with
safety/checking/debug instrumentation.

Definition at line *50* of file *debug/list*.

* Member Typedef Documentation
** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
typedef _Allocator *std::__debug::list*< _Tp, _Allocator
>::allocator_type
Definition at line *91* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
typedef *__gnu_debug::_Safe_iterator*<*_Base_const_iterator*, *list*>
*std::__debug::list*< _Tp, _Allocator >::*const_iterator*
Definition at line *85* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
typedef _Base::const_pointer *std::__debug::list*< _Tp, _Allocator
>::const_pointer
Definition at line *93* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
typedef _Base::const_reference *std::__debug::list*< _Tp, _Allocator
>::const_reference
Definition at line *80* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
typedef *std::reverse_iterator*<*const_iterator*> *std::__debug::list*<
_Tp, _Allocator >::*const_reverse_iterator*
Definition at line *95* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
typedef _Base::difference_type *std::__debug::list*< _Tp, _Allocator
>::difference_type
Definition at line *88* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
typedef *__gnu_debug::_Safe_iterator*<*_Base_iterator*, *list*>
*std::__debug::list*< _Tp, _Allocator >::*iterator*
Definition at line *83* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
typedef _Base::pointer *std::__debug::list*< _Tp, _Allocator >::pointer
Definition at line *92* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
typedef _Base::reference *std::__debug::list*< _Tp, _Allocator
>::reference
Definition at line *79* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
typedef *std::reverse_iterator*<*iterator*> *std::__debug::list*< _Tp,
_Allocator >::*reverse_iterator*
Definition at line *94* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
typedef _Base::size_type *std::__debug::list*< _Tp, _Allocator
>::size_type
Definition at line *87* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
typedef _Tp *std::__debug::list*< _Tp, _Allocator >::value_type
Definition at line *90* of file *debug/list*.

* Constructor & Destructor Documentation
** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*std::__debug::list*< _Tp, _Allocator >::*list* (*initializer_list*<
value_type > __l, const allocator_type & __a =
=allocator_type()=)= [inline]=
Definition at line *112* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*std::__debug::list*< _Tp, _Allocator >::*list* (const *list*< _Tp,
_Allocator > & __x, const allocator_type & __a)= [inline]=
Definition at line *118* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*std::__debug::list*< _Tp, _Allocator >::*list* (*list*< _Tp, _Allocator
> && __x, const allocator_type & __a)= [inline]=, = [noexcept]=
Definition at line *121* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*std::__debug::list*< _Tp, _Allocator >::*list* (const _Allocator &
__a)= [inline]=, = [explicit]=, = [noexcept]=
Definition at line *130* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*std::__debug::list*< _Tp, _Allocator >::*list* (size_type __n, const
allocator_type & __a = =allocator_type()=)= [inline]=, = [explicit]=
Definition at line *135* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*std::__debug::list*< _Tp, _Allocator >::*list* (size_type __n, const
_Tp & __value, const _Allocator & __a = =_Allocator()=)= [inline]=
Definition at line *138* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
template<class _InputIterator , typename =
std::_RequireInputIter<_InputIterator>> *std::__debug::list*< _Tp,
_Allocator >::*list* (_InputIterator __first, _InputIterator __last,
const _Allocator & __a = =_Allocator()=)= [inline]=
Definition at line *154* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*std::__debug::list*< _Tp, _Allocator >::*list* (_Base_ref
__x)= [inline]=
Definition at line *161* of file *debug/list*.

* Member Function Documentation
** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
const *_Base* & *std::__debug::list*< _Tp, _Allocator >::_M_base ()
const= [inline]=, = [noexcept]=
Definition at line *897* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*_Base* & *std::__debug::list*< _Tp, _Allocator >::_M_base
()= [inline]=, = [noexcept]=
Definition at line *894* of file *debug/list*.

** void __gnu_debug::_Safe_sequence_base::_M_detach_all
()= [protected]=, = [inherited]=
Detach all iterators, leaving them singular.

Referenced by
*__gnu_debug::_Safe_sequence_base::~_Safe_sequence_base()*.

** void __gnu_debug::_Safe_sequence_base::_M_detach_singular
()= [protected]=, = [inherited]=
Detach all singular iterators.

*Postcondition*

#+begin_quote
  for all iterators i attached to this sequence, i->_M_version ==
  _M_version.
#+end_quote

** __gnu_cxx::__mutex & __gnu_debug::_Safe_sequence_base::_M_get_mutex
()= [protected]=, = [inherited]=
For use in _Safe_sequence.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** template<typename _Sequence > void
*__gnu_debug::_Safe_node_sequence*< _Sequence >::_M_invalidate_all
()= [inline]=, = [protected]=, = [inherited]=
Definition at line *136* of file *safe_sequence.h*.

** void __gnu_debug::_Safe_sequence_base::_M_invalidate_all ()
const= [inline]=, = [protected]=, = [inherited]=
Invalidates all iterators.

Definition at line *256* of file *safe_base.h*.

References *__gnu_debug::_Safe_sequence_base::_M_version*.

** template<typename _Sequence > template<typename _Predicate > void
*__gnu_debug::_Safe_sequence*< _Sequence >::_M_invalidate_if (_Predicate
__pred)= [inherited]=
Invalidates all iterators =x= that reference this sequence, are not
singular, and for which =__pred(x)= returns =true=. =__pred= will be
invoked with the normal iterators nested in the safe ones.

Definition at line *37* of file *safe_sequence.tcc*.

** void __gnu_debug::_Safe_sequence_base::_M_revalidate_singular
()= [protected]=, = [inherited]=
Revalidates all attached singular iterators. This method may be used to
validate iterators that were invalidated before (but for some reason,
such as an exception, need to become valid again).

** _Safe_container & *__gnu_debug::_Safe_container*< *list*< _Tp,
*std::allocator*< _Tp > > , *std::allocator*< _Tp > ,
*__gnu_debug::_Safe_node_sequence* , true >::_M_safe ()= [inline]=,
= [protected]=, = [noexcept]=, = [inherited]=
Definition at line *52* of file *safe_container.h*.

** void *__gnu_debug::_Safe_container*< *list*< _Tp, *std::allocator*<
_Tp > > , *std::allocator*< _Tp > , *__gnu_debug::_Safe_node_sequence* ,
true >::_M_swap (*_Safe_container*< *list*< _Tp, *std::allocator*< _Tp >
>, *std::allocator*< _Tp >, *__gnu_debug::_Safe_node_sequence* > &
__x)= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *111* of file *safe_container.h*.

** void __gnu_debug::_Safe_sequence_base::_M_swap (*_Safe_sequence_base*
& __x)= [protected]=, = [noexcept]=, = [inherited]=
Swap this sequence with the given sequence. This operation also swaps
ownership of the iterators, so that when the operation is complete all
iterators that originally referenced one container now reference the
other container.

** template<typename _Sequence > template<typename _Predicate > void
*__gnu_debug::_Safe_sequence*< _Sequence >::_M_transfer_from_if
(*_Safe_sequence*< _Sequence > & __from, _Predicate
__pred)= [inherited]=
Transfers all iterators =x= that reference =from= sequence, are not
singular, and for which =__pred(x)= returns =true=. =__pred= will be
invoked with the normal iterators nested in the safe ones.

Definition at line *68* of file *safe_sequence.tcc*.

References *std::__addressof()*,
*__gnu_debug::_Safe_sequence_base::_M_const_iterators*,
*__gnu_debug::_Safe_iterator_base::_M_detach_single()*,
*__gnu_debug::_Safe_sequence_base::_M_get_mutex()*,
*__gnu_debug::_Safe_sequence_base::_M_iterators*,
*__gnu_debug::_Safe_iterator_base::_M_next*,
*__gnu_debug::_Safe_iterator_base::_M_prior*,
*__gnu_debug::_Safe_iterator_base::_M_sequence*, and
*__gnu_debug::_Safe_iterator_base::_M_version*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
template<class _InputIterator , typename =
std::_RequireInputIter<_InputIterator>> void *std::__debug::list*< _Tp,
_Allocator >::assign (_InputIterator __first, _InputIterator
__last)= [inline]=
Definition at line *202* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
void *std::__debug::list*< _Tp, _Allocator >::assign
(*initializer_list*< value_type > __l)= [inline]=
Definition at line *188* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
void *std::__debug::list*< _Tp, _Allocator >::assign (size_type __n,
const _Tp & __t)= [inline]=
Definition at line *217* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
const_reference *std::__debug::list*< _Tp, _Allocator >::back ()
const= [inline]=, = [noexcept]=
Definition at line *381* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
reference *std::__debug::list*< _Tp, _Allocator >::back ()= [inline]=,
= [noexcept]=
Definition at line *374* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*const_iterator* *std::__debug::list*< _Tp, _Allocator >::begin ()
const= [inline]=, = [noexcept]=
Definition at line *231* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*iterator* *std::__debug::list*< _Tp, _Allocator >::begin ()= [inline]=,
= [noexcept]=
Definition at line *227* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*const_iterator* *std::__debug::list*< _Tp, _Allocator >::cbegin ()
const= [inline]=, = [noexcept]=
Definition at line *260* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*const_iterator* *std::__debug::list*< _Tp, _Allocator >::cend ()
const= [inline]=, = [noexcept]=
Definition at line *264* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
void *std::__debug::list*< _Tp, _Allocator >::clear ()= [inline]=,
= [noexcept]=
Definition at line *560* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*const_reverse_iterator* *std::__debug::list*< _Tp, _Allocator
>::crbegin () const= [inline]=, = [noexcept]=
Definition at line *268* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*const_reverse_iterator* *std::__debug::list*< _Tp, _Allocator >::crend
() const= [inline]=, = [noexcept]=
Definition at line *272* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
template<typename... _Args> *iterator* *std::__debug::list*< _Tp,
_Allocator >::emplace (*const_iterator* __position, _Args &&...
__args)= [inline]=
Definition at line *419* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*const_iterator* *std::__debug::list*< _Tp, _Allocator >::end ()
const= [inline]=, = [noexcept]=
Definition at line *239* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*iterator* *std::__debug::list*< _Tp, _Allocator >::end ()= [inline]=,
= [noexcept]=
Definition at line *235* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*iterator* *std::__debug::list*< _Tp, _Allocator >::erase
(*const_iterator* __first, *const_iterator* __last)= [inline]=,
= [noexcept]=
Definition at line *530* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*iterator* *std::__debug::list*< _Tp, _Allocator >::erase
(*const_iterator* __position)= [inline]=, = [noexcept]=
Definition at line *519* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
const_reference *std::__debug::list*< _Tp, _Allocator >::front ()
const= [inline]=, = [noexcept]=
Definition at line *367* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
reference *std::__debug::list*< _Tp, _Allocator >::front ()= [inline]=,
= [noexcept]=
Definition at line *360* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*iterator* *std::__debug::list*< _Tp, _Allocator >::insert
(*const_iterator* __p, *initializer_list*< value_type > __l)= [inline]=
Definition at line *444* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
template<class _InputIterator , typename =
std::_RequireInputIter<_InputIterator>> *iterator* *std::__debug::list*<
_Tp, _Allocator >::insert (*const_iterator* __position, _InputIterator
__first, _InputIterator __last)= [inline]=
Definition at line *471* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*iterator* *std::__debug::list*< _Tp, _Allocator >::insert
(*const_iterator* __position, _Tp && __x)= [inline]=
Definition at line *440* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*iterator* *std::__debug::list*< _Tp, _Allocator >::insert
(*const_iterator* __position, const _Tp & __x)= [inline]=
Definition at line *429* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*iterator* *std::__debug::list*< _Tp, _Allocator >::insert
(*const_iterator* __position, size_type __n, const _Tp & __x)= [inline]=
Definition at line *453* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
void *std::__debug::list*< _Tp, _Allocator >::merge (*list*< _Tp,
_Allocator > && __x)= [inline]=
Definition at line *834* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
template<class _Compare > void *std::__debug::list*< _Tp, _Allocator
>::merge (*list*< _Tp, _Allocator > && __x, _Compare __comp)= [inline]=
Definition at line *859* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
void *std::__debug::list*< _Tp, _Allocator >::merge (*list*< _Tp,
_Allocator > & __x)= [inline]=
Definition at line *852* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
template<typename _Compare > void *std::__debug::list*< _Tp, _Allocator
>::merge (*list*< _Tp, _Allocator > & __x, _Compare __comp)= [inline]=
Definition at line *880* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*list* & *std::__debug::list*< _Tp, _Allocator >::operator=
(*initializer_list*< value_type > __l)= [inline]=
Definition at line *180* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
void *std::__debug::list*< _Tp, _Allocator >::pop_back ()= [inline]=,
= [noexcept]=
Definition at line *409* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
void *std::__debug::list*< _Tp, _Allocator >::pop_front ()= [inline]=,
= [noexcept]=
Definition at line *395* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*const_reverse_iterator* *std::__debug::list*< _Tp, _Allocator >::rbegin
() const= [inline]=, = [noexcept]=
Definition at line *247* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*reverse_iterator* *std::__debug::list*< _Tp, _Allocator >::rbegin
()= [inline]=, = [noexcept]=
Definition at line *243* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
__remove_return_type *std::__debug::list*< _Tp, _Allocator >::remove
(const _Tp & __value)= [inline]=
Definition at line *683* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
template<class _Predicate > __remove_return_type *std::__debug::list*<
_Tp, _Allocator >::remove_if (_Predicate __pred)= [inline]=
Definition at line *722* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*const_reverse_iterator* *std::__debug::list*< _Tp, _Allocator >::rend
() const= [inline]=, = [noexcept]=
Definition at line *255* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
*reverse_iterator* *std::__debug::list*< _Tp, _Allocator >::rend
()= [inline]=, = [noexcept]=
Definition at line *251* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
void *std::__debug::list*< _Tp, _Allocator >::resize (size_type
__sz)= [inline]=
Definition at line *283* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
void *std::__debug::list*< _Tp, _Allocator >::resize (size_type __sz,
const _Tp & __c)= [inline]=
Definition at line *308* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
void *std::__debug::list*< _Tp, _Allocator >::sort ()= [inline]=
Definition at line *885* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
template<typename _StrictWeakOrdering > void *std::__debug::list*< _Tp,
_Allocator >::sort (_StrictWeakOrdering __pred)= [inline]=
Definition at line *889* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
void *std::__debug::list*< _Tp, _Allocator >::splice (*const_iterator*
__position, *list*< _Tp, _Allocator > && __x)= [inline]=, = [noexcept]=
Definition at line *569* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
void *std::__debug::list*< _Tp, _Allocator >::splice (*const_iterator*
__position, *list*< _Tp, _Allocator > && __x, *const_iterator* __first,
*const_iterator* __last)= [inline]=, = [noexcept]=
Definition at line *621* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
void *std::__debug::list*< _Tp, _Allocator >::splice (*const_iterator*
__position, *list*< _Tp, _Allocator > && __x, *const_iterator*
__i)= [inline]=, = [noexcept]=
Definition at line *589* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
void *std::__debug::list*< _Tp, _Allocator >::splice (*const_iterator*
__position, *list*< _Tp, _Allocator > & __x)= [inline]=, = [noexcept]=
Definition at line *583* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
void *std::__debug::list*< _Tp, _Allocator >::splice (*const_iterator*
__position, *list*< _Tp, _Allocator > & __x, *const_iterator* __first,
*const_iterator* __last)= [inline]=, = [noexcept]=
Definition at line *663* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
void *std::__debug::list*< _Tp, _Allocator >::splice (*const_iterator*
__position, *list*< _Tp, _Allocator > & __x, *const_iterator*
__i)= [inline]=, = [noexcept]=
Definition at line *615* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
void *std::__debug::list*< _Tp, _Allocator >::swap (*list*< _Tp,
_Allocator > & __x)= [inline]=, = [noexcept]=
Definition at line *552* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
__remove_return_type *std::__debug::list*< _Tp, _Allocator >::unique
()= [inline]=
Definition at line *756* of file *debug/list*.

** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
template<class _BinaryPredicate > __remove_return_type
*std::__debug::list*< _Tp, _Allocator >::unique (_BinaryPredicate
__binary_pred)= [inline]=
Definition at line *793* of file *debug/list*.

* Friends And Related Function Documentation
** template<typename _Tp , typename _Allocator = std::allocator<_Tp>>
template<typename _ItT , typename _SeqT , typename _CatT > friend class
::*__gnu_debug::_Safe_iterator*= [friend]=
Definition at line *66* of file *debug/list*.

* Member Data Documentation
** _Safe_iterator_base*
__gnu_debug::_Safe_sequence_base::_M_const_iterators= [inherited]=
The list of constant iterators that reference this container.

Definition at line *197* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** _Safe_iterator_base*
__gnu_debug::_Safe_sequence_base::_M_iterators= [inherited]=
The list of mutable iterators that reference this container.

Definition at line *194* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** unsigned int
__gnu_debug::_Safe_sequence_base::_M_version= [mutable]=, = [inherited]=
The container version number. This number may never be 0.

Definition at line *200* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence_base::_M_invalidate_all()*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
