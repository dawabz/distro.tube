#+TITLE: Manpages - afGetDataOffset.3
#+DESCRIPTION: Linux manpage for afGetDataOffset.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about afGetDataOffset.3 is found in manpage for: [[../afGetFrameCount.3][afGetFrameCount.3]]