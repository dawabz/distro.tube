#+TITLE: Manpages - XGetAtomNames.3
#+DESCRIPTION: Linux manpage for XGetAtomNames.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XGetAtomNames.3 is found in manpage for: [[../man3/XInternAtom.3][man3/XInternAtom.3]]