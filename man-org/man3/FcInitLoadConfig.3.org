#+TITLE: Manpages - FcInitLoadConfig.3
#+DESCRIPTION: Linux manpage for FcInitLoadConfig.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcInitLoadConfig - load configuration

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcConfig * FcInitLoadConfig (void*);*

* DESCRIPTION
Loads the default configuration file and returns the resulting
configuration. Does not load any font information.
