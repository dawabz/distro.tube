#+TITLE: Manpages - xcb_xinerama_query_screens_screen_info_iterator.3
#+DESCRIPTION: Linux manpage for xcb_xinerama_query_screens_screen_info_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_xinerama_query_screens_screen_info_iterator.3 is found in manpage for: [[../man3/xcb_xinerama_query_screens.3][man3/xcb_xinerama_query_screens.3]]