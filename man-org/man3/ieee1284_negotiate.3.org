#+TITLE: Manpages - ieee1284_negotiate.3
#+DESCRIPTION: Linux manpage for ieee1284_negotiate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ieee1284_negotiate, ieee1284_terminate - IEEE 1284 negotiation

* SYNOPSIS
#+begin_example
  #include <ieee1284.h>
#+end_example

*int ieee1284_negotiate(struct parport **/port/*, int */mode/*);*

*void ieee1284_terminate(struct parport **/port/*);*

* DESCRIPTION
These functions are for negotiating to and terminating from IEEE 1284
data transfer modes. The default mode is called compatibility mode, or
in other words normal printer protocol. It is a host-to-peripheral mode
only. There are special modes that allow peripheral-to-host transfer as
well, which may be negotiated to using *ieee1284_negotiate*. IEEE 1284
negotiation is a process by which the host requests a transfer mode and
the peripheral accepts or rejects it. An IEEE 1284-compliant device will
require a successful negotiation to a particular mode before it is used
for data transfer (but simpler devices may not if they only speak one
transfer mode).

To terminate the special mode and go back to compatilibity mode use
*ieee1284_terminate*.

These functions act on the parallel port associated with /port/, which
must be claimed.

With a device strictly complying to IEEE 1284 you will need to call
*ieee1284_terminate* in between any two calls to *ieee1284_negotiate*
for modes other than *M1284_COMPAT*.

* AVAILABLE MODES
** Uni-directional modes

#+begin_quote
  ·

  *M1284_COMPAT*: Compatibility mode. Normal printer protocol. This is
  not a negotiated mode, but is the default mode in absence of
  negotiation. *ieee1284_negotiate(port, M1284_COMPAT)* is equivalent to
  *ieee1284_terminate(port)*. This host-to-peripheral mode is used for
  sending data to printers, and is historically the mode that has been
  used for that before IEEE 1284.
#+end_quote

#+begin_quote
  ·

  *M1284_NIBBLE*: Nibble mode. This peripheral-to-host mode uses the
  status lines to read data from the peripheral four bits at a time.
#+end_quote

#+begin_quote
  ·

  *M1284_BYTE*: Byte mode. This peripheral-to-host mode uses the data
  lines in reverse mode to read data from the peripheral a byte at a
  time.
#+end_quote

** Bi-directional modes

#+begin_quote
  ·

  *M1284_ECP*: ECP mode. On entry to ECP mode it is a host-to-peripheral
  (i.e. forward) mode, but it may be set to reverse mode using
  *ieee1284_ecp_fwd_to_rev*(3). It is common for PC hardware to provide
  assistance with this mode by the use of a FIFO which the host (or, in
  reverse mode, the peripheral) may fill, so that the hardware can do
  the handshaking itself.
#+end_quote

#+begin_quote
  ·

  *M1284_EPP*: EPP mode. In this bi-directional mode the direction of
  data transfer is signalled at each byte.
#+end_quote

** Mode variations

#+begin_quote
  ·

  *M1284_FLAG_DEVICEID*: Device ID retrieval. This flag may be combined
  with a nibble, byte, or ECP mode to notify the device that it should
  send its IEEE 1284 Device ID when asked for data.
#+end_quote

#+begin_quote
  ·

  *M1284_BECP*: Bounded ECP is a modification to ECP that makes it more
  robust at the point that the direction is changed. (Unfortunately it
  is not yet implemented in the Linux kernel driver.)
#+end_quote

#+begin_quote
  ·

  *M1284_ECPRLE*: ECP with run length encoding. In this mode,
  consecutive data bytes of the same value may be transferred in only a
  few cycles.
#+end_quote

* RETURN VALUE
*E1284_OK*

#+begin_quote
  The negotiation was successful.
#+end_quote

*E1284_NOTAVAIL*

#+begin_quote
  Negotiation is not available with this port type.
#+end_quote

*E1284_REJECTED*

#+begin_quote
  Negotiation was rejected by the peripheral.
#+end_quote

*E1284_NEGFAILED*

#+begin_quote
  Negotiation failed for some reason. Perhaps the device is not IEEE
  1284 compliant.
#+end_quote

*E1284_SYS*

#+begin_quote
  A system error occured during negotiation.
#+end_quote

*E1284_INVALIDPORT*

#+begin_quote
  The /port/ parameter is invalid (for instance, perhaps the /port/ is
  not claimed).
#+end_quote

* AUTHOR
*Tim Waugh* <twaugh@redhat.com>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2001-2003 Tim Waugh\\
