#+TITLE: Manpages - pcre_version.3
#+DESCRIPTION: Linux manpage for pcre_version.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE - Perl-compatible regular expressions

* SYNOPSIS
*#include <pcre.h>*

*const char *pcre_version(void);*

*const char *pcre16_version(void);*

*const char *pcre32_version(void);*

* DESCRIPTION
This function (even in the 16-bit and 32-bit libraries) returns a
zero-terminated, 8-bit character string that gives the version number of
the PCRE library and the date of its release.

There is a complete description of the PCRE native API in the *pcreapi*
page and a description of the POSIX API in the *pcreposix* page.
