#+TITLE: Manpages - std___detail__Power2_rehash_policy.3
#+DESCRIPTION: Linux manpage for std___detail__Power2_rehash_policy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Power2_rehash_policy - Rehash policy providing power of
2 bucket numbers. Avoids modulo operations.

* SYNOPSIS
\\

=#include <hashtable_policy.h>=

** Public Types
using *__has_load_factor* = *true_type*\\

typedef std::size_t *_State*\\

** Public Member Functions
*_Power2_rehash_policy* (float __z=1.0) noexcept\\

std::size_t *_M_bkt_for_elements* (std::size_t __n) const noexcept\\

*std::pair*< bool, std::size_t > *_M_need_rehash* (std::size_t __n_bkt,
std::size_t __n_elt, std::size_t __n_ins) noexcept\\

std::size_t *_M_next_bkt* (std::size_t __n) noexcept\\

void *_M_reset* () noexcept\\

void *_M_reset* (_State __state) noexcept\\

_State *_M_state* () const noexcept\\

float *max_load_factor* () const noexcept\\

** Public Attributes
float *_M_max_load_factor*\\

std::size_t *_M_next_resize*\\

** Static Public Attributes
static const std::size_t *_S_growth_factor*\\

* Detailed Description
Rehash policy providing power of 2 bucket numbers. Avoids modulo
operations.

Definition at line *519* of file *hashtable_policy.h*.

* Member Typedef Documentation
** using *std::__detail::_Power2_rehash_policy::__has_load_factor* =
*true_type*
Definition at line *521* of file *hashtable_policy.h*.

** typedef std::size_t std::__detail::_Power2_rehash_policy::_State
Definition at line *599* of file *hashtable_policy.h*.

* Constructor & Destructor Documentation
** std::__detail::_Power2_rehash_policy::_Power2_rehash_policy (float
__z = =1.0=)= [inline]=, = [noexcept]=
Definition at line *523* of file *hashtable_policy.h*.

* Member Function Documentation
** std::size_t std::__detail::_Power2_rehash_policy::_M_bkt_for_elements
(std::size_t __n) const= [inline]=, = [noexcept]=
Definition at line *567* of file *hashtable_policy.h*.

** *std::pair*< bool, std::size_t >
std::__detail::_Power2_rehash_policy::_M_need_rehash (std::size_t
__n_bkt, std::size_t __n_elt, std::size_t __n_ins)= [inline]=,
= [noexcept]=
Definition at line *575* of file *hashtable_policy.h*.

** std::size_t std::__detail::_Power2_rehash_policy::_M_next_bkt
(std::size_t __n)= [inline]=, = [noexcept]=
Definition at line *533* of file *hashtable_policy.h*.

** void std::__detail::_Power2_rehash_policy::_M_reset ()= [inline]=,
= [noexcept]=
Definition at line *606* of file *hashtable_policy.h*.

** void std::__detail::_Power2_rehash_policy::_M_reset (_State
__state)= [inline]=, = [noexcept]=
Definition at line *610* of file *hashtable_policy.h*.

** _State std::__detail::_Power2_rehash_policy::_M_state ()
const= [inline]=, = [noexcept]=
Definition at line *602* of file *hashtable_policy.h*.

** float std::__detail::_Power2_rehash_policy::max_load_factor ()
const= [inline]=, = [noexcept]=
Definition at line *527* of file *hashtable_policy.h*.

* Member Data Documentation
** float std::__detail::_Power2_rehash_policy::_M_max_load_factor
Definition at line *615* of file *hashtable_policy.h*.

** std::size_t std::__detail::_Power2_rehash_policy::_M_next_resize
Definition at line *616* of file *hashtable_policy.h*.

** const std::size_t
std::__detail::_Power2_rehash_policy::_S_growth_factor= [static]=
Definition at line *613* of file *hashtable_policy.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
