#+TITLE: Manpages - SDL_JoystickNumHats.3
#+DESCRIPTION: Linux manpage for SDL_JoystickNumHats.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_JoystickNumHats - Get the number of joystick hats

* SYNOPSIS
*#include "SDL.h"*

*int SDL_JoystickNumHats*(*SDL_Joystick *joystick*);

* DESCRIPTION
Return the number of hats available from a previously opened
*SDL_Joystick*.

* RETURN VALUE
Number of hats.

* SEE ALSO
*SDL_JoystickGetHat*, *SDL_JoystickOpen*
