#+TITLE: Manpages - MPI_Win_detach.3
#+DESCRIPTION: Linux manpage for MPI_Win_detach.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about MPI_Win_detach.3 is found in manpage for: [[../man3/MPI_Win_attach.3][man3/MPI_Win_attach.3]]