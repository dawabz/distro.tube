#+TITLE: Manpages - XSetWindowBackgroundPixmap.3
#+DESCRIPTION: Linux manpage for XSetWindowBackgroundPixmap.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XSetWindowBackgroundPixmap.3 is found in manpage for: [[../man3/XChangeWindowAttributes.3][man3/XChangeWindowAttributes.3]]