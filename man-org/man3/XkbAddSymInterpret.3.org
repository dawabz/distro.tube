#+TITLE: Manpages - XkbAddSymInterpret.3
#+DESCRIPTION: Linux manpage for XkbAddSymInterpret.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbAddSymInterpret - Add a symbol interpretation to the list of symbol
interpretations in an XkbCompatRec

* SYNOPSIS
*XkbSymInterpretPtr XkbAddSymInterpret* *( XkbDescPtr */xkb/* ,*
*XkbSymInterpretPtr */si/* ,* *Bool */updateMap/* ,* *XkbChangesPtr
*/changes/* );*

* ARGUMENTS
- /- xkb/ :: keyboard description to be updated

- /- si/ :: symbol interpretation to be added

- /- updateMap/ :: True=>apply compatibility map to keys

- /- changes/ :: changes are put here

* DESCRIPTION
/XkbAddSymInterpret/ adds /si/ to the list of symbol interpretations in
/xkb./ If /updateMap/ is True, it (re)applies the compatibility map to
all of the keys on the keyboard. If /changes/ is non-NULL, it reports
the parts of the keyboard that were affected (unless /updateMap/ is
True, not much changes). /XkbAddSymInterpret/ returns a pointer to the
actual new symbol interpretation in the list or NULL if it failed.
