#+TITLE: Manpages - std__List_const_iterator.3
#+DESCRIPTION: Linux manpage for std__List_const_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_List_const_iterator< _Tp > - A list::const_iterator.

* SYNOPSIS
\\

=#include <stl_list.h>=

** Public Types
typedef const *_List_node*< _Tp > *_Node*\\

typedef *_List_const_iterator*< _Tp > *_Self*\\

typedef ptrdiff_t *difference_type*\\

typedef *_List_iterator*< _Tp > *iterator*\\

typedef *std::bidirectional_iterator_tag* *iterator_category*\\

typedef const _Tp * *pointer*\\

typedef const _Tp & *reference*\\

typedef _Tp *value_type*\\

** Public Member Functions
*_List_const_iterator* (const *__detail::_List_node_base* *__x)
noexcept\\

*_List_const_iterator* (const *iterator* &__x) noexcept\\

*iterator* *_M_const_cast* () const noexcept\\

reference *operator** () const noexcept\\

*_Self* & *operator++* () noexcept\\

*_Self* *operator++* (int) noexcept\\

*_Self* & *operator--* () noexcept\\

*_Self* *operator--* (int) noexcept\\

pointer *operator->* () const noexcept\\

** Public Attributes
const *__detail::_List_node_base* * *_M_node*\\

** Friends
bool *operator!=* (const *_Self* &__x, const *_Self* &__y) noexcept\\

bool *operator==* (const *_Self* &__x, const *_Self* &__y) noexcept\\

* Detailed Description
** "template<typename _Tp>
\\
struct std::_List_const_iterator< _Tp >"A list::const_iterator.

All the functions are op overloads.

Definition at line *266* of file *stl_list.h*.

* Member Typedef Documentation
** template<typename _Tp > typedef const *_List_node*<_Tp>
*std::_List_const_iterator*< _Tp >::*_Node*
Definition at line *269* of file *stl_list.h*.

** template<typename _Tp > typedef *_List_const_iterator*<_Tp>
*std::_List_const_iterator*< _Tp >::*_Self*
Definition at line *268* of file *stl_list.h*.

** template<typename _Tp > typedef ptrdiff_t
*std::_List_const_iterator*< _Tp >::difference_type
Definition at line *272* of file *stl_list.h*.

** template<typename _Tp > typedef *_List_iterator*<_Tp>
*std::_List_const_iterator*< _Tp >::*iterator*
Definition at line *270* of file *stl_list.h*.

** template<typename _Tp > typedef *std::bidirectional_iterator_tag*
*std::_List_const_iterator*< _Tp >::*iterator_category*
Definition at line *273* of file *stl_list.h*.

** template<typename _Tp > typedef const _Tp*
*std::_List_const_iterator*< _Tp >::pointer
Definition at line *275* of file *stl_list.h*.

** template<typename _Tp > typedef const _Tp&
*std::_List_const_iterator*< _Tp >::reference
Definition at line *276* of file *stl_list.h*.

** template<typename _Tp > typedef _Tp *std::_List_const_iterator*< _Tp
>::value_type
Definition at line *274* of file *stl_list.h*.

* Constructor & Destructor Documentation
** template<typename _Tp > *std::_List_const_iterator*< _Tp
>::*_List_const_iterator* ()= [inline]=, = [noexcept]=
Definition at line *278* of file *stl_list.h*.

** template<typename _Tp > *std::_List_const_iterator*< _Tp
>::*_List_const_iterator* (const *__detail::_List_node_base* *
__x)= [inline]=, = [explicit]=, = [noexcept]=
Definition at line *282* of file *stl_list.h*.

** template<typename _Tp > *std::_List_const_iterator*< _Tp
>::*_List_const_iterator* (const *iterator* & __x)= [inline]=,
= [noexcept]=
Definition at line *286* of file *stl_list.h*.

* Member Function Documentation
** template<typename _Tp > *iterator* *std::_List_const_iterator*< _Tp
>::_M_const_cast () const= [inline]=, = [noexcept]=
Definition at line *290* of file *stl_list.h*.

** template<typename _Tp > reference *std::_List_const_iterator*< _Tp
>::operator* () const= [inline]=, = [noexcept]=
Definition at line *295* of file *stl_list.h*.

** template<typename _Tp > *_Self* & *std::_List_const_iterator*< _Tp
>::operator++ ()= [inline]=, = [noexcept]=
Definition at line *303* of file *stl_list.h*.

** template<typename _Tp > *_Self* *std::_List_const_iterator*< _Tp
>::operator++ (int)= [inline]=, = [noexcept]=
Definition at line *310* of file *stl_list.h*.

** template<typename _Tp > *_Self* & *std::_List_const_iterator*< _Tp
>::operator-- ()= [inline]=, = [noexcept]=
Definition at line *318* of file *stl_list.h*.

** template<typename _Tp > *_Self* *std::_List_const_iterator*< _Tp
>::operator-- (int)= [inline]=, = [noexcept]=
Definition at line *325* of file *stl_list.h*.

** template<typename _Tp > pointer *std::_List_const_iterator*< _Tp
>::operator-> () const= [inline]=, = [noexcept]=
Definition at line *299* of file *stl_list.h*.

* Friends And Related Function Documentation
** template<typename _Tp > bool operator!= (const *_Self* & __x, const
*_Self* & __y)= [friend]=
Definition at line *338* of file *stl_list.h*.

** template<typename _Tp > bool operator== (const *_Self* & __x, const
*_Self* & __y)= [friend]=
Definition at line *333* of file *stl_list.h*.

* Member Data Documentation
** template<typename _Tp > const *__detail::_List_node_base**
*std::_List_const_iterator*< _Tp >::_M_node
Definition at line *343* of file *stl_list.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
