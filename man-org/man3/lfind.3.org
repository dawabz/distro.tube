#+TITLE: Manpages - lfind.3
#+DESCRIPTION: Linux manpage for lfind.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about lfind.3 is found in manpage for: [[../man3/lsearch.3][man3/lsearch.3]]