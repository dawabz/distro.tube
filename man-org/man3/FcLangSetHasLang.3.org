#+TITLE: Manpages - FcLangSetHasLang.3
#+DESCRIPTION: Linux manpage for FcLangSetHasLang.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcLangSetHasLang - test langset for language support

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcLangResult FcLangSetHasLang (const FcLangSet */ls/*, const FcChar8
**/lang/*);*

* DESCRIPTION
*FcLangSetHasLang* checks whether /ls/ supports /lang/. If /ls/ has a
matching language and territory pair, this function returns FcLangEqual.
If /ls/ has a matching language but differs in which territory that
language is for, this function returns FcLangDifferentTerritory. If /ls/
has no matching language, this function returns FcLangDifferentLang.
