#+TITLE: Manpages - SDL_WaitEvent.3
#+DESCRIPTION: Linux manpage for SDL_WaitEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_WaitEvent - Waits indefinitely for the next available event.

* SYNOPSIS
*#include "SDL.h"*

*int SDL_WaitEvent*(*SDL_Event *event*);

* DESCRIPTION
Waits indefinitely for the next available event, returning *1*, or *0*
if there was an error while waiting for events.

If *event* is not *NULL*, the next event is removed from the queue and
stored in that area.

* SEE ALSO
*SDL_Event*, *SDL_PollEvent*
