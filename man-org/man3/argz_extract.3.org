#+TITLE: Manpages - argz_extract.3
#+DESCRIPTION: Linux manpage for argz_extract.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about argz_extract.3 is found in manpage for: [[../man3/argz_add.3][man3/argz_add.3]]