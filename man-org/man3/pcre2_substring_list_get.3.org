#+TITLE: Manpages - pcre2_substring_list_get.3
#+DESCRIPTION: Linux manpage for pcre2_substring_list_get.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  int pcre2_substring_list_get(pcre2_match_data *match_data,
  " PCRE2_UCHAR ***listptr, PCRE2_SIZE **lengthsptr);
#+end_example

* DESCRIPTION
This is a convenience function for extracting all the captured
substrings after a pattern match. It builds a list of pointers to the
strings, and (optionally) a second list that contains their lengths (in
code units), excluding a terminating zero that is added to each of them.
All this is done in a single block of memory that is obtained using the
same memory allocation function that was used to get the match data
block. The convenience function *pcre2_substring_list_free()* can be
used to free it when it is no longer needed. The arguments are:

/match_data/ The match data block /listptr/ Where to put a pointer to
the list /lengthsptr/ Where to put a pointer to the lengths, or NULL

A pointer to a list of pointers is put in the variable whose address is
in /listptr/. The list is terminated by a NULL pointer. If /lengthsptr/
is not NULL, a matching list of lengths is created, and its address is
placed in /lengthsptr/. The yield of the function is zero on success or
PCRE2_ERROR_NOMEMORY if sufficient memory could not be obtained.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
