#+TITLE: Manpages - XtDestroyApplicationContext.3
#+DESCRIPTION: Linux manpage for XtDestroyApplicationContext.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtDestroyApplicationContext.3 is found in manpage for: [[../man3/XtCreateApplicationContext.3][man3/XtCreateApplicationContext.3]]