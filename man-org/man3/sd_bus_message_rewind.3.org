#+TITLE: Manpages - sd_bus_message_rewind.3
#+DESCRIPTION: Linux manpage for sd_bus_message_rewind.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_bus_message_rewind - Return to beginning of message or current
container

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

*int sd_bus_message_rewind(sd_bus_message **/m/*, int */complete/*);*

* DESCRIPTION
*sd_bus_message_rewind()* moves the "read pointer" in the message /m/ to
either the beginning of the message (if /complete/ is true) or to the
beginning of the currently open container. If no container is open,
/complete/ has no effect.

* RETURN VALUE
On success, this function returns 0 or a positive integer. The value is
zero if the current container or whole message in case no container is
open is empty, and positive otherwise. On failure, it returns a negative
errno-style error code.

** Errors
Returned errors may indicate the following problems:

*-EINVAL*

#+begin_quote
  The /m/ parameter is *NULL*.
#+end_quote

*-EPERM*

#+begin_quote
  The message /m/ has not been sealed.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-bus*(3), *sd_bus_message_read*(3)
