#+TITLE: Manpages - FcConfigSetSysRoot.3
#+DESCRIPTION: Linux manpage for FcConfigSetSysRoot.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigSetSysRoot - Set the system root directory

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcConfigSetSysRoot (FcConfig */config/*, const FcChar8
**/sysroot/*);*

* DESCRIPTION
Set 'sysroot' as the system root directory. All file paths used or
created with this 'config' (including file properties in patterns) will
be considered or made relative to this 'sysroot'. This allows a host to
generate caches for targets at build time. This also allows a cache to
be re-targeted to a different base directory if 'FcConfigGetSysRoot' is
used to resolve file paths. When setting this on the current config this
causes changing current config (calls FcConfigSetCurrent()).

* SINCE
version 2.10.92
