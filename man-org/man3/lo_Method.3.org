#+TITLE: Manpages - lo_Method.3
#+DESCRIPTION: Linux manpage for lo_Method.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lo::Method - Class representing an OSC method, proxy for *lo_method*.

* SYNOPSIS
\\

* Detailed Description
Class representing an OSC method, proxy for *lo_method*.

Definition at line 136 of file lo_cpp.h.

* Author
Generated automatically by Doxygen for liblo from the source code.
