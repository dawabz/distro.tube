#+TITLE: Manpages - zip_fseek.3
#+DESCRIPTION: Linux manpage for zip_fseek.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function seeks to the specified

relative to

just like

only works on uncompressed (stored) data. When called on compressed data
it will return an error.

If successful,

returns 0. Otherwise, -1 is returned.

was added in libzip 1.2.0.

and
