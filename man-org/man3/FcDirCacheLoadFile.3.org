#+TITLE: Manpages - FcDirCacheLoadFile.3
#+DESCRIPTION: Linux manpage for FcDirCacheLoadFile.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcDirCacheLoadFile - load a cache file

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcCache * FcDirCacheLoadFile (const FcChar8 */cache_file/*, struct stat
**/file_stat/*);*

* DESCRIPTION
This function loads a directory cache from /cache_file/. If /file_stat/
is non-NULL, it will be filled with the results of stat(2) on the cache
file.
