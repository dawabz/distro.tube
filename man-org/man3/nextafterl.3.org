#+TITLE: Manpages - nextafterl.3
#+DESCRIPTION: Linux manpage for nextafterl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about nextafterl.3 is found in manpage for: [[../man3/nextafter.3][man3/nextafter.3]]