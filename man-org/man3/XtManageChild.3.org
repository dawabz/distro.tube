#+TITLE: Manpages - XtManageChild.3
#+DESCRIPTION: Linux manpage for XtManageChild.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtManageChild.3 is found in manpage for: [[../man3/XtManageChildren.3][man3/XtManageChildren.3]]