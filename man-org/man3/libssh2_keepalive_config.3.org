#+TITLE: Manpages - libssh2_keepalive_config.3
#+DESCRIPTION: Linux manpage for libssh2_keepalive_config.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_keepalive_config - short function description

* SYNOPSIS
#include <libssh2.h>

#+begin_example

  void libssh2_keepalive_config(LIBSSH2_SESSION *session,
                                int want_reply,
                                unsigned interval);
#+end_example

* DESCRIPTION
Set how often keepalive messages should be sent. *want_reply* indicates
whether the keepalive messages should request a response from the
server. *interval* is number of seconds that can pass without any I/O,
use 0 (the default) to disable keepalives. To avoid some busy-loop
corner-cases, if you specify an interval of 1 it will be treated as 2.

Note that non-blocking applications are responsible for sending the
keepalive messages using *libssh2_keepalive_send(3)*.

* RETURN VALUE
Nothing

* AVAILABILITY
Added in libssh2 1.2.5

* SEE ALSO
*libssh2_keepalive_send(3)*
