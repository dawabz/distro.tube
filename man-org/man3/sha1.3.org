#+TITLE: Manpages - sha1.3
#+DESCRIPTION: Linux manpage for sha1.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The SHA1 functions implement the NIST Secure Hash Algorithm (SHA-1),
FIPS PUB 180-1. SHA-1 is used to generate a condensed representation of
a message called a message digest. The algorithm takes a message less
than 2^64 bits as input and produces a 160-bit digest suitable for use
as a digital signature.

While the SHA1 functions are considered to be more secure than the

and

functions with which they share a similar interface, they are considered
insecure as of 2005, and as of 2020 chosen-prefix attacks have become
practical, thus these must not be used in cryptographic contexts.

The

function initializes a SHA1_CTX

for use with

and

The

function adds

of length

to the SHA1_CTX specified by

is called when all data has been added via

and stores a message digest in the

parameter.

The

function can be used to apply padding to the message digest as in

but the current context can still be used with

The

function is used by

to hash 512-bit blocks and forms the core of the algorithm. Most
programs should use the interface provided by

and

instead of calling

directly.

The

function is a front end for

which converts the digest into an

representation of the 160 bit digest in hexadecimal.

The

function calculates the digest for a file and returns the result via

If

is unable to open the file a NULL pointer is returned.

behaves like

but calculates the digest only for that portion of the file starting at

and continuing for

bytes or until end of file is reached, whichever comes first. A zero

can be specified to read until end of file. A negative

or

will be ignored.

The

function calculates the digest of an arbitrary string and returns the
result via

For each of the

and

functions the

parameter should either be a string of at least 41 characters in size or
a NULL pointer. In the latter case, space will be dynamically allocated
via

and should be freed using

when it is no longer needed.

The follow code fragment will calculate the digest for the string "abc"
which is ``0xa9993e364706816aba3e25717850c26c9cd0d89d''.

SHA1_CTX sha; uint8_t results[SHA1_DIGEST_LENGTH]; char *buf; int n;

buf = "abc"; n = strlen(buf); SHA1Init(&sha); SHA1Update(&sha, (uint8_t
*)buf, n); SHA1Final(results, &sha);

/* Print the digest as one long hex value */ printf("0x"); for (n = 0; n
< SHA1_DIGEST_LENGTH; n++) printf("%02x", results[n]); putchar('\n');

Alternately, the helper functions could be used in the following way:

uint8_t output[SHA1_DIGEST_STRING_LENGTH]; char *buf = "abc";

printf("0x%s\n", SHA1Data(buf, strlen(buf), output));

The SHA-1 functions appeared in

This implementation of SHA-1 was written by Steve Reid.

The

and

helper functions are derived from code written by Poul-Henning Kamp.

This implementation of SHA-1 has not been validated by NIST and as such
is not in official compliance with the standard.

If a message digest is to be copied to a multi-byte type (ie: an array
of five 32-bit integers) it will be necessary to perform byte swapping
on little endian machines such as the i386, alpha, and vax.
