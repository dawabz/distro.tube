#+TITLE: Manpages - SDL_CreateThread.3
#+DESCRIPTION: Linux manpage for SDL_CreateThread.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CreateThread - Creates a new thread of execution that shares its
parent's properties.

* SYNOPSIS
*#include "SDL.h"* #include "SDL_thread.h"

*SDL_Thread *SDL_CreateThread*(*int (*fn)(void *), void *data*);

* DESCRIPTION
*SDL_CreateThread* creates a new thread of execution that shares all of
its parent's global memory, signal handlers, file descriptors, etc, and
runs the function *fn* passed the void pointer *data* The thread quits
when this function returns.

* SEE ALSO
*SDL_KillThread*
