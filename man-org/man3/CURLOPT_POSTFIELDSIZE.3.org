#+TITLE: Manpages - CURLOPT_POSTFIELDSIZE.3
#+DESCRIPTION: Linux manpage for CURLOPT_POSTFIELDSIZE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_POSTFIELDSIZE - size of POST data pointed to

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_POSTFIELDSIZE, long
size);

* DESCRIPTION
If you want to post data to the server without having libcurl do a
strlen() to measure the data size, this option must be used. When this
option is used you can post fully binary data, which otherwise is likely
to fail. If this size is set to -1, the library will use strlen() to get
the size.

If you post more than 2GB, use /CURLOPT_POSTFIELDSIZE_LARGE(3)/.

* DEFAULT
-1

* PROTOCOLS
HTTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    const char *data = "data to send";

    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* size of the POST data */
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (long) strlen(data));

    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);

    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Along with HTTP

* RETURN VALUE
Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

* SEE ALSO
*CURLOPT_POSTFIELDS*(3), *CURLOPT_POSTFIELDSIZE_LARGE*(3),
