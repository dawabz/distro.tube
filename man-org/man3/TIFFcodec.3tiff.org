#+TITLE: Manpages - TIFFcodec.3tiff
#+DESCRIPTION: Linux manpage for TIFFcodec.3tiff
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
TIFFFindCODEC, TIFFRegisterCODEC, TIFFUnRegisterCODEC,
TIFFIsCODECConfigured - codec-related utility routines

* SYNOPSIS
*#include <tiffio.h>*

*const TIFFCodec* TIFFFindCODEC(uint16_t */scheme/*);*\\
*TIFFCodec* TIFFRegisterCODEC(uint16_t */scheme/*, const char
**/method/*, TIFFInitMethod */init/*);*\\
*void TIFFUnRegisterCODEC(TIFFCodec **/codec/*);*\\
*int TIFFIsCODECConfigured(uint16_t */scheme/*);*

* DESCRIPTION
/libtiff/ supports a variety of compression schemes implemented by
software /codecs/. Each codec adheres to a modular interface that
provides for the decoding and encoding of image data; as well as some
other methods for initialization, setup, cleanup, and the control of
default strip and tile sizes. Codecs are identified by the associated
value of the

/Compression/ tag; e.g. 5 for

compression.

The /TIFFRegisterCODEC/ routine can be used to augment or override the
set of codecs available to an application. If the specified /scheme/
already has a registered codec then it is /overridden/ and any images
with data encoded with this compression scheme will be decoded using the
supplied codec.

/TIFFIsCODECConfigured/ returns 1 if the codec is configured and
working. Otherwise 0 will be returned.

* DIAGNOSTICS
*No space to register compression scheme %s*. /TIFFRegisterCODEC/ was
unable to allocate memory for the data structures needed to register a
codec.

*Cannot remove compression scheme %s; not registered*.
/TIFFUnRegisterCODEC/ did not locate the specified codec in the table of
registered compression schemes.

* SEE ALSO
*libtiff*(3TIFF)

Libtiff library home page: *http://www.simplesystems.org/libtiff/*
