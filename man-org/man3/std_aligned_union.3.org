#+TITLE: Manpages - std_aligned_union.3
#+DESCRIPTION: Linux manpage for std_aligned_union.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::aligned_union< _Len, _Types > - Provide aligned storage for types.

* SYNOPSIS
\\

** Public Types
typedef *aligned_storage*< _S_len, *alignment_value* >::*type* *type*\\
The storage.

** Static Public Attributes
static const size_t *alignment_value*\\
The value of the strictest alignment of _Types.

* Detailed Description
** "template<size_t _Len, typename... _Types>
\\
struct std::aligned_union< _Len, _Types >"Provide aligned storage for
types.

[meta.trans.other]

Provides aligned storage for any of the provided types of at least size
_Len.

*See also*

#+begin_quote
  aligned_storage
#+end_quote

Definition at line *2067* of file *std/type_traits*.

* Member Typedef Documentation
** template<size_t _Len, typename... _Types> typedef
*aligned_storage*<_S_len,*alignment_value*>::*type*
*std::aligned_union*< _Len, _Types >::*type*
The storage.

Definition at line *2079* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
