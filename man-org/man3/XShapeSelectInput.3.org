#+TITLE: Manpages - XShapeSelectInput.3
#+DESCRIPTION: Linux manpage for XShapeSelectInput.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XShapeSelectInput.3 is found in manpage for: [[../man3/XShape.3][man3/XShape.3]]