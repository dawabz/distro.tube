#+TITLE: Manpages - libsystem.3
#+DESCRIPTION: Linux manpage for libsystem.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* Name
pm_system - run a Netpbm program with program input and output

* Synopsis
#+begin_example
  #include <netpbm/pm_system.h>

  pm_system(void                  stdinFeeder(int, void *),
            void *          const feederParm,
            void                  stdoutAccepter(int, void *),
            void *          const accepterParm,
            const char *    const shellCommand);

  pm_system_lp(const char *  const progName,
               void                stdinFeeder(int, void *),
               void *        const feederParm,
               void                stdoutAccepter(int, void *),
               void *        const accepterParm,
               ...);

  pm_system_vp(const char *  const progName,
               const char ** const argArray,
               void                stdinFeeder(int, void *),
               void *        const feederParm,
               void                stdoutAccepter(int, void *),
               void *        const accepterParm);
#+end_example

* Example
This simple example converts a PNM image on Standard Input to a JFIF
(JPEG) image on Standard Output. In this case, *pm_system()* is doing no
more than *system()* would do.

#+begin_example
      pm_system(NULL, NULL, NULL, NULL, "pnmtojpeg");
#+end_example

This example does the same thing, but moves the data through memory
buffers to illustrate use with memory buffers, and we throw in a stage
to shrink the image too:

#+begin_example
  #include <netpbm/pm_system.h>

  char              pnmData[100*1024];   /* Input file better be < 100K */
  char              jfifData[100*1024];
  struct bufferDesc pnmBuffer;
  struct bufferDesc jfifBuffer;
  unsigned int      jfifSize;

  pnmBuffer.size = fread(pnmData, 1, sizeof(pnmData), stdin);
  pnmBuffer.buffer = pnmData;
  pnmBuffer.bytesTransferredP = NULL;

  jfifBuffer.size = sizeof(jfifData);
  jfifBuffer.buffer = jfifData;
  jfifBuffer.bytesTransferredP = &jfifSize; 

  pm_system(&pm_feed_from_memory, &pnmBuffer,
            &pm_accept_to_memory, &jfifBuffer,
            "pamscale .5 | pnmtojpeg");

  fwrite(jfifData, 1, jfifSize, stdout);
#+end_example

This example reads an image into libnetpbm PAM structures, then
brightens it, then writes it out, to illustrate use of *pm_system* with
PAM structures.

#+begin_example
  #include <netpbm/pam.h>
  #include <netpbm/pm_system.h>

  struct pam       inpam;
  struct pam       outpam;
  tuple **         inTuples;
  tuple **         outTuples;
  struct pamtuples inPamtuples;
  struct pamtuples outPamtuples;

  inTuples = pnm_readpam(stdin, &inpam, sizeof(inpam));

  outpam = inpam;

  inPamtuples.pamP = &inpam;
  inPamtuples.tuplesP = &inTuples;
  outPamtuples.pamP = &outpam;
  outPamtuples.tuplesP = &outTuples;

  pm_system(&pm_feed_from_pamtuples, &inPamtuples,
            &pm_accept_to_pamtuples, &outPamtuples,
            "ppmbrighten -v 100");

  outpam.file = stdout;
  pnm_writepam(&outpam, outTuples);
#+end_example

* DESCRIPTION
These library functions are part of *Netpbm*(1)

*pm_system()* is a lot like the standard C library *system()*
subroutine. It runs a shell and has that shell execute a shell command
that you specify. But *pm_system()* gives you more control over the
Standard Input and Standard Output of that shell command than
*system()*. *system()* passes to the shell command as Standard Input and
Output whatever is the Standard Input and Output of the process that
calls *system()*. But with *pm_system()*, you specify as arguments
subroutines to execute to generate the shell command's Standard Input
stream and to process the shell command's Standard Output stream.

Your Standard Input feeder subroutine can generate the stream in
limitless ways. *pm_system()* gives it a file descriptor of a pipe to
which to write the stream it generates. *pm_system()* hooks up the other
end of that pipe to the shell command's Standard Input.

Likewise, your Standard Output accepter subroutine can do anything it
wants with the stream it gets. *pm_system()* gives it a file descriptor
of a pipe from which to read the stream. *pm_system()* hooks up the
other end of that pipe to the shell command's Standard Output.

The argument /stdinFeeder/ is a function pointer that identifies your
Standard Input feeder subroutine. *pm_system()* runs it in a child
process and waits for that process to terminate (and accepts its
completion status) before returning. /feederParm/ is the argument that
*pm_system()* passes to the subroutine; it is opaque to *pm_system()*.

If you pass /stdinFeeder/ = NULL, *pm_system()* simply passes your
current Standard Input stream to the shell command (as *system()* would
do), and does not create a child process.

The argument /stdoutAccepter/ is a function pointer that identifies your
Standard Output accepter subroutine. *pm_system()* calls it in the
current process. /accepterParm/ is an argument analogous to
/feederParm/.

If you pass /stdoutAccepter/ = NULL, *pm_system()* simply passes your
current Standard Output stream to the shell command (as *system()* would
do.

The argument /shellCommand/ is a null-terminated string containing the
shell command that the shell is to execute. It can be any command that
means something to the shell and can take a pipe for Standard Input and
Output. Example:

#+begin_example
      ppmbrighten -v 100 | pamdepth 255 | pamscale .5
#+end_example

*pm_system()* creates a child process to run the shell and waits for
that process to terminate (and accepts its completion status) before
returning.

** pm_system_lp()
*pm_system_lp()* is like *pm_system()* except that instead of running a
shell, which in turn typically runs another program, you run a program
of your choice directly.

Argument /progName/ identifies the program to run, the same way as with
*execlp()* or a shell command: if it contains a slash (/), it is the
full name of the file that contains the program. If not, it is a name to
be looked up in the system's program search path (determined by the PATH
environment variable).

You identify the arguments to the program the same way as for
*execlp()*: with the variable arguments at the end of the
*pm_system_lp()* argument list. Each is a NUL-terminated string. The
last argument /must/ be NULL to tell *pm_system_lp()* where the
arguments end.

Note that the first argument ('arg0') to a program is conventionally the
first word of the command used to run the program, as if it were being
run for a shell command. In other words, typically the name of the
program.

Example:

#+begin_example
      pm_system_lp('pnmtojpeg', NULL, NULL, NULL, NULL,
                   'pnmtojpeg', 'mypicture.jpg', '-quality=50', NULL);
#+end_example

*pm_system_lp()* is much safer than *pm_system()* when your program
computes the arguments or gets them from a user. If you build a shell
command using such arguments, unless you're really careful, you may end
up building a shell command that does something very different from what
you intended, because the argument could contain characters that mean
something to the shell such as '|'.

*pm_system_lp()* can also be considerably faster that *pm_system()*,
since it skips the whole running of the shell.

** pm_system_vp()
*pm_system_vp()* is like *pm_system_lp()* except that instead of
supplying the program arguments as variable arguments, you supply them
as an array, as with *execvp()*. A NULL element in the array identifies
the end of the arguments.

Example:

#+begin_example
      const char * argArray[3];

      argArray[0] = 'pnmtojpeg';
      argArray[1] = '-quality=50';
      argArray[2] = NULL;

      pm_system_vp('pnmtojpeg', argArray, NULL, NULL, NULL, NULL);
#+end_example

* Applications
The point of *pm_system()* and friends is to allow you to write a C
program that uses other programs internally, as a shell script would.
This is particularly desirable with Netpbm, because Netpbm consists of a
lot of programs that perform basic graphic manipulations and you'd like
to be able to build a program that does a more sophisticated graphic
manipulation by calling the more basic Netpbm programs. These building
block programs typically take input from Standard Input and write output
to Standard Output.

The obvious alternative is to use a higher level language -- Bourne
Shell or Perl, for example. But often you want your program to do
manipulations of your graphical data that are easier and more efficient
in C. Or you want to use the Netpbm subroutine library in your program.
The Netpbm subroutine library is a C-linkage library; the subroutines in
it are not usable from a Bourne Shell or Perl program.

A typical use of *pm_system()* is to place the contents of some
graphical image file in memory, run a Netpbm program against it, and
have what would ordinarily go into an output file in memory too, for
further processing. To do that, you can use the memory buffer Standard
Input feeder and Standard Output accepter described below.

If your program uses the Netpbm subroutine library to read, write, and
manipulate images, you may have an image in an array of PAM tuples. If
you want to manipulate that image with a Netpbm program (perhaps remap
the colors using *pnmremap*), you can use the pamtuple Standard Input
feeder and Standard Output acceptor described below.

* Broken Pipe Behavior
When you set up a shell command to take input from a pipe, as you do
with *pm_system()*, you need to understand how pipes work with respect
to the programs at either end of the pipe agreeing to how much data is
to be transferred. Here are some notes on that.

It is normal to read a pipe before the process on the other end has
written the data you hope to read, and it is normal to write to a pipe
before the process on the other end has tried to read your data. Writes
to a pipe can be buffered until the reading end requests the data. A
process reading or writing a pipe can block until the other end is
ready. Or a read or write can complete with an indication that the other
end is not ready at the moment and therefore no data, or less data than
was requested, was transferred.

The pipe is normally controlled by the writing end. When you read from a
pipe, you keep reading until the program on the other end of the pipe
closes it, and then you get an end-of-file indication. You then normally
close the reading end of the pipe, since it is no longer useful.

When you close the reading end of a pipe before getting the end-of-file
indication and the writer subsequently tries to write to the pipe, that
is an error condition for the writer. In a typical default Unix
environment, that error causes the writer to receive a SIGPIPE signal
and that signal causes the writer process to terminate abnormally. But
if, alternatively, the writer has ordered that SIGPIPE be blocked,
ignored, or handled, the signal does not cause the death of the writer.
Instead, the write operation simply completes with an error indication.

* Standard Feeders And Acceptors
You can supply anything you like as a Standard Input feeder or Standard
Output acceptor, but the Netpbm subroutine library comes with a few that
perform commonly needed functions.

** Memory Buffer
These routines are for when you just want to treat an area of memory as
a file. If the shell command would ordinarily read a 513 byte regular
file from its Standard Input, you want it to take 513 bytes from a
certain address in your process' memory. Whatever bytes the shell
command wants to write to its output file you want it to store at
another address in your process' memory.

The Standard Input feeder for this is called *pm_feed_from_memory*. The
Standard Output accepter is *pm_accept_to_memory*.

For both of these, the argument is the address of a *struct* bufferDesc,
which is defined as follows:

#+begin_example
  struct bufferDesc {
      unsigned int    size;
      unsigned char * buffer;
      unsigned int *  bytesTransferredP;
  };
#+end_example

/size/ is the size of the memory buffer and /buffer/ is its location in
memory (address). The Standard Input feeder will attempt to feed the
entire buffer to the shell command's Standard Input; the Standard Output
accepter will not accept any more data from the shell command's Standard
Output than will fit in the buffer. Both return the actual amount of
data read or written, in bytes, at the location identified by
/bytesTransferredP/. Unless *bytesTransferredP* is NULL.

Because a process typically terminates abnormally when it is not able to
write everything to a pipe that it wanted to, /bytesTransferredP/ is not
usually useful in the Standard Input feeder case.

** Pamtuple
These routines are for when you have images in memory in the data
structures used by the PAM family of subroutines in the Netpbm library
-- i.e. struct PAM and an array of struct tuple. With these routines,
you can run a Netpbm program against such an image just as you would
against the same image in a regular file.

The Standard Input feeder for this is called *pm_feed_from_pamtuples*.
The Standard Output accepter is *pm_accept_to_pamtuples*.

For both of these, the argument is the address of a *struct* pamtuples,
which is defined as follows:

#+begin_example
  struct pamtuples {
      struct pam * pamP;
      tuple ***    tuplesP;
  };
#+end_example

For the Standard Input feeder, you supply a struct pam, valid up through
the /tuple_type/ member (except it doesn't matter what the /file/ member
is) and array of tuples.

For the Standard Output Accepter, you supply only space in memory for
the struct pam and the address of the tuple array. The routine fills in
the struct pam up through the /tuple_type/ member (except leaves the
/file/ member undefined) and allocates space for the tuple array with
malloc(). You are responsible for freeing that memory.

* HISTORY
*pm_system()* was introduced in Netpbm 10.13 (January 2003).
