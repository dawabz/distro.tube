#+TITLE: Manpages - Math_BigInt_FastCalc.3perl
#+DESCRIPTION: Linux manpage for Math_BigInt_FastCalc.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Math::BigInt::FastCalc - Math::BigInt::Calc with some XS for more speed

* SYNOPSIS
# to use it with Math::BigInt use Math::BigInt lib => FastCalc; # to use
it with Math::BigFloat use Math::BigFloat lib => FastCalc; # to use it
with Math::BigRat use Math::BigRat lib => FastCalc;

* DESCRIPTION
Math::BigInt::FastCalc inherits from Math::BigInt::Calc.

Provides support for big integer calculations. Not intended to be used
by other modules. Other modules which sport the same functions can also
be used to support Math::BigInt, like Math::BigInt::GMP or
Math::BigInt::Pari.

In order to allow for multiple big integer libraries, Math::BigInt was
rewritten to use library modules for core math routines. Any module
which follows the same API as this can be used instead by using the
following:

use Math::BigInt lib => libname;

'libname' is either the long name ('Math::BigInt::Pari'), or only the
short version like 'Pari'. To use this library:

use Math::BigInt lib => FastCalc;

* STORAGE
Math::BigInt::FastCalc works exactly like Math::BigInt::Calc. Numbers
are stored in decimal form chopped into parts.

* METHODS
The following functions are now implemented in FastCalc.xs:

_is_odd _is_even _is_one _is_zero _is_two _is_ten _zero _one _two _ten
_acmp _len _inc _dec _ _strip_zeros _copy

* BUGS
Please report any bugs or feature requests to
=bug-math-bigint-fastcalc at rt.cpan.org=, or through the web interface
at <https://rt.cpan.org/Ticket/Create.html?Queue=Math-BigInt-FastCalc>
(requires login). We will be notified, and then you'll automatically be
notified of progress on your bug as I make changes.

* SUPPORT
You can find documentation for this module with the perldoc command.

perldoc Math::BigInt::FastCalc

You can also look for information at:

- RT: CPAN's request tracker
  <https://rt.cpan.org/Public/Dist/Display.html?Name=Math-BigInt-FastCalc>

- AnnoCPAN: Annotated CPAN documentation
  <http://annocpan.org/dist/Math-BigInt-FastCalc>

- CPAN Ratings <http://cpanratings.perl.org/dist/Math-BigInt-FastCalc>

- Search CPAN <http://search.cpan.org/dist/Math-BigInt-FastCalc/>

- CPAN Testers Matrix
  <http://matrix.cpantesters.org/?dist=Math-BigInt-FastCalc>

- The Bignum mailing list

  - Post to mailing list =bignum at lists.scsys.co.uk=

  - View mailing list <http://lists.scsys.co.uk/pipermail/bignum/>

  - Subscribe/Unsubscribe
    <http://lists.scsys.co.uk/cgi-bin/mailman/listinfo/bignum>

* LICENSE
This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

* AUTHORS
Original math code by Mark Biggar, rewritten by Tels
<http://bloodgate.com/> in late 2000. Separated from BigInt and shaped
API with the help of John Peacock.

Fixed, sped-up and enhanced by Tels http://bloodgate.com 2001-2003.
Further streamlining (api_version 1 etc.) by Tels 2004-2007.

Bug-fixing by Peter John Acklam <pjacklam@online.no> 2010-2016.

* SEE ALSO
Math::BigInt::Lib for a description of the API.

Alternative libraries Math::BigInt::Calc, Math::BigInt::GMP, and
Math::BigInt::Pari.

Some of the modules that use these libraries Math::BigInt,
Math::BigFloat, and Math::BigRat.
