#+TITLE: Manpages - ExtUtils_MM_OS390.3perl
#+DESCRIPTION: Linux manpage for ExtUtils_MM_OS390.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ExtUtils::MM_OS390 - OS390 specific subclass of ExtUtils::MM_Unix

* SYNOPSIS
Dont use this module directly. Use ExtUtils::MM and let it choose.

* DESCRIPTION
This is a subclass of ExtUtils::MM_Unix which contains functionality for
OS390.

Unless otherwise stated it works just like ExtUtils::MM_Unix.

** Overriden methods
- xs_make_dynamic_lib :: Defines the recipes for the =dynamic_lib=
  section.

* AUTHOR
Michael G Schwern <schwern@pobox.com> with code from ExtUtils::MM_Unix

* SEE ALSO
ExtUtils::MakeMaker
