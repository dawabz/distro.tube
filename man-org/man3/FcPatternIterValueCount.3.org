#+TITLE: Manpages - FcPatternIterValueCount.3
#+DESCRIPTION: Linux manpage for FcPatternIterValueCount.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcPatternIterValueCount - Returns the number of the values which the
iterator point to

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

int FcPatternIterValueCount (const FcPattern */p/*, FcPatternIter
**/iter/*);*

* DESCRIPTION
Returns the number of the values in the object which /iter/ point to. if
/iter/ isn't valid, returns 0.

* SINCE
version 2.13.1
