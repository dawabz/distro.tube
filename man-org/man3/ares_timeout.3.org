#+TITLE: Manpages - ares_timeout.3
#+DESCRIPTION: Linux manpage for ares_timeout.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ares_timeout - return maximum time to wait

* SYNOPSIS
#+begin_example
  #include <ares.h>

  struct timeval *ares_timeout(ares_channel channel,
                               struct timeval *maxtv,
                               struct timeval *tv)
#+end_example

* DESCRIPTION
The *ares_timeout(3)* function determines the maximum time for which the
caller should wait before invoking /ares_process(3)/ to process
timeouts. The parameter /maxtv/ specifies a existing maximum timeout, or
*NULL* if the caller does not wish to apply a maximum timeout. The
parameter /tv/ must point to a writable buffer of type *struct timeval*
It is valid for /maxtv/ and /tv/ to have the same value.

If no queries have timeouts pending sooner than the given maximum
timeout, *ares_timeout(3)* returns the value of /maxtv/; otherwise
*ares_timeout(3)* stores the appropriate timeout value into the buffer
pointed to by /tv/ and returns the value of /tv/.

* SEE ALSO
*ares_fds*(3), *ares_process*(3), *ares_process_fd*(3)

* AUTHOR
Greg Hudson, MIT Information Systems\\
Copyright 1998 by the Massachusetts Institute of Technology.
