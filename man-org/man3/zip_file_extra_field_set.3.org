#+TITLE: Manpages - zip_file_extra_field_set.3
#+DESCRIPTION: Linux manpage for zip_file_extra_field_set.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function sets the extra field with ID (two-byte signature)

and index

for the file at position

in the zip archive. The extra field's data will be set to

and length

If a new entry shall be appended, set

to

At least one of the following

must be set:

Set extra field in the archive's central directory.

Set extra field in the local file headers.

Please note that the extra field IDs 0x0001 (ZIP64 extension), 0x6375
(Infozip UTF-8 comment), and 0x7075 (Infozip UTF-8 file name) can not be
set using

since they are set by

automatically when needed.

Upon successful completion 0 is returned. Otherwise, -1 is returned and
the error code in

is set to indicate the error.

fails if:

The extra field size is too large (ID and length need 4 bytes; the
maximum length of all extra fields for one file combined is 65536
bytes). This error also occurs if

is too large.

Required memory could not be allocated.

is not a valid file index in

was added in libzip 0.11.

and
