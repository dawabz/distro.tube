#+TITLE: Manpages - std_pmr_pool_options.3
#+DESCRIPTION: Linux manpage for std_pmr_pool_options.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::pmr::pool_options - Parameters for tuning a pool resource's
behaviour.

* SYNOPSIS
\\

** Public Attributes
size_t *largest_required_pool_block*\\

size_t *max_blocks_per_chunk*\\
Upper limit on number of blocks in a chunk.

* Detailed Description
Parameters for tuning a pool resource's behaviour.

Definition at line *384* of file *std/memory_resource*.

* Member Data Documentation
** size_t std::pmr::pool_options::largest_required_pool_block
Definition at line *398* of file *std/memory_resource*.

** size_t std::pmr::pool_options::max_blocks_per_chunk
Upper limit on number of blocks in a chunk. A lower value prevents
allocating huge chunks that could remain mostly unused, but means pools
will need to replenished more frequently.

Definition at line *391* of file *std/memory_resource*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
