#+TITLE: Manpages - Test2_EventFacet_Amnesty.3perl
#+DESCRIPTION: Linux manpage for Test2_EventFacet_Amnesty.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test2::EventFacet::Amnesty - Facet for assertion amnesty.

* DESCRIPTION
This package represents what is expected in units of amnesty.

* NOTES
This facet appears in a list instead of being a single item.

* FIELDS
- $string = $amnesty->{details} :: 

- $string = $amnesty->details() :: 

Human readable explanation of why amnesty was granted. Example: /Not
implemented yet, will fix/

- $short_string = $amnesty->{tag} :: 

- $short_string = $amnesty->tag() :: 

Short string (usually 10 characters or less, not enforced, but may be
truncated by renderers) categorizing the amnesty.

- $bool = $amnesty->{inherited} :: 

- $bool = $amnesty->inherited() :: 

This will be true if the amnesty was granted to a parent event and
inherited by this event, which is a child, such as an assertion within a
subtest that is marked todo.

* SOURCE
The source code repository for Test2 can be found at
/http://github.com/Test-More/test-more//.

* MAINTAINERS
- Chad Granum <exodist@cpan.org> :: 

* AUTHORS
- Chad Granum <exodist@cpan.org> :: 

* COPYRIGHT
Copyright 2020 Chad Granum <exodist@cpan.org>.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See /http://dev.perl.org/licenses//
