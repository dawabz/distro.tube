#+TITLE: Manpages - zip_file_extra_field_delete_by_id.3
#+DESCRIPTION: Linux manpage for zip_file_extra_field_delete_by_id.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function deletes the extra field with index

for the file at position

in the zip archive.

If

is

then all extra fields will be deleted.

The following

are supported:

Delete extra fields from the archive's central directory.

Delete extra fields from the local file headers.

The

function deletes the extra field with ID (two-byte signature)

and index

(in other words, the

extra field with ID

The other arguments are the same as for

will delete all extra fields of the specified ID).

Please note that due to the library design, the index of an extra field
may be different between central directory and local file headers. For
this reason, it is not allowed to specify both

and

in

except when deleting all extra fields (i.e.,

being

Upon successful completion 0 is returned. Otherwise, -1 is returned and
the error code in

is set to indicate the error.

and

fail if:

is not a valid file index in

and

were added in libzip 0.11.

and
