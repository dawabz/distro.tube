#+TITLE: Manpages - __gnu_pbds_detail_dumnode_const_iterator.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_detail_dumnode_const_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::detail::dumnode_const_iterator< Key, Data, _Alloc > -
Constant node iterator.

* SYNOPSIS
\\

=#include <null_node_metadata.hpp>=

** Public Types
typedef const_iterator *const_reference*\\

typedef const_reference *reference*\\

typedef const_iterator *value_type*\\

* Detailed Description
** "template<typename Key, typename Data, typename _Alloc>
\\
struct __gnu_pbds::detail::dumnode_const_iterator< Key, Data, _Alloc
>"Constant node iterator.

Definition at line *52* of file *null_node_metadata.hpp*.

* Member Typedef Documentation
** template<typename Key , typename Data , typename _Alloc > typedef
const_iterator *__gnu_pbds::detail::dumnode_const_iterator*< Key, Data,
_Alloc >::const_reference
Definition at line *60* of file *null_node_metadata.hpp*.

** template<typename Key , typename Data , typename _Alloc > typedef
const_reference *__gnu_pbds::detail::dumnode_const_iterator*< Key, Data,
_Alloc >::reference
Definition at line *61* of file *null_node_metadata.hpp*.

** template<typename Key , typename Data , typename _Alloc > typedef
const_iterator *__gnu_pbds::detail::dumnode_const_iterator*< Key, Data,
_Alloc >::value_type
Definition at line *59* of file *null_node_metadata.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
