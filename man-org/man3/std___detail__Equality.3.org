#+TITLE: Manpages - std___detail__Equality.3
#+DESCRIPTION: Linux manpage for std___detail__Equality.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Equality< _Key, _Value, _Alloc, _ExtractKey, _Equal,
_Hash, _RangeHash, _Unused, _RehashPolicy, _Traits, _Unique_keys >

* SYNOPSIS
\\

Inherited by *std::_Hashtable< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits >*.

* Detailed Description
** "template<typename _Key, typename _Value, typename _Alloc, typename
_ExtractKey, typename _Equal, typename _Hash, typename _RangeHash,
typename _Unused, typename _RehashPolicy, typename _Traits, bool
_Unique_keys = _Traits::__unique_keys::value>
\\
struct std::__detail::_Equality< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits, _Unique_keys
>"Primary class template _Equality.

This is for implementing equality comparison for unordered containers,
per N3068, by John Lakos and Pablo Halpern. Algorithmically, we follow
closely the reference implementations therein.

Definition at line *1663* of file *hashtable_policy.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
