#+TITLE: Manpages - std_regex_constants.3
#+DESCRIPTION: Linux manpage for std_regex_constants.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::regex_constants - ISO C++ 2011 namespace for options and flags used
with std::regex.

* SYNOPSIS
\\

** 5.1 Regular Expression Syntax Options
enum *__syntax_option* { *_S_icase*, *_S_nosubs*, *_S_optimize*,
*_S_collate*, *_S_ECMAScript*, *_S_basic*, *_S_extended*, *_S_awk*,
*_S_grep*, *_S_egrep*, *_S_polynomial*, *_S_syntax_last* }\\
This is a bitmask type indicating how to interpret the regex.

enum *syntax_option_type* : unsigned int\\
This is a bitmask type indicating how to interpret the regex.

constexpr *syntax_option_type* *icase*\\

constexpr *syntax_option_type* *nosubs*\\

constexpr *syntax_option_type* *optimize*\\

constexpr *syntax_option_type* *collate*\\

constexpr *syntax_option_type* *ECMAScript*\\

constexpr *syntax_option_type* *basic*\\

constexpr *syntax_option_type* *extended*\\

constexpr *syntax_option_type* *awk*\\

constexpr *syntax_option_type* *grep*\\

constexpr *syntax_option_type* *egrep*\\

constexpr *syntax_option_type* *__polynomial*\\

constexpr *syntax_option_type* *operator&* (*syntax_option_type* __a,
*syntax_option_type* __b)\\
This is a bitmask type indicating how to interpret the regex.

constexpr *syntax_option_type* *operator|* (*syntax_option_type* __a,
*syntax_option_type* __b)\\
This is a bitmask type indicating how to interpret the regex.

constexpr *syntax_option_type* *operator^* (*syntax_option_type* __a,
*syntax_option_type* __b)\\
This is a bitmask type indicating how to interpret the regex.

constexpr *syntax_option_type* *operator~* (*syntax_option_type* __a)\\
This is a bitmask type indicating how to interpret the regex.

*syntax_option_type* & *operator&=* (*syntax_option_type* &__a,
*syntax_option_type* __b)\\
This is a bitmask type indicating how to interpret the regex.

*syntax_option_type* & *operator|=* (*syntax_option_type* &__a,
*syntax_option_type* __b)\\
This is a bitmask type indicating how to interpret the regex.

*syntax_option_type* & *operator^=* (*syntax_option_type* &__a,
*syntax_option_type* __b)\\
This is a bitmask type indicating how to interpret the regex.

** 5.2 Matching Rules
Matching a regular expression against a sequence of characters [first,
last) proceeds according to the rules of the grammar specified for the
regular expression object, modified according to the effects listed
below for any bitmask elements set.

enum *__match_flag* { *_S_not_bol*, *_S_not_eol*, *_S_not_bow*,
*_S_not_eow*, *_S_any*, *_S_not_null*, *_S_continuous*, *_S_prev_avail*,
*_S_sed*, *_S_no_copy*, *_S_first_only*, *_S_match_flag_last* }\\
This is a bitmask type indicating regex matching rules.

enum *match_flag_type* : unsigned int\\
This is a bitmask type indicating regex matching rules.

constexpr *match_flag_type* *match_default*\\

constexpr *match_flag_type* *match_not_bol*\\

constexpr *match_flag_type* *match_not_eol*\\

constexpr *match_flag_type* *match_not_bow*\\

constexpr *match_flag_type* *match_not_eow*\\

constexpr *match_flag_type* *match_any*\\

constexpr *match_flag_type* *match_not_null*\\

constexpr *match_flag_type* *match_continuous*\\

constexpr *match_flag_type* *match_prev_avail*\\

constexpr *match_flag_type* *format_default*\\

constexpr *match_flag_type* *format_sed*\\

constexpr *match_flag_type* *format_no_copy*\\

constexpr *match_flag_type* *format_first_only*\\

constexpr *match_flag_type* *operator&* (*match_flag_type* __a,
*match_flag_type* __b)\\
This is a bitmask type indicating regex matching rules.

constexpr *match_flag_type* *operator|* (*match_flag_type* __a,
*match_flag_type* __b)\\
This is a bitmask type indicating regex matching rules.

constexpr *match_flag_type* *operator^* (*match_flag_type* __a,
*match_flag_type* __b)\\
This is a bitmask type indicating regex matching rules.

constexpr *match_flag_type* *operator~* (*match_flag_type* __a)\\
This is a bitmask type indicating regex matching rules.

*match_flag_type* & *operator&=* (*match_flag_type* &__a,
*match_flag_type* __b)\\
This is a bitmask type indicating regex matching rules.

*match_flag_type* & *operator|=* (*match_flag_type* &__a,
*match_flag_type* __b)\\
This is a bitmask type indicating regex matching rules.

*match_flag_type* & *operator^=* (*match_flag_type* &__a,
*match_flag_type* __b)\\
This is a bitmask type indicating regex matching rules.

** 5.3 Error Types
enum *error_type* { *_S_error_collate*, *_S_error_ctype*,
*_S_error_escape*, *_S_error_backref*, *_S_error_brack*,
*_S_error_paren*, *_S_error_brace*, *_S_error_badbrace*,
*_S_error_range*, *_S_error_space*, *_S_error_badrepeat*,
*_S_error_complexity*, *_S_error_stack* }\\

constexpr *error_type* *error_collate* (_S_error_collate)\\

constexpr *error_type* *error_ctype* (_S_error_ctype)\\

constexpr *error_type* *error_escape* (_S_error_escape)\\

constexpr *error_type* *error_backref* (_S_error_backref)\\

constexpr *error_type* *error_brack* (_S_error_brack)\\

constexpr *error_type* *error_paren* (_S_error_paren)\\

constexpr *error_type* *error_brace* (_S_error_brace)\\

constexpr *error_type* *error_badbrace* (_S_error_badbrace)\\

constexpr *error_type* *error_range* (_S_error_range)\\

constexpr *error_type* *error_space* (_S_error_space)\\

constexpr *error_type* *error_badrepeat* (_S_error_badrepeat)\\

constexpr *error_type* *error_complexity* (_S_error_complexity)\\

constexpr *error_type* *error_stack* (_S_error_stack)\\

* Detailed Description
ISO C++ 2011 namespace for options and flags used with std::regex.

* Enumeration Type Documentation
** enum *std::regex_constants::__match_flag*
This is a bitmask type indicating regex matching rules. The
=match_flag_type= is implementation defined but it is valid to perform
bitwise operations on these values and expect the right thing to happen.

Definition at line *232* of file *regex_constants.h*.

** enum *std::regex_constants::__syntax_option*
This is a bitmask type indicating how to interpret the regex. The
=syntax_option_type= is implementation defined but it is valid to
perform bitwise operations on these values and expect the right thing to
happen.

A valid value of type syntax_option_type shall have exactly one of the
elements =ECMAScript=, =basic=, =extended=, =awk=, =grep=, =egrep= set.

Definition at line *54* of file *regex_constants.h*.

** enum *std::regex_constants::error_type*
The expression contained an invalid collating element name.

Definition at line *49* of file *regex_error.h*.

** enum *std::regex_constants::match_flag_type* : unsigned int
This is a bitmask type indicating regex matching rules. The
=match_flag_type= is implementation defined but it is valid to perform
bitwise operations on these values and expect the right thing to happen.

Definition at line *255* of file *regex_constants.h*.

** enum *std::regex_constants::syntax_option_type* : unsigned int
This is a bitmask type indicating how to interpret the regex. The
=syntax_option_type= is implementation defined but it is valid to
perform bitwise operations on these values and expect the right thing to
happen.

A valid value of type syntax_option_type shall have exactly one of the
elements =ECMAScript=, =basic=, =extended=, =awk=, =grep=, =egrep= set.

Definition at line *81* of file *regex_constants.h*.

* Function Documentation
** constexpr *error_type* std::regex_constants::error_backref
(_S_error_backref)= [constexpr]=
The expression contained an invalid back reference.

** constexpr *error_type* std::regex_constants::error_badbrace
(_S_error_badbrace)= [constexpr]=
The expression contained an invalid range in a {} expression.

** constexpr *error_type* std::regex_constants::error_badrepeat
(_S_error_badrepeat)= [constexpr]=
One of /*?+{/ was not preceded by a valid regular expression.

** constexpr *error_type* std::regex_constants::error_brace
(_S_error_brace)= [constexpr]=
The expression contained mismatched { and }

** constexpr *error_type* std::regex_constants::error_brack
(_S_error_brack)= [constexpr]=
The expression contained mismatched [ and ].

** constexpr *error_type* std::regex_constants::error_collate
(_S_error_collate)= [constexpr]=
The expression contained an invalid collating element name.

** constexpr *error_type* std::regex_constants::error_complexity
(_S_error_complexity)= [constexpr]=
The complexity of an attempted match against a regular expression
exceeded a pre-set level.

** constexpr *error_type* std::regex_constants::error_ctype
(_S_error_ctype)= [constexpr]=
The expression contained an invalid character class name.

** constexpr *error_type* std::regex_constants::error_escape
(_S_error_escape)= [constexpr]=
The expression contained an invalid escaped character, or a trailing
escape.

** constexpr *error_type* std::regex_constants::error_paren
(_S_error_paren)= [constexpr]=
The expression contained mismatched ( and ).

** constexpr *error_type* std::regex_constants::error_range
(_S_error_range)= [constexpr]=
The expression contained an invalid character range, such as [b-a] in
most encodings.

** constexpr *error_type* std::regex_constants::error_space
(_S_error_space)= [constexpr]=
There was insufficient memory to convert the expression into a finite
state machine.

** constexpr *error_type* std::regex_constants::error_stack
(_S_error_stack)= [constexpr]=
There was insufficient memory to determine whether the regular
expression could match the specified character sequence.

** constexpr *match_flag_type* std::regex_constants::operator&
(*match_flag_type* __a, *match_flag_type* __b)= [inline]=,
= [constexpr]=
This is a bitmask type indicating regex matching rules. The
=match_flag_type= is implementation defined but it is valid to perform
bitwise operations on these values and expect the right thing to happen.

Definition at line *374* of file *regex_constants.h*.

** constexpr *syntax_option_type* std::regex_constants::operator&
(*syntax_option_type* __a, *syntax_option_type* __b)= [inline]=,
= [constexpr]=
This is a bitmask type indicating how to interpret the regex. The
=syntax_option_type= is implementation defined but it is valid to
perform bitwise operations on these values and expect the right thing to
happen.

A valid value of type syntax_option_type shall have exactly one of the
elements =ECMAScript=, =basic=, =extended=, =awk=, =grep=, =egrep= set.

Definition at line *183* of file *regex_constants.h*.

** *match_flag_type* & std::regex_constants::operator&=
(*match_flag_type* & __a, *match_flag_type* __b)= [inline]=
This is a bitmask type indicating regex matching rules. The
=match_flag_type= is implementation defined but it is valid to perform
bitwise operations on these values and expect the right thing to happen.

Definition at line *399* of file *regex_constants.h*.

** *syntax_option_type* & std::regex_constants::operator&=
(*syntax_option_type* & __a, *syntax_option_type* __b)= [inline]=
This is a bitmask type indicating how to interpret the regex. The
=syntax_option_type= is implementation defined but it is valid to
perform bitwise operations on these values and expect the right thing to
happen.

A valid value of type syntax_option_type shall have exactly one of the
elements =ECMAScript=, =basic=, =extended=, =awk=, =grep=, =egrep= set.

Definition at line *208* of file *regex_constants.h*.

** constexpr *match_flag_type* std::regex_constants::operator^
(*match_flag_type* __a, *match_flag_type* __b)= [inline]=,
= [constexpr]=
This is a bitmask type indicating regex matching rules. The
=match_flag_type= is implementation defined but it is valid to perform
bitwise operations on these values and expect the right thing to happen.

Definition at line *388* of file *regex_constants.h*.

** constexpr *syntax_option_type* std::regex_constants::operator^
(*syntax_option_type* __a, *syntax_option_type* __b)= [inline]=,
= [constexpr]=
This is a bitmask type indicating how to interpret the regex. The
=syntax_option_type= is implementation defined but it is valid to
perform bitwise operations on these values and expect the right thing to
happen.

A valid value of type syntax_option_type shall have exactly one of the
elements =ECMAScript=, =basic=, =extended=, =awk=, =grep=, =egrep= set.

Definition at line *197* of file *regex_constants.h*.

** *match_flag_type* & std::regex_constants::operator^=
(*match_flag_type* & __a, *match_flag_type* __b)= [inline]=
This is a bitmask type indicating regex matching rules. The
=match_flag_type= is implementation defined but it is valid to perform
bitwise operations on these values and expect the right thing to happen.

Definition at line *407* of file *regex_constants.h*.

** *syntax_option_type* & std::regex_constants::operator^=
(*syntax_option_type* & __a, *syntax_option_type* __b)= [inline]=
This is a bitmask type indicating how to interpret the regex. The
=syntax_option_type= is implementation defined but it is valid to
perform bitwise operations on these values and expect the right thing to
happen.

A valid value of type syntax_option_type shall have exactly one of the
elements =ECMAScript=, =basic=, =extended=, =awk=, =grep=, =egrep= set.

Definition at line *216* of file *regex_constants.h*.

** constexpr *match_flag_type* std::regex_constants::operator|
(*match_flag_type* __a, *match_flag_type* __b)= [inline]=,
= [constexpr]=
This is a bitmask type indicating regex matching rules. The
=match_flag_type= is implementation defined but it is valid to perform
bitwise operations on these values and expect the right thing to happen.

Definition at line *381* of file *regex_constants.h*.

** constexpr *syntax_option_type* std::regex_constants::operator|
(*syntax_option_type* __a, *syntax_option_type* __b)= [inline]=,
= [constexpr]=
This is a bitmask type indicating how to interpret the regex. The
=syntax_option_type= is implementation defined but it is valid to
perform bitwise operations on these values and expect the right thing to
happen.

A valid value of type syntax_option_type shall have exactly one of the
elements =ECMAScript=, =basic=, =extended=, =awk=, =grep=, =egrep= set.

Definition at line *190* of file *regex_constants.h*.

** *match_flag_type* & std::regex_constants::operator|=
(*match_flag_type* & __a, *match_flag_type* __b)= [inline]=
This is a bitmask type indicating regex matching rules. The
=match_flag_type= is implementation defined but it is valid to perform
bitwise operations on these values and expect the right thing to happen.

Definition at line *403* of file *regex_constants.h*.

** *syntax_option_type* & std::regex_constants::operator|=
(*syntax_option_type* & __a, *syntax_option_type* __b)= [inline]=
This is a bitmask type indicating how to interpret the regex. The
=syntax_option_type= is implementation defined but it is valid to
perform bitwise operations on these values and expect the right thing to
happen.

A valid value of type syntax_option_type shall have exactly one of the
elements =ECMAScript=, =basic=, =extended=, =awk=, =grep=, =egrep= set.

Definition at line *212* of file *regex_constants.h*.

** constexpr *match_flag_type* std::regex_constants::operator~
(*match_flag_type* __a)= [inline]=, = [constexpr]=
This is a bitmask type indicating regex matching rules. The
=match_flag_type= is implementation defined but it is valid to perform
bitwise operations on these values and expect the right thing to happen.

Definition at line *395* of file *regex_constants.h*.

** constexpr *syntax_option_type* std::regex_constants::operator~
(*syntax_option_type* __a)= [inline]=, = [constexpr]=
This is a bitmask type indicating how to interpret the regex. The
=syntax_option_type= is implementation defined but it is valid to
perform bitwise operations on these values and expect the right thing to
happen.

A valid value of type syntax_option_type shall have exactly one of the
elements =ECMAScript=, =basic=, =extended=, =awk=, =grep=, =egrep= set.

Definition at line *204* of file *regex_constants.h*.

* Variable Documentation
** constexpr *syntax_option_type*
std::regex_constants::__polynomial= [inline]=, = [constexpr]=
Extension: Ensure both space complexity of compiled regex and time
complexity execution are not exponential. If specified in a regex with
back-references, the exception regex_constants::error_complexity will be
thrown.

Definition at line *179* of file *regex_constants.h*.

** constexpr *syntax_option_type* std::regex_constants::awk= [inline]=,
= [constexpr]=
Specifies that the grammar recognized by the regular expression engine
is that used by POSIX utility awk in IEEE Std 1003.1-2001. This option
is identical to syntax_option_type extended, except that C-style escape
sequences are supported. These sequences are: \\, \a, \b, \f, \n, \r, \t
, \v, \&apos,, &apos,, and \ddd (where ddd is one, two, or three octal
digits).

Definition at line *152* of file *regex_constants.h*.

** constexpr *syntax_option_type*
std::regex_constants::basic= [inline]=, = [constexpr]=
Specifies that the grammar recognized by the regular expression engine
is that used by POSIX basic regular expressions in IEEE Std 1003.1-2001,
Portable Operating System Interface (POSIX), Base Definitions and
Headers, Section 9, Regular Expressions [IEEE, Information Technology --
Portable Operating System Interface (POSIX), IEEE Standard 1003.1-2001].

Definition at line *132* of file *regex_constants.h*.

** constexpr *syntax_option_type*
std::regex_constants::collate= [inline]=, = [constexpr]=
Specifies that character ranges of the form [a-b] should be locale
sensitive.

Definition at line *111* of file *regex_constants.h*.

** constexpr *syntax_option_type*
std::regex_constants::ECMAScript= [inline]=, = [constexpr]=
Specifies that the grammar recognized by the regular expression engine
is that used by ECMAScript in ECMA-262 [Ecma International, ECMAScript
Language Specification, Standard Ecma-262, third edition, 1999], as
modified in section [28.13]. This grammar is similar to that defined in
the PERL scripting language but extended with elements found in the
POSIX regular expression grammar.

Definition at line *122* of file *regex_constants.h*.

** constexpr *syntax_option_type*
std::regex_constants::egrep= [inline]=, = [constexpr]=
Specifies that the grammar recognized by the regular expression engine
is that used by POSIX utility grep when given the -E option in IEEE Std
1003.1-2001. This option is identical to syntax_option_type extended,
except that newlines are treated as whitespace.

Definition at line *170* of file *regex_constants.h*.

** constexpr *syntax_option_type*
std::regex_constants::extended= [inline]=, = [constexpr]=
Specifies that the grammar recognized by the regular expression engine
is that used by POSIX extended regular expressions in IEEE Std
1003.1-2001, Portable Operating System Interface (POSIX), Base
Definitions and Headers, Section 9, Regular Expressions.

Definition at line *141* of file *regex_constants.h*.

** constexpr *match_flag_type*
std::regex_constants::format_default= [inline]=, = [constexpr]=
When a regular expression match is to be replaced by a new string, the
new string is constructed using the rules used by the ECMAScript replace
function in ECMA- 262 [Ecma International, ECMAScript Language
Specification, Standard Ecma-262, third edition, 1999], part 15.5.4.11
String.prototype.replace. In addition, during search and replace
operations all non-overlapping occurrences of the regular expression are
located and replaced, and sections of the input that did not match the
expression are copied unchanged to the output string.

Format strings (from ECMA-262 [15.5.4.11]):

- $$ The dollar-sign itself ($)

- $& The matched substring.

- $` The portion of /string/ that precedes the matched substring. This
  would be match_results::prefix().

- $' The portion of /string/ that follows the matched substring. This
  would be match_results::suffix().

- $n The nth capture, where n is in [1,9] and $n is not followed by a
  decimal digit. If n <= match_results::size() and the nth capture is
  undefined, use the empty string instead. If n > match_results::size(),
  the result is implementation-defined.

- $nn The nnth capture, where nn is a two-digit decimal number on [01,
  99]. If nn <= match_results::size() and the nth capture is undefined,
  use the empty string instead. If nn > match_results::size(), the
  result is implementation-defined.

Definition at line *346* of file *regex_constants.h*.

** constexpr *match_flag_type*
std::regex_constants::format_first_only= [inline]=, = [constexpr]=
When specified during a search and replace operation, only the first
occurrence of the regular expression shall be replaced.

Definition at line *370* of file *regex_constants.h*.

** constexpr *match_flag_type*
std::regex_constants::format_no_copy= [inline]=, = [constexpr]=
During a search and replace operation, sections of the character
container sequence being searched that do not match the regular
expression shall not be copied to the output string.

Definition at line *363* of file *regex_constants.h*.

** constexpr *match_flag_type*
std::regex_constants::format_sed= [inline]=, = [constexpr]=
When a regular expression match is to be replaced by a new string, the
new string is constructed using the rules used by the POSIX sed utility
in IEEE Std 1003.1- 2001 [IEEE, Information Technology -- Portable
Operating System Interface (POSIX), IEEE Standard 1003.1-2001].

Definition at line *355* of file *regex_constants.h*.

** constexpr *syntax_option_type* std::regex_constants::grep= [inline]=,
= [constexpr]=
Specifies that the grammar recognized by the regular expression engine
is that used by POSIX utility grep in IEEE Std 1003.1-2001. This option
is identical to syntax_option_type basic, except that newlines are
treated as whitespace.

Definition at line *161* of file *regex_constants.h*.

** constexpr *syntax_option_type*
std::regex_constants::icase= [inline]=, = [constexpr]=
Specifies that the matching of regular expressions against a character
sequence shall be performed without regard to case.

Definition at line *87* of file *regex_constants.h*.

** constexpr *match_flag_type*
std::regex_constants::match_any= [inline]=, = [constexpr]=
If more than one match is possible then any match is an acceptable
result.

Definition at line *297* of file *regex_constants.h*.

** constexpr *match_flag_type*
std::regex_constants::match_continuous= [inline]=, = [constexpr]=
The expression only matches a sub-sequence that begins at first .

Definition at line *309* of file *regex_constants.h*.

** constexpr *match_flag_type*
std::regex_constants::match_default= [inline]=, = [constexpr]=
The default matching rules.

Definition at line *260* of file *regex_constants.h*.

** constexpr *match_flag_type*
std::regex_constants::match_not_bol= [inline]=, = [constexpr]=
The first character in the sequence [first, last) is treated as though
it is not at the beginning of a line, so the character (^) in the
regular expression shall not match [first, first).

Definition at line *268* of file *regex_constants.h*.

** constexpr *match_flag_type*
std::regex_constants::match_not_bow= [inline]=, = [constexpr]=
The expression \b is not matched against the sub-sequence [first,first).

Definition at line *283* of file *regex_constants.h*.

** constexpr *match_flag_type*
std::regex_constants::match_not_eol= [inline]=, = [constexpr]=
The last character in the sequence [first, last) is treated as though it
is not at the end of a line, so the character ($) in the regular
expression shall not match [last, last).

Definition at line *276* of file *regex_constants.h*.

** constexpr *match_flag_type*
std::regex_constants::match_not_eow= [inline]=, = [constexpr]=
The expression \b should not be matched against the sub-sequence
[last,last).

Definition at line *290* of file *regex_constants.h*.

** constexpr *match_flag_type*
std::regex_constants::match_not_null= [inline]=, = [constexpr]=
The expression does not match an empty sequence.

Definition at line *303* of file *regex_constants.h*.

** constexpr *match_flag_type*
std::regex_constants::match_prev_avail= [inline]=, = [constexpr]=
--first is a valid iterator position. When this flag is set then the
flags match_not_bol and match_not_bow are ignored by the regular
expression algorithms 28.11 and iterators 28.12.

Definition at line *317* of file *regex_constants.h*.

** constexpr *syntax_option_type*
std::regex_constants::nosubs= [inline]=, = [constexpr]=
Specifies that when a regular expression is matched against a character
container sequence, no sub-expression matches are to be stored in the
supplied match_results structure.

Definition at line *95* of file *regex_constants.h*.

** constexpr *syntax_option_type*
std::regex_constants::optimize= [inline]=, = [constexpr]=
Specifies that the regular expression engine should pay more attention
to the speed with which regular expressions are matched, and less to the
speed with which regular expression objects are constructed. Otherwise
it has no detectable effect on the program output.

Definition at line *104* of file *regex_constants.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
