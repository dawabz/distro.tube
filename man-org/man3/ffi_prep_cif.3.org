#+TITLE: Manpages - ffi_prep_cif.3
#+DESCRIPTION: Linux manpage for ffi_prep_cif.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
structure for use with

The

function prepares a

structure for use with

specifies a set of calling conventions to use.

is an array of

pointers to

structs that describe the data type, size and alignment of each
argument.

points to an

that describes the data type, size and alignment of the return value.
Note that to call a variadic function

must be used instead.

Upon successful completion,

returns

It will return

if

is

or

or

is malformed. If

does not refer to a valid ABI,

will be returned. Available ABIs are defined in
