#+TITLE: Manpages - signbit.3
#+DESCRIPTION: Linux manpage for signbit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
signbit - test sign of a real floating-point number

* SYNOPSIS
#+begin_example
  #include <math.h>

  int signbit(x);
#+end_example

Link with /-lm/.

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*signbit*():

#+begin_example
      _ISOC99_SOURCE || _POSIX_C_SOURCE >= 200112L
#+end_example

* DESCRIPTION
*signbit*() is a generic macro which can work on all real floating-point
types. It returns a nonzero value if the value of /x/ has its sign bit
set.

This is not the same as /x < 0.0/, because IEEE 754 floating point
allows zero to be signed. The comparison /-0.0 < 0.0/ is false, but
/signbit(-0.0)/ will return a nonzero value.

NaNs and infinities have a sign bit.

* RETURN VALUE
The *signbit*() macro returns nonzero if the sign of /x/ is negative;
otherwise it returns zero.

* ERRORS
No errors occur.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface   | Attribute     | Value   |
| *signbit*() | Thread safety | MT-Safe |

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008, C99. This function is defined in IEC 559
(and the appendix with recommended functions in IEEE 754/IEEE 854).

* SEE ALSO
*copysign*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
