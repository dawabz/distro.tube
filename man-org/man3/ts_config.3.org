#+TITLE: Manpages - ts_config.3
#+DESCRIPTION: Linux manpage for ts_config.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ts_config, ts_reconfig - read tslib's configuration

* SYNOPSIS
#+begin_example
  #include <tslib.h>

  int ts_config(struct tsdev *dev);

  int ts_reconfig(struct tsdev *dev);
#+end_example

* DESCRIPTION
*ts_config*() loads and initialises all modules configured in the tslib
configuration file *TSLIB_CONFFILE* , see ts.conf (5). *ts_reconfig*()
re-runs ts_config and updates the configuration if the configuration
file has changed.

* RETURN VALUE
Zero is returned on success. A negative value is returned in case of an
error.

* SEE ALSO
*ts_read*(3), *ts_open*(3), *ts_setup*(3), *ts_close*(3), *ts.conf*(5)
