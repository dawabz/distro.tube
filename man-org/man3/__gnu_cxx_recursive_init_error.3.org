#+TITLE: Manpages - __gnu_cxx_recursive_init_error.3
#+DESCRIPTION: Linux manpage for __gnu_cxx_recursive_init_error.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::recursive_init_error - Exception thrown by
__cxa_guard_acquire.

* SYNOPSIS
\\

=#include <cxxabi.h>=

Inherits *std::exception*.

** Public Member Functions
virtual const char * *what* () const noexcept\\

* Detailed Description
Exception thrown by __cxa_guard_acquire.

C++ 2011 6.7 [stmt.dcl]/4: If control re-enters the declaration
recursively while the variable is being initialized, the behavior is
undefined.

Since we already have a library function to handle locking, we might as
well check for this situation and throw an exception. We use the second
byte of the guard variable to remember that we're in the middle of an
initialization.

Definition at line *704* of file *cxxabi.h*.

* Member Function Documentation
** virtual const char * std::exception::what () const= [virtual]=,
= [noexcept]=, = [inherited]=
Returns a C-style character string describing the general cause of the
current error.\\

Reimplemented in *std::ios_base::failure*, *std::bad_alloc*,
*std::filesystem::filesystem_error*, *std::bad_weak_ptr*,
*std::bad_function_call*,
*std::experimental::fundamentals_v1::bad_any_cast*,
*std::experimental::filesystem::v1::filesystem_error*,
*std::bad_any_cast*, *std::future_error*, *std::logic_error*,
*std::runtime_error*, *std::bad_exception*, *std::bad_cast*,
*std::bad_typeid*, and *std::bad_optional_access*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
