#+TITLE: Manpages - libssh2_channel_ignore_extended_data.3
#+DESCRIPTION: Linux manpage for libssh2_channel_ignore_extended_data.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_channel_ignore_extended_data - convenience macro for
/libssh2_channel_handle_extended_data(3)/ calls

* SYNOPSIS
#include <libssh2.h>

libssh2_channel_ignore_extended_data(arguments)

* DESCRIPTION
This function is deprecated. Use the
/libssh2_channel_handle_extended_data2(3)/ function instead!

This is a macro defined in a public libssh2 header file that is using
the underlying function /libssh2_channel_handle_extended_data(3)/.

* RETURN VALUE
See /libssh2_channel_handle_extended_data(3)/

* ERRORS
See /libssh2_channel_handle_extended_data(3)/

* SEE ALSO
*libssh2_channel_handle_extended_data(3)*
