#+TITLE: Manpages - std_is_final.3
#+DESCRIPTION: Linux manpage for std_is_final.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::is_final< _Tp > - is_final

* SYNOPSIS
\\

Inherits *std::integral_constant< bool, __is_final(_Tp)>*.

** Public Types
typedef *integral_constant*< bool, __v > *type*\\

typedef bool *value_type*\\

** Public Member Functions
constexpr *operator value_type* () const noexcept\\

constexpr value_type *operator()* () const noexcept\\

** Static Public Attributes
static constexpr bool *value*\\

* Detailed Description
** "template<typename _Tp>
\\
struct std::is_final< _Tp >"is_final

Definition at line *734* of file *std/type_traits*.

* Member Typedef Documentation
** typedef *integral_constant*<bool , __v> *std::integral_constant*<
bool , __v >::*type*= [inherited]=
Definition at line *61* of file *std/type_traits*.

** typedef bool *std::integral_constant*< bool , __v
>::value_type= [inherited]=
Definition at line *60* of file *std/type_traits*.

* Member Function Documentation
** constexpr *std::integral_constant*< bool , __v >::operator value_type
() const= [inline]=, = [constexpr]=, = [noexcept]=, = [inherited]=
Definition at line *62* of file *std/type_traits*.

** constexpr value_type *std::integral_constant*< bool , __v
>::operator() () const= [inline]=, = [constexpr]=, = [noexcept]=,
= [inherited]=
Definition at line *67* of file *std/type_traits*.

* Member Data Documentation
** constexpr bool *std::integral_constant*< bool , __v
>::value= [static]=, = [constexpr]=, = [inherited]=
Definition at line *59* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
