#+TITLE: Manpages - void.3
#+DESCRIPTION: Linux manpage for void.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about void.3 is found in manpage for: [[../man7/system_data_types.7][man7/system_data_types.7]]