#+TITLE: Manpages - getrpcbyname.3
#+DESCRIPTION: Linux manpage for getrpcbyname.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about getrpcbyname.3 is found in manpage for: [[../man3/getrpcent.3][man3/getrpcent.3]]