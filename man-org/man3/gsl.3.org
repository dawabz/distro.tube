#+TITLE: Manpages - gsl.3
#+DESCRIPTION: Linux manpage for gsl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gsl - GNU Scientific Library

* SYNOPSIS
#include <gsl/...>

* DESCRIPTION
The GNU Scientific Library (GSL) is a collection of routines for
numerical computing. The routines are written from scratch by the GSL
team in C, and present a modern Applications Programming Interface (API)
for C programmers, allowing wrappers to be written for very high level
languages.

The library covers the following areas,

#+begin_example

  Complex Numbers
  Roots of Polynomials
  Special Functions
  Vectors and Matrices
  Permutations
  Combinations
  Sorting
  BLAS Support
  Linear Algebra
  Eigensystems
  Fast Fourier Transforms
  Quadrature
  Random Numbers
  Quasi-Random Sequences
  Random Distributions
  Statistics
  Histograms
  N-Tuples
  Monte Carlo Integration
  Simulated Annealing
  Differential Equations
  Interpolation
  Numerical Differentiation
  Chebyshev Approximations
  Series Acceleration
  Discrete Hankel Transforms
  Root-Finding
  Minimization
  Least-Squares Fitting
  Physical Constants
  IEEE Floating-Point
#+end_example

For more information please consult the GSL Reference Manual, which is
available as an info file. You can read it online using the shell
command *info gsl-ref* (if the library is installed).

Please report any bugs to *bug-gsl@gnu.org.*
