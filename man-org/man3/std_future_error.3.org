#+TITLE: Manpages - std_future_error.3
#+DESCRIPTION: Linux manpage for std_future_error.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::future_error - Exception type thrown by futures.

* SYNOPSIS
\\

Inherits *std::logic_error*.

** Public Member Functions
*future_error* (*future_errc* __errc)\\

const *error_code* & *code* () const noexcept\\

virtual const char * *what* () const noexcept\\

** Friends
void *__throw_future_error* (int)\\

* Detailed Description
Exception type thrown by futures.

Definition at line *95* of file *future*.

* Constructor & Destructor Documentation
** std::future_error::future_error (*future_errc* __errc)= [inline]=,
= [explicit]=
Definition at line *99* of file *future*.

* Member Function Documentation
** const *error_code* & std::future_error::code () const= [inline]=,
= [noexcept]=
Definition at line *109* of file *future*.

** virtual const char * std::future_error::what () const= [virtual]=,
= [noexcept]=
Returns a C-style character string describing the general cause of the
current error (the same string passed to the ctor).\\

Reimplemented from *std::logic_error*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
