#+TITLE: Manpages - std_binary_function.3
#+DESCRIPTION: Linux manpage for std_binary_function.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::binary_function< _Arg1, _Arg2, _Result >

* SYNOPSIS
\\

=#include <stl_function.h>=

Inherited by std::_Sp_owner_less< shared_ptr< _Tp >, weak_ptr< _Tp > >,
std::_Sp_owner_less< weak_ptr< _Tp >, shared_ptr< _Tp > >,
std::_Sp_owner_less< __shared_ptr< _Tp, _Lp >, __weak_ptr< _Tp, _Lp > >,
std::_Sp_owner_less< __weak_ptr< _Tp, _Lp >, __shared_ptr< _Tp, _Lp > >,
std::_Sp_owner_less< void, void >, *std::less< typename
_Sequence::value_type >*, and *std::pointer_to_binary_function< _Arg1,
_Arg2, _Result >*.

** Public Types
typedef _Arg1 *first_argument_type*\\
=first_argument_type= is the type of the first argument

typedef _Result *result_type*\\
=result_type= is the return type

typedef _Arg2 *second_argument_type*\\
=second_argument_type= is the type of the second argument

* Detailed Description
** "template<typename _Arg1, typename _Arg2, typename _Result>
\\
struct std::binary_function< _Arg1, _Arg2, _Result >"This is one of the
*functor base classes*.

Definition at line *118* of file *stl_function.h*.

* Member Typedef Documentation
** template<typename _Arg1 , typename _Arg2 , typename _Result > typedef
_Arg1 *std::binary_function*< _Arg1, _Arg2, _Result
>::*first_argument_type*
=first_argument_type= is the type of the first argument

Definition at line *121* of file *stl_function.h*.

** template<typename _Arg1 , typename _Arg2 , typename _Result > typedef
_Result *std::binary_function*< _Arg1, _Arg2, _Result >::*result_type*
=result_type= is the return type

Definition at line *127* of file *stl_function.h*.

** template<typename _Arg1 , typename _Arg2 , typename _Result > typedef
_Arg2 *std::binary_function*< _Arg1, _Arg2, _Result
>::*second_argument_type*
=second_argument_type= is the type of the second argument

Definition at line *124* of file *stl_function.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
