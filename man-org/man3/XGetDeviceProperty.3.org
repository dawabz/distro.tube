#+TITLE: Manpages - XGetDeviceProperty.3
#+DESCRIPTION: Linux manpage for XGetDeviceProperty.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XGetDeviceProperty, XChangeDeviceProperty, XDeleteDeviceProperty - Get,
change or delete a devices property.

* SYNOPSIS
#+begin_example
  #include <X11/extensions/XInput.h>
#+end_example

#+begin_example
  int XGetDeviceProperty( Display *display,
                          XDevice *device,
                          Atom property,
                          long offset,
                          long length,
                          Bool delete,
                          Atom req_type,
                          Atom *actual_type_return,
                          int *actual_format_return,
                          unsigned long *nitems_return,
                          unsigned long *bytes_after_return,
                          unsigned char **prop_return)
#+end_example

#+begin_example
  void XChangeDeviceProperty( Display *display,
                              XDevice *device,
                              Atom property,
                              Atom type,
                              int format,
                              int mode,
                              const char *data,
                              int nelements)
#+end_example

#+begin_example
  void XDeleteDeviceProperty( Display *display,
                              XDevice *device,
                              Atom property)
#+end_example

#+begin_example
  actual_type_return
         Returns an atom identifier that defines the actual type
         of the property.
#+end_example

#+begin_example
  actual_format_return
         Returns the actual format of the property.
#+end_example

#+begin_example
  bytes_after_return
         Returns the number of bytes remaining to be read in the
         property if a partial read was performed.
#+end_example

#+begin_example
  data
         Specifies the property data.
#+end_example

#+begin_example
  delete
         Specifies a Boolean value that determines whether the
         property is deleted.
#+end_example

#+begin_example
  display
         Specifies the connection to the X server.
#+end_example

#+begin_example
  device
         The device to grab.
#+end_example

#+begin_example
  format
         Specifies whether the data should be viewed as a list of
         8-bit, 16-bit, or 32-bit quantities. Possible values are
         8, 16, and 32. This information allows the X server to
         correctly perform byte-swap operations as necessary. If
         the format is 16-bit or 32-bit, you must explicitly cast
         the data pointer to an (unsigned char*) in the call to
         XChangeDeviceProperty.
#+end_example

#+begin_example
  length
         Specifies the length in 32-bit multiplies of the data to
         be retrieved.
#+end_example

#+begin_example
  mode
         Specifies the mode of operation. You can pass
         PropModeReplace, PropModePrepend, or PropModeAppend.
#+end_example

#+begin_example
  nelements
         Specifies the number of elements in data.
#+end_example

#+begin_example
  nitems_return
         Returns the actual number of 8-bit, 16-bit, or 32-bit
         items stored in the prop_return array.
#+end_example

#+begin_example
  num_values
         Specifies the number of elements in the values list.
#+end_example

#+begin_example
  offset
         Specifies the offset in the specified property (in
         32-bit quantities) where the data is to be retrieved.
#+end_example

#+begin_example
  property
         Specifies the property to modify or query.
#+end_example

#+begin_example
  prop_return
         Returns the data in the specified format. If the
         returned format is 8, the returned data is represented
         as a char array. If the returned format is 16, the
         returned data is represented as an array of short int
         type and should be cast to that type to obtain the
         elements. If the returned format is 32, the property
         data will be stored as an array of longs (which in a
         64-bit application will be 64-bit values that are padded
         in the upper 4 bytes).
#+end_example

#+begin_example
  req_type
         Specifies the atom identifier associated with the
         property type or AnyPropertyType.
#+end_example

* DESCRIPTION
The XGetDeviceProperty function returns the actual type of the property;
the actual format of the property; the number of 8-bit, 16-bit, or
32-bit items transferred; the number of bytes remaining to be read in
the property; and a pointer to the data actually returned. For a
detailed description of this function, see the man page to
XGetWindowProperty.

The XChangeDeviceProperty function alters the property for the specified
device and causes the server to generate a XPropertyNotifyEvent event on
that device. For a detailed description of this function, see the man
page to XChangeProperty.

The XDeleteDeviceProperty function deletes the specified device
property. Note that a client cannot delete a property created by a
driver or the server. Attempting to do so will result in a BadAtom
error.

XGetDeviceProperty can generate a BadAtom, BadDevice error.

XChangeDeviceProperty can generate a BadDevice, a BadMatch, a BadAtom,
and a BadValue error.

XDeleteDeviceProperty can generate a BadDevice, and a BadAtom error.

* DIAGNOSIS

#+begin_quote
  #+begin_example
    BadAtom
           A value does not describe a valid named identifier or
           the client attempted to remove a driver-allocated
           property.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadDevice
           An invalid device was specified. The device does not
           exist.
  #+end_example
#+end_quote

* SEE ALSO

#+begin_quote
  #+begin_example
    XListDeviceProperties(3)
  #+end_example
#+end_quote
