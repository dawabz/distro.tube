#+TITLE: Manpages - libalpm_handle.3
#+DESCRIPTION: Linux manpage for libalpm_handle.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libalpm_handle - Handle

- Functions to initialize and release libalpm.

* SYNOPSIS
\\

** Typedefs
typedef struct __alpm_handle_t *alpm_handle_t*\\
The libalpm context handle.

** Functions
*alpm_handle_t* * *alpm_initialize* (const char *root, const char
*dbpath, *alpm_errno_t* *err)\\
Initializes the library.

int *alpm_release* (*alpm_handle_t* *handle)\\
Release the library.

* Detailed Description
Functions to initialize and release libalpm.

* Typedef Documentation
** typedef struct __alpm_handle_t *alpm_handle_t*
The libalpm context handle. This struct represents an instance of
libalpm.

* Function Documentation
** *alpm_handle_t* * alpm_initialize (const char * root, const char *
dbpath, *alpm_errno_t* * err)
Initializes the library. Creates handle, connects to database and
creates lockfile. This must be called before any other functions are
called.

*Parameters*

#+begin_quote
  /root/ the root path for all filesystem operations\\
  /dbpath/ the absolute path to the libalpm database\\
  /err/ an optional variable to hold any error return codes
#+end_quote

*Returns*

#+begin_quote
  a context handle on success, NULL on error, err will be set if
  provided
#+end_quote

** int alpm_release (*alpm_handle_t* * handle)
Release the library. Disconnects from the database, removes handle and
lockfile This should be the last alpm call you make. After this returns,
handle should be considered invalid and cannot be reused in any way.

*Parameters*

#+begin_quote
  /handle/ the context handle
#+end_quote

*Returns*

#+begin_quote
  0 on success, -1 on error
#+end_quote

* Author
Generated automatically by Doxygen for libalpm from the source code.
