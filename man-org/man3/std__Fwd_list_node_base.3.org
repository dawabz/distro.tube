#+TITLE: Manpages - std__Fwd_list_node_base.3
#+DESCRIPTION: Linux manpage for std__Fwd_list_node_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Fwd_list_node_base - A helper basic node class for forward_list.
This is just a linked list with nothing inside it. There are purely list
shuffling utility methods here.

* SYNOPSIS
\\

=#include <forward_list.h>=

Inherited by *std::_Fwd_list_node< _Tp >*.

** Public Member Functions
*_Fwd_list_node_base* (*_Fwd_list_node_base* &&__x) noexcept\\

*_Fwd_list_node_base* (const *_Fwd_list_node_base* &)=delete\\

void *_M_reverse_after* () noexcept\\

*_Fwd_list_node_base* * *_M_transfer_after* (*_Fwd_list_node_base*
*__begin, *_Fwd_list_node_base* *__end) noexcept\\

*_Fwd_list_node_base* & *operator=* (*_Fwd_list_node_base* &&__x)
noexcept\\

*_Fwd_list_node_base* & *operator=* (const *_Fwd_list_node_base*
&)=delete\\

** Public Attributes
*_Fwd_list_node_base* * *_M_next*\\

* Detailed Description
A helper basic node class for forward_list. This is just a linked list
with nothing inside it. There are purely list shuffling utility methods
here.

Definition at line *54* of file *forward_list.h*.

* Constructor & Destructor Documentation
** std::_Fwd_list_node_base::_Fwd_list_node_base (*_Fwd_list_node_base*
&& __x)= [inline]=, = [noexcept]=
Definition at line *57* of file *forward_list.h*.

* Member Function Documentation
** void std::_Fwd_list_node_base::_M_reverse_after ()= [inline]=,
= [noexcept]=
Definition at line *91* of file *forward_list.h*.

** *_Fwd_list_node_base* * std::_Fwd_list_node_base::_M_transfer_after
(*_Fwd_list_node_base* * __begin, *_Fwd_list_node_base* *
__end)= [inline]=, = [noexcept]=
Definition at line *75* of file *forward_list.h*.

** *_Fwd_list_node_base* & std::_Fwd_list_node_base::operator=
(*_Fwd_list_node_base* && __x)= [inline]=, = [noexcept]=
Definition at line *65* of file *forward_list.h*.

* Member Data Documentation
** *_Fwd_list_node_base** std::_Fwd_list_node_base::_M_next
Definition at line *72* of file *forward_list.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
