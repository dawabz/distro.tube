#+TITLE: Manpages - Xutf8ResetIC.3
#+DESCRIPTION: Linux manpage for Xutf8ResetIC.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about Xutf8ResetIC.3 is found in manpage for: [[../man3/XmbResetIC.3][man3/XmbResetIC.3]]