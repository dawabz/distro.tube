#+TITLE: Manpages - FcConfigEnableHome.3
#+DESCRIPTION: Linux manpage for FcConfigEnableHome.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigEnableHome - controls use of the home directory.

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcConfigEnableHome (FcBool /enable/*);*

* DESCRIPTION
If /enable/ is FcTrue, then Fontconfig will use various files which are
specified relative to the user's home directory (using the ~ notation in
the configuration). When /enable/ is FcFalse, then all use of the home
directory in these contexts will be disabled. The previous setting of
the value is returned.
