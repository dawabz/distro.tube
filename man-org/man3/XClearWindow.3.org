#+TITLE: Manpages - XClearWindow.3
#+DESCRIPTION: Linux manpage for XClearWindow.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XClearWindow.3 is found in manpage for: [[../man3/XClearArea.3][man3/XClearArea.3]]