#+TITLE: Manpages - FcCacheCreateTagFile.3
#+DESCRIPTION: Linux manpage for FcCacheCreateTagFile.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcCacheCreateTagFile - Create CACHEDIR.TAG at cache directory.

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcCacheCreateTagFile (const FcConfig */config/*);*

* DESCRIPTION
This tries to create CACHEDIR.TAG file at the cache directory registered
to /config/.

* SINCE
version 2.9.91
