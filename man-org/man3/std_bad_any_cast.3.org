#+TITLE: Manpages - std_bad_any_cast.3
#+DESCRIPTION: Linux manpage for std_bad_any_cast.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::bad_any_cast - Exception class thrown by a failed =any_cast=.

* SYNOPSIS
\\

Inherits *std::bad_cast*.

** Public Member Functions
virtual const char * *what* () const noexcept\\

* Detailed Description
Exception class thrown by a failed =any_cast=.

Definition at line *54* of file *std/any*.

* Member Function Documentation
** virtual const char * std::bad_any_cast::what () const= [inline]=,
= [virtual]=, = [noexcept]=
Returns a C-style character string describing the general cause of the
current error.\\

Reimplemented from *std::bad_cast*.

Definition at line *57* of file *std/any*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
