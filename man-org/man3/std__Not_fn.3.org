#+TITLE: Manpages - std__Not_fn.3
#+DESCRIPTION: Linux manpage for std__Not_fn.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Not_fn< _Fn > - Generalized negator.

* SYNOPSIS
\\

** Public Member Functions
template<typename _Fn2 > constexpr *_Not_fn* (_Fn2 &&__fn, int)\\

*_Not_fn* (*_Not_fn* &&__fn)=default\\

*_Not_fn* (const *_Not_fn* &__fn)=default\\

** Public Attributes
template<typename... _Args> constexpr decltype(_S_not< __inv_res_t< _Fn
&&, _Args... > >()) *operator()* (_Args &&... __args)
&&noexcept(__is_nothrow_invocable< _Fn &&, _Args... >::value
&&noexcept(_S_not< __inv_res_t< _Fn &&, _Args... > >()))\\

template<typename... _Args> constexpr decltype(_S_not< __inv_res_t< _Fn
&, _Args... > >()) *operator()* (_Args &&... __args)
&noexcept(__is_nothrow_invocable< _Fn &, _Args... >::value
&&noexcept(_S_not< __inv_res_t< _Fn &, _Args... > >()))\\

template<typename... _Args> constexpr decltype(_S_not< __inv_res_t< _Fn
const &&, _Args... > >()) *operator()* (_Args &&... __args) const
&&noexcept(__is_nothrow_invocable< _Fn const &&, _Args... >::value
&&noexcept(_S_not< __inv_res_t< _Fn const &&, _Args... > >()))\\

template<typename... _Args> constexpr decltype(_S_not< __inv_res_t< _Fn
const &, _Args... > >()) *operator()* (_Args &&... __args) const
&noexcept(__is_nothrow_invocable< _Fn const &, _Args... >::value
&&noexcept(_S_not< __inv_res_t< _Fn const &, _Args... > >()))\\

* Detailed Description
** "template<typename _Fn>
\\
class std::_Not_fn< _Fn >"Generalized negator.

Definition at line *923* of file *std/functional*.

* Constructor & Destructor Documentation
** template<typename _Fn > template<typename _Fn2 > constexpr
*std::_Not_fn*< _Fn >::*_Not_fn* (_Fn2 && __fn, int)= [inline]=,
= [constexpr]=
Definition at line *935* of file *std/functional*.

* Member Data Documentation
** template<typename _Fn > template<typename... _Args> constexpr
decltype(_S_not< __inv_res_t< _Fn &&, _Args... > >()) *std::_Not_fn*<
_Fn >::operator()(_Args &&... __args) &&noexcept(__is_nothrow_invocable<
_Fn &&, _Args... >::value &&noexcept(_S_not< __inv_res_t< _Fn &&,
_Args... > >()))= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *958* of file *std/functional*.

** template<typename _Fn > template<typename... _Args> constexpr
decltype(_S_not< __inv_res_t< _Fn &, _Args... > >()) *std::_Not_fn*< _Fn
>::operator()(_Args &&... __args) &noexcept(__is_nothrow_invocable< _Fn
&, _Args... >::value &&noexcept(_S_not< __inv_res_t< _Fn &, _Args... >
>()))= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *956* of file *std/functional*.

** template<typename _Fn > template<typename... _Args> constexpr
decltype(_S_not< __inv_res_t< _Fn const &&, _Args... > >())
*std::_Not_fn*< _Fn >::operator()(_Args &&... __args) const
&&noexcept(__is_nothrow_invocable< _Fn const &&, _Args... >::value
&&noexcept(_S_not< __inv_res_t< _Fn const &&, _Args... >
>()))= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *959* of file *std/functional*.

** template<typename _Fn > template<typename... _Args> constexpr
decltype(_S_not< __inv_res_t< _Fn const &, _Args... > >())
*std::_Not_fn*< _Fn >::operator()(_Args &&... __args) const
&noexcept(__is_nothrow_invocable< _Fn const &, _Args... >::value
&&noexcept(_S_not< __inv_res_t< _Fn const &, _Args... >
>()))= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *957* of file *std/functional*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
