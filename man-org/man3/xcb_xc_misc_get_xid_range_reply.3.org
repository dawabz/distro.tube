#+TITLE: Manpages - xcb_xc_misc_get_xid_range_reply.3
#+DESCRIPTION: Linux manpage for xcb_xc_misc_get_xid_range_reply.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_xc_misc_get_xid_range_reply.3 is found in manpage for: [[../man3/xcb_xc_misc_get_xid_range.3][man3/xcb_xc_misc_get_xid_range.3]]