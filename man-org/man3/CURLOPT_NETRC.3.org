#+TITLE: Manpages - CURLOPT_NETRC.3
#+DESCRIPTION: Linux manpage for CURLOPT_NETRC.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_NETRC - enable use of .netrc

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_NETRC, long level);

* DESCRIPTION
This parameter controls the preference /level/ of libcurl between using
user names and passwords from your /~/.netrc/ file, relative to user
names and passwords in the URL supplied with /CURLOPT_URL(3)/. On
Windows, libcurl will use the file as /%HOME%/_netrc/, but you can also
tell libcurl a different file name to use with /CURLOPT_NETRC_FILE(3)/.

libcurl uses a user name (and supplied or prompted password) supplied
with /CURLOPT_USERPWD(3)/ or /CURLOPT_USERNAME(3)/ in preference to any
of the options controlled by this parameter.

Only machine name, user name and password are taken into account (init
macros and similar things are not supported).

libcurl does not verify that the file has the correct properties set (as
the standard Unix ftp client does). It should only be readable by user.

/level/ should be set to one of the values described below.

- CURL_NETRC_OPTIONAL :: The use of the /~/.netrc/ file is optional, and
  information in the URL is to be preferred. The file will be scanned
  for the host and user name (to find the password only) or for the host
  only, to find the first user name and password after that /machine/,
  which ever information is not specified.

Undefined values of the option will have this effect.

- CURL_NETRC_IGNORED :: The library will ignore the /~/.netrc/ file.

This is the default.

- CURL_NETRC_REQUIRED :: The use of the /~/.netrc/ file is required, and
  information in the URL is to be ignored. The file will be scanned for
  the host and user name (to find the password only) or for the host
  only, to find the first user name and password after that /machine/,
  which ever information is not specified.

* DEFAULT
CURL_NETRC_IGNORED

* PROTOCOLS
Most

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    CURLcode ret;
    curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/");
    curl_easy_setopt(curl, CURLOPT_NETRC, CURL_NETRC_OPTIONAL);
    ret = curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Always

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*CURLOPT_USERPWD*(3), *CURLOPT_USERNAME*(3), *CURLOPT_NETRC_FILE*(3),
