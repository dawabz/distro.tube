#+TITLE: Manpages - XUngrabDevice.3
#+DESCRIPTION: Linux manpage for XUngrabDevice.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XUngrabDevice.3 is found in manpage for: [[../man3/XGrabDevice.3][man3/XGrabDevice.3]]