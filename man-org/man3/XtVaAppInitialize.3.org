#+TITLE: Manpages - XtVaAppInitialize.3
#+DESCRIPTION: Linux manpage for XtVaAppInitialize.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtVaAppInitialize.3 is found in manpage for: [[../man3/XtAppInitialize.3][man3/XtAppInitialize.3]]