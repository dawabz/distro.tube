#+TITLE: Manpages - FcDirScan.3
#+DESCRIPTION: Linux manpage for FcDirScan.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcDirScan - scan a font directory without caching it

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcDirScan (FcFontSet */set/*, FcStrSet **/dirs/*, FcFileCache
**/cache/*, FcBlanks **/blanks/*, const FcChar8 **/dir/*, FcBool
*/force/*);*

* DESCRIPTION
If /cache/ is not zero or if /force/ is FcFalse, this function currently
returns FcFalse. Otherwise, it scans an entire directory and adds all
fonts found to /set/. Any subdirectories found are added to /dirs/.
Calling this function does not create any cache files. Use
FcDirCacheRead() if caching is desired.
