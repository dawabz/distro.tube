#+TITLE: Manpages - XEvent.3
#+DESCRIPTION: Linux manpage for XEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XEvent.3 is found in manpage for: [[../man3/XAnyEvent.3][man3/XAnyEvent.3]]