#+TITLE: Manpages - __gnu_parallel___count_selector.3
#+DESCRIPTION: Linux manpage for __gnu_parallel___count_selector.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::__count_selector< _It, _Diff > - std::count() selector.

* SYNOPSIS
\\

=#include <for_each_selectors.h>=

Inherits *__gnu_parallel::__generic_for_each_selector< _It >*.

** Public Member Functions
template<typename _ValueType > _Diff *operator()* (_ValueType &__v, _It
__i)\\
Functor execution.

** Public Attributes
_It *_M_finish_iterator*\\
_Iterator on last element processed; needed for some algorithms (e. g.
std::transform()).

* Detailed Description
** "template<typename _It, typename _Diff>
\\
struct __gnu_parallel::__count_selector< _It, _Diff >"std::count()
selector.

Definition at line *180* of file *for_each_selectors.h*.

* Member Function Documentation
** template<typename _It , typename _Diff > template<typename _ValueType
> _Diff *__gnu_parallel::__count_selector*< _It, _Diff >::operator()
(_ValueType & __v, _It __i)= [inline]=
Functor execution.

*Parameters*

#+begin_quote
  /__v/ Current value.\\
  /__i/ iterator referencing object.
#+end_quote

*Returns*

#+begin_quote
  1 if count, 0 if does not count.
#+end_quote

Definition at line *188* of file *for_each_selectors.h*.

* Member Data Documentation
** template<typename _It > _It
*__gnu_parallel::__generic_for_each_selector*< _It
>::_M_finish_iterator= [inherited]=
_Iterator on last element processed; needed for some algorithms (e. g.
std::transform()).

Definition at line *47* of file *for_each_selectors.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
