#+TITLE: Manpages - SDL_PumpEvents.3
#+DESCRIPTION: Linux manpage for SDL_PumpEvents.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_PumpEvents - Pumps the event loop, gathering events from the input
devices.

* SYNOPSIS
*#include "SDL.h"*

*void SDL_PumpEvents*(*void*);

* DESCRIPTION
Pumps the event loop, gathering events from the input devices.

*SDL_PumpEvents* gathers all the pending input information from devices
and places it on the event queue. Without calls to *SDL_PumpEvents* no
events would ever be placed on the queue. Often calls the need for
*SDL_PumpEvents* is hidden from the user since *SDL_PollEvent* and
*SDL_WaitEvent* implicitly call *SDL_PumpEvents*. However, if you are
not polling or waiting for events (e.g. your filtering them), then you
must call *SDL_PumpEvents* to force an event queue update.

#+begin_quote
  *Note: *

  You can only call this function in the thread that set the video mode.
#+end_quote

* SEE ALSO
*SDL_PollEvent*
