#+TITLE: Manpages - XGetPointerControl.3
#+DESCRIPTION: Linux manpage for XGetPointerControl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XGetPointerControl.3 is found in manpage for: [[../man3/XChangePointerControl.3][man3/XChangePointerControl.3]]