#+TITLE: Manpages - xcb_xfixes_fetch_region_rectangles_iterator.3
#+DESCRIPTION: Linux manpage for xcb_xfixes_fetch_region_rectangles_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_xfixes_fetch_region_rectangles_iterator.3 is found in manpage for: [[../man3/xcb_xfixes_fetch_region.3][man3/xcb_xfixes_fetch_region.3]]