#+TITLE: Manpages - CURLOPT_TRANSFER_ENCODING.3
#+DESCRIPTION: Linux manpage for CURLOPT_TRANSFER_ENCODING.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_TRANSFER_ENCODING - ask for HTTP Transfer Encoding

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TRANSFER_ENCODING, long
enable);

* DESCRIPTION
Pass a long set to 1L to /enable/ or 0 to disable.

Adds a request for compressed Transfer Encoding in the outgoing HTTP
request. If the server supports this and so desires, it can respond with
the HTTP response sent using a compressed Transfer-Encoding that will be
automatically uncompressed by libcurl on reception.

Transfer-Encoding differs slightly from the Content-Encoding you ask for
with /CURLOPT_ACCEPT_ENCODING(3)/ in that a Transfer-Encoding is
strictly meant to be for the transfer and thus MUST be decoded before
the data arrives in the client. Traditionally, Transfer-Encoding has
been much less used and supported by both HTTP clients and HTTP servers.

* DEFAULT
0

* PROTOCOLS
HTTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
    curl_easy_setopt(curl, CURLOPT_TRANSFER_ENCODING, 1L);
    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.21.6

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_ACCEPT_ENCODING*(3), *CURLOPT_HTTP_TRANSFER_DECODING*(3),
