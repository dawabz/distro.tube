#+TITLE: Manpages - initstate.3
#+DESCRIPTION: Linux manpage for initstate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about initstate.3 is found in manpage for: [[../man3/random.3][man3/random.3]]