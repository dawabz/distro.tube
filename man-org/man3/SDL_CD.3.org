#+TITLE: Manpages - SDL_CD.3
#+DESCRIPTION: Linux manpage for SDL_CD.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CD - CDROM Drive Information

* STRUCTURE DEFINITION
#+begin_example
  typedef struct{
    int id;
    CDstatus status;
    int numtracks;
    int cur_track;
    int cur_frame;
    SDL_CDtrack track[SDL_MAX_TRACKS+1];
  } SDL_CD;
#+end_example

* STRUCTURE DATA
- *id* :: Private drive identifier

- *status* :: Drive /status/

- *numtracks* :: Number of tracks on the CD

- *cur_track* :: Current track

- *cur_frame* :: Current frame offset within the track

- *track*[SDL_MAX_TRACKS+1] :: Array of track descriptions. (see
  *SDL_CDtrack*)

* DESCRIPTION
An *SDL_CD* structure is returned by *SDL_CDOpen*. It represents an
opened CDROM device and stores information on the layout of the tracks
on the disc.

A frame is the base data unit of a CD. *CD_FPS* frames is equal to 1
second of music. SDL provides two macros for converting between time and
frames: *FRAMES_TO_MSF(f, M,S,F)* and *MSF_TO_FRAMES*.

* EXAMPLES
#+begin_example
  int min, sec, frame;
  int frame_offset;

  FRAMES_TO_MSF(cdrom->cur_frame, &min, &sec, &frame);
  printf("Current Position: %d minutes, %d seconds, %d frames
  ", min, sec, frame);

  frame_offset=MSF_TO_FRAMES(min, sec, frame);
#+end_example

* SEE ALSO
*SDL_CDOpen*, *SDL_CDtrack*
