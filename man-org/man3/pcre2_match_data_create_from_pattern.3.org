#+TITLE: Manpages - pcre2_match_data_create_from_pattern.3
#+DESCRIPTION: Linux manpage for pcre2_match_data_create_from_pattern.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  pcre2_match_data *pcre2_match_data_create_from_pattern(
   const pcre2_code *code, pcre2_general_context *gcontext);
#+end_example

* DESCRIPTION
This function creates a new match data block for holding the result of a
match. The first argument points to a compiled pattern. The number of
capturing parentheses within the pattern is used to compute the number
of pairs of offsets that are required in the match data block. These
form the "output vector" (ovector) within the match data block, and are
used to identify the matched string and any captured substrings when
matching with *pcre2_match()*. If you are using *pcre2_dfa_match()*,
which uses the outut vector in a different way, you should use
*pcre2_match_data_create()* instead of this function.

The second argument points to a general context, for custom memory
management, or is NULL to use the same memory allocator as was used for
the compiled pattern. The result of the function is NULL if the memory
for the block could not be obtained.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
