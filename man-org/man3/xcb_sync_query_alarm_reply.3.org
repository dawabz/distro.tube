#+TITLE: Manpages - xcb_sync_query_alarm_reply.3
#+DESCRIPTION: Linux manpage for xcb_sync_query_alarm_reply.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_sync_query_alarm_reply.3 is found in manpage for: [[../man3/xcb_sync_query_alarm.3][man3/xcb_sync_query_alarm.3]]