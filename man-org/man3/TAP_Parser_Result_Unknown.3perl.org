#+TITLE: Manpages - TAP_Parser_Result_Unknown.3perl
#+DESCRIPTION: Linux manpage for TAP_Parser_Result_Unknown.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
TAP::Parser::Result::Unknown - Unknown result token.

* VERSION
Version 3.43

* DESCRIPTION
This is a subclass of TAP::Parser::Result. A token of this class will be
returned if the parser does not recognize the token line. For example:

1..5 VERSION 7 ok 1 - woo hooo! ... woo hooo! is cool!

In the above TAP, the second and fourth lines will generate Unknown
tokens.

* OVERRIDDEN METHODS
Mainly listed here to shut up the pitiful screams of the pod coverage
tests. They keep me awake at night.

- =as_string=

- =raw=
