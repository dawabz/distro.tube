#+TITLE: Manpages - CURLOPT_SOCKOPTDATA.3
#+DESCRIPTION: Linux manpage for CURLOPT_SOCKOPTDATA.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_SOCKOPTDATA - pointer to pass to sockopt callback

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SOCKOPTDATA, void
*pointer);

* DESCRIPTION
Pass a /pointer/ that will be untouched by libcurl and passed as the
first argument in the sockopt callback set with
/CURLOPT_SOCKOPTFUNCTION(3)/.

* DEFAULT
The default value of this parameter is NULL.

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  static int sockopt_callback(void *clientp, curl_socket_t curlfd,
                              curlsocktype purpose)
  {
    int val = *(int *)clientp;
    setsockopt(curldfd, SOL_SOCKET, SO_RCVBUF, (const char *)&val, sizeof(val));
    return CURL_SOCKOPT_OK;
  }

  curl = curl_easy_init();
  if(curl) {
    int recvbuffersize = 256 * 1024;

    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");

    /* call this function to set options for the socket */
    curl_easy_setopt(curl, CURLOPT_SOCKOPTFUNCTION, sockopt_callback);
    curl_easy_setopt(curl, CURLOPT_SOCKOPTDATA, &recvbuffersize);

    res = curl_easy_perform(curl);

    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.16.0

* RETURN VALUE
Returns /CURLE_OK/ if the option is supported, and
/CURLE_UNKNOWN_OPTION/ if not.

* SEE ALSO
*CURLOPT_SOCKOPTFUNCTION*(3), *CURLOPT_OPENSOCKETFUNCTION*(3),
