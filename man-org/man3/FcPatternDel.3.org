#+TITLE: Manpages - FcPatternDel.3
#+DESCRIPTION: Linux manpage for FcPatternDel.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcPatternDel - Delete a property from a pattern

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcPatternDel (FcPattern */p/*, const char **/object/*);*

* DESCRIPTION
Deletes all values associated with the property `object', returning
whether the property existed or not.
