#+TITLE: Manpages - std__Deque_base.3
#+DESCRIPTION: Linux manpage for std__Deque_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Deque_base< _Tp, _Alloc >

* SYNOPSIS
\\

=#include <stl_deque.h>=

** Protected Types
enum { *_S_initial_map_size* }\\

typedef *__gnu_cxx::__alloc_traits*< _Tp_alloc_type > *_Alloc_traits*\\

typedef *__gnu_cxx::__alloc_traits*< _Map_alloc_type >
*_Map_alloc_traits*\\

typedef _Alloc_traits::template rebind< _Ptr >::other
*_Map_alloc_type*\\

typedef iterator::_Map_pointer *_Map_pointer*\\

typedef _Alloc_traits::pointer *_Ptr*\\

typedef _Alloc_traits::const_pointer *_Ptr_const*\\

typedef *__gnu_cxx::__alloc_traits*< _Alloc >::template rebind< _Tp
>::other *_Tp_alloc_type*\\

typedef _Alloc *allocator_type*\\

typedef *_Deque_iterator*< _Tp, const _Tp &, _Ptr_const >
*const_iterator*\\

typedef *_Deque_iterator*< _Tp, _Tp &, _Ptr > *iterator*\\

** Protected Member Functions
*_Deque_base* (*_Deque_base* &&__x)\\

*_Deque_base* (*_Deque_base* &&__x, const allocator_type &__a)\\

*_Deque_base* (*_Deque_base* &&__x, const allocator_type &__a, size_t
__n)\\

*_Deque_base* (const allocator_type &__a)\\

*_Deque_base* (const allocator_type &__a, size_t __num_elements)\\

*_Deque_base* (size_t __num_elements)\\

_Map_pointer *_M_allocate_map* (size_t __n)\\

_Ptr *_M_allocate_node* ()\\

void *_M_create_nodes* (_Map_pointer __nstart, _Map_pointer __nfinish)\\

void *_M_deallocate_map* (_Map_pointer __p, size_t __n) noexcept\\

void *_M_deallocate_node* (_Ptr __p) noexcept\\

void *_M_destroy_nodes* (_Map_pointer __nstart, _Map_pointer __nfinish)
noexcept\\

_Map_alloc_type *_M_get_map_allocator* () const noexcept\\

const _Tp_alloc_type & *_M_get_Tp_allocator* () const noexcept\\

_Tp_alloc_type & *_M_get_Tp_allocator* () noexcept\\

void *_M_initialize_map* (size_t)\\
Layout storage.

allocator_type *get_allocator* () const noexcept\\

** Protected Attributes
_Deque_impl *_M_impl*\\

* Detailed Description
** "template<typename _Tp, typename _Alloc>
\\
class std::_Deque_base< _Tp, _Alloc >"Deque base class. This class
provides the unified face for deque's allocation. This class's
constructor and destructor allocate and deallocate (but do not
initialize) storage. This makes exception safety easier.

Nothing in this class ever constructs or destroys an actual Tp element.
(Deque handles that itself.) Only/All memory management is performed
here.

Definition at line *408* of file *stl_deque.h*.

* Member Typedef Documentation
** template<typename _Tp , typename _Alloc > typedef
*__gnu_cxx::__alloc_traits*<_Tp_alloc_type> *std::_Deque_base*< _Tp,
_Alloc >::*_Alloc_traits*= [protected]=
Definition at line *413* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > typedef
*__gnu_cxx::__alloc_traits*<_Map_alloc_type> *std::_Deque_base*< _Tp,
_Alloc >::*_Map_alloc_traits*= [protected]=
Definition at line *425* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > typedef
_Alloc_traits::template rebind<_Ptr>::other *std::_Deque_base*< _Tp,
_Alloc >::_Map_alloc_type= [protected]=
Definition at line *424* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > typedef
iterator::_Map_pointer *std::_Deque_base*< _Tp, _Alloc
>::_Map_pointer= [protected]=
Definition at line *485* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > typedef
_Alloc_traits::pointer *std::_Deque_base*< _Tp, _Alloc
>::_Ptr= [protected]=
Definition at line *419* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > typedef
_Alloc_traits::const_pointer *std::_Deque_base*< _Tp, _Alloc
>::_Ptr_const= [protected]=
Definition at line *420* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > typedef
*__gnu_cxx::__alloc_traits*<_Alloc>::template rebind<_Tp>::other
*std::_Deque_base*< _Tp, _Alloc >::_Tp_alloc_type= [protected]=
Definition at line *412* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > typedef _Alloc
*std::_Deque_base*< _Tp, _Alloc >::allocator_type= [protected]=
Definition at line *427* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > typedef
*_Deque_iterator*<_Tp, const _Tp&, _Ptr_const> *std::_Deque_base*< _Tp,
_Alloc >::*const_iterator*= [protected]=
Definition at line *434* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > typedef
*_Deque_iterator*<_Tp, _Tp&, _Ptr> *std::_Deque_base*< _Tp, _Alloc
>::*iterator*= [protected]=
Definition at line *433* of file *stl_deque.h*.

* Member Enumeration Documentation
** template<typename _Tp , typename _Alloc > anonymous
enum= [protected]=
Definition at line *589* of file *stl_deque.h*.

* Constructor & Destructor Documentation
** template<typename _Tp , typename _Alloc > *std::_Deque_base*< _Tp,
_Alloc >::*_Deque_base* ()= [inline]=, = [protected]=
Definition at line *436* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > *std::_Deque_base*< _Tp,
_Alloc >::*_Deque_base* (size_t __num_elements)= [inline]=,
= [protected]=
Definition at line *440* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > *std::_Deque_base*< _Tp,
_Alloc >::*_Deque_base* (const allocator_type & __a, size_t
__num_elements)= [inline]=, = [protected]=
Definition at line *444* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > *std::_Deque_base*< _Tp,
_Alloc >::*_Deque_base* (const allocator_type & __a)= [inline]=,
= [protected]=
Definition at line *448* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > *std::_Deque_base*< _Tp,
_Alloc >::*_Deque_base* (*_Deque_base*< _Tp, _Alloc > &&
__x)= [inline]=, = [protected]=
Definition at line *453* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > *std::_Deque_base*< _Tp,
_Alloc >::*_Deque_base* (*_Deque_base*< _Tp, _Alloc > && __x, const
allocator_type & __a)= [inline]=, = [protected]=
Definition at line *461* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > *std::_Deque_base*< _Tp,
_Alloc >::*_Deque_base* (*_Deque_base*< _Tp, _Alloc > && __x, const
allocator_type & __a, size_t __n)= [inline]=, = [protected]=
Definition at line *465* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > *std::_Deque_base*< _Tp,
_Alloc >::~*_Deque_base*= [protected]=, = [noexcept]=
Definition at line *595* of file *stl_deque.h*.

* Member Function Documentation
** template<typename _Tp , typename _Alloc > _Map_pointer
*std::_Deque_base*< _Tp, _Alloc >::_M_allocate_map (size_t
__n)= [inline]=, = [protected]=
Definition at line *572* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > _Ptr *std::_Deque_base*<
_Tp, _Alloc >::_M_allocate_node ()= [inline]=, = [protected]=
Definition at line *558* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > void *std::_Deque_base*<
_Tp, _Alloc >::_M_create_nodes (_Map_pointer __nstart, _Map_pointer
__nfinish)= [protected]=
Definition at line *655* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > void *std::_Deque_base*<
_Tp, _Alloc >::_M_deallocate_map (_Map_pointer __p, size_t
__n)= [inline]=, = [protected]=, = [noexcept]=
Definition at line *579* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > void *std::_Deque_base*<
_Tp, _Alloc >::_M_deallocate_node (_Ptr __p)= [inline]=, = [protected]=,
= [noexcept]=
Definition at line *565* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > void *std::_Deque_base*<
_Tp, _Alloc >::_M_destroy_nodes (_Map_pointer __nstart, _Map_pointer
__nfinish)= [protected]=, = [noexcept]=
Definition at line *673* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > _Map_alloc_type
*std::_Deque_base*< _Tp, _Alloc >::_M_get_map_allocator ()
const= [inline]=, = [protected]=, = [noexcept]=
Definition at line *554* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > const _Tp_alloc_type &
*std::_Deque_base*< _Tp, _Alloc >::_M_get_Tp_allocator ()
const= [inline]=, = [protected]=, = [noexcept]=
Definition at line *550* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > _Tp_alloc_type &
*std::_Deque_base*< _Tp, _Alloc >::_M_get_Tp_allocator ()= [inline]=,
= [protected]=, = [noexcept]=
Definition at line *546* of file *stl_deque.h*.

** template<typename _Tp , typename _Alloc > void *std::_Deque_base*<
_Tp, _Alloc >::_M_initialize_map (size_t __num_elements)= [protected]=
Layout storage.

*Parameters*

#+begin_quote
  /__num_elements/ The count of T's for which to allocate space at
  first.
#+end_quote

*Returns*

#+begin_quote
  Nothing.
#+end_quote

The initial underlying memory layout is a bit complicated...

Definition at line *616* of file *stl_deque.h*.

References *std::max()*.

** template<typename _Tp , typename _Alloc > allocator_type
*std::_Deque_base*< _Tp, _Alloc >::get_allocator () const= [inline]=,
= [protected]=, = [noexcept]=
Definition at line *430* of file *stl_deque.h*.

* Member Data Documentation
** template<typename _Tp , typename _Alloc > _Deque_impl
*std::_Deque_base*< _Tp, _Alloc >::_M_impl= [protected]=
Definition at line *591* of file *stl_deque.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
