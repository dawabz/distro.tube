#+TITLE: Manpages - sd_bus_message_copy.3
#+DESCRIPTION: Linux manpage for sd_bus_message_copy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_bus_message_copy - Copy the contents of one message to another

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

*int sd_bus_message_copy(sd_bus_message **/m/*, sd_bus_message
**/source/*, int */all/*);*

* DESCRIPTION
*sd_bus_message_copy()* copies the contents from message /source/ to
/m/. If /all/ is false, a single complete type is copied (basic or
container). If /all/ is true, the contents are copied until the end of
the currently open container or the end of /source/.

* RETURN VALUE
On success, this call returns true if anything was copied, and false if
there was nothing to copy. On failure, it returns a negative errno-style
error code.

** Errors
Returned errors may indicate the following problems:

*-EINVAL*

#+begin_quote
  /source/ or /m/ are *NULL*.
#+end_quote

*-EPERM*

#+begin_quote
  Message /m/ has been sealed or /source/ has /not/ been sealed.
#+end_quote

*-ESTALE*

#+begin_quote
  Destination message is in invalid state.
#+end_quote

*-ENXIO*

#+begin_quote
  Destination message cannot be appended to.
#+end_quote

*-ENOMEM*

#+begin_quote
  Memory allocation failed.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-bus*(3), *sd_bus_message_append*(3)
