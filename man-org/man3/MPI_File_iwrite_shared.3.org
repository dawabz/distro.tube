#+TITLE: Manpages - MPI_File_iwrite_shared.3
#+DESCRIPTION: Linux manpage for MPI_File_iwrite_shared.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_File_iwrite_shared* - Writes a file using the shared file pointer
(nonblocking, noncollective).

* SYNTAX
#+begin_example
#+end_example

* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_File_iwrite_shared(MPI_File fh, const void *buf, int count, MPI_Datatype
  	datatype, MPI_Request *request)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_FILE_IWRITE_SHARED(FH, BUF, COUNT, DATATYPE, REQUEST, IERROR)
  	<type>	BUF(*)
  	INTEGER	FH, COUNT, DATATYPE, REQUEST, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_File_iwrite_shared(fh, buf, count, datatype, request, ierror)
  	TYPE(MPI_File), INTENT(IN) :: fh
  	TYPE(*), DIMENSION(..), INTENT(IN), ASYNCHRONOUS :: buf
  	INTEGER, INTENT(IN) :: count
  	TYPE(MPI_Datatype), INTENT(IN) :: datatype
  	TYPE(MPI_Request), INTENT(OUT) :: request
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  MPI::Request MPI::File::Iwrite_shared(const void* buf, int count,
  	const MPI::Datatype& datatype)
#+end_example

* INPUT/OUTPUT PARAMETER
- fh :: File handle (handle).

* INPUT PARAMETERS
- count :: Number of elements in buffer (integer).

- datatype :: Data type of each buffer element (handle).

* OUTPUT PARAMETERS
- buf :: Initial address of buffer (choice).

- request :: Request object (handle).

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
MPI_File_iwrite_shared is a nonblocking routine that uses the shared
file pointer to write files. The order of serialization is not
deterministic for this noncollective routine, so you need to use other
methods of synchronization to impose a particular order.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. For MPI I/O function errors, the default error handler is set to
MPI_ERRORS_RETURN. The error handler may be changed with
MPI_File_set_errhandler; the predefined error handler
MPI_ERRORS_ARE_FATAL may be used to make I/O errors fatal. Note that MPI
does not guarantee that an MPI program can continue past an error.
