#+TITLE: Manpages - __gnu_cxx_unary_compose.3
#+DESCRIPTION: Linux manpage for __gnu_cxx_unary_compose.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::unary_compose< _Operation1, _Operation2 > - An *SGI extension
*.

* SYNOPSIS
\\

Inherits *std::unary_function< _Operation2::argument_type,
_Operation1::result_type >*.

** Public Types
typedef _Operation2::argument_type *argument_type*\\
=argument_type= is the type of the argument

typedef _Operation1::result_type *result_type*\\
=result_type= is the return type

** Public Member Functions
*unary_compose* (const _Operation1 &__x, const _Operation2 &__y)\\

_Operation1::result_type *operator()* (const typename
_Operation2::argument_type &__x) const\\

** Protected Attributes
_Operation1 *_M_fn1*\\

_Operation2 *_M_fn2*\\

* Detailed Description
** "template<class _Operation1, class _Operation2>
\\
class __gnu_cxx::unary_compose< _Operation1, _Operation2 >"An *SGI
extension *.

Definition at line *117* of file *ext/functional*.

* Member Typedef Documentation
** typedef _Operation2::argument_type *std::unary_function*<
_Operation2::argument_type , _Operation1::result_type
>::*argument_type*= [inherited]=
=argument_type= is the type of the argument

Definition at line *108* of file *stl_function.h*.

** typedef _Operation1::result_type *std::unary_function*<
_Operation2::argument_type , _Operation1::result_type
>::*result_type*= [inherited]=
=result_type= is the return type

Definition at line *111* of file *stl_function.h*.

* Constructor & Destructor Documentation
** template<class _Operation1 , class _Operation2 >
*__gnu_cxx::unary_compose*< _Operation1, _Operation2 >::*unary_compose*
(const _Operation1 & __x, const _Operation2 & __y)= [inline]=
Definition at line *126* of file *ext/functional*.

* Member Function Documentation
** template<class _Operation1 , class _Operation2 >
_Operation1::result_type *__gnu_cxx::unary_compose*< _Operation1,
_Operation2 >::operator() (const typename _Operation2::argument_type &
__x) const= [inline]=
Definition at line *130* of file *ext/functional*.

* Member Data Documentation
** template<class _Operation1 , class _Operation2 > _Operation1
*__gnu_cxx::unary_compose*< _Operation1, _Operation2
>::_M_fn1= [protected]=
Definition at line *122* of file *ext/functional*.

** template<class _Operation1 , class _Operation2 > _Operation2
*__gnu_cxx::unary_compose*< _Operation1, _Operation2
>::_M_fn2= [protected]=
Definition at line *123* of file *ext/functional*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
