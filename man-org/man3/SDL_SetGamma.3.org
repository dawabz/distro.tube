#+TITLE: Manpages - SDL_SetGamma.3
#+DESCRIPTION: Linux manpage for SDL_SetGamma.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_SetGamma - Sets the color gamma function for the display

* SYNOPSIS
*#include "SDL.h"*

*int SDL_SetGamma*(*float redgamma, float greengamma, float bluegamma*);

* DESCRIPTION
Sets the "gamma function" for the display of each color component. Gamma
controls the brightness/contrast of colors displayed on the screen. A
gamma value of 1.0 is identity (i.e., no adjustment is made).

This function adjusts the gamma based on the "gamma function" parameter,
you can directly specify lookup tables for gamma adjustment with
/SDL_SetGammaRamp/.

Not all display hardware is able to change gamma.

* RETURN VALUE
Returns -1 on error (or if gamma adjustment is not supported).

* SEE ALSO
/SDL_GetGammaRamp/ /SDL_SetGammaRamp/
