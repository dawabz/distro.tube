#+TITLE: Manpages - CURLOPT_HSTS.3
#+DESCRIPTION: Linux manpage for CURLOPT_HSTS.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_HSTS - HSTS cache file name

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>

  CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HSTS, char *filename);
#+end_example

* DESCRIPTION
Make the /filename/ point to a file name to load an existing HSTS cache
from, and to store the cache in when the easy handle is closed. Setting
a file name with this option will also enable HSTS for this handle (the
equivalent of setting /CURLHSTS_ENABLE/ with /CURLOPT_HSTS_CTRL(3)/).

If the given file does not exist or contains no HSTS entries at startup,
the HSTS cache will simply start empty. Setting the file name to NULL or
"" will only enable HSTS without reading from or writing to any file.

If this option is set multiple times, libcurl will load cache entries
from each given file but will only store the last used name for later
writing.

* FILE FORMAT
The HSTS cache is saved to and loaded from a text file with one entry
per physical line. Each line in the file has the following format:

[host] [stamp]

[host] is the domain name for the entry and the name is dot-prefixed if
it is a includeSubDomain entry (if the entry is valid for all subdmains
to the name as well or only for the exact name).

[stamp] is the time (in UTC) when the entry expires and it uses the
format "YYYYMMDD HH:MM:SS".

Lines starting with "#" are treated as comments and are ignored. There
is currently no length or size limit.

* DEFAULT
NULL, no file name

* PROTOCOLS
HTTPS and HTTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_HSTS, "/home/user/.hsts-cache");
    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.74.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_HSTS_CTRL*(3), *CURLOPT_ALTSVC*(3), *CURLOPT_RESOLVE*(3),
