#+TITLE: Manpages - versionsort.3
#+DESCRIPTION: Linux manpage for versionsort.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about versionsort.3 is found in manpage for: [[../man3/scandir.3][man3/scandir.3]]