#+TITLE: Manpages - SDL_GetCursor.3
#+DESCRIPTION: Linux manpage for SDL_GetCursor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_GetCursor - Get the currently active mouse cursor.

* SYNOPSIS
*#include "SDL.h"*

*SDL_Cursor *SDL_GetCursor*(*void*);

* DESCRIPTION
Returns the currently active mouse cursor.

* SEE ALSO
*SDL_SetCursor*, *SDL_CreateCursor*, *SDL_ShowCursor*
