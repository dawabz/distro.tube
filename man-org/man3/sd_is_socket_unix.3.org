#+TITLE: Manpages - sd_is_socket_unix.3
#+DESCRIPTION: Linux manpage for sd_is_socket_unix.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sd_is_socket_unix.3 is found in manpage for: [[../sd_is_fifo.3][sd_is_fifo.3]]