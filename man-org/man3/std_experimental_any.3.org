#+TITLE: Manpages - std_experimental_any.3
#+DESCRIPTION: Linux manpage for std_experimental_any.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::experimental::any - A type-safe container of any type.

* SYNOPSIS
\\

** Public Member Functions
*any* () noexcept\\
Default constructor, creates an empty object.

template<typename _ValueType , typename _Tp = _Decay<_ValueType>,
typename _Mgr = _Manager<_Tp>, typename *enable_if*< *is_constructible*<
_Tp, _ValueType && >::value, bool >::*type* = true> *any* (_ValueType
&&__value)\\
Construct with a copy of =__value= as the contained object.

template<typename _ValueType , typename _Tp = _Decay<_ValueType>,
typename _Mgr = _Manager<_Tp>, typename *enable_if*<!*is_constructible*<
_Tp, _ValueType && >::value, bool >::*type* = false> *any* (_ValueType
&&__value)\\
Construct with a copy of =__value= as the contained object.

*any* (*any* &&__other) noexcept\\
Move constructor, transfer the state from =__other=.

*any* (const *any* &__other)\\
Copy constructor, copies the state of =__other=.

*~any* ()\\
Destructor, calls =clear()=

void *clear* () noexcept\\
If not empty, destroy the contained object.

bool *empty* () const noexcept\\
Reports whether there is a contained object or not.

template<typename _ValueType > *enable_if_t*<!*is_same*< *any*,
*decay_t*< _ValueType > >::value, *any* & > *operator=* (_ValueType
&&__rhs)\\
Store a copy of =__rhs= as the contained object.

*any* & *operator=* (*any* &&__rhs) noexcept\\
Move assignment operator.

*any* & *operator=* (const *any* &__rhs)\\
Copy the state of another object.

void *swap* (*any* &__rhs) noexcept\\
Exchange state with another object.

const *type_info* & *type* () const noexcept\\
The =typeid= of the contained object, or =typeid(void)= if empty.

** Static Public Member Functions
template<typename _Tp > static constexpr bool *__is_valid_cast* ()\\

** Friends
template<typename _Tp > *enable_if_t*< *is_object*< _Tp >::value, void *
> *__any_caster* (const *any* *__any)\\

* Detailed Description
A type-safe container of any type.

An =any= object's state is either empty or it stores a contained object
of CopyConstructible type.

Definition at line *90* of file *experimental/any*.

* Constructor & Destructor Documentation
** std::experimental::any::any ()= [inline]=, = [noexcept]=
Default constructor, creates an empty object.

Definition at line *129* of file *experimental/any*.

** std::experimental::any::any (const *any* & __other)= [inline]=
Copy constructor, copies the state of =__other=.

Definition at line *132* of file *experimental/any*.

** std::experimental::any::any (*any* && __other)= [inline]=,
= [noexcept]=
Move constructor, transfer the state from =__other=.

*Postcondition*

#+begin_quote
  =__other.empty()= (this postcondition is a GNU extension)
#+end_quote

Definition at line *149* of file *experimental/any*.

** template<typename _ValueType , typename _Tp = _Decay<_ValueType>,
typename _Mgr = _Manager<_Tp>, typename *enable_if*< *is_constructible*<
_Tp, _ValueType && >::value, bool >::*type* = true>
std::experimental::any::any (_ValueType && __value)= [inline]=
Construct with a copy of =__value= as the contained object.

Definition at line *166* of file *experimental/any*.

** template<typename _ValueType , typename _Tp = _Decay<_ValueType>,
typename _Mgr = _Manager<_Tp>, typename *enable_if*<!*is_constructible*<
_Tp, _ValueType && >::value, bool >::*type* = false>
std::experimental::any::any (_ValueType && __value)= [inline]=
Construct with a copy of =__value= as the contained object.

Definition at line *179* of file *experimental/any*.

** std::experimental::any::~any ()= [inline]=
Destructor, calls =clear()=

Definition at line *188* of file *experimental/any*.

* Member Function Documentation
** template<typename _Tp > static constexpr bool
std::experimental::any::__is_valid_cast ()= [inline]=, = [static]=,
= [constexpr]=
Definition at line *287* of file *experimental/any*.

** void std::experimental::any::clear ()= [inline]=, = [noexcept]=
If not empty, destroy the contained object.

Definition at line *230* of file *experimental/any*.

** bool std::experimental::any::empty () const= [inline]=, = [noexcept]=
Reports whether there is a contained object or not.

Definition at line *272* of file *experimental/any*.

** template<typename _ValueType > *enable_if_t*<!*is_same*< *any*,
*decay_t*< _ValueType > >::value, *any* & >
std::experimental::any::operator= (_ValueType && __rhs)= [inline]=
Store a copy of =__rhs= as the contained object.

Definition at line *221* of file *experimental/any*.

** *any* & std::experimental::any::operator= (*any* &&
__rhs)= [inline]=, = [noexcept]=
Move assignment operator.

*Postcondition*

#+begin_quote
  =__rhs.empty()= (not guaranteed for other implementations)
#+end_quote

Definition at line *204* of file *experimental/any*.

** *any* & std::experimental::any::operator= (const *any* &
__rhs)= [inline]=
Copy the state of another object.

Definition at line *193* of file *experimental/any*.

** void std::experimental::any::swap (*any* & __rhs)= [inline]=,
= [noexcept]=
Exchange state with another object.

Definition at line *240* of file *experimental/any*.

** const *type_info* & std::experimental::any::type () const= [inline]=,
= [noexcept]=
The =typeid= of the contained object, or =typeid(void)= if empty.

Definition at line *276* of file *experimental/any*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
