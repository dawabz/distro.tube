#+TITLE: Manpages - XkbKeycodeToKeysym.3
#+DESCRIPTION: Linux manpage for XkbKeycodeToKeysym.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbKeycodeToKeysym - Finds the keysym bound to a particular key at a
specified group and shift level

* SYNOPSIS
*KeySym XkbKeycodeToKeysym* *( Display **/dpy/* ,* *KeyCode */kc/* ,*
*unsigned int */group/* ,* *unsigned int */level/* );*

* ARGUMENTS
- /- dpy/ :: connection to X server

- /- kc/ :: key of interest

- /- group/ :: group of interest

- /- level/ :: shift level of interest

* DESCRIPTION
/XkbKeycodeToKeysym/ returns the keysym bound to a particular group and
shift level for a particular key on the core keyboard. If /kc/ is not a
legal keycode for the core keyboard, or if /group/ or /level/ are out of
range for the specified key, /XkbKeycodeToKeysym/ returns NoSymbol.
