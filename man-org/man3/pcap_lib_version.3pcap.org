#+TITLE: Manpages - pcap_lib_version.3pcap
#+DESCRIPTION: Linux manpage for pcap_lib_version.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_lib_version - get the version information for libpcap

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  const char *pcap_lib_version(void);
#+end_example

* DESCRIPTION
*pcap_lib_version*() returns a pointer to a string giving information
about the version of the libpcap library being used; note that it
contains more information than just a version number.

* SEE ALSO
*pcap*(3PCAP)
