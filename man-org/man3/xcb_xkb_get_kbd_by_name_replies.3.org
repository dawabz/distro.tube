#+TITLE: Manpages - xcb_xkb_get_kbd_by_name_replies.3
#+DESCRIPTION: Linux manpage for xcb_xkb_get_kbd_by_name_replies.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_xkb_get_kbd_by_name_replies.3 is found in manpage for: [[../man3/xcb_xkb_get_kbd_by_name.3][man3/xcb_xkb_get_kbd_by_name.3]]