#+TITLE: Manpages - llroundl.3
#+DESCRIPTION: Linux manpage for llroundl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about llroundl.3 is found in manpage for: [[../man3/lround.3][man3/lround.3]]