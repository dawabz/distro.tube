#+TITLE: Manpages - FcCharSetCreate.3
#+DESCRIPTION: Linux manpage for FcCharSetCreate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcCharSetCreate - Create an empty character set

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcCharSet * FcCharSetCreate (void*);*

* DESCRIPTION
*FcCharSetCreate* allocates and initializes a new empty character set
object.
