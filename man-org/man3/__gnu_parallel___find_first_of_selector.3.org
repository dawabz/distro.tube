#+TITLE: Manpages - __gnu_parallel___find_first_of_selector.3
#+DESCRIPTION: Linux manpage for __gnu_parallel___find_first_of_selector.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::__find_first_of_selector< _FIterator > - Test predicate
on several elements.

* SYNOPSIS
\\

=#include <find_selectors.h>=

Inherits *__gnu_parallel::__generic_find_selector*.

** Public Member Functions
*__find_first_of_selector* (_FIterator __begin, _FIterator __end)\\

template<typename _RAIter1 , typename _RAIter2 , typename _Pred >
*std::pair*< _RAIter1, _RAIter2 > *_M_sequential_algorithm* (_RAIter1
__begin1, _RAIter1 __end1, _RAIter2 __begin2, _Pred __pred)\\
Corresponding sequential algorithm on a sequence.

template<typename _RAIter1 , typename _RAIter2 , typename _Pred > bool
*operator()* (_RAIter1 __i1, _RAIter2 __i2, _Pred __pred)\\
Test on one position.

** Public Attributes
_FIterator *_M_begin*\\

_FIterator *_M_end*\\

* Detailed Description
** "template<typename _FIterator>
\\
struct __gnu_parallel::__find_first_of_selector< _FIterator >"Test
predicate on several elements.

Definition at line *153* of file *find_selectors.h*.

* Constructor & Destructor Documentation
** template<typename _FIterator >
*__gnu_parallel::__find_first_of_selector*< _FIterator
>::*__find_first_of_selector* (_FIterator __begin, _FIterator
__end)= [inline]=, = [explicit]=
Definition at line *158* of file *find_selectors.h*.

* Member Function Documentation
** template<typename _FIterator > template<typename _RAIter1 , typename
_RAIter2 , typename _Pred > *std::pair*< _RAIter1, _RAIter2 >
*__gnu_parallel::__find_first_of_selector*< _FIterator
>::_M_sequential_algorithm (_RAIter1 __begin1, _RAIter1 __end1, _RAIter2
__begin2, _Pred __pred)= [inline]=
Corresponding sequential algorithm on a sequence.

*Parameters*

#+begin_quote
  /__begin1/ Begin iterator of first sequence.\\
  /__end1/ End iterator of first sequence.\\
  /__begin2/ Begin iterator of second sequence.\\
  /__pred/ Find predicate.
#+end_quote

Definition at line *186* of file *find_selectors.h*.

** template<typename _FIterator > template<typename _RAIter1 , typename
_RAIter2 , typename _Pred > bool
*__gnu_parallel::__find_first_of_selector*< _FIterator >::operator()
(_RAIter1 __i1, _RAIter2 __i2, _Pred __pred)= [inline]=
Test on one position.

*Parameters*

#+begin_quote
  /__i1/ _Iterator on first sequence.\\
  /__i2/ _Iterator on second sequence (unused).\\
  /__pred/ Find predicate.
#+end_quote

Definition at line *169* of file *find_selectors.h*.

* Member Data Documentation
** template<typename _FIterator > _FIterator
*__gnu_parallel::__find_first_of_selector*< _FIterator >::_M_begin
Definition at line *155* of file *find_selectors.h*.

** template<typename _FIterator > _FIterator
*__gnu_parallel::__find_first_of_selector*< _FIterator >::_M_end
Definition at line *156* of file *find_selectors.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
