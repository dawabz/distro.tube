#+TITLE: Manpages - vfscanf.3
#+DESCRIPTION: Linux manpage for vfscanf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about vfscanf.3 is found in manpage for: [[../man3/scanf.3][man3/scanf.3]]