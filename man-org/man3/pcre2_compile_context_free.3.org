#+TITLE: Manpages - pcre2_compile_context_free.3
#+DESCRIPTION: Linux manpage for pcre2_compile_context_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  void pcre2_compile_context_free(pcre2_compile_context *ccontext);
#+end_example

* DESCRIPTION
This function frees the memory occupied by a compile context, using the
memory freeing function from the general context with which it was
created, or *free()* if that was not set. If the argument is NULL, the
function returns immediately without doing anything.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
