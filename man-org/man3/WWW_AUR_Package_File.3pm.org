#+TITLE: Manpages - WWW_AUR_Package_File.3pm
#+DESCRIPTION: Linux manpage for WWW_AUR_Package_File.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
WWW::AUR::Package::File - Load, extract, and build a source package file

* SYNOPSIS
use WWW::AUR::Package::File; my $fileobj = WWW::AUR::Package::File->new(
package.src.tar.gz ); $fileobj->extract(); $fileobj->build(); my $pbobj
= $fileobj->pkgbuild my %pbfields = $pbobj->fields(); print "Package
file path : %s\n", $fileobj->src_pkg_path; print "Extracted dir : %s\n",
$fileobj->src_dir_path; print "Built package path : %s\n",
$fileobj->bin_pkg_path;

* CONSTRUCTOR
$OBJ = WWW::AUR::Package::File->new( $PATH, %PATH_PARAMS? );

- $PATH :: The path to a source package file. These typically end with
  the .src.tar.gz suffix.

- %PATH_PARAMS (Optional) :: Optional path parameters. See PATH
  PARAMETERS in WWW::AUR.

* METHODS
** extract
$SRCPKGDIR = $OBJ->extract;

- $SRCPKGDIR :: The absolute path to the directory where the source
  package was extracted. (This is the directory that is contained in the
  source package file, extracted)

** build
$BINPKGDIR = $OBJ->build( %BUILD_PARAMS? );

Builds the AUR package, using the makepkg utility.

- %BUILD_PARAMS (Optional) :: Path parameters can be mixed with build
  parameters. Several build parameters can be used to provide arguments
  to makepkg. Build parameter keys:

  - pkgdest :: Overrides where to store the built binary package file.

  - quiet :: If set to a true value the makepkg output is redirected to
    //dev/null/.

  - prefix :: A string to prefix before the makepkg command.

  - args :: A string to append to the makepkg command as arguments.

- $BINPKGDIR :: The absolute path to the binary package that was created
  by running makepkg.

- Errors :: 

  - 

  /makepkg failed to run: signal =%d=./

  - /makepkg failed to run: error code =%d=./

  - /makepkg succeeded but the package file is missing./

** pkgbuild
$PKGBUILD_OBJ = $OBJ->pkgbuild;

Create an object representing the PKGBUILD file of a source package. A
PKGBUILD is the main component of a source package. If the source
package archive has not been extracted yet, extract will be called
automatically.

- $PKGBUILD_OBJ :: A WWW::AUR::PKGBUILD object representing the PKGBUILD
  in the extracted source package directory.

** src_pkg_path
undef | $PATH = $OBJ->src_pkg_path;

If download has been called, then the path of the downloaded source
package file is returned. Otherwise =undef= is returned.

** src_dir_path
undef | $PATH = $OBJ->src_dir_path;

If extract has been called, then the path of the extract source package
dir is returned. Otherwise =undef= is returned.

** bin_pkg_path
undef | $PATH = $OBJ->bin_pkg_path;

If build has been called, then the path of the built binary package is
returned. Otherwise =undef= is returned.

** make_src_path
$PATH = $OBJ->make_src_path( $RELPATH )

Helper function to easily lookup the absolute path to a file inside the
source directory. This just builds the path it does not guarantee the
file exists!

- $RELPATH :: The relative path to a file /inside/ the extracted source
  directory. This is allowed to have a leading forward-slash.

- $PATH :: The absolute path to the file inside the source directory.

/Errors/

- You must call extract() before make_src_path() :: 

* SEE ALSO

- 

WWW::AUR::Package

- WWW::AUR::PKGBUILD

- <http://www.archlinux.org/pacman/makepkg.8.html>

* AUTHOR
Justin Davis, =<juster at cpan dot org>=

* BUGS
Please email me any bugs you find. I will try to fix them as quick as I
can.

* SUPPORT
Send me an email if you have any questions or need help.

* LICENSE AND COPYRIGHT
Copyright 2014 Justin Davis.

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.
