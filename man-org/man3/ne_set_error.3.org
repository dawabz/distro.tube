#+TITLE: Manpages - ne_set_error.3
#+DESCRIPTION: Linux manpage for ne_set_error.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ne_set_error.3 is found in manpage for: [[../ne_get_error.3][ne_get_error.3]]