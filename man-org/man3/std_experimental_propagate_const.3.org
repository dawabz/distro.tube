#+TITLE: Manpages - std_experimental_propagate_const.3
#+DESCRIPTION: Linux manpage for std_experimental_propagate_const.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::experimental::propagate_const< _Tp > - Const-propagating wrapper.

* SYNOPSIS
\\

** Public Types
typedef *remove_reference_t*< decltype(*std::declval< _Tp & >())>
*element_type*\\

** Public Member Functions
template<typename _Up , typename *enable_if*< __and_<
*is_constructible*< _Tp, _Up && >, *is_convertible*< _Up &&, _Tp >,
__not_< __is_propagate_const< typename *decay*< _Up >::type > >
>::value, bool >::type = true> constexpr *propagate_const* (_Up &&__u)\\

template<typename _Up , typename *enable_if*< __and_<
*is_constructible*< _Tp, _Up && >, __not_< *is_convertible*< _Up &&, _Tp
> >, __not_< __is_propagate_const< typename *decay*< _Up >::type > >
>::value, bool >::type = false> constexpr *propagate_const* (_Up
&&__u)\\

*propagate_const* (const *propagate_const* &__p)=delete\\

constexpr *propagate_const* (*propagate_const* &&__p)=default\\

template<typename _Up , typename *enable_if*< __and_<
*is_constructible*< _Tp, _Up && >, *is_convertible*< _Up &&, _Tp >
>::value, bool >::type = true> constexpr *propagate_const*
(*propagate_const*< _Up > &&__pu)\\

template<typename _Up , typename *enable_if*< __and_<
*is_constructible*< _Tp, _Up && >, __not_< *is_convertible*< _Up &&, _Tp
> > >::value, bool >::type = false> constexpr *propagate_const*
(*propagate_const*< _Up > &&__pu)\\

constexpr element_type * *get* ()\\

constexpr const element_type * *get* () const\\

constexpr *operator bool* () const\\

template<typename _Up = _Tp, typename *enable_if*< __or_< *is_pointer*<
_Up >, *is_convertible*< _Up, const element_type * > >::value, bool
>::type = true> constexpr *operator const element_type ** () const\\

template<typename _Up = _Tp, typename *enable_if*< __or_< *is_pointer*<
_Up >, *is_convertible*< _Up, const element_type * > >::value, bool
>::type = true> constexpr *operator element_type ** ()\\

constexpr element_type & *operator** ()\\

constexpr const element_type & *operator** () const\\

constexpr element_type * *operator->* ()\\

constexpr const element_type * *operator->* () const\\

template<typename _Up , typename = typename
enable_if<__and_<is_convertible<_Up&&, _Tp>,
__not_<__is_propagate_const< typename decay<_Up>::type>>
>::value>::type> constexpr *propagate_const* & *operator=* (_Up &&__u)\\

*propagate_const* & *operator=* (const *propagate_const* &__p)=delete\\

constexpr *propagate_const* & *operator=* (*propagate_const*
&&__p)=default\\

template<typename _Up , typename = typename
enable_if<is_convertible<_Up&&, _Tp>::value>::type> constexpr
*propagate_const* & *operator=* (*propagate_const*< _Up > &&__pu)\\

constexpr void *swap* (*propagate_const* &__pt)
noexcept(__is_nothrow_swappable< _Tp >::value)\\

** Friends
template<typename _Up > constexpr friend const _Up & *get_underlying*
(const *propagate_const*< _Up > &__pt) noexcept\\

template<typename _Up > constexpr friend _Up & *get_underlying*
(*propagate_const*< _Up > &__pt) noexcept\\

* Detailed Description
** "template<typename _Tp>
\\
class std::experimental::propagate_const< _Tp >"Const-propagating
wrapper.

Definition at line *64* of file *propagate_const*.

* Member Typedef Documentation
** template<typename _Tp > typedef
*remove_reference_t*<decltype(*std::declval<_Tp&>())>
*std::experimental::propagate_const*< _Tp >::element_type
Definition at line *67* of file *propagate_const*.

* Constructor & Destructor Documentation
** template<typename _Tp > template<typename _Up , typename *enable_if*<
__and_< *is_constructible*< _Tp, _Up && >, *is_convertible*< _Up &&, _Tp
> >::value, bool >::type = true> constexpr
*std::experimental::propagate_const*< _Tp >::*propagate_const*
(*propagate_const*< _Up > && __pu)= [inline]=, = [constexpr]=
Definition at line *120* of file *propagate_const*.

** template<typename _Tp > template<typename _Up , typename *enable_if*<
__and_< *is_constructible*< _Tp, _Up && >, __not_< *is_convertible*< _Up
&&, _Tp > > >::value, bool >::type = false> constexpr
*std::experimental::propagate_const*< _Tp >::*propagate_const*
(*propagate_const*< _Up > && __pu)= [inline]=, = [explicit]=,
= [constexpr]=
Definition at line *127* of file *propagate_const*.

** template<typename _Tp > template<typename _Up , typename *enable_if*<
__and_< *is_constructible*< _Tp, _Up && >, *is_convertible*< _Up &&, _Tp
>, __not_< __is_propagate_const< typename *decay*< _Up >::type > >
>::value, bool >::type = true> constexpr
*std::experimental::propagate_const*< _Tp >::*propagate_const* (_Up &&
__u)= [inline]=, = [constexpr]=
Definition at line *136* of file *propagate_const*.

** template<typename _Tp > template<typename _Up , typename *enable_if*<
__and_< *is_constructible*< _Tp, _Up && >, __not_< *is_convertible*< _Up
&&, _Tp > >, __not_< __is_propagate_const< typename *decay*< _Up >::type
> > >::value, bool >::type = false> constexpr
*std::experimental::propagate_const*< _Tp >::*propagate_const* (_Up &&
__u)= [inline]=, = [explicit]=, = [constexpr]=
Definition at line *145* of file *propagate_const*.

* Member Function Documentation
** template<typename _Tp > constexpr element_type *
*std::experimental::propagate_const*< _Tp >::get ()= [inline]=,
= [constexpr]=
Definition at line *224* of file *propagate_const*.

** template<typename _Tp > constexpr const element_type *
*std::experimental::propagate_const*< _Tp >::get () const= [inline]=,
= [constexpr]=
Definition at line *198* of file *propagate_const*.

** template<typename _Tp > constexpr
*std::experimental::propagate_const*< _Tp >::operator bool ()
const= [inline]=, = [explicit]=, = [constexpr]=
Definition at line *173* of file *propagate_const*.

** template<typename _Tp > template<typename _Up = _Tp, typename
*enable_if*< __or_< *is_pointer*< _Up >, *is_convertible*< _Up, const
element_type * > >::value, bool >::type = true> constexpr
*std::experimental::propagate_const*< _Tp >::operator const element_type
* () const= [inline]=, = [constexpr]=
Definition at line *188* of file *propagate_const*.

** template<typename _Tp > template<typename _Up = _Tp, typename
*enable_if*< __or_< *is_pointer*< _Up >, *is_convertible*< _Up, const
element_type * > >::value, bool >::type = true> constexpr
*std::experimental::propagate_const*< _Tp >::operator element_type *
()= [inline]=, = [constexpr]=
Definition at line *214* of file *propagate_const*.

** template<typename _Tp > constexpr element_type &
*std::experimental::propagate_const*< _Tp >::operator* ()= [inline]=,
= [constexpr]=
Definition at line *219* of file *propagate_const*.

** template<typename _Tp > constexpr const element_type &
*std::experimental::propagate_const*< _Tp >::operator* ()
const= [inline]=, = [constexpr]=
Definition at line *193* of file *propagate_const*.

** template<typename _Tp > constexpr element_type *
*std::experimental::propagate_const*< _Tp >::operator-> ()= [inline]=,
= [constexpr]=
Definition at line *204* of file *propagate_const*.

** template<typename _Tp > constexpr const element_type *
*std::experimental::propagate_const*< _Tp >::operator-> ()
const= [inline]=, = [constexpr]=
Definition at line *178* of file *propagate_const*.

** template<typename _Tp > template<typename _Up , typename = typename
enable_if<__and_<is_convertible<_Up&&, _Tp>,
__not_<__is_propagate_const< typename decay<_Up>::type>>
>::value>::type> constexpr *propagate_const* &
*std::experimental::propagate_const*< _Tp >::operator= (_Up &&
__u)= [inline]=, = [constexpr]=
Definition at line *166* of file *propagate_const*.

** template<typename _Tp > template<typename _Up , typename = typename
enable_if<is_convertible<_Up&&, _Tp>::value>::type> constexpr
*propagate_const* & *std::experimental::propagate_const*< _Tp
>::operator= (*propagate_const*< _Up > && __pu)= [inline]=,
= [constexpr]=
Definition at line *155* of file *propagate_const*.

** template<typename _Tp > constexpr void
*std::experimental::propagate_const*< _Tp >::swap (*propagate_const*<
_Tp > & __pt)= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *231* of file *propagate_const*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
