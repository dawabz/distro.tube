#+TITLE: Manpages - gnutls_init.3
#+DESCRIPTION: Linux manpage for gnutls_init.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_init - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*int gnutls_init(gnutls_session_t * */session/*, unsigned int
*/flags/*);*

* ARGUMENTS
- gnutls_session_t * session :: is a pointer to a *gnutls_session_t*
  type.

- unsigned int flags :: indicate if this session is to be used for
  server or client.

* DESCRIPTION
This function initializes the provided session. Every session must be
initialized before use, and must be deinitialized after used by calling
*gnutls_deinit()*.

/flags/ can be any combination of flags from *gnutls_init_flags_t*.

Note that since version 3.1.2 this function enables some common TLS
extensions such as session tickets and OCSP certificate status request
in client side by default. To prevent that use the
*GNUTLS_NO_EXTENSIONS* flag.

* RETURNS
*GNUTLS_E_SUCCESS* on success, or an error code.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
