#+TITLE: Manpages - sd_event_add_io.3
#+DESCRIPTION: Linux manpage for sd_event_add_io.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_event_add_io, sd_event_source_get_io_events,
sd_event_source_set_io_events, sd_event_source_get_io_revents,
sd_event_source_get_io_fd, sd_event_source_set_io_fd,
sd_event_source_get_io_fd_own, sd_event_source_set_io_fd_own,
sd_event_source, sd_event_io_handler_t - Add an I/O event source to an
event loop

* SYNOPSIS
#+begin_example
  #include <systemd/sd-event.h>
#+end_example

#+begin_example
  typedef struct sd_event_source sd_event_source;
#+end_example

*typedef int (*sd_event_io_handler_t)(sd_event_source **/s/*, int
*/fd/*, uint32_t */revents/*, void **/userdata/*);*

*int sd_event_add_io(sd_event **/event/*, sd_event_source ***/source/*,
int */fd/*, uint32_t */events/*, sd_event_io_handler_t */handler/*, void
**/userdata/*);*

*int sd_event_source_get_io_events(sd_event_source **/source/*, uint32_t
**/events/*);*

*int sd_event_source_set_io_events(sd_event_source **/source/*, uint32_t
*/events/*);*

*int sd_event_source_get_io_revents(sd_event_source **/source/*,
uint32_t **/revents/*);*

*int sd_event_source_get_io_fd(sd_event_source **/source/*);*

*int sd_event_source_set_io_fd(sd_event_source **/source/*, int
*/fd/*);*

*int sd_event_source_get_io_fd_own(sd_event_source **/source/*);*

*int sd_event_source_set_io_fd_own(sd_event_source **/source/*, int
*/b/*);*

* DESCRIPTION
*sd_event_add_io()* adds a new I/O event source to an event loop. The
event loop object is specified in the /event/ parameter, the event
source object is returned in the /source/ parameter. The /fd/ parameter
takes the UNIX file descriptor to watch, which may refer to a socket, a
FIFO, a message queue, a serial connection, a character device, or any
other file descriptor compatible with Linux *epoll*(7). The /events/
parameter takes a bit mask of events to watch for, a combination of the
following event flags: *EPOLLIN*, *EPOLLOUT*, *EPOLLRDHUP*, *EPOLLPRI*,
and *EPOLLET*, see *epoll_ctl*(2) for details. The /handler/ shall
reference a function to call when the event source is triggered. The
/userdata/ pointer will be passed to the handler function, and may be
chosen freely by the caller. The handler will also be passed the file
descriptor the event was seen on, as well as the actual event flags. Its
generally a subset of the events watched, however may additionally
include *EPOLLERR* and *EPOLLHUP*.

By default, an event source will stay enabled continuously
(*SD_EVENT_ON*), but this may be changed with
*sd_event_source_set_enabled*(3). If the handler function returns a
negative error code, it will be disabled after the invocation, even if
the *SD_EVENT_ON* mode was requested before. Note that an event source
set to *SD_EVENT_ON* will fire continuously unless data is read from or
written to the file descriptor to reset the mask of events seen.

Setting the I/O event mask to watch for to 0 does not mean that the
event source wont be triggered anymore, as *EPOLLHUP* and *EPOLLERR* may
be triggered even with a zero event mask. To temporarily disable an I/O
event source use *sd_event_source_set_enabled*(3) with *SD_EVENT_OFF*
instead.

To destroy an event source object use *sd_event_source_unref*(3), but
note that the event source is only removed from the event loop when all
references to the event source are dropped. To make sure an event source
does not fire anymore, even if it is still referenced, disable the event
source using *sd_event_source_set_enabled*(3) with *SD_EVENT_OFF*.

If the second parameter of *sd_event_add_io()* is *NULL* no reference to
the event source object is returned. In this case the event source is
considered "floating", and will be destroyed implicitly when the event
loop itself is destroyed.

If the /handler/ to *sd_event_add_io()* is *NULL*, and the event source
fires, this will be considered a request to exit the event loop. In this
case, the /userdata/ parameter, cast to an integer, is passed as the
exit code parameter to *sd_event_exit*(3).

Note that this call does not take possession of the file descriptor
passed in, ownership (and thus the duty to close it when it is no longer
needed) remains with the caller. However, with the
*sd_event_source_set_io_fd_own()* call (see below) the event source may
optionally take ownership of the file descriptor after the event source
has been created. In that case the file descriptor is closed
automatically as soon as the event source is released.

It is recommended to use *sd_event_add_io()* only in conjunction with
file descriptors that have *O_NONBLOCK* set, to ensure that all I/O
operations from invoked handlers are properly asynchronous and
non-blocking. Using file descriptors without *O_NONBLOCK* might result
in unexpected starvation of other event sources. See *fcntl*(2) for
details on enabling *O_NONBLOCK* mode.

*sd_event_source_get_io_events()* retrieves the configured mask of
watched I/O events of an event source created previously with
*sd_event_add_io()*. It takes the event source object and a pointer to a
variable to store the mask in.

*sd_event_source_set_io_events()* configures the mask of watched I/O
events of an event source created previously with *sd_event_add_io()*.
It takes the event source object and the new event mask.

*sd_event_source_get_io_revents()* retrieves the I/O event mask of
currently seen but undispatched events from an event source created
previously with *sd_event_add_io()*. It takes the event source object
and a pointer to a variable to store the event mask in. When called from
a handler function on the handlers event source object this will return
the same mask as passed to the handlers /revents/ parameter. This call
is primarily useful to check for undispatched events of an event source
from the handler of an unrelated (possibly higher priority) event
source. Note the relation between *sd_event_source_get_pending()* and
*sd_event_source_get_io_revents()*: both functions will report non-zero
results when theres an event pending for the event source, but the
former applies to all event source types, the latter only to I/O event
sources.

*sd_event_source_get_io_fd()* retrieves the UNIX file descriptor of an
event source created previously with *sd_event_add_io()*. It takes the
event source object and returns the non-negative file descriptor or a
negative error number on error (see below).

*sd_event_source_set_io_fd()* changes the UNIX file descriptor of an I/O
event source created previously with *sd_event_add_io()*. It takes the
event source object and the new file descriptor.

*sd_event_source_set_io_fd_own()* controls whether the file descriptor
of the event source shall be closed automatically when the event source
is freed, i.e. whether it shall be considered owned by the event source
object. By default it is not closed automatically, and the application
has to do this on its own. The /b/ parameter is a boolean parameter: if
zero, the file descriptor is not closed automatically when the event
source is freed, otherwise it is closed.

*sd_event_source_get_io_fd_own()* may be used to query the current
setting of the file descriptor ownership boolean flag as set with
*sd_event_source_set_io_fd_own()*. It returns positive if the file
descriptor is closed automatically when the event source is destroyed,
zero if not, and negative on error.

* RETURN VALUE
On success, these functions return 0 or a positive integer. On failure,
they return a negative errno-style error code.

** Errors
Returned values may indicate the following problems:

*-ENOMEM*

#+begin_quote
  Not enough memory to allocate an object.
#+end_quote

*-EINVAL*

#+begin_quote
  An invalid argument has been passed.
#+end_quote

*-ESTALE*

#+begin_quote
  The event loop is already terminated.
#+end_quote

*-ECHILD*

#+begin_quote
  The event loop has been created in a different process.
#+end_quote

*-EDOM*

#+begin_quote
  The passed event source is not an I/O event source.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-event*(3), *sd_event_new*(3), *sd_event_now*(3),
*sd_event_add_time*(3), *sd_event_add_signal*(3),
*sd_event_add_child*(3), *sd_event_add_inotify*(3),
*sd_event_add_defer*(3), *sd_event_source_set_enabled*(3),
*sd_event_source_set_priority*(3), *sd_event_source_set_userdata*(3),
*sd_event_source_set_description*(3), *sd_event_source_get_pending*(3),
*sd_event_source_set_floating*(3), *epoll_ctl*(2), *epoll*(7)
