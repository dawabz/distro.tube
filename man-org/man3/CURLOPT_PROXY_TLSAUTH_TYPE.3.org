#+TITLE: Manpages - CURLOPT_PROXY_TLSAUTH_TYPE.3
#+DESCRIPTION: Linux manpage for CURLOPT_PROXY_TLSAUTH_TYPE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_PROXY_TLSAUTH_TYPE - HTTPS proxy TLS authentication methods

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_TLSAUTH_TYPE, char
*type);

* DESCRIPTION
Pass a pointer to a null-terminated string as parameter. The string
should be the method of the TLS authentication used for the HTTPS
connection. Supported method is "SRP".

- SRP :: TLS-SRP authentication. Secure Remote Password authentication
  for TLS is defined in RFC5054 and provides mutual authentication if
  both sides have a shared secret. To use TLS-SRP, you must also set the
  /CURLOPT_PROXY_TLSAUTH_USERNAME(3)/ and
  /CURLOPT_PROXY_TLSAUTH_PASSWORD(3)/ options.

The application does not have to keep the string around after setting
this option.

* DEFAULT
blank

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    curl_easy_setopt(curl, CURLOPT_PROXY, "https://proxy");
    curl_easy_setopt(curl, CURLOPT_PROXY_TLSAUTH_TYPE, "SRP");
    curl_easy_setopt(curl, CURLOPT_PROXY_TLSAUTH_USERNAME, "user");
    curl_easy_setopt(curl, CURLOPT_PROXY_TLSAUTH_PASSWORD, "secret");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.52.0

You need to build libcurl with GnuTLS or OpenSSL with TLS-SRP support
for this to work.

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_PROXY_TLSAUTH_USERNAME*(3),
*CURLOPT_PROXY_TLSAUTH_PASSWORD*(3), *CURLOPT_TLSAUTH_USERNAME*(3),
*CURLOPT_TLSAUTH_PASSWORD*(3),
