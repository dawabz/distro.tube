#+TITLE: Manpages - MPI_Type_f2c.3
#+DESCRIPTION: Linux manpage for MPI_Type_f2c.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about MPI_Type_f2c.3 is found in manpage for: [[../man3/MPI_Comm_f2c.3][man3/MPI_Comm_f2c.3]]