#+TITLE: Manpages - gnutls_cipher_set_iv.3
#+DESCRIPTION: Linux manpage for gnutls_cipher_set_iv.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_cipher_set_iv - API function

* SYNOPSIS
*#include <gnutls/crypto.h>*

*void gnutls_cipher_set_iv(gnutls_cipher_hd_t */handle/*, void * */iv/*,
size_t */ivlen/*);*

* ARGUMENTS
- gnutls_cipher_hd_t handle :: is a *gnutls_cipher_hd_t* type

- void * iv :: the IV to set

- size_t ivlen :: the length of the IV

* DESCRIPTION
This function will set the IV to be used for the next encryption block.

* SINCE
3.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
