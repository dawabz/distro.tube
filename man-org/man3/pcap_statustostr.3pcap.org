#+TITLE: Manpages - pcap_statustostr.3pcap
#+DESCRIPTION: Linux manpage for pcap_statustostr.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_statustostr - convert a PCAP_ERROR_ or PCAP_WARNING_ value to a
string

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  const char *pcap_statustostr(int error);
#+end_example

* DESCRIPTION
*pcap_statustostr*() converts a *PCAP_ERROR_* or *PCAP_WARNING_* value
returned by a libpcap routine to an error string.

* SEE ALSO
*pcap*(3PCAP)
