#+TITLE: Manpages - sd_bus_list_names.3
#+DESCRIPTION: Linux manpage for sd_bus_list_names.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_bus_list_names - Retrieve information about registered names on a bus

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

*int sd_bus_list_names(sd_bus **/bus/*, char ****/acquired/*, char
****/activatable/*);*

* DESCRIPTION
*sd_bus_list_names()* retrieves information about the registered names
on a bus. If /acquired/ is not *NULL*, this function calls
*org.freedesktop.DBus.ListNames*[1] to retrieve the list of
currently-owned names on the bus. If /acquired/ is not *NULL*, the
function calls *org.freedesktop.DBus.ListActivableNames*[2] to retrieve
the list of all names on the bus that can be activated. Note that
ownership of the arrays returned by *sd_bus_list_names()* in /acquired/
and /activatable/ is transferred to the caller and hence, the caller is
responsible for freeing these arrays and their contents.

* RETURN VALUE
On success, *sd_bus_list_names()* returns a non-negative integer. On
failure, it returns a negative errno-style error code.

** Errors
Returned errors may indicate the following problems:

*-EINVAL*

#+begin_quote
  /bus/ or both /acquired/ and /activatable/ were *NULL*.
#+end_quote

*-ENOPKG*

#+begin_quote
  The bus cannot be resolved.
#+end_quote

*-ECHILD*

#+begin_quote
  The bus was created in a different process.
#+end_quote

*-ENOMEM*

#+begin_quote
  Memory allocation failed.
#+end_quote

*-ENOTCONN*

#+begin_quote
  The bus is not connected.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-bus*(3)

* NOTES
-  1. :: org.freedesktop.DBus.ListNames

  https://dbus.freedesktop.org/doc/dbus-specification.html#bus-messages-list-names

-  2. :: org.freedesktop.DBus.ListActivableNames

  https://dbus.freedesktop.org/doc/dbus-specification.html#bus-messages-list-activatable-names
