#+TITLE: Manpages - Sys_Hostname.3perl
#+DESCRIPTION: Linux manpage for Sys_Hostname.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Sys::Hostname - Try every conceivable way to get hostname

* SYNOPSIS
use Sys::Hostname; $host = hostname;

* DESCRIPTION
Attempts several methods of getting the system hostname and then caches
the result. It tries the first available of the C library's
*gethostname()*, =`$Config{aphostname}`=, *uname* (2),
=syscall(SYS_gethostname)=, =`hostname`=, =`uname -n`=, and the file
//com/host/. If all that fails it =croak=s.

All NULs, returns, and newlines are removed from the result.

* AUTHOR
David Sundstrom </sunds@asictest.sc.ti.com/>

Texas Instruments

XS code added by Greg Bacon </gbacon@cs.uah.edu/>
