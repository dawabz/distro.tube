#+TITLE: Manpages - std_time_put_byname.3
#+DESCRIPTION: Linux manpage for std_time_put_byname.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::time_put_byname< _CharT, _OutIter > - class time_put_byname
[22.2.5.4].

* SYNOPSIS
\\

=#include <locale_facets_nonio.h>=

Inherits *std::time_put< _CharT, _OutIter >*.

** Public Types
typedef _CharT *char_type*\\

typedef _OutIter *iter_type*\\

** Public Member Functions
*time_put_byname* (const char *, size_t __refs=0)\\

*time_put_byname* (const *string* &__s, size_t __refs=0)\\

*iter_type* *put* (*iter_type* __s, *ios_base* &__io, *char_type*
__fill, const tm *__tm, char __format, char __mod=0) const\\
Format and output a time or date.

*iter_type* *put* (*iter_type* __s, *ios_base* &__io, *char_type*
__fill, const tm *__tm, const _CharT *__beg, const _CharT *__end)
const\\
Format and output a time or date.

** Static Public Attributes
static *locale::id* *id*\\
Numpunct facet id.

** Protected Member Functions
virtual *iter_type* *do_put* (*iter_type* __s, *ios_base* &__io,
*char_type* __fill, const tm *__tm, char __format, char __mod) const\\
Format and output a time or date.

** Static Protected Member Functions
static __c_locale *_S_clone_c_locale* (__c_locale &__cloc) throw ()\\

static void *_S_create_c_locale* (__c_locale &__cloc, const char *__s,
__c_locale __old=0)\\

static void *_S_destroy_c_locale* (__c_locale &__cloc)\\

static __c_locale *_S_get_c_locale* ()\\

static const char * *_S_get_c_name* () throw ()\\

static __c_locale *_S_lc_ctype_c_locale* (__c_locale __cloc, const char
*__s)\\

* Detailed Description
** "template<typename _CharT, typename _OutIter>
\\
class std::time_put_byname< _CharT, _OutIter >"class time_put_byname
[22.2.5.4].

Definition at line *893* of file *locale_facets_nonio.h*.

* Member Typedef Documentation
** template<typename _CharT , typename _OutIter > typedef _CharT
*std::time_put_byname*< _CharT, _OutIter >::*char_type*
Definition at line *897* of file *locale_facets_nonio.h*.

** template<typename _CharT , typename _OutIter > typedef _OutIter
*std::time_put_byname*< _CharT, _OutIter >::*iter_type*
Definition at line *898* of file *locale_facets_nonio.h*.

* Constructor & Destructor Documentation
** template<typename _CharT , typename _OutIter >
*std::time_put_byname*< _CharT, _OutIter >::*time_put_byname* (const
char *, size_t __refs = =0=)= [inline]=, = [explicit]=
Definition at line *901* of file *locale_facets_nonio.h*.

** template<typename _CharT , typename _OutIter >
*std::time_put_byname*< _CharT, _OutIter >::*time_put_byname* (const
*string* & __s, size_t __refs = =0=)= [inline]=, = [explicit]=
Definition at line *907* of file *locale_facets_nonio.h*.

** template<typename _CharT , typename _OutIter > virtual
*std::time_put_byname*< _CharT, _OutIter >::~*time_put_byname*
()= [inline]=, = [protected]=, = [virtual]=
Definition at line *913* of file *locale_facets_nonio.h*.

* Member Function Documentation
** template<typename _CharT , typename _OutIter > _OutIter
*std::time_put*< _CharT, _OutIter >::do_put (*iter_type* __s, *ios_base*
& __io, *char_type* __fill, const tm * __tm, char __format, char __mod)
const= [protected]=, = [virtual]=, = [inherited]=
Format and output a time or date. This function formats the data in
struct tm according to the provided format char and optional modifier.
This function is a hook for derived classes to change the value
returned.

*See also*

#+begin_quote
  put() for more details.
#+end_quote

*Parameters*

#+begin_quote
  /__s/ The stream to write to.\\
  /__io/ Source of locale.\\
  /__fill/ char_type to use for padding.\\
  /__tm/ Struct tm with date and time info to format.\\
  /__format/ Format char.\\
  /__mod/ Optional modifier char.
#+end_quote

*Returns*

#+begin_quote
  Iterator after writing.
#+end_quote

Definition at line *1365* of file *locale_facets_nonio.tcc*.

References *std::ios_base::_M_getloc()*, and
*std::__ctype_abstract_base< _CharT >::widen()*.

Referenced by *std::time_put< _CharT, _OutIter >::put()*.

** template<typename _CharT , typename _OutIter > *iter_type*
*std::time_put*< _CharT, _OutIter >::put (*iter_type* __s, *ios_base* &
__io, *char_type* __fill, const tm * __tm, char __format, char __mod =
=0=) const= [inline]=, = [inherited]=
Format and output a time or date. This function formats the data in
struct tm according to the provided format char and optional modifier.
The format and modifier are interpreted as by strftime(). It does so by
returning time_put::do_put().

*Parameters*

#+begin_quote
  /__s/ The stream to write to.\\
  /__io/ Source of locale.\\
  /__fill/ char_type to use for padding.\\
  /__tm/ Struct tm with date and time info to format.\\
  /__format/ Format char.\\
  /__mod/ Optional modifier char.
#+end_quote

*Returns*

#+begin_quote
  Iterator after writing.
#+end_quote

Definition at line *857* of file *locale_facets_nonio.h*.

References *std::time_put< _CharT, _OutIter >::do_put()*.

** template<typename _CharT , typename _OutIter > _OutIter
*std::time_put*< _CharT, _OutIter >::put (*iter_type* __s, *ios_base* &
__io, *char_type* __fill, const tm * __tm, const _CharT * __beg, const
_CharT * __end) const= [inherited]=
Format and output a time or date. This function formats the data in
struct tm according to the provided format string. The format string is
interpreted as by strftime().

*Parameters*

#+begin_quote
  /__s/ The stream to write to.\\
  /__io/ Source of locale.\\
  /__fill/ char_type to use for padding.\\
  /__tm/ Struct tm with date and time info to format.\\
  /__beg/ Start of format string.\\
  /__end/ End of format string.
#+end_quote

*Returns*

#+begin_quote
  Iterator after writing.
#+end_quote

Definition at line *1330* of file *locale_facets_nonio.tcc*.

References *std::ios_base::_M_getloc()*, and
*std::__ctype_abstract_base< _CharT >::narrow()*.

* Member Data Documentation
** template<typename _CharT , typename _OutIter > *locale::id*
*std::time_put*< _CharT, _OutIter >::id= [static]=, = [inherited]=
Numpunct facet id.

Definition at line *808* of file *locale_facets_nonio.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
