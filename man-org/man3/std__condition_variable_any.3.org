#+TITLE: Manpages - std__condition_variable_any.3
#+DESCRIPTION: Linux manpage for std__condition_variable_any.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::condition_variable_any - condition_variable_any

* SYNOPSIS
\\

** Public Member Functions
*condition_variable_any* (const *condition_variable_any* &)=delete\\

void *notify_all* () noexcept\\

void *notify_one* () noexcept\\

*condition_variable_any* & *operator=* (const *condition_variable_any*
&)=delete\\

template<typename _Lock > void *wait* (_Lock &__lock)\\

template<typename _Lock , typename _Predicate > void *wait* (_Lock
&__lock, _Predicate __p)\\

template<typename _Lock , typename _Rep , typename _Period > *cv_status*
*wait_for* (_Lock &__lock, const *chrono::duration*< _Rep, _Period >
&__rtime)\\

template<typename _Lock , typename _Rep , typename _Period , typename
_Predicate > bool *wait_for* (_Lock &__lock, const *chrono::duration*<
_Rep, _Period > &__rtime, _Predicate __p)\\

template<typename _Lock , typename _Clock , typename _Duration >
*cv_status* *wait_until* (_Lock &__lock, const *chrono::time_point*<
_Clock, _Duration > &__atime)\\

template<typename _Lock , typename _Clock , typename _Duration ,
typename _Predicate > bool *wait_until* (_Lock &__lock, const
*chrono::time_point*< _Clock, _Duration > &__atime, _Predicate __p)\\

* Detailed Description
condition_variable_any

Definition at line *242* of file *condition_variable*.

* Constructor & Destructor Documentation
** std::condition_variable_any::condition_variable_any ()= [inline]=
Definition at line *283* of file *condition_variable*.

* Member Function Documentation
** void std::condition_variable_any::notify_all ()= [inline]=,
= [noexcept]=
Definition at line *297* of file *condition_variable*.

** void std::condition_variable_any::notify_one ()= [inline]=,
= [noexcept]=
Definition at line *290* of file *condition_variable*.

** template<typename _Lock > void std::condition_variable_any::wait
(_Lock & __lock)= [inline]=
Definition at line *305* of file *condition_variable*.

** template<typename _Lock , typename _Predicate > void
std::condition_variable_any::wait (_Lock & __lock, _Predicate
__p)= [inline]=
Definition at line *319* of file *condition_variable*.

** template<typename _Lock , typename _Rep , typename _Period >
*cv_status* std::condition_variable_any::wait_for (_Lock & __lock, const
*chrono::duration*< _Rep, _Period > & __rtime)= [inline]=
Definition at line *354* of file *condition_variable*.

** template<typename _Lock , typename _Rep , typename _Period , typename
_Predicate > bool std::condition_variable_any::wait_for (_Lock & __lock,
const *chrono::duration*< _Rep, _Period > & __rtime, _Predicate
__p)= [inline]=
Definition at line *360* of file *condition_variable*.

** template<typename _Lock , typename _Clock , typename _Duration >
*cv_status* std::condition_variable_any::wait_until (_Lock & __lock,
const *chrono::time_point*< _Clock, _Duration > & __atime)= [inline]=
Definition at line *327* of file *condition_variable*.

** template<typename _Lock , typename _Clock , typename _Duration ,
typename _Predicate > bool std::condition_variable_any::wait_until
(_Lock & __lock, const *chrono::time_point*< _Clock, _Duration > &
__atime, _Predicate __p)= [inline]=
Definition at line *342* of file *condition_variable*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
