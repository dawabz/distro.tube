#+TITLE: Manpages - Alien_Build_Log.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Log.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Log - Alien::Build logging

* VERSION
version 2.44

* SYNOPSIS
Create your custom log class:

package Alien::Build::Log::MyLog; use parent qw( Alien::Build::Log );
sub log { my(undef, %opt) = @_; my($package, $filename, $line) = @{
$opt{caller} }; my $message = $opt{message}; ...; }

override log class:

% env ALIEN_BUILD_LOG=Alien::Build::Log::MyLog cpanm Alien::libfoo

* DESCRIPTION
* CONSTRUCTORS
** new
my $log = Alien::Build::Log->new;

Create an instance of the log class.

** default
my $log = Alien::Build::Log->default;

Return singleton instance of log class used by Alien::Build.

* METHODS
** set_log_class
Alien::Build::Log->set_log_class($class);

Set the default log class used by Alien::Build. This method will also
reset the default instance used by Alien::Build. If not specified,
Alien::Build::Log::Default will be used.

** log
$log->log(%options);

Overridable method which does the actual work of the log class. Options:

- caller :: Array references containing the package, file and line
  number of where the log was called.

- message :: The message to log.

* ENVIRONMENT
- ALIEN_BUILD_LOG :: The default log class used by Alien::Build.

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
