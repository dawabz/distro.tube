#+TITLE: Manpages - idn2_register_ul.3
#+DESCRIPTION: Linux manpage for idn2_register_ul.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
idn2_register_ul - API function

* SYNOPSIS
*#include <idn2.h>*

*int idn2_register_ul(const char * */ulabel/*, const char * */alabel/*,
char ** */insertname/*, int */flags/*);*

* ARGUMENTS
- const char * ulabel :: input zero-terminated locale encoded string, or
  NULL.

- const char * alabel :: input zero-terminated ACE encoded string
  (xn--), or NULL.

- char ** insertname :: newly allocated output variable with name to
  register in DNS.

- int flags :: optional *idn2_flags* to modify behaviour.

* DESCRIPTION
Perform IDNA2008 register string conversion on domain label /ulabel/ and
/alabel/ , as described in section 4 of RFC 5891. Note that the input
/ulabel/ is assumed to be encoded in the locale's default coding system,
and will be transcoded to UTF-8 and NFC normalized by this function.

It is recommended to supply both /ulabel/ and /alabel/ for better error
checking, but supplying just one of them will work. Passing in only
/alabel/ is better than only /ulabel/ . See RFC 5891 section 4 for more
information.

After version 0.11: /insertname/ may be NULL to test conversion of /src/
without allocating memory.

* RETURNS
On successful conversion *IDN2_OK* is returned, when the given /ulabel/
and /alabel/ does not match each other *IDN2_UALABEL_MISMATCH* is
returned, when either of the input labels are too long
*IDN2_TOO_BIG_LABEL* is returned, when /alabel/ does does not appear to
be a proper A-label *IDN2_INVALID_ALABEL* is returned, when /ulabel/
locale to UTF-8 conversion failed *IDN2_ICONV_FAIL* is returned, or
another error code is returned.

* SEE ALSO
The full documentation for *libidn2* is maintained as a Texinfo manual.
If the *info* and *libidn2* programs are properly installed at your
site, the command

#+begin_quote
  *info libidn2*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *https://www.gnu.org/software/libidn/libidn2/manual/*
#+end_quote
