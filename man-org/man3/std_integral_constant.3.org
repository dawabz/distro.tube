#+TITLE: Manpages - std_integral_constant.3
#+DESCRIPTION: Linux manpage for std_integral_constant.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::integral_constant< _Tp, __v > - integral_constant

* SYNOPSIS
\\

Inherited by std::__is_constructible_impl< _Tp, const _Tp & >,
std::__is_constructible_impl< _Tp, _Tp && >,
std::__is_constructible_impl< _Tp, _Args... >, std::__is_fast_hash<
hash< std::basic_string< _CharT > > >, std::__is_fast_hash< hash< _Tp >
>, std::__is_in_place_type_impl< _Tp >, std::__not_< is_array< _Tp > >,
*std::is_base_of< random_access_iterator_tag, __iterator_category_t< _It
> >*, *std::is_same< decltype(declval< _From >()+declval< _To >()), _To
>*, *std::is_same< _Tp, __intrinsic_type< remove_reference_t<
decltype(declval< _Tp >()[0])>, sizeof(_Tp)>::type >*, *std::is_same<
_Tp, __vector_type< remove_reference_t< decltype(declval< _Tp >()[0])>,
sizeof(_Tp)>::type >*, std::__and_<>, std::__call_is_nothrow< _Result,
_Fn, _Args >, std::__detail::__is_contiguous_iter<
__gnu_cxx::__normal_iterator< _Tp *, _Cont > >,
std::__detail::__variant::_Extra_visit_slot_needed<
_Maybe_variant_cookie, _Variant >::_Variant_never_valueless< variant<
_Types... > >, std::__detail::__variant::_Multi_array< _Tp
>::__untag_result< typename >, std::__detail::__variant::_Multi_array<
_Tp >::__untag_result< __deduce_visit_result< _Res >(*)(_Args...)>,
std::__detail::__variant::_Multi_array< _Tp >::__untag_result<
__variant_cookie(*)(_Args...)>, std::__detail::__variant::_Multi_array<
_Tp >::__untag_result< __variant_idx_cookie(*)(_Args...)>,
std::__detail::__variant::_Multi_array< _Tp >::__untag_result< const
void(*)(_Args...)>, std::__detail::__variant::_Never_valueless_alt<
std::any >, std::__detail::__variant::_Never_valueless_alt<
std::function< _Signature > >,
std::__detail::__variant::_Never_valueless_alt< std::shared_ptr< _Tp >
>, std::__detail::__variant::_Never_valueless_alt< std::unique_ptr< _Tp,
_Del > >, std::__detail::__variant::_Never_valueless_alt< std::weak_ptr<
_Tp > >, std::__has_iec559_behavior< _Trait, _Tp >,
std::__has_iec559_storage_format< _Tp >,
std::__is_alloc_insertable_impl< _Alloc, _Tp, _ValueT, typename >,
std::__is_alloc_insertable_impl< _Alloc, _Tp, _ValueT, __void_t<
decltype(allocator_traits< _Alloc >::construct(std::declval< _Alloc &
>(), std::declval< _ValueT * >(), std::declval< _Tp >()))> >,
std::__is_allocator< _Alloc, typename >, std::__is_allocator< _Alloc,
__void_t< typename _Alloc::value_type, decltype(std::declval< _Alloc &
>().allocate(size_t{}))> >, std::__is_bitwise_relocatable<::deque< _Tp >
>, std::__is_byte_like< _Tp, _Pred >, std::__is_byte_like< _Tp,
equal_to< _Tp > >, std::__is_byte_like< _Tp, equal_to< void > >,
std::__is_byte_like< byte, equal_to< byte > >, std::__is_byte_like<
byte, equal_to< void > >, std::__is_constructible_impl< _Tp, _Args >,
std::__is_copy_assignable_impl< _Tp, false >,
std::__is_copy_assignable_impl< _Tp, true >,
std::__is_copy_constructible_impl< _Tp, false >,
std::__is_copy_insertable< _Alloc >, std::__is_destructible_safe< _Tp,
false, true >, std::__is_destructible_safe< _Tp, true, false >,
std::__is_empty_non_tuple< tuple< _El0, _El... > >,
std::__is_extractable< _Istream, _Tp, typename >, std::__is_extractable<
_Istream, _Tp, __void_t< decltype(declval< _Istream & >() > > declval<
_Tp >())> >, std::__is_fast_hash< _Hash >, std::__is_fast_hash< hash<
experimental::string_view > >, std::__is_fast_hash< hash<
experimental::u16string_view > >, std::__is_fast_hash< hash<
experimental::u32string_view > >, std::__is_fast_hash< hash<
experimental::wstring_view > >, std::__is_fast_hash< hash< long double >
>, std::__is_fast_hash< hash< string > >, std::__is_fast_hash< hash<
string_view > >, std::__is_fast_hash< hash< u16string > >,
std::__is_fast_hash< hash< u16string_view > >, std::__is_fast_hash<
hash< u32string > >, std::__is_fast_hash< hash< u32string_view > >,
std::__is_fast_hash< hash< variant< _Types... > > >,
std::__is_fast_hash< hash< wstring > >, std::__is_fast_hash< hash<
wstring_view > >, std::__is_floating_point_helper< typename >,
std::__is_floating_point_helper< double >,
std::__is_floating_point_helper< float >,
std::__is_floating_point_helper< long double >,
std::__is_in_place_type_impl< typename >, std::__is_in_place_type_impl<
in_place_type_t< _Tp > >, std::__is_insertable< _Ostream, _Tp, typename
>, std::__is_insertable< _Ostream, _Tp, __void_t< decltype(declval<
_Ostream & >()<< declval< const _Tp & >())> >,
std::__is_integral_helper< typename >, std::__is_integral_helper< bool
>, std::__is_integral_helper< char >, std::__is_integral_helper<
char16_t >, std::__is_integral_helper< char32_t >,
std::__is_integral_helper< int >, std::__is_integral_helper< long >,
std::__is_integral_helper< long long >, std::__is_integral_helper< short
>, std::__is_integral_helper< signed char >, std::__is_integral_helper<
unsigned char >, std::__is_integral_helper< unsigned int >,
std::__is_integral_helper< unsigned long >, std::__is_integral_helper<
unsigned long long >, std::__is_integral_helper< unsigned short >,
std::__is_invocable_impl< _Result, _Ret, bool, typename >,
std::__is_invocable_impl< _Result, _Ret, true, __void_t< typename
_Result::type > >, std::__is_location_invariant<
__future_base::_State_base::_Setter< _Res, _Arg > >,
std::__is_location_invariant< __future_base::_Task_setter< _Res_ptr,
_Fn, _Res > >, std::__is_member_function_pointer_helper< typename >,
std::__is_member_object_pointer_helper< typename >,
std::__is_member_pointer_helper< _Tp >, std::__is_member_pointer_helper<
_Tp _Cp::* >, std::__is_move_assignable_impl< _Tp, false >,
std::__is_move_assignable_impl< _Tp, true >,
std::__is_move_constructible_impl< _Tp, false >,
std::__is_move_insertable< _Alloc >,
std::__is_nothrow_copy_constructible_impl< _Tp, false >,
std::__is_nothrow_copy_constructible_impl< _Tp, true >,
std::__is_nothrow_move_constructible_impl< _Tp, false >,
std::__is_nothrow_move_constructible_impl< _Tp, true >,
std::__is_nt_copy_assignable_impl< _Tp, false >,
std::__is_nt_copy_assignable_impl< _Tp, true >,
std::__is_nt_destructible_safe< _Tp, false, true >,
std::__is_nt_destructible_safe< _Tp, true, false >,
std::__is_nt_invocable_impl< _Result, _Ret, typename >,
std::__is_nt_move_assignable_impl< _Tp, false >,
std::__is_nt_move_assignable_impl< _Tp, true >,
std::__is_null_pointer_helper< typename >,
std::__is_null_pointer_helper< std::nullptr_t >,
std::__is_pointer_helper< typename >, std::__is_pointer_helper< _Tp * >,
std::__is_referenceable< _Tp, typename >, std::__is_referenceable< _Tp,
__void_t< _Tp & > >, std::__is_signed_helper< _Tp, bool >,
std::__is_trivially_copy_assignable_impl< _Tp, false >,
std::__is_trivially_copy_assignable_impl< _Tp, true >,
std::__is_trivially_copy_constructible_impl< _Tp, false >,
std::__is_trivially_move_assignable_impl< _Tp, false >,
std::__is_trivially_move_assignable_impl< _Tp, true >,
std::__is_trivially_move_constructible_impl< _Tp, false >,
std::__is_tuple_like_impl< typename >, std::__is_tuple_like_impl< array<
_Tp, _Nm > >, *std::__is_tuple_like_impl< pair< _T1, _T2 > >*,
std::__is_tuple_like_impl< tuple< _Tps... > >, std::__is_void_helper<
typename >, std::__is_void_helper< void >, std::__not_< _Pp >,
std::__or_<>, std::__parse_int::_Number< _Base, _Digs >,
std::__parse_int::_Number< _Base >, std::__parse_int::_Parse_int< _Digs
>, std::__parse_int::_Parse_int< '0', 'B', _Digs... >,
std::__parse_int::_Parse_int< '0', 'X', _Digs... >,
std::__parse_int::_Parse_int< '0', 'b', _Digs... >,
std::__parse_int::_Parse_int< '0', 'x', _Digs... >,
std::__parse_int::_Parse_int< '0', _Digs... >, std::__parse_int::_Power<
_Base, _Digs >, std::__parse_int::_Power< _Base >,
std::__sp_compatible_with< _Yp_ptr, _Tp_ptr >,
std::__sp_compatible_with< _Up(*)[_Nm], _Up(*)[]>,
std::__sp_compatible_with< _Up(*)[_Nm], const _Up(*)[]>,
std::__sp_compatible_with< _Up(*)[_Nm], const volatile _Up(*)[]>,
std::__sp_compatible_with< _Up(*)[_Nm], volatile _Up(*)[]>,
std::__sp_is_constructible_arr< _Up, _Yp, typename >,
std::__sp_is_constructible_arrN< _Up, _Nm, _Yp, typename >,
std::__uses_allocator_helper< _Tp, _Alloc, typename >,
std::__value_exists_impl< _Trait, _Tp, typename >,
std::__value_exists_impl< _Trait, _Tp, void_t< decltype(_Trait< _Tp
>::value)> >, std::experimental::fundamentals_v2::__make_array_elem<
void, _Types... >::__is_reference_wrapper< typename >,
std::experimental::fundamentals_v2::__make_array_elem< void, _Types...
>::__is_reference_wrapper< reference_wrapper< _Up > >,
std::experimental::net::v1::__detail::__is_acceptable_protocol< _Tp,
typename >, std::experimental::net::v1::__detail::__is_endpoint< _Tp,
typename >, std::experimental::net::v1::__detail::__is_endpoint< _Tp,
decltype(__detail::__endpoint_reqs< _Tp >())>,
std::experimental::net::v1::__detail::__is_inet_protocol< _Tp, typename
>, std::experimental::net::v1::__detail::__is_inet_protocol< _Tp,
decltype(__inet_proto_reqs< _Tp >())>,
std::experimental::net::v1::__detail::__is_protocol< _Tp, typename >,
std::experimental::net::v1::__detail::__is_protocol< _Tp,
decltype(__detail::__protocol_reqs< _Tp >())>,
std::experimental::parallelism_v2::__converts_to_higher_integer_rank<
_From, _To, bool >, std::experimental::parallelism_v2::__is_bitmask<
_Tp, typename >, std::experimental::parallelism_v2::__is_bitmask<
_BitMask< _Np, _Sanitized >, void >,
std::experimental::parallelism_v2::__is_bitmask< _Tp, void_t<
decltype(declval< unsigned & >()=declval< _Tp >() &1u)> >,
std::experimental::parallelism_v2::__is_fixed_size_abi< _Tp >,
std::experimental::parallelism_v2::__is_fixed_size_abi<
simd_abi::fixed_size< _Np > >,
std::experimental::parallelism_v2::__is_intrinsic_type< _Tp, typename >,
std::experimental::parallelism_v2::__is_narrowing_conversion< _Tp, _Tp,
true, true >,
std::experimental::parallelism_v2::__is_narrowing_conversion< _Tp, bool,
true, true >,
std::experimental::parallelism_v2::__is_narrowing_conversion< bool,
bool, true, true >,
std::experimental::parallelism_v2::__is_possible_loadstore_conversion<
bool, bool >, std::experimental::parallelism_v2::__is_simd_wrapper< _Tp
>, std::experimental::parallelism_v2::__is_simd_wrapper< _SimdWrapper<
_Tp, _Np > >, std::experimental::parallelism_v2::__is_vector_type< _Tp,
typename >, std::experimental::parallelism_v2::__is_vectorizable< bool
>, std::experimental::parallelism_v2::__simd_size_impl< _Tp, _Abi,
enable_if_t< conjunction_v< __is_vectorizable< _Tp >, is_abi_tag< _Abi >
> > >, std::experimental::parallelism_v2::is_abi_tag< _Tp, typename >,
std::experimental::parallelism_v2::is_simd< _Tp >,
std::experimental::parallelism_v2::is_simd< simd< _Tp, _Abi > >,
std::experimental::parallelism_v2::is_simd_mask< _Tp >,
std::experimental::parallelism_v2::is_simd_mask< simd_mask< _Tp, _Abi >
>, std::experimental::parallelism_v2::memory_alignment< _Tp, _Up >,
*std::is_array< typename >*, std::is_array< _Tp[]>, std::is_array<
_Tp[_Size]>, *std::is_assignable< _Tp, _Up >*, *std::is_bind_expression<
_Tp >*, *std::is_bind_expression< _Bind< _Signature > >*,
*std::is_bind_expression< _Bind_result< _Result, _Signature > >*,
*std::is_bind_expression< const _Bind< _Signature > >*,
*std::is_bind_expression< const _Bind_result< _Result, _Signature > >*,
*std::is_bind_expression< const volatile _Bind< _Signature > >*,
*std::is_bind_expression< const volatile _Bind_result< _Result,
_Signature > >*, *std::is_bind_expression< volatile _Bind< _Signature >
>*, *std::is_bind_expression< volatile _Bind_result< _Result, _Signature
> >*, *std::is_compound< _Tp >*, *std::is_const< typename >*,
std::is_const< _Tp const >, *std::is_default_constructible< _Tp >*,
*std::is_error_code_enum< _Tp >*, *std::is_error_code_enum< future_errc
>*, std::is_error_code_enum< io_errc >, *std::is_error_condition_enum<
_Tp >*, std::is_error_condition_enum< errc >, *std::is_floating_point<
_Tp >*, *std::is_function< _Tp >*, std::is_function< _Tp & >,
std::is_function< _Tp && >, *std::is_integral< _Tp >*,
*std::is_invocable< _Fn, _ArgTypes >*, *std::is_invocable_r< _Ret, _Fn,
_ArgTypes >*, *std::is_lvalue_reference< typename >*,
std::is_lvalue_reference< _Tp & >, *std::is_member_function_pointer< _Tp
>*, *std::is_member_object_pointer< _Tp >*, *std::is_member_pointer< _Tp
>*, *std::is_nothrow_assignable< _Tp, _Up >*,
*std::is_nothrow_constructible< _Tp, _Args >*,
*std::is_nothrow_default_constructible< _Tp >*, *std::is_null_pointer<
_Tp >*, *std::is_object< _Tp >*, *std::is_pointer< _Tp >*,
*std::is_rvalue_reference< typename >*, std::is_rvalue_reference< _Tp &&
>, *std::is_same< _Tp, _Up >*, std::is_same< _Tp, _Tp >,
*std::is_trivially_assignable< _Tp, _Up >*,
*std::is_trivially_constructible< _Tp, _Args >*,
*std::is_trivially_default_constructible< _Tp >*, *std::is_void< _Tp >*,
*std::is_volatile< typename >*, std::is_volatile< _Tp volatile >,
*std::uses_allocator< _Tp, _Alloc >*, std::uses_allocator<
priority_queue< _Tp, _Sequence, _Compare >, _Alloc >,
std::uses_allocator< promise< _Res >, _Alloc >, std::uses_allocator<
queue< _Tp, _Seq >, _Alloc >, std::uses_allocator< stack< _Tp, _Seq >,
_Alloc >, and *std::uses_allocator< tuple< _Types... >, _Alloc >*.

** Public Types
typedef *integral_constant*< _Tp, __v > *type*\\

typedef _Tp *value_type*\\

** Public Member Functions
constexpr *operator value_type* () const noexcept\\

constexpr value_type *operator()* () const noexcept\\

** Static Public Attributes
static constexpr _Tp *value*\\

* Detailed Description
** "template<typename _Tp, _Tp __v>
\\
struct std::integral_constant< _Tp, __v >"integral_constant

Definition at line *57* of file *std/type_traits*.

* Member Typedef Documentation
** template<typename _Tp , _Tp __v> typedef *integral_constant*<_Tp,
__v> *std::integral_constant*< _Tp, __v >::*type*
Definition at line *61* of file *std/type_traits*.

** template<typename _Tp , _Tp __v> typedef _Tp
*std::integral_constant*< _Tp, __v >::value_type
Definition at line *60* of file *std/type_traits*.

* Member Function Documentation
** template<typename _Tp , _Tp __v> constexpr *std::integral_constant*<
_Tp, __v >::operator value_type () const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *62* of file *std/type_traits*.

** template<typename _Tp , _Tp __v> constexpr value_type
*std::integral_constant*< _Tp, __v >::operator() () const= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *67* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
