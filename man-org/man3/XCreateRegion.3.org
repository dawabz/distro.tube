#+TITLE: Manpages - XCreateRegion.3
#+DESCRIPTION: Linux manpage for XCreateRegion.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XCreateRegion, XSetRegion, XDestroyRegion - create or destroy regions

* SYNTAX
Region XCreateRegion (void );

int XSetRegion ( Display */display/ , GC /gc/ , Region /r/ );

int XDestroyRegion ( Region /r/ );

* ARGUMENTS
- display :: Specifies the connection to the X server.

- gc :: Specifies the GC.

18. Specifies the region.

* DESCRIPTION
The *XCreateRegion* function creates a new empty region.

The *XSetRegion* function sets the clip-mask in the GC to the specified
region. The region is specified relative to the drawable's origin. The
resulting GC clip origin is implementation-dependent. Once it is set in
the GC, the region can be destroyed.

The *XDestroyRegion* function deallocates the storage associated with a
specified region.

* SEE ALSO
XEmptyRegion(3), XIntersectRegion(3)\\
/Xlib - C Language X Interface/
