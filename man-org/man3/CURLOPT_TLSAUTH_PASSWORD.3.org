#+TITLE: Manpages - CURLOPT_TLSAUTH_PASSWORD.3
#+DESCRIPTION: Linux manpage for CURLOPT_TLSAUTH_PASSWORD.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_TLSAUTH_PASSWORD - password to use for TLS authentication

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TLSAUTH_PASSWORD, char
*pwd);

* DESCRIPTION
Pass a char * as parameter, which should point to the null-terminated
password to use for the TLS authentication method specified with the
/CURLOPT_TLSAUTH_TYPE(3)/ option. Requires that the
/CURLOPT_TLSAUTH_USERNAME(3)/ option also be set.

The application does not have to keep the string around after setting
this option.

This feature relies in TLS SRP which does not work with TLS 1.3.

* DEFAULT
NULL

* PROTOCOLS
All TLS-based protocols

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    curl_easy_setopt(curl, CURLOPT_TLSAUTH_TYPE, "SRP");
    curl_easy_setopt(curl, CURLOPT_TLSAUTH_USERNAME, "user");
    curl_easy_setopt(curl, CURLOPT_TLSAUTH_PASSWORD, "secret");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.21.4

* RETURN VALUE
Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if
not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_TLSAUTH_TYPE*(3), *CURLOPT_TLSAUTH_USERNAME*(3),
