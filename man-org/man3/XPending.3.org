#+TITLE: Manpages - XPending.3
#+DESCRIPTION: Linux manpage for XPending.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XPending.3 is found in manpage for: [[../man3/XFlush.3][man3/XFlush.3]]