#+TITLE: Manpages - iswctype.3
#+DESCRIPTION: Linux manpage for iswctype.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
iswctype - wide-character classification

* SYNOPSIS
#+begin_example
  #include <wctype.h>

  int iswctype(wint_t wc, wctype_t desc);
#+end_example

* DESCRIPTION
If /wc/ is a wide character having the character property designated by
/desc/ (or in other words: belongs to the character class designated by
/desc/), the *iswctype*() function returns nonzero. Otherwise, it
returns zero. If /wc/ is *WEOF*, zero is returned.

/desc/ must be a character property descriptor returned by the
*wctype*(3) function.

* RETURN VALUE
The *iswctype*() function returns nonzero if the /wc/ has the designated
property. Otherwise, it returns 0.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface    | Attribute     | Value   |
| *iswctype*() | Thread safety | MT-Safe |

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008, C99.

* NOTES
The behavior of *iswctype*() depends on the *LC_CTYPE* category of the
current locale.

* SEE ALSO
*iswalnum*(3), *iswalpha*(3), *iswblank*(3), *iswcntrl*(3),
*iswdigit*(3), *iswgraph*(3), *iswlower*(3), *iswprint*(3),
*iswpunct*(3), *iswspace*(3), *iswupper*(3), *iswxdigit*(3), *wctype*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
