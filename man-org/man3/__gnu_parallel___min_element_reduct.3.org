#+TITLE: Manpages - __gnu_parallel___min_element_reduct.3
#+DESCRIPTION: Linux manpage for __gnu_parallel___min_element_reduct.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::__min_element_reduct< _Compare, _It > - Reduction for
finding the maximum element, using a comparator.

* SYNOPSIS
\\

=#include <for_each_selectors.h>=

** Public Member Functions
*__min_element_reduct* (_Compare &__c)\\

_It *operator()* (_It __x, _It __y)\\

** Public Attributes
_Compare & *__comp*\\

* Detailed Description
** "template<typename _Compare, typename _It>
\\
struct __gnu_parallel::__min_element_reduct< _Compare, _It >"Reduction
for finding the maximum element, using a comparator.

Definition at line *307* of file *for_each_selectors.h*.

* Constructor & Destructor Documentation
** template<typename _Compare , typename _It >
*__gnu_parallel::__min_element_reduct*< _Compare, _It
>::*__min_element_reduct* (_Compare & __c)= [inline]=, = [explicit]=
Definition at line *312* of file *for_each_selectors.h*.

* Member Function Documentation
** template<typename _Compare , typename _It > _It
*__gnu_parallel::__min_element_reduct*< _Compare, _It >::operator() (_It
__x, _It __y)= [inline]=
Definition at line *315* of file *for_each_selectors.h*.

* Member Data Documentation
** template<typename _Compare , typename _It > _Compare&
*__gnu_parallel::__min_element_reduct*< _Compare, _It >::__comp
Definition at line *309* of file *for_each_selectors.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
