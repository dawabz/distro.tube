#+TITLE: Manpages - openlog.3
#+DESCRIPTION: Linux manpage for openlog.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about openlog.3 is found in manpage for: [[../man3/syslog.3][man3/syslog.3]]