#+TITLE: Manpages - XIntersectRegion.3
#+DESCRIPTION: Linux manpage for XIntersectRegion.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XIntersectRegion, XUnionRegion, XUnionRectWithRegion, XSubtractRegion,
XXorRegion, XOffsetRegion, XShrinkRegion - region arithmetic

* SYNTAX
int XIntersectRegion ( Region /sra/ , Region /srb/ , Region
/dr_return/ );

int XUnionRegion ( Region /sra/ , Region /srb/ , Region /dr_return/ );

int XUnionRectWithRegion ( XRectangle */rectangle/ , Region
/src_region/ , Region /dest_region_return/ );

int XSubtractRegion ( Region /sra/ , Region /srb/ , Region
/dr_return/ );

int XXorRegion ( Region /sra/ , Region /srb/ , Region /dr_return/ );

int XOffsetRegion ( Region /r/ , int /dx/ , int /dy/ );

int XShrinkRegion ( Region /r/ , int /dx/ , int /dy/ );

* ARGUMENTS
- dest_region_return :: Returns the destination region.

- dr_return :: Returns the result of the computation.

510. \\

- dy :: Specify the x and y coordinates, which define the amount you
  want to move or shrink the specified region.

18. Specifies the region.

- rectangle :: Specifies the rectangle.

- sra :: \\

- srb :: Specify the two regions with which you want to perform the
  computation.

- src_region :: Specifies the source region to be used.

* DESCRIPTION
The *XIntersectRegion* function computes the intersection of two
regions.

The *XUnionRegion* function computes the union of two regions.

The *XUnionRectWithRegion* function updates the destination region from
a union of the specified rectangle and the specified source region.

The *XSubtractRegion* function subtracts srb from sra and stores the
results in dr_return.

The *XXorRegion* function calculates the difference between the union
and intersection of two regions.

The *XOffsetRegion* function moves the specified region by a specified
amount.

The *XShrinkRegion* function reduces the specified region by a specified
amount. Positive values shrink the size of the region, and negative
values expand the region.

* SEE ALSO
XCreateRegion(3), XDrawRectangle(3), XEmptyRegion(3)\\
/Xlib - C Language X Interface/
