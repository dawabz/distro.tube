#+TITLE: Manpages - SDL_VideoModeOK.3
#+DESCRIPTION: Linux manpage for SDL_VideoModeOK.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_VideoModeOK - Check to see if a particular video mode is supported.

* SYNOPSIS
*#include "SDL.h"*

*int SDL_VideoModeOK*(*int width, int height, int bpp, Uint32 flags*);

* DESCRIPTION
*SDL_VideoModeOK* returns *0* if the requested mode is not supported
under any bit depth, or returns the bits-per-pixel of the closest
available mode with the given width, height and requested /surface/
flags (see *SDL_SetVideoMode*).

The bits-per-pixel value returned is only a suggested mode. You can
usually request and bpp you want when /setting/ the video mode and SDL
will emulate that color depth with a shadow video surface.

The arguments to *SDL_VideoModeOK* are the same ones you would pass to
/SDL_SetVideoMode/

* EXAMPLE
#+begin_example
  SDL_Surface *screen;
  Uint32 bpp;
  .
  .
  .
  printf("Checking mode 640x480@16bpp.
  ");
  bpp=SDL_VideoModeOK(640, 480, 16, SDL_HWSURFACE);

  if(!bpp){
    printf("Mode not available.
  ");
    exit(-1);
  }

  printf("SDL Recommends 640x480@%dbpp.
  ", bpp);
  screen=SDL_SetVideoMode(640, 480, bpp, SDL_HWSURFACE);
  .
  .
#+end_example

* SEE ALSO
*SDL_SetVideoMode*, *SDL_GetVideoInfo*
