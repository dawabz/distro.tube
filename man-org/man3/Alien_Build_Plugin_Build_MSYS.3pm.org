#+TITLE: Manpages - Alien_Build_Plugin_Build_MSYS.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Plugin_Build_MSYS.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Plugin::Build::MSYS - MSYS plugin for Alien::Build

* VERSION
version 2.44

* SYNOPSIS
use alienfile; plugin Build::MSYS;

* DESCRIPTION
This plugin sets up the MSYS environment for your build on Windows. It
does not do anything on non-windows platforms. MSYS provides the
essential tools for building software that is normally expected in a
UNIX or POSIX environment. This like =sh=, =awk= and =make=. To provide
MSYS, this plugin uses Alien::MSYS.

* PROPERTIES
** msys_version
The version of Alien::MSYS required if it is deemed necessary. If
Alien::MSYS isn't needed (if running under Unix, or MSYS2, for example)
this will do nothing.

* HELPERS
** make
%{make}

On windows the default =%{make}= helper is replace with the make that
comes with Alien::MSYS. This is almost certainly what you want, as most
unix style make projects will not build with =nmake= or =dmake=
typically used by Perl on Windows.

* SEE ALSO
Alien::Build::Plugin::Autoconf, Alien::Build::Plugin, Alien::Build,
Alien::Base, Alien

<http://www.mingw.org/wiki/MSYS>

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
