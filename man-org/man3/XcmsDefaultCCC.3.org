#+TITLE: Manpages - XcmsDefaultCCC.3
#+DESCRIPTION: Linux manpage for XcmsDefaultCCC.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XcmsDefaultCCC - obtain the default CCC for a screen

* SYNTAX
XcmsCCC XcmsDefaultCCC ( Display */display/ , int /screen_number/ );

* ARGUMENTS
- display :: Specifies the connection to the X server.

- screen_number :: Specifies the appropriate screen number on the host
  server.

* DESCRIPTION
The *XcmsDefaultCCC* function returns the default CCC for the specified
screen. Its visual is the default visual of the screen. Its initial
gamut compression and white point adjustment procedures as well as the
associated client data are implementation specific.

* SEE ALSO
DisplayOfCCC(3), XcmsCCCOfColormap(3), XcmsConvertColors(3),
XcmsCreateCCC(3), XcmsSetWhitePoint(3)\\
/Xlib - C Language X Interface/
