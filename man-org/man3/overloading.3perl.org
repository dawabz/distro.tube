#+TITLE: Manpages - overloading.3perl
#+DESCRIPTION: Linux manpage for overloading.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
overloading - perl pragma to lexically control overloading

* SYNOPSIS
{ no overloading; my $str = "$object"; # doesnt call stringification
overload } # its lexical, so this stringifies: warn "$object"; # it can
be enabled per op no overloading qw(""); warn "$object"; # and also
reenabled use overloading;

* DESCRIPTION
This pragma allows you to lexically disable or enable overloading.

- "no overloading" :: Disables overloading entirely in the current
  lexical scope.

- "no overloading @ops" :: Disables only specific overloads in the
  current lexical scope.

- "use overloading" :: Reenables overloading in the current lexical
  scope.

- "use overloading @ops" :: Reenables overloading only for specific ops
  in the current lexical scope.
