#+TITLE: Manpages - hwloc_get_pu_obj_by_os_index.3
#+DESCRIPTION: Linux manpage for hwloc_get_pu_obj_by_os_index.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about hwloc_get_pu_obj_by_os_index.3 is found in manpage for: [[../man3/hwlocality_helper_find_misc.3][man3/hwlocality_helper_find_misc.3]]