#+TITLE: Manpages - closedir.3
#+DESCRIPTION: Linux manpage for closedir.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
closedir - close a directory

* SYNOPSIS
#+begin_example
  #include <sys/types.h>
  #include <dirent.h>

  int closedir(DIR *dirp);
#+end_example

* DESCRIPTION
The *closedir*() function closes the directory stream associated with
/dirp/. A successful call to *closedir*() also closes the underlying
file descriptor associated with /dirp/. The directory stream descriptor
/dirp/ is not available after this call.

* RETURN VALUE
The *closedir*() function returns 0 on success. On error, -1 is
returned, and /errno/ is set to indicate the error.

* ERRORS
- *EBADF* :: Invalid directory stream descriptor /dirp/.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface    | Attribute     | Value   |
| *closedir*() | Thread safety | MT-Safe |

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD.

* SEE ALSO
*close*(2), *opendir*(3), *readdir*(3), *rewinddir*(3), *scandir*(3),
*seekdir*(3), *telldir*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
