#+TITLE: Manpages - hwlocality_object_strings.3
#+DESCRIPTION: Linux manpage for hwlocality_object_strings.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwlocality_object_strings - Converting between Object Types and
Attributes, and Strings

* SYNOPSIS
\\

** Functions
const char * *hwloc_obj_type_string* (*hwloc_obj_type_t* type)\\

int *hwloc_obj_type_snprintf* (char *restrict string, size_t size,
*hwloc_obj_t* obj, int verbose)\\

int *hwloc_obj_attr_snprintf* (char *restrict string, size_t size,
*hwloc_obj_t* obj, const char *restrict separator, int verbose)\\

int *hwloc_type_sscanf* (const char *string, *hwloc_obj_type_t* *typep,
union *hwloc_obj_attr_u* *attrp, size_t attrsize)\\

int *hwloc_type_sscanf_as_depth* (const char *string, *hwloc_obj_type_t*
*typep, *hwloc_topology_t* topology, int *depthp)\\

* Detailed Description
* Function Documentation
** int hwloc_obj_attr_snprintf (char *restrict string, size_t size,
*hwloc_obj_t* obj, const char *restrict separator, int verbose)
Stringify the attributes of a given topology object into a
human-readable form. Attribute values are separated by =separator=.

Only the major attributes are printed in non-verbose mode.

If =size= is 0, =string= may safely be =NULL=.

*Returns*

#+begin_quote
  the number of character that were actually written if not truncating,
  or that would have been written (not including the ending \0).
#+end_quote

** int hwloc_obj_type_snprintf (char *restrict string, size_t size,
*hwloc_obj_t* obj, int verbose)
Stringify the type of a given topology object into a human-readable
form. Contrary to *hwloc_obj_type_string()*, this function includes
object-specific attributes (such as the Group depth, the Bridge type, or
OS device type) in the output, and it requires the caller to provide the
output buffer.

The output is guaranteed to be the same for all objects of a same
topology level.

If =verbose= is 1, longer type names are used, e.g. L1Cache instead of
L1.

The output string may be parsed back by *hwloc_type_sscanf()*.

If =size= is 0, =string= may safely be =NULL=.

*Returns*

#+begin_quote
  the number of character that were actually written if not truncating,
  or that would have been written (not including the ending \0).
#+end_quote

** const char* hwloc_obj_type_string (*hwloc_obj_type_t* type)
Return a constant stringified object type. This function is the basic
way to convert a generic type into a string. The output string may be
parsed back by *hwloc_type_sscanf()*.

*hwloc_obj_type_snprintf()* may return a more precise output for a
specific object, but it requires the caller to provide the output
buffer.

** int hwloc_type_sscanf (const char * string, *hwloc_obj_type_t* *
typep, union *hwloc_obj_attr_u* * attrp, size_t attrsize)
Return an object type and attributes from a type string. Convert strings
such as 'Package' or 'L1iCache' into the corresponding types. Matching
is case-insensitive, and only the first letters are actually required to
match.

The matched object type is set in =typep= (which cannot be =NULL=).

Type-specific attributes, for instance Cache type, Cache depth, Group
depth, Bridge type or OS Device type may be returned in =attrp=.
Attributes that are not specified in the string (for instance 'Group'
without a depth, or 'L2Cache' without a cache type) are set to -1.

=attrp= is only filled if not =NULL= and if its size specified in
=attrsize= is large enough. It should be at least as large as union
*hwloc_obj_attr_u*.

*Returns*

#+begin_quote
  0 if a type was correctly identified, otherwise -1.
#+end_quote

*Note*

#+begin_quote
  This function is guaranteed to match any string returned by
  *hwloc_obj_type_string()* or *hwloc_obj_type_snprintf()*.

  This is an extended version of the now deprecated
  hwloc_obj_type_sscanf().
#+end_quote

** int hwloc_type_sscanf_as_depth (const char * string,
*hwloc_obj_type_t* * typep, *hwloc_topology_t* topology, int * depthp)
Return an object type and its level depth from a type string. Convert
strings such as 'Package' or 'L1iCache' into the corresponding types and
return in =depthp= the depth of the corresponding level in the topology
=topology=.

If no object of this type is present on the underlying architecture,
*HWLOC_TYPE_DEPTH_UNKNOWN* is returned.

If multiple such levels exist (for instance if giving Group without any
depth), the function may return *HWLOC_TYPE_DEPTH_MULTIPLE* instead.

The matched object type is set in =typep= if =typep= is non =NULL=.

*Note*

#+begin_quote
  This function is similar to *hwloc_type_sscanf()* followed by
  *hwloc_get_type_depth()* but it also automatically disambiguates
  multiple group levels etc.

  This function is guaranteed to match any string returned by
  *hwloc_obj_type_string()* or *hwloc_obj_type_snprintf()*.
#+end_quote

* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
