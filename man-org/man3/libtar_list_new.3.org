#+TITLE: Manpages - libtar_list_new.3
#+DESCRIPTION: Linux manpage for libtar_list_new.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libtar_list_new, libtar_list_free, libtar_list_next, libtar_list_prev,
libtar_list_add, libtar_list_del, libtar_list_search, libtar_list_dup,
libtar_list_merge, libtar_list_add_str - linked list routines

* SYNOPSIS
*#include <libtar.h>*

*libtar_list_t *libtar_list_new(int */flags/*,* *int
(**/cmpfunc/*)());*\\
*void libtar_list_free(libtar_list_t **/l/*, void (**/freefunc/*)());*\\
*int libtar_list_add_str(libtar_list_t **/l/*, char **/str/*,* *char
**/delim/*);*\\
*int libtar_list_add(libtar_list_t **/l/*, void **/data/*);*\\
*void libtar_list_del(libtar_list_t **/l/*, libtar_node_t ***/n/*);*\\
*int libtar_list_search(libtar_list_t **/l/*, libtar_node_t ***/n/*,*
*void **/data/*, int (**/matchfunc/*)());*\\
*int libtar_list_next(libtar_list_t **/l/*, libtar_node_t ***/n/*);*\\
*int libtar_list_prev(libtar_list_t **/l/*, libtar_node_t ***/n/*);*\\
*libtar_list_t *libtar_list_dup(libtar_list_t **/l/*);*\\
*libtar_list_t *libtar_list_merge(int (**/cmpfunc/*)(), int */flags/*,*
*libtar_list_t **/list1/*, libtar_list_t **/list2/*);*

* DESCRIPTION
The *libtar_list_new*() function creates a new list. The /flags/
argument must be one of the following values:

- LIST_USERFUNC :: The /cmpfunc/ argument points to a user-supplied
  function which determines the ordering of the list.

- LIST_STACK :: Use the list as a stack. New elements are added to the
  front of the list.

- LIST_QUEUE :: Use the list as a queue. New elements are added to the
  end of the list.

The *libtar_list_free*() function deallocates all memory associated with
the list /l/. If /freefunc/ is not /NULL/, it is called to free memory
associated with each node in the list.

The *libtar_list_add*() function adds the element pointed to by /data/
to the list /l/. The position of the new element will be determined by
the flags passed to *libtar_list_new*() when the list was created.

The *libtar_list_add_str*() function tokenizes the string /str/ using
the delimiter characters in the string /delim/. The resulting tokens are
added to list /l/ using *libtar_list_add*().

The *libtar_list_search*() function searches for an element which
matches /data/ using the matching function /matchfunc/. If /matchfunc/
is /NULL/, a default matching function designed for ASCII strings is
used. Searching begins from the node pointed to by /n/.

The *libtar_list_del*() function removes the entry pointed to by /n/
from the list pointed to by /l/.

The *libtar_list_dup*() function creates a copy of the list /l/ using
dynamically allocated memory.

The *libtar_list_merge*() function creates a new list with /flags/ and
/cmpfunc/, in the same way as *libtar_list_new*(). It then adds all
elements from /list1/ and /list2/ using *libtar_list_add*().

* RETURN VALUE
The *libtar_list_new*(), *libtar_list_dup*(), and *libtar_list_merge*()
functions return a pointer to the new list structure, or /NULL/ on
error.

The *libtar_list_next*(), *libtar_list_prev*(), and
*libtar_list_search*() functions return 1 when valid data is returned,
or 0 otherwise.

The *libtar_list_add*() and *libtar_list_add_str*() functions return 0
on success, or -1 on error.

* SEE ALSO
*libtar_hash_new*(3)
