#+TITLE: Manpages - SDL_InitSubSystem.3
#+DESCRIPTION: Linux manpage for SDL_InitSubSystem.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_InitSubSystem - Initialize subsystems

* SYNOPSIS
*#include "SDL.h"*

*int SDL_InitSubSystem*(*Uint32 flags*);

* DESCRIPTION
After SDL has been initialized with *SDL_Init* you may initialize
uninitialized subsystems with *SDL_InitSubSystem*. The *flags* parameter
is the same as that used in *SDL_Init*.

* EXAMPLES
#+begin_example
  /* Seperating Joystick and Video initialization. */
  SDL_Init(SDL_INIT_VIDEO);
  .
  .
  SDL_SetVideoMode(640, 480, 16, SDL_DOUBLEBUF|SDL_FULLSCREEN);
  .
  /* Do Some Video stuff */
  .
  .
  /* Initialize the joystick subsystem */
  SDL_InitSubSystem(SDL_INIT_JOYSTICK);

  /* Do some stuff with video and joystick */
  .
  .
  .
  /* Shut them both down */
  SDL_Quit();
#+end_example

* RETURN VALUE
Returns *-1* on an error or *0* on success.

* SEE ALSO
*SDL_Init*, *SDL_Quit*, *SDL_QuitSubSystem*
