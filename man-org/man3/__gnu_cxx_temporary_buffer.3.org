#+TITLE: Manpages - __gnu_cxx_temporary_buffer.3
#+DESCRIPTION: Linux manpage for __gnu_cxx_temporary_buffer.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::temporary_buffer< _ForwardIterator, _Tp >

* SYNOPSIS
\\

Inherits *std::_Temporary_buffer< _ForwardIterator, _Tp >*.

** Public Types
typedef pointer *iterator*\\

typedef value_type * *pointer*\\

typedef ptrdiff_t *size_type*\\

typedef _Tp *value_type*\\

** Public Member Functions
*temporary_buffer* (_ForwardIterator __first, _ForwardIterator __last)\\
Requests storage large enough to hold a copy of [first,last).

*~temporary_buffer* ()\\
Destroys objects and frees storage.

iterator *begin* ()\\
As per Table mumble.

iterator *end* ()\\
As per Table mumble.

size_type *requested_size* () const\\
Returns the size requested by the constructor; may be >size().

size_type *size* () const\\
As per Table mumble.

** Protected Attributes
pointer *_M_buffer*\\

size_type *_M_len*\\

size_type *_M_original_len*\\

* Detailed Description
** "template<class _ForwardIterator, class _Tp = typename
std::iterator_traits<_ForwardIterator>::value_type>
\\
struct __gnu_cxx::temporary_buffer< _ForwardIterator, _Tp >"This class
provides similar behavior and semantics of the standard functions
get_temporary_buffer() and return_temporary_buffer(), but encapsulated
in a type vaguely resembling a standard container.

By default, a temporary_buffer<Iter> stores space for objects of
whatever type the Iter iterator points to. It is constructed from a
typical [first,last) range, and provides the begin(), end(), size()
functions, as well as requested_size(). For non-trivial types, copies of
*first will be used to initialize the storage.

=malloc= is used to obtain underlying storage.

Like get_temporary_buffer(), not all the requested memory may be
available. Ideally, the created buffer will be large enough to hold a
copy of [first,last), but if size() is less than requested_size(), then
this didn't happen.

Definition at line *184* of file *ext/memory*.

* Member Typedef Documentation
** template<typename _ForwardIterator , typename _Tp > typedef pointer
*std::_Temporary_buffer*< _ForwardIterator, _Tp
>::iterator= [inherited]=
Definition at line *144* of file *stl_tempbuf.h*.

** template<typename _ForwardIterator , typename _Tp > typedef
value_type* *std::_Temporary_buffer*< _ForwardIterator, _Tp
>::pointer= [inherited]=
Definition at line *143* of file *stl_tempbuf.h*.

** template<typename _ForwardIterator , typename _Tp > typedef ptrdiff_t
*std::_Temporary_buffer*< _ForwardIterator, _Tp
>::size_type= [inherited]=
Definition at line *145* of file *stl_tempbuf.h*.

** template<typename _ForwardIterator , typename _Tp > typedef _Tp
*std::_Temporary_buffer*< _ForwardIterator, _Tp
>::value_type= [inherited]=
Definition at line *142* of file *stl_tempbuf.h*.

* Constructor & Destructor Documentation
** template<class _ForwardIterator , class _Tp = typename
std::iterator_traits<_ForwardIterator>::value_type>
*__gnu_cxx::temporary_buffer*< _ForwardIterator, _Tp
>::*temporary_buffer* (_ForwardIterator __first, _ForwardIterator
__last)= [inline]=
Requests storage large enough to hold a copy of [first,last).

Definition at line *187* of file *ext/memory*.

** template<class _ForwardIterator , class _Tp = typename
std::iterator_traits<_ForwardIterator>::value_type>
*__gnu_cxx::temporary_buffer*< _ForwardIterator, _Tp
>::~*temporary_buffer* ()= [inline]=
Destroys objects and frees storage.

Definition at line *193* of file *ext/memory*.

* Member Function Documentation
** template<typename _ForwardIterator , typename _Tp > iterator
*std::_Temporary_buffer*< _ForwardIterator, _Tp >::begin ()= [inline]=,
= [inherited]=
As per Table mumble.

Definition at line *165* of file *stl_tempbuf.h*.

** template<typename _ForwardIterator , typename _Tp > iterator
*std::_Temporary_buffer*< _ForwardIterator, _Tp >::end ()= [inline]=,
= [inherited]=
As per Table mumble.

Definition at line *170* of file *stl_tempbuf.h*.

** template<typename _ForwardIterator , typename _Tp > size_type
*std::_Temporary_buffer*< _ForwardIterator, _Tp >::requested_size ()
const= [inline]=, = [inherited]=
Returns the size requested by the constructor; may be >size().

Definition at line *160* of file *stl_tempbuf.h*.

** template<typename _ForwardIterator , typename _Tp > size_type
*std::_Temporary_buffer*< _ForwardIterator, _Tp >::size ()
const= [inline]=, = [inherited]=
As per Table mumble.

Definition at line *155* of file *stl_tempbuf.h*.

* Member Data Documentation
** template<typename _ForwardIterator , typename _Tp > pointer
*std::_Temporary_buffer*< _ForwardIterator, _Tp
>::_M_buffer= [protected]=, = [inherited]=
Definition at line *150* of file *stl_tempbuf.h*.

** template<typename _ForwardIterator , typename _Tp > size_type
*std::_Temporary_buffer*< _ForwardIterator, _Tp >::_M_len= [protected]=,
= [inherited]=
Definition at line *149* of file *stl_tempbuf.h*.

** template<typename _ForwardIterator , typename _Tp > size_type
*std::_Temporary_buffer*< _ForwardIterator, _Tp
>::_M_original_len= [protected]=, = [inherited]=
Definition at line *148* of file *stl_tempbuf.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
