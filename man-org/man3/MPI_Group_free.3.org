#+TITLE: Manpages - MPI_Group_free.3
#+DESCRIPTION: Linux manpage for MPI_Group_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Group_free * - Frees a group.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Group_free(MPI_Group *group)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_GROUP_FREE(GROUP, IERROR)
  	INTEGER	GROUP, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Group_free(group, ierror)
  	TYPE(MPI_Group), INTENT(INOUT) :: group
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  void Group::Free()
#+end_example

* INPUT/OUTPUT PARAMETER
- group :: Group (handle).

* OUTPUT PARAMETER
- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
This operation marks a group object for deallocation. The handle group
is set to MPI_GROUP_NULL by the call. Any ongoing operation using this
group will complete normally.

* NOTE
On return, group is set to MPI_GROUP_NULL.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.
