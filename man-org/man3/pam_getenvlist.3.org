#+TITLE: Manpages - pam_getenvlist.3
#+DESCRIPTION: Linux manpage for pam_getenvlist.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_getenvlist - getting the PAM environment

* SYNOPSIS
#+begin_example
  #include <security/pam_appl.h>
#+end_example

*char **pam_getenvlist(pam_handle_t **/pamh/*);*

* DESCRIPTION
The *pam_getenvlist* function returns a complete copy of the PAM
environment as associated with the handle /pamh/. The PAM environment
variables represent the contents of the regular environment variables of
the authenticated user when service is granted.

The format of the memory is a malloc()d array of char pointers, the last
element of which is set to NULL. Each of the non-NULL entries in this
array point to a NUL terminated and malloc()d char string of the form:
"/name=value/".

It should be noted that this memory will never be free()d by libpam.
Once obtained by a call to *pam_getenvlist*, it is the responsibility of
the calling application to free() this memory.

It is by design, and not a coincidence, that the format and contents of
the returned array matches that required for the third argument of the
*execle*(3) function call.

* RETURN VALUES
The *pam_getenvlist* function returns NULL on failure.

* SEE ALSO
*pam_start*(3), *pam_getenv*(3), *pam_putenv*(3), *pam*(8)
