#+TITLE: Manpages - ne_buffer_altered.3
#+DESCRIPTION: Linux manpage for ne_buffer_altered.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ne_buffer_altered.3 is found in manpage for: [[../ne_buffer_clear.3][ne_buffer_clear.3]]