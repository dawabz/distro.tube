#+TITLE: Manpages - FcCharSetEqual.3
#+DESCRIPTION: Linux manpage for FcCharSetEqual.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcCharSetEqual - Compare two charsets

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcCharSetEqual (const FcCharSet */a/*, const FcCharSet **/b/*);*

* DESCRIPTION
Returns whether /a/ and /b/ contain the same set of Unicode chars.
