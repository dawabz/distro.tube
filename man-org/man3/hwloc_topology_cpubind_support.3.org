#+TITLE: Manpages - hwloc_topology_cpubind_support.3
#+DESCRIPTION: Linux manpage for hwloc_topology_cpubind_support.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwloc_topology_cpubind_support

* SYNOPSIS
\\

=#include <hwloc.h>=

** Data Fields
unsigned char *set_thisproc_cpubind*\\

unsigned char *get_thisproc_cpubind*\\

unsigned char *set_proc_cpubind*\\

unsigned char *get_proc_cpubind*\\

unsigned char *set_thisthread_cpubind*\\

unsigned char *get_thisthread_cpubind*\\

unsigned char *set_thread_cpubind*\\

unsigned char *get_thread_cpubind*\\

unsigned char *get_thisproc_last_cpu_location*\\

unsigned char *get_proc_last_cpu_location*\\

unsigned char *get_thisthread_last_cpu_location*\\

* Detailed Description
Flags describing actual PU binding support for this topology.

A flag may be set even if the feature isn't supported in all cases (e.g.
binding to random sets of non-contiguous objects).

* Field Documentation
** unsigned char hwloc_topology_cpubind_support::get_proc_cpubind
Getting the binding of a whole given process is supported.

** unsigned char
hwloc_topology_cpubind_support::get_proc_last_cpu_location
Getting the last processors where a whole process ran is supported

** unsigned char hwloc_topology_cpubind_support::get_thisproc_cpubind
Getting the binding of the whole current process is supported.

** unsigned char
hwloc_topology_cpubind_support::get_thisproc_last_cpu_location
Getting the last processors where the whole current process ran is
supported

** unsigned char hwloc_topology_cpubind_support::get_thisthread_cpubind
Getting the binding of the current thread only is supported.

** unsigned char
hwloc_topology_cpubind_support::get_thisthread_last_cpu_location
Getting the last processors where the current thread ran is supported

** unsigned char hwloc_topology_cpubind_support::get_thread_cpubind
Getting the binding of a given thread only is supported.

** unsigned char hwloc_topology_cpubind_support::set_proc_cpubind
Binding a whole given process is supported.

** unsigned char hwloc_topology_cpubind_support::set_thisproc_cpubind
Binding the whole current process is supported.

** unsigned char hwloc_topology_cpubind_support::set_thisthread_cpubind
Binding the current thread only is supported.

** unsigned char hwloc_topology_cpubind_support::set_thread_cpubind
Binding a given thread only is supported.

* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
