#+TITLE: Manpages - endusershell.3
#+DESCRIPTION: Linux manpage for endusershell.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about endusershell.3 is found in manpage for: [[../man3/getusershell.3][man3/getusershell.3]]