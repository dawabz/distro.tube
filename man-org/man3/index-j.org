#+TITLE: Man3 - J
#+DESCRIPTION: Man3 - J
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* J
#+begin_src bash :exports results
readarray -t starts_with_j < <(find . -type f -iname "j*" | sort)

for x in "${starts_with_j[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./j0.3.org][j0.3]]                           |
| [[file:./j0f.3.org][j0f.3]]                          |
| [[file:./j0l.3.org][j0l.3]]                          |
| [[file:./j1.3.org][j1.3]]                           |
| [[file:./j1f.3.org][j1f.3]]                          |
| [[file:./j1l.3.org][j1l.3]]                          |
| [[file:./JavaScript_Minifier_XS.3pm.org][JavaScript_Minifier_XS.3pm]]     |
| [[file:./jemalloc.3.org][jemalloc.3]]                     |
| [[file:./jn.3.org][jn.3]]                           |
| [[file:./jnf.3.org][jnf.3]]                          |
| [[file:./jnl.3.org][jnl.3]]                          |
| [[file:./jrand48.3.org][jrand48.3]]                      |
| [[file:./jrand48_r.3.org][jrand48_r.3]]                    |
| [[file:./JSON.3pm.org][JSON.3pm]]                       |
| [[file:./JSON_backportPP.3pm.org][JSON_backportPP.3pm]]            |
| [[file:./JSON_backportPP_Boolean.3pm.org][JSON_backportPP_Boolean.3pm]]    |
| [[file:./JSON_backportPP_Compat5005.3pm.org][JSON_backportPP_Compat5005.3pm]] |
| [[file:./JSON_backportPP_Compat5006.3pm.org][JSON_backportPP_Compat5006.3pm]] |
| [[file:./JSON_PP.3perl.org][JSON_PP.3perl]]                  |
| [[file:./JSON_PP_Boolean.3perl.org][JSON_PP_Boolean.3perl]]          |
