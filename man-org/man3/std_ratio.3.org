#+TITLE: Manpages - std_ratio.3
#+DESCRIPTION: Linux manpage for std_ratio.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::ratio< _Num, _Den > - Provides compile-time rational arithmetic.

* SYNOPSIS
\\

** Public Types
typedef *ratio*< num, den > *type*\\

** Static Public Attributes
static constexpr intmax_t *den*\\

static constexpr intmax_t *num*\\

* Detailed Description
** "template<intmax_t _Num, intmax_t _Den = 1>
\\
struct std::ratio< _Num, _Den >"Provides compile-time rational
arithmetic.

This class template represents any finite rational number with a
numerator and denominator representable by compile-time constants of
type intmax_t. The ratio is simplified when instantiated.

For example:

#+begin_example
  std::ratio<7,-21>::num == -1;
  std::ratio<7,-21>::den == 3;
#+end_example

Definition at line *266* of file *std/ratio*.

* Member Typedef Documentation
** template<intmax_t _Num, intmax_t _Den = 1> typedef *ratio*<num, den>
*std::ratio*< _Num, _Den >::*type*
Definition at line *279* of file *std/ratio*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
