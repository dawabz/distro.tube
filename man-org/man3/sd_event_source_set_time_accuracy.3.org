#+TITLE: Manpages - sd_event_source_set_time_accuracy.3
#+DESCRIPTION: Linux manpage for sd_event_source_set_time_accuracy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sd_event_source_set_time_accuracy.3 is found in manpage for: [[../sd_event_add_time.3][sd_event_add_time.3]]