#+TITLE: Manpages - isfinite.3
#+DESCRIPTION: Linux manpage for isfinite.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about isfinite.3 is found in manpage for: [[../man3/fpclassify.3][man3/fpclassify.3]]