#+TITLE: Manpages - sd_bus_slot_set_description.3
#+DESCRIPTION: Linux manpage for sd_bus_slot_set_description.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_bus_slot_set_description, sd_bus_slot_get_description - Set or query
the description of bus slot objects

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

*int sd_bus_slot_set_description(sd_bus_slot* */slot/*, const char
**/description/*);*

*int sd_bus_slot_get_description(sd_bus_slot* */bus/*, const char
***/description/*);*

* DESCRIPTION
*sd_bus_slot_set_description()* sets the description string that is used
in logging to the specified string. The string is copied internally and
freed when the bus slot object is deallocated. The /description/
argument may be *NULL*, in which case the description is unset.

*sd_bus_slot_get_description()* returns a description string in
/description/. If the string is not set, e.g. with
*sd_bus_slot_set_description()*, and the slot is a bus match callback
slot, the match string will be returned. Otherwise, *-ENXIO* is
returned.

* RETURN VALUE
On success, these functions return 0 or a positive integer. On failure,
they return a negative errno-style error code.

** Errors
Returned errors may indicate the following problems:

*-EINVAL*

#+begin_quote
  An required argument is *NULL*.
#+end_quote

*-ENXIO*

#+begin_quote
  The bus slot object has no description.
#+end_quote

*-ENOMEM*

#+begin_quote
  Memory allocation failed.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-bus*(3) *sd_bus_slot_ref*(3),
*sd_bus_slot_set_userdata*(3)
