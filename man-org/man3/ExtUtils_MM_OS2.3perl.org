#+TITLE: Manpages - ExtUtils_MM_OS2.3perl
#+DESCRIPTION: Linux manpage for ExtUtils_MM_OS2.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ExtUtils::MM_OS2 - methods to override UN*X behaviour in
ExtUtils::MakeMaker

* SYNOPSIS
use ExtUtils::MM_OS2; # Done internally by ExtUtils::MakeMaker if needed

* DESCRIPTION
See ExtUtils::MM_Unix for a documentation of the methods provided there.
This package overrides the implementation of these methods, not the
semantics.

* METHODS
- init_dist :: Define TO_UNIX to convert OS2 linefeeds to Unix style.

- init_linker :: 

- os_flavor :: 

OS/2 is OS/2

- xs_static_lib_is_xs :: 
