#+TITLE: Manpages - FcConfigDestroy.3
#+DESCRIPTION: Linux manpage for FcConfigDestroy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigDestroy - Destroy a configuration

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcConfigDestroy (FcConfig */config/*);*

* DESCRIPTION
Decrements the config reference count. If all references are gone,
destroys the configuration and any data associated with it. Note that
calling this function with the return from FcConfigGetCurrent will cause
a new configuration to be created for use as current configuration.
