#+TITLE: Manpages - std_piecewise_linear_distribution.3
#+DESCRIPTION: Linux manpage for std_piecewise_linear_distribution.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::piecewise_linear_distribution< _RealType > - A
piecewise_linear_distribution random number distribution.

* SYNOPSIS
\\

=#include <random.h>=

** Classes
struct *param_type*\\

** Public Types
typedef _RealType *result_type*\\

** Public Member Functions
template<typename _InputIteratorB , typename _InputIteratorW >
*piecewise_linear_distribution* (_InputIteratorB __bfirst,
_InputIteratorB __bend, _InputIteratorW __wbegin)\\

*piecewise_linear_distribution* (const *param_type* &__p)\\

template<typename _Func > *piecewise_linear_distribution*
(*initializer_list*< _RealType > __bl, _Func __fw)\\

template<typename _Func > *piecewise_linear_distribution* (size_t __nw,
_RealType __xmin, _RealType __xmax, _Func __fw)\\

template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void *__generate* (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator &__urng)\\

template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void *__generate* (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator &__urng, const
*param_type* &__p)\\

template<typename _UniformRandomNumberGenerator > void *__generate*
(*result_type* *__f, *result_type* *__t, _UniformRandomNumberGenerator
&__urng, const *param_type* &__p)\\

*std::vector*< double > *densities* () const\\
Return a vector of the probability densities of the distribution.

*std::vector*< _RealType > *intervals* () const\\
Return the intervals of the distribution.

*result_type* *max* () const\\
Returns the least upper bound value of the distribution.

*result_type* *min* () const\\
Returns the greatest lower bound value of the distribution.

template<typename _UniformRandomNumberGenerator > *result_type*
*operator()* (_UniformRandomNumberGenerator &__urng)\\
Generating functions.

template<typename _UniformRandomNumberGenerator > *result_type*
*operator()* (_UniformRandomNumberGenerator &__urng, const *param_type*
&__p)\\

*param_type* *param* () const\\
Returns the parameter set of the distribution.

void *param* (const *param_type* &__param)\\
Sets the parameter set of the distribution.

void *reset* ()\\

** Friends
template<typename _RealType1 , typename _CharT , typename _Traits >
*std::basic_ostream*< _CharT, _Traits > & *operator<<*
(*std::basic_ostream*< _CharT, _Traits > &__os, const
*std::piecewise_linear_distribution*< _RealType1 > &__x)\\
Inserts a piecewise_linear_distribution random number distribution =__x=
into the output stream =__os=.

bool *operator==* (const *piecewise_linear_distribution* &__d1, const
*piecewise_linear_distribution* &__d2)\\
Return true if two piecewise linear distributions have the same
parameters.

template<typename _RealType1 , typename _CharT , typename _Traits >
*std::basic_istream*< _CharT, _Traits > & *operator>>*
(*std::basic_istream*< _CharT, _Traits > &__is,
*std::piecewise_linear_distribution*< _RealType1 > &__x)\\
Extracts a piecewise_linear_distribution random number distribution
=__x= from the input stream =__is=.

* Detailed Description
** "template<typename _RealType = double>
\\
class std::piecewise_linear_distribution< _RealType >"A
piecewise_linear_distribution random number distribution.

The formula for the piecewise linear probability mass function is

Definition at line *5785* of file *random.h*.

* Member Typedef Documentation
** template<typename _RealType = double> typedef _RealType
*std::piecewise_linear_distribution*< _RealType >::*result_type*
The type of the range of the distribution.

Definition at line *5792* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _RealType = double>
*std::piecewise_linear_distribution*< _RealType
>::*piecewise_linear_distribution* ()= [inline]=
Definition at line *5855* of file *random.h*.

** template<typename _RealType = double> template<typename
_InputIteratorB , typename _InputIteratorW >
*std::piecewise_linear_distribution*< _RealType
>::*piecewise_linear_distribution* (_InputIteratorB __bfirst,
_InputIteratorB __bend, _InputIteratorW __wbegin)= [inline]=
Definition at line *5860* of file *random.h*.

** template<typename _RealType = double> template<typename _Func >
*std::piecewise_linear_distribution*< _RealType
>::*piecewise_linear_distribution* (*initializer_list*< _RealType >
__bl, _Func __fw)= [inline]=
Definition at line *5867* of file *random.h*.

** template<typename _RealType = double> template<typename _Func >
*std::piecewise_linear_distribution*< _RealType
>::*piecewise_linear_distribution* (size_t __nw, _RealType __xmin,
_RealType __xmax, _Func __fw)= [inline]=
Definition at line *5873* of file *random.h*.

** template<typename _RealType = double>
*std::piecewise_linear_distribution*< _RealType
>::*piecewise_linear_distribution* (const *param_type* &
__p)= [inline]=, = [explicit]=
Definition at line *5880* of file *random.h*.

* Member Function Documentation
** template<typename _RealType = double> template<typename
_ForwardIterator , typename _UniformRandomNumberGenerator > void
*std::piecewise_linear_distribution*< _RealType >::__generate
(_ForwardIterator __f, _ForwardIterator __t,
_UniformRandomNumberGenerator & __urng)= [inline]=
Definition at line *5969* of file *random.h*.

** template<typename _RealType = double> template<typename
_ForwardIterator , typename _UniformRandomNumberGenerator > void
*std::piecewise_linear_distribution*< _RealType >::__generate
(_ForwardIterator __f, _ForwardIterator __t,
_UniformRandomNumberGenerator & __urng, const *param_type* &
__p)= [inline]=
Definition at line *5976* of file *random.h*.

** template<typename _RealType = double> template<typename
_UniformRandomNumberGenerator > void
*std::piecewise_linear_distribution*< _RealType >::__generate
(*result_type* * __f, *result_type* * __t, _UniformRandomNumberGenerator
& __urng, const *param_type* & __p)= [inline]=
Definition at line *5983* of file *random.h*.

** template<typename _RealType = double> *std::vector*< double >
*std::piecewise_linear_distribution*< _RealType >::densities ()
const= [inline]=
Return a vector of the probability densities of the distribution.

Definition at line *5912* of file *random.h*.

** template<typename _RealType = double> *std::vector*< _RealType >
*std::piecewise_linear_distribution*< _RealType >::intervals ()
const= [inline]=
Return the intervals of the distribution.

Definition at line *5895* of file *random.h*.

** template<typename _RealType = double> *result_type*
*std::piecewise_linear_distribution*< _RealType >::max ()
const= [inline]=
Returns the least upper bound value of the distribution.

Definition at line *5947* of file *random.h*.

** template<typename _RealType = double> *result_type*
*std::piecewise_linear_distribution*< _RealType >::min ()
const= [inline]=
Returns the greatest lower bound value of the distribution.

Definition at line *5937* of file *random.h*.

** template<typename _RealType = double> template<typename
_UniformRandomNumberGenerator > *result_type*
*std::piecewise_linear_distribution*< _RealType >::operator()
(_UniformRandomNumberGenerator & __urng)= [inline]=
Generating functions.

Definition at line *5958* of file *random.h*.

References *std::piecewise_linear_distribution< _RealType
>::operator()()*.

Referenced by *std::piecewise_linear_distribution< _RealType
>::operator()()*.

** template<typename _RealType > template<typename
_UniformRandomNumberGenerator > *piecewise_linear_distribution*<
_RealType >::*result_type* *std::piecewise_linear_distribution*<
_RealType >::operator() (_UniformRandomNumberGenerator & __urng, const
*param_type* & __p)
Definition at line *3126* of file *bits/random.tcc*.

** template<typename _RealType = double> *param_type*
*std::piecewise_linear_distribution*< _RealType >::param ()
const= [inline]=
Returns the parameter set of the distribution.

Definition at line *5922* of file *random.h*.

** template<typename _RealType = double> void
*std::piecewise_linear_distribution*< _RealType >::param (const
*param_type* & __param)= [inline]=
Sets the parameter set of the distribution.

*Parameters*

#+begin_quote
  /__param/ The new parameter set of the distribution.
#+end_quote

Definition at line *5930* of file *random.h*.

** template<typename _RealType = double> void
*std::piecewise_linear_distribution*< _RealType >::reset ()= [inline]=
Resets the distribution state.

Definition at line *5888* of file *random.h*.

* Friends And Related Function Documentation
** template<typename _RealType = double> template<typename _RealType1 ,
typename _CharT , typename _Traits > *std::basic_ostream*< _CharT,
_Traits > & operator<< (*std::basic_ostream*< _CharT, _Traits > & __os,
const *std::piecewise_linear_distribution*< _RealType1 > &
__x)= [friend]=
Inserts a piecewise_linear_distribution random number distribution =__x=
into the output stream =__os=.

*Parameters*

#+begin_quote
  /__os/ An output stream.\\
  /__x/ A piecewise_linear_distribution random number distribution.
#+end_quote

*Returns*

#+begin_quote
  The output stream with the state of =__x= inserted or in an error
  state.
#+end_quote

** template<typename _RealType = double> bool operator== (const
*piecewise_linear_distribution*< _RealType > & __d1, const
*piecewise_linear_distribution*< _RealType > & __d2)= [friend]=
Return true if two piecewise linear distributions have the same
parameters.

Definition at line *5993* of file *random.h*.

** template<typename _RealType = double> template<typename _RealType1 ,
typename _CharT , typename _Traits > *std::basic_istream*< _CharT,
_Traits > & operator>> (*std::basic_istream*< _CharT, _Traits > & __is,
*std::piecewise_linear_distribution*< _RealType1 > & __x)= [friend]=
Extracts a piecewise_linear_distribution random number distribution
=__x= from the input stream =__is=.

*Parameters*

#+begin_quote
  /__is/ An input stream.\\
  /__x/ A piecewise_linear_distribution random number generator engine.
#+end_quote

*Returns*

#+begin_quote
  The input stream with =__x= extracted or in an error state.
#+end_quote

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
