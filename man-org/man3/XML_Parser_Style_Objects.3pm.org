#+TITLE: Manpages - XML_Parser_Style_Objects.3pm
#+DESCRIPTION: Linux manpage for XML_Parser_Style_Objects.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
XML::Parser::Style::Objects - Objects styler parser

* SYNOPSIS
use XML::Parser; my $p = XML::Parser->new(Style => Objects, Pkg =>
MyNode); my $tree = $p->parsefile(foo.xml);

* DESCRIPTION
This module implements XML::Parser's Objects style parser.

This is similar to the Tree style, except that a hash object is created
for each element. The corresponding object will be in the class whose
name is created by appending :: and the element name to the package set
with the Pkg option. Non-markup text will be in the ::Characters class.
The contents of the corresponding object will be in an anonymous array
that is the value of the Kids property for that object.

* SEE ALSO
XML::Parser::Style::Tree
