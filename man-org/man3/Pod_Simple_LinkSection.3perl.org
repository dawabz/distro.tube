#+TITLE: Manpages - Pod_Simple_LinkSection.3perl
#+DESCRIPTION: Linux manpage for Pod_Simple_LinkSection.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Pod::Simple::LinkSection -- represent "section" attributes of L codes

* SYNOPSIS
# a long story

* DESCRIPTION
This class is not of interest to general users.

Pod::Simple uses this class for representing the value of the section
attribute of L start-element events. Most applications can just use the
normal stringification of objects of this class; they stringify to just
the text content of the section, such as foo for =L<Stuff/foo>=, and bar
for =L<Stuff/bI<ar>>=.

However, anyone particularly interested in getting the full value of the
treelet, can just traverse the content of the treeleet @$treelet_object.
To wit:

% perl -MData::Dumper -e "use base qw(Pod::Simple::Methody); sub start_L
{ print Dumper($_[1]{section} ) }
_ _PACKAGE_ _->new->parse_string_document(=head1 L<Foo/bI<ar>baz>>) "
Output: $VAR1 = bless( [ , {}, b, bless( [ I, {}, ar ],
Pod::Simple::LinkSection ), baz ], Pod::Simple::LinkSection );

But stringify it and you get just the text content:

% perl -MData::Dumper -e "use base qw(Pod::Simple::Methody); sub start_L
{ print Dumper( . $_[1]{section} ) }
_ _PACKAGE_ _->new->parse_string_document(=head1 L<Foo/bI<ar>baz>>) "
Output: $VAR1 = barbaz;

* SEE ALSO
Pod::Simple

* SUPPORT
Questions or discussion about POD and Pod::Simple should be sent to the
pod-people@perl.org mail list. Send an empty email to
pod-people-subscribe@perl.org to subscribe.

This module is managed in an open GitHub repository,
<https://github.com/perl-pod/pod-simple/>. Feel free to fork and
contribute, or to clone <git://github.com/perl-pod/pod-simple.git> and
send patches!

Patches against Pod::Simple are welcome. Please send bug reports to
<bug-pod-simple@rt.cpan.org>.

* COPYRIGHT AND DISCLAIMERS
Copyright (c) 2004 Sean M. Burke.

This library is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

This program is distributed in the hope that it will be useful, but
without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose.

* AUTHOR
Pod::Simple was created by Sean M. Burke <sburke@cpan.org>. But don't
bother him, he's retired.

Pod::Simple is maintained by:

- Allison Randal =allison@perl.org=

- Hans Dieter Pearcey =hdp@cpan.org=

- David E. Wheeler =dwheeler@cpan.org=
