#+TITLE: Manpages - __gnu_pbds_detail_stored_hash.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_detail_stored_hash.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::detail::stored_hash< _Th > - Stored hash.

* SYNOPSIS
\\

=#include <types_traits.hpp>=

Inherited by *__gnu_pbds::detail::stored_data< _Tv, _Th, Store_Hash >*.

** Public Types
typedef _Th *hash_type*\\

** Public Attributes
hash_type *m_hash*\\

* Detailed Description
** "template<typename _Th>
\\
struct __gnu_pbds::detail::stored_hash< _Th >"Stored hash.

Definition at line *86* of file *types_traits.hpp*.

* Member Typedef Documentation
** template<typename _Th > typedef _Th
*__gnu_pbds::detail::stored_hash*< _Th >::hash_type
Definition at line *88* of file *types_traits.hpp*.

* Member Data Documentation
** template<typename _Th > hash_type *__gnu_pbds::detail::stored_hash*<
_Th >::m_hash
Definition at line *89* of file *types_traits.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
