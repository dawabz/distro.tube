#+TITLE: Manpages - FcFontSetPrint.3
#+DESCRIPTION: Linux manpage for FcFontSetPrint.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcFontSetPrint - Print a set of patterns to stdout

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcFontSetPrint (FcFontSet */set/*);*

* DESCRIPTION
This function is useful for diagnosing font related issues, printing the
complete contents of every pattern in /set/. The format of the output is
designed to be of help to users and developers, and may change at any
time.
