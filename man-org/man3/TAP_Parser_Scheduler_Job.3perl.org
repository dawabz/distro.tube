#+TITLE: Manpages - TAP_Parser_Scheduler_Job.3perl
#+DESCRIPTION: Linux manpage for TAP_Parser_Scheduler_Job.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
TAP::Parser::Scheduler::Job - A single testing job.

* VERSION
Version 3.43

* SYNOPSIS
use TAP::Parser::Scheduler::Job;

* DESCRIPTION
Represents a single test 'job'.

* METHODS
** Class Methods
/=new=/

my $job = TAP::Parser::Scheduler::Job->new( $filename, $description );

Given the filename and description of a test as scalars, returns a new
TAP::Parser::Scheduler::Job object.

** Instance Methods
/=on_finish=/

$self->on_finish(\&method).

Register a closure to be called when this job is destroyed. The callback
will be passed the =TAP::Parser::Scheduler::Job= object as it's only
argument.

/=finish=/

$self->finish;

Called when a job is complete to unlock it. If a callback has been
registered with =on_finish=, it calls it. Otherwise, it does nothing.

** Attributes
$self->filename; $self->description; $self->context;

These are all getters which return the data set for these attributes
during object construction.

/=filename=/

/=description=/

/=context=/

/=as_array_ref=/

For backwards compatibility in callbacks.

/=is_spinner=/

$self->is_spinner;

Returns false indicating that this is a real job rather than a
'spinner'. Spinners are returned when the scheduler still has pending
jobs but can't (because of locking) return one right now.
