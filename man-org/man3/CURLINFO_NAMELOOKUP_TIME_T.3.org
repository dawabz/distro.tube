#+TITLE: Manpages - CURLINFO_NAMELOOKUP_TIME_T.3
#+DESCRIPTION: Linux manpage for CURLINFO_NAMELOOKUP_TIME_T.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLINFO_NAMELOOKUP_TIME_T - get the name lookup time in microseconds

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_NAMELOOKUP_TIME_T,
curl_off_t *timep);

* DESCRIPTION
Pass a pointer to a curl_off_t to receive the total time in microseconds
from the start until the name resolving was completed.

When a redirect is followed, the time from each request is added
together.

See also the TIMES overview in the /curl_easy_getinfo(3)/ man page.

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  curl = curl_easy_init();
  if(curl) {
    curl_off_t namelookup;
    curl_easy_setopt(curl, CURLOPT_URL, url);
    res = curl_easy_perform(curl);
    if(CURLE_OK == res) {
      res = curl_easy_getinfo(curl, CURLINFO_NAMELOOKUP_TIME_T, &namelookup);
      if(CURLE_OK == res) {
        printf("Time: %" CURL_FORMAT_CURL_OFF_T ".%06ld", namelookup / 1000000,
               (long)(namelookup % 1000000));
      }
    }
    /* always cleanup */
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.61.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*curl_easy_getinfo*(3), *curl_easy_setopt*(3),
*CURLINFO_NAMELOOKUP_TIME*(3)
