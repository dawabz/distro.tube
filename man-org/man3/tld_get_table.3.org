#+TITLE: Manpages - tld_get_table.3
#+DESCRIPTION: Linux manpage for tld_get_table.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tld_get_table - API function

* SYNOPSIS
*#include <tld.h>*

*const Tld_table * tld_get_table(const char * */tld/*, const Tld_table
** */tables/*);*

* ARGUMENTS
- const char * tld :: TLD name (e.g. "com") as zero terminated ASCII
  byte string.

- const Tld_table ** tables :: Zero terminated array of *Tld_table*
  info-structures for TLDs.

* DESCRIPTION
Get the TLD table for a named TLD by searching through the given TLD
table array.

Return value: Return structure corresponding to TLD /tld/ by going thru
/tables/ , or return *NULL* if no such structure is found.

* DESCRIPTION
Get the TLD table for a named TLD by searching through the given TLD
table array.

Return value: Return structure corresponding to TLD /tld/ by going thru
/tables/ , or return *NULL* if no such structure is found.

* REPORTING BUGS
Report bugs to <help-libidn@gnu.org>.\\
General guidelines for reporting bugs: http://www.gnu.org/gethelp/\\
GNU Libidn home page: http://www.gnu.org/software/libidn/

* COPYRIGHT
Copyright © 2002-2021 Simon Josefsson.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libidn* is maintained as a Texinfo manual.
If the *info* and *libidn* programs are properly installed at your site,
the command

#+begin_quote
  *info libidn*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *http://www.gnu.org/software/libidn/manual/*
#+end_quote
