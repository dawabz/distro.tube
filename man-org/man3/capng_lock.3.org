#+TITLE: Manpages - capng_lock.3
#+DESCRIPTION: Linux manpage for capng_lock.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
capng_lock - lock the current process capabilities settings

* SYNOPSIS
*#include <cap-ng.h>*

int capng_lock(void);

* DESCRIPTION
capng_lock will take steps to prevent children of the current process to
regain full privileges if the uid is 0. This should be called while
possessing the CAP_SETPCAP capability in the kernel. This function will
do the following if permitted by the kernel: Set the NOROOT option on
for PR_SET_SECUREBITS, set the NOROOT_LOCKED option to on for
PR_SET_SECUREBITS, set the PR_NO_SETUID_FIXUP option on for
PR_SET_SECUREBITS, and set the PR_NO_SETUID_FIXUP_LOCKED option on for
PR_SET_SECUREBITS.

* RETURN VALUE
This returns 0 on success and a negative number on failure. -1 means a
failure setting any of the PR_SET_SECUREBITS options.

* SEE ALSO
*capng_apply*(3), *prctl*(2), *capabilities*(7)

* AUTHOR
Steve Grubb
