#+TITLE: Manpages - XtUngrabPointer.3
#+DESCRIPTION: Linux manpage for XtUngrabPointer.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtUngrabPointer.3 is found in manpage for: [[../man3/XtGrabKey.3][man3/XtGrabKey.3]]