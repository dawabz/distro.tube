#+TITLE: Manpages - sd_event_inotify_handler_t.3
#+DESCRIPTION: Linux manpage for sd_event_inotify_handler_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sd_event_inotify_handler_t.3 is found in manpage for: [[../sd_event_add_inotify.3][sd_event_add_inotify.3]]