#+TITLE: Manpages - lo_Server.3
#+DESCRIPTION: Linux manpage for lo_Server.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lo::Server - Class representing a local OSC server, proxy for
*lo_server*.

* SYNOPSIS
\\

Inherited by *lo::ServerThread*.

** Public Member Functions
*Server* (*lo_server* s)\\

template<typename E > *Server* (const num_string_type &port, E &&e)\\

template<typename E > *Server* (const num_string_type &port, int proto,
E &&e=0)\\

template<typename E > *Server* (const string_type &group, const
num_string_type &port, const string_type &iface=0, const string_type
&ip=0, E &&e=0)\\

*Server* (const num_string_type &port, *lo_err_handler* err_h=0)\\

*Server* (const num_string_type &port, int proto, *lo_err_handler*
err_h=0)\\

*Server* (const string_type &group, const num_string_type &port, const
string_type &iface='', const string_type &ip='', *lo_err_handler*
err_h=0)\\

virtual *~Server* ()\\

*Method* *add_method* (const string_type &path, const string_type
&types, *lo_method_handler* h, void *data) const\\

*LO_ADD_METHOD* ((const char *, const char *, *lo_arg* **, int),((char
*) 0,(char *) 0,(*lo_arg* **) 0,(int) 0),(path, types, argv, argc))\\

*LO_ADD_METHOD* ((const char *, *lo_arg* **, int),((char *) 0,(*lo_arg*
**) 0,(int) 0),(types, argv, argc))\\

* Detailed Description
Class representing a local OSC server, proxy for *lo_server*.

Definition at line 485 of file lo_cpp.h.

* Constructor & Destructor Documentation
** lo::Server::Server (*lo_server* s)= [inline]=
Constructor.

Definition at line 489 of file lo_cpp.h.

** template<typename E > lo::Server::Server (const num_string_type &
port, E && e)= [inline]=
Constructor taking an error handler.

Definition at line 493 of file lo_cpp.h.

** template<typename E > lo::Server::Server (const num_string_type &
port, int proto, E && e = =0=)= [inline]=
Constructor taking a port number and error handler.

Definition at line 510 of file lo_cpp.h.

** template<typename E > lo::Server::Server (const string_type & group,
const num_string_type & port, const string_type & iface = =0=, const
string_type & ip = =0=, E && e = =0=)= [inline]=
Constructor taking a multicast group, port number, interface identifier
or IP, and error handler.

Definition at line 528 of file lo_cpp.h.

** lo::Server::Server (const num_string_type & port, *lo_err_handler*
err_h = =0=)= [inline]=
Constructor taking a port number and error handler.

Definition at line 551 of file lo_cpp.h.

** lo::Server::Server (const num_string_type & port, int proto,
*lo_err_handler* err_h = =0=)= [inline]=
Constructor taking a port number, protocol, and error handler.

Definition at line 555 of file lo_cpp.h.

** lo::Server::Server (const string_type & group, const num_string_type
& port, const string_type & iface = =''=, const string_type & ip = =''=,
*lo_err_handler* err_h = =0=)= [inline]=
Constructor taking a multicast group, port number, interface identifier
or IP, and error handler.

Definition at line 561 of file lo_cpp.h.

** virtual lo::Server::~Server ()= [inline]=, = [virtual]=
Destructor

Definition at line 570 of file lo_cpp.h.

References lo_server_free().

* Member Function Documentation
** *Method* lo::Server::add_method (const string_type & path, const
string_type & types, *lo_method_handler* h, void * data)
const= [inline]=
Add a method to handle a given path and type, with a handler and user
data pointer.

Definition at line 579 of file lo_cpp.h.

** lo::Server::LO_ADD_METHOD ((const char *, const char *, *lo_arg* **,
int), ((char *) 0,(char *) 0,(*lo_arg* **) 0,(int) 0), (path, types,
argv, argc))
Add a method to handle a given path and type, with a handler taking
(argv, argc), user data.

** lo::Server::LO_ADD_METHOD ((const char *, *lo_arg* **, int), ((char
*) 0,(*lo_arg* **) 0,(int) 0), (types, argv, argc))
Add a method to handle a given path and type, with a handler taking
(types, argv, argc), user data.

* Author
Generated automatically by Doxygen for liblo from the source code.
