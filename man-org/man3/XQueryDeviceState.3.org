#+TITLE: Manpages - XQueryDeviceState.3
#+DESCRIPTION: Linux manpage for XQueryDeviceState.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XQueryDeviceState - query the state of an extension input device.

* SYNOPSIS
#+begin_example
  #include <X11/extensions/XInput.h>
#+end_example

#+begin_example
  XDeviceState* XQueryDeviceState( Display *display,
                                   XDevice *device);
#+end_example

#+begin_example
  display
         Specifies the connection to the X server.
#+end_example

#+begin_example
  device
         Specifies the device whose state is to be queried.
#+end_example

* DESCRIPTION

#+begin_quote
  #+begin_example
    The XQueryDeviceState request queries the state of an input
    device. The current state of keys and buttons (up or down), and
    valuators (current value) on the device is reported by this
    request. Each key or button is represented by a bit in the
    XDeviceState structure that is returned. Valuators on the
    device report 0 if they are reporting relative information, and
    the current value if they are reporting absolute information.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XQueryDeviceState can generate a BadDevice error.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    Structures:
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The XDeviceState structure contains:
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    typedef struct {
        XID device_id;
        int num_classes;
        XInputClass *data;
    } XDeviceState;
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The XValuatorState structure contains:
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    typedef struct {
        unsigned char class;
        unsigned char length;
        unsigned char num_valuators;
        unsigned char mode;
        int *valuators;
    } XValuatorState;
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The XKeyState structure contains:
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    typedef struct {
        unsigned char class;
        unsigned char length;
        short     num_keys;
        char keys[32];
    } XKeyState;
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The XButtonState structure contains:
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    typedef struct {
        unsigned char class;
        unsigned char length;
        short     num_buttons;
        char buttons[32];
    } XButtonState;
  #+end_example
#+end_quote

* DIAGNOSTICS

#+begin_quote
  #+begin_example
    BadDevice
           An invalid device was specified. The specified device
           does not exist or has not been opened by this client via
           XOpenInputDevice. This error may also occur if some
           other client has caused the specified device to become
           the X keyboard or X pointer device via the
           XChangeKeyboardDevice or XChangePointerDevice requests.
  #+end_example
#+end_quote
