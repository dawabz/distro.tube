#+TITLE: Manpages - udev_monitor_enable_receiving.3
#+DESCRIPTION: Linux manpage for udev_monitor_enable_receiving.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about udev_monitor_enable_receiving.3 is found in manpage for: [[../udev_monitor_receive_device.3][udev_monitor_receive_device.3]]