#+TITLE: Manpages - XtBuildEventMask.3
#+DESCRIPTION: Linux manpage for XtBuildEventMask.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtBuildEventMask - retrieve a widget's event mask

* SYNTAX
#include <X11/Intrinsic.h>

EventMask XtBuildEventMask(Widget /w/);

* ARGUMENTS

23. Specifies the widget.

* DESCRIPTION
The *XtBuildEventMask* function returns the event mask representing the
logical OR of all event masks for event handlers registered on the
widget with *XtAddEventHandler* and all event translations, including
accelerators, installed on the widget. This is the same event mask
stored into the *XSetWindowAttributes* structure by *XtRealizeWidget*
and sent to the server when event handlers and translations are
installed or removed on the realized widget.

* SEE ALSO
XtAddEventHandler(3)\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
