#+TITLE: Manpages - TAP_Parser_Scheduler_Spinner.3perl
#+DESCRIPTION: Linux manpage for TAP_Parser_Scheduler_Spinner.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
TAP::Parser::Scheduler::Spinner - A no-op job.

* VERSION
Version 3.43

* SYNOPSIS
use TAP::Parser::Scheduler::Spinner;

* DESCRIPTION
A no-op job. Returned by =TAP::Parser::Scheduler= as an instruction to
the harness to spin (keep executing tests) while the scheduler can't
return a real job.

* METHODS
** Class Methods
/=new=/

my $job = TAP::Parser::Scheduler::Spinner->new;

Ignores any arguments and returns a new
=TAP::Parser::Scheduler::Spinner= object.

** Instance Methods
/=is_spinner=/

Returns true indicating that is a 'spinner' job. Spinners are returned
when the scheduler still has pending jobs but can't (because of locking)
return one right now.

* SEE ALSO
TAP::Parser::Scheduler, TAP::Parser::Scheduler::Job
