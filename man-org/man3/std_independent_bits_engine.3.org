#+TITLE: Manpages - std_independent_bits_engine.3
#+DESCRIPTION: Linux manpage for std_independent_bits_engine.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::independent_bits_engine< _RandomNumberEngine, __w, _UIntType >

* SYNOPSIS
\\

=#include <random.h>=

** Public Types
typedef _UIntType *result_type*\\

** Public Member Functions
*independent_bits_engine* ()\\
Constructs a default independent_bits_engine engine.

*independent_bits_engine* (_RandomNumberEngine &&__rng)\\
Move constructs a independent_bits_engine engine.

template<typename _Sseq , typename = _If_seed_seq<_Sseq>>
*independent_bits_engine* (_Sseq &__q)\\
Generator construct a independent_bits_engine engine.

*independent_bits_engine* (const _RandomNumberEngine &__rng)\\
Copy constructs a independent_bits_engine engine.

*independent_bits_engine* (*result_type* __s)\\
Seed constructs a independent_bits_engine engine.

const _RandomNumberEngine & *base* () const noexcept\\
Gets a const reference to the underlying generator engine object.

void *discard* (unsigned long long __z)\\
Discard a sequence of random numbers.

*result_type* *operator()* ()\\
Gets the next value in the generated random number sequence.

void *seed* ()\\
Reseeds the independent_bits_engine object with the default seed for the
underlying base class generator engine.

template<typename _Sseq > _If_seed_seq< _Sseq > *seed* (_Sseq &__q)\\
Reseeds the independent_bits_engine object with the given seed sequence.

void *seed* (*result_type* __s)\\
Reseeds the independent_bits_engine object with the default seed for the
underlying base class generator engine.

** Static Public Member Functions
static constexpr *result_type* *max* ()\\
Gets the maximum value in the generated random number range.

static constexpr *result_type* *min* ()\\
Gets the minimum value in the generated random number range.

** Friends
bool *operator==* (const *independent_bits_engine* &__lhs, const
*independent_bits_engine* &__rhs)\\
Compares two independent_bits_engine random number generator objects of
the same type for equality.

template<typename _CharT , typename _Traits > *std::basic_istream*<
_CharT, _Traits > & *operator>>* (*std::basic_istream*< _CharT, _Traits
> &__is, *std::independent_bits_engine*< _RandomNumberEngine, __w,
_UIntType > &__x)\\
Extracts the current state of a % subtract_with_carry_engine random
number generator engine =__x= from the input stream =__is=.

* Detailed Description
** "template<typename _RandomNumberEngine, size_t __w, typename
_UIntType>
\\
class std::independent_bits_engine< _RandomNumberEngine, __w, _UIntType
>"Produces random numbers by combining random numbers from some base
engine to produce random numbers with a specified number of bits =__w=.

Definition at line *1105* of file *random.h*.

* Member Typedef Documentation
** template<typename _RandomNumberEngine , size_t __w, typename
_UIntType > typedef _UIntType *std::independent_bits_engine*<
_RandomNumberEngine, __w, _UIntType >::*result_type*
The type of the generated random value.

Definition at line *1118* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _RandomNumberEngine , size_t __w, typename
_UIntType > *std::independent_bits_engine*< _RandomNumberEngine, __w,
_UIntType >::*independent_bits_engine* ()= [inline]=
Constructs a default independent_bits_engine engine. The underlying
engine is default constructed as well.

Definition at line *1125* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __w, typename
_UIntType > *std::independent_bits_engine*< _RandomNumberEngine, __w,
_UIntType >::*independent_bits_engine* (const _RandomNumberEngine &
__rng)= [inline]=, = [explicit]=
Copy constructs a independent_bits_engine engine. Copies an existing
base class random number generator.

*Parameters*

#+begin_quote
  /__rng/ An existing (base class) engine object.
#+end_quote

Definition at line *1135* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __w, typename
_UIntType > *std::independent_bits_engine*< _RandomNumberEngine, __w,
_UIntType >::*independent_bits_engine* (_RandomNumberEngine &&
__rng)= [inline]=, = [explicit]=
Move constructs a independent_bits_engine engine. Copies an existing
base class random number generator.

*Parameters*

#+begin_quote
  /__rng/ An existing (base class) engine object.
#+end_quote

Definition at line *1145* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __w, typename
_UIntType > *std::independent_bits_engine*< _RandomNumberEngine, __w,
_UIntType >::*independent_bits_engine* (*result_type* __s)= [inline]=,
= [explicit]=
Seed constructs a independent_bits_engine engine. Constructs the
underlying generator engine seeded with =__s=.

*Parameters*

#+begin_quote
  /__s/ A seed value for the base class engine.
#+end_quote

Definition at line *1155* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __w, typename
_UIntType > template<typename _Sseq , typename = _If_seed_seq<_Sseq>>
*std::independent_bits_engine*< _RandomNumberEngine, __w, _UIntType
>::*independent_bits_engine* (_Sseq & __q)= [inline]=, = [explicit]=
Generator construct a independent_bits_engine engine.

*Parameters*

#+begin_quote
  /__q/ A seed sequence.
#+end_quote

Definition at line *1165* of file *random.h*.

* Member Function Documentation
** template<typename _RandomNumberEngine , size_t __w, typename
_UIntType > const _RandomNumberEngine & *std::independent_bits_engine*<
_RandomNumberEngine, __w, _UIntType >::base () const= [inline]=,
= [noexcept]=
Gets a const reference to the underlying generator engine object.

Definition at line *1200* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __w, typename
_UIntType > void *std::independent_bits_engine*< _RandomNumberEngine,
__w, _UIntType >::discard (unsigned long long __z)= [inline]=
Discard a sequence of random numbers.

Definition at line *1221* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __w, typename
_UIntType > static constexpr *result_type*
*std::independent_bits_engine*< _RandomNumberEngine, __w, _UIntType
>::max ()= [inline]=, = [static]=, = [constexpr]=
Gets the maximum value in the generated random number range.

Definition at line *1214* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __w, typename
_UIntType > static constexpr *result_type*
*std::independent_bits_engine*< _RandomNumberEngine, __w, _UIntType
>::min ()= [inline]=, = [static]=, = [constexpr]=
Gets the minimum value in the generated random number range.

Definition at line *1207* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __w, typename
_UIntType > *independent_bits_engine*< _RandomNumberEngine, __w,
_UIntType >::*result_type* *std::independent_bits_engine*<
_RandomNumberEngine, __w, _UIntType >::operator()
Gets the next value in the generated random number sequence.

Definition at line *736* of file *bits/random.tcc*.

References *std::__lg()*, *std::numeric_limits< _Tp >::max()*, and
*std::numeric_limits< _Tp >::min()*.

** template<typename _RandomNumberEngine , size_t __w, typename
_UIntType > void *std::independent_bits_engine*< _RandomNumberEngine,
__w, _UIntType >::seed ()= [inline]=
Reseeds the independent_bits_engine object with the default seed for the
underlying base class generator engine.

Definition at line *1174* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __w, typename
_UIntType > template<typename _Sseq > _If_seed_seq< _Sseq >
*std::independent_bits_engine*< _RandomNumberEngine, __w, _UIntType
>::seed (_Sseq & __q)= [inline]=
Reseeds the independent_bits_engine object with the given seed sequence.

*Parameters*

#+begin_quote
  /__q/ A seed generator function.
#+end_quote

Definition at line *1192* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __w, typename
_UIntType > void *std::independent_bits_engine*< _RandomNumberEngine,
__w, _UIntType >::seed (*result_type* __s)= [inline]=
Reseeds the independent_bits_engine object with the default seed for the
underlying base class generator engine.

Definition at line *1182* of file *random.h*.

* Friends And Related Function Documentation
** template<typename _RandomNumberEngine , size_t __w, typename
_UIntType > bool operator== (const *independent_bits_engine*<
_RandomNumberEngine, __w, _UIntType > & __lhs, const
*independent_bits_engine*< _RandomNumberEngine, __w, _UIntType > &
__rhs)= [friend]=
Compares two independent_bits_engine random number generator objects of
the same type for equality.

*Parameters*

#+begin_quote
  /__lhs/ A independent_bits_engine random number generator object.\\
  /__rhs/ Another independent_bits_engine random number generator
  object.
#+end_quote

*Returns*

#+begin_quote
  true if the infinite sequences of generated values would be equal,
  false otherwise.
#+end_quote

Definition at line *1246* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __w, typename
_UIntType > template<typename _CharT , typename _Traits >
*std::basic_istream*< _CharT, _Traits > & operator>>
(*std::basic_istream*< _CharT, _Traits > & __is,
*std::independent_bits_engine*< _RandomNumberEngine, __w, _UIntType > &
__x)= [friend]=
Extracts the current state of a % subtract_with_carry_engine random
number generator engine =__x= from the input stream =__is=.

*Parameters*

#+begin_quote
  /__is/ An input stream.\\
  /__x/ A independent_bits_engine random number generator engine.
#+end_quote

*Returns*

#+begin_quote
  The input stream with the state of =__x= extracted or in an error
  state.
#+end_quote

Definition at line *1264* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
