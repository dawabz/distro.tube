#+TITLE: Manpages - zmq_ctx_get.3
#+DESCRIPTION: Linux manpage for zmq_ctx_get.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_ctx_get - get context options

* SYNOPSIS
*int zmq_ctx_get (void */*context/*, int */option_name/*);*

* DESCRIPTION
The /zmq_ctx_get()/ function shall return the option specified by the
/option_name/ argument.

The /zmq_ctx_get()/ function accepts the following option names:

** ZMQ_IO_THREADS: Get number of I/O threads
The /ZMQ_IO_THREADS/ argument returns the size of the 0MQ thread pool
for this context.

** ZMQ_MAX_SOCKETS: Get maximum number of sockets
The /ZMQ_MAX_SOCKETS/ argument returns the maximum number of sockets
allowed for this context.

** ZMQ_MAX_MSGSZ: Get maximum message size
The /ZMQ_MAX_MSGSZ/ argument returns the maximum size of a message
allowed for this context. Default value is INT_MAX.

** ZMQ_ZERO_COPY_RECV: Get message decoding strategy
The /ZMQ_ZERO_COPY_RECV/ argument return whether message decoder uses a
zero copy strategy when receiving messages. Default value is 1. NOTE: in
DRAFT state, not yet available in stable releases.

** ZMQ_SOCKET_LIMIT: Get largest configurable number of sockets
The /ZMQ_SOCKET_LIMIT/ argument returns the largest number of sockets
that *zmq_ctx_set*(3) will accept.

** ZMQ_IPV6: Set IPv6 option
The /ZMQ_IPV6/ argument returns the IPv6 option for the context.

** ZMQ_BLOCKY: Get blocky setting
The /ZMQ_BLOCKY/ argument returns 1 if the context will block on
terminate, zero if the "block forever on context termination" gambit was
disabled by setting ZMQ_BLOCKY to false on all new contexts.

** ZMQ_THREAD_SCHED_POLICY: Get scheduling policy for I/O threads
The /ZMQ_THREAD_SCHED_POLICY/ argument returns the scheduling policy for
internal context's thread pool.

** ZMQ_THREAD_NAME_PREFIX: Get name prefix for I/O threads
The /ZMQ_THREAD_NAME_PREFIX/ argument gets the numeric prefix of each
thread created for the internal context's thread pool.

** ZMQ_MSG_T_SIZE: Get the zmq_msg_t size at runtime
The /ZMQ_MSG_T_SIZE/ argument returns the size of the zmq_msg_t
structure at runtime, as defined in the include/zmq.h public header.
This is useful for example for FFI bindings that can't simply do a
sizeof().

* RETURN VALUE
The /zmq_ctx_get()/ function returns a value of 0 or greater if
successful. Otherwise it returns -1 and sets /errno/ to one of the
values defined below.

* ERRORS
*EINVAL*

#+begin_quote
  The requested option /option_name/ is unknown.
#+end_quote

*EFAULT*

#+begin_quote
  The provided /context/ is invalid.
#+end_quote

* EXAMPLE
*Setting a limit on the number of sockets*.

#+begin_quote
  #+begin_example
    void *context = zmq_ctx_new ();
    zmq_ctx_set (context, ZMQ_MAX_SOCKETS, 256);
    int max_sockets = zmq_ctx_get (context, ZMQ_MAX_SOCKETS);
    assert (max_sockets == 256);
  #+end_example
#+end_quote

*Switching off the context deadlock gambit*.

#+begin_quote
  #+begin_example
    zmq_ctx_set (ctx, ZMQ_BLOCKY, false);
  #+end_example
#+end_quote

* SEE ALSO
*zmq_ctx_set*(3) *zmq*(7)

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
