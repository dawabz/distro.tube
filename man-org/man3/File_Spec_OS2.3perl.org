#+TITLE: Manpages - File_Spec_OS2.3perl
#+DESCRIPTION: Linux manpage for File_Spec_OS2.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
File::Spec::OS2 - methods for OS/2 file specs

* SYNOPSIS
require File::Spec::OS2; # Done internally by File::Spec if needed

* DESCRIPTION
See File::Spec and File::Spec::Unix. This package overrides the
implementation of these methods, not the semantics.

Amongst the changes made for OS/2 are...

- tmpdir :: Modifies the list of places temp directory information is
  looked for. $ENV{TMPDIR} $ENV{TEMP} $ENV{TMP} /tmp /

- splitpath :: Volumes can be drive letters or UNC sharenames
  (\\server\share).

* COPYRIGHT
Copyright (c) 2004 by the Perl 5 Porters. All rights reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.
