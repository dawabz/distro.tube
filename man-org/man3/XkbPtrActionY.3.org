#+TITLE: Manpages - XkbPtrActionY.3
#+DESCRIPTION: Linux manpage for XkbPtrActionY.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbPtrActionY - Returns the high_YYY and low_YYY fields of act converted
to a signed int

* SYNOPSIS
*int XkbPtrActionY* *( XkbPtrAction */act/* );*

* ARGUMENTS
- /- act/ :: action from which to extract Y

* DESCRIPTION
Actions associated with the XkbPtrAction structure move the pointer when
keys are pressed and released.

If the MouseKeys control is not enabled, KeyPress and KeyRelease events
are treated as though the action is XkbSA_NoAction.

If the MouseKeys control is enabled, a server action of type
XkbSA_MovePtr instructs the server to generate core pointer MotionNotify
events rather than the usual KeyPress event, and the corresponding
KeyRelease event disables any mouse keys timers that were created as a
result of handling the XkbSA_MovePtr action.

The /type/ field of the XkbPtrAction structure is always XkbSA_MovePtr.

The /flags/ field is a bitwise inclusive OR of the masks shown in
Table 1.

TABLE

Each of the X and Y coordinates of the XkbPtrAction structure is
composed of two signed 16-bit values, that is, the X coordinate is
composed of /high_XXX/ and /low_XXX,/ and similarly for the Y
coordinate. Xkb provides the following macros, to convert between a
signed integer and two signed 16-bit values in XkbPtrAction structures.

* STRUCTURES
#+begin_example

      typedef struct _XkbPtrAction {
          unsigned char    type;         /* XkbSA_MovePtr */
          unsigned char    flags;        /* determines type of pointer motion */
          unsigned char    high_XXX;     /* x coordinate, high bits*/
          unsigned char    low_XXX;      /* y coordinate, low bits */
          unsigned char    high_YYY;     /* x coordinate, high bits */
          unsigned char    low_YYY;      /* y coordinate, low bits */
      } XkbPtrAction;
#+end_example
