#+TITLE: Manpages - sd_bus_add_object_manager.3
#+DESCRIPTION: Linux manpage for sd_bus_add_object_manager.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_bus_add_object_manager - Add a D-Bus object manager for a D-Bus
object sub-tree

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

*int sd_bus_add_object_manager(sd_bus **/bus/*, sd_bus_slot ***/slot/*,
const char **/path/*);*

* DESCRIPTION
*sd_bus_add_object_manager()* installs a handler for the given path that
implements the *GetManagedObjects()* method of the
*org.freedesktop.DBus.ObjectManager* interface. See
*org.freedesktop.DBus.ObjectManager*[1] for more information.

To implement the *InterfacesAdded* and *InterfacesRemoved* signals of
the *org.freedesktop.DBus.ObjectManager* interface, call
*sd_bus_emit_interfaces_added*(3) and
*sd_bus_emit_interfaces_removed*(3) whenever interfaces are added or
removed from the sub-tree, respectively.

When *sd_bus_add_object_manager()* succeeds, a slot is created
internally. If the output parameter /slot/ is *NULL*, a "floating" slot
object is created, see *sd_bus_slot_set_floating*(3). Otherwise, a
pointer to the slot object is returned. In that case, the reference to
the slot object should be dropped when the object manager is not needed
anymore, see *sd_bus_slot_unref*(3).

* RETURN VALUE
On success, *sd_bus_add_object_manager()* returns a non-negative
integer. On failure, it returns a negative errno-style error code.

** Errors
Returned errors may indicate the following problems:

*-EINVAL*

#+begin_quote
  One of the required parameters is *NULL* or /path/ is not a valid
  object path.
#+end_quote

*-ENOPKG*

#+begin_quote
  The bus cannot be resolved.
#+end_quote

*-ECHILD*

#+begin_quote
  The bus was created in a different process.
#+end_quote

*-ENOMEM*

#+begin_quote
  Memory allocation failed.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*sd-bus*(3), *busctl*(1), *sd_bus_add_object_vtable*(3),
*sd_bus_emit_interfaces_added*(3), *sd_bus_slot_unref*(3)

* NOTES
-  1. :: org.freedesktop.DBus.ObjectManager

  https://dbus.freedesktop.org/doc/dbus-specification.html#standard-interfaces-objectmanager
