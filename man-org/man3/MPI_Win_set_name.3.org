#+TITLE: Manpages - MPI_Win_set_name.3
#+DESCRIPTION: Linux manpage for MPI_Win_set_name.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Win_set_name* - Sets the name of a window.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Win_set_name(MPI_Win win, const char *win_name)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_WIN_SET_NAME(WIN, WIN_NAME, IERROR)
  	INTEGER WIN, IERROR
  	CHARACTER*(*) WIN_NAME
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Win_set_name(win, win_name, ierror)
  	TYPE(MPI_Win), INTENT(IN) :: win
  	CHARACTER(LEN=*), INTENT(IN) :: win_name
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  void MPI::Win::Set_name(const char* win_name)
#+end_example

* INPUT/OUTPUT PARAMETER
- win :: Window whose identifier is to be set (handle).

* INPUT PARAMETER
- win_name :: The character string used as the name (string).

* OUTPUT PARAMETER
- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.
