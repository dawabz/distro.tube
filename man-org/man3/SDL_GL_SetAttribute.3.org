#+TITLE: Manpages - SDL_GL_SetAttribute.3
#+DESCRIPTION: Linux manpage for SDL_GL_SetAttribute.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_GL_SetAttribute - Set a special SDL/OpenGL attribute

* SYNOPSIS
*#include "SDL.h"*

*int SDL_GL_SetAttribute*(*SDL_GLattr attr, int value*);

* DESCRIPTION
Sets the OpenGL /attribute/ *attr* to *value*. The attributes you set
don't take effect until after a call to *SDL_SetVideoMode*. You should
use *SDL_GL_GetAttribute* to check the values after a *SDL_SetVideoMode*
call.

* RETURN VALUE
Returns *0* on success, or *-1* on error.

* EXAMPLE
#+begin_example
  SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 5 );
  SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 5 );
  SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 5 );
  SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
  SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
  if ( (screen=SDL_SetVideoMode( 640, 480, 16, SDL_OPENGL )) == NULL ) {
    fprintf(stderr, "Couldn't set GL mode: %s
  ", SDL_GetError());
    SDL_Quit();
    return;
  }
#+end_example

#+begin_quote
  *Note: *

  The *SDL_DOUBLEBUF flag is not required to enable double buffering
  when setting an OpenGL video mode. Double buffering is enabled or
  disabled using the SDL_GL_DOUBLEBUFFER attribute.*
#+end_quote

* SEE ALSO
*SDL_GL_GetAttribute*, /GL Attributes/
