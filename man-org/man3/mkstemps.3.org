#+TITLE: Manpages - mkstemps.3
#+DESCRIPTION: Linux manpage for mkstemps.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about mkstemps.3 is found in manpage for: [[../man3/mkstemp.3][man3/mkstemp.3]]