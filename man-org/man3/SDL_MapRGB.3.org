#+TITLE: Manpages - SDL_MapRGB.3
#+DESCRIPTION: Linux manpage for SDL_MapRGB.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_MapRGB - Map a RGB color value to a pixel format.

* SYNOPSIS
*#include "SDL.h"*

*Uint32 SDL_MapRGB*(*SDL_PixelFormat *fmt, Uint8 r, Uint8 g, Uint8 b*);

* DESCRIPTION
Maps the RGB color value to the specified pixel format and returns the
pixel value as a 32-bit int.

If the format has a palette (8-bit) the index of the closest matching
color in the palette will be returned.

If the specified pixel format has an alpha component it will be returned
as all 1 bits (fully opaque).

* RETURN VALUE
A pixel value best approximating the given RGB color value for a given
pixel format. If the pixel format bpp (color depth) is less than 32-bpp
then the unused upper bits of the return value can safely be ignored
(e.g., with a 16-bpp format the return value can be assigned to a
*Uint16*, and similarly a *Uint8* for an 8-bpp format).

* SEE ALSO
*SDL_GetRGB*, *SDL_GetRGBA*, *SDL_MapRGBA*, *SDL_PixelFormat*
