#+TITLE: Manpages - lroundl.3
#+DESCRIPTION: Linux manpage for lroundl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about lroundl.3 is found in manpage for: [[../man3/lround.3][man3/lround.3]]