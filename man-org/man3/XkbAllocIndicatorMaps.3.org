#+TITLE: Manpages - XkbAllocIndicatorMaps.3
#+DESCRIPTION: Linux manpage for XkbAllocIndicatorMaps.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbAllocIndicatorMaps - Allocates, directly, the /indicators/ member of
the keyboard description record

* SYNOPSIS
*Status XkbAllocIndicatorMaps* *( variable_type **/xkb/* );*

* ARGUMENTS
- /- xkb/ :: keyboard description structure

* DESCRIPTION
The /xkb/ parameter must point to a valid keyboard description. If it
doesn't, /XkbAllocIndicatorMaps/ returns a BadMatch error. Otherwise,
/XkbAllocIndicatorMaps/ allocates and initializes the /indicators/
member of the keyboard description record and returns Success. If
/XkbAllocIndicatorMaps/ was unable to allocate the indicators record, it
reports a BadAlloc error.

* RETURN VALUES"
- Success :: The /XkbAllocIndicatorMaps/ function returns Success if it
  is successful in allocating and initializing the /indicators/ member
  of the keyboard description record.

* DIAGNOSTICS
- *BadMatch* :: A compatible version of Xkb was not available in the
  server or an argument has correct type and range, but is otherwise
  invalid

- *BadAlloc* :: Unable to allocate storage
