#+TITLE: Manpages - Unicode_Collate_CJK_Zhuyin.3perl
#+DESCRIPTION: Linux manpage for Unicode_Collate_CJK_Zhuyin.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Unicode::Collate::CJK::Zhuyin - weighting CJK Unified Ideographs for
Unicode::Collate

* SYNOPSIS
use Unicode::Collate; use Unicode::Collate::CJK::Zhuyin; my $collator =
Unicode::Collate->new( overrideCJK =>
\&Unicode::Collate::CJK::Zhuyin::weightZhuyin );

* DESCRIPTION
=Unicode::Collate::CJK::Zhuyin= provides =weightZhuyin()=, that is
adequate for =overrideCJK= of =Unicode::Collate= and makes tailoring of
CJK Unified Ideographs in the order of CLDR's zhuyin (bopomofo)
ordering.

* CAVEAT
The zhuyin ordering includes some characters that are not CJK Unified
Ideographs and can't utilize =weightZhuyin()= for collation. For them,
use =entry= instead.

* SEE ALSO
- CLDR - Unicode Common Locale Data
  Repository :: <http://cldr.unicode.org/>

- Unicode Locale Data Markup Language (LDML) - UTS
  #35 :: <http://www.unicode.org/reports/tr35/>

- Unicode::Collate :: 

- Unicode::Collate::Locale :: 
