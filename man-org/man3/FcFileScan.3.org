#+TITLE: Manpages - FcFileScan.3
#+DESCRIPTION: Linux manpage for FcFileScan.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcFileScan - scan a font file

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcFileScan (FcFontSet */set/*, FcStrSet **/dirs/*, FcFileCache
**/cache/*, FcBlanks **/blanks/*, const FcChar8 **/file/*, FcBool
*/force/*);*

* DESCRIPTION
Scans a single file and adds all fonts found to /set/. If /force/ is
FcTrue, then the file is scanned even if associated information is found
in /cache/. If /file/ is a directory, it is added to /dirs/. Whether
fonts are found depends on fontconfig policy as well as the current
configuration. Internally, fontconfig will ignore BDF and PCF fonts
which are not in Unicode (or the effectively equivalent ISO Latin-1)
encoding as those are not usable by Unicode-based applications. The
configuration can ignore fonts based on filename or contents of the font
file itself. Returns FcFalse if any of the fonts cannot be added (due to
allocation failure). Otherwise returns FcTrue.
