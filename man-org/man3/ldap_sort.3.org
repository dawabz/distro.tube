#+TITLE: Manpages - ldap_sort.3
#+DESCRIPTION: Linux manpage for ldap_sort.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldap_sort_entries, ldap_sort_values, ldap_sort_strcasecmp - LDAP sorting
routines (deprecated)

* LIBRARY
OpenLDAP LDAP (libldap, -lldap)

* DESCRIPTION
The *ldap_sort_entries*(), *ldap_sort_values*(), and
*ldap_sort_strcasecmp*() are deprecated.

Deprecated interfaces generally remain in the library. The macro
LDAP_DEPRECATED can be defined to a non-zero value (e.g.,
-DLDAP_DEPRECATED=1) when compiling program designed to use deprecated
interfaces. It is recommended that developers writing new programs, or
updating old programs, avoid use of deprecated interfaces. Over time, it
is expected that documentation (and, eventually, support) for deprecated
interfaces to be eliminated.

* SEE ALSO
*ldap*(3)

* ACKNOWLEDGEMENTS
*OpenLDAP Software* is developed and maintained by The OpenLDAP Project
<http://www.openldap.org/>. *OpenLDAP Software* is derived from the
University of Michigan LDAP 3.3 Release.
