#+TITLE: Manpages - cap_init.3
#+DESCRIPTION: Linux manpage for cap_init.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cap_init, cap_free, cap_dup - capability data object storage management

* SYNOPSIS
#+begin_example
  #include <sys/capability.h>

  cap_t cap_init(void);
  int cap_free(void *obj_d);
  cap_t cap_dup(cap_t cap_p);
#+end_example

Link with /-lcap/.

* DESCRIPTION
The capabilities associated with a file or process are never edited
directly. Instead, working storage is allocated to contain a
representation of the capability state. Capabilities are edited and
manipulated only within this working storage area. Once editing of the
capability state is complete, the updated capability state is used to
replace the capability state associated with the file or process.

*cap_init*() creates a capability state in working storage and returns a
pointer to the capability state. The initial value of all flags are
cleared. The caller should free any releasable memory, when the
capability state in working storage is no longer required, by calling
*cap_free*() with the /cap_t/ as an argument.

*cap_free*() liberates any releasable memory that has been allocated to
the capability state identified by /obj_d/. The /obj_d/ argument may
identify either a /cap_t/ entity, or a /char */ entity allocated by the
*cap_to_text*() function.

*cap_dup*() returns a duplicate capability state in working storage
given by the source object /cap_p/, allocating any memory necessary, and
returning a pointer to the newly created capability state. Once
duplicated, no operation on either capability state affects the other in
any way. When the duplicated capability state in working storage is no
longer required, the caller should free any releasable memory by calling
*cap_free*() with the /cap_t/ as an argument.

* RETURN VALUE
*cap_init*() and *cap_dup*() return a non-NULL value on success, and
NULL on failure.

*cap_free*() returns zero on success, and -1 on failure.

On failure, /errno/ is set to *EINVAL* or *ENOMEM*.

* CONFORMING TO
These functions are specified in the withdrawn POSIX.1e draft
specification.

* SEE ALSO
*libcap*(3), *cap_clear*(3), *cap_copy_ext*(3), *cap_from_text*(3),
*cap_get_file*(3), *cap_get_proc*(3), *capabilities*(7)
