#+TITLE: Manpages - punycode_decode.3
#+DESCRIPTION: Linux manpage for punycode_decode.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
punycode_decode - API function

* SYNOPSIS
*#include <punycode.h>*

*int punycode_decode(size_t */input_length/*, const char [] */input/*,
size_t * */output_length/*, punycode_uint [] */output/*, unsigned char
[] */case_flags/*);*

* ARGUMENTS
- size_t input_length :: The number of ASCII code points in the /input/
  array.

- const char [] input :: An array of ASCII code points (0..7F).

- size_t * output_length :: The caller passes in the maximum number of
  code points that it can receive into the /output/ array (which is also
  the maximum number of flags that it can receive into the /case_flags/
  array, if /case_flags/ is not a *NULL* pointer). On successful return
  it will contain the number of code points actually output (which is
  also the number of flags actually output, if case_flags is not a null
  pointer). The decoder will never need to output more code points than
  the number of ASCII code points in the input, because of the way the
  encoding is defined. The number of code points output cannot exceed
  the maximum possible value of a punycode_uint, even if the supplied
  /output_length/ is greater than that.

- punycode_uint [] output :: An array of code points like the input
  argument of *punycode_encode()* (see above).

- unsigned char [] case_flags :: A *NULL* pointer (if the flags are not
  needed by the caller) or an array of boolean values parallel to the
  /output/ array. Nonzero (true, flagged) suggests that the
  corresponding Unicode character be forced to uppercase by the caller
  (if possible), and zero (false, unflagged) suggests that it be forced
  to lowercase (if possible). ASCII code points (0..7F) are output
  already in the proper case, but their flags will be set appropriately
  so that applying the flags would be harmless.

* DESCRIPTION
Converts Punycode to a sequence of code points (presumed to be Unicode
code points).

Return value: The return value can be any of the *Punycode_status*
values defined above. If not *PUNYCODE_SUCCESS*, then /output_length/ ,
/output/ , and /case_flags/ might contain garbage.

* REPORTING BUGS
Report bugs to <help-libidn@gnu.org>.\\
General guidelines for reporting bugs: http://www.gnu.org/gethelp/\\
GNU Libidn home page: http://www.gnu.org/software/libidn/

* COPYRIGHT
Copyright © 2002-2021 Simon Josefsson.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libidn* is maintained as a Texinfo manual.
If the *info* and *libidn* programs are properly installed at your site,
the command

#+begin_quote
  *info libidn*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *http://www.gnu.org/software/libidn/manual/*
#+end_quote
