#+TITLE: Manpages - SDL_GLattr.3
#+DESCRIPTION: Linux manpage for SDL_GLattr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_GLattr - SDL GL Attributes

* ATTRIBUTES
- *SDL_GL_RED_SIZE* :: Size of the framebuffer red component, in bits

- *SDL_GL_GREEN_SIZE* :: Size of the framebuffer green component, in
  bits

- *SDL_GL_BLUE_SIZE* :: Size of the framebuffer blue component, in bits

- *SDL_GL_ALPHA_SIZE* :: Size of the framebuffer alpha component, in
  bits

- *SDL_GL_DOUBLEBUFFER* :: 0 or 1, enable or disable double buffering

- *SDL_GL_BUFFER_SIZE* :: Size of the framebuffer, in bits

- *SDL_GL_DEPTH_SIZE* :: Size of the depth buffer, in bits

- *SDL_GL_STENCIL_SIZE* :: Size of the stencil buffer, in bits

- *SDL_GL_ACCUM_RED_SIZE* :: Size of the accumulation buffer red
  component, in bits

- *SDL_GL_ACCUM_GREEN_SIZE* :: Size of the accumulation buffer green
  component, in bits

- *SDL_GL_ACCUM_BLUE_SIZE* :: Size of the accumulation buffer blue
  component, in bits

- *SDL_GL_ACCUM_ALPHA_SIZE* :: Size of the accumulation buffer alpha
  component, in bits

* DESCRIPTION
While you can set most OpenGL attributes normally, the attributes list
above must be known /before/ SDL sets the video mode. These attributes a
set and read with *SDL_GL_SetAttribute* and *SDL_GL_GetAttribute*.

* SEE ALSO
*SDL_GL_SetAttribute*, *SDL_GL_GetAttribute*
