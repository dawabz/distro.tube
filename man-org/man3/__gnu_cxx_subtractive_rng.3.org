#+TITLE: Manpages - __gnu_cxx_subtractive_rng.3
#+DESCRIPTION: Linux manpage for __gnu_cxx_subtractive_rng.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::subtractive_rng

* SYNOPSIS
\\

Inherits *std::unary_function< unsigned int, unsigned int >*.

** Public Types
typedef unsigned int *argument_type*\\
=argument_type= is the type of the argument

typedef unsigned int *result_type*\\
=result_type= is the return type

** Public Member Functions
*subtractive_rng* ()\\
Default ctor; initializes its state with some number you don't see.

*subtractive_rng* (unsigned int __seed)\\
Ctor allowing you to initialize the seed.

void *_M_initialize* (unsigned int __seed)\\

unsigned int *operator()* (unsigned int __limit)\\
Returns a number less than the argument.

* Detailed Description
The =subtractive_rng= class is documented on =SGI's site=. Note that
this code assumes that =int= is 32 bits.

Definition at line *344* of file *ext/functional*.

* Member Typedef Documentation
** typedef unsigned int *std::unary_function*< unsigned int , unsigned
int >::*argument_type*= [inherited]=
=argument_type= is the type of the argument

Definition at line *108* of file *stl_function.h*.

** typedef unsigned int *std::unary_function*< unsigned int , unsigned
int >::*result_type*= [inherited]=
=result_type= is the return type

Definition at line *111* of file *stl_function.h*.

* Constructor & Destructor Documentation
** __gnu_cxx::subtractive_rng::subtractive_rng (unsigned int
__seed)= [inline]=
Ctor allowing you to initialize the seed.

Definition at line *386* of file *ext/functional*.

** __gnu_cxx::subtractive_rng::subtractive_rng ()= [inline]=
Default ctor; initializes its state with some number you don't see.

Definition at line *390* of file *ext/functional*.

* Member Function Documentation
** void __gnu_cxx::subtractive_rng::_M_initialize (unsigned int
__seed)= [inline]=
Definition at line *364* of file *ext/functional*.

** unsigned int __gnu_cxx::subtractive_rng::operator() (unsigned int
__limit)= [inline]=
Returns a number less than the argument.

Definition at line *355* of file *ext/functional*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
