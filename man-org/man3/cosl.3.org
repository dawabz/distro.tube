#+TITLE: Manpages - cosl.3
#+DESCRIPTION: Linux manpage for cosl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about cosl.3 is found in manpage for: [[../man3/cos.3][man3/cos.3]]