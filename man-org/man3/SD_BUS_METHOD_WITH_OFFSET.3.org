#+TITLE: Manpages - SD_BUS_METHOD_WITH_OFFSET.3
#+DESCRIPTION: Linux manpage for SD_BUS_METHOD_WITH_OFFSET.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about SD_BUS_METHOD_WITH_OFFSET.3 is found in manpage for: [[../sd_bus_add_object.3][sd_bus_add_object.3]]