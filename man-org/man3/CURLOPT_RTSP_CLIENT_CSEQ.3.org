#+TITLE: Manpages - CURLOPT_RTSP_CLIENT_CSEQ.3
#+DESCRIPTION: Linux manpage for CURLOPT_RTSP_CLIENT_CSEQ.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_RTSP_CLIENT_CSEQ - RTSP client CSEQ number

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_RTSP_CLIENT_CSEQ, long
cseq);

* DESCRIPTION
Pass a long to set the CSEQ number to issue for the next RTSP request.
Useful if the application is resuming a previously broken connection.
The CSEQ will increment from this new number henceforth.

* DEFAULT
0

* PROTOCOLS
RTSP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "rtsp://example.com/");
    curl_easy_setopt(curl, CURLOPT_RTSP_CLIENT_CSEQ, 1234L);
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.20.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_RTSP_SERVER_CSEQ*(3), *CURLOPT_RTSP_REQUEST*(3),
