#+TITLE: Manpages - sd_bus_message_close_container.3
#+DESCRIPTION: Linux manpage for sd_bus_message_close_container.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sd_bus_message_close_container.3 is found in manpage for: [[../sd_bus_message_open_container.3][sd_bus_message_open_container.3]]