#+TITLE: Manpages - CURLMOPT_MAX_CONCURRENT_STREAMS.3
#+DESCRIPTION: Linux manpage for CURLMOPT_MAX_CONCURRENT_STREAMS.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLMOPT_MAX_CONCURRENT_STREAMS - max concurrent streams for http2

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>

  CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_MAX_CONCURRENT_STREAMS,
                              long max);
#+end_example

* DESCRIPTION
Pass a long indicating the *max*. The set number will be used as the
maximum number of concurrent streams for a connections that libcurl
should support on connections done using HTTP/2.

Valid values range from 1 to 2147483647 (2^31 - 1) and defaults to 100.
The value passed here would be honoured based on other system resources
properties.

* DEFAULT
100

* PROTOCOLS
All

* EXAMPLE
#+begin_example
    CURLM *m = curl_multi_init();
    /* max concurrent streams 200 */
    curl_multi_setopt(m, CURLMOPT_MAX_CONCURRENT_STREAMS, 200L);
#+end_example

* AVAILABILITY
Added in 7.67.0

* RETURN VALUE
Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_MAXCONNECTS*(3), *CURLMOPT_MAXCONNECTS*(3),
