#+TITLE: Manpages - SDL_AudioCVT.3
#+DESCRIPTION: Linux manpage for SDL_AudioCVT.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_AudioCVT - Audio Conversion Structure

* STRUCTURE DEFINITION
#+begin_example
  typedef struct{
    int needed;
    Uint16 src_format;
    Uint16 dest_format;
    double rate_incr;
    Uint8 *buf;
    int len;
    int len_cvt;
    int len_mult;
    double len_ratio;
    void (*filters[10])(struct SDL_AudioCVT *cvt, Uint16 format);
    int filter_index;
  } SDL_AudioCVT;
#+end_example

* STRUCTURE DATA
- *needed* :: Set to one if the conversion is possible

- *src_format* :: Audio format of the source

- *dest_format* :: Audio format of the destination

- *rate_incr* :: Rate conversion increment

- *buf* :: Audio buffer

- *len* :: Length of the original audio buffer in bytes

- *len_cvt* :: Length of converted audio buffer in bytes (calculated)

- *len_mult* :: *buf* must be *len***len_mult* bytes in size(calculated)

- *len_ratio* :: Final audio size is *len***len_ratio*

- *filters[10](..)* :: Pointers to functions needed for this conversion

- *filter_index* :: Current conversion function

* DESCRIPTION
The *SDL_AudioCVT* is used to convert audio data between different
formats. A *SDL_AudioCVT* structure is created with the
*SDL_BuildAudioCVT* function, while the actual conversion is done by the
*SDL_ConvertAudio* function.

Many of the fields in the *SDL_AudioCVT* structure should be considered
private and their function will not be discussed here.

- Uint8 *buf :: 

- int len :: 

- int len_mult :: 

- double len_ratio :: 

* SEE ALSO
*SDL_BuildAudioCVT*, *SDL_ConvertAudio*, *SDL_AudioSpec*
