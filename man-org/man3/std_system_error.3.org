#+TITLE: Manpages - std_system_error.3
#+DESCRIPTION: Linux manpage for std_system_error.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::system_error - An exception type that includes an =error_code=
value.

* SYNOPSIS
\\

Inherits *std::runtime_error*.

Inherited by *std::experimental::filesystem::v1::filesystem_error*, and
*std::filesystem::filesystem_error*.

** Public Member Functions
*system_error* (const *system_error* &)=default\\

*system_error* (*error_code* __ec, const char *__what)\\

*system_error* (*error_code* __ec, const *string* &__what)\\

*system_error* (*error_code* __ec=*error_code*())\\

*system_error* (int __v, const *error_category* &__ecat)\\

*system_error* (int __v, const *error_category* &__ecat, const char
*__what)\\

*system_error* (int __v, const *error_category* &__ecat, const *string*
&__what)\\

const *error_code* & *code* () const noexcept\\

*system_error* & *operator=* (const *system_error* &)=default\\

virtual const char * *what* () const noexcept\\

* Detailed Description
An exception type that includes an =error_code= value.

Typically used to report errors from the operating system and other
low-level APIs.

Definition at line *430* of file *std/system_error*.

* Member Function Documentation
** virtual const char * std::runtime_error::what () const= [virtual]=,
= [noexcept]=, = [inherited]=
Returns a C-style character string describing the general cause of the
current error (the same string passed to the ctor).\\

Reimplemented from *std::exception*.

Reimplemented in *std::filesystem::filesystem_error*, and
*std::experimental::filesystem::v1::filesystem_error*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
