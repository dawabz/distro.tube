#+TITLE: Manpages - FcCharSetDelChar.3
#+DESCRIPTION: Linux manpage for FcCharSetDelChar.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcCharSetDelChar - Add a character to a charset

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcCharSetDelChar (FcCharSet */fcs/*, FcChar32 */ucs4/*);*

* DESCRIPTION
*FcCharSetDelChar* deletes a single Unicode char from the set, returning
FcFalse on failure, either as a result of a constant set or from running
out of memory.

* SINCE
version 2.9.0
