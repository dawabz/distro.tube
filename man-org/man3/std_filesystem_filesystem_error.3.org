#+TITLE: Manpages - std_filesystem_filesystem_error.3
#+DESCRIPTION: Linux manpage for std_filesystem_filesystem_error.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::filesystem::filesystem_error - Exception type thrown by the
Filesystem library.

* SYNOPSIS
\\

=#include <fs_path.h>=

Inherits *std::system_error*.

** Public Member Functions
*filesystem_error* (const *filesystem_error* &)=default\\

*filesystem_error* (const *string* &__what_arg, const *path* &__p1,
const *path* &__p2, *error_code* __ec)\\

*filesystem_error* (const *string* &__what_arg, const *path* &__p1,
*error_code* __ec)\\

*filesystem_error* (const *string* &__what_arg, *error_code* __ec)\\

const *error_code* & *code* () const noexcept\\

*filesystem_error* & *operator=* (const *filesystem_error* &)=default\\

const *path* & *path1* () const noexcept\\

const *path* & *path2* () const noexcept\\

const char * *what* () const noexcept\\

* Detailed Description
Exception type thrown by the Filesystem library.

Definition at line *700* of file *bits/fs_path.h*.

* Member Function Documentation
** const char * std::filesystem::filesystem_error::what ()
const= [virtual]=, = [noexcept]=
Returns a C-style character string describing the general cause of the
current error (the same string passed to the ctor).\\

Reimplemented from *std::runtime_error*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
