#+TITLE: Manpages - std_thread.3
#+DESCRIPTION: Linux manpage for std_thread.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::thread - thread

* SYNOPSIS
\\

=#include <std_thread.h>=

** Classes
class *id*\\
thread::id

** Public Types
template<typename... _Tp> using *_Call_wrapper* = _Invoker< *tuple*<
typename *decay*< _Tp >::type... > >\\

using *_State_ptr* = *unique_ptr*< _State >\\

using *native_handle_type* = __gthread_t\\

** Public Member Functions
template<typename _Callable , typename... _Args, typename =
_Require<__not_same<_Callable>>> *thread* (_Callable &&__f, _Args &&...
__args)\\

*thread* (const *thread* &)=delete\\

*thread* (*thread* &&__t) noexcept\\

void *detach* ()\\

*id* *get_id* () const noexcept\\

void *join* ()\\

bool *joinable* () const noexcept\\

native_handle_type *native_handle* ()\\

*thread* & *operator=* (const *thread* &)=delete\\

*thread* & *operator=* (*thread* &&__t) noexcept\\

void *swap* (*thread* &__t) noexcept\\

** Static Public Member Functions
static unsigned int *hardware_concurrency* () noexcept\\

* Detailed Description
thread

Definition at line *62* of file *std_thread.h*.

* Member Typedef Documentation
** template<typename... _Tp> using std::thread::_Call_wrapper =
_Invoker<*tuple*<typename *decay*<_Tp>::type...> >
Definition at line *266* of file *std_thread.h*.

** using *std::thread::_State_ptr* = *unique_ptr*<_State>
Definition at line *73* of file *std_thread.h*.

** using std::thread::native_handle_type = __gthread_t
Definition at line *75* of file *std_thread.h*.

* Constructor & Destructor Documentation
** template<typename _Callable , typename... _Args, typename =
_Require<__not_same<_Callable>>> std::thread::thread (_Callable && __f,
_Args &&... __args)= [inline]=, = [explicit]=
Definition at line *127* of file *std_thread.h*.

** std::thread::~thread ()= [inline]=
Definition at line *149* of file *std_thread.h*.

** std::thread::thread (*thread* && __t)= [inline]=, = [noexcept]=
Definition at line *157* of file *std_thread.h*.

* Member Function Documentation
** *id* std::thread::get_id () const= [inline]=, = [noexcept]=
Definition at line *185* of file *std_thread.h*.

** bool std::thread::joinable () const= [inline]=, = [noexcept]=
Definition at line *175* of file *std_thread.h*.

** native_handle_type std::thread::native_handle ()= [inline]=
*Precondition*

#+begin_quote
  thread is joinable
#+end_quote

Definition at line *191* of file *std_thread.h*.

** *thread* & std::thread::operator= (*thread* && __t)= [inline]=,
= [noexcept]=
Definition at line *162* of file *std_thread.h*.

** void std::thread::swap (*thread* & __t)= [inline]=, = [noexcept]=
Definition at line *171* of file *std_thread.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
