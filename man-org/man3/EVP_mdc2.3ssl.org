#+TITLE: Manpages - EVP_mdc2.3ssl
#+DESCRIPTION: Linux manpage for EVP_mdc2.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
EVP_mdc2 - MDC-2 For EVP

* SYNOPSIS
#include <openssl/evp.h> const EVP_MD *EVP_mdc2(void);

* DESCRIPTION
MDC-2 (Modification Detection Code 2 or Meyer-Schilling) is a
cryptographic hash function based on a block cipher.

- EVP_mdc2() :: The MDC-2DES algorithm of using MDC-2 with the DES block
  cipher. It produces a 128-bit output from a given input.

* RETURN VALUES
These functions return a *EVP_MD* structure that contains the
implementation of the symmetric cipher. See *EVP_MD_meth_new* (3) for
details of the *EVP_MD* structure.

* CONFORMING TO
ISO/IEC 10118-2:2000 Hash-Function 2, with DES as the underlying block
cipher.

* SEE ALSO
*evp* (7), *EVP_DigestInit* (3)

* COPYRIGHT
Copyright 2017 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
