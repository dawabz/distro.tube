#+TITLE: Manpages - std_pointer_traits.3
#+DESCRIPTION: Linux manpage for std_pointer_traits.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::pointer_traits< _Ptr > - Uniform interface to all pointer-like
types.

* SYNOPSIS
\\

=#include <ptr_traits.h>=

** Public Types
using *difference_type* = __detected_or_t< ptrdiff_t, __difference_type,
_Ptr >\\
The type used to represent the difference between two pointers.

using *element_type* = __detected_or_t< __get_first_arg_t< _Ptr >,
__element_type, _Ptr >\\
The type pointed to.

using *pointer* = _Ptr\\
The pointer type.

template<typename _Up > using *rebind* = typename __rebind< _Ptr, _Up
>::type\\
A pointer to a different type.

** Static Public Member Functions
static _Ptr *pointer_to* (__make_not_void< *element_type* > &__e)\\

* Detailed Description
** "template<typename _Ptr>
\\
struct std::pointer_traits< _Ptr >"Uniform interface to all pointer-like
types.

Definition at line *83* of file *ptr_traits.h*.

* Member Typedef Documentation
** template<typename _Ptr > using *std::pointer_traits*< _Ptr
>::*difference_type* = __detected_or_t<ptrdiff_t, __difference_type,
_Ptr>
The type used to represent the difference between two pointers.

Definition at line *108* of file *ptr_traits.h*.

** template<typename _Ptr > using *std::pointer_traits*< _Ptr
>::*element_type* = __detected_or_t<__get_first_arg_t<_Ptr>,
__element_type, _Ptr>
The type pointed to.

Definition at line *104* of file *ptr_traits.h*.

** template<typename _Ptr > using *std::pointer_traits*< _Ptr
>::*pointer* = _Ptr
The pointer type.

Definition at line *101* of file *ptr_traits.h*.

** template<typename _Ptr > template<typename _Up > using
*std::pointer_traits*< _Ptr >::*rebind* = typename __rebind<_Ptr,
_Up>::type
A pointer to a different type.

Definition at line *113* of file *ptr_traits.h*.

* Member Function Documentation
** template<typename _Ptr > static _Ptr *std::pointer_traits*< _Ptr
>::pointer_to (__make_not_void< *element_type* > & __e)= [inline]=,
= [static]=
Definition at line *116* of file *ptr_traits.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
