#+TITLE: Manpages - timerisset.3
#+DESCRIPTION: Linux manpage for timerisset.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about timerisset.3 is found in manpage for: [[../man3/timeradd.3][man3/timeradd.3]]