#+TITLE: Manpages - pam_acct_mgmt.3
#+DESCRIPTION: Linux manpage for pam_acct_mgmt.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_acct_mgmt - PAM account validation management

* SYNOPSIS
#+begin_example
  #include <security/pam_appl.h>
#+end_example

*int pam_acct_mgmt(pam_handle_t **/pamh/*, int */flags/*);*

* DESCRIPTION
The *pam_acct_mgmt* function is used to determine if the users account
is valid. It checks for authentication token and account expiration and
verifies access restrictions. It is typically called after the user has
been authenticated.

The /pamh/ argument is an authentication handle obtained by a prior call
to pam_start(). The flags argument is the binary or of zero or more of
the following values:

PAM_SILENT

#+begin_quote
  Do not emit any messages.
#+end_quote

PAM_DISALLOW_NULL_AUTHTOK

#+begin_quote
  The PAM module service should return PAM_NEW_AUTHTOK_REQD if the user
  has a null authentication token.
#+end_quote

* RETURN VALUES
PAM_ACCT_EXPIRED

#+begin_quote
  User account has expired.
#+end_quote

PAM_AUTH_ERR

#+begin_quote
  Authentication failure.
#+end_quote

PAM_NEW_AUTHTOK_REQD

#+begin_quote
  The user account is valid but their authentication token is /expired/.
  The correct response to this return-value is to require that the user
  satisfies the *pam_chauthtok()* function before obtaining service. It
  may not be possible for some applications to do this. In such cases,
  the user should be denied access until such time as they can update
  their password.
#+end_quote

PAM_PERM_DENIED

#+begin_quote
  Permission denied.
#+end_quote

PAM_SUCCESS

#+begin_quote
  The authentication token was successfully updated.
#+end_quote

PAM_USER_UNKNOWN

#+begin_quote
  User unknown to password service.
#+end_quote

* SEE ALSO
*pam_start*(3), *pam_authenticate*(3), *pam_chauthtok*(3),
*pam_strerror*(3), *pam*(8)
