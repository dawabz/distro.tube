#+TITLE: Manpages - XFreeDeviceList.3
#+DESCRIPTION: Linux manpage for XFreeDeviceList.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XFreeDeviceList.3 is found in manpage for: [[../man3/XListInputDevices.3][man3/XListInputDevices.3]]