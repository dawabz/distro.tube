#+TITLE: Manpages - DSA_generate_key.3ssl
#+DESCRIPTION: Linux manpage for DSA_generate_key.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
DSA_generate_key - generate DSA key pair

* SYNOPSIS
#include <openssl/dsa.h> int DSA_generate_key(DSA *a);

* DESCRIPTION
*DSA_generate_key()* expects *a* to contain DSA parameters. It generates
a new key pair and stores it in *a->pub_key* and *a->priv_key*.

The random generator must be seeded prior to calling
*DSA_generate_key()*. If the automatic seeding or reseeding of the
OpenSSL CSPRNG fails due to external circumstances (see *RAND* (7)), the
operation will fail.

* RETURN VALUES
*DSA_generate_key()* returns 1 on success, 0 otherwise. The error codes
can be obtained by *ERR_get_error* (3).

* SEE ALSO
*DSA_new* (3), *ERR_get_error* (3), *RAND_bytes* (3),
*DSA_generate_parameters_ex* (3)

* COPYRIGHT
Copyright 2000-2019 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
