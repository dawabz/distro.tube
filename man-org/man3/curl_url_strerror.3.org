#+TITLE: Manpages - curl_url_strerror.3
#+DESCRIPTION: Linux manpage for curl_url_strerror.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_url_strerror - return string describing error code

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>
  const char *curl_url_strerror(CURLUcode errornum);
#+end_example

* DESCRIPTION
The curl_url_strerror() function returns a string describing the
CURLUcode error code passed in the argument /errornum/.

* EXAMPLE
#+begin_example
    CURLUcode rc;
    CURLU *url = curl_url();
    rc = curl_url_set(url, CURLUPART_URL, "https://example.com", 0);
    if(rc)
      printf("URL error: %s\n", curl_url_strerror(rc));
    curl_url_cleanup(url);
#+end_example

* AVAILABILITY
Added in 7.80.0

* RETURN VALUE
A pointer to a null-terminated string.

* SEE ALSO
*libcurl-errors*(3), *curl_url_get*(3), *curl_url_set*(3),
*curl_easy_strerror*(3), *curl_multi_strerror*(3),
*curl_share_strerror*(3)
