#+TITLE: Manpages - zip_source_begin_write.3
#+DESCRIPTION: Linux manpage for zip_source_begin_write.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The functions

and

prepare

for writing. Usually this involves creating temporary files or
allocating buffers.

preserves the first

bytes of the original file. This is done efficiently, and writes to

won't overwrite the original data until

is called.

Upon successful completion 0 is returned. Otherwise, -1 is returned and
the error information in

is set to indicate the error.

was added in libzip 1.0.

was added in libzip 1.4.0.

and
