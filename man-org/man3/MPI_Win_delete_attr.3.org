#+TITLE: Manpages - MPI_Win_delete_attr.3
#+DESCRIPTION: Linux manpage for MPI_Win_delete_attr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Win_delete_attr* - Deletes an attribute from a window.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Win_delete_attr(MPI_Win win, int win_keyval)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_WIN_DELETE_ATTR(WIN, WIN_KEYVAL, IERROR)
  	INTEGER WIN, WIN_KEYVAL, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Win_delete_attr(win, win_keyval, ierror)
  	TYPE(MPI_Win), INTENT(IN) :: win
  	INTEGER, INTENT(IN) :: win_keyval
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  void MPI::Win::Delete_attr(int win_keyval)
#+end_example

* INPUT/OUTPUT PARAMETER
- win :: Window from which the attribute is deleted (handle).

* INPUT PARAMETER
- win_keyval :: Key value (integer).

* OUTPUT PARAMETER
- IERROR :: Fortran only: Error status (integer).

* NOTES
Note that it is not defined by the MPI standard what happens if the
delete_fn callback invokes other MPI functions. In Open MPI, it is not
valid for delete_fn callbacks (or any of their children) to add or
delete attributes on the same object on which the delete_fn callback is
being invoked.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.
