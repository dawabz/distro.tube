#+TITLE: Manpages - execvpe.3
#+DESCRIPTION: Linux manpage for execvpe.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about execvpe.3 is found in manpage for: [[../man3/exec.3][man3/exec.3]]