#+TITLE: Manpages - std_promise.3
#+DESCRIPTION: Linux manpage for std_promise.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::promise< _Res > - Primary template for promise.

* SYNOPSIS
\\

** Public Member Functions
template<typename _Allocator > *promise* (*allocator_arg_t*, const
_Allocator &, *promise* &&__rhs)\\

template<typename _Allocator > *promise* (*allocator_arg_t*, const
_Allocator &__a)\\

*promise* (const *promise* &)=delete\\

*promise* (*promise* &&__rhs) noexcept\\

*future*< _Res > *get_future* ()\\

*promise* & *operator=* (const *promise* &)=delete\\

*promise* & *operator=* (*promise* &&__rhs) noexcept\\

void *set_exception* (*exception_ptr* __p)\\

void *set_exception_at_thread_exit* (*exception_ptr* __p)\\

void *set_value* (_Res &&__r)\\

void *set_value* (const _Res &__r)\\

void *set_value_at_thread_exit* (_Res &&__r)\\

void *set_value_at_thread_exit* (const _Res &__r)\\

void *swap* (*promise* &__rhs) noexcept\\

** Friends
template<typename , typename > struct *_State::_Setter*\\

* Detailed Description
** "template<typename _Res>
\\
class std::promise< _Res >"Primary template for promise.

Definition at line *1059* of file *future*.

* Constructor & Destructor Documentation
** template<typename _Res > *std::promise*< _Res >::*promise*
()= [inline]=
Definition at line *1078* of file *future*.

** template<typename _Res > *std::promise*< _Res >::*promise*
(*promise*< _Res > && __rhs)= [inline]=, = [noexcept]=
Definition at line *1083* of file *future*.

** template<typename _Res > template<typename _Allocator >
*std::promise*< _Res >::*promise* (*allocator_arg_t*, const _Allocator &
__a)= [inline]=
Definition at line *1089* of file *future*.

** template<typename _Res > template<typename _Allocator >
*std::promise*< _Res >::*promise* (*allocator_arg_t*, const _Allocator
&, *promise*< _Res > && __rhs)= [inline]=
Definition at line *1095* of file *future*.

** template<typename _Res > *std::promise*< _Res >::~*promise*
()= [inline]=
Definition at line *1102* of file *future*.

* Member Function Documentation
** template<typename _Res > *future*< _Res > *std::promise*< _Res
>::get_future ()= [inline]=
Definition at line *1127* of file *future*.

** template<typename _Res > *promise* & *std::promise*< _Res
>::operator= (*promise*< _Res > && __rhs)= [inline]=, = [noexcept]=
Definition at line *1110* of file *future*.

** template<typename _Res > void *std::promise*< _Res >::set_exception
(*exception_ptr* __p)= [inline]=
Definition at line *1140* of file *future*.

** template<typename _Res > void *std::promise*< _Res
>::set_exception_at_thread_exit (*exception_ptr* __p)= [inline]=
Definition at line *1158* of file *future*.

** template<typename _Res > void *std::promise*< _Res >::set_value (_Res
&& __r)= [inline]=
Definition at line *1136* of file *future*.

** template<typename _Res > void *std::promise*< _Res >::set_value
(const _Res & __r)= [inline]=
Definition at line *1132* of file *future*.

** template<typename _Res > void *std::promise*< _Res
>::set_value_at_thread_exit (_Res && __r)= [inline]=
Definition at line *1151* of file *future*.

** template<typename _Res > void *std::promise*< _Res
>::set_value_at_thread_exit (const _Res & __r)= [inline]=
Definition at line *1144* of file *future*.

** template<typename _Res > void *std::promise*< _Res >::swap
(*promise*< _Res > & __rhs)= [inline]=, = [noexcept]=
Definition at line *1119* of file *future*.

* Friends And Related Function Documentation
** template<typename _Res > template<typename , typename > friend struct
_State::_Setter= [friend]=
Definition at line *1071* of file *future*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
