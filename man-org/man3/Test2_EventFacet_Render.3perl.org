#+TITLE: Manpages - Test2_EventFacet_Render.3perl
#+DESCRIPTION: Linux manpage for Test2_EventFacet_Render.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test2::EventFacet::Render - Facet that dictates how to render an event.

* DESCRIPTION
This facet is used to dictate how the event should be rendered by the
standard test2 rendering tools. If this facet is present then ONLY what
is specified by it will be rendered. It is assumed that anything
important or note-worthy will be present here, no other facets will be
considered for rendering/display.

This facet is a list type, you can add as many items as needed.

* FIELDS
- $string = $render->[#]->{details} :: 

- $string = $render->[#]->details() :: 

Human readable text for display.

- $string = $render->[#]->{tag} :: 

- $string = $render->[#]->tag() :: 

Tag that should prefix/identify the main text.

- $string = $render->[#]->{facet} :: 

- $string = $render->[#]->facet() :: 

Optional, if the display text was generated from another facet this
should state what facet it was.

- $mode = $render->[#]->{mode} :: 

- $mode = $render->[#]->mode() :: 

  - calculated :: 

  Calculated means the facet was generated from another facet.
  Calculated facets may be cleared and regenerated whenever the event
  state changes.

  - replace :: Replace means the facet is intended to replace the normal
    rendering of the event.

* SOURCE
The source code repository for Test2 can be found at
/http://github.com/Test-More/test-more//.

* MAINTAINERS
- Chad Granum <exodist@cpan.org> :: 

* AUTHORS
- Chad Granum <exodist@cpan.org> :: 

* COPYRIGHT
Copyright 2020 Chad Granum <exodist@cpan.org>.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See /http://dev.perl.org/licenses//
