#+TITLE: Manpages - SDL_PeepEvents.3
#+DESCRIPTION: Linux manpage for SDL_PeepEvents.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_PeepEvents - Checks the event queue for messages and optionally
returns them.

* SYNOPSIS
*#include "SDL.h"*

*int SDL_PeepEvents*(*SDL_Event *events, int numevents, SDL_eventaction
action, Uint32 mask*);

* DESCRIPTION
Checks the event queue for messages and optionally returns them.

If *action* is *SDL_ADDEVENT*, up to *numevents* events will be added to
the back of the event queue.

If *action* is *SDL_PEEKEVENT*, up to *numevents* events at the front of
the event queue, matching *mask*, will be returned and will not be
removed from the queue.

If *action* is *SDL_GETEVENT*, up to *numevents* events at the front of
the event queue, matching *mask*, will be returned and will be removed
from the queue.

This function is thread-safe.

* RETURN VALUE
This function returns the number of events actually stored, or *-1* if
there was an error.

* SEE ALSO
*SDL_Event*, *SDL_PollEvent*, *SDL_PushEvent*
