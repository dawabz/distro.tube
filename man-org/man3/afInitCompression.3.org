#+TITLE: Manpages - afInitCompression.3
#+DESCRIPTION: Linux manpage for afInitCompression.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
afInitCompression - initialize compression for a track in an audio file
setup

* SYNOPSIS
#+begin_example
  #include <audiofile.h>
#+end_example

#+begin_example
  void afInitCompression(AFfilesetup setup, int track, int compression);
#+end_example

* PARAMETERS
setup is a valid file setup returned by *afNewFileSetup*(3).

track specifies a track within the audio file setup. track is always
AF_DEFAULT_TRACK for all currently supported file formats.

compression is an identifier specifying the compression type (such as
AF_COMPRESSION_G711_ULAW) to be used for audio data in the track.

* DESCRIPTION
Given an AFfilesetup structure created with *afNewFileSetup*(3) and a
track identified by track (AF_DEFAULT_TRACK for all currently supported
file formats), afInitCompression initializes the track to the specified
compression.

The following compression types are currently supported:

AF_COMPRESSION_NONE

#+begin_quote
  no compression
#+end_quote

AF_COMPRESSION_G711_ULAW

#+begin_quote
  CCITT G.711 mu-law encoding
#+end_quote

AF_COMPRESSION_G711_ALAW

#+begin_quote
  CCITT G.711 A-law encoding
#+end_quote

AF_COMPRESSION_IMA

#+begin_quote
  IMA ADPCM encoding
#+end_quote

AF_COMPRESSION_MS_ADPCM

#+begin_quote
  MS ADPCM encoding
#+end_quote

AF_COMPRESSION_FLAC

#+begin_quote
  FLAC
#+end_quote

AF_COMPRESSION_ALAC

#+begin_quote
  Apple Lossless Audio Codec
#+end_quote

* ERRORS
afInitCompression can produce the following errors:

AF_BAD_FILESETUP

#+begin_quote
  setup represents an invalid file setup.
#+end_quote

AF_BAD_TRACKID

#+begin_quote
  track represents an invalid track identifier.
#+end_quote

AF_BAD_COMPTYPE

#+begin_quote
  compression represents an invalid compression type.
#+end_quote

* SEE ALSO
*afNewFileSetup*(3), *afInitSampleFormat*(3)

* AUTHOR
Michael Pruett <michael@68k.org>
