#+TITLE: Manpages - alienfile.3pm
#+DESCRIPTION: Linux manpage for alienfile.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
alienfile - Specification for defining an external dependency for CPAN

* VERSION
version 2.44

* SYNOPSIS
Do-it-yourself approach:

use alienfile; probe [ pkg-config --exists libarchive ]; share {
start_url http://libarchive.org/downloads/libarchive-3.2.2.tar.gz; # the
first one which succeeds will be used download [ wget %{.meta.start_url}
]; download [ curl -o %{.meta.start_url} ]; extract [ tar xf
%{.install.download} ]; build [ # Note: will not work on Windows, better
to use Build::Autoconf plugin # if you need windows support ./configure
--prefix=%{.install.prefix} --disable-shared, %{make}, %{make} install,
]; } gather [ [ pkg-config, --modversion, libarchive,
\%{.runtime.version} ], [ pkg-config, --cflags, libarchive,
\%{.runtime.cflags} ], [ pkg-config, --libs, libarchive,
\%{.runtime.libs} ], ];

With plugins (better):

use alienfile; plugin PkgConfig => libarchive; share { start_url
http://libarchive.org/downloads/; plugin Download => ( filter =>
qr/^libarchive-.*\.tar\.gz$/, version => qr/([0-9\.]+)/, ); plugin
Extract => tar.gz; plugin Build::Autoconf; plugin
Gather::IsolateDynamic; build [ %{configure}, %{make}, %{make} install,
]; };

* DESCRIPTION
An alienfile is a recipe used by Alien::Build to, probe for system
libraries or download from the internet, and build source for those
libraries. This document acts as reference for the alienfile system, but
if you are starting out writing your own Alien you should read
Alien::Build::Manual::AlienAuthor, which will teach you how to write
your own complete Alien using alienfile + Alien::Build +
ExtUtils::MakeMaker. Special attention should be taken to the section a
note about dynamic vs. static libraries.

* DIRECTIVES
** requires
any requirement (either share or system):

requires $module; requires $module => $version;

configure time requirement:

configure { requires $module; requires $module => $version; };

system requirement:

sys { requires $module; requires $module => $version; };

share requirement:

share { requires $module; requires $module => $version; };

specifies a requirement. Alien::Build takes advantage of dynamic
requirements, so only modules that are needed for the specific type of
install need to be loaded. Here are the different types of requirements:

- configure :: Configure requirements should already be installed before
  the alienfile is loaded.

- any :: Any requirements are those that are needed either for the probe
  stage, or in either the system or share installs.

- share :: Share requirements are those modules needed when downloading
  and building from source.

- system :: System requirements are those modules needed when the system
  provides the library or tool.

** plugin
plugin $name => (%args); plugin $name => $arg;

Load the given plugin. If you prefix the plugin name with an === sign,
then it will be assumed to be a fully qualified path name. Otherwise the
plugin will be assumed to live in the =Alien::Build::Plugin= namespace.
If there is an appropriate negotiate plugin, that one will be loaded.
Examples:

# Loads Alien::Build::Plugin::Fetch::Negotiate # which will pick the
best Alien::Build::Plugin::Fetch # plugin based on the URL, and system
configuration plugin Fetch => http://ftp.gnu.org/gnu/gcc; # loads the
plugin with the badly named class! plugin
=Badly::Named::Plugin::Not::In::Alien::Build::Namespace; # explicitly
loads Alien::Build::Plugin::Prefer::SortVersions plugin
Prefer::SortVersions => ( filter => qr/^gcc-.*\.tar\.gz$/, version =>
qr/([0-9\.]+)/, );

** probe
probe \&code; probe \@commandlist;

Instructions for the probe stage. May be either a code reference, or a
command list.

** configure
configure { ... };

Configure block. The only directive allowed in a configure block is
requires.

** sys
sys { ... };

System block. Allowed directives are: requires and gather.

** share
share { ... };

System block. Allowed directives are: download, fetch, decode, prefer,
extract, build, gather.

** start_url
share { start_url $url; };

Set the start URL for download. This should be the URL to an index page,
or the actual tarball of the source.

** download
share { download \&code; download \@commandlist; };

Instructions for the download stage. May be either a code reference, or
a command list.

** fetch
share { fetch \&code; fetch \@commandlist; };

Instructions for the fetch stage. May be either a code reference, or a
command list.

** decode
share { decode \&code; decode \@commandlist; };

Instructions for the decode stage. May be either a code reference, or a
command list.

** prefer
share { prefer \&code; prefer \@commandlist; };

Instructions for the prefer stage. May be either a code reference, or a
command list.

** extract
share { extract \&code; extract \@commandlist; };

Instructions for the extract stage. May be either a code reference, or a
command list.

** patch
share { patch \&code; patch \@commandlist; };

Instructions for the patch stage. May be either a code reference, or a
command list.

** patch_ffi
share { patch_ffi \&code; patch_ffi \@commandlist; };

[DEPRECATED]

Instructions for the patch_ffi stage. May be either a code reference, or
a command list.

** build
share { build \&code; build \@commandlist; };

Instructions for the build stage. May be either a code reference, or a
command list.

** build_ffi
share { build \&code; build \@commandlist; };

[DEPRECATED]

Instructions for the build FFI stage. Builds shared libraries instead of
static. This is optional, and is only necessary if a fresh and separate
build needs to be done for FFI.

** gather
gather \&code; gather \@commandlist; share { gather \&code; gather
\@commandlist; }; sys { gather \&code; gather \@commandlist; };

Instructions for the gather stage. May be either a code reference, or a
command list. In the root block of the alienfile it will trigger in both
share and system build. In the share or sys block it will only trigger
in the corresponding build.

** gather_ffi
share { gather_ffi \&code; gather_ffi \@commandlist; }

[DEPRECATED]

Gather specific to =build_ffi=. Not usually necessary.

** ffi
share { ffi { patch \&code; patch \@commandlist; build \&code; build
\@commandlist; gather \&code; gather \@commandlist; } }

Specify patch, build or gather stages related to FFI.

** meta_prop
my $hash = meta_prop;

Get the meta_prop hash reference.

** meta
my $meta = meta;

Returns the meta object for your alienfile.

** log
log($message);

Prints the given log to stdout.

** test
share { test \&code; test \@commandlist; }; sys { test \&code; test
\@commandlist; };

Run the tests

** before
before $stage => \&code;

Execute the given code before the given stage. Stage should be one of
=probe=, =download=, =fetch=, =decode=, =prefer=, =extract=, =patch=,
=build=, =test=, and =gather=.

The before directive is only legal in the same blocks as the stage would
normally be legal in. For example, you can't do this:

use alienfile; sys { before build => sub { ... }; };

Because a =build= wouldn't be legal inside a =sys= block.

** after
after $stage => \&code;

Execute the given code after the given stage. Stage should be one of
=probe=, =download=, =fetch=, =decode=, =prefer=, =extract=, =patch=,
=build=, =test=, and =gather=.

The after directive is only legal in the same blocks as the stage would
normally be legal in. For example, you can't do this:

use alienfile; sys { after build => sub { ... }; };

Because a =build= wouldn't be legal inside a =sys= block.

* SEE ALSO
- Alien :: 

- Alien::Build :: 

- Alien::Build::MM :: 

- Alien::Base :: 

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
