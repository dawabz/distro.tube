#+TITLE: Manpages - std::string.3
#+DESCRIPTION: Linux manpage for std::string.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about std::string.3 is found in manpage for: [[../man3/std_basic_string.3.org][man3/std_basic_string.3]]
