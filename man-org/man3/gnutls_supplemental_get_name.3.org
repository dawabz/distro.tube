#+TITLE: Manpages - gnutls_supplemental_get_name.3
#+DESCRIPTION: Linux manpage for gnutls_supplemental_get_name.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_supplemental_get_name - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*const char *
gnutls_supplemental_get_name(gnutls_supplemental_data_format_type_t
*/type/*);*

* ARGUMENTS
- gnutls_supplemental_data_format_type_t type :: is a supplemental data
  format type

* DESCRIPTION
Convert a *gnutls_supplemental_data_format_type_t* value to a string.

* RETURNS
a string that contains the name of the specified supplemental data
format type, or *NULL* for unknown types.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
