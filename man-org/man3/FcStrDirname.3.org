#+TITLE: Manpages - FcStrDirname.3
#+DESCRIPTION: Linux manpage for FcStrDirname.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcStrDirname - directory part of filename

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcChar8 * FcStrDirname (const FcChar8 */file/*);*

* DESCRIPTION
Returns the directory containing /file/. This is returned in newly
allocated storage which should be freed when no longer needed.
