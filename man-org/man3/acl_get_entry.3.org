#+TITLE: Manpages - acl_get_entry.3
#+DESCRIPTION: Linux manpage for acl_get_entry.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function obtains a descriptor for an ACL entry as specified by

within the ACL indicated by the argument

If the value of

is ACL_FIRST_ENTRY, then the function returns in

a descriptor for the first ACL entry within

If the value of

is ACL_NEXT_ENTRY, then the function returns in

a descriptor for the next ACL entry within

If a call is made to

with

set to ACL_NEXT_ENTRY when there has not been either an initial
successful call to

or a previous successful call to

following a call to

or

then the effect is unspecified.

Calls to

do not modify any ACL entries. Subsequent operations using the returned
ACL entry descriptor operate on the ACL entry within the ACL in working
storage. The order of all existing entries in the ACL remains unchanged.
Any existing ACL entry descriptors that refer to entries within the ACL
continue to refer to those entries. Any existing ACL pointers that refer
to the ACL referred to by

continue to refer to the ACL.

If the function successfully obtains an ACL entry, the function returns
a value of

If the ACL has no ACL entries, the function returns the value

If the value of

is ACL_NEXT_ENTRY and the last ACL entry in the ACL has already been
returned by a previous call to

the function returns the value

until a successful call with an

of ACL_FIRST_ENTRY is made. Otherwise, the value

is returned and

is set to indicate the error.

If any of the following conditions occur, the

function returns

and sets

to the corresponding value:

The argument

is not a valid pointer to an ACL.

The argument

is neither ACL_NEXT_ENTRY nor ACL_FIRST_ENTRY.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
