#+TITLE: Manpages - TAP_Parser_Result_Version.3perl
#+DESCRIPTION: Linux manpage for TAP_Parser_Result_Version.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
TAP::Parser::Result::Version - TAP syntax version token.

* VERSION
Version 3.43

* DESCRIPTION
This is a subclass of TAP::Parser::Result. A token of this class will be
returned if a version line is encountered.

TAP version 13 ok 1 not ok 2

The first version of TAP to include an explicit version number is 13.

* OVERRIDDEN METHODS
Mainly listed here to shut up the pitiful screams of the pod coverage
tests. They keep me awake at night.

- =as_string=

- =raw=

** Instance Methods
/=version=/

if ( $result->is_version ) { print $result->version; }

This is merely a synonym for =as_string=.
