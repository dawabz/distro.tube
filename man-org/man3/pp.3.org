#+TITLE: Manpages - pp.3
#+DESCRIPTION: Linux manpage for pp.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pp - These functions all print an ASCII representation of their argument
to stdout. Useful for debugging.

* SYNOPSIS
\\

** Functions
void *lo_bundle_pp* (*lo_bundle* b)\\
Pretty-print a lo_bundle object.

void *lo_message_pp* (*lo_message* m)\\
Pretty-print a lo_message object.

void *lo_arg_pp* (*lo_type* type, void *data)\\
Pretty-print a set of typed arguments.

void *lo_server_pp* (*lo_server* s)\\
Pretty-print a lo_server object.

void *lo_method_pp* (*lo_method* m)\\
Pretty-print a lo_method object.

void *lo_method_pp_prefix* (*lo_method* m, const char *p)\\
Pretty-print a lo_method object, but prepend a given prefix to all field
names.

* Detailed Description
These functions all print an ASCII representation of their argument to
stdout. Useful for debugging.

* Function Documentation
** void lo_arg_pp (*lo_type* type, void * data)
Pretty-print a set of typed arguments.

*Parameters*

#+begin_quote
  /type/ A type string in the form provided to *lo_send()*.\\
  /data/ An OSC data pointer, like that provided in the
  lo_method_handler.
#+end_quote

** void lo_bundle_pp (*lo_bundle* b)
Pretty-print a lo_bundle object.

** void lo_message_pp (*lo_message* m)
Pretty-print a lo_message object.

** void lo_method_pp (*lo_method* m)
Pretty-print a lo_method object.

** void lo_method_pp_prefix (*lo_method* m, const char * p)
Pretty-print a lo_method object, but prepend a given prefix to all field
names.

** void lo_server_pp (*lo_server* s)
Pretty-print a lo_server object.

* Author
Generated automatically by Doxygen for liblo from the source code.
