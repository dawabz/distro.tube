#+TITLE: Manpages - fwrite.3
#+DESCRIPTION: Linux manpage for fwrite.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about fwrite.3 is found in manpage for: [[../man3/fread.3][man3/fread.3]]