#+TITLE: Manpages - zip_source_filep.3
#+DESCRIPTION: Linux manpage for zip_source_filep.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The functions

and

create a zip source from a file stream. They read

bytes from offset

from the open file stream

If

is 0 or -1, the whole file (starting from

is used.

If the file stream supports seeking, the source can be used to open a
read-only zip archive from.

The file stream is closed when the source is being freed, usually by

Upon successful completion, the created source is returned. Otherwise,

is returned and the error code in

or

is set to indicate the error.

fails if:

or

are invalid.

Required memory could not be allocated.

and

were added in libzip 1.0.

and
