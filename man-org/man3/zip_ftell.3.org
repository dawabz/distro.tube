#+TITLE: Manpages - zip_ftell.3
#+DESCRIPTION: Linux manpage for zip_ftell.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function reports the current offset in the file.

If successful,

returns the current file position. Otherwise, -1 is returned.

was added in libzip 1.2.0.

and
