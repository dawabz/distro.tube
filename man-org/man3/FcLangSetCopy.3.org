#+TITLE: Manpages - FcLangSetCopy.3
#+DESCRIPTION: Linux manpage for FcLangSetCopy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcLangSetCopy - copy a langset object

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcLangSet * FcLangSetCopy (const FcLangSet */ls/*);*

* DESCRIPTION
*FcLangSetCopy* creates a new FcLangSet object and populates it with the
contents of /ls/.
