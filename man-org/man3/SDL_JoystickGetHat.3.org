#+TITLE: Manpages - SDL_JoystickGetHat.3
#+DESCRIPTION: Linux manpage for SDL_JoystickGetHat.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_JoystickGetHat - Get the current state of a joystick hat

* SYNOPSIS
*#include "SDL.h"*

*Uint8 SDL_JoystickGetHat*(*SDL_Joystick *joystick, int hat*);

* DESCRIPTION
SDL_JoystickGetHat returns the current state of the given *hat* on the
given *joystick*.

* RETURN VALUE
The current state is returned as a Uint8 which is defined as an OR'd
combination of one or more of the following

-  :: *SDL_HAT_CENTERED*

-  :: *SDL_HAT_UP*

-  :: *SDL_HAT_RIGHT*

-  :: *SDL_HAT_DOWN*

-  :: *SDL_HAT_LEFT*

-  :: *SDL_HAT_RIGHTUP*

-  :: *SDL_HAT_RIGHTDOWN*

-  :: *SDL_HAT_LEFTUP*

-  :: *SDL_HAT_LEFTDOWN*

* SEE ALSO
*SDL_JoystickNumHats*
