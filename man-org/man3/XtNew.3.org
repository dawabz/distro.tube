#+TITLE: Manpages - XtNew.3
#+DESCRIPTION: Linux manpage for XtNew.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtNew.3 is found in manpage for: [[../man3/XtMalloc.3][man3/XtMalloc.3]]