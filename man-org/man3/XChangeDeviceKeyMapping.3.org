#+TITLE: Manpages - XChangeDeviceKeyMapping.3
#+DESCRIPTION: Linux manpage for XChangeDeviceKeyMapping.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XChangeDeviceKeyMapping.3 is found in manpage for: [[../man3/XGetDeviceKeyMapping.3][man3/XGetDeviceKeyMapping.3]]