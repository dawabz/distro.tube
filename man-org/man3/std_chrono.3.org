#+TITLE: Manpages - std_chrono.3
#+DESCRIPTION: Linux manpage for std_chrono.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::chrono - ISO C++ 2011 namespace for date and time utilities.

* SYNOPSIS
\\

** Classes
struct *duration*\\
duration

struct *duration_values*\\
duration_values

struct *steady_clock*\\
Monotonic clock.

struct *system_clock*\\
System clock.

struct *time_point*\\
time_point

struct *treat_as_floating_point*\\
treat_as_floating_point

** Typedefs
using *high_resolution_clock* = *system_clock*\\
Highest-resolution clock.

using *hours* = *duration*< int64_t, *ratio*< 3600 > >\\
hours

using *microseconds* = *duration*< int64_t, *micro* >\\
microseconds

using *milliseconds* = *duration*< int64_t, *milli* >\\
milliseconds

using *minutes* = *duration*< int64_t, *ratio*< 60 > >\\
minutes

using *nanoseconds* = *duration*< int64_t, *nano* >\\
nanoseconds

using *seconds* = *duration*< int64_t >\\
seconds

** Functions
template<typename _Rep , typename _Period > constexpr *enable_if_t*<
*numeric_limits*< _Rep >::is_signed, *duration*< _Rep, _Period > > *abs*
(*duration*< _Rep, _Period > __d)\\

template<typename _ToDur , typename _Rep , typename _Period > constexpr
__enable_if_is_duration< _ToDur > *ceil* (const *duration*< _Rep,
_Period > &__d)\\

template<typename _ToDur , typename _Clock , typename _Dur > constexpr
*enable_if_t*< __is_duration< _ToDur >::value, *time_point*< _Clock,
_ToDur > > *ceil* (const *time_point*< _Clock, _Dur > &__tp)\\

template<typename _ToDur , typename _Rep , typename _Period > constexpr
__enable_if_is_duration< _ToDur > *duration_cast* (const *duration*<
_Rep, _Period > &__d)\\
duration_cast

template<typename _ToDur , typename _Rep , typename _Period > constexpr
__enable_if_is_duration< _ToDur > *floor* (const *duration*< _Rep,
_Period > &__d)\\

template<typename _ToDur , typename _Clock , typename _Dur > constexpr
*enable_if_t*< __is_duration< _ToDur >::value, *time_point*< _Clock,
_ToDur > > *floor* (const *time_point*< _Clock, _Dur > &__tp)\\

template<typename _ToDur , typename _Rep , typename _Period > constexpr
*enable_if_t*< __and_< __is_duration< _ToDur >, __not_<
*treat_as_floating_point*< typename _ToDur::rep > > >::value, _ToDur >
*round* (const *duration*< _Rep, _Period > &__d)\\

template<typename _ToDur , typename _Clock , typename _Dur > constexpr
*enable_if_t*< __and_< __is_duration< _ToDur >, __not_<
*treat_as_floating_point*< typename _ToDur::rep > > >::value,
*time_point*< _Clock, _ToDur > > *round* (const *time_point*< _Clock,
_Dur > &__tp)\\

template<typename _ToDur , typename _Clock , typename _Dur > constexpr
*enable_if*< __is_duration< _ToDur >::value, *time_point*< _Clock,
_ToDur > >::type *time_point_cast* (const *time_point*< _Clock, _Dur >
&__t)\\
time_point_cast

** Variables
template<typename _Rep > constexpr bool *treat_as_floating_point_v*\\

* Detailed Description
ISO C++ 2011 namespace for date and time utilities.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
