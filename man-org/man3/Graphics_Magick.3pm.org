#+TITLE: Manpages - Graphics_Magick.3pm
#+DESCRIPTION: Linux manpage for Graphics_Magick.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Graphics::Magick - Perl extension for calling GraphicsMagick's
libGraphicsMagick routines

* SYNOPSIS
use Graphics::Magick; p = new Graphics::Magick; p->Read("imagefile");
p->Set(attribute => value, ...) ($a, ...) = p->Get("attribute", ...)
p->routine(parameter => value, ...) p->Mogrify("Routine", parameter =>
value, ...) p->Write("filename");

* DESCRIPTION
This Perl extension allows the reading, manipulation and writing of a
large number of image file formats using the GraphicsMagick library. It
was originally developed to be used by CGI scripts for Web pages.

A Web page has been set up for this extension. See:

http://www.GraphicsMagick.org/www/perl.html

* AUTHOR
Kyle Shorter magick@wizards.dupont.com

* BUGS
Has all the bugs of GraphicsMagick and much, much more!

* SEE ALSO
*perl* (1).
