#+TITLE: Manpages - libmagic.3
#+DESCRIPTION: Linux manpage for libmagic.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
These functions operate on the magic database file which is described in

The function

creates a magic cookie pointer and returns it. It returns

if there was an error allocating the magic cookie. The

argument specifies how the other magic functions should behave:

No special handling.

Print debugging messages to stderr.

If the file queried is a symlink, follow it.

If the file is compressed, unpack it and look at the contents.

If the file is a block or character special device, then open the device
and try to look in its contents.

Return a MIME type string, instead of a textual description.

Return a MIME encoding, instead of a textual description.

A shorthand for MAGIC_MIME_TYPE | MAGIC_MIME_ENCODING.

Return all matches, not just the first.

Check the magic database for consistency and print warnings to stderr.

On systems that support

or

attempt to preserve the access time of files analysed.

Don't translate unprintable characters to a \ooo octal representation.

Treat operating system errors while trying to open files and follow
symlinks as real errors, instead of printing them in the magic buffer.

Return the Apple creator and type.

Return a slash-separated list of extensions for this file type.

Don't report on compression, only report about the uncompressed data.

Don't check for

application type (only on EMX).

Don't get extra information on MS Composite Document Files.

Don't look inside compressed files.

Don't print ELF details.

Don't check text encodings.

Don't consult magic files.

Don't examine tar files.

Don't check for various types of text files.

Don't look for known tokens inside ascii files.

Don't examine JSON files.

Don't examine CSV files.

The

function closes the

database and deallocates any resources used.

The

function returns a textual explanation of the last error, or

if there was no error.

The

function returns the last operating system error number

that was encountered by a system call.

The

function returns a textual description of the contents of the

argument, or

if an error occurred. If the

is

then stdin is used.

The

function returns a textual description of the contents of the

argument, or

if an error occurred.

The

function returns a textual description of the contents of the

argument with

bytes size.

The

functions returns a value representing current

set.

The

function sets the

described above. Note that using both MIME flags together can also
return extra information on the charset.

The

function can be used to check the validity of entries in the colon
separated database files passed in as

or

for the default database. It returns 0 on success and -1 on failure.

The

function can be used to compile the colon separated list of database
files passed in as

or

for the default database. It returns 0 on success and -1 on failure. The
compiled files created are named from the

of each file argument with

appended to it.

The

function dumps all magic entries in a human readable format, dumping
first the entries that are matched against binary files and then the
ones that match text files. It takes and optional

argument which is a colon separated list of database files, or

for the default database.

The

function must be used to load the colon separated list of database files
passed in as

or

for the default database file before any magic queries can performed.

The default database file is named by the MAGIC environment variable. If
that variable is not set, the default database file name is
/usr/share/file/misc/magic.

adds

to the database filename as appropriate.

The

function takes an array of size

of

with a respective size for each in the array of

loaded with the contents of the magic databases from the filesystem.
This function can be used in environment where the magic library does
not have direct access to the filesystem, but can access the magic
database via shared memory or other IPC means.

The

and

allow getting and setting various limits related to the magic library.

The

parameter controls how many levels of recursion will be followed for
indirect magic entries.

The

parameter controls how many levels of recursion will be followed for for
name/use calls.

The

parameter controls the maximum number of calls for name/use.

The

parameter controls how many ELF notes will be processed.

The

parameter controls how many ELF program sections will be processed.

The

parameter controls how many ELF sections will be processed.

The

command returns the version number of this library which is compiled
into the shared library using the constant

from

This can be used by client programs to verify that the version they
compile against is the same as the version that they run against.

The function

returns a magic cookie on success and

on failure setting errno to an appropriate value. It will set errno to

if an unsupported value for flags was given. The

and

functions return 0 on success and -1 on failure. The

and

functions return a string on success and

on failure. The

function returns a textual description of the errors of the above
functions, or

if there was no error. The

always returns the version number of the library. Finally,

returns -1 on systems that don't support

or

when

is set.

The non-compiled default magic database.

The compiled default magic database.

The results from

and

where the buffer and the file contain the same data can produce
different results, because in the

case, the program can

and

the file descriptor.

Initial libmagic implementation, and configuration.

API cleanup, error code and allocation handling.
