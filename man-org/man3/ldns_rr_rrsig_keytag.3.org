#+TITLE: Manpages - ldns_rr_rrsig_keytag.3
#+DESCRIPTION: Linux manpage for ldns_rr_rrsig_keytag.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_rr_rrsig_typecovered, ldns_rr_rrsig_set_typecovered,
ldns_rr_rrsig_algorithm, ldns_rr_rrsig_set_algorithm,
ldns_rr_rrsig_labels, ldns_rr_rrsig_set_labels, ldns_rr_rrsig_origttl,
ldns_rr_rrsig_set_origttl, ldns_rr_rrsig_expiration,
ldns_rr_rrsig_set_expiration, ldns_rr_rrsig_inception,
ldns_rr_rrsig_set_inception, ldns_rr_rrsig_keytag,
ldns_rr_rrsig_set_keytag, ldns_rr_rrsig_signame,
ldns_rr_rrsig_set_signame, ldns_rr_rrsig_sig, ldns_rr_rrsig_set_sig -
get and set RRSIG RR rdata fields

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_rdf* ldns_rr_rrsig_typecovered(const ldns_rr *r);

bool ldns_rr_rrsig_set_typecovered(ldns_rr *r, ldns_rdf *f);

ldns_rdf* ldns_rr_rrsig_algorithm(const ldns_rr *r);

bool ldns_rr_rrsig_set_algorithm(ldns_rr *r, ldns_rdf *f);

ldns_rdf* ldns_rr_rrsig_labels(const ldns_rr *r);

bool ldns_rr_rrsig_set_labels(ldns_rr *r, ldns_rdf *f);

ldns_rdf* ldns_rr_rrsig_origttl(const ldns_rr *r);

bool ldns_rr_rrsig_set_origttl(ldns_rr *r, ldns_rdf *f);

ldns_rdf* ldns_rr_rrsig_expiration(const ldns_rr *r);

bool ldns_rr_rrsig_set_expiration(ldns_rr *r, ldns_rdf *f);

ldns_rdf* ldns_rr_rrsig_inception(const ldns_rr *r);

bool ldns_rr_rrsig_set_inception(ldns_rr *r, ldns_rdf *f);

ldns_rdf* ldns_rr_rrsig_keytag(const ldns_rr *r);

bool ldns_rr_rrsig_set_keytag(ldns_rr *r, ldns_rdf *f);

ldns_rdf* ldns_rr_rrsig_signame(const ldns_rr *r);

bool ldns_rr_rrsig_set_signame(ldns_rr *r, ldns_rdf *f);

ldns_rdf* ldns_rr_rrsig_sig(const ldns_rr *r);

bool ldns_rr_rrsig_set_sig(ldns_rr *r, ldns_rdf *f);

* DESCRIPTION
/ldns_rr_rrsig_typecovered/() returns the type covered of a
LDNS_RR_TYPE_RRSIG rr .br *r*: the resource record .br Returns a
ldns_rdf* with the type covered or NULL on failure

/ldns_rr_rrsig_set_typecovered/() sets the typecovered of a
LDNS_RR_TYPE_RRSIG rr .br *r*: the rr to use .br *f*: the typecovered to
set .br Returns true on success, false otherwise

/ldns_rr_rrsig_algorithm/() returns the algorithm of a
LDNS_RR_TYPE_RRSIG RR .br *r*: the resource record .br Returns a
ldns_rdf* with the algorithm or NULL on failure

/ldns_rr_rrsig_set_algorithm/() sets the algorithm of a
LDNS_RR_TYPE_RRSIG rr .br *r*: the rr to use .br *f*: the algorithm to
set .br Returns true on success, false otherwise

/ldns_rr_rrsig_labels/() returns the number of labels of a
LDNS_RR_TYPE_RRSIG RR .br *r*: the resource record .br Returns a
ldns_rdf* with the number of labels or NULL on failure

/ldns_rr_rrsig_set_labels/() sets the number of labels of a
LDNS_RR_TYPE_RRSIG rr .br *r*: the rr to use .br *f*: the number of
labels to set .br Returns true on success, false otherwise

/ldns_rr_rrsig_origttl/() returns the original TTL of a
LDNS_RR_TYPE_RRSIG RR .br *r*: the resource record .br Returns a
ldns_rdf* with the original TTL or NULL on failure

/ldns_rr_rrsig_set_origttl/() sets the original TTL of a
LDNS_RR_TYPE_RRSIG rr .br *r*: the rr to use .br *f*: the original TTL
to set .br Returns true on success, false otherwise

/ldns_rr_rrsig_expiration/() returns the expiration time of a
LDNS_RR_TYPE_RRSIG RR .br *r*: the resource record .br Returns a
ldns_rdf* with the expiration time or NULL on failure

/ldns_rr_rrsig_set_expiration/() sets the expireation date of a
LDNS_RR_TYPE_RRSIG rr .br *r*: the rr to use .br *f*: the expireation
date to set .br Returns true on success, false otherwise

/ldns_rr_rrsig_inception/() returns the inception time of a
LDNS_RR_TYPE_RRSIG RR .br *r*: the resource record .br Returns a
ldns_rdf* with the inception time or NULL on failure

/ldns_rr_rrsig_set_inception/() sets the inception date of a
LDNS_RR_TYPE_RRSIG rr .br *r*: the rr to use .br *f*: the inception date
to set .br Returns true on success, false otherwise

/ldns_rr_rrsig_keytag/() returns the keytag of a LDNS_RR_TYPE_RRSIG RR
.br *r*: the resource record .br Returns a ldns_rdf* with the keytag or
NULL on failure

/ldns_rr_rrsig_set_keytag/() sets the keytag of a LDNS_RR_TYPE_RRSIG rr
.br *r*: the rr to use .br *f*: the keytag to set .br Returns true on
success, false otherwise

/ldns_rr_rrsig_signame/() returns the signers name of a
LDNS_RR_TYPE_RRSIG RR .br *r*: the resource record .br Returns a
ldns_rdf* with the signers name or NULL on failure

/ldns_rr_rrsig_set_signame/() sets the signers name of a
LDNS_RR_TYPE_RRSIG rr .br *r*: the rr to use .br *f*: the signers name
to set .br Returns true on success, false otherwise

/ldns_rr_rrsig_sig/() returns the signature data of a LDNS_RR_TYPE_RRSIG
RR .br *r*: the resource record .br Returns a ldns_rdf* with the
signature data or NULL on failure

/ldns_rr_rrsig_set_sig/() sets the signature data of a
LDNS_RR_TYPE_RRSIG rr .br *r*: the rr to use .br *f*: the signature data
to set .br Returns true on success, false otherwise

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
*perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*, *RFC4034* and
*RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
