#+TITLE: Manpages - getnetbyname.3
#+DESCRIPTION: Linux manpage for getnetbyname.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about getnetbyname.3 is found in manpage for: [[../man3/getnetent.3][man3/getnetent.3]]