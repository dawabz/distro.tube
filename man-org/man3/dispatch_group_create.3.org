#+TITLE: Manpages - dispatch_group_create.3
#+DESCRIPTION: Linux manpage for dispatch_group_create.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
A dispatch group is an association of one or more blocks submitted to
dispatch queues for asynchronous invocation. Applications may use
dispatch groups to wait for the completion of blocks associated with the
group.

The

function returns a new and empty dispatch group.

The

and

functions update the number of blocks running within a group.

The

function waits until all blocks associated with the

have completed, or until the specified

has elapsed. If the

becomes empty within the specified amount of time, the function will
return zero indicating success. Otherwise, a non-zero return code will
be returned. When

is passed as the

calls to this function will wait an unlimited amount of time until the
group becomes empty and the return value is always zero.

The

function provides asynchronous notification of the completion of the
blocks associated with the

by submitting the

to the specified

once all blocks associated with the

have completed. The system holds a reference to the dispatch group while
an asynchronous notification is pending, therefore it is valid to
release the

after setting a notification block. The group will be empty at the time
the notification block is submitted to the target queue. The group may
either be released with

or reused for additional operations.

The

convenience function behaves like so:

void dispatch_group_async(dispatch_group_t group, dispatch_queue_t
queue, dispatch_block_t block) { dispatch_retain(group);
dispatch_group_enter(group); dispatch_async(queue, ^{ block();
dispatch_group_leave(group); dispatch_release(group); }); }

The

function returns NULL on failure and non-NULL on success.

The

function returns zero upon success and non-zero after the timeout
expires. If the timeout is

then

waits forever and always returns zero.

Dispatch groups are retained and released via calls to

and

The

and

functions are wrappers around

and

respectively.

In order to ensure deterministic behavior, it is recommended to call

only once all blocks have been submitted to the group. If it is later
determined that new blocks should be run, it is recommended not to reuse
an already-running group, but to create a new group.

returns as soon as there are exactly zero

blocks associated with a group (more precisely, as soon as every

call has been balanced by a

call). If one thread waits for a group while another thread submits new
blocks to the group, then the count of associated blocks might
momentarily reach zero before all blocks have been submitted. If this
happens,

will return too early: some blocks associated with the group have
finished, but some have not yet been submitted or run.

However, as a special case, a block associated with a group may submit
new blocks associated with its own group. In this case, the behavior is
deterministic: a waiting thread will

wake up until the newly submitted blocks have also finished.

All of the foregoing also applies to

as well, with "block to be submitted" substituted for "waiting thread".
