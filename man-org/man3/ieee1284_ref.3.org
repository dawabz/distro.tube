#+TITLE: Manpages - ieee1284_ref.3
#+DESCRIPTION: Linux manpage for ieee1284_ref.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ieee1284_ref, ieee1284_unref - modify a ports reference count

* SYNOPSIS
#+begin_example
  #include <ieee1284.h>
#+end_example

*int ieee1284_ref(struct parport **/port/*);*

*int ieee1284_unref(struct parport **/port/*);*

* DESCRIPTION
If you want to free the port list from *ieee1284_find_ports* but open
one of the ports later on, you will need to prevent it from being
destroyed in *ieee1284_free_ports*. Each port has a reference count, and
you can use *ieee1284_ref* to increment it and *ieee1284_unref* to
decrement it.

If you use *ieee1284_ref* at any stage, you must later call
*ieee1284_unref* to relinquish the extra reference. If you do not do
this, the resources associated with the port will not be cleaned up.

If you have not previously used *ieee1284_ref* on a port, you must not
use *ieee1284_unref* on it.

* RETURN VALUE
These functions return the number of references held after the increment
or decrement.

* SEE ALSO
*ieee1284_open*(3)

* AUTHOR
*Tim Waugh* <twaugh@redhat.com>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2001-2003 Tim Waugh\\
