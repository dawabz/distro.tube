#+TITLE: Manpages - Memoize_AnyDBM_File.3perl
#+DESCRIPTION: Linux manpage for Memoize_AnyDBM_File.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Memoize::AnyDBM_File - glue to provide EXISTS for AnyDBM_File for
Storable use

* DESCRIPTION
See Memoize.
