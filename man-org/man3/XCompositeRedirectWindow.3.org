#+TITLE: Manpages - XCompositeRedirectWindow.3
#+DESCRIPTION: Linux manpage for XCompositeRedirectWindow.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XCompositeRedirectWindow.3 is found in manpage for: [[../man3/Xcomposite.3][man3/Xcomposite.3]]