#+TITLE: Manpages - rtcGetBufferData.3embree3
#+DESCRIPTION: Linux manpage for rtcGetBufferData.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcGetBufferData - gets a pointer to the buffer data
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void* rtcGetBufferData(RTCBuffer buffer);
#+end_example

** DESCRIPTION
The =rtcGetBufferData= function returns a pointer to the buffer data of
the specified buffer object (=buffer= argument).

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcNewBuffer]
