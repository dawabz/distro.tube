#+TITLE: Manpages - stringprep_ucs4_nfkc_normalize.3
#+DESCRIPTION: Linux manpage for stringprep_ucs4_nfkc_normalize.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
stringprep_ucs4_nfkc_normalize - API function

* SYNOPSIS
*#include <stringprep.h>*

*uint32_t * stringprep_ucs4_nfkc_normalize(const uint32_t * */str/*,
ssize_t */len/*);*

* ARGUMENTS
- const uint32_t * str :: a Unicode string.

- ssize_t len :: length of /str/ array, or -1 if /str/ is
  nul-terminated.

* DESCRIPTION
Converts a UCS4 string into canonical form, see
*stringprep_utf8_nfkc_normalize()* for more information.

Return value: a newly allocated Unicode string, that is the NFKC
normalized form of /str/ .

* REPORTING BUGS
Report bugs to <help-libidn@gnu.org>.\\
General guidelines for reporting bugs: http://www.gnu.org/gethelp/\\
GNU Libidn home page: http://www.gnu.org/software/libidn/

* COPYRIGHT
Copyright © 2002-2021 Simon Josefsson.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libidn* is maintained as a Texinfo manual.
If the *info* and *libidn* programs are properly installed at your site,
the command

#+begin_quote
  *info libidn*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *http://www.gnu.org/software/libidn/manual/*
#+end_quote
