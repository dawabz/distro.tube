#+TITLE: Manpages - CURLOPT_AUTOREFERER.3
#+DESCRIPTION: Linux manpage for CURLOPT_AUTOREFERER.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_AUTOREFERER - automatically update the referer header

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_AUTOREFERER, long
autorefer);

* DESCRIPTION
Pass a parameter set to 1 to enable this. When enabled, libcurl will
automatically set the Referer: header field in HTTP requests to the full
URL where it follows a Location: redirect.

* DEFAULT
0, disabled

* PROTOCOLS
HTTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

    /* follow redirects */
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

    /* set Referer: automatically when following redirects */
    curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 1L);

    ret = curl_easy_perform(curl);

    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Along with HTTP

* RETURN VALUE
Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

* SEE ALSO
*CURLOPT_REFERER*(3), *CURLOPT_FOLLOWLOCATION*(3),
