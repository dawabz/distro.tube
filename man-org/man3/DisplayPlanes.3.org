#+TITLE: Manpages - DisplayPlanes.3
#+DESCRIPTION: Linux manpage for DisplayPlanes.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about DisplayPlanes.3 is found in manpage for: [[../man3/AllPlanes.3][man3/AllPlanes.3]]