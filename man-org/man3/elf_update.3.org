#+TITLE: Manpages - elf_update.3
#+DESCRIPTION: Linux manpage for elf_update.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
elf_update - update an ELF descriptor

#+begin_example
#+end_example

* SYNOPSIS
*#include <libelf.h>*

*off_t elf_update (Elf **/elf/*, Elf_Cmd */cmd/*);*

* DESCRIPTION
The *elf_update*()
