#+TITLE: Manpages - rtcRetainScene.3embree3
#+DESCRIPTION: Linux manpage for rtcRetainScene.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcRetainScene - increments the scene reference count
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcRetainScene(RTCScene scene);
#+end_example

** DESCRIPTION
Scene objects are reference counted. The =rtcRetainScene= function
increments the reference count of the passed scene object (=scene=
argument). This function together with =rtcReleaseScene= allows to use
the internal reference counting in a C++ wrapper class to handle the
ownership of the object.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcNewScene], [rtcReleaseScene]
