#+TITLE: Manpages - std_time_base.3
#+DESCRIPTION: Linux manpage for std_time_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::time_base - Time format ordering data.

* SYNOPSIS
\\

=#include <locale_facets_nonio.h>=

Inherited by *std::time_get< _CharT, _InIter >*.

** Public Types
enum *dateorder* { *no_order*, *dmy*, *mdy*, *ymd*, *ydm* }\\

* Detailed Description
Time format ordering data.

This class provides an enum representing different orderings of time:
day, month, and year.

Definition at line *52* of file *locale_facets_nonio.h*.

* Member Enumeration Documentation
** enum std::time_base::dateorder
Definition at line *55* of file *locale_facets_nonio.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
