#+TITLE: Manpages - XFreeFontSet.3
#+DESCRIPTION: Linux manpage for XFreeFontSet.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XFreeFontSet.3 is found in manpage for: [[../man3/XCreateFontSet.3][man3/XCreateFontSet.3]]