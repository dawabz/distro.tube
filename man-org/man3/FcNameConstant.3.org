#+TITLE: Manpages - FcNameConstant.3
#+DESCRIPTION: Linux manpage for FcNameConstant.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcNameConstant - Get the value for a symbolic constant

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcNameConstant (FcChar8 */string/*, int **/result/*);*

* DESCRIPTION
Returns whether a symbolic constant with name /string/ is registered,
placing the value of the constant in /result/ if present.
