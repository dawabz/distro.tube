#+TITLE: Manpages - CURLOPT_CRLF.3
#+DESCRIPTION: Linux manpage for CURLOPT_CRLF.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_CRLF - CRLF conversion

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CRLF, long conv);

* DESCRIPTION
Pass a long. If the value is set to 1 (one), libcurl converts Unix
newlines to CRLF newlines on transfers. Disable this option again by
setting the value to 0 (zero).

This is a legacy option of questionable use.

* DEFAULT
0

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    CURLcode ret;
    curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/");
    curl_easy_setopt(curl, CURLOPT_CRLF, 1L);
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
SMTP since 7.40.0, other protocols since they were introduced

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*CURLOPT_CONV_FROM_NETWORK_FUNCTION*(3),
*CURLOPT_CONV_TO_NETWORK_FUNCTION*(3),
