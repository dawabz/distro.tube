#+TITLE: Manpages - CURLOPT_SOCKS5_AUTH.3
#+DESCRIPTION: Linux manpage for CURLOPT_SOCKS5_AUTH.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_SOCKS5_AUTH - methods for SOCKS5 proxy authentication

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SOCKS5_AUTH, long
bitmask);

* DESCRIPTION
Pass a long as parameter, which is set to a bitmask, to tell libcurl
which authentication method(s) are allowed for SOCKS5 proxy
authentication. The only supported flags are /CURLAUTH_BASIC/, which
allows username/password authentication, /CURLAUTH_GSSAPI/, which allows
GSS-API authentication, and /CURLAUTH_NONE/, which allows no
authentication. Set the actual user name and password with the
/CURLOPT_PROXYUSERPWD(3)/ option.

* DEFAULT
CURLAUTH_BASIC|CURLAUTH_GSSAPI

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* request to use a SOCKS5 proxy */
    curl_easy_setopt(curl, CURLOPT_PROXY, "socks5://user:pass@myproxy.com");

    /* enable username/password authentication only */
    curl_easy_setopt(curl, CURLOPT_SOCKS5_AUTH, CURLAUTH_BASIC);

    /* Perform the request */
    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.55.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if
not, or CURLE_NOT_BUILT_IN if the bitmask contains unsupported flags.

* SEE ALSO
*CURLOPT_PROXY*(3), *CURLOPT_PROXYTYPE*(3)
