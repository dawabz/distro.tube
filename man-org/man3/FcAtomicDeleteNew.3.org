#+TITLE: Manpages - FcAtomicDeleteNew.3
#+DESCRIPTION: Linux manpage for FcAtomicDeleteNew.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcAtomicDeleteNew - delete new file

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcAtomicDeleteNew (FcAtomic */atomic/*);*

* DESCRIPTION
Deletes the new file. Used in error recovery to back out changes.
