#+TITLE: Manpages - archive_write_set_options.3
#+DESCRIPTION: Linux manpage for archive_write_set_options.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Streaming Archive Library (libarchive, -larchive)

These functions provide a way for libarchive clients to configure
specific write modules.

Specifies an option that will be passed to the currently-registered
filters (including decompression filters) or format readers.

If

and

are both

these functions will do nothing and

will be returned. If

is

but

is not, these functions will do nothing and

will be returned.

If

is not

and

will be provided to the filter or reader named

The return value will be either

if the option was successfully handled or

if the option was unrecognized by the module or could otherwise not be
handled. If there is no such module,

will be returned.

If

is

and

will be provided to every registered module. If any module returns

this value will be returned immediately. Otherwise,

will be returned if any module accepts the option, and

in all other cases.

Calls

then

If either function returns

will be returned immediately. Otherwise, the greater of the two values
will be returned.

is a comma-separated list of options. If

is

or empty,

will be returned immediately.

Individual options have one of the following forms:

The option/value pair will be provided to every module. Modules that do
not accept an option with this name will ignore it.

The option will be provided to every module with a value of

The option will be provided to every module with a NULL value.

As above, but the corresponding option and value will be provided only
to modules whose name matches

The value is interpreted as octal digits specifying the file mode.

The value specifies the file name.

The value is interpreted as a decimal integer specifying the bzip2
compression level. Supported values are from 1 to 9.

The value is interpreted as a decimal integer specifying the gzip
compression level. Supported values are from 0 to 9.

Store timestamp. This is enabled by default.

Use

as compression method. Supported values are

and

The value is interpreted as a decimal integer specifying the lrzip
compression level. Supported values are from 1 to 9.

The value is interpreted as a decimal integer specifying the lz4
compression level. Supported values are from 0 to 9.

Enable stream checksum. This is enabled by default.

Enable block checksum. This is disabled by default.

The value is interpreted as a decimal integer specifying the lz4
compression block size. Supported values are from 4 to 7

Use the previous block of the block being compressed for a compression
dictionary to improve compression ratio. This is disabled by default.

The value is interpreted as a decimal integer specifying the lzop
compression level. Supported values are from 1 to 9.

The value is interpreted as octal digits specifying the file mode.

The value specifies the file name.

The value is interpreted as a decimal integer specifying the compression
level. Supported values are from 0 to 9.

The value is interpreted as a decimal integer specifying the number of
threads for multi-threaded lzma compression. If supported, the default
value is read from

The value is interpreted as a decimal integer specifying the compression
level. Supported values depend on the library version, common values are
from 1 to 22.

The value is one of

or

to indicate how the following entries should be compressed. Note that
this setting is ignored for directories, symbolic links, and other
special entries.

The value is interpreted as a decimal integer specifying the compression
level. Values between 0 and 9 are supported. The interpretation of the
compression level depends on the chosen compression method.

The value is used as a character set name that will be used when
translating file names.

The value is used as a character set name that will be used when
translating file, group and user names.

These options are used to set standard ISO9660 metadata.

The file with the specified name will be identified in the ISO9660
metadata as holding the abstract for this volume. Default: none.

The file with the specified name will be identified in the ISO9660
metadata as holding the application identifier for this volume. Default:
none.

The file with the specified name will be identified in the ISO9660
metadata as holding the bibliography for this volume. Default: none.

The file with the specified name will be identified in the ISO9660
metadata as holding the copyright for this volume. Default: none.

The file with the specified name will be identified in the ISO9660
metadata as holding the publisher information for this volume. Default:
none.

The specified string will be used as the Volume Identifier in the
ISO9660 metadata. It is limited to 32 bytes. Default: none.

These options are used to make an ISO9660 image that can be directly
booted on various systems.

The file matching this name will be used as the El Torito boot image
file.

The name that will be used for the El Torito boot catalog. Default:

The boot image file provided by the

option will be edited with appropriate boot information in bytes 8
through 64. Default: disabled

The load segment for a no-emulation boot image.

The number of "virtual" 512-byte sectors to be loaded from a
no-emulation boot image. Some very old BIOSes can only load very small
images, setting this value to 4 will often allow such BIOSes to load the
first part of the boot image (which will then need to be intelligent
enough to load the rest of itself). This should not be needed unless you
are trying to support systems with very old BIOSes. This defaults to the
full size of the image.

Specifies the boot semantics used by the El Torito boot image: If the

is

then the boot image is assumed to be a bootable floppy image. If the

is

then the boot image is assumed to be a bootable hard disk image. If the

is

the boot image is used without floppy or hard disk emulation. If the
boot image is exactly 1.2MB, 1.44MB, or 2.88MB, then the default is

otherwise the default is

Various extensions to the base ISO9660 format.

If enabled, allows filenames to begin with a leading period. If
disabled, filenames that begin with a leading period will have that
period replaced by an underscore character in the standard ISO9660
namespace. This does not impact names stored in the Rockridge or Joliet
extension area. Default: disabled.

If enabled, allows filenames to contain lowercase characters. If
disabled, filenames will be forced to uppercase. This does not impact
names stored in the Rockridge or Joliet extension area. Default:
disabled.

If enabled, allows filenames to contain multiple period characters, in
violation of the ISO9660 specification. If disabled, additional periods
will be converted to underscore characters. This does not impact names
stored in the Rockridge or Joliet extension area. Default: disabled.

If enabled, allows filenames to contain trailing period characters, in
violation of the ISO9660 specification. If disabled, trailing periods
will be converted to underscore characters. This does not impact names
stored in the Rockridge or Joliet extension area. Default: disabled.

If enabled, the Primary Volume Descriptor may contain lowercase ASCII
characters, in violation of the ISO9660 specification. If disabled,
characters will be converted to uppercase ASCII. Default: disabled.

If enabled, sharp and tilde characters will be permitted in filenames,
in violation if the ISO9660 specification. If disabled, such characters
will be converted to underscore characters. Default: disabled.

If enabled, version numbers will be included with files. If disabled,
version numbers will be suppressed, in violation of the ISO9660
standard. This does not impact names stored in the Rockridge or Joliet
extension area. Default: enabled.

This enables support for file size and file name extensions in the core
ISO9660 area. The name extensions specified here do not affect the names
stored in the Rockridge or Joliet extension areas.

The most compliant form of ISO9660 image. Filenames are limited to 8.3
uppercase format, directory names are limited to 8 uppercase characters,
files are limited to 4 GiB, the complete ISO9660 image cannot exceed 4
GiB.

Filenames are limited to 30 uppercase characters with a 30-character
extension, directory names are limited to 30 characters, files are
limited to 4 GiB.

As with

except that files may exceed 4 GiB.

As with

except that filenames may be up to 193 characters and may include
arbitrary 8-bit characters.

Microsoft's Joliet extensions store a completely separate set of
directory information about each file. In particular, this information
includes Unicode filenames of up to 255 characters. Default: enabled.

If enabled, libarchive will use directory relocation records to ensure
that no pathname exceeds the ISO9660 limit of 8 directory levels. If
disabled, no relocation will occur. Default: enabled.

If enabled, libarchive will cause an error if there are more than 65536
directories. If disabled, there is no limit on the number of
directories. Default: enabled

If enabled, 300 kiB of zero bytes will be appended to the end of the
archive. Default: enabled

If enabled, all 7-bit ASCII characters are permitted in filenames
(except lowercase characters unless

is also specified). This violates ISO9660 standards. This does not
impact names stored in the Rockridge or Joliet extension area. Default:
disabled.

The Rockridge extensions store an additional set of POSIX-style file
information with each file, including mtime, atime, ctime, permissions,
and long filenames with arbitrary 8-bit characters. These extensions
also support symbolic links and other POSIX file types. Default:
enabled.

The zisofs extensions permit each file to be independently compressed
using a gzip-compatible compression. This can provide significant size
savings, but requires the reading system to have support for these
extensions. These extensions are disabled by default.

The compression level used by the deflate compressor. Ranges from 0
(least effort) to 9 (most effort). Default: 6

Synonym for

Compress each file in the archive. Unlike

this is handled entirely within libarchive and does not require a
separate utility. For best results, libarchive tests each file and will
store the file uncompressed if the compression does not actually save
any space. In particular, files under 2k will never be compressed. Note
that boot image files are never compressed.

Recognizes files that have already been compressed with the

utility and sets up the necessary file metadata so that readers will
correctly identify these as zisofs-compressed files.

Specifies a filename that should not be compressed when using

This option can be provided multiple times to suppress compression on
many files.

Enable a particular keyword in the mtree output. Prefix with an
exclamation mark to disable the corresponding keyword. The default is
equivalent to

Enables all of the above keywords.

Enables generation of

lines that specify default values for the following files and/or
directories.

XXX needs explanation XXX

The value is used as a character set name that will be used when
translating file names.

The value is used as a character set name that will be used when
translating file names.

The value is used as a character set name that will be used when
translating file names.

The value is used as a character set name that will be used when
translating file, group and user names. The value is one of

or

With

there is no character conversion, with

names are converted to UTF-8.

When storing extended attributes, this option configures which headers
should be written. The value is one of

or

By default, both

and

headers are written.

The value is used as a character set name that will be used when
translating file, group and user names.

The value is used as a character set name that will be used when
translating file, group and user names.

Set to

to disable output of the warcinfo record.

Use

as file checksum method. Supported values are

and

Use

as compression method. Supported values are

and

The value is a decimal integer from 1 to 9 specifying the compression
level.

Use

as table of contents checksum method. Supported values are

and

The value is either

or

to indicate how the following entries should be compressed. Note that
this setting is ignored for directories, symbolic links, and other
special entries.

The value is interpreted as a decimal integer specifying the compression
level. Values between 0 and 9 are supported. A compression level of 0
switches the compression method to

other values will enable

compression with the given level.

Enable encryption using traditional zip encryption.

Use

as encryption type. Supported values are

and

This boolean option enables or disables experimental Zip features that
may not be compatible with other Zip implementations.

This boolean option disables CRC calculations. All CRC fields are set to
zero. It should not be used except for testing purposes.

The value is used as a character set name that will be used when
translating file names.

Zip64 extensions provide additional file size information for entries
larger than 4 GiB. They also provide extended file offset and archive
size information when archives exceed 4 GiB. By default, the Zip writer
selectively enables these extensions only as needed. In particular, if
the file size is unknown, the Zip writer will include Zip64 extensions
to guard against the possibility that the file might be larger than 4
GiB.

Setting this boolean option will force the writer to use Zip64
extensions even for small files that would not otherwise require them.
This is primarily useful for testing.

Disabling this option with

will force the Zip writer to avoid Zip64 extensions: It will reject
files with size greater than 4 GiB, it will reject any new entries once
the total archive size reaches 4 GiB, and it will not use Zip64
extensions for files with unknown size. In particular, this can improve
compatibility when generating archives where the entry sizes are not
known in advance.

The following example creates an archive write handle to create a
gzip-compressed ISO9660 format image. The two options here specify that
the ISO9660 archive will use

as the boot image for El Torito booting, and that the gzip compressor
should use the maximum compression level.

a = archive_write_new(); archive_write_add_filter_gzip(a);
archive_write_set_format_iso9660(a); archive_write_set_options(a,
"boot=kernel.img,compression=9"); archive_write_open_filename(a,
filename, blocksize);

More detailed error codes and textual descriptions are available from
the

and

functions.

The

library first appeared in

The options support for libarchive was originally implemented by
