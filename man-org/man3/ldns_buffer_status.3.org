#+TITLE: Manpages - ldns_buffer_status.3
#+DESCRIPTION: Linux manpage for ldns_buffer_status.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_buffer_remaining_at, ldns_buffer_remaining,
ldns_buffer_available_at, ldns_buffer_available, ldns_buffer_status,
ldns_buffer_status_ok - check buffer status

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

size_t ldns_buffer_remaining_at(const ldns_buffer *buffer, size_t at);

size_t ldns_buffer_remaining(const ldns_buffer *buffer);

int ldns_buffer_available_at(const ldns_buffer *buffer, size_t at,
size_t count);

int ldns_buffer_available(const ldns_buffer *buffer, size_t count);

ldns_status ldns_buffer_status(const ldns_buffer *buffer);

bool ldns_buffer_status_ok(const ldns_buffer *buffer);

* DESCRIPTION
/ldns_buffer_remaining_at/() returns the number of bytes remaining
between the indicated position and the limit. .br *buffer*: the buffer
.br *at*: indicated position .br Returns number of bytes

/ldns_buffer_remaining/() returns the number of bytes remaining between
the buffer's position and limit. .br *buffer*: the buffer .br Returns
the number of bytes

/ldns_buffer_available_at/() checks if the buffer has at least COUNT
more bytes available. Before reading or writing the caller needs to
ensure enough space is available! .br *buffer*: the buffer .br *at*:
indicated position .br *count*: how much is available .br Returns true
or false (as int?)

/ldns_buffer_available/() checks if the buffer has count bytes available
at the current position .br *buffer*: the buffer .br *count*: how much
is available .br Returns true or false (as int?)

/ldns_buffer_status/() returns the status of the buffer \param[in]
buffer .br Returns the status

/ldns_buffer_status_ok/() returns true if the status of the buffer is
LDNS_STATUS_OK, false otherwise .br *buffer*: the buffer .br Returns
true or false

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_buffer/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*,
*RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
