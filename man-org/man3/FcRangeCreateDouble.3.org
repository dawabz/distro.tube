#+TITLE: Manpages - FcRangeCreateDouble.3
#+DESCRIPTION: Linux manpage for FcRangeCreateDouble.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcRangeCreateDouble - create a range object for double

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcRange * FcRangeCreateDouble (double/begin/*, double*/end/*);*

* DESCRIPTION
*FcRangeCreateDouble* creates a new FcRange object with double sized
value.

* SINCE
version 2.11.91
