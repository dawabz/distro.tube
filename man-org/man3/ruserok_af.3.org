#+TITLE: Manpages - ruserok_af.3
#+DESCRIPTION: Linux manpage for ruserok_af.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ruserok_af.3 is found in manpage for: [[../man3/rcmd.3][man3/rcmd.3]]