#+TITLE: Manpages - setgrent.3
#+DESCRIPTION: Linux manpage for setgrent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about setgrent.3 is found in manpage for: [[../man3/getgrent.3][man3/getgrent.3]]