#+TITLE: Manpages - std___detail__Mask_range_hashing.3
#+DESCRIPTION: Linux manpage for std___detail__Mask_range_hashing.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Mask_range_hashing - Range hashing function assuming
that second arg is a power of 2.

* SYNOPSIS
\\

=#include <hashtable_policy.h>=

** Public Types
typedef std::size_t *first_argument_type*\\

typedef std::size_t *result_type*\\

typedef std::size_t *second_argument_type*\\

** Public Member Functions
result_type *operator()* (first_argument_type __num,
second_argument_type __den) const noexcept\\

* Detailed Description
Range hashing function assuming that second arg is a power of 2.

Definition at line *490* of file *hashtable_policy.h*.

* Member Typedef Documentation
** typedef std::size_t
std::__detail::_Mask_range_hashing::first_argument_type
Definition at line *492* of file *hashtable_policy.h*.

** typedef std::size_t std::__detail::_Mask_range_hashing::result_type
Definition at line *494* of file *hashtable_policy.h*.

** typedef std::size_t
std::__detail::_Mask_range_hashing::second_argument_type
Definition at line *493* of file *hashtable_policy.h*.

* Member Function Documentation
** result_type std::__detail::_Mask_range_hashing::operator()
(first_argument_type __num, second_argument_type __den)
const= [inline]=, = [noexcept]=
Definition at line *497* of file *hashtable_policy.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
