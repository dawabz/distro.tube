#+TITLE: Manpages - errx.3
#+DESCRIPTION: Linux manpage for errx.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about errx.3 is found in manpage for: [[../man3/err.3][man3/err.3]]