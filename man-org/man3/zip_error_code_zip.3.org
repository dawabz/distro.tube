#+TITLE: Manpages - zip_error_code_zip.3
#+DESCRIPTION: Linux manpage for zip_error_code_zip.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function returns the libzip specific part of the error from the
zip_error error

was added in libzip 1.0.

and
