#+TITLE: Manpages - paperwithsize.3
#+DESCRIPTION: Linux manpage for paperwithsize.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about paperwithsize.3 is found in manpage for: [[../man3/paperinfo.3][man3/paperinfo.3]]