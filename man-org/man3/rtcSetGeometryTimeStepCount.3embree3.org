#+TITLE: Manpages - rtcSetGeometryTimeStepCount.3embree3
#+DESCRIPTION: Linux manpage for rtcSetGeometryTimeStepCount.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcSetGeometryTimeStepCount - sets the number of time steps of the
    geometry
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcSetGeometryTimeStepCount(
    RTCGeometry geometry,
    unsigned int timeStepCount
  );
#+end_example

** DESCRIPTION
The =rtcSetGeometryTimeStepCount= function sets the number of time steps
for multi-segment motion blur (=timeStepCount= parameter) of the
specified geometry (=geometry= parameter).

For triangle meshes (=RTC_GEOMETRY_TYPE_TRIANGLE=), quad meshes
(=RTC_GEOMETRY_TYPE_QUAD=), curves (=RTC_GEOMETRY_TYPE_CURVE=), points
(=RTC_GEOMETRY_TYPE_POINT=), and subdivision geometries
(=RTC_GEOMETRY_TYPE_SUBDIVISION=), the number of time steps directly
corresponds to the number of vertex buffer slots available
(=RTC_BUFFER_TYPE_VERTEX= buffer type). For these geometries, one vertex
buffer per time step must be specified when creating multi-segment
motion blur geometries.

For instance geometries (=RTC_GEOMETRY_TYPE_INSTANCE=), a transformation
must be specified for each time step (see =rtcSetGeometryTransform=).

For user geometries, the registered bounding callback function must
provide a bounding box per primitive and time step, and the intersection
and occlusion callback functions should properly intersect the
motion-blurred geometry at the ray time.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcNewGeometry], [rtcSetGeometryTimeRange]
