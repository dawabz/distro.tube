#+TITLE: Manpages - xcb_destroy_window.3
#+DESCRIPTION: Linux manpage for xcb_destroy_window.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_destroy_window - Destroys a window

* SYNOPSIS
*#include <xcb/xproto.h>*

** Request function
xcb_void_cookie_t *xcb_destroy_window*(xcb_connection_t */conn/,
xcb_window_t /window/);\\

* REQUEST ARGUMENTS
- conn :: The XCB connection to X11.

- window :: The window to destroy.

* DESCRIPTION
Destroys the specified window and all of its subwindows. A DestroyNotify
event is generated for each destroyed window (a DestroyNotify event is
first generated for any given window's inferiors). If the window was
mapped, it will be automatically unmapped before destroying.

Calling DestroyWindow on the root window will do nothing.

* RETURN VALUE
Returns an /xcb_void_cookie_t/. Errors (if any) have to be handled in
the event loop.

If you want to handle errors directly with /xcb_request_check/ instead,
use /xcb_destroy_window_checked/. See *xcb-requests(3)* for details.

* ERRORS
- xcb_window_error_t :: The specified window does not exist.

* SEE ALSO
*xcb-requests*(3), *xcb_destroy_notify_event_t*(3), *xcb_map_window*(3),
*xcb_unmap_window*(3)

* AUTHOR
Generated from xproto.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
