#+TITLE: Manpages - rtcFilterOcclusion.3embree3
#+DESCRIPTION: Linux manpage for rtcFilterOcclusion.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcFilterOcclusion - invokes the occlusion filter function
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcFilterOcclusion(
    const struct RTCOccludedFunctionNArguments* args,
    const struct RTCFilterFunctionNArguments* filterArgs
  );
#+end_example

** DESCRIPTION
The =rtcFilterOcclusion= function can be called inside an
=RTCOccludedFunctionN= callback function to invoke the occlusion filter
registered to the geometry and stored inside the context. For this an
=RTCFilterFunctionNArguments= structure must be created (see
=rtcSetGeometryIntersectFilterFunction=) which basically consists of a
valid mask, a hit packet to filter, the corresponding ray packet, and
the packet size. After the invocation of =rtcFilterOcclusion= only rays
that are still valid (valid mask set to -1) should signal an occlusion.

** EXIT STATUS
For performance reasons this function does not do any error checks, thus
will not set any error flags on failure.

** SEE ALSO
[rtcFilterIntersection], [rtcSetGeometryOccludedFunction]
