#+TITLE: Manpages - TAP_Formatter_Console.3perl
#+DESCRIPTION: Linux manpage for TAP_Formatter_Console.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
TAP::Formatter::Console - Harness output delegate for default console
output

* VERSION
Version 3.43

* DESCRIPTION
This provides console orientated output formatting for TAP::Harness.

* SYNOPSIS
use TAP::Formatter::Console; my $harness = TAP::Formatter::Console->new(
\%args );

** "open_test"
See TAP::Formatter::Base
