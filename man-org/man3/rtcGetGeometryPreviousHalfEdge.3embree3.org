#+TITLE: Manpages - rtcGetGeometryPreviousHalfEdge.3embree3
#+DESCRIPTION: Linux manpage for rtcGetGeometryPreviousHalfEdge.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcGetGeometryPreviousHalfEdge - returns the previous half edge
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  unsigned int rtcGetGeometryPreviousHalfEdge(
    RTCGeometry geometry,
    unsigned int edgeID
  );
#+end_example

** DESCRIPTION
The =rtcGetGeometryPreviousHalfEdge= function returns the ID of the
previous half edge of the specified half edge (=edgeID= argument). For
instance in the following example the previous half edge of =e6= is
=e5=.

#+begin_example
#+end_example

This function can only be used for subdivision geometries. As all
topologies of a subdivision geometry share the same face buffer the
function does not depend on the topology ID.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcGetGeometryFirstHalfEdge], [rtcGetGeometryFace],
[rtcGetGeometryOppositeHalfEdge], [rtcGetGeometryNextHalfEdge],
[rtcGetGeometryPreviousHalfEdge]
