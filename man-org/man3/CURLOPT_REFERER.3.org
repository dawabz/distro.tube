#+TITLE: Manpages - CURLOPT_REFERER.3
#+DESCRIPTION: Linux manpage for CURLOPT_REFERER.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_REFERER - the HTTP referer header

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_REFERER, char *where);

* DESCRIPTION
Pass a pointer to a null-terminated string as parameter. It will be used
to set the Referer: header in the http request sent to the remote
server. This can be used to fool servers or scripts. You can also set
any custom header with /CURLOPT_HTTPHEADER(3)/.

The application does not have to keep the string around after setting
this option.

* DEFAULT
NULL

* PROTOCOLS
HTTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* tell it where we found the link to this place */
    curl_easy_setopt(curl, CURLOPT_REFERER, "https://example.com/aboutme.html");

    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
If built with HTTP support

* RETURN VALUE
Returns CURLE_OK if HTTP support is enabled, CURLE_UNKNOWN_OPTION if
not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_USERAGENT*(3), *CURLOPT_HTTPHEADER*(3), *CURLINFO_REFERER*(3),
