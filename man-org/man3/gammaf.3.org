#+TITLE: Manpages - gammaf.3
#+DESCRIPTION: Linux manpage for gammaf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about gammaf.3 is found in manpage for: [[../man3/gamma.3][man3/gamma.3]]