#+TITLE: Manpages - fmaf.3
#+DESCRIPTION: Linux manpage for fmaf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about fmaf.3 is found in manpage for: [[../man3/fma.3][man3/fma.3]]