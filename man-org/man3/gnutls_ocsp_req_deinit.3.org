#+TITLE: Manpages - gnutls_ocsp_req_deinit.3
#+DESCRIPTION: Linux manpage for gnutls_ocsp_req_deinit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_ocsp_req_deinit - API function

* SYNOPSIS
*#include <gnutls/ocsp.h>*

*void gnutls_ocsp_req_deinit(gnutls_ocsp_req_t */req/*);*

* ARGUMENTS
- gnutls_ocsp_req_t req :: The data to be deinitialized

* DESCRIPTION
This function will deinitialize a OCSP request structure.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
