#+TITLE: Manpages - std_regex_token_iterator.3
#+DESCRIPTION: Linux manpage for std_regex_token_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::regex_token_iterator< _Bi_iter, _Ch_type, _Rx_traits >

* SYNOPSIS
\\

=#include <regex.h>=

** Public Types
typedef std::ptrdiff_t *difference_type*\\

typedef *std::forward_iterator_tag* *iterator_category*\\

typedef const *value_type* * *pointer*\\

typedef const *value_type* & *reference*\\

typedef *basic_regex*< _Ch_type, _Rx_traits > *regex_type*\\

typedef *sub_match*< _Bi_iter > *value_type*\\

** Public Member Functions
*regex_token_iterator* ()\\
Default constructs a regex_token_iterator.

template<std::size_t _Nm> *regex_token_iterator* (_Bi_iter __a, _Bi_iter
__b, const *regex_type* &__re, const int(&__submatches)[_Nm],
*regex_constants::match_flag_type*
__m=*regex_constants::match_default*)\\

*regex_token_iterator* (_Bi_iter __a, _Bi_iter __b, const *regex_type*
&__re, const *std::vector*< int > &__submatches,
*regex_constants::match_flag_type*
__m=*regex_constants::match_default*)\\

*regex_token_iterator* (_Bi_iter __a, _Bi_iter __b, const *regex_type*
&__re, *initializer_list*< int > __submatches,
*regex_constants::match_flag_type*
__m=*regex_constants::match_default*)\\

*regex_token_iterator* (_Bi_iter __a, _Bi_iter __b, const *regex_type*
&__re, int __submatch=0, *regex_constants::match_flag_type*
__m=*regex_constants::match_default*)\\

template<std::size_t _Nm> *regex_token_iterator* (_Bi_iter, _Bi_iter,
const *regex_type* &&, const int(&)[_Nm],
*regex_constants::match_flag_type*=*regex_constants::match_default*)=delete\\

*regex_token_iterator* (_Bi_iter, _Bi_iter, const *regex_type* &&, const
*std::vector*< int > &,
*regex_constants::match_flag_type*=*regex_constants::match_default*)=delete\\

*regex_token_iterator* (_Bi_iter, _Bi_iter, const *regex_type* &&,
*initializer_list*< int >,
*regex_constants::match_flag_type*=*regex_constants::match_default*)=delete\\

*regex_token_iterator* (_Bi_iter, _Bi_iter, const *regex_type* &&,
int=0,
*regex_constants::match_flag_type*=*regex_constants::match_default*)=delete\\

*regex_token_iterator* (const *regex_token_iterator* &__rhs)\\
Copy constructs a regex_token_iterator.

bool *operator!=* (const *regex_token_iterator* &__rhs) const\\
Compares a regex_token_iterator to another for inequality.

const *value_type* & *operator** () const\\
Dereferences a regex_token_iterator.

*regex_token_iterator* & *operator++* ()\\
Increments a regex_token_iterator.

*regex_token_iterator* *operator++* (int)\\
Postincrements a regex_token_iterator.

const *value_type* * *operator->* () const\\
Selects a regex_token_iterator member.

*regex_token_iterator* & *operator=* (const *regex_token_iterator*
&__rhs)\\
Assigns a regex_token_iterator to another.

bool *operator==* (const *regex_token_iterator* &__rhs) const\\
Compares a regex_token_iterator to another for equality.

* Detailed Description
** "template<typename _Bi_iter, typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>>
\\
class std::regex_token_iterator< _Bi_iter, _Ch_type, _Rx_traits
>"Iterates over submatches in a range (or /splits/ a text string).

The purpose of this iterator is to enumerate all, or all specified,
matches of a regular expression within a text range. The dereferenced
value of an iterator of this class is a std::sub_match object.

Definition at line *2744* of file *regex.h*.

* Member Typedef Documentation
** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> typedef std::ptrdiff_t
*std::regex_token_iterator*< _Bi_iter, _Ch_type, _Rx_traits
>::difference_type
Definition at line *2749* of file *regex.h*.

** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> typedef *std::forward_iterator_tag*
*std::regex_token_iterator*< _Bi_iter, _Ch_type, _Rx_traits
>::*iterator_category*
Definition at line *2752* of file *regex.h*.

** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> typedef const *value_type**
*std::regex_token_iterator*< _Bi_iter, _Ch_type, _Rx_traits >::*pointer*
Definition at line *2750* of file *regex.h*.

** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> typedef const *value_type*&
*std::regex_token_iterator*< _Bi_iter, _Ch_type, _Rx_traits
>::*reference*
Definition at line *2751* of file *regex.h*.

** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> typedef *basic_regex*<_Ch_type, _Rx_traits>
*std::regex_token_iterator*< _Bi_iter, _Ch_type, _Rx_traits
>::*regex_type*
Definition at line *2747* of file *regex.h*.

** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> typedef *sub_match*<_Bi_iter>
*std::regex_token_iterator*< _Bi_iter, _Ch_type, _Rx_traits
>::*value_type*
Definition at line *2748* of file *regex.h*.

* Constructor & Destructor Documentation
** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> *std::regex_token_iterator*< _Bi_iter, _Ch_type,
_Rx_traits >::*regex_token_iterator* ()= [inline]=
Default constructs a regex_token_iterator. A default-constructed
regex_token_iterator is a singular iterator that will compare equal to
the one-past-the-end value for any iterator of the same type.

Definition at line *2762* of file *regex.h*.

** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> *std::regex_token_iterator*< _Bi_iter, _Ch_type,
_Rx_traits >::*regex_token_iterator* (_Bi_iter __a, _Bi_iter __b, const
*regex_type* & __re, int __submatch = =0=,
*regex_constants::match_flag_type* __m =
*regex_constants::match_default)*= [inline]=
Constructs a regex_token_iterator...

*Parameters*

#+begin_quote
  /__a/* [IN] The start of the text to search. *\\
  /__b/* [IN] One-past-the-end of the text to search. *\\
  /__re/* [IN] The regular expression to search for. *\\
  /__submatch/* [IN] Which submatch to return. There are some special
  values for this parameter:*

  - -1 each enumerated subexpression does NOT match the regular
    expression (aka field splitting)

  - 0 the entire string matching the subexpression is returned for each
    match within the text.

  - >0 enumerates only the indicated subexpression from a match within
    the text.

  \\
  /__m/* [IN] Policy flags for match rules. *
#+end_quote

Definition at line *2784 of file regex.h.*

** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> *std::regex_token_iterator< _Bi_iter, _Ch_type,
_Rx_traits >::regex_token_iterator (_Bi_iter __a, _Bi_iter __b, const
regex_type & __re, const std::vector< int > & __submatches,
regex_constants::match_flag_type __m =
regex_constants::match_default)*= [inline]=
Constructs a regex_token_iterator...

*Parameters*

#+begin_quote
  /__a/* [IN] The start of the text to search. *\\
  /__b/* [IN] One-past-the-end of the text to search. *\\
  /__re/* [IN] The regular expression to search for. *\\
  /__submatches/* [IN] A list of subexpressions to return for each
  regular expression match within the text. *\\
  /__m/* [IN] Policy flags for match rules. *
#+end_quote

Definition at line *2800 of file regex.h.*

** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> *std::regex_token_iterator< _Bi_iter, _Ch_type,
_Rx_traits >::regex_token_iterator (_Bi_iter __a, _Bi_iter __b, const
regex_type & __re, initializer_list< int > __submatches,
regex_constants::match_flag_type __m =
regex_constants::match_default)*= [inline]=
Constructs a regex_token_iterator...

*Parameters*

#+begin_quote
  /__a/* [IN] The start of the text to search. *\\
  /__b/* [IN] One-past-the-end of the text to search. *\\
  /__re/* [IN] The regular expression to search for. *\\
  /__submatches/* [IN] A list of subexpressions to return for each
  regular expression match within the text. *\\
  /__m/* [IN] Policy flags for match rules. *
#+end_quote

Definition at line *2817 of file regex.h.*

** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> template<std::size_t _Nm>
*std::regex_token_iterator< _Bi_iter, _Ch_type, _Rx_traits
>::regex_token_iterator (_Bi_iter __a, _Bi_iter __b, const regex_type &
__re, const int(&) __submatches[_Nm], regex_constants::match_flag_type
__m = regex_constants::match_default)*= [inline]=
Constructs a regex_token_iterator...

*Parameters*

#+begin_quote
  /__a/* [IN] The start of the text to search. *\\
  /__b/* [IN] One-past-the-end of the text to search. *\\
  /__re/* [IN] The regular expression to search for. *\\
  /__submatches/* [IN] A list of subexpressions to return for each
  regular expression match within the text. *\\
  /__m/* [IN] Policy flags for match rules. *
#+end_quote

Definition at line *2835 of file regex.h.*

** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> *std::regex_token_iterator< _Bi_iter, _Ch_type,
_Rx_traits >::regex_token_iterator (const regex_token_iterator<
_Bi_iter, _Ch_type, _Rx_traits > & __rhs)*= [inline]=
Copy constructs a regex_token_iterator.

*Parameters*

#+begin_quote
  /__rhs/* [IN] A regex_token_iterator to copy. *
#+end_quote

Definition at line *2867 of file regex.h.*

* Member Function Documentation
** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> bool *std::regex_token_iterator< _Bi_iter,
_Ch_type, _Rx_traits >::operator!= (const regex_token_iterator<
_Bi_iter, _Ch_type, _Rx_traits > & __rhs) const*= [inline]=
Compares a regex_token_iterator to another for inequality.

Definition at line *2889 of file regex.h.*

** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> const *value_type & std::regex_token_iterator<
_Bi_iter, _Ch_type, _Rx_traits >::operator* () const*= [inline]=
Dereferences a regex_token_iterator.

Definition at line *2896 of file regex.h.*

** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> *regex_token_iterator &
std::regex_token_iterator< _Bi_iter, _Ch_type, _Rx_traits >::operator++
()*
Increments a regex_token_iterator.

** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> *regex_token_iterator std::regex_token_iterator<
_Bi_iter, _Ch_type, _Rx_traits >::operator++ (int)*= [inline]=
Postincrements a regex_token_iterator.

Definition at line *2916 of file regex.h.*

** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> const *value_type * std::regex_token_iterator<
_Bi_iter, _Ch_type, _Rx_traits >::operator-> () const*= [inline]=
Selects a regex_token_iterator member.

Definition at line *2903 of file regex.h.*

** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> *regex_token_iterator &
std::regex_token_iterator< _Bi_iter, _Ch_type, _Rx_traits >::operator=
(const regex_token_iterator< _Bi_iter, _Ch_type, _Rx_traits > & __rhs)*
Assigns a regex_token_iterator to another.

*Parameters*

#+begin_quote
  /__rhs/* [IN] A regex_token_iterator to copy. *
#+end_quote

** template<typename _Bi_iter , typename _Ch_type = typename
iterator_traits<_Bi_iter>::value_type, typename _Rx_traits =
regex_traits<_Ch_type>> bool *std::regex_token_iterator< _Bi_iter,
_Ch_type, _Rx_traits >::operator== (const regex_token_iterator<
_Bi_iter, _Ch_type, _Rx_traits > & __rhs) const*
Compares a regex_token_iterator to another for equality.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
