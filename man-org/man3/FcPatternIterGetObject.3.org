#+TITLE: Manpages - FcPatternIterGetObject.3
#+DESCRIPTION: Linux manpage for FcPatternIterGetObject.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcPatternIterGetObject - Returns an object name which the iterator point
to

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

const char * FcPatternIterGetObject (const FcPattern */p/*,
FcPatternIter **/iter/*);*

* DESCRIPTION
Returns an object name in /p/ which /iter/ point to. returns NULL if
/iter/ isn't valid.

* SINCE
version 2.13.1
