#+TITLE: Manpages - bswap.3
#+DESCRIPTION: Linux manpage for bswap.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
bswap_16, bswap_32, bswap_64 - reverse order of bytes

* SYNOPSIS
#+begin_example
  #include <byteswap.h>

  uint16_t bswap_16(uint16_t x);
  uint32_t bswap_32(uint32_t x);
  uint64_t bswap_64(uint64_t x);
#+end_example

* DESCRIPTION
These functions return a value in which the order of the bytes in their
2-, 4-, or 8-byte arguments is reversed.

* RETURN VALUE
These functions return the value of their argument with the bytes
reversed.

* ERRORS
These functions always succeed.

* CONFORMING TO
These functions are GNU extensions.

* EXAMPLES
The program below swaps the bytes of the 8-byte integer supplied as its
command-line argument. The following shell session demonstrates the use
of the program:

#+begin_example
  $ ./a.out 0x0123456789abcdef
  0x123456789abcdef ==> 0xefcdab8967452301
#+end_example

** Program source
#+begin_example
  #include <stdio.h>
  #include <stdint.h>
  #include <stdlib.h>
  #include <inttypes.h>
  #include <byteswap.h>

  int
  main(int argc, char *argv[])
  {
      uint64_t x;

      if (argc != 2) {
          fprintf(stderr, "Usage: %s <num>\n", argv[0]);
          exit(EXIT_FAILURE);
      }

      x = strtoull(argv[1], NULL, 0);
      printf("%#" PRIx64 " ==> %#" PRIx64 "\n", x, bswap_64(x));

      exit(EXIT_SUCCESS);
  }
#+end_example

* SEE ALSO
*byteorder*(3), *endian*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
