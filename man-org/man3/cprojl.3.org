#+TITLE: Manpages - cprojl.3
#+DESCRIPTION: Linux manpage for cprojl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about cprojl.3 is found in manpage for: [[../man3/cproj.3][man3/cproj.3]]