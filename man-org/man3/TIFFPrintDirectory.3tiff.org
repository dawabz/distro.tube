#+TITLE: Manpages - TIFFPrintDirectory.3tiff
#+DESCRIPTION: Linux manpage for TIFFPrintDirectory.3tiff
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
TIFFPrintDirectory - print a description of a

directory

* SYNOPSIS
*#include <tiffio.h>*

*void TIFFPrintDirectory(TIFF **/tif/*, FILE **/fd/*, long */flags/*)*

* DESCRIPTION
/TIFFPrintDirectory/ prints a description of the current directory in
the specified

file to the standard I/O output stream /fd/. The /flags/ parameter is
used to control the /level of detail/ of the printed information; it is
a bit-or of the flags defined in *tiffio.h*:

#+begin_example
  #define	TIFFPRINT_NONE	0x0	/* no extra info */
  #define	TIFFPRINT_STRIPS	0x1	/* strips/tiles info */
  #define	TIFFPRINT_CURVES	0x2	/* color/gray response curves */
  #define	TIFFPRINT_COLORMAP	0x4	/* colormap */
  #define	TIFFPRINT_JPEGQTABLES	0x100	/* JPEG Q matrices */
  #define	TIFFPRINT_JPEGACTABLES	0x200	/* JPEG AC tables */
  #define	TIFFPRINT_JPEGDCTABLES	0x200	/* JPEG DC tables */
#+end_example

* NOTES
In C++ the /flags/ parameter defaults to 0.

* RETURN VALUES
None.

* DIAGNOSTICS
None.

* SEE ALSO
/libtiff/(3TIFF), /TIFFOpen/(3TIFF), /TIFFReadDirectory/(3TIFF),
/TIFFSetDirectory/(3TIFF)
