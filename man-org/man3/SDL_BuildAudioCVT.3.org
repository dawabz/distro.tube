#+TITLE: Manpages - SDL_BuildAudioCVT.3
#+DESCRIPTION: Linux manpage for SDL_BuildAudioCVT.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_BuildAudioCVT - Initializes a SDL_AudioCVT structure for conversion

* SYNOPSIS
*#include "SDL.h"*

*int SDL_BuildAudioCVT*(*SDL_AudioCVT *cvt, Uint16 src_format, Uint8
src_channels, int src_rate, Uint16 dst_format, Uint8 dst_channels, int
dst_rate*);

* DESCRIPTION
Before an *SDL_AudioCVT* structure can be used to convert audio data it
must be initialized with source and destination information.

*src_format* and *dst_format* are the source and destination format of
the conversion. (For information on audio formats see * SDL_AudioSpec*).
*src_channels* and *dst_channels* are the number of channels in the
source and destination formats. Finally, *src_rate* and *dst_rate* are
the frequency or samples-per-second of the source and destination
formats. Once again, see *SDL_AudioSpec*.

* RETURN VALUES
Returns *-1* if the filter could not be built or 1 if it could.

* EXAMPLES
See *SDL_ConvertAudio*.

* SEE ALSO
*SDL_ConvertAudio*, *SDL_AudioCVT*
