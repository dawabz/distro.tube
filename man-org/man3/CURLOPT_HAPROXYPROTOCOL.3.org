#+TITLE: Manpages - CURLOPT_HAPROXYPROTOCOL.3
#+DESCRIPTION: Linux manpage for CURLOPT_HAPROXYPROTOCOL.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_HAPROXYPROTOCOL - send HAProxy PROXY protocol v1 header

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HAPROXYPROTOCOL, long
haproxy_protocol);

* DESCRIPTION
A long parameter set to 1 tells the library to send an HAProxy PROXY
protocol v1 header at beginning of the connection. The default action is
not to send this header.

This option is primarily useful when sending test requests to a service
that expects this header.

Most applications do not need this option.

* DEFAULT
0, do not send any HAProxy PROXY protocol header

* PROTOCOLS
HTTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    CURLcode ret;
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    curl_easy_setopt(curl, CURLOPT_HAPROXYPROTOCOL, 1L);
    ret = curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Along with HTTP. Added in 7.60.0.

* RETURN VALUE
Returns CURLE_OK if HTTP is enabled, and CURLE_UNKNOWN_OPTION if not.

* SEE ALSO
*CURLOPT_PROXY*(3),
