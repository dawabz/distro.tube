#+TITLE: Manpages - XResizeWindow.3
#+DESCRIPTION: Linux manpage for XResizeWindow.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XResizeWindow.3 is found in manpage for: [[../man3/XConfigureWindow.3][man3/XConfigureWindow.3]]