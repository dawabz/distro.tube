#+TITLE: Manpages - WWW_AUR_UserAgent.3pm
#+DESCRIPTION: Linux manpage for WWW_AUR_UserAgent.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
WWW::AUR::UserAgent - Default HTTP UserAgent object for requests.

* DESCRIPTION
This is the default user agent for making HTTP requests. By default this
class ISA LWP::UserAgent object with all methods provided by
LWP::UserAgent. Maybe in the future I will fancy it up a bit more in
order to use AnyEvent and concurrent requests if I need to.

The only difference from LWP::UserAgent is that the 'useragent' string
parameter is set to WWW::AUR/v$VERSION where =$VERSION= is equal to
=$WWW::AUR::VERSION=.

* OBJECT METHODS
Right now the methods are the same as LWP::UserAgent.

* CLASS METHODS
This class also contains the =InitTLS= class method to load the modules
required for SSL/TLS https connections. This is used by WWW::AUR::Login
and WWW::AUR::RPC. =InitTLS= takes no parameters. If TLS cannot be
loaded (for example, if LWP::Protocol::https fails to load) then an
error message is croaked.

* SEE ALSO
WWW::AUR

* AUTHOR
Justin Davis, =<juster at cpan dot org>=

* BUGS
Please email me any bugs you find. I will try to fix them as quick as I
can.

* SUPPORT
Send me an email if you have any questions or need help.

* LICENSE AND COPYRIGHT
Copyright 2014 Justin Davis.

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.
