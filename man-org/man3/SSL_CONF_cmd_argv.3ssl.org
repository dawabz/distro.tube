#+TITLE: Manpages - SSL_CONF_cmd_argv.3ssl
#+DESCRIPTION: Linux manpage for SSL_CONF_cmd_argv.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_CONF_cmd_argv - SSL configuration command line processing

* SYNOPSIS
#include <openssl/ssl.h> int SSL_CONF_cmd_argv(SSL_CONF_CTX *cctx, int
*pargc, char ***pargv);

* DESCRIPTION
The function *SSL_CONF_cmd_argv()* processes at most two command line
arguments from *pargv* and *pargc*. The values of *pargv* and *pargc*
are updated to reflect the number of command options processed. The
*pargc* argument can be set to *NULL* if it is not used.

* RETURN VALUES
*SSL_CONF_cmd_argv()* returns the number of command arguments processed:
0, 1, 2 or a negative error code.

If -2 is returned then an argument for a command is missing.

If -1 is returned the command is recognised but couldn't be processed
due to an error: for example a syntax error in the argument.

* SEE ALSO
*SSL_CONF_CTX_new* (3), *SSL_CONF_CTX_set_flags* (3),
*SSL_CONF_CTX_set1_prefix* (3), *SSL_CONF_CTX_set_ssl_ctx* (3),
*SSL_CONF_cmd* (3)

* HISTORY
These functions were added in OpenSSL 1.0.2.

* COPYRIGHT
Copyright 2012-2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
