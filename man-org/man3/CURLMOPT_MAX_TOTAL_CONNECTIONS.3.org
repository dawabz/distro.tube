#+TITLE: Manpages - CURLMOPT_MAX_TOTAL_CONNECTIONS.3
#+DESCRIPTION: Linux manpage for CURLMOPT_MAX_TOTAL_CONNECTIONS.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLMOPT_MAX_TOTAL_CONNECTIONS - max simultaneously open connections

* SYNOPSIS
#include <curl/curl.h>

CURLMcode curl_multi_setopt(CURLM *handle,
CURLMOPT_MAX_TOTAL_CONNECTIONS, long amount);

* DESCRIPTION
Pass a long for the *amount*. The set number will be used as the maximum
number of simultaneously open connections in total using this multi
handle. For each new session, libcurl will open a new connection up to
the limit set by /CURLMOPT_MAX_TOTAL_CONNECTIONS(3)/. When the limit is
reached, the sessions will be pending until there are available
connections. If /CURLMOPT_PIPELINING(3)/ is enabled, libcurl will try to
use multiplexing if the host is capable of it.

When more transfers are added to the multi handle than what can be
performed due to the set limit, they will be queued up waiting for their
chance. When that happens, the /CURLOPT_TIMEOUT_MS(3)/ timeout will be
counted inclusive of the waiting time, meaning that if you set a too
narrow timeout in such a case the transfer might never even start before
it times out.

Even in the queued up situation, the /CURLOPT_CONNECTTIMEOUT_MS(3)/
timeout is however treated as a per-connect timeout.

* DEFAULT
The default value is 0, which means that there is no limit. It is then
simply controlled by the number of easy handles added.

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURLM *m = curl_multi_init();
  /* never do more than 15 connections */
  curl_multi_setopt(m, CURLMOPT_MAX_TOTAL_CONNECTIONS, 15L);
#+end_example

* AVAILABILITY
Added in 7.30.0

* RETURN VALUE
Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLMOPT_MAXCONNECTS*(3), *CURLMOPT_MAX_HOST_CONNECTIONS*(3),
