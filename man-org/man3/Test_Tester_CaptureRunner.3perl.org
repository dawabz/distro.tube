#+TITLE: Manpages - Test_Tester_CaptureRunner.3perl
#+DESCRIPTION: Linux manpage for Test_Tester_CaptureRunner.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test::Tester::CaptureRunner - Help testing test modules built with
Test::Builder

* DESCRIPTION
This stuff if needed to allow me to play with other ways of monitoring
the test results.

* AUTHOR
Copyright 2003 by Fergal Daly <fergal@esatclear.ie>.

* LICENSE
Under the same license as Perl itself

See http://www.perl.com/perl/misc/Artistic.html
