#+TITLE: Manpages - netsnmp_pdu_api.3
#+DESCRIPTION: Linux manpage for netsnmp_pdu_api.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
snmp_pdu_create, snmp_clone_pdu, snmp_fix_pdu, snmp_free_pdu -
netsnmp_pdu_api functions

* SYNOPSIS
*#include <net-snmp/pdu_api.h>*

*netsnmp_pdu *snmp_pdu_create( int*/type/*);*

*netsnmp_pdu *snmp_clone_pdu( netsnmp_pdu**/pdu/*);*

*netsnmp_pdu *snmp_fix_pdu( netsnmp_pdu**/pdu/*, int */idx/*);*

*netsnmp_pdu *snmp_free_pdu( netsnmp_pdu**/pdu/*);*

* DESCRIPTION
These functions deal with SNMP request structures.

*snmp_pdu_create*

*snmp_clone_pdu*

*snmp_fix_pdu*

*snmp_free_pdu*

* SEE ALSO
netsnmp_varbind_api(3), netsnmp_session_api(3)
