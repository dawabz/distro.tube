#+TITLE: Manpages - MPI_File_set_size.3
#+DESCRIPTION: Linux manpage for MPI_File_set_size.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_File_set_size* - Resizes a file (collective).

* SYNTAX
#+begin_example
#+end_example

* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_File_set_size(MPI_File fh, MPI_Offset size)
#+end_example

* Fortran Syntax (see FORTRAN 77 NOTES)
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_FILE_SET_SIZE(FH, SIZE, IERROR)
  	INTEGER	FH, IERROR
  	INTEGER(KIND=MPI_OFFSET_KIND)	SIZE
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_File_set_size(fh, size, ierror)
  	TYPE(MPI_File), INTENT(IN) :: fh
  	INTEGER(KIND=MPI_OFFSET_KIND), INTENT(IN) :: size
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  void MPI::File::Set_size(MPI::Offset size)
#+end_example

* INPUT PARAMETERS
- fh :: File handle (handle).

- size :: Size to truncate or expand file (integer).

* OUTPUT PARAMETER
- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
MPI_File_set_size resizes the file associated with the file handle /fh,/
truncating UNIX files as necessary. MPI_File_set_size is collective; all
processes in the group must pass identical values for size.

When using MPI_File_set_size on a UNIX file, if /size/ is larger than
the current file size, the file size becomes /size/. If /size/ is
smaller than the current file size, the file is truncated at the
position defined by /size/ (from the beginning of the file and measured
in bytes). Regions of the file which have been previously written are
unaffected.

MPI_File_set_size does not affect the individual file pointers or the
shared file pointer.

Note that the actual amount of storage space cannot be allocated by
MPI_File_set_size. Use MPI_File_preallocate to accomplish this.

It is erroneous to call this function if MPI_MODE_SEQUENTIAL mode was
specified when the file was opened.

* FORTRAN 77 NOTES
The MPI standard prescribes portable Fortran syntax for the /SIZE/
argument only for Fortran 90. FORTRAN 77 users may use the non-portable
syntax

#+begin_example
       INTEGER*MPI_OFFSET_KIND SIZE
#+end_example

where MPI_OFFSET_KIND is a constant defined in mpif.h and gives the
length of the declared integer in bytes.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. For MPI I/O function errors, the default error handler is set to
MPI_ERRORS_RETURN. The error handler may be changed with
MPI_File_set_errhandler; the predefined error handler
MPI_ERRORS_ARE_FATAL may be used to make I/O errors fatal. Note that MPI
does not guarantee that an MPI program can continue past an error.
