#+TITLE: Manpages - xcb_xfixes_get_cursor_image.3
#+DESCRIPTION: Linux manpage for xcb_xfixes_get_cursor_image.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_xfixes_get_cursor_image -

* SYNOPSIS
*#include <xcb/xfixes.h>*

** Request function
xcb_xfixes_get_cursor_image_cookie_t
*xcb_xfixes_get_cursor_image*(xcb_connection_t */conn/,

** Reply datastructure
#+begin_example

  typedef struct xcb_xfixes_get_cursor_image_reply_t {
      uint8_t  response_type;
      uint8_t  pad0;
      uint16_t sequence;
      uint32_t length;
      int16_t  x;
      int16_t  y;
      uint16_t width;
      uint16_t height;
      uint16_t xhot;
      uint16_t yhot;
      uint32_t cursor_serial;
      uint8_t  pad1[8];
  } xcb_xfixes_get_cursor_image_reply_t;
#+end_example

** Reply function
xcb_xfixes_get_cursor_image_reply_t
**xcb_xfixes_get_cursor_image_reply*(xcb_connection_t */conn/,
xcb_xfixes_get_cursor_image_cookie_t /cookie/, xcb_generic_error_t
**/e/);

** Reply accessors
uint32_t **xcb_xfixes_get_cursor_image_cursor_image*(const
xcb_xfixes_get_cursor_image_request_t */reply/);

int *xcb_xfixes_get_cursor_image_cursor_image_length*(const
xcb_xfixes_get_cursor_image_reply_t */reply/);

xcb_generic_iterator_t
*xcb_xfixes_get_cursor_image_cursor_image_end*(const
xcb_xfixes_get_cursor_image_reply_t */reply/);\\

* REQUEST ARGUMENTS
- conn :: The XCB connection to X11.

* REPLY FIELDS
- response_type :: The type of this reply, in this case
  /XCB_XFIXES_GET_CURSOR_IMAGE/. This field is also present in the
  /xcb_generic_reply_t/ and can be used to tell replies apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- length :: The length of the reply, in words (a word is 4 bytes).

24. TODO: NOT YET DOCUMENTED.

25. TODO: NOT YET DOCUMENTED.

- width :: TODO: NOT YET DOCUMENTED.

- height :: TODO: NOT YET DOCUMENTED.

- xhot :: TODO: NOT YET DOCUMENTED.

- yhot :: TODO: NOT YET DOCUMENTED.

- cursor_serial :: TODO: NOT YET DOCUMENTED.

* DESCRIPTION
* RETURN VALUE
Returns an /xcb_xfixes_get_cursor_image_cookie_t/. Errors have to be
handled when calling the reply function
/xcb_xfixes_get_cursor_image_reply/.

If you want to handle errors in the event loop instead, use
/xcb_xfixes_get_cursor_image_unchecked/. See *xcb-requests(3)* for
details.

* ERRORS
This request does never generate any errors.

* SEE ALSO
* AUTHOR
Generated from xfixes.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
