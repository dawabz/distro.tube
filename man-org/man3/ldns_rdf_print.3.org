#+TITLE: Manpages - ldns_rdf_print.3
#+DESCRIPTION: Linux manpage for ldns_rdf_print.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_rdf_new, ldns_rdf_clone, ldns_rdf_new_frm_data,
ldns_rdf_new_frm_str, ldns_rdf_new_frm_fp, ldns_rdf_free,
ldns_rdf_deep_free, ldns_rdf_print - ldns_rdf creation, destruction and
printing

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_rdf* ldns_rdf_new(ldns_rdf_type type, size_t size, void *data);

ldns_rdf* ldns_rdf_clone(const ldns_rdf *rd);

ldns_rdf* ldns_rdf_new_frm_data(ldns_rdf_type type, size_t size, const
void *data);

ldns_rdf* ldns_rdf_new_frm_str(ldns_rdf_type type, const char *str);

ldns_status ldns_rdf_new_frm_fp(ldns_rdf **r, ldns_rdf_type type, FILE
*fp);

void ldns_rdf_free(ldns_rdf *rd);

void ldns_rdf_deep_free(ldns_rdf *rd);

void ldns_rdf_print(FILE *output, const ldns_rdf *rdf);

* DESCRIPTION
/ldns_rdf_new/() allocates a new rdf structure and fills it. This
function DOES NOT copy the contents from the buffer, unlinke
ldns_rdf_new_frm_data() .br *type*: type of the rdf .br *size*: size of
the buffer .br *data*: pointer to the buffer to be copied .br Returns
the new rdf structure or NULL on failure

/ldns_rdf_clone/() clones a rdf structure. The data is copied. .br *rd*:
rdf to be copied .br Returns a new rdf structure

/ldns_rdf_new_frm_data/() allocates a new rdf structure and fills it.
This function _does_ copy the contents from the buffer, unlinke
ldns_rdf_new() .br *type*: type of the rdf .br *size*: size of the
buffer .br *data*: pointer to the buffer to be copied .br Returns the
new rdf structure or NULL on failure

/ldns_rdf_new_frm_str/() creates a new rdf from a string. .br *type*:
type to use .br *str*: string to use .br Returns ldns_rdf* or NULL in
case of an error

/ldns_rdf_new_frm_fp/() creates a new rdf from a file containing a
string. .br *r*: the new rdf .br *type*: type to use .br *fp*: the file
pointer to use .br Returns LDNS_STATUS_OK or the error

/ldns_rdf_free/() frees a rdf structure, leaving the data pointer
intact. .br *rd*: the pointer to be freed .br Returns void

/ldns_rdf_deep_free/() frees a rdf structure _and_ frees the data. rdf
should be created with _new_frm_data .br *rd*: the rdf structure to be
freed .br Returns void

/ldns_rdf_print/() Prints the data in the rdata field to the given file
stream (in presentation format)

.br *output*: the file stream to print to .br *rdf*: the rdata field to
print .br Returns void

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_rdf/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*,
*RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
