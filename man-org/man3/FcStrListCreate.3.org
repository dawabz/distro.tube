#+TITLE: Manpages - FcStrListCreate.3
#+DESCRIPTION: Linux manpage for FcStrListCreate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcStrListCreate - create a string iterator

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcStrList * FcStrListCreate (FcStrSet */set/*);*

* DESCRIPTION
Creates an iterator to list the strings in /set/.
