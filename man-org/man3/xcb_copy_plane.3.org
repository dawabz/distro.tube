#+TITLE: Manpages - xcb_copy_plane.3
#+DESCRIPTION: Linux manpage for xcb_copy_plane.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_copy_plane -

* SYNOPSIS
*#include <xcb/xproto.h>*

** Request function
xcb_void_cookie_t *xcb_copy_plane*(xcb_connection_t */conn/,
xcb_drawable_t /src_drawable/, xcb_drawable_t /dst_drawable/,
xcb_gcontext_t /gc/, int16_t /src_x/, int16_t /src_y/, int16_t /dst_x/,
int16_t /dst_y/, uint16_t /width/, uint16_t /height/, uint32_t
/bit_plane/);\\

* REQUEST ARGUMENTS
- conn :: The XCB connection to X11.

- src_drawable :: TODO: NOT YET DOCUMENTED.

- dst_drawable :: TODO: NOT YET DOCUMENTED.

- gc :: TODO: NOT YET DOCUMENTED.

- src_x :: TODO: NOT YET DOCUMENTED.

- src_y :: TODO: NOT YET DOCUMENTED.

- dst_x :: TODO: NOT YET DOCUMENTED.

- dst_y :: TODO: NOT YET DOCUMENTED.

- width :: TODO: NOT YET DOCUMENTED.

- height :: TODO: NOT YET DOCUMENTED.

- bit_plane :: TODO: NOT YET DOCUMENTED.

* DESCRIPTION
* RETURN VALUE
Returns an /xcb_void_cookie_t/. Errors (if any) have to be handled in
the event loop.

If you want to handle errors directly with /xcb_request_check/ instead,
use /xcb_copy_plane_checked/. See *xcb-requests(3)* for details.

* ERRORS
This request does never generate any errors.

* SEE ALSO
* AUTHOR
Generated from xproto.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
