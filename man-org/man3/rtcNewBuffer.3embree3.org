#+TITLE: Manpages - rtcNewBuffer.3embree3
#+DESCRIPTION: Linux manpage for rtcNewBuffer.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcNewBuffer - creates a new data buffer
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  RTCBuffer rtcNewBuffer(
    RTCDevice device,
    size_t byteSize
  );
#+end_example

** DESCRIPTION
The =rtcNewBuffer= function creates a new data buffer object of
specified size in bytes (=byteSize= argument) that is bound to the
specified device (=device= argument). The buffer object is reference
counted with an initial reference count of 1. The returned buffer object
can be released using the =rtcReleaseBuffer= API call. The specified
number of bytes are allocated at buffer construction time and
deallocated when the buffer is destroyed.

#+begin_example
#+end_example

** EXIT STATUS
On failure =NULL= is returned and an error code is set that can be
queried using =rtcGetDeviceError=.

** SEE ALSO
[rtcRetainBuffer], [rtcReleaseBuffer]
