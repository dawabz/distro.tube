#+TITLE: Manpages - CURLINFO_PRIVATE.3
#+DESCRIPTION: Linux manpage for CURLINFO_PRIVATE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLINFO_PRIVATE - get the private pointer

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_PRIVATE, char
**private);

* DESCRIPTION
Pass a pointer to a char pointer to receive the pointer to the private
data associated with the curl handle (set with the
/CURLOPT_PRIVATE(3)/). Please note that for internal reasons, the value
is returned as a char pointer, although effectively being a 'void *'.

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    void *pointer = 0x2345454;
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

    /* set the private pointer */
    curl_easy_setopt(curl, CURLOPT_PRIVATE, pointer);
    ret = curl_easy_perform(curl);

    /* extract the private pointer again */
    ret = curl_easy_getinfo(curl, CURLINFO_PRIVATE, &pointer);

    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.10.3

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*curl_easy_getinfo*(3), *curl_easy_setopt*(3), *CURLOPT_PRIVATE*(3),
