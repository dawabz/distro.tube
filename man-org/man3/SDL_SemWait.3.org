#+TITLE: Manpages - SDL_SemWait.3
#+DESCRIPTION: Linux manpage for SDL_SemWait.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_SemWait - Lock a semaphore and suspend the thread if the semaphore
value is zero.

* SYNOPSIS
*#include "SDL.h"* #include "SDL_thread.h"

*int SDL_SemWait*(*SDL_sem *sem*);

* DESCRIPTION
*SDL_SemWait()* suspends the calling thread until either the semaphore
pointed to by *sem* has a positive value, the call is interrupted by a
signal or error. If the call is successful it will atomically decrement
the semaphore value.

After *SDL_SemWait()* is successful, the semaphore can be released and
its count atomically incremented by a successful call to /SDL_SemPost/.

* RETURN VALUE
Returns *0* if successful or *-1* if there was an error (leaving the
semaphore unchanged).

* EXAMPLES
#+begin_example
  if (SDL_SemWait(my_sem) == -1) {
          return WAIT_FAILED;
  }

  ...

  SDL_SemPost(my_sem);
#+end_example

* SEE ALSO
*SDL_CreateSemaphore*, *SDL_DestroySemaphore*, *SDL_SemTryWait*,
*SDL_SemWaitTimeout*, *SDL_SemPost*, *SDL_SemValue*
