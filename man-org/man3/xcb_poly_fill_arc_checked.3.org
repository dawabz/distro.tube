#+TITLE: Manpages - xcb_poly_fill_arc_checked.3
#+DESCRIPTION: Linux manpage for xcb_poly_fill_arc_checked.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_poly_fill_arc_checked.3 is found in manpage for: [[../man3/xcb_poly_fill_arc.3][man3/xcb_poly_fill_arc.3]]