#+TITLE: Manpages - aa_features.3
#+DESCRIPTION: Linux manpage for aa_features.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
aa_features - an opaque object representing a set of AppArmor kernel
features

aa_features_new - create a new aa_features object based on a path

aa_features_new_from_string - create a new aa_features object based on a
string

aa_features_new_from_kernel - create a new aa_features object based on
the current kernel

aa_features_ref - increments the ref count of an aa_features object

aa_features_unref - decrements the ref count and frees the aa_features
object when 0

aa_features_write_to_fd - write a string representation of an
aa_features object to a file descriptor

aa_features_write_to_file - write a string representation of an
aa_features object to a file

aa_features_is_equal - equality test for two aa_features objects

aa_features_supports - provides aa_features object support status

aa_features_id - provides unique identifier for an aa_features object

aa_features_value - the value associated with a given feature.

* SYNOPSIS
*#include <sys/apparmor.h>*

*typedef struct aa_features aa_features;*

*int aa_features_new(aa_features **features, int dirfd, const char
*path);*

*int aa_features_new_from_file(aa_features **features, int fd);*

*int aa_features_new_from_string(aa_features **features, const char
*string, size_t size);*

*int aa_features_new_from_kernel(aa_features **features);*

*aa_features *aa_features_ref(aa_features *features);*

*void aa_features_unref(aa_features *features);*

*int aa_features_write_to_fd(aa_features *features, int fd);*

*int aa_features_write_to_file(aa_features *features, int dirfd, const
char *path);*

*bool aa_features_is_equal(aa_features *features1, aa_features
*features2);*

*bool aa_features_supports(aa_features *features, const char *str);*

*char *aa_features_id(aa_features *features);*

*char *aa_features_value(aa_features *features, const char *str, size_t
*len);*

Link with *-lapparmor* when compiling.

* DESCRIPTION
The /aa_features/ object contains information about the AppArmor
features supported by a kernel. The feature support information is based
upon the files AppArmor represents in securityfs, which is typically
found at /sys/kernel/security/apparmor/features/. That information may
be parsed and turned into a string or flat file in order to represent a
set of features of a kernel that is not currently running.

The *aa_features_new()* function creates an /aa_features/ object based
upon a directory file descriptor and path. The /path/ can point to a
file or directory. See the *openat* (2) man page for examples of /dirfd/
and /path/. The allocated /features/ object must be freed using
*aa_features_unref()*.

The *aa_features_new_from_file()* function is similar except that it
accepts an open file as the argument. The allocated /features/ object
must be freed using *aa_features_unref()*.

The *aa_features_new_from_string()* function is similar except that it
accepts a NUL-terminated string representation of the AppArmor features
as the /string/ argument. The length of the features string, not
counting the NUL-terminator, must be specified as the /size/ argument.
The allocated /features/ object must be freed using
*aa_features_unref()*.

The *aa_features_new_from_kernel()* function creates an /aa_features/
object from the current running kernel. The allocated /features/ object
must be freed using *aa_features_unref()*.

*aa_features_ref()* increments the reference count on the /features/
object.

*aa_features_unref()* decrements the reference count on the /features/
object and releases all corresponding resources when the reference count
reaches zero.

The *aa_features_write_to_fd()* function writes a string representation
of the /features/ object to the file descriptor specified by the /fd/.

The *aa_features_write_to_file()* function writes a string
representation of the /features/ object to the file specified by the
/dirfd/ and /path/ combination.

*aa_features_is_equal()* can be used to detect if the /features1/ and
/features2/ objects are equal. The definition of equality is private to
libapparmor and may be changed in ways that do not break backward
compatibility.

The *aa_features_supports()* function can be used to query the
/features/ object to determine if a feature is supported. The /str/
argument should be equal to the path, relative to the apparmor/features/
directory of securityfs, of the feature to query. For example, to test
if policy version 6 is supported, /str/ would be policy/versions/v6.

The *aa_features_id()* function returns a string representation of an
identifier that can be used to uniquely identify an /aa_features/
object. The mechanism for generating the string representation is
internal to libapparmor and subject to change but an example
implementation is applying a hash function to the features string.

* RETURN VALUE
The *aa_features_new()* family of functions return 0 on success and
/*features/ will point to an /aa_features/ object that must be freed by
*aa_features_unref()*. -1 is returned on error, with errno set
appropriately, and /*features/ will be set to NULL.

*aa_features_ref()* returns the value of /features/.

*aa_features_write_to_file()* returns 0 on success. -1 is returned on
error, with errno set appropriately.

*aa_features_is_equal()* returns true if /features1/ and /features2/ are
equal and false if they are not equal.

*aa_features_supports()* returns true if the feature represented by
/str/ is supported and false if it is not supported.

*aa_features_id()* returns a string identifying /features/ which must be
freed by the caller. NULL is returned on error, with errno set
appropriately.

*aa_features_value()* returns a null terminated string with is
associated length which must be freed by the caller. NULL is returned on
error, with errno set to ENOENT if the feature was not found, ENODIR if
the specified feature does not resolve to a leaf feature.

* ERRORS
The errno value will be set according to the underlying error in the
/aa_features/ family of functions that return -1 or NULL on error.

* NOTES
The *aa_features_id()* function can be found in libapparmor version
2.13. All the other aa_feature functions described above are present in
libapparmor version 2.10.

*aa_features_unref()* saves the value of errno when called and restores
errno before exiting in libapparmor version 2.12 and newer.

* BUGS
None known. If you find any, please report them at
<https://gitlab.com/apparmor/apparmor/-/issues>.

* SEE ALSO
*openat* (2), *aa-features-abi* (1) and <https://wiki.apparmor.net>.
