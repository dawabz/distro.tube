#+TITLE: Manpages - dispatch_api.3
#+DESCRIPTION: Linux manpage for dispatch_api.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The following is a brief summary of some of the common design patterns
to consider when designing and implementing API in terms of dispatch
queues and blocks.

A general recommendation is to allow both a callback block and target
dispatch queue to be specified. This gives the application the greatest
flexibility in handling asynchronous events.

It's also recommended that interfaces take only a single block as the
last parameter. This is both for consistency across projects, as well as
the visual aesthetics of multiline blocks that are declared inline. The
dispatch queue to which the block will be submitted should immediately
precede the block argument (second-to-last argument). For example:

read_async(file, callback_queue, ^{ printf("received callback.\n"); });

When function pointer alternatives to interfaces that take blocks are
provided, the argument order of the function signature should be
identical to the block variant; with the exception that the block
argument is replaced with a context pointer, and a new last parameter is
added, which is the function to call.

The function based callback should pass the context pointer as the first
argument, and the subsequent arguments should be identical to the block
based variant (albeit offset by one in order).

It is also important to use consistent naming. The dispatch API, for
example, uses the suffix "_f" for function based variants.
