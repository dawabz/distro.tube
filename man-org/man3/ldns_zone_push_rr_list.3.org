#+TITLE: Manpages - ldns_zone_push_rr_list.3
#+DESCRIPTION: Linux manpage for ldns_zone_push_rr_list.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_zone_push_rr, ldns_zone_push_rr_list - add rr's to a ldns_zone

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

bool ldns_zone_push_rr(ldns_zone *z, ldns_rr *rr);

bool ldns_zone_push_rr_list(ldns_zone *z, const ldns_rr_list *list);

* DESCRIPTION
/ldns_zone_push_rr/() push an single rr to a zone structure. This
function use pointer copying, so the rr_list structure inside z is
modified! .br *z*: the zone to add to .br *rr*: the rr to add .br
Returns a true on succes otherwise falsed

/ldns_zone_push_rr_list/() push an rrlist to a zone structure. This
function use pointer copying, so the rr_list structure inside z is
modified! .br *z*: the zone to add to .br *list*: the list to add .br
Returns a true on succes otherwise falsed

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_zone/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*,
*RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
