#+TITLE: Manpages - XSetArcMode.3
#+DESCRIPTION: Linux manpage for XSetArcMode.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XSetArcMode, XSetSubwindowMode, XSetGraphicsExposure - GC convenience
routines

* SYNTAX
int XSetArcMode ( Display */display/ , GC /gc/ , int /arc_mode/ );

int XSetSubwindowMode ( Display */display/ , GC /gc/ , int
/subwindow_mode/ );

int XSetGraphicsExposures ( Display */display/ , GC /gc/ , Bool
/graphics_exposures/ );

* ARGUMENTS
- arc_mode :: Specifies the arc mode. You can pass *ArcChord* or
  *ArcPieSlice*.

- display :: Specifies the connection to the X server.

- gc :: Specifies the GC.

- graphics_exposures :: Specifies a Boolean value that indicates whether
  you want *GraphicsExpose* and *NoExpose* events to be reported when
  calling *XCopyArea* and *XCopyPlane* with this GC.

- subwindow_mode :: Specifies the subwindow mode. You can pass
  *ClipByChildren* or *IncludeInferiors*.

* DESCRIPTION
The *XSetArcMode* function sets the arc mode in the specified GC.

*XSetArcMode* can generate *BadAlloc*, *BadGC*, and *BadValue* errors.

The *XSetSubwindowMode* function sets the subwindow mode in the
specified GC.

*XSetSubwindowMode* can generate *BadAlloc*, *BadGC*, and *BadValue*
errors.

The *XSetGraphicsExposures* function sets the graphics-exposures flag in
the specified GC.

*XSetGraphicsExposures* can generate *BadAlloc*, *BadGC*, and *BadValue*
errors.

* DIAGNOSTICS
- *BadAlloc* :: The server failed to allocate the requested resource or
  server memory.

- *BadGC* :: A value for a GContext argument does not name a defined
  GContext.

- *BadValue* :: Some numeric value falls outside the range of values
  accepted by the request. Unless a specific range is specified for an
  argument, the full range defined by the argument's type is accepted.
  Any argument defined as a set of alternatives can generate this error.

* SEE ALSO
XCopyArea(3), XCreateGC(3), XQueryBestSize(3), XSetClipOrigin(3),
XSetFillStyle(3), XSetFont(3), XSetLineAttributes(3), XSetState(3),
XSetTile(3)\\
/Xlib - C Language X Interface/
