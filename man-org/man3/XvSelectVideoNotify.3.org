#+TITLE: Manpages - XvSelectVideoNotify.3
#+DESCRIPTION: Linux manpage for XvSelectVideoNotify.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XvSelectVideoNotify - enable or disable VideoNotify events

* SYNOPSIS
*#include <X11/extensions/Xvlib.h>*

#+begin_example
  int XvSelectVideoNotify(Display *dpy, Drawable drawable, Bool onoff);
#+end_example

* ARGUMENTS
- dpy :: Specifies the connection to the X server.

- drawable :: Defines the drawable in which video activity is to be
  reported.

- onoff :: Selects whether video notification is enabled or disabled.

* DESCRIPTION
*XvSelectVideoNotify*(3) enables or disables *XvVideoNotify*(3) events
to be reported for video activity in a drawable.

* RETURN VALUES
- [Success] :: Returned if *XvSelectVideoNotify*(3) completed
  successfully.

- [XvBadExtension] :: Returned if the Xv extension is unavailable.

- [XvBadAlloc] :: Returned if *XvSelectVideoNotify*(3) failed to
  allocate memory to process the request.

* DIAGNOSTICS
- [BadDrawable] :: Generated if the requested drawable does not exist.

* SEE ALSO
*XvVideoNotify*(3)
