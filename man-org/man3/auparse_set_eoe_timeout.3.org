#+TITLE: Manpages - auparse_set_eoe_timeout.3
#+DESCRIPTION: Linux manpage for auparse_set_eoe_timeout.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_set_eoe_timeout - set the end of event timeout value

* SYNOPSIS
*#include <auparse.h>*

int auparse_set_eoe_timeout(time_t new_tmo)

* DESCRIPTION
auparse_set_eoe_timeout is used to set the end of event timeout value
(seconds). The value should be a positive integer. If this function is
called, it overrides any setting in /etc/auditd.conf. The function
should be called after the /auparse_init()/ function call.

For details on the timeout, see the *end_of_event_timeout* configuration
item description in /auditd.conf(5)/.

* RETURN VALUE
Returns -1 if an error occurs; otherwise, 0 for success.

* SEE ALSO
*auparse_init*(3). *auditd.conf*(8).

* AUTHOR
Steve Grubb
