#+TITLE: Manpages - xcb_shm_completion_event_t.3
#+DESCRIPTION: Linux manpage for xcb_shm_completion_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_shm_completion_event_t -

* SYNOPSIS
*#include <xcb/shm.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_shm_completion_event_t {
      uint8_t        response_type;
      uint8_t        pad0;
      uint16_t       sequence;
      xcb_drawable_t drawable;
      uint16_t       minor_event;
      uint8_t        major_event;
      uint8_t        pad1;
      xcb_shm_seg_t  shmseg;
      uint32_t       offset;
  } xcb_shm_completion_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_SHM_COMPLETION/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- drawable :: NOT YET DOCUMENTED.

- minor_event :: NOT YET DOCUMENTED.

- major_event :: NOT YET DOCUMENTED.

- shmseg :: NOT YET DOCUMENTED.

- offset :: NOT YET DOCUMENTED.

* DESCRIPTION
* SEE ALSO
* AUTHOR
Generated from shm.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
