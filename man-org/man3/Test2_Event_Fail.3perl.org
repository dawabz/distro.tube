#+TITLE: Manpages - Test2_Event_Fail.3perl
#+DESCRIPTION: Linux manpage for Test2_Event_Fail.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test2::Event::Fail - Event for a simple failed assertion

* DESCRIPTION
This is an optimal representation of a failed assertion.

* SYNOPSIS
use Test2::API qw/context/; sub fail { my ($name) = @_; my $ctx =
context(); $ctx->fail($name); $ctx->release; }

* SOURCE
The source code repository for Test2 can be found at
/http://github.com/Test-More/test-more//.

* MAINTAINERS
- Chad Granum <exodist@cpan.org> :: 

* AUTHORS
- Chad Granum <exodist@cpan.org> :: 

* COPYRIGHT
Copyright 2020 Chad Granum <exodist@cpan.org>.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See /http://dev.perl.org/licenses//
