#+TITLE: Manpages - CURLOPT_USERAGENT.3
#+DESCRIPTION: Linux manpage for CURLOPT_USERAGENT.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_USERAGENT - HTTP user-agent header

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_USERAGENT, char *ua);

* DESCRIPTION
Pass a pointer to a null-terminated string as parameter. It will be used
to set the User-Agent: header in the HTTP request sent to the remote
server. This can be used to fool servers or scripts. You can also set
any custom header with /CURLOPT_HTTPHEADER(3)/.

The application does not have to keep the string around after setting
this option.

* DEFAULT
NULL, no User-Agent: header is used by default.

* PROTOCOLS
HTTP, HTTPS

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    curl_easy_setopt(curl, CURLOPT_USERAGENT, "Dark Secret Ninja/1.0");

    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
As long as HTTP is supported

* RETURN VALUE
Returns CURLE_OK if HTTP is supported, CURLE_UNKNOWN_OPTION if not, or
CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_REFERER*(3), *CURLOPT_HTTPHEADER*(3),
