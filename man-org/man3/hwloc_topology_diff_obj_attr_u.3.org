#+TITLE: Manpages - hwloc_topology_diff_obj_attr_u.3
#+DESCRIPTION: Linux manpage for hwloc_topology_diff_obj_attr_u.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwloc_topology_diff_obj_attr_u

* SYNOPSIS
\\

=#include <diff.h>=

** Data Structures
struct *hwloc_topology_diff_obj_attr_generic_s*\\

struct *hwloc_topology_diff_obj_attr_string_s*\\

struct *hwloc_topology_diff_obj_attr_uint64_s*\\

** Data Fields
struct
*hwloc_topology_diff_obj_attr_u::hwloc_topology_diff_obj_attr_generic_s*
*generic*\\

struct
*hwloc_topology_diff_obj_attr_u::hwloc_topology_diff_obj_attr_uint64_s*
*uint64*\\

struct
*hwloc_topology_diff_obj_attr_u::hwloc_topology_diff_obj_attr_string_s*
*string*\\

* Detailed Description
One object attribute difference.

* Field Documentation
** struct
*hwloc_topology_diff_obj_attr_u::hwloc_topology_diff_obj_attr_generic_s*
hwloc_topology_diff_obj_attr_u::generic
** struct
*hwloc_topology_diff_obj_attr_u::hwloc_topology_diff_obj_attr_string_s*
hwloc_topology_diff_obj_attr_u::string
** struct
*hwloc_topology_diff_obj_attr_u::hwloc_topology_diff_obj_attr_uint64_s*
hwloc_topology_diff_obj_attr_u::uint64
* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
