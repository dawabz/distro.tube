#+TITLE: Manpages - ldns_rr_mx_exchange.3
#+DESCRIPTION: Linux manpage for ldns_rr_mx_exchange.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_rr_mx_preference, ldns_rr_mx_exchange - get MX RR rdata fields

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_rdf* ldns_rr_mx_preference(const ldns_rr *r);

ldns_rdf* ldns_rr_mx_exchange(const ldns_rr *r);

* DESCRIPTION
/ldns_rr_mx_preference/() returns the mx pref. of a LDNS_RR_TYPE_MX rr
.br *r*: the resource record .br Returns a ldns_rdf* with the preference
or NULL on failure

/ldns_rr_mx_exchange/() returns the mx host of a LDNS_RR_TYPE_MX rr .br
*r*: the resource record .br Returns a ldns_rdf* with the name of the MX
host or NULL on failure

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
*perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*, *RFC4034* and
*RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
