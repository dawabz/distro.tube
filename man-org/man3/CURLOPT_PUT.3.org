#+TITLE: Manpages - CURLOPT_PUT.3
#+DESCRIPTION: Linux manpage for CURLOPT_PUT.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_PUT - make an HTTP PUT request

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PUT, long put);

* DESCRIPTION
A parameter set to 1 tells the library to use HTTP PUT to transfer data.
The data should be set with /CURLOPT_READDATA(3)/ and
/CURLOPT_INFILESIZE(3)/.

This option is *deprecated* since version 7.12.1. Use
/CURLOPT_UPLOAD(3)/!

* DEFAULT
0, disabled

* PROTOCOLS
HTTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    /* we want to use our own read function */
    curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);

    /* enable PUT */
    curl_easy_setopt(curl, CURLOPT_PUT, 1L);

    /* specify target */
    curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/dir/to/newfile");

    /* now specify which pointer to pass to our callback */
    curl_easy_setopt(curl, CURLOPT_READDATA, hd_src);

    /* Set the size of the file to upload */
    curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, (curl_off_t)fsize);

    /* Now run off and do what you have been told! */
    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Deprecated since 7.12.1. Do not use.

* RETURN VALUE
Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

* SEE ALSO
*CURLOPT_UPLOAD*(3), *CURLOPT_HTTPGET*(3),
