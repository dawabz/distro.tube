#+TITLE: Manpages - SDL_JoyAxisEvent.3
#+DESCRIPTION: Linux manpage for SDL_JoyAxisEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_JoyAxisEvent - Joystick axis motion event structure

* STRUCTURE DEFINITION
#+begin_example
  typedef struct{
    Uint8 type;
    Uint8 which;
    Uint8 axis;
    Sint16 value;
  } SDL_JoyAxisEvent;
#+end_example

* STRUCTURE DATA
- *type* :: *SDL_JOYAXISMOTION*

- *which* :: Joystick device index

- *axis* :: Joystick axis index

- *value* :: Axis value (range: -32768 to 32767)

* DESCRIPTION
*SDL_JoyAxisEvent* is a member of the *SDL_Event* union and is used when
an event of type *SDL_JOYAXISMOTION* is reported.

A *SDL_JOYAXISMOTION* event occurs when ever a user moves an axis on the
joystick. The field *which* is the index of the joystick that reported
the event and *axis* is the index of the axis (for a more detailed
explaination see the /Joystick section/). *value* is the current
position of the axis.

* SEE ALSO
*SDL_Event*, /Joystick Functions/, *SDL_JoystickEventState*,
*SDL_JoystickGetAxis*
