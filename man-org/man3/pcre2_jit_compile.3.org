#+TITLE: Manpages - pcre2_jit_compile.3
#+DESCRIPTION: Linux manpage for pcre2_jit_compile.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  int pcre2_jit_compile(pcre2_code *code, uint32_t options);
#+end_example

* DESCRIPTION
This function requests JIT compilation, which, if the just-in-time
compiler is available, further processes a compiled pattern into machine
code that executes much faster than the *pcre2_match()* interpretive
matching function. Full details are given in the *pcre2jit*
documentation.

The first argument is a pointer that was returned by a successful call
to *pcre2_compile()*, and the second must contain one or more of the
following bits:

PCRE2_JIT_COMPLETE compile code for full matching PCRE2_JIT_PARTIAL_SOFT
compile code for soft partial matching PCRE2_JIT_PARTIAL_HARD compile
code for hard partial matching

There is also an obsolete option called PCRE2_JIT_INVALID_UTF, which has
been superseded by the *pcre2_compile()* option PCRE2_MATCH_INVALID_UTF.
The old option is deprecated and may be removed in the future.

The yield of the function is 0 for success, or a negative error code
otherwise. In particular, PCRE2_ERROR_JIT_BADOPTION is returned if JIT
is not supported or if an unknown bit is set in /options/. The function
can also return PCRE2_ERROR_NOMEMORY if JIT is unable to allocate
executable memory for the compiler, even if it was because of a system
security restriction.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
