#+TITLE: Manpages - config_perror.3
#+DESCRIPTION: Linux manpage for config_perror.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about config_perror.3 is found in manpage for: [[../man3/netsnmp_config_api.3][man3/netsnmp_config_api.3]]