#+TITLE: Manpages - sd_id128_to_string.3
#+DESCRIPTION: Linux manpage for sd_id128_to_string.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_id128_to_string, sd_id128_from_string - Format or parse 128-bit IDs
as strings

* SYNOPSIS
#+begin_example
  #include <systemd/sd-id128.h>
#+end_example

*char *sd_id128_to_string(sd_id128_t */id/*, char */s/*[33]);*

*int sd_id128_from_string(const char **/s/*, sd_id128_t **/ret/*);*

* DESCRIPTION
*sd_id128_to_string()* formats a 128-bit ID as a character string. It
expects the ID and a string array capable of storing 33 characters. The
ID will be formatted as 32 lowercase hexadecimal digits and be
terminated by a *NUL* byte.

*sd_id128_from_string()* implements the reverse operation: it takes a 33
character string with 32 hexadecimal digits (either lowercase or
uppercase, terminated by *NUL*) and parses them back into a 128-bit ID
returned in /ret/. Alternatively, this call can also parse a
37-character string with a 128-bit ID formatted as RFC UUID. If /ret/ is
passed as *NULL* the function will validate the passed ID string, but
not actually return it in parsed form.

Note that when parsing 37 character UUIDs this is done strictly in Big
Endian byte order, i.e. according to *RFC4122*[1] Variant 1 rules, even
if the UUID encodes a different variant. This matches behaviour in
various other Linux userspace tools. Its probably wise to avoid UUIDs of
other variant types.

For more information about the "sd_id128_t" type see *sd-id128*(3). Note
that these calls operate the same way on all architectures, i.e. the
results do not depend on endianness.

When formatting a 128-bit ID into a string, it is often easier to use a
format string for *printf*(3). This is easily done using the
*SD_ID128_FORMAT_STR* and *SD_ID128_FORMAT_VAL()* macros. For more
information see *sd-id128*(3).

* RETURN VALUE
*sd_id128_to_string()* always succeeds and returns a pointer to the
string array passed in. *sd_id128_from_string()* returns 0 on success,
in which case /ret/ is filled in, or a negative errno-style error code.

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-id128*(3), *printf*(3)

* NOTES
-  1. :: RFC4122

  https://tools.ietf.org/html/rfc4122
