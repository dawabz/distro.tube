#+TITLE: Manpages - CURLOPT_CHUNK_END_FUNCTION.3
#+DESCRIPTION: Linux manpage for CURLOPT_CHUNK_END_FUNCTION.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_CHUNK_END_FUNCTION - callback after a transfer with FTP
wildcardmatch

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>

  long chunk_end_callback(void *ptr);

  CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CHUNK_END_FUNCTION,
                            chunk_end_callback);
#+end_example

* DESCRIPTION
Pass a pointer to your callback function, which should match the
prototype shown above.

This function gets called by libcurl as soon as a part of the stream has
been transferred (or skipped).

Return /CURL_CHUNK_END_FUNC_OK/ if everything is fine or
*CURL_CHUNK_END_FUNC_FAIL* to tell the lib to stop if some error
occurred.

* DEFAULT
NULL

* PROTOCOLS
FTP

* EXAMPLE
#+begin_example
  static long file_is_downloaded(struct callback_data *data)
  {
    if(data->output) {
      fclose(data->output);
      data->output = 0x0;
    }
    return CURL_CHUNK_END_FUNC_OK;
  }

  int main()
  {
    /* data for callback */
    struct callback_data callback_info;
    curl_easy_setopt(curl, CURLOPT_CHUNK_END_FUNCTION, file_is_downloaded);
    curl_easy_setopt(curl, CURLOPT_CHUNK_DATA, &callback_info);
  }
#+end_example

* AVAILABILITY
Added in 7.21.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_WILDCARDMATCH*(3), *CURLOPT_CHUNK_BGN_FUNCTION*(3),
