#+TITLE: Manpages - CURLINFO_HEADER_SIZE.3
#+DESCRIPTION: Linux manpage for CURLINFO_HEADER_SIZE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLINFO_HEADER_SIZE - get size of retrieved headers

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_HEADER_SIZE, long
*sizep);

* DESCRIPTION
Pass a pointer to a long to receive the total size of all the headers
received. Measured in number of bytes.

The total includes the size of any received headers suppressed by
/CURLOPT_SUPPRESS_CONNECT_HEADERS(3)/.

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    CURLcode res;
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
    res = curl_easy_perform(curl);
    if(res == CURLE_OK) {
      long size;
      res = curl_easy_getinfo(curl, CURLINFO_HEADER_SIZE, &size);
      if(!res)
        printf("Header size: %ld bytes\n", size);
    }
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.4.1

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*curl_easy_getinfo*(3), *curl_easy_setopt*(3),
*CURLINFO_REQUEST_SIZE*(3), *CURLINFO_SIZE_DOWNLOAD*(3),
