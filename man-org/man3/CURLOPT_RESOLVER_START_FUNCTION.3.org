#+TITLE: Manpages - CURLOPT_RESOLVER_START_FUNCTION.3
#+DESCRIPTION: Linux manpage for CURLOPT_RESOLVER_START_FUNCTION.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_RESOLVER_START_FUNCTION - callback called before a new name
resolve is started

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>

  int resolver_start_cb(void *resolver_state, void *reserved, void *userdata);

  CURLcode curl_easy_setopt(CURL *handle,
                            CURLOPT_RESOLVER_START_FUNCTION,
                            resolver_start_cb);
#+end_example

* DESCRIPTION
Pass a pointer to your callback function, which should match the
prototype shown above.

This callback function gets called by libcurl every time before a new
resolve request is started.

/resolver_state/ points to a backend-specific resolver state. Currently
only the ares resolver backend has a resolver state. It can be used to
set up any desired option on the ares channel before it's used, for
example setting up socket callback options.

/reserved/ is reserved.

/userdata/ is the user pointer set with the
/CURLOPT_RESOLVER_START_DATA(3)/ option.

The callback must return 0 on success. Returning a non-zero value will
cause the resolve to fail.

* DEFAULT
NULL (No callback)

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  static int resolver_start_cb(void *resolver_state, void *reserved,
                               void *userdata)
  {
    (void)reserved;
    printf("Received resolver_state=%p userdata=%p\n",
           resolver_state, userdata);
    return 0;
  }

  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_RESOLVER_START_FUNCTION, resolver_start_cb);
    curl_easy_setopt(curl, CURLOPT_RESOLVER_START_DATA, curl);
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
    curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.59.0

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*CURLOPT_RESOLVER_START_DATA*(3)
