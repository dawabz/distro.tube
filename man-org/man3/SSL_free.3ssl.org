#+TITLE: Manpages - SSL_free.3ssl
#+DESCRIPTION: Linux manpage for SSL_free.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_free - free an allocated SSL structure

* SYNOPSIS
#include <openssl/ssl.h> void SSL_free(SSL *ssl);

* DESCRIPTION
*SSL_free()* decrements the reference count of *ssl*, and removes the
SSL structure pointed to by *ssl* and frees up the allocated memory if
the reference count has reached 0. If *ssl* is NULL nothing is done.

* NOTES
*SSL_free()* also calls the *free()*ing procedures for indirectly
affected items, if applicable: the buffering BIO, the read and write
BIOs, cipher lists specially created for this *ssl*, the *SSL_SESSION*.
Do not explicitly free these indirectly freed up items before or after
calling *SSL_free()*, as trying to free things twice may lead to program
failure.

The ssl session has reference counts from two users: the SSL object, for
which the reference count is removed by *SSL_free()* and the internal
session cache. If the session is considered bad, because
*SSL_shutdown* (3) was not called for the connection and
*SSL_set_shutdown* (3) was not used to set the SSL_SENT_SHUTDOWN state,
the session will also be removed from the session cache as required by
RFC2246.

* RETURN VALUES
*SSL_free()* does not provide diagnostic information.

*SSL_new* (3), *SSL_clear* (3), *SSL_shutdown* (3),
*SSL_set_shutdown* (3), *ssl* (7)

* COPYRIGHT
Copyright 2000-2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
