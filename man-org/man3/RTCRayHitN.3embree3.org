#+TITLE: Manpages - RTCRayHitN.3embree3
#+DESCRIPTION: Linux manpage for RTCRayHitN.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  RTCRayHitN - combined ray/hit packet of runtime size
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore_ray.h>

  struct RTCRayHitN;

  struct RTCRayN* RTCRayHitN_RayN(struct RTCRayHitN* rayhit, unsigned int N);
  struct RTCHitN* RTCRayHitN_HitN(struct RTCRayHitN* rayhit, unsigned int N);
#+end_example

** DESCRIPTION
When the packet size of a ray/hit structure is not known at compile time
(e.g. when Embree returns a ray/hit packet in the
=RTCIntersectFunctionN= callback function), Embree uses the =RTCRayHitN=
type for ray packets. These ray/hit packets can only have sizes of 1, 4,
8, or 16. No other packet size will be used.

You can either implement different special code paths for each of these
possible packet sizes and cast the ray/hit to the appropriate ray/hit
packet type, or extract the =RTCRayN= and =RTCHitN= components using the
=rtcGetRayN= and =rtcGetHitN= helper functions and use the =RTCRayN_XXX=
and =RTCHitN_XXX= functions to access the ray and hit parts of the
structure.

** EXIT STATUS
** SEE ALSO
[RTCHitN]
