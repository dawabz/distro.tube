#+TITLE: Manpages - std_owner_less.3
#+DESCRIPTION: Linux manpage for std_owner_less.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::owner_less< _Tp > - Primary template owner_less.

* SYNOPSIS
\\

* Detailed Description
** "template<typename _Tp = void>
\\
struct std::owner_less< _Tp >"Primary template owner_less.

Definition at line *769* of file *bits/shared_ptr.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
