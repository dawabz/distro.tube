#+TITLE: Manpages - sd_event_get_exit_code.3
#+DESCRIPTION: Linux manpage for sd_event_get_exit_code.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sd_event_get_exit_code.3 is found in manpage for: [[../sd_event_exit.3][sd_event_exit.3]]