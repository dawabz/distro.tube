#+TITLE: Manpages - ldns_dnssec_rrs_print.3
#+DESCRIPTION: Linux manpage for ldns_dnssec_rrs_print.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_dnssec_rrs_new, ldns_dnssec_rrs_free, ldns_dnssec_rrs_add_rr,
ldns_dnssec_rrs_print - functions for ldns_dnssecrrs

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_dnssec_rrs_new();

void ldns_dnssec_rrs_free(ldns_dnssec_rrs *rrs);

ldns_status ldns_dnssec_rrs_add_rr(ldns_dnssec_rrs *rrs, ldns_rr *rr);

void ldns_dnssec_rrs_print(FILE *out, const ldns_dnssec_rrs *rrs);

* DESCRIPTION
/ldns_dnssec_rrs_new/()

/ldns_dnssec_rrs_free/() Frees the list of rrs, but *not* the individual
ldns_rr records contained in the list

.br *rrs*: the data structure to free

/ldns_dnssec_rrs_add_rr/() Adds an RR to the list of RRs. The list will
remain ordered. If an equal RR already exists, this RR will not be
added.

.br *rrs*: the list to add to .br *rr*: the RR to add .br Returns
LDNS_STATUS_OK on success

/ldns_dnssec_rrs_print/() Prints the given rrs to the file descriptor

.br *out*: the file descriptor to print to .br *rrs*: the list of RRs to
print

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_dnssec_zone/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*,
*RFC4033*, *RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
