#+TITLE: Manpages - rpc_svc_reg.3t
#+DESCRIPTION: Linux manpage for rpc_svc_reg.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
These routines are a part of the RPC library which allows the RPC
servers to register themselves with rpcbind (see

and associate the given program and version number with the dispatch
function. When the RPC server receives a RPC request, the library
invokes the dispatch routine with the appropriate arguments.

See

for the definition of the

data structure.

Register program

procedure

and version

with the RPC service package. If a request arrives for program

version

and procedure

is called with a pointer to its argument(s);

should return a pointer to its static result(s);

is the XDR function used to decode the arguments while

is the XDR function used to encode the results. Procedures are
registered on all available transports of the class

See

This routine returns 0 if the registration succeeded, -1 otherwise.

Associates

and

with the service dispatch procedure,

If

is

the service is not registered with the

service. If

is non-zero, then a mapping of the triple

to

is established with the local rpcbind service.

The

routine returns 1 if it succeeds, and 0 otherwise.

Remove from the rpcbind service, all mappings of the triple

to network address and all mappings within the RPC service package of
the double

to dispatch routines.

Registers the service authentication routine

with the dispatch mechanism so that it can be invoked to authenticate
RPC requests received with authentication type

This interface allows developers to add new authentication types to
their RPC applications without needing to modify the libraries. Service
implementors usually do not need this routine.

Typical service application would call

after registering the service and prior to calling

When needed to process an RPC credential of type

the

procedure will be called with two arguments,

and

and is expected to return a valid

value. There is no provision to change or delete an authentication
handler once registered.

The

routine returns 0 if the registration is successful, 1 if

already has an authentication handler registered for it, and -1
otherwise.

After RPC service transport handle

is created, it is registered with the RPC service package. This routine
modifies the global variable

(see

Service implementors usually do not need this routine.

Before an RPC service transport handle

is destroyed, it unregisters itself with the RPC service package. This
routine modifies the global variable

(see

Service implementors usually do not need this routine.

These functions are part of libtirpc.
