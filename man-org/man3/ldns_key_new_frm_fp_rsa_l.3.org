#+TITLE: Manpages - ldns_key_new_frm_fp_rsa_l.3
#+DESCRIPTION: Linux manpage for ldns_key_new_frm_fp_rsa_l.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_key_new, ldns_key_new_frm_algorithm, ldns_key_new_frm_fp,
ldns_key_new_frm_fp_l, ldns_key_new_frm_fp_rsa,
ldns_key_new_frm_fp_rsa_l, ldns_key_new_frm_fp_dsa,
ldns_key_new_frm_fp_dsa_l - create a ldns_key

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_key* ldns_key_new(void);

ldns_key* ldns_key_new_frm_algorithm(ldns_signing_algorithm a, uint16_t
size);

ldns_status ldns_key_new_frm_fp(ldns_key **k, FILE *fp);

ldns_status ldns_key_new_frm_fp_l(ldns_key **k, FILE *fp, int *line_nr);

RSA* ldns_key_new_frm_fp_rsa(FILE *fp);

RSA* ldns_key_new_frm_fp_rsa_l(FILE *fp, int *line_nr);

DSA* ldns_key_new_frm_fp_dsa(FILE *fp);

DSA* ldns_key_new_frm_fp_dsa_l(FILE *fp, int *line_nr);

* DESCRIPTION
/ldns_key_new/() Creates a new empty key structure .br Returns a new
ldns_key * structure

/ldns_key_new_frm_algorithm/() Creates a new key based on the algorithm

.br *a*: The algorithm to use .br *size*: the number of bytes for the
keysize .br Returns a new ldns_key structure with the key

/ldns_key_new_frm_fp/() Creates a new priv key based on the contents of
the file pointed by fp.

The file should be in Private-key-format v1.x.

.br *k*: the new ldns_key structure .br *fp*: the file pointer to use
.br Returns an error or LDNS_STATUS_OK

/ldns_key_new_frm_fp_l/() Creates a new private key based on the
contents of the file pointed by fp

The file should be in Private-key-format v1.x.

.br *k*: the new ldns_key structure .br *fp*: the file pointer to use
.br *line_nr*: pointer to an integer containing the current line number
(for debugging purposes) .br Returns an error or LDNS_STATUS_OK

/ldns_key_new_frm_fp_rsa/() frm_fp helper function. This function parses
the remainder of the (RSA) priv. key file generated from bind9 .br *fp*:
the file to parse .br Returns NULL on failure otherwise a RSA structure

/ldns_key_new_frm_fp_rsa_l/() frm_fp helper function. This function
parses the remainder of the (RSA) priv. key file generated from bind9
.br *fp*: the file to parse .br *line_nr*: pointer to an integer
containing the current line number (for debugging purposes) .br Returns
NULL on failure otherwise a RSA structure

/ldns_key_new_frm_fp_dsa/() frm_fp helper function. This function parses
the remainder of the (DSA) priv. key file .br *fp*: the file to parse
.br Returns NULL on failure otherwise a RSA structure

/ldns_key_new_frm_fp_dsa_l/() frm_fp helper function. This function
parses the remainder of the (DSA) priv. key file .br *fp*: the file to
parse .br *line_nr*: pointer to an integer containing the current line
number (for debugging purposes) .br Returns NULL on failure otherwise a
RSA structure

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_key/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*,
*RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
