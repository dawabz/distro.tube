#+TITLE: Manpages - CURLOPT_TRAILERDATA.3
#+DESCRIPTION: Linux manpage for CURLOPT_TRAILERDATA.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_TRAILERDATA - pointer passed to trailing headers callback

* SYNOPSIS
#include <curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TRAILERDATA, void
*userdata);

* DESCRIPTION
Data pointer to be passed to the HTTP trailer callback function.

* DEFAULT
NULL

* PROTOCOLS
HTTP

* EXAMPLE
#+begin_example
  /* Assuming we have a CURL handle in the hndl variable. */

  struct MyData data;

  curl_easy_setopt(hndl, CURLOPT_TRAILERDATA, &data);
#+end_example

A more complete example can be found in examples/http_trailers.html

* AVAILABILITY
This option was added in curl 7.64.0 and is present if HTTP support is
enabled

* RETURN VALUE
Returns CURLE_OK.

* SEE ALSO
*CURLOPT_TRAILERFUNCTION*(3),
