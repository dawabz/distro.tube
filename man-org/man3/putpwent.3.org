#+TITLE: Manpages - putpwent.3
#+DESCRIPTION: Linux manpage for putpwent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
putpwent - write a password file entry

* SYNOPSIS
#+begin_example
  #include <stdio.h>
  #include <sys/types.h>
  #include <pwd.h>

  int putpwent(const struct passwd *restrict p",FILE*restrict"stream);
#+end_example

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*putpwent*():

#+begin_example
      Since glibc 2.19:
          _DEFAULT_SOURCE
      Glibc 2.19 and earlier:
          _SVID_SOURCE
#+end_example

* DESCRIPTION
The *putpwent*() function writes a password entry from the structure /p/
in the file associated with /stream/.

The /passwd/ structure is defined in /<pwd.h>/ as follows:

#+begin_example
  struct passwd {
      char    *pw_name;        /* username */
      char    *pw_passwd;      /* user password */
      uid_t    pw_uid;         /* user ID */
      gid_t    pw_gid;         /* group ID */
      char    *pw_gecos;       /* real name */
      char    *pw_dir;         /* home directory */
      char    *pw_shell;       /* shell program */
  };
#+end_example

* RETURN VALUE
The *putpwent*() function returns 0 on success. On failure, it returns
-1, and /errno/ is set to indicate the error.

* ERRORS
- *EINVAL* :: Invalid (NULL) argument given.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface    | Attribute     | Value          |
| *putpwent*() | Thread safety | MT-Safe locale |

* CONFORMING TO
SVr4.

* SEE ALSO
*endpwent*(3), *fgetpwent*(3), *getpw*(3), *getpwent*(3), *getpwnam*(3),
*getpwuid*(3), *setpwent*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
