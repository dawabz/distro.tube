#+TITLE: Manpages - libnet-macros.h.3
#+DESCRIPTION: Linux manpage for libnet-macros.h.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libnet-macros.h -

libnet macros and symbolic constants

* SYNOPSIS
\\

** Defines
#define *LIBNET_DONT_RESOLVE* 0\\

#define *LIBNET_RESOLVE* 1\\

#define *LIBNET_ON* 0\\

#define *LIBNET_OFF* 1\\

#define *IN6ADDR_ERROR_INIT*\\

#define *LIBNET_PR2* 0\\

#define *LIBNET_PR8* 1\\

#define *LIBNET_PR16* 2\\

#define *LIBNET_PRu16* 3\\

#define *LIBNET_PR32* 4\\

#define *LIBNET_PRu32* 5\\

#define *LIBNET_PRAND_MAX* 0xffffffff\\

#define *LIBNET_MAX_PACKET* 0xffff\\

#define *IP_MAXPACKET* 0xffff\\

#define *ETHER_ADDR_LEN* 0x6\\

#define *FDDI_ADDR_LEN* 0x6\\

#define *TOKEN_RING_ADDR_LEN* 0x6\\

#define *LIBNET_ORG_CODE_SIZE* 0x3\\

#define *LIBNET_ERRBUF_SIZE* 0x100\\

#define *LIBNET_MAXOPTION_SIZE* 0x28\\

#define *FIX*(n) (n)\\

#define *UNFIX*(n) (n)\\

#define *LIBNET_DO_PAYLOAD*(l, p)\\

#define *LIBNET_CKSUM_CARRY*(x) (x = (x >> 16) + (x & 0xffff), (~(x + (x
>> 16)) & 0xffff))\\

#define *LIBNET_OSPF_AUTHCPY*(x, y) memcpy((uint8_t *)x, (uint8_t *)y,
sizeof(y))\\

#define *LIBNET_OSPF_CKSUMBUF*(x, y) memcpy((uint8_t *)x, (uint8_t *)y,
sizeof(y))\\

#define *LIBNET_NTP_DO_LI_VN_MODE*(li, vn, md) ((uint8_t)((((li) << 6) &
0xc0) | (((vn) << 3) & 0x38) | ((md) & 0x7)))\\

#define *LIBNET_ISLOOPBACK*(p) (strcmp((p)->ifr_name, 'lo') == 0)\\

#define *LIBNET_ISADVMODE*(x) (x & 0x08)\\

#define *LIBNET_LABEL_SIZE* 64\\

#define *LIBNET_LABEL_DEFAULT* 'cardshark'\\

#define *CQ_LOCK_UNLOCKED* (u_int)0x00000000\\

#define *CQ_LOCK_READ* (u_int)0x00000001\\

#define *CQ_LOCK_WRITE* (u_int)0x00000002\\

#define *for_each_context_in_cq*(l) for (l = libnet_cq_head();
libnet_cq_last(); l = libnet_cq_next())\\

#define *cq_is_wlocked*() (l_cqd.cq_lock & CQ_LOCK_WRITE)\\

#define *cq_is_rlocked*() (l_cqd.cq_lock & CQ_LOCK_READ)\\

#define *cq_is_locked*() (l_cqd.cq_lock & (CQ_LOCK_READ |
CQ_LOCK_WRITE))\\

#define *check_cq_lock*(x) (l_cqd.cq_lock & x)\\

* Detailed Description
libnet macros and symbolic constants

* Define Documentation
** #define for_each_context_in_cq(l) for (l = libnet_cq_head();
libnet_cq_last(); l = libnet_cq_next()) Provides an interface to iterate
through the context queue of libnet contexts. Before calling this macro,
be sure to set the queue using *libnet_cq_head()*.
** #define IN6ADDR_ERROR_INIT *Value:*
#+begin_example
  { { { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,                                  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,                                  0xff, 0xff } } }
#+end_example

IPv6 error code

** #define LIBNET_DO_PAYLOAD(l, p) *Value:*
#+begin_example
  if (payload_s && !payload)                                                   {                                                                                snprintf(l->err_buf, LIBNET_ERRBUF_SIZE,                                             '%s(): payload inconsistency, __func__);                          goto bad;                                                                }                                                                            if (payload_s)                                                               {                                                                                n = libnet_pblock_append(l, p, payload, payload_s);                          if (n == (uint32_t) - 1)                                                     {                                                                                goto bad;                                                                }                                                                        }                                                                            .fi
#+end_example

** #define LIBNET_DONT_RESOLVE 0 Used for libnet's name resolution
functions, specifies that no DNS lookups should be performed and the IP
address should be kept in numeric form.
** #define LIBNET_ERRBUF_SIZE 0x100 The libnet error buffer is 256 bytes
long.
** #define LIBNET_MAX_PACKET 0xffff The biggest an IP packet can be --
65,535 bytes.
** #define LIBNET_MAXOPTION_SIZE 0x28 IP and TCP options can be up to 40
bytes long.
** #define LIBNET_OFF 1 Used several places, to specify 'on' or 'one'
** #define LIBNET_ON 0 Used several places, to specify 'on' or 'one'
** #define LIBNET_PR2 0 Used for *libnet_get_prand()* to specify
function disposition
** #define LIBNET_RESOLVE 1 Used for libnet's name resolution functions,
specifies that a DNS lookup can be performed if needed to resolve the IP
address to a canonical form.
* Author
Generated automatically by Doxygen for libnet from the source code.
