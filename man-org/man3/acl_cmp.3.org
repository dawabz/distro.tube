#+TITLE: Manpages - acl_cmp.3
#+DESCRIPTION: Linux manpage for acl_cmp.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function compares the ACLs pointed to by the arguments

and

for equality. The two ACLs are considered equal if for each entry in

there is an entry in

with matching tag type, qualifier, and permissions, and vice versa.

If successful, the

function returns

if the two ACLs

and

are equal, and

if they differ. Otherwise, the value

is returned and the global variable

is set to indicate the error.

If any of the following conditions occur, the

function returns

and sets

to the corresponding value:

The argument

is not a valid pointer to an ACL.

The argument

is not a valid pointer to an ACL.

This is a non-portable, Linux specific extension to the ACL manipulation
functions defined in IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned).

Written by
