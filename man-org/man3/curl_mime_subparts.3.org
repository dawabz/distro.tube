#+TITLE: Manpages - curl_mime_subparts.3
#+DESCRIPTION: Linux manpage for curl_mime_subparts.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_mime_subparts - set subparts of a multipart mime part

* SYNOPSIS
*#include <curl/curl.h>*

*CURLcode curl_mime_subparts(curl_mimepart * */part/*,* *curl_mime *
*/subparts/*);*

* DESCRIPTION
/curl_mime_subparts(3)/ sets a multipart mime part's content from a mime
structure.

/part/ is a handle to the multipart part.

/subparts/ is a mime structure handle holding the subparts. After
/curl_mime_subparts/ succeeds, the mime structure handle belongs to the
multipart part and must not be freed explicitly. It may however be
updated by subsequent calls to mime API functions.

Setting a part's contents twice is valid: only the value set by the last
call is retained. It is possible to unassign previous part's contents by
setting /subparts/ to NULL.

* EXAMPLE
#+begin_example
   /* The inline part is an alternative proposing the html and the text
      versions of the e-mail. */
   alt = curl_mime_init(curl);

   /* HTML message. */
   part = curl_mime_addpart(alt);
   curl_mime_data(part, inline_html, CURL_ZERO_TERMINATED);
   curl_mime_type(part, "text/html");

   /* Text message. */
   part = curl_mime_addpart(alt);
   curl_mime_data(part, inline_text, CURL_ZERO_TERMINATED);

   /* Create the inline part. */
   part = curl_mime_addpart(mime);
   curl_mime_subparts(part, alt);
   curl_mime_type(part, "multipart/alternative");
   slist = curl_slist_append(NULL, "Content-Disposition: inline");
   curl_mime_headers(part, slist, 1);
#+end_example

* AVAILABILITY
As long as at least one of HTTP, SMTP or IMAP is enabled. Added in
7.56.0.

* RETURN VALUE
CURLE_OK or a CURL error code upon failure.

* SEE ALSO
*curl_mime_addpart*(3), *curl_mime_init*(3)
