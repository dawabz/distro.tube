#+TITLE: Manpages - CURLINFO_HTTP_CONNECTCODE.3
#+DESCRIPTION: Linux manpage for CURLINFO_HTTP_CONNECTCODE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLINFO_HTTP_CONNECTCODE - get the CONNECT response code

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_HTTP_CONNECTCODE, long
*p);

* DESCRIPTION
Pass a pointer to a long to receive the last received HTTP proxy
response code to a CONNECT request. The returned value will be zero if
no such response code was available.

* PROTOCOLS
HTTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    CURLcode res;
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* typically CONNECT is used to do HTTPS over HTTP proxies */
    curl_easy_setopt(curl, CURLOPT_PROXY, "http://127.0.0.1");
    res = curl_easy_perform(curl);
    if(res == CURLE_OK) {
      long code;
      res = curl_easy_getinfo(curl, CURLINFO_HTTP_CONNECTCODE, &code);
      if(!res && code)
        printf("The CONNECT response code: %03ld\n", code);
    }
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.10.7

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLINFO_RESPONSE_CODE*(3), *curl_easy_getinfo*(3),
*curl_easy_setopt*(3),
