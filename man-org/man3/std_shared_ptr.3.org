#+TITLE: Manpages - std_shared_ptr.3
#+DESCRIPTION: Linux manpage for std_shared_ptr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::shared_ptr< _Tp > - A smart pointer with reference-counted copy
semantics.

* SYNOPSIS
\\

=#include <shared_ptr.h>=

Inherits std::__shared_ptr< _Tp, _Lp >.

** Public Types
using *element_type* = typename __shared_ptr< _Tp >::*element_type*\\
The type pointed to by the stored pointer, remove_extent_t<_Tp>

using *weak_type* = *weak_ptr*< _Tp >\\
The corresponding weak_ptr type for this shared_ptr.

** Public Member Functions
constexpr *shared_ptr* () noexcept\\
Construct an empty shared_ptr.

template<typename _Yp , typename = _Constructible<_Yp*>> *shared_ptr*
(_Yp *__p)\\
Construct a shared_ptr that owns the pointer /__p/.

template<typename _Yp , typename _Deleter , typename =
_Constructible<_Yp*, _Deleter>> *shared_ptr* (_Yp *__p, _Deleter __d)\\
Construct a shared_ptr that owns the pointer /__p/ and the deleter
/__d/.

template<typename _Yp , typename _Deleter , typename _Alloc , typename =
_Constructible<_Yp*, _Deleter, _Alloc>> *shared_ptr* (_Yp *__p, _Deleter
__d, _Alloc __a)\\
Construct a shared_ptr that owns the pointer /__p/ and the deleter
/__d/.

*shared_ptr* (const *shared_ptr* &) noexcept=default\\
Copy constructor.

template<typename _Yp , typename = _Constructible<const
shared_ptr<_Yp>&>> *shared_ptr* (const *shared_ptr*< _Yp > &__r)
noexcept\\
If /__r/ is empty, constructs an empty shared_ptr; otherwise construct a
shared_ptr that shares ownership with /__r/.

template<typename _Yp > *shared_ptr* (const *shared_ptr*< _Yp > &__r,
*element_type* *__p) noexcept\\
Constructs a =shared_ptr= instance that stores =__p= and shares
ownership with =__r=.

template<typename _Yp , typename = _Constructible<const weak_ptr<_Yp>&>>
*shared_ptr* (const *weak_ptr*< _Yp > &__r)\\
Constructs a shared_ptr that shares ownership with /__r/ and stores a
copy of the pointer stored in /__r/.

template<typename _Deleter > *shared_ptr* (nullptr_t __p, _Deleter
__d)\\
Construct a shared_ptr that owns a null pointer and the deleter /__d/.

template<typename _Deleter , typename _Alloc > *shared_ptr* (nullptr_t
__p, _Deleter __d, _Alloc __a)\\
Construct a shared_ptr that owns a null pointer and the deleter /__d/.

constexpr *shared_ptr* (nullptr_t) noexcept\\
Construct an empty shared_ptr.

*shared_ptr* (*shared_ptr* &&__r) noexcept\\
Move-constructs a shared_ptr instance from /__r/.

template<typename _Yp , typename = _Constructible<shared_ptr<_Yp>>>
*shared_ptr* (*shared_ptr*< _Yp > &&__r) noexcept\\
Move-constructs a shared_ptr instance from /__r/.

template<typename _Tp1 , typename > *shared_ptr* (*std::auto_ptr*< _Tp1
> &&__r)\\

template<typename _Yp , typename _Del , typename =
_Constructible<unique_ptr<_Yp, _Del>>> *shared_ptr* (*unique_ptr*< _Yp,
_Del > &&__r)\\

*element_type* * *get* () const noexcept\\
Return the stored pointer.

*operator bool* () const noexcept\\
Return true if the stored pointer is not null.

*element_type* & *operator** () const noexcept\\

*element_type* * *operator->* () const noexcept\\

*shared_ptr* & *operator=* (const *shared_ptr* &) noexcept=default\\

template<typename _Yp > _Assignable< const *shared_ptr*< _Yp > & >
*operator=* (const *shared_ptr*< _Yp > &__r) noexcept\\

*shared_ptr* & *operator=* (*shared_ptr* &&__r) noexcept\\

template<class _Yp > _Assignable< *shared_ptr*< _Yp > > *operator=*
(*shared_ptr*< _Yp > &&__r) noexcept\\

template<typename _Yp , typename _Del > _Assignable< *unique_ptr*< _Yp,
_Del > > *operator=* (*unique_ptr*< _Yp, _Del > &&__r)\\

void *reset* () noexcept\\

template<typename _Yp > _SafeConv< _Yp > *reset* (_Yp *__p)\\

template<typename _Yp , typename _Deleter > _SafeConv< _Yp > *reset*
(_Yp *__p, _Deleter __d)\\

template<typename _Yp , typename _Deleter , typename _Alloc > _SafeConv<
_Yp > *reset* (_Yp *__p, _Deleter __d, _Alloc __a)\\

void *swap* (__shared_ptr< _Tp, _Lp > &__other) noexcept\\
Exchange both the owned pointer and the stored pointer.

bool *unique* () const noexcept\\
Return true if use_count() == 1.

long *use_count* () const noexcept\\
If *this owns a pointer, return the number of owners, otherwise zero.

\\

template<typename _Tp1 > bool *owner_before* (__shared_ptr< _Tp1, _Lp >
const &__rhs) const noexcept\\
Define an ordering based on ownership.

template<typename _Tp1 > bool *owner_before* (__weak_ptr< _Tp1, _Lp >
const &__rhs) const noexcept\\
Define an ordering based on ownership.

** Friends
template<typename _Yp , typename _Alloc , typename... _Args>
*shared_ptr*< _Yp > *allocate_shared* (const _Alloc &__a, _Args &&...
__args)\\

class *weak_ptr< _Tp >*\\

** Related Functions
(Note that these are not member functions.)

template<typename _Del , typename _Tp > _Del * *get_deleter* (const
*shared_ptr*< _Tp > &__p) noexcept\\
20.7.2.2.10 shared_ptr get_deleter

template<typename _Ch , typename _Tr , typename _Tp , _Lock_policy _Lp>
*std::basic_ostream*< _Ch, _Tr > & *operator<<* (*std::basic_ostream*<
_Ch, _Tr > &__os, const __shared_ptr< _Tp, _Lp > &__p)\\
Write the stored pointer to an ostream.

\\

template<typename _Tp , typename _Up > bool *operator==* (const
*shared_ptr*< _Tp > &__a, const *shared_ptr*< _Up > &__b) noexcept\\

template<typename _Tp > bool *operator==* (const *shared_ptr*< _Tp >
&__a, nullptr_t) noexcept\\
shared_ptr comparison with nullptr

template<typename _Tp > bool *operator==* (nullptr_t, const
*shared_ptr*< _Tp > &__a) noexcept\\
shared_ptr comparison with nullptr

template<typename _Tp , typename _Up > bool *operator!=* (const
*shared_ptr*< _Tp > &__a, const *shared_ptr*< _Up > &__b) noexcept\\
Inequality operator for shared_ptr objects, compares the stored
pointers.

template<typename _Tp > bool *operator!=* (const *shared_ptr*< _Tp >
&__a, nullptr_t) noexcept\\
shared_ptr comparison with nullptr

template<typename _Tp > bool *operator!=* (nullptr_t, const
*shared_ptr*< _Tp > &__a) noexcept\\
shared_ptr comparison with nullptr

template<typename _Tp , typename _Up > bool *operator<* (const
*shared_ptr*< _Tp > &__a, const *shared_ptr*< _Up > &__b) noexcept\\
Relational operator for shared_ptr objects, compares the stored
pointers.

template<typename _Tp > bool *operator<* (const *shared_ptr*< _Tp >
&__a, nullptr_t) noexcept\\
shared_ptr comparison with nullptr

template<typename _Tp > bool *operator<* (nullptr_t, const *shared_ptr*<
_Tp > &__a) noexcept\\
shared_ptr comparison with nullptr

template<typename _Tp , typename _Up > bool *operator<=* (const
*shared_ptr*< _Tp > &__a, const *shared_ptr*< _Up > &__b) noexcept\\
Relational operator for shared_ptr objects, compares the stored
pointers.

template<typename _Tp > bool *operator<=* (const *shared_ptr*< _Tp >
&__a, nullptr_t) noexcept\\
shared_ptr comparison with nullptr

template<typename _Tp > bool *operator<=* (nullptr_t, const
*shared_ptr*< _Tp > &__a) noexcept\\
shared_ptr comparison with nullptr

template<typename _Tp , typename _Up > bool *operator>* (const
*shared_ptr*< _Tp > &__a, const *shared_ptr*< _Up > &__b) noexcept\\
Relational operator for shared_ptr objects, compares the stored
pointers.

template<typename _Tp > bool *operator>* (const *shared_ptr*< _Tp >
&__a, nullptr_t) noexcept\\
shared_ptr comparison with nullptr

template<typename _Tp > bool *operator>* (nullptr_t, const *shared_ptr*<
_Tp > &__a) noexcept\\
shared_ptr comparison with nullptr

template<typename _Tp , typename _Up > bool *operator>=* (const
*shared_ptr*< _Tp > &__a, const *shared_ptr*< _Up > &__b) noexcept\\
Relational operator for shared_ptr objects, compares the stored
pointers.

template<typename _Tp > bool *operator>=* (const *shared_ptr*< _Tp >
&__a, nullptr_t) noexcept\\
shared_ptr comparison with nullptr

template<typename _Tp > bool *operator>=* (nullptr_t, const
*shared_ptr*< _Tp > &__a) noexcept\\
shared_ptr comparison with nullptr

template<typename _Tp > void *swap* (*shared_ptr*< _Tp > &__a,
*shared_ptr*< _Tp > &__b) noexcept\\
Swap overload for shared_ptr.

template<typename _Tp , typename _Up > *shared_ptr*< _Tp >
*static_pointer_cast* (const *shared_ptr*< _Up > &__r) noexcept\\
Convert type of =shared_ptr=, via =static_cast=

template<typename _Tp , typename _Up > *shared_ptr*< _Tp >
*const_pointer_cast* (const *shared_ptr*< _Up > &__r) noexcept\\
Convert type of =shared_ptr=, via =const_cast=

template<typename _Tp , typename _Up > *shared_ptr*< _Tp >
*dynamic_pointer_cast* (const *shared_ptr*< _Up > &__r) noexcept\\
Convert type of =shared_ptr=, via =dynamic_cast=

template<typename _Tp , typename _Up > *shared_ptr*< _Tp >
*reinterpret_pointer_cast* (const *shared_ptr*< _Up > &__r) noexcept\\
Convert type of =shared_ptr=, via =reinterpret_cast=

\\

template<typename _Tp , typename _Alloc , typename... _Args>
*shared_ptr*< _Tp > *allocate_shared* (const _Alloc &__a, _Args &&...
__args)\\
Create an object that is owned by a shared_ptr.

template<typename _Tp , typename... _Args> *shared_ptr*< _Tp >
*make_shared* (_Args &&... __args)\\
Create an object that is owned by a shared_ptr.

\\

template<typename _Tp , _Lock_policy _Lp> bool *atomic_is_lock_free*
(const __shared_ptr< _Tp, _Lp > *__p)\\
Report whether shared_ptr atomic operations are lock-free.

\\

template<typename _Tp > *shared_ptr*< _Tp > *atomic_load_explicit*
(const *shared_ptr*< _Tp > *__p, *memory_order*)\\
Atomic load for shared_ptr objects.

\\

template<typename _Tp > void *atomic_store_explicit* (*shared_ptr*< _Tp
> *__p, *shared_ptr*< _Tp > __r, *memory_order*)\\
Atomic store for shared_ptr objects.

\\

template<typename _Tp > *shared_ptr*< _Tp > *atomic_exchange_explicit*
(*shared_ptr*< _Tp > *__p, *shared_ptr*< _Tp > __r, *memory_order*)\\
Atomic exchange for shared_ptr objects.

\\

template<typename _Tp > bool *atomic_compare_exchange_strong_explicit*
(*shared_ptr*< _Tp > *__p, *shared_ptr*< _Tp > *__v, *shared_ptr*< _Tp >
__w, *memory_order*, *memory_order*)\\
Atomic compare-and-swap for shared_ptr objects.

* Detailed Description
** "template<typename _Tp>
\\
class std::shared_ptr< _Tp >"A smart pointer with reference-counted copy
semantics.

A =shared_ptr= object is either empty or /owns/ a pointer passed to the
constructor. Copies of a =shared_ptr= share ownership of the same
pointer. When the last =shared_ptr= that owns the pointer is destroyed
or reset, the owned pointer is freed (either by =delete= or by invoking
a custom deleter that was passed to the constructor).

A =shared_ptr= also stores another pointer, which is usually (but not
always) the same pointer as it owns. The stored pointer can be retrieved
by calling the =get()= member function.

The equality and relational operators for =shared_ptr= only compare the
stored pointer returned by =get()=, not the owned pointer. To test
whether two =shared_ptr= objects share ownership of the same pointer see
=std::shared_ptr::owner_before= and =std::owner_less=.

Definition at line *122* of file *bits/shared_ptr.h*.

* Member Typedef Documentation
** template<typename _Tp > using *std::shared_ptr*< _Tp
>::*element_type* = typename __shared_ptr<_Tp>::*element_type*
The type pointed to by the stored pointer, remove_extent_t<_Tp>

Definition at line *137* of file *bits/shared_ptr.h*.

** template<typename _Tp > using *std::shared_ptr*< _Tp >::*weak_type* =
*weak_ptr*<_Tp>
The corresponding weak_ptr type for this shared_ptr.

Definition at line *142* of file *bits/shared_ptr.h*.

* Constructor & Destructor Documentation
** template<typename _Tp > constexpr *std::shared_ptr*< _Tp
>::*shared_ptr* ()= [inline]=, = [constexpr]=, = [noexcept]=
Construct an empty shared_ptr.

*Postcondition*

#+begin_quote
  use_count()==0 && get()==0
#+end_quote

Definition at line *148* of file *bits/shared_ptr.h*.

** template<typename _Tp > *std::shared_ptr*< _Tp >::*shared_ptr* (const
*shared_ptr*< _Tp > &)= [default]=, = [noexcept]=
Copy constructor.

** template<typename _Tp > template<typename _Yp , typename =
_Constructible<_Yp*>> *std::shared_ptr*< _Tp >::*shared_ptr* (_Yp *
__p)= [inline]=, = [explicit]=
Construct a shared_ptr that owns the pointer /__p/.

*Parameters*

#+begin_quote
  /__p/ A pointer that is convertible to element_type*.
#+end_quote

*Postcondition*

#+begin_quote
  use_count() == 1 && get() == __p
#+end_quote

*Exceptions*

#+begin_quote
  /std::bad_alloc,in/ which case =delete= /__p/ is called.
#+end_quote

Definition at line *160* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<typename _Yp , typename _Deleter ,
typename = _Constructible<_Yp*, _Deleter>> *std::shared_ptr*< _Tp
>::*shared_ptr* (_Yp * __p, _Deleter __d)= [inline]=
Construct a shared_ptr that owns the pointer /__p/ and the deleter
/__d/.

*Parameters*

#+begin_quote
  /__p/ A pointer.\\
  /__d/ A deleter.
#+end_quote

*Postcondition*

#+begin_quote
  use_count() == 1 && get() == __p
#+end_quote

*Exceptions*

#+begin_quote
  /std::bad_alloc,in/ which case /__d(__p)/ is called.
#+end_quote

Requirements: _Deleter's copy constructor and destructor must not throw

__shared_ptr will release __p by calling __d(__p)

Definition at line *177* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<typename _Deleter >
*std::shared_ptr*< _Tp >::*shared_ptr* (nullptr_t __p, _Deleter
__d)= [inline]=
Construct a shared_ptr that owns a null pointer and the deleter /__d/.

*Parameters*

#+begin_quote
  /__p/ A null pointer constant.\\
  /__d/ A deleter.
#+end_quote

*Postcondition*

#+begin_quote
  use_count() == 1 && get() == __p
#+end_quote

*Exceptions*

#+begin_quote
  /std::bad_alloc,in/ which case /__d(__p)/ is called.
#+end_quote

Requirements: _Deleter's copy constructor and destructor must not throw

The last owner will call __d(__p)

Definition at line *194* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<typename _Yp , typename _Deleter ,
typename _Alloc , typename = _Constructible<_Yp*, _Deleter, _Alloc>>
*std::shared_ptr*< _Tp >::*shared_ptr* (_Yp * __p, _Deleter __d, _Alloc
__a)= [inline]=
Construct a shared_ptr that owns the pointer /__p/ and the deleter
/__d/.

*Parameters*

#+begin_quote
  /__p/ A pointer.\\
  /__d/ A deleter.\\
  /__a/ An allocator.
#+end_quote

*Postcondition*

#+begin_quote
  use_count() == 1 && get() == __p
#+end_quote

*Exceptions*

#+begin_quote
  /std::bad_alloc,in/ which case /__d(__p)/ is called.
#+end_quote

Requirements: _Deleter's copy constructor and destructor must not throw
_Alloc's copy constructor and destructor must not throw.

__shared_ptr will release __p by calling __d(__p)

Definition at line *214* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<typename _Deleter , typename _Alloc
> *std::shared_ptr*< _Tp >::*shared_ptr* (nullptr_t __p, _Deleter __d,
_Alloc __a)= [inline]=
Construct a shared_ptr that owns a null pointer and the deleter /__d/.

*Parameters*

#+begin_quote
  /__p/ A null pointer constant.\\
  /__d/ A deleter.\\
  /__a/ An allocator.
#+end_quote

*Postcondition*

#+begin_quote
  use_count() == 1 && get() == __p
#+end_quote

*Exceptions*

#+begin_quote
  /std::bad_alloc,in/ which case /__d(__p)/ is called.
#+end_quote

Requirements: _Deleter's copy constructor and destructor must not throw
_Alloc's copy constructor and destructor must not throw.

The last owner will call __d(__p)

Definition at line *233* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<typename _Yp > *std::shared_ptr*<
_Tp >::*shared_ptr* (const *shared_ptr*< _Yp > & __r, *element_type* *
__p)= [inline]=, = [noexcept]=
Constructs a =shared_ptr= instance that stores =__p= and shares
ownership with =__r=.

*Parameters*

#+begin_quote
  /__r/ A =shared_ptr=.\\
  /__p/ A pointer that will remain valid while =*__r= is valid.
#+end_quote

*Postcondition*

#+begin_quote
  =get() == __p && use_count() == __r.use_count()=
#+end_quote

This can be used to construct a =shared_ptr= to a sub-object of an
object managed by an existing =shared_ptr=. The complete object will
remain valid while any =shared_ptr= owns it, even if they don't store a
pointer to the complete object.

#+begin_example
  shared_ptr<pair<int,int>> pii(new pair<int,int>());
  shared_ptr<int> pi(pii, &pii->first);
  assert(pii.use_count() == 2);
#+end_example

Definition at line *257* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<typename _Yp , typename =
_Constructible<const shared_ptr<_Yp>&>> *std::shared_ptr*< _Tp
>::*shared_ptr* (const *shared_ptr*< _Yp > & __r)= [inline]=,
= [noexcept]=
If /__r/ is empty, constructs an empty shared_ptr; otherwise construct a
shared_ptr that shares ownership with /__r/.

*Parameters*

#+begin_quote
  /__r/ A shared_ptr.
#+end_quote

*Postcondition*

#+begin_quote
  get() == __r.get() && use_count() == __r.use_count()
#+end_quote

Definition at line *296* of file *bits/shared_ptr.h*.

** template<typename _Tp > *std::shared_ptr*< _Tp >::*shared_ptr*
(*shared_ptr*< _Tp > && __r)= [inline]=, = [noexcept]=
Move-constructs a shared_ptr instance from /__r/.

*Parameters*

#+begin_quote
  /__r/ A shared_ptr rvalue.
#+end_quote

*Postcondition*

#+begin_quote
  *this contains the old value of /__r/, /__r/ is empty.
#+end_quote

Definition at line *304* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<typename _Yp , typename =
_Constructible<shared_ptr<_Yp>>> *std::shared_ptr*< _Tp >::*shared_ptr*
(*shared_ptr*< _Yp > && __r)= [inline]=, = [noexcept]=
Move-constructs a shared_ptr instance from /__r/.

*Parameters*

#+begin_quote
  /__r/ A shared_ptr rvalue.
#+end_quote

*Postcondition*

#+begin_quote
  *this contains the old value of /__r/, /__r/ is empty.
#+end_quote

Definition at line *313* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<typename _Yp , typename =
_Constructible<const weak_ptr<_Yp>&>> *std::shared_ptr*< _Tp
>::*shared_ptr* (const *weak_ptr*< _Yp > & __r)= [inline]=,
= [explicit]=
Constructs a shared_ptr that shares ownership with /__r/ and stores a
copy of the pointer stored in /__r/.

*Parameters*

#+begin_quote
  /__r/ A weak_ptr.
#+end_quote

*Postcondition*

#+begin_quote
  use_count() == __r.use_count()
#+end_quote

*Exceptions*

#+begin_quote
  /bad_weak_ptr/ when __r.expired(), in which case the constructor has
  no effect.
#+end_quote

Definition at line *325* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<typename _Yp , typename _Del ,
typename = _Constructible<unique_ptr<_Yp, _Del>>> *std::shared_ptr*< _Tp
>::*shared_ptr* (*unique_ptr*< _Yp, _Del > && __r)= [inline]=
Definition at line *340* of file *bits/shared_ptr.h*.

** template<typename _Tp > constexpr *std::shared_ptr*< _Tp
>::*shared_ptr* (nullptr_t)= [inline]=, = [constexpr]=, = [noexcept]=
Construct an empty shared_ptr.

*Postcondition*

#+begin_quote
  use_count() == 0 && get() == nullptr
#+end_quote

Definition at line *357* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<typename _Tp1 , typename >
*std::shared_ptr*< _Tp >::*shared_ptr* (*std::auto_ptr*< _Tp1 > &&
__r)= [inline]=
Definition at line *322* of file *auto_ptr.h*.

* Member Function Documentation
** template<typename _Tp , _Lock_policy _Lp> *element_type* *
std::__shared_ptr< _Tp, _Lp >::get () const= [inline]=, = [noexcept]=,
= [inherited]=
Return the stored pointer.

Definition at line *1290* of file *shared_ptr_base.h*.

** template<typename _Tp , _Lock_policy _Lp> std::__shared_ptr< _Tp, _Lp
>::operator bool () const= [inline]=, = [explicit]=, = [noexcept]=,
= [inherited]=
Return true if the stored pointer is not null.

Definition at line *1294* of file *shared_ptr_base.h*.

** template<typename _Tp , _Lock_policy _Lp, bool =
is_array<_Tp>::value, bool = is_void<_Tp>::value> *element_type* &
std::__shared_ptr_access< _Tp, _Lp, bool, bool >::operator* ()
const= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *974* of file *shared_ptr_base.h*.

** template<typename _Tp , _Lock_policy _Lp, bool =
is_array<_Tp>::value, bool = is_void<_Tp>::value> *element_type* *
std::__shared_ptr_access< _Tp, _Lp, bool, bool >::operator-> ()
const= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *981* of file *shared_ptr_base.h*.

** template<typename _Tp > template<typename _Yp > _Assignable< const
*shared_ptr*< _Yp > & > *std::shared_ptr*< _Tp >::operator= (const
*shared_ptr*< _Yp > & __r)= [inline]=, = [noexcept]=
Definition at line *363* of file *bits/shared_ptr.h*.

** template<typename _Tp > *shared_ptr* & *std::shared_ptr*< _Tp
>::operator= (*shared_ptr*< _Tp > && __r)= [inline]=, = [noexcept]=
Definition at line *383* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<class _Yp > _Assignable<
*shared_ptr*< _Yp > > *std::shared_ptr*< _Tp >::operator= (*shared_ptr*<
_Yp > && __r)= [inline]=, = [noexcept]=
Definition at line *391* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<typename _Yp , typename _Del >
_Assignable< *unique_ptr*< _Yp, _Del > > *std::shared_ptr*< _Tp
>::operator= (*unique_ptr*< _Yp, _Del > && __r)= [inline]=
Definition at line *399* of file *bits/shared_ptr.h*.

** template<typename _Tp , _Lock_policy _Lp> template<typename _Tp1 >
bool std::__shared_ptr< _Tp, _Lp >::owner_before (__shared_ptr< _Tp1,
_Lp > const & __rhs) const= [inline]=, = [noexcept]=, = [inherited]=
Define an ordering based on ownership. This function defines a strict
weak ordering between two shared_ptr or weak_ptr objects, such that one
object is less than the other unless they share ownership of the same
pointer, or are both empty.

Definition at line *1324* of file *shared_ptr_base.h*.

** template<typename _Tp , _Lock_policy _Lp> template<typename _Tp1 >
bool std::__shared_ptr< _Tp, _Lp >::owner_before (__weak_ptr< _Tp1, _Lp
> const & __rhs) const= [inline]=, = [noexcept]=, = [inherited]=
Define an ordering based on ownership. This function defines a strict
weak ordering between two shared_ptr or weak_ptr objects, such that one
object is less than the other unless they share ownership of the same
pointer, or are both empty.

Definition at line *1329* of file *shared_ptr_base.h*.

** template<typename _Tp , _Lock_policy _Lp> void std::__shared_ptr<
_Tp, _Lp >::reset ()= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *1266* of file *shared_ptr_base.h*.

** template<typename _Tp , _Lock_policy _Lp> template<typename _Yp >
_SafeConv< _Yp > std::__shared_ptr< _Tp, _Lp >::reset (_Yp *
__p)= [inline]=, = [inherited]=
Definition at line *1271* of file *shared_ptr_base.h*.

** template<typename _Tp , _Lock_policy _Lp> template<typename _Yp ,
typename _Deleter > _SafeConv< _Yp > std::__shared_ptr< _Tp, _Lp
>::reset (_Yp * __p, _Deleter __d)= [inline]=, = [inherited]=
Definition at line *1280* of file *shared_ptr_base.h*.

** template<typename _Tp , _Lock_policy _Lp> template<typename _Yp ,
typename _Deleter , typename _Alloc > _SafeConv< _Yp >
std::__shared_ptr< _Tp, _Lp >::reset (_Yp * __p, _Deleter __d, _Alloc
__a)= [inline]=, = [inherited]=
Definition at line *1285* of file *shared_ptr_base.h*.

** template<typename _Tp , _Lock_policy _Lp> void std::__shared_ptr<
_Tp, _Lp >::swap (__shared_ptr< _Tp, _Lp > & __other)= [inline]=,
= [noexcept]=, = [inherited]=
Exchange both the owned pointer and the stored pointer.

Definition at line *1309* of file *shared_ptr_base.h*.

** template<typename _Tp , _Lock_policy _Lp> bool std::__shared_ptr<
_Tp, _Lp >::unique () const= [inline]=, = [noexcept]=, = [inherited]=
Return true if use_count() == 1.

Definition at line *1299* of file *shared_ptr_base.h*.

** template<typename _Tp , _Lock_policy _Lp> long std::__shared_ptr<
_Tp, _Lp >::use_count () const= [inline]=, = [noexcept]=, = [inherited]=
If *this owns a pointer, return the number of owners, otherwise zero.

Definition at line *1304* of file *shared_ptr_base.h*.

* Friends And Related Function Documentation
** template<typename _Tp > friend class *weak_ptr*< _Tp >= [friend]=
Definition at line *417* of file *bits/shared_ptr.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
