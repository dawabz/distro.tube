#+TITLE: Manpages - pthread_mutexattr_destroy.3
#+DESCRIPTION: Linux manpage for pthread_mutexattr_destroy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about pthread_mutexattr_destroy.3 is found in manpage for: [[../man3/pthread_mutexattr_init.3][man3/pthread_mutexattr_init.3]]