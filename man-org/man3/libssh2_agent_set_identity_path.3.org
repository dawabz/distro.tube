#+TITLE: Manpages - libssh2_agent_set_identity_path.3
#+DESCRIPTION: Linux manpage for libssh2_agent_set_identity_path.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_agent_set_identity_path - set an ssh-agent socket path on disk

* SYNOPSIS
#include <libssh2.h>

void libssh2_agent_set_identity_path(LIBSSH2_AGENT *agent, const char
*path);

* DESCRIPTION
Allows a custom agent identity socket path instead of the default
SSH_AUTH_SOCK env value

* RETURN VALUE
Returns void

* AVAILABILITY
Added in libssh2 1.9

* SEE ALSO
*libssh2_agent_init(3)* *libssh2_agent_get_identity_path(3)*
