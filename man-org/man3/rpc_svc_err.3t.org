#+TITLE: Manpages - rpc_svc_err.3t
#+DESCRIPTION: Linux manpage for rpc_svc_err.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
These routines are part of the RPC library which allows C language
programs to make procedure calls on other machines across the network.

These routines can be called by the server side dispatch function if
there is any error in the transaction with the client.

See

for the definition of the

data structure.

Called by a service dispatch routine that refuses to perform a remote
procedure call due to an authentication error.

Called by a service dispatch routine that cannot successfully decode the
remote arguments (see

in

Called by a service dispatch routine that does not implement the
procedure number that the caller requests.

Called when the desired program is not registered with the RPC package.
Service implementors usually do not need this routine.

Called when the desired version of a program is not registered with the
RPC package. The

argument is the lowest version number, and

is the highest version number. Service implementors usually do not need
this routine.

Called by a service dispatch routine when it detects a system error not
covered by any particular protocol. For example, if a service can no
longer allocate storage, it may call this routine.

Called by a service dispatch routine that refuses to perform a remote
procedure call due to insufficient (but correct) authentication
arguments. The routine calls

These functions are part of libtirpc.
