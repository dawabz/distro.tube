#+TITLE: Manpages - XrmMergeDatabases.3
#+DESCRIPTION: Linux manpage for XrmMergeDatabases.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XrmMergeDatabases, XrmCombineDatabase, XrmCombineFileDatabase - merge
resource databases

* SYNTAX
#include <X11/Xresource.h>

void XrmMergeDatabases( XrmDatabase /source_db/, XrmDatabase
*/target_db/ );

void XrmCombineDatabase( XrmDatabase /source_db/, XrmDatabase
*/target_db/, Bool /override/ );

Status XrmCombineFileDatabase( _Xconst char */filename/, XrmDatabase
*/target_db/ , Bool /override/);

* ARGUMENTS
- source_db :: Specifies the resource database that is to be merged into
  the target database.

- target_db :: Specifies the resource database into which the source
  database is to be merged.

- filename :: Specifies the resource database file name.

- override :: Specifies whether source entries override target ones.

* DESCRIPTION
Calling the *XrmMergeDatabases* function is equivalent to calling the
*XrmCombineDatabase* function with an override argument of *True*.

The *XrmCombineDatabase* function merges the contents of one database
into another. If the same specifier is used for an entry in both
databases, the entry in the source_db will replace the entry in the
target_db if override is *True*; otherwise, the entry in source_db is
discarded. If target_db contains NULL, *XrmCombineDatabase* simply
stores source_db in it. Otherwise, source_db is destroyed by the merge,
but the database pointed to by target_db is not destroyed. The database
entries are merged without changing values or types, regardless of the
locales of the databases. The locale of the target database is not
modified.

The *XrmCombineFileDatabase* function merges the contents of a resource
file into a database. If the same specifier is used for an entry in both
the file and the database, the entry in the file will replace the entry
in the database if override is *True*; otherwise, the entry in the file
is discarded. The file is parsed in the current locale. If the file
cannot be read, a zero status is returned; otherwise, a nonzero status
is returned. If target_db contains NULL, *XrmCombineFileDatabase*
creates and returns a new database to it. Otherwise, the database
pointed to by target_db is not destroyed by the merge. The database
entries are merged without changing values or types, regardless of the
locale of the database. The locale of the target database is not
modified.

* SEE ALSO
XrmGetResource(3), XrmInitialize(3), XrmPutResource(3)\\
/Xlib - C Language X Interface/
