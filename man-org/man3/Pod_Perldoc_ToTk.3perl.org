#+TITLE: Manpages - Pod_Perldoc_ToTk.3perl
#+DESCRIPTION: Linux manpage for Pod_Perldoc_ToTk.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Pod::Perldoc::ToTk - let Perldoc use Tk::Pod to render Pod

* SYNOPSIS
perldoc -o tk Some::Modulename &

* DESCRIPTION
This is a plug-in class that allows Perldoc to use Tk::Pod as a
formatter class.

You have to have installed Tk::Pod first, or this class won't load.

* SEE ALSO
Tk::Pod, Pod::Perldoc

* AUTHOR
Current maintainer: Mark Allen =<mallen@cpan.org>=

Past contributions from: brian d foy =<bdfoy@cpan.org>= Adriano R.
Ferreira =<ferreira@cpan.org>=; Sean M. Burke =<sburke@cpan.org>=;
significant portions copied from /tkpod/ in the Tk::Pod dist, by Nick
Ing-Simmons, Slaven Rezic, et al.
