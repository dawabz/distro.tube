#+TITLE: Manpages - SDL_GetTicks.3
#+DESCRIPTION: Linux manpage for SDL_GetTicks.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_GetTicks - Get the number of milliseconds since the SDL library
initialization.

* SYNOPSIS
*#include "SDL.h"*

*Uint32 SDL_GetTicks*(*void*)

* DESCRIPTION
Get the number of milliseconds since the SDL library initialization.
Note that this value wraps if the program runs for more than ~49 days.

* SEE ALSO
*SDL_Delay*
