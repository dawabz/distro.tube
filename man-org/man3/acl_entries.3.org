#+TITLE: Manpages - acl_entries.3
#+DESCRIPTION: Linux manpage for acl_entries.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function returns the number of ACL entries that are contained in the ACL
referred to by the argument

The

function returns the number of entries in

if successful; otherwise the value

is returned and the global variable

is set to indicate the error.

If any of the following conditions occur, the

function returns

and sets

to the corresponding value:

The argument

is not a valid pointer to an ACL.

This is a non-portable, Linux specific extension to the ACL manipulation
functions defined in IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned).

Written by
