#+TITLE: Manpages - Pod_Text_Termcap.3perl
#+DESCRIPTION: Linux manpage for Pod_Text_Termcap.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Pod::Text::Termcap - Convert POD data to ASCII text with format escapes

* SYNOPSIS
use Pod::Text::Termcap; my $parser = Pod::Text::Termcap->new (sentence
=> 0, width => 78); # Read POD from STDIN and write to STDOUT.
$parser->parse_from_filehandle; # Read POD from file.pod and write to
file.txt. $parser->parse_from_file (file.pod, file.txt);

* DESCRIPTION
Pod::Text::Termcap is a simple subclass of Pod::Text that highlights
output text using the correct termcap escape sequences for the current
terminal. Apart from the format codes, it in all ways functions like
Pod::Text. See Pod::Text for details and available options.

This module uses Term::Cap to find the correct terminal settings. See
the documentation of that module for how it finds terminal database
information and how to override that behavior if necessary. If unable to
find control strings for bold and underscore formatting, that formatting
is skipped, resulting in the same output as Pod::Text.

* AUTHOR
Russ Allbery <rra@cpan.org>

* COPYRIGHT AND LICENSE
Copyright 1999, 2001-2002, 2004, 2006, 2008-2009, 2014-2015, 2018-2019
Russ Allbery <rra@cpan.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

* SEE ALSO
Pod::Text, Pod::Simple, Term::Cap

The current version of this module is always available from its web site
at <https://www.eyrie.org/~eagle/software/podlators/>. It is also part
of the Perl core distribution as of 5.6.0.
