#+TITLE: Manpages - funopen.3bsd
#+DESCRIPTION: Linux manpage for funopen.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

The

function associates a stream with up to four

Either

or

must be specified; the others can be given as an appropriately-typed

pointer. These

functions will be used to read, write, seek and close the new stream.

In general, omitting a function means that any attempt to perform the
associated operation on the resulting stream will fail. If the close
function is omitted, closing the stream will flush any buffered output
and then succeed.

The calling conventions of

and

must match those, respectively, of

and

with the single exception that they are passed the

argument specified to

in place of the traditional file descriptor argument.

Read and write

functions are allowed to change the underlying buffer on fully buffered
or line buffered streams by calling

They are also not required to completely fill or empty the buffer. They
are not, however, allowed to change streams from unbuffered to buffered
or to change the state of the line buffering flag. They must also be
prepared to have read or write calls occur on buffers other than the one
most recently specified.

All user

functions can report an error by returning -1. Additionally, all of the
functions should set the external variable

appropriately if an error occurs.

An error on

does not keep the stream open.

As a convenience, the include file

defines the macros

and

as calls to

with only a read or write function specified.

Upon successful completion,

returns a

pointer. Otherwise,

is returned and the global variable

is set to indicate the error.

The

function was called without either a read or write function. The

function may also fail and set

for any of the errors specified for the routine

The

functions first appeared in

The

function may not be portable to systems other than

On

and

the

interface erroneously assumes that

is an integral type, and uses it in the

hook; but because code using a

hook will fail to build on systems where

is a struct, and it will need to be slightly fixed anyway, the
implementation provided by libbsd (in the same way as

uses the correct

types.
