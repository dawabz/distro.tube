#+TITLE: Manpages - XRectInRegion.3
#+DESCRIPTION: Linux manpage for XRectInRegion.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XRectInRegion.3 is found in manpage for: [[../man3/XEmptyRegion.3][man3/XEmptyRegion.3]]