#+TITLE: Manpages - XcursorImagesCreate.3
#+DESCRIPTION: Linux manpage for XcursorImagesCreate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XcursorImagesCreate.3 is found in manpage for: [[../man3/Xcursor.3][man3/Xcursor.3]]