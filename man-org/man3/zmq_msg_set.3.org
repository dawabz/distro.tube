#+TITLE: Manpages - zmq_msg_set.3
#+DESCRIPTION: Linux manpage for zmq_msg_set.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_msg_set - set message property

* SYNOPSIS
*int zmq_msg_set (zmq_msg_t */*message/*, int */property/*, int
*/value/*);*

* DESCRIPTION
The /zmq_msg_set()/ function shall set the property specified by the
/property/ argument to the value of the /value/ argument for the 0MQ
message fragment pointed to by the /message/ argument.

Currently the /zmq_msg_set()/ function does not support any property
names.

* RETURN VALUE
The /zmq_msg_set()/ function shall return zero if successful. Otherwise
it shall return -1 and set /errno/ to one of the values defined below.

* ERRORS
*EINVAL*

#+begin_quote
  The requested property /property/ is unknown.
#+end_quote

* SEE ALSO
*zmq_msg_get*(3) *zmq*(7)

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
