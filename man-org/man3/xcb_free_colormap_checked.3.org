#+TITLE: Manpages - xcb_free_colormap_checked.3
#+DESCRIPTION: Linux manpage for xcb_free_colormap_checked.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_free_colormap_checked.3 is found in manpage for: [[../man3/xcb_free_colormap.3][man3/xcb_free_colormap.3]]