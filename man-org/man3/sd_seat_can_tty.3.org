#+TITLE: Manpages - sd_seat_can_tty.3
#+DESCRIPTION: Linux manpage for sd_seat_can_tty.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sd_seat_can_tty.3 is found in manpage for: [[../sd_seat_get_active.3][sd_seat_get_active.3]]