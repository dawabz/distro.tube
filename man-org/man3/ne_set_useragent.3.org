#+TITLE: Manpages - ne_set_useragent.3
#+DESCRIPTION: Linux manpage for ne_set_useragent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ne_set_useragent, ne_set_read_timeout, ne_set_connect_timeout,
ne_get_scheme, ne_get_server_hostport - common properties for HTTP
sessions

* SYNOPSIS
#+begin_example
  #include <ne_session.h>
#+end_example

*void ne_set_useragent(ne_session **/session/*, const char
**/product/*);*

*void ne_set_read_timeout(ne_session **/session/*, int */timeout/*);*

*void ne_set_connect_timeout(ne_session **/session/*, int */timeout/*);*

*const char *ne_get_scheme(ne_sesssion **/session/*);*

*const char *ne_get_server_hostport(ne_sesssion **/session/*);*

* DESCRIPTION
The User-Agent request header is used to identify the software which
generated the request for statistical or debugging purposes. neon does
not send a User-Agent header unless a call is made to the
*ne_set_useragent*. *ne_set_useragent* must be passed a product string
conforming to RFC2616s product token grammar; of the form
"Product/Version".

When neon reads from a socket, by default the read operation will time
out after 60 seconds, and the request will fail giving an *NE_TIMEOUT*
error. To configure this timeout interval, call *ne_set_read_timeout*
giving the desired number of seconds as the /timeout/ parameter.

When a connection is being established to a server, normally only the
systems TCP timeout handling will apply. To configure a specific (and
probably shorter) timeout, the *ne_set_connect_timeout* can be used,
giving the desired number of seconds as the /timeout/ parameter. If 0 is
passed, then the default behaviour of using the system TCP timeout will
be used.

The scheme used to initially create the session will be returned by
*ne_get_scheme*.

The hostport pair with which the session is associated will be returned
by the *ne_get_server_hostport*; for example www.example.com:8080. Note
that the :port will be omitted if the default port for the scheme is
used.

* EXAMPLES
Set a user-agent string:

#+begin_quote
  #+begin_example
    ne_session *sess = ne_session_create(...);
    ne_set_useragent(sess, "MyApplication/2.1");
  #+end_example
#+end_quote

Set a 30 second read timeout:

#+begin_quote
  #+begin_example
    ne_session *sess = ne_session_create(...);
    ne_set_read_timeout(sess, 30);
  #+end_example
#+end_quote

* SEE ALSO
ne_session_create, ne_set_session_flag.

* AUTHOR
*Joe Orton* <neon@lists.manyfish.co.uk>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
