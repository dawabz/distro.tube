#+TITLE: Manpages - dysize.3
#+DESCRIPTION: Linux manpage for dysize.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
dysize - get number of days for a given year

* SYNOPSIS
#+begin_example
  #include <time.h>

  int dysize(int year);
#+end_example

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*dysize*():

#+begin_example
      Since glibc 2.19:
          _DEFAULT_SOURCE
      Glibc 2.19 and earlier:
          _BSD_SOURCE || _SVID_SOURCE
#+end_example

* DESCRIPTION
The function returns 365 for a normal year and 366 for a leap year. The
calculation for leap year is based on:

#+begin_example
  (year) %4 == 0 && ((year) %100 != 0 || (year) %400 == 0)
#+end_example

The formula is defined in the macro /__isleap(year)/ also found in
/<time.h>/.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface  | Attribute     | Value   |
| *dysize*() | Thread safety | MT-Safe |

* CONFORMING TO
This function occurs in SunOS 4.x.

* NOTES
This is a compatibility function only. Don't use it in new programs.

* SEE ALSO
*strftime*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
