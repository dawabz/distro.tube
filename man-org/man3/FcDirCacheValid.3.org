#+TITLE: Manpages - FcDirCacheValid.3
#+DESCRIPTION: Linux manpage for FcDirCacheValid.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcDirCacheValid - check directory cache

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcDirCacheValid (const FcChar8 */dir/*);*

* DESCRIPTION
Returns FcTrue if /dir/ has an associated valid cache file, else returns
FcFalse
