#+TITLE: Man3 - W
#+DESCRIPTION: Man3 - W
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* W
#+begin_src bash :exports results
readarray -t starts_with_w < <(find . -type f -iname "w*" | sort)

for x in "${starts_with_w[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./warn.3.org][warn.3]]                         |
| [[file:./warnings.3perl.org][warnings.3perl]]                 |
| [[file:./warnings_register.3perl.org][warnings_register.3perl]]        |
| [[file:./warnx.3.org][warnx.3]]                        |
| [[file:./wcpcpy.3.org][wcpcpy.3]]                       |
| [[file:./wcpncpy.3.org][wcpncpy.3]]                      |
| [[file:./wcrtomb.3.org][wcrtomb.3]]                      |
| [[file:./wcscasecmp.3.org][wcscasecmp.3]]                   |
| [[file:./wcscat.3.org][wcscat.3]]                       |
| [[file:./wcschr.3.org][wcschr.3]]                       |
| [[file:./wcscmp.3.org][wcscmp.3]]                       |
| [[file:./wcscpy.3.org][wcscpy.3]]                       |
| [[file:./wcscspn.3.org][wcscspn.3]]                      |
| [[file:./wcsdup.3.org][wcsdup.3]]                       |
| [[file:./wcslcat.3bsd.org][wcslcat.3bsd]]                   |
| [[file:./wcslcpy.3bsd.org][wcslcpy.3bsd]]                   |
| [[file:./wcslen.3.org][wcslen.3]]                       |
| [[file:./wcsncasecmp.3.org][wcsncasecmp.3]]                  |
| [[file:./wcsncat.3.org][wcsncat.3]]                      |
| [[file:./wcsncmp.3.org][wcsncmp.3]]                      |
| [[file:./wcsncpy.3.org][wcsncpy.3]]                      |
| [[file:./wcsnlen.3.org][wcsnlen.3]]                      |
| [[file:./wcsnrtombs.3.org][wcsnrtombs.3]]                   |
| [[file:./wcspbrk.3.org][wcspbrk.3]]                      |
| [[file:./wcsrchr.3.org][wcsrchr.3]]                      |
| [[file:./wcsrtombs.3.org][wcsrtombs.3]]                    |
| [[file:./wcsspn.3.org][wcsspn.3]]                       |
| [[file:./wcsstr.3.org][wcsstr.3]]                       |
| [[file:./wcstoimax.3.org][wcstoimax.3]]                    |
| [[file:./wcstok.3.org][wcstok.3]]                       |
| [[file:./wcstombs.3.org][wcstombs.3]]                     |
| [[file:./wcstoumax.3.org][wcstoumax.3]]                    |
| [[file:./wcswidth.3.org][wcswidth.3]]                     |
| [[file:./wctob.3.org][wctob.3]]                        |
| [[file:./wctomb.3.org][wctomb.3]]                       |
| [[file:./wctrans.3.org][wctrans.3]]                      |
| [[file:./wctype.3.org][wctype.3]]                       |
| [[file:./wcwidth.3.org][wcwidth.3]]                      |
| [[file:./WhitePixel.3.org][WhitePixel.3]]                   |
| [[file:./WhitePixelOfScreen.3.org][WhitePixelOfScreen.3]]           |
| [[file:./WidthMMOfScreen.3.org][WidthMMOfScreen.3]]              |
| [[file:./WidthOfScreen.3.org][WidthOfScreen.3]]                |
| [[file:./WildMidi_ClearError.3.org][WildMidi_ClearError.3]]          |
| [[file:./WildMidi_Close.3.org][WildMidi_Close.3]]               |
| [[file:./WildMidi_ConvertBufferToMidi.3.org][WildMidi_ConvertBufferToMidi.3]] |
| [[file:./WildMidi_ConvertToMidi.3.org][WildMidi_ConvertToMidi.3]]       |
| [[file:./WildMidi_FastSeek.3.org][WildMidi_FastSeek.3]]            |
| [[file:./WildMidi_GetError.3.org][WildMidi_GetError.3]]            |
| [[file:./WildMidi_GetInfo.3.org][WildMidi_GetInfo.3]]             |
| [[file:./WildMidi_GetLyric.3.org][WildMidi_GetLyric.3]]            |
| [[file:./WildMidi_GetMidiOutput.3.org][WildMidi_GetMidiOutput.3]]       |
| [[file:./WildMidi_GetOutput.3.org][WildMidi_GetOutput.3]]           |
| [[file:./WildMidi_GetString.3.org][WildMidi_GetString.3]]           |
| [[file:./WildMidi_GetVersion.3.org][WildMidi_GetVersion.3]]          |
| [[file:./WildMidi_Init.3.org][WildMidi_Init.3]]                |
| [[file:./WildMidi_InitVIO.3.org][WildMidi_InitVIO.3]]             |
| [[file:./WildMidi_MasterVolume.3.org][WildMidi_MasterVolume.3]]        |
| [[file:./WildMidi_Open.3.org][WildMidi_Open.3]]                |
| [[file:./WildMidi_OpenBuffer.3.org][WildMidi_OpenBuffer.3]]          |
| [[file:./WildMidi_SetCvtOption.3.org][WildMidi_SetCvtOption.3]]        |
| [[file:./WildMidi_SetOption.3.org][WildMidi_SetOption.3]]           |
| [[file:./WildMidi_Shutdown.3.org][WildMidi_Shutdown.3]]            |
| [[file:./WildMidi_SongSeek.3.org][WildMidi_SongSeek.3]]            |
| [[file:./Win32_DBIODBC.3pm.org][Win32_DBIODBC.3pm]]              |
| [[file:./wmemchr.3.org][wmemchr.3]]                      |
| [[file:./wmemcmp.3.org][wmemcmp.3]]                      |
| [[file:./wmemcpy.3.org][wmemcpy.3]]                      |
| [[file:./wmemmove.3.org][wmemmove.3]]                     |
| [[file:./wmempcpy.3.org][wmempcpy.3]]                     |
| [[file:./wmemset.3.org][wmemset.3]]                      |
| [[file:./wordexp.3.org][wordexp.3]]                      |
| [[file:./wordfree.3.org][wordfree.3]]                     |
| [[file:./wprintf.3.org][wprintf.3]]                      |
| [[file:./wresize.3x.org][wresize.3x]]                     |
| [[file:./WWW_AUR.3pm.org][WWW_AUR.3pm]]                    |
| [[file:./WWW_AUR_Iterator.3pm.org][WWW_AUR_Iterator.3pm]]           |
| [[file:./WWW_AUR_Login.3pm.org][WWW_AUR_Login.3pm]]              |
| [[file:./WWW_AUR_Maintainer.3pm.org][WWW_AUR_Maintainer.3pm]]         |
| [[file:./WWW_AUR_Package.3pm.org][WWW_AUR_Package.3pm]]            |
| [[file:./WWW_AUR_Package_File.3pm.org][WWW_AUR_Package_File.3pm]]       |
| [[file:./WWW_AUR_PKGBUILD.3pm.org][WWW_AUR_PKGBUILD.3pm]]           |
| [[file:./WWW_AUR_URI.3pm.org][WWW_AUR_URI.3pm]]                |
| [[file:./WWW_AUR_UserAgent.3pm.org][WWW_AUR_UserAgent.3pm]]          |
| [[file:./WWW_RobotRules.3pm.org][WWW_RobotRules.3pm]]             |
| [[file:./WWW_RobotRules_AnyDBM_File.3pm.org][WWW_RobotRules_AnyDBM_File.3pm]] |
