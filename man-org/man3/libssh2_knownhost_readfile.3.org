#+TITLE: Manpages - libssh2_knownhost_readfile.3
#+DESCRIPTION: Linux manpage for libssh2_knownhost_readfile.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_knownhost_readfile - parse a file of known hosts

* SYNOPSIS
#include <libssh2.h>

int libssh2_knownhost_readfile(LIBSSH2_KNOWNHOSTS *hosts, const char
*filename, int type);

* DESCRIPTION
Reads a collection of known hosts from a specified file and adds them to
the collection of known hosts.

/filename/ specifies which file to read

/type/ specifies what file type it is, and
/LIBSSH2_KNOWNHOST_FILE_OPENSSH/ is the only currently supported format.
This file is normally found named ~/.ssh/known_hosts

* RETURN VALUE
Returns a negative value, a regular libssh2 error code for errors, or a
positive number as number of parsed known hosts in the file.

* AVAILABILITY
Added in libssh2 1.2

* SEE ALSO
*libssh2_knownhost_init(3)* *libssh2_knownhost_free(3)*
*libssh2_knownhost_check(3)*
