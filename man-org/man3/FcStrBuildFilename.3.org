#+TITLE: Manpages - FcStrBuildFilename.3
#+DESCRIPTION: Linux manpage for FcStrBuildFilename.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcStrBuildFilename - Concatenate strings as a file path

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcChar8 * FcStrBuildFilename (const FcChar8 */path/*, ...);*

* DESCRIPTION
Creates a filename from the given elements of strings as file paths and
concatenate them with the appropriate file separator. Arguments must be
null-terminated. This returns a newly-allocated memory which should be
freed when no longer needed.
