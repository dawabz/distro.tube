#+TITLE: Manpages - acl_copy_entry.3
#+DESCRIPTION: Linux manpage for acl_copy_entry.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function copies the contents of the ACL entry indicated by the

descriptor to the existing ACL entry indicated by the

descriptor. The

and

descriptors may refer to entries in different ACLs.

If any of the following conditions occur, the

function returns

and sets

to the corresponding value:

The argument

or

is not a valid descriptor for an ACL entry.

The arguments

and

reference the same ACL entry.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
