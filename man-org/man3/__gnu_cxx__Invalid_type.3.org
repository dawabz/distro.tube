#+TITLE: Manpages - __gnu_cxx__Invalid_type.3
#+DESCRIPTION: Linux manpage for __gnu_cxx__Invalid_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::_Invalid_type

* SYNOPSIS
\\

=#include <pointer.h>=

* Detailed Description
The specialization on this type helps resolve the problem of reference
to void, and eliminates the need to specialize _Pointer_adapter for
cases of void*, const void*, and so on.

Definition at line *216* of file *pointer.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
