#+TITLE: Manpages - XkbLatchGroup.3
#+DESCRIPTION: Linux manpage for XkbLatchGroup.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbLatchGroup - Latches the keysym group

* SYNOPSIS
*Bool XkbLatchGroup* *( Display **/display/* ,* *unsigned int
*/device_spec/* ,* *unsigned int */group/* );*

* ARGUMENTS
- /display/ :: connection to the X server

- /device_spec/ :: device ID, or XkbUseCoreKbd

- /group/ :: index of the keysym group to latch

* DESCRIPTION
/XkbLatchGroup/ sends a request to the server to latch the specified
/group/ and does not wait for a reply. It returns True if the request
was sent and False otherwise.

Reference the keysym group indices with these symbolic constants:

TABLE

* RETURN VALUES
- True :: The /XkbLatchGroup/ function returns True if a request was
  sent to the server to latch the specified /group./

- False :: The /XkbLatchGroup/ function returns False if the request was
  not sent.
