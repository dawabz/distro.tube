#+TITLE: Manpages - gethostbyname2.3
#+DESCRIPTION: Linux manpage for gethostbyname2.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about gethostbyname2.3 is found in manpage for: [[../man3/gethostbyname.3][man3/gethostbyname.3]]