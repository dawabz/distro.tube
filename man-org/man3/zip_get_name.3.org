#+TITLE: Manpages - zip_get_name.3
#+DESCRIPTION: Linux manpage for zip_get_name.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function returns the name of the file at position

in

The name is in UTF-8 encoding unless

was specified (see below).

If

is set to

the original unchanged filename is returned. The returned string must
not be modified or freed, and becomes invalid when

is closed.

Additionally, the following

are supported:

Return the unmodified names as it is in the ZIP archive.

(Default.) Guess the encoding of the name in the ZIP archive and convert
it to UTF-8, if necessary. (Only CP-437 and UTF-8 are recognized.)

Follow the ZIP specification and expect CP-437 encoded names in the ZIP
archive (except if they are explicitly marked as UTF-8). Convert it to
UTF-8.

ASCII is a subset of both CP-437 and UTF-8.

Upon successful completion, a pointer to the name is returned.
Otherwise,

and the error code in

is set to indicate the error.

fails if:

refers to a file that has been deleted (see

is not a valid file index in

or

points to an added file and

is set.

Required memory could not be allocated.

was added in libzip 0.6. In libzip 0.10 the type of

was changed from

to

In libzip 0.11 the type of

was changed from

to

and
