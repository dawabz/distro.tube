#+TITLE: Manpages - std_logical_not.3
#+DESCRIPTION: Linux manpage for std_logical_not.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::logical_not< _Tp > - One of the *Boolean operations functors*.

* SYNOPSIS
\\

=#include <stl_function.h>=

Inherits *std::unary_function< _Tp, bool >*.

** Public Types
typedef _Tp *argument_type*\\
=argument_type= is the type of the argument

typedef bool *result_type*\\
=result_type= is the return type

** Public Member Functions
constexpr bool *operator()* (const _Tp &__x) const\\

* Detailed Description
** "template<typename _Tp>
\\
struct std::logical_not< _Tp >"One of the *Boolean operations functors*.

Definition at line *806* of file *stl_function.h*.

* Member Typedef Documentation
** typedef _Tp *std::unary_function*< _Tp , bool
>::*argument_type*= [inherited]=
=argument_type= is the type of the argument

Definition at line *108* of file *stl_function.h*.

** typedef bool *std::unary_function*< _Tp , bool
>::*result_type*= [inherited]=
=result_type= is the return type

Definition at line *111* of file *stl_function.h*.

* Member Function Documentation
** template<typename _Tp > constexpr bool *std::logical_not*< _Tp
>::operator() (const _Tp & __x) const= [inline]=, = [constexpr]=
Definition at line *810* of file *stl_function.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
