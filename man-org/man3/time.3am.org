#+TITLE: Manpages - time.3am
#+DESCRIPTION: Linux manpage for time.3am
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
time - time functions for gawk

* SYNOPSIS
@load "time"

time = gettimeofday()\\
ret = sleep(amount)

* CAUTION
This extension is deprecated in favor of the *timex* extension in the
/gawkextlib/ project. In the next major release of /gawk/, loading it
will issue a warning. It will be removed from the /gawk/ distribution in
the major release after the next one.

* DESCRIPTION
The /time/ extension adds two functions named *gettimeofday()* and
*sleep()*, as follows.

- *gettimeofday()* :: This function returns the number of seconds since
  the Epoch as a floating-point value. It should have subsecond
  precision. It returns -1 upon error and sets *ERRNO* to indicate the
  problem.

- *sleep(*/seconds/*)* :: This function attempts to sleep for the given
  amount of seconds, which may include a fractional portion. If
  /seconds/ is negative, or the attempt to sleep fails, then it returns
  -1 and sets *ERRNO*. Otherwise, the function should return 0 after
  sleeping for the indicated amount of time.

* EXAMPLE
#+begin_example
  @load "time"
  ...
  printf "It is now %g seconds since the Epoch\n", gettimeofday()
  printf "Pausing for a while... " ; sleep(2.5) ; print "done"
#+end_example

* SEE ALSO
/GAWK: Effective AWK Programming/, /filefuncs/(3am), /fnmatch/(3am),
/fork/(3am), /inplace/(3am), /ordchr/(3am), /readdir/(3am),
/readfile/(3am), /revoutput/(3am), /rwarray/(3am).

/gettimeofday/(2), /nanosleep/(2), /select/(2).

* AUTHOR
Arnold Robbins, *arnold@skeeve.com*.

* COPYING PERMISSIONS
Copyright © 2012, 2013, 2018, Free Software Foundation, Inc.

Permission is granted to make and distribute verbatim copies of this
manual page provided the copyright notice and this permission notice are
preserved on all copies.

Permission is granted to process this file through troff and print the
results, provided the printed document carries copying permission notice
identical to this one except for the removal of this paragraph (this
paragraph not being relevant to the printed manual page).

Permission is granted to copy and distribute modified versions of this
manual page under the conditions for verbatim copying, provided that the
entire resulting derived work is distributed under the terms of a
permission notice identical to this one.

Permission is granted to copy and distribute translations of this manual
page into another language, under the above conditions for modified
versions, except that this permission notice may be stated in a
translation approved by the Foundation.
