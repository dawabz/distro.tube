#+TITLE: Manpages - XtGetDisplays.3
#+DESCRIPTION: Linux manpage for XtGetDisplays.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtGetDisplays - retrieve a list of displays associated with an
application context

* SYNTAX
#include <X11/Intrinsic.h>

void XtGetDisplays(XtAppContext /app_context/, Display ***/dpy_return/,
Cardinal* /num_dpy_return/);

* ARGUMENTS
- app_context :: Specifies the application context.

- dpy_return :: Returns a list of open X display connections in the
  specified application context.

- num_dpy_return :: Returns the count of open X display connections in
  /dpy_return/.

* DESCRIPTION
To free the list of displays, use *XtFree*.

* SEE ALSO
\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
