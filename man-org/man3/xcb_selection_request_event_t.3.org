#+TITLE: Manpages - xcb_selection_request_event_t.3
#+DESCRIPTION: Linux manpage for xcb_selection_request_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_selection_request_event_t -

* SYNOPSIS
*#include <xcb/xproto.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_selection_request_event_t {
      uint8_t         response_type;
      uint8_t         pad0;
      uint16_t        sequence;
      xcb_timestamp_t time;
      xcb_window_t    owner;
      xcb_window_t    requestor;
      xcb_atom_t      selection;
      xcb_atom_t      target;
      xcb_atom_t      property;
  } xcb_selection_request_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_SELECTION_REQUEST/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- time :: NOT YET DOCUMENTED.

- owner :: NOT YET DOCUMENTED.

- requestor :: NOT YET DOCUMENTED.

- selection :: NOT YET DOCUMENTED.

- target :: NOT YET DOCUMENTED.

- property :: NOT YET DOCUMENTED.

* DESCRIPTION
* SEE ALSO
* AUTHOR
Generated from xproto.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
