#+TITLE: Manpages - sgetspent.3
#+DESCRIPTION: Linux manpage for sgetspent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sgetspent.3 is found in manpage for: [[../man3/getspnam.3][man3/getspnam.3]]