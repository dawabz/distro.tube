#+TITLE: Manpages - Alien_Build_Plugin_Build_Make.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Plugin_Build_Make.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Plugin::Build::Make - Make plugin for Alien::Build

* VERSION
version 2.44

* SYNOPSIS
use alienfile; # For a recipe that requires GNU Make plugin Build::Make
=> gmake;

* DESCRIPTION
By default Alien::Build provides a helper for the =make= that is used by
Perl and ExtUtils::MakeMaker itself. This is handy, because it is the
one make that you can mostly guarantee that you will have. Unfortunately
it may be a =make= that isn't supported by the library or tool that you
are trying to alienize. This is mostly a problem on Windows, where the
supported =make=s for years were Microsoft's =nmake= and Sun's =dmake=,
which many open source projects do not use. This plugin will alter the
alienfile recipe to use a different =make=. It may (as in the case of
=gmake= / Alien::gmake) automatically download and install an alienized
version of that =make= if it is not already installed.

This plugin should NOT be used with other plugins that replace the
=make= helper, like Alien::Build::Plugin::Build::CMake,
Alien::Build::Plugin::Build::Autoconf,
Alien::Build::Plugin::Build::MSYS. This plugin is intended instead for
projects that use vanilla makefiles of a specific type.

This plugin is for now distributed separately from Alien::Build, but the
intention is for it to soon become a core plugin for Alien::Build.

* PROPERTIES
** make_type
The make type needed by the alienfile recipe:

- dmake :: Sun's dmake.

- gmake :: GNU Make.

- nmake :: Microsoft's nmake. It comes with Visual C++.

- umake :: Any UNIX =make= Usually either BSD or GNU Make.

* HELPERS
** make
%{make}

This plugin may change the make helper used by your alienfile recipe.

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
