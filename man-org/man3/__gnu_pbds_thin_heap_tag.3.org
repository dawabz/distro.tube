#+TITLE: Manpages - __gnu_pbds_thin_heap_tag.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_thin_heap_tag.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::thin_heap_tag - Thin heap.

* SYNOPSIS
\\

=#include <tag_and_trait.hpp>=

Inherits *__gnu_pbds::priority_queue_tag*.

* Detailed Description
Thin heap.

Definition at line *186* of file *tag_and_trait.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
