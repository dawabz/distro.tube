#+TITLE: Manpages - tld_check_4.3
#+DESCRIPTION: Linux manpage for tld_check_4.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tld_check_4 - API function

* SYNOPSIS
*#include <tld.h>*

*int tld_check_4(const uint32_t * */in/*, size_t */inlen/*, size_t *
*/errpos/*, const Tld_table ** */overrides/*);*

* ARGUMENTS
- const uint32_t * in :: Array of unicode code points to process. Does
  not need to be zero terminated.

- size_t inlen :: Number of unicode code points.

- size_t * errpos :: Position of offending character is returned here.

- const Tld_table ** overrides :: A *Tld_table* array of additional
  domain restriction structures that complement and supersede the
  built-in information.

* DESCRIPTION
Test each of the code points in /in/ for whether or not they are allowed
by the information in /overrides/ or by the built-in TLD restriction
data. When data for the same TLD is available both internally and in
/overrides/ , the information in /overrides/ takes precedence. If
several entries for a specific TLD are found, the first one is used. If
/overrides/ is *NULL*, only the built-in information is used. The
position of the first offending character is returned in /errpos/ .

Return value: Returns the *Tld_rc* value *TLD_SUCCESS* if all code
points are valid or when /tld/ is null, *TLD_INVALID* if a character is
not allowed, or additional error codes on general failure conditions.

* REPORTING BUGS
Report bugs to <help-libidn@gnu.org>.\\
General guidelines for reporting bugs: http://www.gnu.org/gethelp/\\
GNU Libidn home page: http://www.gnu.org/software/libidn/

* COPYRIGHT
Copyright © 2002-2021 Simon Josefsson.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libidn* is maintained as a Texinfo manual.
If the *info* and *libidn* programs are properly installed at your site,
the command

#+begin_quote
  *info libidn*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *http://www.gnu.org/software/libidn/manual/*
#+end_quote
