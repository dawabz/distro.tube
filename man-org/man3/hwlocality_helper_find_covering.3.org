#+TITLE: Manpages - hwlocality_helper_find_covering.3
#+DESCRIPTION: Linux manpage for hwlocality_helper_find_covering.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwlocality_helper_find_covering - Finding Objects covering at least CPU
set

* SYNOPSIS
\\

** Functions
static *hwloc_obj_t* *hwloc_get_child_covering_cpuset*
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set, *hwloc_obj_t*
parent)\\

static *hwloc_obj_t* *hwloc_get_obj_covering_cpuset* (*hwloc_topology_t*
topology, *hwloc_const_cpuset_t* set)\\

static *hwloc_obj_t* *hwloc_get_next_obj_covering_cpuset_by_depth*
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set, int depth,
*hwloc_obj_t* prev)\\

static *hwloc_obj_t* *hwloc_get_next_obj_covering_cpuset_by_type*
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set,
*hwloc_obj_type_t* type, *hwloc_obj_t* prev)\\

* Detailed Description
* Function Documentation
** static *hwloc_obj_t* hwloc_get_child_covering_cpuset
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set, *hwloc_obj_t*
parent)= [inline]=, = [static]=
Get the child covering at least CPU set =set=.

*Returns*

#+begin_quote
  =NULL= if no child matches or if =set= is empty.
#+end_quote

*Note*

#+begin_quote
  This function cannot work if parent does not have a CPU set (I/O or
  Misc objects).
#+end_quote

** static *hwloc_obj_t* hwloc_get_next_obj_covering_cpuset_by_depth
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set, int depth,
*hwloc_obj_t* prev)= [inline]=, = [static]=
Iterate through same-depth objects covering at least CPU set =set=. If
object =prev= is =NULL=, return the first object at depth =depth=
covering at least part of CPU set =set=. The next invokation should pass
the previous return value in =prev= so as to obtain the next object
covering at least another part of =set=.

*Note*

#+begin_quote
  This function cannot work if objects at the given depth do not have
  CPU sets (I/O or Misc objects).
#+end_quote

** static *hwloc_obj_t* hwloc_get_next_obj_covering_cpuset_by_type
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set,
*hwloc_obj_type_t* type, *hwloc_obj_t* prev)= [inline]=, = [static]=
Iterate through same-type objects covering at least CPU set =set=. If
object =prev= is =NULL=, return the first object of type =type= covering
at least part of CPU set =set=. The next invokation should pass the
previous return value in =prev= so as to obtain the next object of type
=type= covering at least another part of =set=.

If there are no or multiple depths for type =type=, =NULL= is returned.
The caller may fallback to
*hwloc_get_next_obj_covering_cpuset_by_depth()* for each depth.

*Note*

#+begin_quote
  This function cannot work if objects of the given type do not have CPU
  sets (I/O or Misc objects).
#+end_quote

** static *hwloc_obj_t* hwloc_get_obj_covering_cpuset
(*hwloc_topology_t* topology, *hwloc_const_cpuset_t* set)= [inline]=,
= [static]=
Get the lowest object covering at least CPU set =set=.

*Returns*

#+begin_quote
  =NULL= if no object matches or if =set= is empty.
#+end_quote

* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
