#+TITLE: Manpages - XIGetFocus.3
#+DESCRIPTION: Linux manpage for XIGetFocus.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XIGetFocus.3 is found in manpage for: [[../man3/XISetFocus.3][man3/XISetFocus.3]]