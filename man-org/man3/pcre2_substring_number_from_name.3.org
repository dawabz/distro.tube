#+TITLE: Manpages - pcre2_substring_number_from_name.3
#+DESCRIPTION: Linux manpage for pcre2_substring_number_from_name.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  int pcre2_substring_number_from_name(const pcre2_code *code,
   PCRE2_SPTR name);
#+end_example

* DESCRIPTION
This convenience function finds the number of a named substring
capturing parenthesis in a compiled pattern, provided that it is a
unique name. The function arguments are:

/code/ Compiled regular expression /name/ Name whose number is required

The yield of the function is the number of the parenthesis if the name
is found, or PCRE2_ERROR_NOSUBSTRING if it is not found. When duplicate
names are allowed (PCRE2_DUPNAMES is set), if the name is not unique,
PCRE2_ERROR_NOUNIQUESUBSTRING is returned. You can obtain the list of
numbers with the same name by calling
*pcre2_substring_nametable_scan()*.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
