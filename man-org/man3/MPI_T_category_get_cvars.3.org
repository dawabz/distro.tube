#+TITLE: Manpages - MPI_T_category_get_cvars.3
#+DESCRIPTION: Linux manpage for MPI_T_category_get_cvars.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_T_category_get_cvars* - Query which control variables are in a
category

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_T_category_get_cvars(int cat_index, int len, int indices[])
#+end_example

* INPUT PARAMETERS
- cat_index :: Index of the category to be queried.

- len :: The length of the indices array.

* OUTPUT PARAMETERS
- indices :: An integer array of size len, indicating control variable
  indices.

* DESCRIPTION
MPI_T_category_get_cvars can be used to query which control variables
are contained in a particular category.

* ERRORS
MPI_T_category_get_cvars() will fail if:

- [MPI_T_ERR_NOT_INITIALIZED] :: The MPI Tools interface not initialized

- [MPI_T_ERR_INVALID_INDEX] :: The category index is invalid
