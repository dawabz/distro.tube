#+TITLE: Manpages - pam_close_session.3
#+DESCRIPTION: Linux manpage for pam_close_session.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_close_session - terminate PAM session management

* SYNOPSIS
#+begin_example
  #include <security/pam_appl.h>
#+end_example

*int pam_close_session(pam_handle_t **/pamh/*, int */flags/*);*

* DESCRIPTION
The *pam_close_session* function is used to indicate that an
authenticated session has ended. The session should have been created
with a call to *pam_open_session*(3).

It should be noted that the effective uid, *geteuid*(2). of the
application should be of sufficient privilege to perform such tasks as
unmounting the users home directory for example.

The flags argument is the binary or of zero or more of the following
values:

PAM_SILENT

#+begin_quote
  Do not emit any messages.
#+end_quote

* RETURN VALUES
PAM_ABORT

#+begin_quote
  General failure.
#+end_quote

PAM_BUF_ERR

#+begin_quote
  Memory buffer error.
#+end_quote

PAM_SESSION_ERR

#+begin_quote
  Session failure.
#+end_quote

PAM_SUCCESS

#+begin_quote
  Session was successful terminated.
#+end_quote

* SEE ALSO
*pam_open_session*(3), *pam_strerror*(3)
