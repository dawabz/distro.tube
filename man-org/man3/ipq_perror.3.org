#+TITLE: Manpages - ipq_perror.3
#+DESCRIPTION: Linux manpage for ipq_perror.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ipq_perror.3 is found in manpage for: [[../man3/ipq_errstr.3][man3/ipq_errstr.3]]