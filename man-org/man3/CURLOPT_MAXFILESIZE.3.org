#+TITLE: Manpages - CURLOPT_MAXFILESIZE.3
#+DESCRIPTION: Linux manpage for CURLOPT_MAXFILESIZE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_MAXFILESIZE - maximum file size allowed to download

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_MAXFILESIZE, long size);

* DESCRIPTION
Pass a long as parameter. This allows you to specify the maximum /size/
(in bytes) of a file to download. If the file requested is found larger
than this value, the transfer will not start and
/CURLE_FILESIZE_EXCEEDED/ will be returned.

The file size is not always known prior to download, and for such files
this option has no effect even if the file transfer ends up being larger
than this given limit.

If you want a limit above 2GB, use /CURLOPT_MAXFILESIZE_LARGE(3)/.

* DEFAULT
None

* PROTOCOLS
FTP, HTTP and MQTT

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    CURLcode ret;
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    /* refuse to download if larger than 1000 bytes! */
    curl_easy_setopt(curl, CURLOPT_MAXFILESIZE, 1000L);
    ret = curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Always

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*CURLOPT_MAXFILESIZE_LARGE*(3), *CURLOPT_MAX_RECV_SPEED_LARGE*(3),
