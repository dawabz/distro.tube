#+TITLE: Manpages - FcPatternRemove.3
#+DESCRIPTION: Linux manpage for FcPatternRemove.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcPatternRemove - Remove one object of the specified type from the
pattern

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcPatternRemove (FcPattern */p/*, const char **/object/*, int
*/id/*);*

* DESCRIPTION
Removes the value associated with the property `object' at position
`id', returning whether the property existed and had a value at that
position or not.
