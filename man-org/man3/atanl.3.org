#+TITLE: Manpages - atanl.3
#+DESCRIPTION: Linux manpage for atanl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about atanl.3 is found in manpage for: [[../man3/atan.3][man3/atan.3]]