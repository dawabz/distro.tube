#+TITLE: Manpages - SDL_SysWMEvent.3
#+DESCRIPTION: Linux manpage for SDL_SysWMEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_SysWMEvent - Platform-dependent window manager event.

* DESCRIPTION
The system window manager event contains a pointer to system-specific
information about unknown window manager events. If you enable this
event using *SDL_EventState()*, it will be generated whenever unhandled
events are received from the window manager. This can be used, for
example, to implement cut-and-paste in your application.

#+begin_example
  typedef struct {
           Uint8 type;   /* Always SDL_SysWM */
   } SDL_SysWMEvent;
#+end_example

If you want to obtain system-specific information about the window
manager, you can fill the version member of a *SDL_SysWMinfo* structure
(details can be found in *SDL_syswm.h*, which must be included) using
the *SDL_VERSION()* macro found in *SDL_version.h*, and pass it to the
function:

*int SDL_GetWMInfo*(*SDL_SysWMinfo *info*);

* SEE ALSO
*SDL_EventState*
