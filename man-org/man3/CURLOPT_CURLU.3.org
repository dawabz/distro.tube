#+TITLE: Manpages - CURLOPT_CURLU.3
#+DESCRIPTION: Linux manpage for CURLOPT_CURLU.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_CURLU - URL in CURLU * format

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CURLU, void *pointer);

* DESCRIPTION
Pass in a pointer to the /URL/ to work with. The parameter should be a
CURLU *. Setting /CURLOPT_CURLU(3)/ will explicitly override
/CURLOPT_URL(3)/.

/CURLOPT_URL(3)/ or /CURLOPT_CURLU(3)/ *must* be set before a transfer
is started.

libcurl will use this handle and its contents read-only and will not
change its contents. An application can update the contents of the URL
handle after a transfer is done and if the same handle is then used in a
subsequent request the updated contents will then be used.

* DEFAULT
The default value of this parameter is NULL.

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *handle = curl_easy_init();
  CURLU *urlp = curl_url();
  int res = 0;
  if(curl) {

    res = curl_url_set(urlp, CURLUPART_URL, "https://example.com", 0);

    curl_easy_setopt(handle, CURLOPT_CURLU, urlp);

    ret = curl_easy_perform(handle);

    curl_url_cleanup(urlp);
    curl_easy_cleanup(handle);
  }
#+end_example

* AVAILABILITY
Added in 7.63.0.

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_URL*(3), *curl_url*(3), *curl_url_get*(3), *curl_url_set*(3),
*curl_url_dup*(3), *curl_url_cleanup*(3), *curl_url_strerror*(3)
