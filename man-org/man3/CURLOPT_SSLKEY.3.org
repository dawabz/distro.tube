#+TITLE: Manpages - CURLOPT_SSLKEY.3
#+DESCRIPTION: Linux manpage for CURLOPT_SSLKEY.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_SSLKEY - private keyfile for TLS and SSL client cert

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSLKEY, char *keyfile);

* DESCRIPTION
Pass a pointer to a null-terminated string as parameter. The string
should be the file name of your private key. The default format is "PEM"
and can be changed with /CURLOPT_SSLKEYTYPE(3)/.

(iOS and Mac OS X only) This option is ignored if curl was built against
Secure Transport. Secure Transport expects the private key to be already
present in the keychain or PKCS#12 file containing the certificate.

The application does not have to keep the string around after setting
this option.

* DEFAULT
NULL

* PROTOCOLS
All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    curl_easy_setopt(curl, CURLOPT_SSLCERT, "client.pem");
    curl_easy_setopt(curl, CURLOPT_SSLKEY, "key.pem");
    curl_easy_setopt(curl, CURLOPT_KEYPASSWD, "s3cret");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
If built TLS enabled.

* RETURN VALUE
Returns CURLE_OK if TLS is supported, CURLE_UNKNOWN_OPTION if not, or
CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_SSLKEYTYPE*(3), *CURLOPT_SSLCERT*(3),
