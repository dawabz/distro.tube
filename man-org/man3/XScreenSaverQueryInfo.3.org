#+TITLE: Manpages - XScreenSaverQueryInfo.3
#+DESCRIPTION: Linux manpage for XScreenSaverQueryInfo.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XScreenSaverQueryInfo.3 is found in manpage for: [[../man3/Xss.3][man3/Xss.3]]