#+TITLE: Manpages - xcb_input_get_device_dont_propagate_list_reply.3
#+DESCRIPTION: Linux manpage for xcb_input_get_device_dont_propagate_list_reply.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_input_get_device_dont_propagate_list_reply.3 is found in manpage for: [[../man3/xcb_input_get_device_dont_propagate_list.3][man3/xcb_input_get_device_dont_propagate_list.3]]