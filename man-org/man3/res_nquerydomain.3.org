#+TITLE: Manpages - res_nquerydomain.3
#+DESCRIPTION: Linux manpage for res_nquerydomain.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about res_nquerydomain.3 is found in manpage for: [[../man3/resolver.3][man3/resolver.3]]