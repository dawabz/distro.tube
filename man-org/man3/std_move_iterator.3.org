#+TITLE: Manpages - std_move_iterator.3
#+DESCRIPTION: Linux manpage for std_move_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::move_iterator< _Iterator >

* SYNOPSIS
\\

=#include <stl_iterator.h>=

** Public Types
typedef __traits_type::difference_type *difference_type*\\

typedef __traits_type::iterator_category *iterator_category*\\

using *iterator_type* = _Iterator\\

typedef _Iterator *pointer*\\

typedef *conditional*< *is_reference*< __base_ref >::value,
typenameremove_reference< __base_ref >::type &&, __base_ref >::type
*reference*\\

typedef __traits_type::value_type *value_type*\\

** Public Member Functions
template<typename _Iter > constexpr *move_iterator* (const
*move_iterator*< _Iter > &__i)\\

constexpr *move_iterator* (iterator_type __i)\\

constexpr iterator_type *base* () const\\

constexpr reference *operator** () const\\

constexpr *move_iterator* *operator+* (difference_type __n) const\\

constexpr *move_iterator* & *operator++* ()\\

constexpr *move_iterator* *operator++* (int)\\

constexpr *move_iterator* & *operator+=* (difference_type __n)\\

constexpr *move_iterator* *operator-* (difference_type __n) const\\

constexpr *move_iterator* & *operator--* ()\\

constexpr *move_iterator* *operator--* (int)\\

constexpr *move_iterator* & *operator-=* (difference_type __n)\\

constexpr pointer *operator->* () const\\

template<typename _Iter > constexpr *move_iterator* & *operator=* (const
*move_iterator*< _Iter > &__i)\\

constexpr reference *operator[]* (difference_type __n) const\\

* Detailed Description
** "template<typename _Iterator>
\\
class std::move_iterator< _Iterator >"Class template move_iterator is an
iterator adapter with the same behavior as the underlying iterator
except that its dereference operator implicitly converts the value
returned by the underlying iterator's dereference operator to an rvalue
reference. Some generic algorithms can be called with move iterators to
replace copying with moving.

Definition at line *1333* of file *bits/stl_iterator.h*.

* Member Typedef Documentation
** template<typename _Iterator > typedef __traits_type::difference_type
*std::move_iterator*< _Iterator >::difference_type
Definition at line *1369* of file *bits/stl_iterator.h*.

** template<typename _Iterator > typedef
__traits_type::iterator_category *std::move_iterator*< _Iterator
>::iterator_category
Definition at line *1367* of file *bits/stl_iterator.h*.

** template<typename _Iterator > using *std::move_iterator*< _Iterator
>::iterator_type = _Iterator
Definition at line *1357* of file *bits/stl_iterator.h*.

** template<typename _Iterator > typedef _Iterator *std::move_iterator*<
_Iterator >::pointer
Definition at line *1371* of file *bits/stl_iterator.h*.

** template<typename _Iterator > typedef
*conditional*<*is_reference*<__base_ref>::value,typenameremove_reference<__base_ref>::type&&,__base_ref>::type
*std::move_iterator*< _Iterator >::reference
Definition at line *1376* of file *bits/stl_iterator.h*.

** template<typename _Iterator > typedef __traits_type::value_type
*std::move_iterator*< _Iterator >::value_type
Definition at line *1368* of file *bits/stl_iterator.h*.

* Constructor & Destructor Documentation
** template<typename _Iterator > constexpr *std::move_iterator*<
_Iterator >::*move_iterator* ()= [inline]=, = [constexpr]=
Definition at line *1380* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *std::move_iterator*<
_Iterator >::*move_iterator* (iterator_type __i)= [inline]=,
= [explicit]=, = [constexpr]=
Definition at line *1384* of file *bits/stl_iterator.h*.

** template<typename _Iterator > template<typename _Iter > constexpr
*std::move_iterator*< _Iterator >::*move_iterator* (const
*move_iterator*< _Iter > & __i)= [inline]=, = [constexpr]=
Definition at line *1392* of file *bits/stl_iterator.h*.

* Member Function Documentation
** template<typename _Iterator > constexpr iterator_type
*std::move_iterator*< _Iterator >::base () const= [inline]=,
= [constexpr]=
Definition at line *1409* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr reference
*std::move_iterator*< _Iterator >::operator* () const= [inline]=,
= [constexpr]=
Definition at line *1425* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *move_iterator*
*std::move_iterator*< _Iterator >::operator+ (difference_type __n)
const= [inline]=, = [constexpr]=
Definition at line *1473* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *move_iterator* &
*std::move_iterator*< _Iterator >::operator++ ()= [inline]=,
= [constexpr]=
Definition at line *1437* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *move_iterator*
*std::move_iterator*< _Iterator >::operator++ (int)= [inline]=,
= [constexpr]=
Definition at line *1444* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *move_iterator* &
*std::move_iterator*< _Iterator >::operator+= (difference_type
__n)= [inline]=, = [constexpr]=
Definition at line *1477* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *move_iterator*
*std::move_iterator*< _Iterator >::operator- (difference_type __n)
const= [inline]=, = [constexpr]=
Definition at line *1484* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *move_iterator* &
*std::move_iterator*< _Iterator >::operator-- ()= [inline]=,
= [constexpr]=
Definition at line *1458* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *move_iterator*
*std::move_iterator*< _Iterator >::operator-- (int)= [inline]=,
= [constexpr]=
Definition at line *1465* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr *move_iterator* &
*std::move_iterator*< _Iterator >::operator-= (difference_type
__n)= [inline]=, = [constexpr]=
Definition at line *1488* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr pointer *std::move_iterator*<
_Iterator >::operator-> () const= [inline]=, = [constexpr]=
Definition at line *1433* of file *bits/stl_iterator.h*.

** template<typename _Iterator > template<typename _Iter > constexpr
*move_iterator* & *std::move_iterator*< _Iterator >::operator= (const
*move_iterator*< _Iter > & __i)= [inline]=, = [constexpr]=
Definition at line *1401* of file *bits/stl_iterator.h*.

** template<typename _Iterator > constexpr reference
*std::move_iterator*< _Iterator >::operator[] (difference_type __n)
const= [inline]=, = [constexpr]=
Definition at line *1495* of file *bits/stl_iterator.h*.

* Friends And Related Function Documentation
** template<typename _Iterator > template<typename _Iter2 > friend class
*move_iterator*= [friend]=
Definition at line *1346* of file *bits/stl_iterator.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
