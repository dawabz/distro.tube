#+TITLE: Manpages - auparse_get_field_str.3
#+DESCRIPTION: Linux manpage for auparse_get_field_str.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_get_field_str - get current field's value

* SYNOPSIS
*#include <auparse.h>*

const char *auparse_get_field_str(auparse_state_t *au);

* DESCRIPTION
auparse_get_field_str allows access to the value in the current field of
the current record in the current event.

* RETURN VALUE
Returns NULL if an error occurs; otherwise, a pointer to the field's
value.

* SEE ALSO
*auparse_get_field_name*(3), *auparse_get_field_int*(3),
*auparse_interpret_field*(3), *auparse_next_field*(3).

* AUTHOR
Steve Grubb
