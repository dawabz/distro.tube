#+TITLE: Manpages - Test2_EventFacet_Meta.3perl
#+DESCRIPTION: Linux manpage for Test2_EventFacet_Meta.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test2::EventFacet::Meta - Facet for meta-data

* DESCRIPTION
This facet can contain any random meta-data that has been attached to
the event.

* METHODS AND FIELDS
Any/all fields and accessors are autovivified into existence. There is
no way to know what metadata may be added, so any is allowed.

- $anything = $meta->{anything} :: 

- $anything = $meta->anything() :: 

* SOURCE
The source code repository for Test2 can be found at
/http://github.com/Test-More/test-more//.

* MAINTAINERS
- Chad Granum <exodist@cpan.org> :: 

* AUTHORS
- Chad Granum <exodist@cpan.org> :: 

* COPYRIGHT
Copyright 2020 Chad Granum <exodist@cpan.org>.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See /http://dev.perl.org/licenses//
