#+TITLE: Manpages - libssh2_sftp_readlink.3
#+DESCRIPTION: Linux manpage for libssh2_sftp_readlink.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_sftp_readlink - convenience macro for
/libssh2_sftp_symlink_ex(3)/

* SYNOPSIS
#+begin_example
  #include <libssh2.h>

  #define libssh2_sftp_readlink(sftp, path, target, maxlen) \
      libssh2_sftp_symlink_ex((sftp), (path), strlen(path), (target), (maxlen), \
      LIBSSH2_SFTP_READLINK)
#+end_example

* DESCRIPTION
This is a macro defined in a public libssh2 header file that is using
the underlying function /libssh2_sftp_symlink_ex(3)/.

* RETURN VALUE
See /libssh2_sftp_symlink_ex(3)/

* ERRORS
See /libssh2_sftp_symlink_ex(3)/

* SEE ALSO
*libssh2_sftp_symlink_ex(3)*
