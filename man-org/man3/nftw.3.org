#+TITLE: Manpages - nftw.3
#+DESCRIPTION: Linux manpage for nftw.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about nftw.3 is found in manpage for: [[../man3/ftw.3][man3/ftw.3]]