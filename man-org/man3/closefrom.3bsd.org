#+TITLE: Manpages - closefrom.3bsd
#+DESCRIPTION: Linux manpage for closefrom.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

The

system call deletes all open file descriptors greater than or equal to

from the per-process object reference table. Any errors encountered
while closing file descriptors are ignored.

The

function first appeared in
