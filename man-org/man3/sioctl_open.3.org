#+TITLE: Manpages - sioctl_open.3
#+DESCRIPTION: Linux manpage for sioctl_open.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Audio devices may expose a number of controls, like the playback volume
control. Each control has an integer

and an integer

Some values are boolean and can only be switched to either 0 (off) or 1
(on). Any control may be changed by submitting a new value to its
address. When values change, asynchronous notifications are sent.

Control descriptions are available, allowing them to be grouped and
represented in a human readable form.

First the application must call the

function to obtain a handle that will be passed as the

argument to other functions.

The

parameter gives the device string discussed in

In most cases it should be set to SIO_DEVANY to allow the user to select
it using the

environment variable. The

parameter is a bitmap of the

and

constants indicating whether control values can be read and modified
respectively.

If the

argument is 1, then the

function (see below) may fail instead of blocking and the

function doesn't block.

The

function closes the control device and frees any allocated resources
associated with the handle.

The

function can be used to obtain the description of all available controls
and their initial values. It registers a callback function that is
immediately invoked for all controls. It is called once with a

argument to indicate that the full description was sent and that the
caller has a consistent representation of the control set.

Then, whenever a control description changes, the callback is invoked
with the updated information followed by a call with a

argument.

Controls are described by the

structure as follows:

struct sioctl_node { char name[SIOCTL_NAMEMAX]; /* ex. "spkr" */ int
unit; /* optional number or -1 */ };

struct sioctl_desc { unsigned int addr; /* control address */ #define
SIOCTL_NONE 0 /* deleted */ #define SIOCTL_NUM 2 /* integer in the
maxval range */ #define SIOCTL_SW 3 /* on/off switch (1 or 0) */ #define
SIOCTL_VEC 4 /* number, element of vector */ #define SIOCTL_LIST 5 /*
switch, element of a list */ #define SIOCTL_SEL 6 /* element of a
selector */ unsigned int type; /* one of above */ char
func[SIOCTL_NAMEMAX]; /* function name, ex. "level" */ char
group[SIOCTL_NAMEMAX]; /* group this control belongs to */ struct
sioctl_node node0; /* affected node */ struct sioctl_node node1; /* dito
for SIOCTL_{VEC,LIST,SEL} */ unsigned int maxval; /* max value */ };

The

attribute is the control address, usable with

to set its value.

The

attribute indicates what the structure describes. Possible types are:

A previously valid control was deleted.

An integer control in the range from 0 to

inclusive. For instance the volume of the speaker.

A boolean control. For instance the switch to mute the speaker.

Element of an array of integer controls. For instance the knob to
control the amount of signal flowing from the line input to the speaker.

An element of an array of boolean switches. For instance the line-in
position of the speaker source selector.

Same as

but exactly one element is selected at a time.

The

attribute is the name of the parameter being controlled. There may be no
parameters of different types with the same name.

The

and

attributes indicate the names of the controlled nodes, typically
channels of audio streams.

is meaningful for

and

only.

Names in the

and

attributes and

are strings usable as unique identifiers within the given

The

attribute indicates the maximum value of this control. For boolean
control types it is set to 1.

Controls are changed with the

function, by giving the index of the control and the new value. The

function can be used to register a callback which will be invoked
whenever a control changes. Integer values are in the range from 0 to

The

function fills the array

of

structures, used by

with

the latter is a bit-mask of

and

constants.

returns the number of

structures filled. The

function returns the bit-mask set by

in the

array of

structures. If

is set,

can be called without blocking.

may be set if an error occurs, even if it is not selected with

is not used yet.

The

function returns the number of

structures the caller must preallocate in order to be sure that

will never overrun.

The default

device used by

These functions first appeared in
