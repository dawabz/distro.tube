#+TITLE: Manpages - __gnu_cxx___common_pool_policy.3
#+DESCRIPTION: Linux manpage for __gnu_cxx___common_pool_policy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::__common_pool_policy< _PoolTp, _Thread > - Policy for shared
__pool objects.

* SYNOPSIS
\\

=#include <mt_allocator.h>=

Inherits __gnu_cxx::__common_pool_base< _PoolTp, _Thread >.

* Detailed Description
** "template<template< bool > class _PoolTp, bool _Thread>
\\
struct __gnu_cxx::__common_pool_policy< _PoolTp, _Thread >"Policy for
shared __pool objects.

Definition at line *459* of file *mt_allocator.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
