#+TITLE: Manpages - ieee1284_open.3
#+DESCRIPTION: Linux manpage for ieee1284_open.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ieee1284_open - open a port

* SYNOPSIS
#+begin_example
  #include <ieee1284.h>
#+end_example

*int ieee1284_open(struct parport **/port/*, int */flags/*, int
**/capabilities/*);*

* DESCRIPTION
In order to begin using a port it must be opened. Any initial set-up of
the port is done at this stage. When an open port is no longer needed it
should be closed with *ieee1284_close*(3).

The possible /flags/ are:

*F1284_EXCL*

#+begin_quote
  This device cannot share the port with any other device. If this is
  the case it must be declared at this stage, so that other drivers
  trying to access the port know not to bother; otherwise they will wait
  until this driver releases the port, i.e. never.

  The iopl/dev-port access methods dont support this yet, but the ppdev
  ones do.
#+end_quote

If /capabilities/ is not *NULL* it must point to storage for an *int*,
which will be treated as a set of flags, one per bit, which the library
sets or clears as appropriate. If a capability is present it will be
used when asked for. They are:

*CAP1284_RAW*

#+begin_quote
  Pin-level access is available. If this capability is present then the
  following functions are effective: *ieee1284_write_data*,
  *ieee1284_read_status*, *ieee1284_wait_status*,
  *ieee1284_write_control*, *ieee1284_read_control*,
  *ieee1284_frob_control*.
#+end_quote

*CAP1284_NIBBLE*

#+begin_quote
  There is an implementation of nibble mode for this port.
#+end_quote

*CAP1284_BYTE*

#+begin_quote
  There is an implementation of byte mode for this port.
#+end_quote

*CAP1284_COMPAT*

#+begin_quote
  There is an implementation of compatibility mode for this port.
#+end_quote

*CAP1284_ECP*

#+begin_quote
  There is a hardware implementation of ECP mode for this port.
#+end_quote

*CAP1284_ECPRLE*

#+begin_quote
  There is an RLE-aware implementation of ECP mode for this port (the
  *F1284_RLE* flag is recognised by the ECP transfer functions).
#+end_quote

*CAP1284_ECPSWE*

#+begin_quote
  There is a software implementation of ECP mode for this port.
#+end_quote

*CAP1284_BECP*

#+begin_quote
  There is an implementation of bounded ECP mode for this port.
#+end_quote

*CAP1284_EPP*

#+begin_quote
  There is a hardware implementation of EPP mode for this port.
#+end_quote

*CAP1284_EPPSWE*

#+begin_quote
  There is a software implementation of EPP mode for this port.
#+end_quote

*CAP1284_IRQ*

#+begin_quote
  An interrupt line is configured for this port and interrupt
  notifications can be received using *ieee1284_get_irq_fd*(3).
#+end_quote

*CAP1284_DMA*

#+begin_quote
  A DMA channel is configured for this port.
#+end_quote

* RETURN VALUE
*E1284_OK*

#+begin_quote
  The port is now opened.
#+end_quote

*E1284_INIT*

#+begin_quote
  There was a problem during port initialization. This could be because
  another driver has opened the port exclusively, or some other reason.
#+end_quote

*E1284_NOMEM*

#+begin_quote
  There is not enough memory.
#+end_quote

*E1284_NOTAVAIL*

#+begin_quote
  One or more of the supplied flags is not supported by this type of
  port.
#+end_quote

*E1284_INVALIDPORT*

#+begin_quote
  The /port/ parameter is invalid (for instance, the /port/ may already
  be open).
#+end_quote

*E1284_SYS*

#+begin_quote
  There was a problem at the operating system level. The global variable
  /errno/ has been set appropriately.
#+end_quote

* SEE ALSO
*ieee1284_close*(3)

* AUTHOR
*Tim Waugh* <twaugh@redhat.com>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2001-2003 Tim Waugh\\
