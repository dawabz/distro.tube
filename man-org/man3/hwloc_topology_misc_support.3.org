#+TITLE: Manpages - hwloc_topology_misc_support.3
#+DESCRIPTION: Linux manpage for hwloc_topology_misc_support.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwloc_topology_misc_support

* SYNOPSIS
\\

=#include <hwloc.h>=

** Data Fields
unsigned char *imported_support*\\

* Detailed Description
Flags describing miscellaneous features.

* Field Documentation
** unsigned char hwloc_topology_misc_support::imported_support
Support was imported when importing another topology, see
*HWLOC_TOPOLOGY_FLAG_IMPORT_SUPPORT*.

* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
