#+TITLE: Manpages - zip_name_locate.3
#+DESCRIPTION: Linux manpage for zip_name_locate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function returns the index of the file named

in

If

does not contain a file with that name, -1 is returned. The

are specified by

the following values, or 0 for none of them.

Ignore case distinctions. (Will only work well if the file names are
ASCII.) With this flag,

will be slow for archives with many files.

Ignore directory part of file name in archive. With this flag,

will be slow for archives with many files.

Compare against the unmodified names as it is in the ZIP archive.

(Default.) Guess the encoding of the name in the ZIP archive and convert
it to UTF-8, if necessary, before comparing. (Only CP-437 and UTF-8 are
recognized.)

Follow the ZIP specification and expect CP-437 encoded names in the ZIP
archive (except if they are explicitly marked as UTF-8). Convert it to
UTF-8 before comparing.

ASCII is a subset of both CP-437 and UTF-8.

returns the index of the file named

or -1, if

does not contain an entry of that name.

fails if:

One of the arguments is invalid.

Required memory could not be allocated.

No entry of the name

is found in the archive.

was added in libzip 0.6. In libzip 0.11 the return type was changed from

to

In libzip 0.11 the type of

was changed from

to

and
