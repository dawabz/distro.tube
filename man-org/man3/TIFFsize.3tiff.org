#+TITLE: Manpages - TIFFsize.3tiff
#+DESCRIPTION: Linux manpage for TIFFsize.3tiff
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
TIFFScanlineSize, TIFFRasterScanlineSize, - return the size of various
items associated with an open

file

* SYNOPSIS
*#include <tiffio.h>*

*tsize_t TIFFRasterScanlineSize(TIFF **/tif/*)*\\
*tsize_t TIFFScanlineSize(TIFF **/tif/*)*

* DESCRIPTION
/TIFFScanlineSize/ returns the size in bytes of a row of data as it
would be returned in a call to /TIFFReadScanline/, or as it would be
expected in a call to /TIFFWriteScanline/.

/TIFFRasterScanlineSize/ returns the size in bytes of a complete decoded
and packed raster scanline. Note that this value may be different from
the value returned by /TIFFScanlineSize/ if data is stored as separate
planes.

* DIAGNOSTICS
None.

* SEE ALSO
*TIFFOpen*(3TIFF), *TIFFReadScanline*(3TIFF), *libtiff*(3TIFF)

Libtiff library home page: *http://www.simplesystems.org/libtiff/*
