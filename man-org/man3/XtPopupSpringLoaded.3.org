#+TITLE: Manpages - XtPopupSpringLoaded.3
#+DESCRIPTION: Linux manpage for XtPopupSpringLoaded.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtPopupSpringLoaded.3 is found in manpage for: [[../man3/XtPopup.3][man3/XtPopup.3]]