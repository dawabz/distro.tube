#+TITLE: Manpages - std_extreme_value_distribution.3
#+DESCRIPTION: Linux manpage for std_extreme_value_distribution.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::extreme_value_distribution< _RealType > - A
extreme_value_distribution random number distribution.

* SYNOPSIS
\\

=#include <random.h>=

** Classes
struct *param_type*\\

** Public Types
typedef _RealType *result_type*\\

** Public Member Functions
*extreme_value_distribution* (_RealType __a, _RealType
__b=_RealType(1))\\

*extreme_value_distribution* (const *param_type* &__p)\\

template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void *__generate* (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator &__urng)\\

template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void *__generate* (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator &__urng, const
*param_type* &__p)\\

template<typename _UniformRandomNumberGenerator > void *__generate*
(*result_type* *__f, *result_type* *__t, _UniformRandomNumberGenerator
&__urng, const *param_type* &__p)\\

_RealType *a* () const\\
Return the $a$ parameter of the distribution.

_RealType *b* () const\\
Return the $b$ parameter of the distribution.

*result_type* *max* () const\\
Returns the least upper bound value of the distribution.

*result_type* *min* () const\\
Returns the greatest lower bound value of the distribution.

template<typename _UniformRandomNumberGenerator > *result_type*
*operator()* (_UniformRandomNumberGenerator &__urng)\\
Generating functions.

template<typename _UniformRandomNumberGenerator > *result_type*
*operator()* (_UniformRandomNumberGenerator &__urng, const *param_type*
&__p)\\

*param_type* *param* () const\\
Returns the parameter set of the distribution.

void *param* (const *param_type* &__param)\\
Sets the parameter set of the distribution.

void *reset* ()\\
Resets the distribution state.

** Friends
bool *operator==* (const *extreme_value_distribution* &__d1, const
*extreme_value_distribution* &__d2)\\
Return true if two extreme value distributions have the same parameters.

* Detailed Description
** "template<typename _RealType = double>
\\
class std::extreme_value_distribution< _RealType >"A
extreme_value_distribution random number distribution.

The formula for the normal probability mass function is �

Definition at line *5072* of file *random.h*.

* Member Typedef Documentation
** template<typename _RealType = double> typedef _RealType
*std::extreme_value_distribution*< _RealType >::*result_type*
The type of the range of the distribution.

Definition at line *5079* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _RealType = double>
*std::extreme_value_distribution*< _RealType
>::*extreme_value_distribution* ()= [inline]=
Definition at line *5114* of file *random.h*.

** template<typename _RealType = double>
*std::extreme_value_distribution*< _RealType
>::*extreme_value_distribution* (_RealType __a, _RealType __b =
=_RealType(1)=)= [inline]=, = [explicit]=
Definition at line *5117* of file *random.h*.

** template<typename _RealType = double>
*std::extreme_value_distribution*< _RealType
>::*extreme_value_distribution* (const *param_type* & __p)= [inline]=,
= [explicit]=
Definition at line *5122* of file *random.h*.

* Member Function Documentation
** template<typename _RealType = double> template<typename
_ForwardIterator , typename _UniformRandomNumberGenerator > void
*std::extreme_value_distribution*< _RealType >::__generate
(_ForwardIterator __f, _ForwardIterator __t,
_UniformRandomNumberGenerator & __urng)= [inline]=
Definition at line *5192* of file *random.h*.

** template<typename _RealType = double> template<typename
_ForwardIterator , typename _UniformRandomNumberGenerator > void
*std::extreme_value_distribution*< _RealType >::__generate
(_ForwardIterator __f, _ForwardIterator __t,
_UniformRandomNumberGenerator & __urng, const *param_type* &
__p)= [inline]=
Definition at line *5199* of file *random.h*.

** template<typename _RealType = double> template<typename
_UniformRandomNumberGenerator > void *std::extreme_value_distribution*<
_RealType >::__generate (*result_type* * __f, *result_type* * __t,
_UniformRandomNumberGenerator & __urng, const *param_type* &
__p)= [inline]=
Definition at line *5206* of file *random.h*.

** template<typename _RealType = double> _RealType
*std::extreme_value_distribution*< _RealType >::a () const= [inline]=
Return the $a$ parameter of the distribution.

Definition at line *5137* of file *random.h*.

** template<typename _RealType = double> _RealType
*std::extreme_value_distribution*< _RealType >::b () const= [inline]=
Return the $b$ parameter of the distribution.

Definition at line *5144* of file *random.h*.

** template<typename _RealType = double> *result_type*
*std::extreme_value_distribution*< _RealType >::max () const= [inline]=
Returns the least upper bound value of the distribution.

Definition at line *5173* of file *random.h*.

References *std::numeric_limits< _Tp >::max()*.

** template<typename _RealType = double> *result_type*
*std::extreme_value_distribution*< _RealType >::min () const= [inline]=
Returns the greatest lower bound value of the distribution.

Definition at line *5166* of file *random.h*.

References *std::numeric_limits< _Tp >::lowest()*.

** template<typename _RealType = double> template<typename
_UniformRandomNumberGenerator > *result_type*
*std::extreme_value_distribution*< _RealType >::operator()
(_UniformRandomNumberGenerator & __urng)= [inline]=
Generating functions.

Definition at line *5181* of file *random.h*.

References *std::extreme_value_distribution< _RealType >::operator()()*.

Referenced by *std::extreme_value_distribution< _RealType
>::operator()()*.

** template<typename _RealType > template<typename
_UniformRandomNumberGenerator > *extreme_value_distribution*< _RealType
>::*result_type* *std::extreme_value_distribution*< _RealType
>::operator() (_UniformRandomNumberGenerator & __urng, const
*param_type* & __p)
Definition at line *2582* of file *bits/random.tcc*.

** template<typename _RealType = double> *param_type*
*std::extreme_value_distribution*< _RealType >::param ()
const= [inline]=
Returns the parameter set of the distribution.

Definition at line *5151* of file *random.h*.

Referenced by *std::operator>>()*.

** template<typename _RealType = double> void
*std::extreme_value_distribution*< _RealType >::param (const
*param_type* & __param)= [inline]=
Sets the parameter set of the distribution.

*Parameters*

#+begin_quote
  /__param/ The new parameter set of the distribution.
#+end_quote

Definition at line *5159* of file *random.h*.

** template<typename _RealType = double> void
*std::extreme_value_distribution*< _RealType >::reset ()= [inline]=
Resets the distribution state.

Definition at line *5130* of file *random.h*.

* Friends And Related Function Documentation
** template<typename _RealType = double> bool operator== (const
*extreme_value_distribution*< _RealType > & __d1, const
*extreme_value_distribution*< _RealType > & __d2)= [friend]=
Return true if two extreme value distributions have the same parameters.

Definition at line *5216* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
