#+TITLE: Manpages - wcslcpy.3bsd
#+DESCRIPTION: Linux manpage for wcslcpy.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

The functions implement string manipulation operations over wide
character strings. For a detailed description, refer to documents for
the respective single-byte counterpart, such as

and

which are BSD extensions.
