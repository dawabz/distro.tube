#+TITLE: Manpages - aa_splitcon.3
#+DESCRIPTION: Linux manpage for aa_splitcon.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
aa_splitcon - split the confinement context into a label and mode

* SYNOPSIS
*#include <sys/apparmor.h>*

*char *aa_splitcon(char *con, char **mode);*

Link with *-lapparmor* when compiling.

* DESCRIPTION
The *aa_splitcon()* function splits a confinement context into separate
label and mode strings. The =@con= string is modified so that the label
portion is NUL terminated. The enforcement mode is also NUL terminated
and the parenthesis surrounding the mode are removed. If =@mode= is
non-NULL, it will point to the first character in the enforcement mode
string on success.

The Linux kernel's /proc/<PID>/attr/current interface appends a trailing
newline character to AppArmor contexts that are read from that file. If
=@con= contains a single trailing newline character, it will be stripped
by *aa_splitcon()* prior to all other processing.

* RETURN VALUE
Returns a pointer to the first character in the label string. NULL is
returned on error.

* EXAMPLE
Context Label Mode ----------------------------- ------------------
------- unconfined unconfined NULL unconfined\n unconfined NULL
/bin/ping (enforce) /bin/ping enforce /bin/ping (enforce)\n /bin/ping
enforce /usr/sbin/rsyslogd (complain) /usr/sbin/rsyslogd complain

* BUGS
None known. If you find any, please report them at
<https://gitlab.com/apparmor/apparmor/-/issues>.

* SEE ALSO
*aa_getcon* (2) and <https://wiki.apparmor.net>.
