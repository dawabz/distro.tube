#+TITLE: Manpages - pcre_utf16_to_host_byte_order.3
#+DESCRIPTION: Linux manpage for pcre_utf16_to_host_byte_order.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE - Perl-compatible regular expressions

* SYNOPSIS
*#include <pcre.h>*

#+begin_example
  int pcre16_utf16_to_host_byte_order(PCRE_UCHAR16 *output,
   PCRE_SPTR16 input, int length, int *host_byte_order,
   int keep_boms);
#+end_example

* DESCRIPTION
This function, which exists only in the 16-bit library, converts a
UTF-16 string to the correct order for the current host, taking account
of any byte order marks (BOMs) within the string. Its arguments are:

/output/ pointer to output buffer, may be the same as /input/ /input/
pointer to input buffer /length/ number of 16-bit units in the input, or
negative for a zero-terminated string /host_byte_order/ a NULL value or
a non-zero value pointed to means start in host byte order /keep_boms/
if non-zero, BOMs are copied to the output string

The result of the function is the number of 16-bit units placed into the
output buffer, including the zero terminator if the string was
zero-terminated.

If /host_byte_order/ is not NULL, it is set to indicate the byte order
that is current at the end of the string.

There is a complete description of the PCRE native API in the *pcreapi*
page and a description of the POSIX API in the *pcreposix* page.
