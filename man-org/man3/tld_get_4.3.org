#+TITLE: Manpages - tld_get_4.3
#+DESCRIPTION: Linux manpage for tld_get_4.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tld_get_4 - API function

* SYNOPSIS
*#include <tld.h>*

*int tld_get_4(const uint32_t * */in/*, size_t */inlen/*, char **
*/out/*);*

* ARGUMENTS
- const uint32_t * in :: Array of unicode code points to process. Does
  not need to be zero terminated.

- size_t inlen :: Number of unicode code points.

- char ** out :: Zero terminated ascii result string pointer.

* DESCRIPTION
Isolate the top-level domain of /in/ and return it as an ASCII string in
/out/ .

Return value: Return *TLD_SUCCESS* on success, or the corresponding
*Tld_rc* error code otherwise.

* REPORTING BUGS
Report bugs to <help-libidn@gnu.org>.\\
General guidelines for reporting bugs: http://www.gnu.org/gethelp/\\
GNU Libidn home page: http://www.gnu.org/software/libidn/

* COPYRIGHT
Copyright © 2002-2021 Simon Josefsson.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libidn* is maintained as a Texinfo manual.
If the *info* and *libidn* programs are properly installed at your site,
the command

#+begin_quote
  *info libidn*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *http://www.gnu.org/software/libidn/manual/*
#+end_quote
