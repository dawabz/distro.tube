#+TITLE: Manpages - dispatch_object.3
#+DESCRIPTION: Linux manpage for dispatch_object.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Dispatch objects share functions for coordinating memory management,
suspension, cancellation and context pointers.

Objects returned by creation functions in the dispatch framework may be
uniformly retained and released with the functions

and

respectively.

The dispatch framework does not guarantee that any given client has the
last or only reference to a given object. Objects may be retained
internally by the system.

When building with an Objective-C or Objective-C++ compiler, dispatch
objects are declared as Objective-C types. This results in the following
differences compared to building as plain C/C++:

if Objective-C Automated Reference Counting is enabled, dispatch objects
are memory managed by the Objective-C runtime and explicit calls to the

and

functions will produce build errors.

when ARC is enabled, care needs to be taken with dispatch API returning
an interior pointer that is only valid as long as an associated object
has not been released. If that object is held in a variable with
automatic storage, it may need to be annotated with the

attribute, or stored in a

instance variable instead, to ensure that the object is not prematurely
released. The functions returning interior pointers are

and

the Blocks runtime automatically retains and releases dispatch objects
captured by blocks upon

and

e.g. as performed during asynchronous execution of a block via

retain cycles may be encountered if dispatch source objects are captured
by their handler blocks; these cycles can be broken by declaring the
captured object

or by calling

to cause its handler blocks to be released explicitly.

dispatch objects can be added directly to Cocoa collections, and their
lifetime is tracked by the Objective-C static analyzer.

Integration of dispatch objects with Objective-C requires targeting Mac
OS X 10.8 or later, and is disabled when building for the legacy
Objective-C runtime. It can also be disabled manually by using compiler
options to define the

preprocessor macro to

When building with a plain C/C++ compiler or when integration with
Objective-C is disabled, dispatch objects are

automatically retained and released when captured by a block. Therefore,
when a dispatch object is captured by a block that will be executed
asynchronously, the object must be manually retained and released:

dispatch_retain(object); dispatch_async(queue, ^{
do_something_with_object(object); dispatch_release(object); });

Dispatch objects such as queues and sources may be created in an
inactive state. Objects in this state must be activated before any
blocks associated with them will be invoked. Calling

on an active object has no effect.

Changing attributes such as the target queue or a source handler is no
longer permitted once the object has been activated (see

The invocation of blocks on dispatch queues or dispatch sources may be
suspended or resumed with the functions

and

respectively. Other dispatch objects do not support suspension.

The dispatch framework always checks the suspension status before
executing a block, but such changes never affect a block during
execution (non-preemptive). Therefore the suspension of an object is
asynchronous, unless it is performed from the context of the target
queue for the given object. The result of suspending or resuming an
object that is not a dispatch queue or a dispatch source is undefined.

suspension applies to all aspects of the dispatch object life cycle,
including the finalizer function and cancellation handler. Suspending an
object causes it to be retained and resuming an object causes it to be
released. Therefore it is important to balance calls to

and

such that the dispatch object is fully resumed when the last reference
is released. The result of releasing all references to a dispatch object
while in an inactive or suspended state is undefined.

Dispatch objects support supplemental context pointers. The value of the
context pointer may be retrieved and updated with

and

respectively. The

specifies an optional per-object finalizer function that is invoked
asynchronously if the context pointer is not NULL when the last
reference to the object is released. This gives the application an
opportunity to free the context data associated with the object. The
finalizer will be run on the object's target queue.
