#+TITLE: Manpages - xcb_xfixes_get_cursor_image_and_name_reply.3
#+DESCRIPTION: Linux manpage for xcb_xfixes_get_cursor_image_and_name_reply.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_xfixes_get_cursor_image_and_name_reply.3 is found in manpage for: [[../man3/xcb_xfixes_get_cursor_image_and_name.3][man3/xcb_xfixes_get_cursor_image_and_name.3]]