#+TITLE: Manpages - sd_notifyf.3
#+DESCRIPTION: Linux manpage for sd_notifyf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sd_notifyf.3 is found in manpage for: [[../sd_notify.3][sd_notify.3]]