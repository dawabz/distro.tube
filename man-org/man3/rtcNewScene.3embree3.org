#+TITLE: Manpages - rtcNewScene.3embree3
#+DESCRIPTION: Linux manpage for rtcNewScene.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcNewScene - creates a new scene
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  RTCScene rtcNewScene(RTCDevice device);
#+end_example

** DESCRIPTION
This function creates a new scene bound to the specified device
(=device= argument), and returns a handle to this scene. The scene
object is reference counted with an initial reference count of 1. The
scene handle can be released using the =rtcReleaseScene= API call.

** EXIT STATUS
On success a scene handle is returned. On failure =NULL= is returned and
an error code is set that can be queried using =rtcGetDeviceError=.

** SEE ALSO
[rtcRetainScene], [rtcReleaseScene]
