#+TITLE: Manpages - auparse_get_node.3
#+DESCRIPTION: Linux manpage for auparse_get_node.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_get_node - get the event's machine node name

* SYNOPSIS
*#include <auparse.h>*

const char *auparse_get_node(auparse_state_t *au);

* DESCRIPTION
auparse_get_node gets the machine's node name if it exists in the audit
event from the current event's timestamp data structure. Not all records
have node names since its an admin configurable option.

* RETURN VALUE
Returns a copy of the node name or NULL if it does not exist or there
was an error. The caller must free the string.

* SEE ALSO
*auparse_get_timestamp*(3), *auparse_get_time*(3),
*auparse_get_milli*(3). *auparse_get_serial*(3).

* AUTHOR
Steve Grubb
