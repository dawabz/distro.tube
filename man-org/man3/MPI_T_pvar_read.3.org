#+TITLE: Manpages - MPI_T_pvar_read.3
#+DESCRIPTION: Linux manpage for MPI_T_pvar_read.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_T_pvar_read* - Read the value of a performance variable

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_T_pvar_read(MPI_T_pvar_session session, MPI_T_pvar_handle handle, const void *buf)
#+end_example

* INPUT PARAMETERS
- session :: Performance experiment session.

- handle :: Performance variable handle.

- buf :: Initial address of storage location for variable value.

* DESCRIPTION
MPI_T_pvar_read queries the value of a performance variable identified
by the handle specified in /handle/ in the session specified in
/session/. The result is stored in the buffer pointed to by /buf/. The
caller must ensure that the buffer pointed to by /buf/ is large enough
to hold the entire value of the performance variable.

* ERRORS
MPI_T_pvar_read() will fail if:

- [MPI_T_ERR_NOT_INITIALIZED] :: The MPI Tools interface not initialized

- [MPI_T_ERR_INVALID_HANDLE] :: The handle is invalid or not associated
  with the session

- [MPI_T_ERR_INVALID_SESSION] :: Session argument is not a valid session

* SEE ALSO
#+begin_example
  MPI_T_pvar_handle_alloc
  MPI_T_pvar_get_info
  MPI_T_pvar_session_create
#+end_example
