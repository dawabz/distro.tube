#+TITLE: Manpages - pcre_jit_stack_alloc.3
#+DESCRIPTION: Linux manpage for pcre_jit_stack_alloc.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE - Perl-compatible regular expressions

* SYNOPSIS
*#include <pcre.h>*

#+begin_example
  pcre_jit_stack *pcre_jit_stack_alloc(int startsize,
   int maxsize);

  pcre16_jit_stack *pcre16_jit_stack_alloc(int startsize,
   int maxsize);

  pcre32_jit_stack *pcre32_jit_stack_alloc(int startsize,
   int maxsize);
#+end_example

* DESCRIPTION
This function is used to create a stack for use by the code compiled by
the JIT optimization of *pcre[16|32]_study()*. The arguments are a
starting size for the stack, and a maximum size to which it is allowed
to grow. The result can be passed to the JIT run-time code by
*pcre[16|32]_assign_jit_stack()*, or that function can set up a callback
for obtaining a stack. A maximum stack size of 512K to 1M should be more
than enough for any pattern. For more details, see the *pcrejit* page.

There is a complete description of the PCRE native API in the *pcreapi*
page and a description of the POSIX API in the *pcreposix* page.
