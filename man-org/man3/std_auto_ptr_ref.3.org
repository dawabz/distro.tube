#+TITLE: Manpages - std_auto_ptr_ref.3
#+DESCRIPTION: Linux manpage for std_auto_ptr_ref.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::auto_ptr_ref< _Tp1 >

* SYNOPSIS
\\

=#include <auto_ptr.h>=

** Public Member Functions
*auto_ptr_ref* (_Tp1 *__p)\\

** Public Attributes
_Tp1 * *_M_ptr*\\

* Detailed Description
** "template<typename _Tp1>
\\
struct std::auto_ptr_ref< _Tp1 >"A wrapper class to provide auto_ptr
with reference semantics. For example, an auto_ptr can be assigned (or
constructed from) the result of a function which returns an auto_ptr by
value.

All the auto_ptr_ref stuff should happen behind the scenes.

Definition at line *48* of file *auto_ptr.h*.

* Constructor & Destructor Documentation
** template<typename _Tp1 > *std::auto_ptr_ref*< _Tp1 >::*auto_ptr_ref*
(_Tp1 * __p)= [inline]=, = [explicit]=
Definition at line *53* of file *auto_ptr.h*.

* Member Data Documentation
** template<typename _Tp1 > _Tp1* *std::auto_ptr_ref*< _Tp1 >::_M_ptr
Definition at line *50* of file *auto_ptr.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
