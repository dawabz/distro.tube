#+TITLE: Manpages - SDL_CreateYUVOverlay.3
#+DESCRIPTION: Linux manpage for SDL_CreateYUVOverlay.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CreateYUVOverlay - Create a YUV video overlay

* SYNOPSIS
*#include "SDL.h"*

*SDL_Overlay *SDL_CreateYUVOverlay*(*int width, int height, Uint32
format, SDL_Surface *display*);

* DESCRIPTION
*SDL_CreateYUVOverlay* creates a YUV overlay of the specified *width*,
*height* and *format* (see *SDL_Overlay* for a list of available
formats), for the provided *display*. A *SDL_Overlay* structure is
returned.

The term 'overlay' is a misnomer since, unless the overlay is created in
hardware, the contents for the display surface underneath the area where
the overlay is shown will be overwritten when the overlay is displayed.

* SEE ALSO
*SDL_Overlay*, *SDL_DisplayYUVOverlay*, *SDL_FreeYUVOverlay*
