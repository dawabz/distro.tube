#+TITLE: Manpages - th_get_mtime.3
#+DESCRIPTION: Linux manpage for th_get_mtime.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about th_get_mtime.3 is found in manpage for: [[../man3/th_get_pathname.3][man3/th_get_pathname.3]]