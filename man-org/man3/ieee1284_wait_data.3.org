#+TITLE: Manpages - ieee1284_wait_data.3
#+DESCRIPTION: Linux manpage for ieee1284_wait_data.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ieee1284_wait_data.3 is found in manpage for: [[../ieee1284_read_data.3][ieee1284_read_data.3]]