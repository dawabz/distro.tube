#+TITLE: Manpages - curl_easy_option_by_name.3
#+DESCRIPTION: Linux manpage for curl_easy_option_by_name.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_easy_option_by_name - find an easy setopt option by name

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>

  const struct curl_easyoption *curl_easy_option_by_name(const char *name);
#+end_example

* DESCRIPTION
Given a *name*, this function returns a pointer to the curl_easyoption
struct, holding information about the /curl_easy_setopt(3)/ option using
that name. The name should be specified without the "CURLOPT_" prefix
and the name comparison is made case insensitive.

If libcurl has no option with the given name, this function returns
NULL.

* EXAMPLE
#+begin_example
  const struct curl_easyoption *opt = curl_easy_option_by_name("URL");
  if(opt) {
    printf("This option wants CURLoption %x\n", (int)opt->id);
  }
#+end_example

* AVAILABILITY
This function was added in libcurl 7.73.0

* RETURN VALUE
A pointer to the curl_easyoption struct for the option or NULL.

* SEE ALSO
*curl_easy_option_next*(3),*curl_easy_option_by_id*(3),
*curl_easy_setopt*(3),
