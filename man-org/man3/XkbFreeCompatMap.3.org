#+TITLE: Manpages - XkbFreeCompatMap.3
#+DESCRIPTION: Linux manpage for XkbFreeCompatMap.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbFreeCompatMap - Free an entire compatibility map or selected portions
of one

* SYNOPSIS
*void XkbFreeCompatMap* *( XkbDescPtr */xkb/* ,* *unsigned int
*/which/* ,* *Bool */free_map/* );*

* ARGUMENTS
- /- xkb/ :: Xkb description in which to free compatibility map

- /- which/ :: mask of compatibility map components to free

- /- free_map/ :: True => free XkbCompatMap structure itself

* DESCRIPTION
/which/ specifies the compatibility map components to be freed (see
XkbGetCompatMap). /which/ is an inclusive OR of the bits shown in
Table 1.

TABLE

/free_map/ indicates whether the XkbCompatMap structure itself should be
freed. If /free_map/ is True, /which/ is ignored, all non-NULL
compatibility map components are freed, and the /compat/ field in the
XkbDescRec referenced by /xkb/ is set to NULL.

* STRUCTURES
#+begin_example

      typedef struct _XkbCompatMapRec {
          XkbSymInterpretPtr    sym_interpret;            /* symbol based key semantics*/
          XkbModsRec            groups[XkbNumKbdGroups];  /* group => modifier map */
          unsigned short        num_si;                   /* # structures used in sym_interpret */
          unsigned short        size_si;                  /* # structures allocated in sym_interpret */
      } XkbCompatMapRec, *XkbCompatMapPtr;
      
#+end_example

The complete description of an Xkb keyboard is given by an XkbDescRec.
The component structures in the XkbDescRec represent the major Xkb
components outlined in Figure 1.1.

#+begin_example
  typedef struct {
     struct _XDisplay * display;      /* connection to X server */
     unsigned short     flags;        /* private to Xkb, do not modify */
     unsigned short     device_spec;  /* device of interest */
     KeyCode            min_key_code; /* minimum keycode for device */
     KeyCode            max_key_code; /* maximum keycode for device */
     XkbControlsPtr     ctrls;        /* controls */
     XkbServerMapPtr    server;       /* server keymap */
     XkbClientMapPtr    map;          /* client keymap */
     XkbIndicatorPtr    indicators;   /* indicator map */
     XkbNamesPtr        names;        /* names for all components */
     XkbCompatMapPtr    compat;       /* compatibility map */
     XkbGeometryPtr     geom;         /* physical geometry of keyboard */
  } XkbDescRec, *XkbDescPtr;
#+end_example

The /display/ field points to an X display structure. The /flags field
is private to the library: modifying/ /flags/ may yield unpredictable
results. The /device_spec/ field specifies the device identifier of the
keyboard input device, or XkbUseCoreKeyboard, which specifies the core
keyboard device. The /min_key_code/ and /max_key_code/ fields specify
the least and greatest keycode that can be returned by the keyboard. The
other fields specify structure components of the keyboard description
and are described in detail in other sections of this document. Table 2
identifies the subsequent sections of this document that discuss the
individual components of the XkbDescRec.

TABLE

Each structure component has a corresponding mask bit that is used in
function calls to indicate that the structure should be manipulated in
some manner, such as allocating it or freeing it. These masks and their
relationships to the fields in the XkbDescRec are shown in Table 3.

TABLE

* SEE ALSO
*XkbGetCompatMap*(3)
