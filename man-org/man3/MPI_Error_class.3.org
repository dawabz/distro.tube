#+TITLE: Manpages - MPI_Error_class.3
#+DESCRIPTION: Linux manpage for MPI_Error_class.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Error_class * - Converts an error code into an error class.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Error_class(int errorcode, int *errorclass)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_ERROR_CLASS(ERRORCODE, ERRORCLASS, IERROR)
  	INTEGER	ERRORCODE, ERRORCLASS, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Error_class(errorcode, errorclass, ierror)
  	INTEGER, INTENT(IN) :: errorcode
  	INTEGER, INTENT(OUT) :: errorclass
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  int Get_error_class(int errorcode)
#+end_example

* INPUT PARAMETER
- errorcode :: Error code returned by an MPI routine.

* OUTPUT PARAMETERS
- errorclass :: Error class associated with errorcode.

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
The function MPI_Error_class maps each standard error code (error class)
onto itself.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.

* SEE ALSO
MPI_Error_string
