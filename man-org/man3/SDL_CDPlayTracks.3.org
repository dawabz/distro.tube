#+TITLE: Manpages - SDL_CDPlayTracks.3
#+DESCRIPTION: Linux manpage for SDL_CDPlayTracks.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CDPlayTracks - Play the given CD track(s)

* SYNOPSIS
*#include "SDL.h"*

*int SDL_CDPlayTracks*(*SDL_CD *cdrom, int start_track, int start_frame,
int ntracks, int nframes)*);

* DESCRIPTION
*SDL_CDPlayTracks* plays the given CD starting at track *start_track*,
for *ntracks* tracks.

*start_frame* is the frame offset, from the beginning of the
*start_track*, at which to start. *nframes* is the frame offset, from
the beginning of the last track (*start_track*+*ntracks*), at which to
end playing.

*SDL_CDPlayTracks* should only be called after calling *SDL_CDStatus* to
get track information about the CD.

#+begin_quote
  *Note: *

  Data tracks are ignored.
#+end_quote

* RETURN VALUE
Returns *0*, or *-1* if there was an error.

* EXAMPLES
#+begin_example
  /* assuming cdrom is a previously opened device */
  /* Play the entire CD */
  if(CD_INDRIVE(SDL_CDStatus(cdrom)))
    SDL_CDPlayTracks(cdrom, 0, 0, 0, 0);

  /* Play the first track */
  if(CD_INDRIVE(SDL_CDStatus(cdrom)))
    SDL_CDPlayTracks(cdrom, 0, 0, 1, 0);

  /* Play first 15 seconds of the 2nd track */
  if(CD_INDRIVE(SDL_CDStatus(cdrom)))
    SDL_CDPlayTracks(cdrom, 1, 0, 0, CD_FPS*15);
#+end_example

* SEE ALSO
*SDL_CDPlay*, *SDL_CDStatus*, *SDL_CD*
