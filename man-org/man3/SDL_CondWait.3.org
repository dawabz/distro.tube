#+TITLE: Manpages - SDL_CondWait.3
#+DESCRIPTION: Linux manpage for SDL_CondWait.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CondWait - Wait on a condition variable

* SYNOPSIS
*#include "SDL.h"* #include "SDL_thread.h"

*int SDL_CondWait*(*SDL_cond *cond, SDL_mutex *mut*);

* DESCRIPTION
Wait on the condition variable *cond* and unlock the provided mutex. The
mutex must the locked before entering this function. Returns *0* when it
is signalled, or *-1* on an error.

* SEE ALSO
*SDL_CondWaitTimeout*, *SDL_CondSignal*, *SDL_mutexP*
