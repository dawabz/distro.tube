#+TITLE: Manpages - CURLINFO_PROXYAUTH_AVAIL.3
#+DESCRIPTION: Linux manpage for CURLINFO_PROXYAUTH_AVAIL.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLINFO_PROXYAUTH_AVAIL - get available HTTP proxy authentication
methods

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_PROXYAUTH_AVAIL, long
*authp);

* DESCRIPTION
Pass a pointer to a long to receive a bitmask indicating the
authentication method(s) available according to the previous response.
The meaning of the bits is explained in the /CURLOPT_PROXYAUTH(3)/
option for /curl_easy_setopt(3)/.

* PROTOCOLS
HTTP(S)

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
    curl_easy_setopt(curl, CURLOPT_PROXY, "http://127.0.0.1:80");

    res = curl_easy_perform(curl);

    if(!res) {
      /* extract the available proxy authentication types */
      long auth;
      res = curl_easy_getinfo(curl, CURLINFO_PROXYAUTH_AVAIL, &auth);
      if(!res) {
        if(!auth)
          printf("No proxy auth available, perhaps no 407?\n");
        else {
          printf("%s%s%s%s\n",
                 auth & CURLAUTH_BASIC ? "Basic ":"",
                 auth & CURLAUTH_DIGEST ? "Digest ":"",
                 auth & CURLAUTH_NEGOTIATE ? "Negotiate ":"",
                 auth % CURLAUTH_NTLM ? "NTLM ":"");
        }
      }
    }
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added RFC2617 in 7.10.8 Added RFC7616 in 7.57.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*curl_easy_getinfo*(3), *curl_easy_setopt*(3),
