#+TITLE: Manpages - MPI_Group_rank.3
#+DESCRIPTION: Linux manpage for MPI_Group_rank.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Group_rank* - Returns the rank of the calling process in the given
group.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Group_rank(MPI_Group group, int *rank)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_GROUP_RANK(GROUP, RANK, IERROR)
  	INTEGER	GROUP, RANK, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Group_rank(group, rank, ierror)
  	TYPE(MPI_Group), INTENT(IN) :: group
  	INTEGER, INTENT(OUT) :: rank
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  int Group::Get_rank() const
#+end_example

* INPUT PARAMETERS
- group :: Group (handle).

* OUTPUT PARAMETERS
- rank :: Rank of the calling process in group, or MPI_UNDEFINED if the
  process is not a member (integer).

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
MPI_Group_rank returns as the output parameter /rank/ the rank of the
calling process in group. If the process is not a member of group then
MPI_UNDEFINED is returned.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.
