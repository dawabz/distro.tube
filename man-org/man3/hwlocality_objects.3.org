#+TITLE: Manpages - hwlocality_objects.3
#+DESCRIPTION: Linux manpage for hwlocality_objects.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwlocality_objects - Object Structure and Attributes

* SYNOPSIS
\\

** Data Structures
struct *hwloc_obj*\\

union *hwloc_obj_attr_u*\\

struct *hwloc_info_s*\\

** Typedefs
typedef struct *hwloc_obj* * *hwloc_obj_t*\\

* Detailed Description
* Typedef Documentation
** typedef struct *hwloc_obj** *hwloc_obj_t*
Convenience typedef; a pointer to a struct *hwloc_obj*.

* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
