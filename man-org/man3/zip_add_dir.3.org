#+TITLE: Manpages - zip_add_dir.3
#+DESCRIPTION: Linux manpage for zip_add_dir.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The function

is the obsolete version of

It is the same as calling

with an empty flags argument.

was added in libzip 0.8. In libzip 0.10 the return type was changed from

to

It was deprecated in libzip 0.11, use

instead.

and
