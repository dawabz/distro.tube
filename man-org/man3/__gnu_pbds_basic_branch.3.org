#+TITLE: Manpages - __gnu_pbds_basic_branch.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_basic_branch.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::basic_branch< Key, Mapped, Tag, Node_Update, Policy_Tl,
_Alloc >

* SYNOPSIS
\\

=#include <assoc_container.hpp>=

Inherits detail::container_base_dispatch::type.

** Public Types
typedef Node_Update *node_update*\\

** Protected Member Functions
*basic_branch* (const *basic_branch* &other)\\

template<typename T0 > *basic_branch* (T0 t0)\\

template<typename T0 , typename T1 > *basic_branch* (T0 t0, T1 t1)\\

template<typename T0 , typename T1 , typename T2 > *basic_branch* (T0
t0, T1 t1, T2 t2)\\

template<typename T0 , typename T1 , typename T2 , typename T3 >
*basic_branch* (T0 t0, T1 t1, T2 t2, T3 t3)\\

template<typename T0 , typename T1 , typename T2 , typename T3 ,
typename T4 > *basic_branch* (T0 t0, T1 t1, T2 t2, T3 t3, T4 t4)\\

template<typename T0 , typename T1 , typename T2 , typename T3 ,
typename T4 , typename T5 > *basic_branch* (T0 t0, T1 t1, T2 t2, T3 t3,
T4 t4, T5 t5)\\

template<typename T0 , typename T1 , typename T2 , typename T3 ,
typename T4 , typename T5 , typename T6 > *basic_branch* (T0 t0, T1 t1,
T2 t2, T3 t3, T4 t4, T5 t5, T6 t6)\\

* Detailed Description
** "template<typename Key, typename Mapped, typename Tag, typename
Node_Update, typename Policy_Tl, typename _Alloc>
\\
class __gnu_pbds::basic_branch< Key, Mapped, Tag, Node_Update,
Policy_Tl, _Alloc >"A branched, tree-like (tree, trie) container
abstraction.

*Template Parameters*

#+begin_quote
  /Key/ Key type.\\
  /Mapped/ Map type.\\
  /Tag/ Instantiating data structure type, see container_tag.\\
  /Node_Update/ Updates nodes, restores invariants.\\
  /Policy_TL/ Policy typelist.\\
  /_Alloc/ Allocator type.
#+end_quote

Base is dispatched at compile time via Tag, from the following choices:
tree_tag, trie_tag, and their descendants.

Base choices are: detail::ov_tree_map, detail::rb_tree_map,
detail::splay_tree_map, and detail::pat_trie_map.

Definition at line *555* of file *assoc_container.hpp*.

* Member Typedef Documentation
** template<typename Key , typename Mapped , typename Tag , typename
Node_Update , typename Policy_Tl , typename _Alloc > typedef Node_Update
*__gnu_pbds::basic_branch*< Key, Mapped, Tag, Node_Update, Policy_Tl,
_Alloc >::node_update
Definition at line *561* of file *assoc_container.hpp*.

* Constructor & Destructor Documentation
** template<typename Key , typename Mapped , typename Tag , typename
Node_Update , typename Policy_Tl , typename _Alloc > virtual
*__gnu_pbds::basic_branch*< Key, Mapped, Tag, Node_Update, Policy_Tl,
_Alloc >::~*basic_branch* ()= [inline]=, = [virtual]=
Definition at line *564* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Tag , typename
Node_Update , typename Policy_Tl , typename _Alloc >
*__gnu_pbds::basic_branch*< Key, Mapped, Tag, Node_Update, Policy_Tl,
_Alloc >::*basic_branch* ()= [inline]=, = [protected]=
Definition at line *567* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Tag , typename
Node_Update , typename Policy_Tl , typename _Alloc >
*__gnu_pbds::basic_branch*< Key, Mapped, Tag, Node_Update, Policy_Tl,
_Alloc >::*basic_branch* (const *basic_branch*< Key, Mapped, Tag,
Node_Update, Policy_Tl, _Alloc > & other)= [inline]=, = [protected]=
Definition at line *569* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Tag , typename
Node_Update , typename Policy_Tl , typename _Alloc > template<typename
T0 > *__gnu_pbds::basic_branch*< Key, Mapped, Tag, Node_Update,
Policy_Tl, _Alloc >::*basic_branch* (T0 t0)= [inline]=, = [protected]=
Definition at line *573* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Tag , typename
Node_Update , typename Policy_Tl , typename _Alloc > template<typename
T0 , typename T1 > *__gnu_pbds::basic_branch*< Key, Mapped, Tag,
Node_Update, Policy_Tl, _Alloc >::*basic_branch* (T0 t0, T1
t1)= [inline]=, = [protected]=
Definition at line *576* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Tag , typename
Node_Update , typename Policy_Tl , typename _Alloc > template<typename
T0 , typename T1 , typename T2 > *__gnu_pbds::basic_branch*< Key,
Mapped, Tag, Node_Update, Policy_Tl, _Alloc >::*basic_branch* (T0 t0, T1
t1, T2 t2)= [inline]=, = [protected]=
Definition at line *579* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Tag , typename
Node_Update , typename Policy_Tl , typename _Alloc > template<typename
T0 , typename T1 , typename T2 , typename T3 >
*__gnu_pbds::basic_branch*< Key, Mapped, Tag, Node_Update, Policy_Tl,
_Alloc >::*basic_branch* (T0 t0, T1 t1, T2 t2, T3 t3)= [inline]=,
= [protected]=
Definition at line *582* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Tag , typename
Node_Update , typename Policy_Tl , typename _Alloc > template<typename
T0 , typename T1 , typename T2 , typename T3 , typename T4 >
*__gnu_pbds::basic_branch*< Key, Mapped, Tag, Node_Update, Policy_Tl,
_Alloc >::*basic_branch* (T0 t0, T1 t1, T2 t2, T3 t3, T4 t4)= [inline]=,
= [protected]=
Definition at line *586* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Tag , typename
Node_Update , typename Policy_Tl , typename _Alloc > template<typename
T0 , typename T1 , typename T2 , typename T3 , typename T4 , typename T5
> *__gnu_pbds::basic_branch*< Key, Mapped, Tag, Node_Update, Policy_Tl,
_Alloc >::*basic_branch* (T0 t0, T1 t1, T2 t2, T3 t3, T4 t4, T5
t5)= [inline]=, = [protected]=
Definition at line *591* of file *assoc_container.hpp*.

** template<typename Key , typename Mapped , typename Tag , typename
Node_Update , typename Policy_Tl , typename _Alloc > template<typename
T0 , typename T1 , typename T2 , typename T3 , typename T4 , typename T5
, typename T6 > *__gnu_pbds::basic_branch*< Key, Mapped, Tag,
Node_Update, Policy_Tl, _Alloc >::*basic_branch* (T0 t0, T1 t1, T2 t2,
T3 t3, T4 t4, T5 t5, T6 t6)= [inline]=, = [protected]=
Definition at line *596* of file *assoc_container.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
