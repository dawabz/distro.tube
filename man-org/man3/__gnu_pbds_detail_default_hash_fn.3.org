#+TITLE: Manpages - __gnu_pbds_detail_default_hash_fn.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_detail_default_hash_fn.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::detail::default_hash_fn< Key > - Primary template,
default_hash_fn.

* SYNOPSIS
\\

=#include <standard_policies.hpp>=

** Public Types
typedef std::tr1::hash< Key > *type*\\
Dispatched type.

* Detailed Description
** "template<typename Key>
\\
struct __gnu_pbds::detail::default_hash_fn< Key >"Primary template,
default_hash_fn.

Definition at line *59* of file *standard_policies.hpp*.

* Member Typedef Documentation
** template<typename Key > typedef std::tr1::hash<Key>
*__gnu_pbds::detail::default_hash_fn*< Key >::*type*
Dispatched type.

Definition at line *62* of file *standard_policies.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
