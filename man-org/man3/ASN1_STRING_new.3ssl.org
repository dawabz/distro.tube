#+TITLE: Manpages - ASN1_STRING_new.3ssl
#+DESCRIPTION: Linux manpage for ASN1_STRING_new.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ASN1_STRING_new, ASN1_STRING_type_new, ASN1_STRING_free - ASN1_STRING
allocation functions

* SYNOPSIS
#include <openssl/asn1.h> ASN1_STRING * ASN1_STRING_new(void);
ASN1_STRING * ASN1_STRING_type_new(int type); void
ASN1_STRING_free(ASN1_STRING *a);

* DESCRIPTION
*ASN1_STRING_new()* returns an allocated *ASN1_STRING* structure. Its
type is undefined.

*ASN1_STRING_type_new()* returns an allocated *ASN1_STRING* structure of
type *type*.

*ASN1_STRING_free()* frees up *a*. If *a* is NULL nothing is done.

* NOTES
Other string types call the *ASN1_STRING* functions. For example
*ASN1_OCTET_STRING_new()* calls ASN1_STRING_type(V_ASN1_OCTET_STRING).

* RETURN VALUES
*ASN1_STRING_new()* and *ASN1_STRING_type_new()* return a valid
ASN1_STRING structure or *NULL* if an error occurred.

*ASN1_STRING_free()* does not return a value.

* SEE ALSO
*ERR_get_error* (3)

* COPYRIGHT
Copyright 2002-2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
