#+TITLE: Manpages - XtPeekEvent.3
#+DESCRIPTION: Linux manpage for XtPeekEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtPeekEvent.3 is found in manpage for: [[../man3/XtNextEvent.3][man3/XtNextEvent.3]]