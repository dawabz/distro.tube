#+TITLE: Manpages - XmbufChangeBufferAttributes.3
#+DESCRIPTION: Linux manpage for XmbufChangeBufferAttributes.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XmbufChangeBufferAttributes.3 is found in manpage for: [[../man3/Xmbuf.3][man3/Xmbuf.3]]