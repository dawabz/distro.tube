#+TITLE: Manpages - tslib_version.3
#+DESCRIPTION: Linux manpage for tslib_version.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tslib_version - simply return tslib's version string

* SYNOPSIS
#+begin_example
  #include <tslib.h>

  char *tslib_version();
#+end_example

* DESCRIPTION
*tslib_version*() This function returns a static string holding tslib's
version information.

* SEE ALSO
*ts_libversion*(3), *ts.conf*(5)
