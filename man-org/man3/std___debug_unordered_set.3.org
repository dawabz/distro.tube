#+TITLE: Manpages - std___debug_unordered_set.3
#+DESCRIPTION: Linux manpage for std___debug_unordered_set.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__debug::unordered_set< _Value, _Hash, _Pred, _Alloc > - Class
std::unordered_set with safety/checking/debug instrumentation.

* SYNOPSIS
\\

Inherits *__gnu_debug::_Safe_container< unordered_set< _Value,
std::hash< _Value >, std::equal_to< _Value >, std::allocator< _Value >
>, std::allocator< _Value >, __gnu_debug::_Safe_unordered_container >*,
and unordered_set< _Value, std::hash< _Value >, std::equal_to< _Value >,
std::allocator< _Value > >.

** Public Types
typedef _Base::allocator_type *allocator_type*\\

typedef *__gnu_debug::_Safe_iterator*< _Base_const_iterator,
*unordered_set* > *const_iterator*\\

typedef *__gnu_debug::_Safe_local_iterator*< _Base_const_local_iterator,
*unordered_set* > *const_local_iterator*\\

typedef _Base::hasher *hasher*\\

using *insert_return_type* = *_Node_insert_return*< *iterator*,
node_type >\\

typedef *__gnu_debug::_Safe_iterator*< _Base_iterator, *unordered_set* >
*iterator*\\

typedef _Base::key_equal *key_equal*\\

typedef _Base::key_type *key_type*\\

typedef *__gnu_debug::_Safe_local_iterator*< _Base_local_iterator,
*unordered_set* > *local_iterator*\\

using *node_type* = typename _Base::node_type\\

typedef _Base::size_type *size_type*\\

typedef _Base::value_type *value_type*\\

** Public Member Functions
*unordered_set* (_Base_ref __x)\\

template<typename _InputIterator > *unordered_set* (_InputIterator
__first, _InputIterator __last, size_type __n, const allocator_type
&__a)\\

template<typename _InputIterator > *unordered_set* (_InputIterator
__first, _InputIterator __last, size_type __n, const hasher &__hf, const
allocator_type &__a)\\

template<typename _InputIterator > *unordered_set* (_InputIterator
__first, _InputIterator __last, size_type __n=0, const hasher
&__hf=hasher(), const key_equal &__eql=key_equal(), const allocator_type
&__a=allocator_type())\\

*unordered_set* (const allocator_type &__a)\\

*unordered_set* (const *unordered_set* &)=default\\

*unordered_set* (const *unordered_set* &__uset, const allocator_type
&__a)\\

*unordered_set* (*initializer_list*< value_type > __l, size_type __n,
const allocator_type &__a)\\

*unordered_set* (*initializer_list*< value_type > __l, size_type __n,
const hasher &__hf, const allocator_type &__a)\\

*unordered_set* (*initializer_list*< value_type > __l, size_type __n=0,
const hasher &__hf=hasher(), const key_equal &__eql=key_equal(), const
allocator_type &__a=allocator_type())\\

*unordered_set* (size_type __n, const allocator_type &__a)\\

*unordered_set* (size_type __n, const hasher &__hf, const allocator_type
&__a)\\

*unordered_set* (size_type __n, const hasher &__hf=hasher(), const
key_equal &__eql=key_equal(), const allocator_type
&__a=allocator_type())\\

*unordered_set* (*unordered_set* &&)=default\\

*unordered_set* (*unordered_set* &&__uset, const allocator_type &__a)
noexcept(noexcept(*_Base*(*std::move*(__uset._M_base()), __a)))\\

const *_Base* & *_M_base* () const noexcept\\

*_Base* & *_M_base* () noexcept\\

void *_M_swap* (_Safe_container &__x) noexcept\\

*const_iterator* *begin* () const noexcept\\

*iterator* *begin* () noexcept\\

*local_iterator* *begin* (size_type __b)\\

*const_local_iterator* *begin* (size_type __b) const\\

size_type *bucket_size* (size_type __b) const\\

*const_iterator* *cbegin* () const noexcept\\

*const_local_iterator* *cbegin* (size_type __b) const\\

*const_iterator* *cend* () const noexcept\\

*const_local_iterator* *cend* (size_type __b) const\\

void *clear* () noexcept\\

template<typename... _Args> *std::pair*< *iterator*, bool > *emplace*
(_Args &&... __args)\\

template<typename... _Args> *iterator* *emplace_hint* (*const_iterator*
__hint, _Args &&... __args)\\

*const_iterator* *end* () const noexcept\\

*iterator* *end* () noexcept\\

*local_iterator* *end* (size_type __b)\\

*const_local_iterator* *end* (size_type __b) const\\

*std::pair*< *iterator*, *iterator* > *equal_range* (const key_type
&__key)\\

*std::pair*< *const_iterator*, *const_iterator* > *equal_range* (const
key_type &__key) const\\

size_type *erase* (const key_type &__key)\\

*iterator* *erase* (*const_iterator* __first, *const_iterator* __last)\\

*iterator* *erase* (*const_iterator* __it)\\

*iterator* *erase* (*iterator* __it)\\

node_type *extract* (const key_type &__key)\\

node_type *extract* (*const_iterator* __position)\\

*iterator* *find* (const key_type &__key)\\

*const_iterator* *find* (const key_type &__key) const\\

template<typename _InputIterator > void *insert* (_InputIterator
__first, _InputIterator __last)\\

*std::pair*< *iterator*, bool > *insert* (const value_type &__obj)\\

*iterator* *insert* (*const_iterator* __hint, const value_type &__obj)\\

*iterator* *insert* (*const_iterator* __hint, node_type &&__nh)\\

*iterator* *insert* (*const_iterator* __hint, value_type &&__obj)\\

*insert_return_type* *insert* (node_type &&__nh)\\

void *insert* (*std::initializer_list*< value_type > __l)\\

*std::pair*< *iterator*, bool > *insert* (value_type &&__obj)\\

float *max_load_factor* () const noexcept\\

void *max_load_factor* (float __f)\\

*unordered_set* & *operator=* (const *unordered_set* &)=default\\

*unordered_set* & *operator=* (*initializer_list*< value_type > __l)\\

*unordered_set* & *operator=* (*unordered_set* &&)=default\\

void *swap* (*unordered_set* &__x) noexcept(noexcept(declval< *_Base* &
>().swap(__x)))\\

** Public Attributes
_Safe_iterator_base * *_M_const_iterators*\\
The list of constant iterators that reference this container.

_Safe_iterator_base * *_M_const_local_iterators*\\
The list of constant local iterators that reference this container.

_Safe_iterator_base * *_M_iterators*\\
The list of mutable iterators that reference this container.

_Safe_iterator_base * *_M_local_iterators*\\
The list of mutable local iterators that reference this container.

unsigned int *_M_version*\\
The container version number. This number may never be 0.

** Protected Member Functions
void *_M_detach_all* ()\\

void *_M_detach_singular* ()\\

__gnu_cxx::__mutex & *_M_get_mutex* () throw ()\\

void *_M_invalidate_all* ()\\

void *_M_invalidate_all* () const\\

template<typename _Predicate > void *_M_invalidate_if* (_Predicate
__pred)\\

template<typename _Predicate > void *_M_invalidate_local_if* (_Predicate
__pred)\\

void *_M_invalidate_locals* ()\\

void *_M_revalidate_singular* ()\\

_Safe_container & *_M_safe* () noexcept\\

void *_M_swap* (_Safe_sequence_base &__x) noexcept\\

void *_M_swap* (_Safe_unordered_container_base &__x) noexcept\\

** Friends
template<typename _ItT , typename _SeqT , typename _CatT > class
*::__gnu_debug::_Safe_iterator*\\

template<typename _ItT , typename _SeqT > class
*::__gnu_debug::_Safe_local_iterator*\\

* Detailed Description
** "template<typename _Value, typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>>
\\
class std::__debug::unordered_set< _Value, _Hash, _Pred, _Alloc >"Class
std::unordered_set with safety/checking/debug instrumentation.

Definition at line *60* of file *debug/unordered_set*.

* Member Typedef Documentation
** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef _Base::allocator_type
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc
>::allocator_type
Definition at line *93* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef *__gnu_debug::_Safe_iterator*<
_Base_const_iterator, *unordered_set*> *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::*const_iterator*
Definition at line *101* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef *__gnu_debug::_Safe_local_iterator*<
_Base_const_local_iterator, *unordered_set*>
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc
>::*const_local_iterator*
Definition at line *105* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef _Base::hasher
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc >::hasher
Definition at line *91* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> using *std::__debug::unordered_set*< _Value,
_Hash, _Pred, _Alloc >::*insert_return_type* =
*_Node_insert_return*<*iterator*, node_type>
Definition at line *393* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef *__gnu_debug::_Safe_iterator*<
_Base_iterator, *unordered_set*> *std::__debug::unordered_set*< _Value,
_Hash, _Pred, _Alloc >::*iterator*
Definition at line *99* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef _Base::key_equal
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc >::key_equal
Definition at line *92* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef _Base::key_type
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc >::key_type
Definition at line *95* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef *__gnu_debug::_Safe_local_iterator*<
_Base_local_iterator, *unordered_set*> *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::*local_iterator*
Definition at line *103* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> using *std::__debug::unordered_set*< _Value,
_Hash, _Pred, _Alloc >::node_type = typename _Base::node_type
Definition at line *392* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef _Base::size_type
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc >::size_type
Definition at line *90* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef _Base::value_type
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc
>::value_type
Definition at line *96* of file *debug/unordered_set*.

* Constructor & Destructor Documentation
** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_set*< _Value, _Hash,
_Pred, _Alloc >::*unordered_set* (size_type __n, const hasher & __hf =
=hasher()=, const key_equal & __eql = =key_equal()=, const
allocator_type & __a = =allocator_type()=)= [inline]=, = [explicit]=
Definition at line *110* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> template<typename _InputIterator >
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc
>::*unordered_set* (_InputIterator __first, _InputIterator __last,
size_type __n = =0=, const hasher & __hf = =hasher()=, const key_equal &
__eql = =key_equal()=, const allocator_type & __a =
=allocator_type()=)= [inline]=
Definition at line *117* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_set*< _Value, _Hash,
_Pred, _Alloc >::*unordered_set* (_Base_ref __x)= [inline]=
Definition at line *129* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_set*< _Value, _Hash,
_Pred, _Alloc >::*unordered_set* (const allocator_type &
__a)= [inline]=, = [explicit]=
Definition at line *135* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_set*< _Value, _Hash,
_Pred, _Alloc >::*unordered_set* (const *unordered_set*< _Value, _Hash,
_Pred, _Alloc > & __uset, const allocator_type & __a)= [inline]=
Definition at line *138* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_set*< _Value, _Hash,
_Pred, _Alloc >::*unordered_set* (*unordered_set*< _Value, _Hash, _Pred,
_Alloc > && __uset, const allocator_type & __a)= [inline]=,
= [noexcept]=
Definition at line *142* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_set*< _Value, _Hash,
_Pred, _Alloc >::*unordered_set* (*initializer_list*< value_type > __l,
size_type __n = =0=, const hasher & __hf = =hasher()=, const key_equal &
__eql = =key_equal()=, const allocator_type & __a =
=allocator_type()=)= [inline]=
Definition at line *148* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_set*< _Value, _Hash,
_Pred, _Alloc >::*unordered_set* (size_type __n, const allocator_type &
__a)= [inline]=
Definition at line *155* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_set*< _Value, _Hash,
_Pred, _Alloc >::*unordered_set* (size_type __n, const hasher & __hf,
const allocator_type & __a)= [inline]=
Definition at line *159* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> template<typename _InputIterator >
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc
>::*unordered_set* (_InputIterator __first, _InputIterator __last,
size_type __n, const allocator_type & __a)= [inline]=
Definition at line *165* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> template<typename _InputIterator >
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc
>::*unordered_set* (_InputIterator __first, _InputIterator __last,
size_type __n, const hasher & __hf, const allocator_type &
__a)= [inline]=
Definition at line *172* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_set*< _Value, _Hash,
_Pred, _Alloc >::*unordered_set* (*initializer_list*< value_type > __l,
size_type __n, const allocator_type & __a)= [inline]=
Definition at line *178* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_set*< _Value, _Hash,
_Pred, _Alloc >::*unordered_set* (*initializer_list*< value_type > __l,
size_type __n, const hasher & __hf, const allocator_type &
__a)= [inline]=
Definition at line *184* of file *debug/unordered_set*.

* Member Function Documentation
** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> const *_Base* & *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::_M_base () const= [inline]=,
= [noexcept]=
Definition at line *543* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *_Base* & *std::__debug::unordered_set*< _Value,
_Hash, _Pred, _Alloc >::_M_base ()= [inline]=, = [noexcept]=
Definition at line *540* of file *debug/unordered_set*.

** void __gnu_debug::_Safe_unordered_container_base::_M_detach_all
()= [protected]=, = [inherited]=
Detach all iterators, leaving them singular.

** void __gnu_debug::_Safe_sequence_base::_M_detach_singular
()= [protected]=, = [inherited]=
Detach all singular iterators.

*Postcondition*

#+begin_quote
  for all iterators i attached to this sequence, i->_M_version ==
  _M_version.
#+end_quote

** __gnu_cxx::__mutex & __gnu_debug::_Safe_sequence_base::_M_get_mutex
()= [protected]=, = [inherited]=
For use in _Safe_sequence.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** template<typename _Container > void
*__gnu_debug::_Safe_unordered_container*< _Container
>::_M_invalidate_all ()= [inline]=, = [protected]=, = [inherited]=
Definition at line *76* of file *safe_unordered_container.h*.

** void __gnu_debug::_Safe_sequence_base::_M_invalidate_all ()
const= [inline]=, = [protected]=, = [inherited]=
Invalidates all iterators.

Definition at line *256* of file *safe_base.h*.

References *__gnu_debug::_Safe_sequence_base::_M_version*.

** template<typename _Container > template<typename _Predicate > void
*__gnu_debug::_Safe_unordered_container*< _Container >::_M_invalidate_if
(_Predicate __pred)= [protected]=, = [inherited]=
Invalidates all iterators =x= that reference this container, are not
singular, and for which =__pred(x)= returns =true=. =__pred= will be
invoked with the normal iterators nested in the safe ones.

Definition at line *37* of file *safe_unordered_container.tcc*.

** template<typename _Container > template<typename _Predicate > void
*__gnu_debug::_Safe_unordered_container*< _Container
>::_M_invalidate_local_if (_Predicate __pred)= [protected]=,
= [inherited]=
Invalidates all local iterators =x= that reference this container, are
not singular, and for which =__pred(x)= returns =true=. =__pred= will be
invoked with the normal local iterators nested in the safe ones.

Definition at line *69* of file *safe_unordered_container.tcc*.

** template<typename _Container > void
*__gnu_debug::_Safe_unordered_container*< _Container
>::_M_invalidate_locals ()= [inline]=, = [protected]=, = [inherited]=
Definition at line *67* of file *safe_unordered_container.h*.

** void __gnu_debug::_Safe_sequence_base::_M_revalidate_singular
()= [protected]=, = [inherited]=
Revalidates all attached singular iterators. This method may be used to
validate iterators that were invalidated before (but for some reason,
such as an exception, need to become valid again).

** _Safe_container & *__gnu_debug::_Safe_container*< *unordered_set*<
_Value, *std::hash*< _Value >, *std::equal_to*< _Value >,
*std::allocator*< _Value > > , *std::allocator*< _Value > ,
*__gnu_debug::_Safe_unordered_container* , true >::_M_safe
()= [inline]=, = [protected]=, = [noexcept]=, = [inherited]=
Definition at line *52* of file *safe_container.h*.

** void *__gnu_debug::_Safe_container*< *unordered_set*< _Value,
*std::hash*< _Value >, *std::equal_to*< _Value >, *std::allocator*<
_Value > > , *std::allocator*< _Value > ,
*__gnu_debug::_Safe_unordered_container* , true >::_M_swap
(*_Safe_container*< *unordered_set*< _Value, *std::hash*< _Value >,
*std::equal_to*< _Value >, *std::allocator*< _Value > >,
*std::allocator*< _Value >, *__gnu_debug::_Safe_unordered_container* > &
__x)= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *111* of file *safe_container.h*.

** void __gnu_debug::_Safe_sequence_base::_M_swap (*_Safe_sequence_base*
& __x)= [protected]=, = [noexcept]=, = [inherited]=
Swap this sequence with the given sequence. This operation also swaps
ownership of the iterators, so that when the operation is complete all
iterators that originally referenced one container now reference the
other container.

** void __gnu_debug::_Safe_unordered_container_base::_M_swap
(*_Safe_unordered_container_base* & __x)= [protected]=, = [noexcept]=,
= [inherited]=
Swap this container with the given container. This operation also swaps
ownership of the iterators, so that when the operation is complete all
iterators that originally referenced one container now reference the
other container.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_iterator* *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::begin () const= [inline]=, = [noexcept]=
Definition at line *226* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::begin ()= [inline]=, = [noexcept]=
Definition at line *222* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *local_iterator* *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::begin (size_type __b)= [inline]=
Definition at line *247* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_local_iterator*
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc >::begin
(size_type __b) const= [inline]=
Definition at line *261* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> size_type *std::__debug::unordered_set*< _Value,
_Hash, _Pred, _Alloc >::bucket_size (size_type __b) const= [inline]=
Definition at line *289* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_iterator* *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::cbegin () const= [inline]=,
= [noexcept]=
Definition at line *238* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_local_iterator*
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc >::cbegin
(size_type __b) const= [inline]=
Definition at line *275* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_iterator* *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::cend () const= [inline]=, = [noexcept]=
Definition at line *242* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_local_iterator*
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc >::cend
(size_type __b) const= [inline]=
Definition at line *282* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> void *std::__debug::unordered_set*< _Value,
_Hash, _Pred, _Alloc >::clear ()= [inline]=, = [noexcept]=
Definition at line *215* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> template<typename... _Args> *std::pair*<
*iterator*, bool > *std::__debug::unordered_set*< _Value, _Hash, _Pred,
_Alloc >::emplace (_Args &&... __args)= [inline]=
Definition at line *308* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> template<typename... _Args> *iterator*
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc
>::emplace_hint (*const_iterator* __hint, _Args &&... __args)= [inline]=
Definition at line *318* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_iterator* *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::end () const= [inline]=, = [noexcept]=
Definition at line *234* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::end ()= [inline]=, = [noexcept]=
Definition at line *230* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *local_iterator* *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::end (size_type __b)= [inline]=
Definition at line *254* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_local_iterator*
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc >::end
(size_type __b) const= [inline]=
Definition at line *268* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::pair*< *iterator*, *iterator* >
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc
>::equal_range (const key_type & __key)= [inline]=
Definition at line *456* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::pair*< *const_iterator*, *const_iterator*
> *std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc
>::equal_range (const key_type & __key) const= [inline]=
Definition at line *475* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> size_type *std::__debug::unordered_set*< _Value,
_Hash, _Pred, _Alloc >::erase (const key_type & __key)= [inline]=
Definition at line *494* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::erase (*const_iterator* __first,
*const_iterator* __last)= [inline]=
Definition at line *521* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::erase (*const_iterator* __it)= [inline]=
Definition at line *507* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::erase (*iterator* __it)= [inline]=
Definition at line *514* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> node_type *std::__debug::unordered_set*< _Value,
_Hash, _Pred, _Alloc >::extract (const key_type & __key)= [inline]=
Definition at line *403* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> node_type *std::__debug::unordered_set*< _Value,
_Hash, _Pred, _Alloc >::extract (*const_iterator* __position)= [inline]=
Definition at line *396* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::find (const key_type & __key)= [inline]=
Definition at line *430* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_iterator* *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::find (const key_type & __key)
const= [inline]=
Definition at line *443* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> template<typename _InputIterator > void
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc >::insert
(_InputIterator __first, _InputIterator __last)= [inline]=
Definition at line *376* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::pair*< *iterator*, bool >
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc >::insert
(const value_type & __obj)= [inline]=
Definition at line *329* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::insert (*const_iterator* __hint, const
value_type & __obj)= [inline]=
Definition at line *338* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::insert (*const_iterator* __hint,
node_type && __nh)= [inline]=
Definition at line *420* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::insert (*const_iterator* __hint,
value_type && __obj)= [inline]=
Definition at line *357* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *insert_return_type*
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc >::insert
(node_type && __nh)= [inline]=
Definition at line *412* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> void *std::__debug::unordered_set*< _Value,
_Hash, _Pred, _Alloc >::insert (*std::initializer_list*< value_type >
__l)= [inline]=
Definition at line *367* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::pair*< *iterator*, bool >
*std::__debug::unordered_set*< _Value, _Hash, _Pred, _Alloc >::insert
(value_type && __obj)= [inline]=
Definition at line *348* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> float *std::__debug::unordered_set*< _Value,
_Hash, _Pred, _Alloc >::max_load_factor () const= [inline]=,
= [noexcept]=
Definition at line *296* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> void *std::__debug::unordered_set*< _Value,
_Hash, _Pred, _Alloc >::max_load_factor (float __f)= [inline]=
Definition at line *300* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *unordered_set* & *std::__debug::unordered_set*<
_Value, _Hash, _Pred, _Alloc >::operator= (*initializer_list*<
value_type > __l)= [inline]=
Definition at line *199* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> void *std::__debug::unordered_set*< _Value,
_Hash, _Pred, _Alloc >::swap (*unordered_set*< _Value, _Hash, _Pred,
_Alloc > & __x)= [inline]=, = [noexcept]=
Definition at line *207* of file *debug/unordered_set*.

* Friends And Related Function Documentation
** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> template<typename _ItT , typename _SeqT ,
typename _CatT > friend class ::*__gnu_debug::_Safe_iterator*= [friend]=
Definition at line *77* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> template<typename _ItT , typename _SeqT > friend
class ::*__gnu_debug::_Safe_local_iterator*= [friend]=
Definition at line *79* of file *debug/unordered_set*.

* Member Data Documentation
** _Safe_iterator_base*
__gnu_debug::_Safe_sequence_base::_M_const_iterators= [inherited]=
The list of constant iterators that reference this container.

Definition at line *197* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** _Safe_iterator_base*
__gnu_debug::_Safe_unordered_container_base::_M_const_local_iterators= [inherited]=
The list of constant local iterators that reference this container.

Definition at line *130* of file *safe_unordered_base.h*.

** _Safe_iterator_base*
__gnu_debug::_Safe_sequence_base::_M_iterators= [inherited]=
The list of mutable iterators that reference this container.

Definition at line *194* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** _Safe_iterator_base*
__gnu_debug::_Safe_unordered_container_base::_M_local_iterators= [inherited]=
The list of mutable local iterators that reference this container.

Definition at line *127* of file *safe_unordered_base.h*.

** unsigned int
__gnu_debug::_Safe_sequence_base::_M_version= [mutable]=, = [inherited]=
The container version number. This number may never be 0.

Definition at line *200* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence_base::_M_invalidate_all()*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
