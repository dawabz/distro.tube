#+TITLE: Manpages - XPutBackEvent.3
#+DESCRIPTION: Linux manpage for XPutBackEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XPutBackEvent - put events back on the queue

* SYNTAX
XPutBackEvent ( Display */display/ , XEvent */event/ );

* ARGUMENTS
- display :: Specifies the connection to the X server.

- event :: Specifies the event.

* DESCRIPTION
The *XPutBackEvent* function pushes an event back onto the head of the
display's event queue by copying the event into the queue. This can be
useful if you read an event and then decide that you would rather deal
with it later. There is no limit to the number of times in succession
that you can call *XPutBackEvent*.

* SEE ALSO
XAnyEvent(3), XIfEvent(3), XNextEvent(3), XSendEvent(3)\\
/Xlib - C Language X Interface/
