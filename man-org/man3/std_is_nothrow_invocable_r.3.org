#+TITLE: Manpages - std_is_nothrow_invocable_r.3
#+DESCRIPTION: Linux manpage for std_is_nothrow_invocable_r.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::is_nothrow_invocable_r< _Ret, _Fn, _ArgTypes > -
std::is_nothrow_invocable_r

* SYNOPSIS
\\

Inherits __and_::type.

* Detailed Description
** "template<typename _Ret, typename _Fn, typename... _ArgTypes>
\\
struct std::is_nothrow_invocable_r< _Ret, _Fn, _ArgTypes
>"std::is_nothrow_invocable_r

Definition at line *2986* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
