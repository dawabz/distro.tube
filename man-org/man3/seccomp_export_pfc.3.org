#+TITLE: Manpages - seccomp_export_pfc.3
#+DESCRIPTION: Linux manpage for seccomp_export_pfc.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about seccomp_export_pfc.3 is found in manpage for: [[../man3/seccomp_export_bpf.3][man3/seccomp_export_bpf.3]]