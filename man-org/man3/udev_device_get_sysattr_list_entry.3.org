#+TITLE: Manpages - udev_device_get_sysattr_list_entry.3
#+DESCRIPTION: Linux manpage for udev_device_get_sysattr_list_entry.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about udev_device_get_sysattr_list_entry.3 is found in manpage for: [[../udev_device_has_tag.3][udev_device_has_tag.3]]