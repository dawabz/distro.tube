#+TITLE: Manpages - gnutls_pkcs_schema_get_oid.3
#+DESCRIPTION: Linux manpage for gnutls_pkcs_schema_get_oid.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_pkcs_schema_get_oid - API function

* SYNOPSIS
*#include <gnutls/x509.h>*

*const char * gnutls_pkcs_schema_get_oid(unsigned int */schema/*);*

* ARGUMENTS
- unsigned int schema :: Holds the PKCS *12* or PBES2 schema
  (*gnutls_pkcs_encrypt_flags_t*)

* DESCRIPTION
This function will return the object identifier of the PKCS12 or PBES2
schema.

* RETURNS
a constraint string or *NULL* on error.

* SINCE
3.4.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
