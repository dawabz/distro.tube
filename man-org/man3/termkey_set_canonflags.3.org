#+TITLE: Manpages - termkey_set_canonflags.3
#+DESCRIPTION: Linux manpage for termkey_set_canonflags.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
termkey_set_canonflags, termkey_get_canonflags - control the
canonicalisation flags

* SYNOPSIS
#+begin_example
  #include <termkey.h>

  void termkey_set_canonflags(TermKey *tk, int newflags);
  int termkey_get_canonflags(TermKey *tk);
#+end_example

Link with /-ltermkey/.

* DESCRIPTION
*termkey_set_canonflags*() changes the set of canonicalisation flags in
the *termkey*(7) instance to those given by /newflags/. For detail on
the available flags and their meaning, see the *termkey* manpage.

*termkey_get_canonflags*() returns the value set by the last call to
*termkey_set_canonflags*().

* RETURN VALUE
*termkey_set_canonflags*() returns no value. *termkey_get_canonflags*()
returns the current canonicalisation flags.

* SEE ALSO
*termkey_canonicalise*(3), *termkey*(7)
