#+TITLE: Manpages - XtGetErrorDatabase.3
#+DESCRIPTION: Linux manpage for XtGetErrorDatabase.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtGetErrorDatabase, XtGetErrorDatabaseText - obtain error database

* SYNTAX
#include <X11/Intrinsic.h>

XrmDatabase *XtGetErrorDatabase(void);

void XtGetErrorDatabaseText(const char */name/, const char */type/,
const char */class/, const char */defaultMsg/, char */buffer_return/,
int /nbytes/);

* ARGUMENTS
- buffer_return :: Specifies the buffer into which the error message is
  to be returned.

- class :: Specifies the resource class of the error message.

- default :: Specifies the default message to use.

- name :: \\

- type :: Specifies the name and type that are concatenated to form the
  resource name of the error message.

- nbytes :: Specifies the size of the buffer in bytes.

* DESCRIPTION
The *XtGetErrorDatabase* function has been superceded by
*XtAppGetErrorDatabase*.

The *XtGetErrorDatabaseText* function has been superceded by
*XtAppGetErrorDatabaseText*.

* SEE ALSO
*XtAppGetErrorDatabase*(3) *XtAppGetErrorDatabaseText*(3)\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
