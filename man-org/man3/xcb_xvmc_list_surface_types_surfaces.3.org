#+TITLE: Manpages - xcb_xvmc_list_surface_types_surfaces.3
#+DESCRIPTION: Linux manpage for xcb_xvmc_list_surface_types_surfaces.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_xvmc_list_surface_types_surfaces.3 is found in manpage for: [[../man3/xcb_xvmc_list_surface_types.3][man3/xcb_xvmc_list_surface_types.3]]