#+TITLE: Manpages - pcre2_match_context_copy.3
#+DESCRIPTION: Linux manpage for pcre2_match_context_copy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  pcre2_match_context *pcre2_match_context_copy(
   pcre2_match_context *mcontext);
#+end_example

* DESCRIPTION
This function makes a new copy of a match context, using the memory
allocation function that was used for the original context. The result
is NULL if the memory cannot be obtained.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
