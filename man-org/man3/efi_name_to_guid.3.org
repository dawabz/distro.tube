#+TITLE: Manpages - efi_name_to_guid.3
#+DESCRIPTION: Linux manpage for efi_name_to_guid.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about efi_name_to_guid.3 is found in manpage for: [[../man3/efi_get_variable.3][man3/efi_get_variable.3]]