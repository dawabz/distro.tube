#+TITLE: Manpages - FcFontSetSortDestroy.3
#+DESCRIPTION: Linux manpage for FcFontSetSortDestroy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcFontSetSortDestroy - DEPRECATED destroy a font set

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcFontSetSortDestroy (FcFontSet */set/*);*

* DESCRIPTION
This function is DEPRECATED. *FcFontSetSortDestroy* destroys /set/ by
calling *FcFontSetDestroy*. Applications should use *FcFontSetDestroy*
directly instead.
