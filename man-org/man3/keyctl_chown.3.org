#+TITLE: Manpages - keyctl_chown.3
#+DESCRIPTION: Linux manpage for keyctl_chown.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
keyctl_chown - change the ownership of a key

* SYNOPSIS
#+begin_example
  #include <keyutils.h>

  long keyctl_chown(key_serial_t key, uid_t uid, gid_t gid);
#+end_example

* DESCRIPTION
*keyctl_chown*() changes the user and group ownership details of a key.

A setting of *-1* on either /uid/ or /gid/ will cause that setting to be
ignored.

A process that does not have the *SysAdmin* capability may not change a
key's UID or set the key's GID to a value that does not match the
process's GID or one of its group list.

The caller must have *setattr* permission on a key to be able to change
its ownership.

* RETURN VALUE
On success *keyctl_chown*() returns *0 .* On error, the value *-1* will
be returned and /errno/ will have been set to an appropriate error.

* ERRORS
- *ENOKEY* :: The specified key does not exist.

- *EKEYEXPIRED* :: The specified key has expired.

- *EKEYREVOKED* :: The specified key has been revoked.

- *EDQUOT* :: Changing the UID to the one specified would run that UID
  out of quota.

- *EACCES* :: The key exists, but does not grant *setattr* permission to
  the calling process.

* LINKING
This is a library function that can be found in /libkeyutils/. When
linking, *-lkeyutils* should be specified to the linker.

* SEE ALSO
*keyctl*(1), *add_key*(2), *keyctl*(2), *request_key*(2), *keyctl*(3),
*keyrings*(7), *keyutils*(7)
