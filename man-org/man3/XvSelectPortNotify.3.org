#+TITLE: Manpages - XvSelectPortNotify.3
#+DESCRIPTION: Linux manpage for XvSelectPortNotify.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XvSelectPortNotify - enable or disable XvPortNotify events

* SYNOPSIS
*#include <X11/extensions/Xvlib.h>*

#+begin_example
  int XvSelectPortNotify(Display *dpy, XvPortID port, Bool onoff);
#+end_example

* ARGUMENTS
- dpy :: Specifies the connection to the X server.

- port :: Specifies the port for which PortNotify events are to be
  generated when its attributes are changed using
  *XvSetPortAttribute*(3)

- onoff :: Specifies whether notification is to be enabled or disabled.

* DESCRIPTION
*XvSelectPortNotify*(3) enables or disables PortNotify event delivery to
the requesting client. *XvPortNotify*(3) events are generated when port
attributes are changed using *XvSetPortAttribute*(3).

* RETURN VALUES
- [Success] :: Returned if *XvSelectPortNotify*(3) completed
  successfully.

- [XvBadExtension] :: Returned if the Xv extension is unavailable.

- [XvBadAlloc] :: Returned if *XvSelectPortNotify*(3) failed to allocate
  memory to process the request.

* DIAGNOSTICS
- [XvBadPort] :: Generated if the requested port does not exist.

* SEE ALSO
*XvSetPortNotify*(3), *XvSetPortAttribute*(3), *XvPortNotify*(3)
