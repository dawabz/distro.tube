#+TITLE: Manpages - X509_check_issued.3ssl
#+DESCRIPTION: Linux manpage for X509_check_issued.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
X509_check_issued - checks if certificate is apparently issued by
another certificate

* SYNOPSIS
#include <openssl/x509v3.h> int X509_check_issued(X509 *issuer, X509
*subject);

* DESCRIPTION
*X509_check_issued()* checks if certificate /subject/ was apparently
issued using (CA) certificate /issuer/. This function takes into account
not only matching of the issuer field of /subject/ with the subject
field of /issuer/, but also compares all sub-fields of the
*authorityKeyIdentifier* extension of /subject/, as far as present, with
the respective *subjectKeyIdentifier*, serial number, and issuer fields
of /issuer/, as far as present. It also checks if the *keyUsage* field
(if present) of /issuer/ allows certificate signing. It does not check
the certificate signature.

* RETURN VALUES
Function return *X509_V_OK* if certificate /subject/ is issued by
/issuer/ or some *X509_V_ERR** constant to indicate an error.

* SEE ALSO
*X509_verify_cert* (3), *X509_check_ca* (3), *verify* (1)

* COPYRIGHT
Copyright 2015-2020 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
