#+TITLE: Manpages - autodie_Scope_Guard.3perl
#+DESCRIPTION: Linux manpage for autodie_Scope_Guard.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
autodie::Scope::Guard - Wrapper class for calling subs at end of scope

* SYNOPSIS
use autodie::Scope::Guard; $^H{my-key} = autodie::Scope::Guard->new(sub
{ print "Hallo world\n"; });

* DESCRIPTION
This class is used to bless perl subs so that they are invoked when they
are destroyed. This is mostly useful for ensuring the code is invoked at
end of scope. This module is not a part of autodie's public API.

This module is directly inspired by chocolateboy's excellent
Scope::Guard module.

** Methods
/new/

my $hook = autodie::Scope::Guard->new(sub {});

Creates a new =autodie::Scope::Guard=, which will invoke the given sub
once it goes out of scope (i.e. its DESTROY handler is called).

* AUTHOR
Copyright 2008-2009, Paul Fenwick <pjf@perltraining.com.au>

* LICENSE
This module is free software. You may distribute it under the same terms
as Perl itself.
