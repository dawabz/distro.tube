#+TITLE: Manpages - XDeleteProperty.3
#+DESCRIPTION: Linux manpage for XDeleteProperty.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XDeleteProperty.3 is found in manpage for: [[../man3/XGetWindowProperty.3][man3/XGetWindowProperty.3]]