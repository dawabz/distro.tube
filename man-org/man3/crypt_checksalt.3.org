#+TITLE: Manpages - crypt_checksalt.3
#+DESCRIPTION: Linux manpage for crypt_checksalt.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
checks the

string against the system configuration and reports whether the hashing
method and parameters it specifies are acceptable. It is intended to be
used by programs such as

to determine whether the user's passphrase should be re-hashed using the
currently preferred hashing method.

The return value is 0 if there is nothing wrong with this setting.
Otherwise, it is one of the following constants:

is a fully correct setting string. This constant is guaranteed to
equal 0.

is not a valid setting string; either it specifies a hashing method that
is not known to this version of libxcrypt, or it specifies invalid
parameters for the method.

specifies a hashing method that is no longer allowed to be used at all;

will fail if passed this

Manual intervention will be required to reactivate the user's account.

specifies a hashing method that is no longer considered strong enough
for use with new passphrases.

will still authenticate a passphrase against this setting, but if
authentication succeeds, the passphrase should be re-hashed using the
currently preferred method.

specifies cost parameters that are considered too cheap for use with new
passphrases.

will still authenticate a passphrase against this setting, but if
authentication succeeds, the passphrase should be re-hashed using the
currently preferred method.

will define the macro

if

is available in the current version of libxcrypt.

Since full configurability is not yet implemented, the current
implementation will only ever return

or

when invoked.

The function

is not part of any standard. It was added to libxcrypt in version 4.3.0.

For an explanation of the terms used in this section, see

| Interface | Attribute     | Value   |
|           | Thread safety | MT-Safe |
