#+TITLE: Manpages - sqrtf.3
#+DESCRIPTION: Linux manpage for sqrtf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sqrtf.3 is found in manpage for: [[../man3/sqrt.3][man3/sqrt.3]]