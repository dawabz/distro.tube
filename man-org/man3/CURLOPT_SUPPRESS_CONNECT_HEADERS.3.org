#+TITLE: Manpages - CURLOPT_SUPPRESS_CONNECT_HEADERS.3
#+DESCRIPTION: Linux manpage for CURLOPT_SUPPRESS_CONNECT_HEADERS.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_SUPPRESS_CONNECT_HEADERS - suppress proxy CONNECT response
headers from user callbacks

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>

  CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SUPPRESS_CONNECT_HEADERS, long onoff);
#+end_example

* DESCRIPTION
When /CURLOPT_HTTPPROXYTUNNEL(3)/ is used and a CONNECT request is made,
suppress proxy CONNECT response headers from the user callback functions
/CURLOPT_HEADERFUNCTION(3)/ and /CURLOPT_WRITEFUNCTION(3)/.

Proxy CONNECT response headers can complicate header processing since
it's essentially a separate set of headers. You can enable this option
to suppress those headers.

For example let's assume an HTTPS URL is to be retrieved via CONNECT. On
success there would normally be two sets of headers, and each header
line sent to the header function and/or the write function. The data
given to the callbacks would look like this:

#+begin_example
  HTTP/1.1 200 Connection established
  {headers}...

  HTTP/1.1 200 OK
  Content-Type: application/json
  {headers}...

  {body}...
#+end_example

However by enabling this option the CONNECT response headers are
suppressed, so the data given to the callbacks would look like this:

#+begin_example
  HTTP/1.1 200 OK
  Content-Type: application/json
  {headers}...

  {body}...
#+end_example

* DEFAULT
0

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    curl_easy_setopt(curl, CURLOPT_HEADER, 1L);
    curl_easy_setopt(curl, CURLOPT_PROXY, "http://foo:3128");
    curl_easy_setopt(curl, CURLOPT_HTTPPROXYTUNNEL, 1L);
    curl_easy_setopt(curl, CURLOPT_SUPPRESS_CONNECT_HEADERS, 1L);

    curl_easy_perform(curl);

    /* always cleanup */
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.54.0

* RETURN VALUE
CURLE_OK or an error such as CURLE_UNKNOWN_OPTION.

* SEE ALSO
*CURLOPT_HEADER*(3), *CURLOPT_PROXY*(3), *CURLOPT_HTTPPROXYTUNNEL*(3),
