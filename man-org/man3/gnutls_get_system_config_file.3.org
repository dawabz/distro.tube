#+TITLE: Manpages - gnutls_get_system_config_file.3
#+DESCRIPTION: Linux manpage for gnutls_get_system_config_file.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_get_system_config_file - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*const char * gnutls_get_system_config_file( */void/*);*

* ARGUMENTS
-  void :: 

* DESCRIPTION
Returns the filename of the system wide configuration file loaded by the
library. The returned pointer is valid until the library is unloaded.

* RETURNS
a constant pointer to the config file loaded, or *NULL* if none

* SINCE
3.6.9

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
