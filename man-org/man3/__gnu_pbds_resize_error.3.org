#+TITLE: Manpages - __gnu_pbds_resize_error.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_resize_error.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::resize_error - A container cannot be resized.

* SYNOPSIS
\\

=#include <exception.hpp>=

Inherits *__gnu_pbds::container_error*.

** Public Member Functions
virtual const char * *what* () const noexcept\\

* Detailed Description
A container cannot be resized.

Definition at line *73* of file *exception.hpp*.

* Member Function Documentation
** virtual const char * std::logic_error::what () const= [virtual]=,
= [noexcept]=, = [inherited]=
Returns a C-style character string describing the general cause of the
current error (the same string passed to the ctor).\\

Reimplemented from *std::exception*.

Reimplemented in *std::future_error*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
