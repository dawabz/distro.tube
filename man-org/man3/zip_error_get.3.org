#+TITLE: Manpages - zip_error_get.3
#+DESCRIPTION: Linux manpage for zip_error_get.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The functions

and

are deprecated. Use

and

instead.

For

replace

int ze, se; zip_error_get(za, &ze, &se);

with

int ze, se; zip_error_t *error = zip_get_error(za); ze =
zip_error_code_zip(error); se = zip_error_code_system(error);

For

replace

int ze, se; zip_file_error_get(zf, &ze, &se);

with

int ze, se; zip_error_t *error = zip_file_get_error(zf); ze =
zip_error_code_zip(error); se = zip_error_code_system(error);

was added in libzip 0.6. It was deprecated in libzip 1.0, use

/

instead.

was added in libzip 0.6. It was deprecated in libzip 1.0, use

/

instead.

and
