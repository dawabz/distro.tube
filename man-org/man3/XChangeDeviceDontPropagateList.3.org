#+TITLE: Manpages - XChangeDeviceDontPropagateList.3
#+DESCRIPTION: Linux manpage for XChangeDeviceDontPropagateList.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XChangeDeviceDontPropagateList, XGetDeviceDontPropagateList - query or
change the dont-propagate-list for extension devices

* SYNOPSIS
#+begin_example
  #include <X11/extensions/XInput.h>
#+end_example

#+begin_example
  int XChangeDeviceDontPropagateList( Display *display,
                                      Window window,
                                      int count,
                                      XEventClass *event_list,
                                      int mode);
#+end_example

#+begin_example
  XEventClass* XGetDeviceDontPropagateList( Display *display,
                                            Window window,
                                            int *count);
#+end_example

#+begin_example
  display
         Specifies the connection to the X server.
#+end_example

#+begin_example
  window
         Specifies the window whose dont-propagate-list is to be
         queried or modified.
#+end_example

#+begin_example
  event_list
         Specifies a pointer to a list of event classes.
#+end_example

#+begin_example
  mode
         Specifies the mode. You can pass AddToList, or
         DeleteFromList.
#+end_example

#+begin_example
  count
         Specifies the number of event classes in the list.
#+end_example

* DESCRIPTION

#+begin_quote
  #+begin_example
    The XChangeDeviceDontPropagateList request modifies the list of
    events that should not be propagated to ancestors of the event
    window. This request allows extension events to be added to or
    deleted from that list. By default, all events are propagated
    to ancestor windows. Once modified, the list remains modified
    for the life of the window. Events are not removed from the
    list because the client that added them has terminated.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    Suppression of event propagation is not allowed for all input
    extension events. If a specified event class is one that cannot
    be suppressed, a BadClass error will result. Events that can be
    suppressed include DeviceKeyPress, DeviceKeyRelease,
    DeviceButtonPress, DeviceButtonRelease, DeviceMotionNotify,
    ProximityIn, and ProximityOut.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XChangeDeviceDontPropagateList can generate a BadDevice,
    BadClass, or BadValue error.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The XGetDeviceDontPropagateList request queries the list of
    events that should not be propagated to ancestors of the event
    window.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XGetDeviceDontPropagateList can generate a BadClass or
    BadWindow error.
  #+end_example
#+end_quote

* DIAGNOSTICS

#+begin_quote
  #+begin_example
    BadDevice
           An invalid device was specified. The specified device
           does not exist or has not been opened by this client via
           XOpenInputDevice. This error may also occur if some
           other client has caused the specified device to become
           the X keyboard or X pointer device via the
           XChangeKeyboardDevice or XChangePointerDevice requests.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadValue
           Some numeric value falls outside the range of values
           accepted by the request. Unless a specific range is
           specified for an argument, the full range defined by the
           arguments type is accepted. Any argument defined as a
           set of alternatives can generate this error.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadWindow
           An invalid window id was specified.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadClass
           An invalid event class was specified.
  #+end_example
#+end_quote
