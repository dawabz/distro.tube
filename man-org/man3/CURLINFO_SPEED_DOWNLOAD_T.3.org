#+TITLE: Manpages - CURLINFO_SPEED_DOWNLOAD_T.3
#+DESCRIPTION: Linux manpage for CURLINFO_SPEED_DOWNLOAD_T.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLINFO_SPEED_DOWNLOAD_T - get download speed

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_SPEED_DOWNLOAD_T,
curl_off_t *speed);

* DESCRIPTION
Pass a pointer to a /curl_off_t/ to receive the average download speed
that curl measured for the complete download. Measured in bytes/second.

* PROTOCOLS
* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* Perform the request */
    res = curl_easy_perform(curl);

    if(!res) {
      curl_off_t speed;
      res = curl_easy_getinfo(curl, CURLINFO_SPEED_DOWNLOAD_T, &speed);
      if(!res) {
        printf("Download speed %" CURL_FORMAT_CURL_OFF_T " bytes/sec\n", speed);
      }
    }
  }
#+end_example

* AVAILABILITY
Added in 7.55.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*curl_easy_getinfo*(3), *curl_easy_setopt*(3),
*CURLINFO_SPEED_UPLOAD*(3), *CURLINFO_SIZE_UPLOAD_T*(3),
