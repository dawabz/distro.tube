#+TITLE: Manpages - std_is_swappable.3
#+DESCRIPTION: Linux manpage for std_is_swappable.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::is_swappable< _Tp > - Metafunctions used for detecting swappable
types: p0185r1.

* SYNOPSIS
\\

Inherits __is_swappable_impl::type.

* Detailed Description
** "template<typename _Tp>
\\
struct std::is_swappable< _Tp >"Metafunctions used for detecting
swappable types: p0185r1.

is_swappable

Definition at line *2680* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
