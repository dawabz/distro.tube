#+TITLE: Manpages - pcap_set_buffer_size.3pcap
#+DESCRIPTION: Linux manpage for pcap_set_buffer_size.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_set_buffer_size - set the buffer size for a not-yet-activated
capture handle

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  int pcap_set_buffer_size(pcap_t *p, int buffer_size);
#+end_example

* DESCRIPTION
*pcap_set_buffer_size*() sets the buffer size that will be used on a
capture handle when the handle is activated to /buffer_size/, which is
in units of bytes.

* RETURN VALUE
*pcap_set_buffer_size*() returns *0* on success or
*PCAP_ERROR_ACTIVATED* if called on a capture handle that has been
activated.

* SEE ALSO
*pcap*(3PCAP), *pcap_create*(3PCAP), *pcap_activate*(3PCAP)
