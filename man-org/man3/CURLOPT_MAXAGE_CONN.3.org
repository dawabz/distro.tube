#+TITLE: Manpages - CURLOPT_MAXAGE_CONN.3
#+DESCRIPTION: Linux manpage for CURLOPT_MAXAGE_CONN.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_MAXAGE_CONN - max idle time allowed for reusing a connection

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_MAXAGE_CONN, long
maxage);

* DESCRIPTION
Pass a long as parameter containing /maxage/ - the maximum time in
seconds that you allow an existing connection to have been idle to be
considered for reuse for this request.

The "connection cache" that holds previously used connections. When a
new request is to be done, it will consider any connection that matches
for reuse. The /CURLOPT_MAXAGE_CONN(3)/ limit prevents libcurl from
trying too old connections for reuse, since old connections have a high
risk of not working and thus trying them is a performance loss and
sometimes service loss due to the difficulties to figure out the
situation. If a connection is found in the cache that is older than this
set /maxage/, it will instead be closed.

* DEFAULT
Default maxage is 118 seconds.

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* only allow 30 seconds idle time */
    curl_easy_setopt(curl, CURLOPT_MAXAGE_CONN, 30L);

    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.65.0

* RETURN VALUE
Returns CURLE_OK.

* SEE ALSO
*CURLOPT_TIMEOUT*(3), *CURLOPT_FORBID_REUSE*(3),
*CURLOPT_FRESH_CONNECT*(3), *CURLOPT_MAXLIFETIME_CONN*(3),
