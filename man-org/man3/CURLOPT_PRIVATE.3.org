#+TITLE: Manpages - CURLOPT_PRIVATE.3
#+DESCRIPTION: Linux manpage for CURLOPT_PRIVATE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_PRIVATE - store a private pointer

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PRIVATE, void *pointer);

* DESCRIPTION
Pass a void * as parameter, pointing to data that should be associated
with this curl handle. The pointer can subsequently be retrieved using
/curl_easy_getinfo(3)/ with the CURLINFO_PRIVATE option. libcurl itself
never does anything with this data.

* DEFAULT
NULL

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  struct private secrets;
  if(curl) {
    struct private *extracted;
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* store a pointer to our private struct */
    curl_easy_setopt(curl, CURLOPT_PRIVATE, &secrets);

    curl_easy_perform(curl);

    /* we can extract the private pointer again too */
    curl_easy_getinfo(curl, CURLINFO_PRIVATE, &extracted);
  }
#+end_example

* AVAILABILITY
Added in 7.10.3

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_VERBOSE*(3), *CURLOPT_STDERR*(3),
