#+TITLE: Manpages - SDL_Flip.3
#+DESCRIPTION: Linux manpage for SDL_Flip.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_Flip - Swaps screen buffers

* SYNOPSIS
*#include "SDL.h"*

*int SDL_Flip*(*SDL_Surface *screen*);

* DESCRIPTION
On hardware that supports double-buffering, this function sets up a flip
and returns. The hardware will wait for vertical retrace, and then swap
video buffers before the next video surface blit or lock will return. On
hardware that doesn't support double-buffering, this is equivalent to
calling /SDL_UpdateRect/*(screen, 0, 0, 0, 0)*

The *SDL_DOUBLEBUF* flag must have been passed to /SDL_SetVideoMode/,
when setting the video mode for this function to perform hardware
flipping.

* RETURN VALUE
This function returns *0* if successful, or *-1* if there was an error.

* SEE ALSO
*SDL_SetVideoMode*, *SDL_UpdateRect*, *SDL_Surface*
