#+TITLE: Manpages - unibi_set_str.3
#+DESCRIPTION: Linux manpage for unibi_set_str.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
unibi_get_bool, unibi_set_bool, unibi_get_num, unibi_set_num,
unibi_get_str, unibi_set_str - access the capabilities of a terminal
object

* SYNOPSIS
#include <unibilium.h> int unibi_get_bool(const unibi_term *ut, enum
unibi_boolean b); void unibi_set_bool(unibi_term *ut, enum unibi_boolean
b, int x); int unibi_get_num(const unibi_term *ut, enum unibi_numeric
n); void unibi_set_num(unibi_term *ut, enum unibi_numeric n, int x);
const char *unibi_get_str(const unibi_term *ut, enum unibi_string s);
void unibi_set_str(unibi_term *ut, enum unibi_string s, const char *x);

* DESCRIPTION
Get/set boolean, numeric, and string capabilities. Absent numeric
capabilities are represented as =-1=, absent string capabilities as
=NULL=.

Note that =unibi_set_str= simply stores the pointer it is given; it will
not free /x/ or make a copy of the string.

* SEE ALSO
*unibilium.h* (3)
