#+TITLE: Manpages - tld_check_4tz.3
#+DESCRIPTION: Linux manpage for tld_check_4tz.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tld_check_4tz - API function

* SYNOPSIS
*#include <tld.h>*

*int tld_check_4tz(const uint32_t * */in/*, size_t * */errpos/*, const
Tld_table * */tld/*);*

* ARGUMENTS
- const uint32_t * in :: Zero terminated array of unicode code points to
  process.

- size_t * errpos :: Position of offending character is returned here.

- const Tld_table * tld :: A *Tld_table* data structure representing the
  restrictions for which the input should be tested.

* DESCRIPTION
Test each of the code points in /in/ for whether or not they are allowed
by the data structure in /tld/ , return the position of the first
character for which this is not the case in /errpos/ .

Return value: Returns the *Tld_rc* value *TLD_SUCCESS* if all code
points are valid or when /tld/ is null, *TLD_INVALID* if a character is
not allowed, or additional error codes on general failure conditions.

* REPORTING BUGS
Report bugs to <help-libidn@gnu.org>.\\
General guidelines for reporting bugs: http://www.gnu.org/gethelp/\\
GNU Libidn home page: http://www.gnu.org/software/libidn/

* COPYRIGHT
Copyright © 2002-2021 Simon Josefsson.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libidn* is maintained as a Texinfo manual.
If the *info* and *libidn* programs are properly installed at your site,
the command

#+begin_quote
  *info libidn*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *http://www.gnu.org/software/libidn/manual/*
#+end_quote
