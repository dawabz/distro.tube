#+TITLE: Manpages - atof.3
#+DESCRIPTION: Linux manpage for atof.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
atof - convert a string to a double

* SYNOPSIS
#+begin_example
  #include <stdlib.h>

  double atof(const char *nptr);
#+end_example

* DESCRIPTION
The *atof*() function converts the initial portion of the string pointed
to by /nptr/ to /double/. The behavior is the same as

#+begin_example
  strtod(nptr, NULL);
#+end_example

except that *atof*() does not detect errors.

* RETURN VALUE
The converted value.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface | Attribute     | Value          |
| *atof*()  | Thread safety | MT-Safe locale |

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD.

* SEE ALSO
*atoi*(3), *atol*(3), *strfromd*(3), *strtod*(3), *strtol*(3),
*strtoul*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
