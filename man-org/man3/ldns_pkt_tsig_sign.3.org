#+TITLE: Manpages - ldns_pkt_tsig_sign.3
#+DESCRIPTION: Linux manpage for ldns_pkt_tsig_sign.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_pkt_tsig_verify, ldns_pkt_tsig_sign - tsig signing and verification

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

bool ldns_pkt_tsig_verify(ldns_pkt *pkt, const uint8_t *wire, size_t
wire_size, const char *key_name, const char *key_data, const ldns_rdf
*mac);

ldns_status ldns_pkt_tsig_sign(ldns_pkt *pkt, const char *key_name,
const char *key_data, uint16_t fudge, const char *algorithm_name, const
ldns_rdf *query_mac);

* DESCRIPTION
/ldns_pkt_tsig_verify/() verifies the tsig rr for the given packet and
key. The wire must be given too because tsig does not sign normalized
packets. .br *pkt*: the packet to verify .br *wire*: needed to verify
the mac .br *wire_size*: size of wire .br *key_name*: the name of the
shared key .br *key_data*: the key in base 64 format .br *mac*: original
mac .br Returns true if tsig is correct, false if not, or if tsig is not
set

/ldns_pkt_tsig_sign/() creates a tsig rr for the given packet and key.
.br *pkt*: the packet to sign .br *key_name*: the name of the shared key
.br *key_data*: the key in base 64 format .br *fudge*: seconds of error
permitted in time signed .br *algorithm_name*: the name of the algorithm
used .br *query_mac*: is added to the digest if not NULL (so NULL is for
signing queries, not NULL is for signing answers) .br Returns status (OK
if success)

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_key/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*,
*RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
