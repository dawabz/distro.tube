#+TITLE: Manpages - ldns_dnssec_verify_denial_nsec3.3
#+DESCRIPTION: Linux manpage for ldns_dnssec_verify_denial_nsec3.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_dnssec_verify_denial, ldns_dnssec_verify_denial_nsec3 - verify
denial of existence

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_status ldns_dnssec_verify_denial(ldns_rr *rr, ldns_rr_list *nsecs,
ldns_rr_list *rrsigs);

ldns_status ldns_dnssec_verify_denial_nsec3(ldns_rr *rr, ldns_rr_list
*nsecs, ldns_rr_list *rrsigs, ldns_pkt_rcode packet_rcode, ldns_rr_type
packet_qtype, bool packet_nodata);

* DESCRIPTION
/ldns_dnssec_verify_denial/() denial is not just a river in egypt

.br *rr*: The (query) RR to check the denial of existence for .br
*nsecs*: The list of NSEC RRs that are supposed to deny the existence of
the RR .br *rrsigs*: The RRSIG RR covering the NSEC RRs .br Returns
LDNS_STATUS_OK if the NSEC RRs deny the existence, error code containing
the reason they do not otherwise

/ldns_dnssec_verify_denial_nsec3/() Denial of existence using NSEC3
records Since NSEC3 is a bit more complicated than normal denial, some
context arguments are needed

.br *rr*: The (query) RR to check the denial of existence for .br
*nsecs*: The list of NSEC3 RRs that are supposed to deny the existence
of the RR .br *rrsigs*: The RRSIG rr covering the NSEC RRs .br
*packet_rcode*: The RCODE value of the packet that provided the NSEC3
RRs .br *packet_qtype*: The original query RR type .br *packet_nodata*:
True if the providing packet had an empty ANSWER section .br Returns
LDNS_STATUS_OK if the NSEC3 RRs deny the existence, error code
containing the reason they do not otherwise

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_dnssec_trust_tree/, /ldns_dnssec_data_chain/. And *perldoc
Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*, *RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
