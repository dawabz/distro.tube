#+TITLE: Manpages - libzip.3
#+DESCRIPTION: Linux manpage for libzip.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

is a library for reading, creating, and modifying zip archives.

The main design criteria for

were:

Do not create corrupt files, even in case of errors.

Do not delete data.

Be efficient.

For this reason, when modifying zip archives,

writes to a temporary file and replaces the original zip archive
atomically.

Below there are two sections listing functions: one for how to read from
zip archives and one for how to create/modify them.

In general, different zip archives opened by

are independent of each other and can be used by parallel-running
threads without locking. If you want to use an archive from multiple
threads, you have to synchronize access to it yourself. If you use an
archive as a source for

or

access to the target archive must be synchronized with access to the
source archive as well.

(uncompressed files only)

(uncompressed files only)

and
