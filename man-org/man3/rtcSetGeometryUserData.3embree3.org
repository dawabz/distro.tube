#+TITLE: Manpages - rtcSetGeometryUserData.3embree3
#+DESCRIPTION: Linux manpage for rtcSetGeometryUserData.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcSetGeometryUserData - sets the user-defined data pointer of the
    geometry
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcSetGeometryUserData(RTCGeometry geometry, void* userPtr);
#+end_example

** DESCRIPTION
The =rtcSetGeometryUserData= function sets the user-defined data pointer
(=userPtr= argument) for a geometry (=geometry= argument). This user
data pointer is intended to be pointing to the application's
representation of the geometry, and is passed to various callback
functions. The application can use this pointer inside the callback
functions to access its geometry representation.

The =rtcGetGeometryUserData= function can be used to query an already
set user data pointer of a geometry.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcGetGeometryUserData]
