#+TITLE: Manpages - dispatch_data_create.3
#+DESCRIPTION: Linux manpage for dispatch_data_create.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Dispatch data objects are opaque containers of bytes that represent one
or more regions of memory. They are created either from memory buffers
managed by the application or the system or from other dispatch data
objects. Dispatch data objects are immutable and the memory regions they
represent are required to remain unchanged for the lifetime of all data
objects that reference them. Dispatch data objects avoid copying the
represented memory as much as possible. Multiple data objects can
represent the same memory regions or subsections thereof.

The

function creates a new dispatch data object of given

from a

The provided

block will be submitted to the specified

when the object reaches the end of its lifecycle, indicating that the
system no longer references the

This allows the application to deallocate the associated storage. The

argument is ignored if one of the following predefined destructors is
passed:

indicates that the provided buffer can be deallocated with

directly.

indicates that the provided buffer is not managed by the application and
should be copied into memory managed and automatically deallocated by
the system.

The

function creates a new data object representing the concatenation of the
memory regions represented by the provided data objects.

The

function creates a new data object representing the sub-region of the
provided

object specified by the

and

parameters.

The

function creates a new data object by mapping the memory represented by
the provided

object as a single contiguous memory region (moving or copying memory as
necessary). If the

and

references are not

they are filled with the location and extent of the contiguous region,
allowing direct read access to the mapped memory. These values are valid
only as long as the newly created object has not been released.

The

function provides read access to represented memory without requiring it
to be mapped as a single contiguous region. It traverses the memory
regions represented by the

argument in logical order, invokes the specified

block for each region and returns a boolean indicating whether traversal
completed successfully. The

block is passed the following arguments for each memory region and
returns a boolean indicating whether traversal should continue:

data object representing the region

logical position of the region in

memory location of the region

extent of the region

The

data object is released by the system when the

block returns. The associated memory location

is valid only as long as

has not been deallocated; if

is needed outside of the

block, the

object must be retained in the block.

The

function finds the contiguous memory region containing the logical
position specified by the

argument among the regions represented by the provided

object and returns a newly created copy of the data object representing
that region. The variable specified by the

argument is filled with the logical position where the returned object
starts in the

object.

The

function returns the logical size of the memory region or regions
represented by the provided

object.

The

object is the global singleton object representing a zero-length memory
region. It is a valid input to any dispatch_data functions that take
data object parameters.

Dispatch data objects are retained and released via calls to

and

Data objects passed as arguments to a dispatch data

or

function can be released when the function returns. The newly created
object holds implicit references to their constituent memory regions as
necessary.

The functions

and

return an interior pointer to represented memory that is only valid as
long as an associated object has not been released. When Objective-C
Automated Reference Counting is enabled, care needs to be taken if that
object is held in a variable with automatic storage. It may need to be
annotated with the

attribute, or stored in a

instance variable instead, to ensure that the object is not released
prematurely before memory accesses via the interor pointer have been
completed.
