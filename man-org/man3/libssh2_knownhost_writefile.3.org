#+TITLE: Manpages - libssh2_knownhost_writefile.3
#+DESCRIPTION: Linux manpage for libssh2_knownhost_writefile.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_knownhost_writefile - write a collection of known hosts to a
file

* SYNOPSIS
#include <libssh2.h>

int libssh2_knownhost_writefile(LIBSSH2_KNOWNHOSTS *hosts, const char
*filename, int type);

* DESCRIPTION
Writes all the known hosts to the specified file using the specified
file format.

/filename/ specifies what filename to create

/type/ specifies what file type it is, and
/LIBSSH2_KNOWNHOST_FILE_OPENSSH/ is the only currently supported format.

* RETURN VALUE
Returns a regular libssh2 error code, where negative values are error
codes and 0 indicates success.

* AVAILABILITY
Added in libssh2 1.2

* SEE ALSO
*libssh2_knownhost_readfile(3)* *libssh2_knownhost_add(3)*
