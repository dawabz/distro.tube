#+TITLE: Manpages - pcre2_pattern_convert.3
#+DESCRIPTION: Linux manpage for pcre2_pattern_convert.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  int pcre2_pattern_convert(PCRE2_SPTR pattern, PCRE2_SIZE length,
   uint32_t options, PCRE2_UCHAR **buffer,
   PCRE2_SIZE *blength, pcre2_convert_context *cvcontext);
#+end_example

* DESCRIPTION
This function is part of an experimental set of pattern conversion
functions. It converts a foreign pattern (for example, a glob) into a
PCRE2 regular expression pattern. Its arguments are:

/pattern/ The foreign pattern /length/ The length of the input pattern
or PCRE2_ZERO_TERMINATED /options/ Option bits /buffer/ Pointer to
pointer to output buffer, or NULL /blength/ Pointer to output length
field /cvcontext/ Pointer to a convert context or NULL

The length of the converted pattern (excluding the terminating zero) is
returned via /blength/. If /buffer/ is NULL, the function just returns
the output length. If /buffer/ points to a NULL pointer, heap memory is
obtained for the converted pattern, using the allocator in the context
if present (or else *malloc()*), and the field pointed to by /buffer/ is
updated. If /buffer/ points to a non-NULL field, that must point to a
buffer whose size is in the variable pointed to by /blength/. This value
is updated.

The option bits are:

PCRE2_CONVERT_UTF Input is UTF PCRE2_CONVERT_NO_UTF_CHECK Do not check
UTF validity PCRE2_CONVERT_POSIX_BASIC Convert POSIX basic pattern
PCRE2_CONVERT_POSIX_EXTENDED Convert POSIX extended pattern
PCRE2_CONVERT_GLOB ) Convert PCRE2_CONVERT_GLOB_NO_WILD_SEPARATOR )
various types PCRE2_CONVERT_GLOB_NO_STARSTAR ) of glob

The return value from *pcre2_pattern_convert()* is zero on success or a
non-zero PCRE2 error code.

The pattern conversion functions are described in the *pcre2convert*
documentation.
