#+TITLE: Manpages - Test2_EventFacet_Info_Table.3perl
#+DESCRIPTION: Linux manpage for Test2_EventFacet_Info_Table.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test2::EventFacet::Info::Table - Intermediary representation of a table.

* DESCRIPTION
Intermediary representation of a table for use in specialized
Test::API::Context methods which generate Test2::EventFacet::Info
facets.

* SYNOPSIS
use Test2::EventFacet::Info::Table; use Test2::API qw/context/; sub
my_tool { my $ctx = context(); ... $ctx->fail( $name, "failure diag
message", Test2::EventFacet::Info::Table->new( # Required rows => [[a,
b], [c, d], ...], # Strongly Recommended as_string => "... string to
print when table cannot be rendered ...", # Optional header => [col1,
col2], collapse => $bool, no_collapse => [col1, ...], ), ); ...
$ctx->release; } my_tool();

* ATTRIBUTES
- $header_aref = $t->header() :: 

- $rows_aref = $t->rows() :: 

- $bool = $t->collapse() :: 

- $aref = $t->no_collapse() :: 

The above are all directly tied to the table hashref structure described
in Test2::EventFacet::Info.

- $str = $t->as_string() :: This returns the string form of the table if
  it was set, otherwise it returns the string ="<TABLE NOT DISPLAYED>"=.

- $href = $t->as_hash() :: This returns the data structure used for
  tables by Test2::EventFacet::Info.

- %args = $t->info_args() :: This returns the arguments that should be
  used to construct the proper Test2::EventFacet::Info structure. return
  (table => $t->as_hash(), details => $t->as_string());

* SOURCE
The source code repository for Test2 can be found at
/http://github.com/Test-More/test-more//.

* MAINTAINERS
- Chad Granum <exodist@cpan.org> :: 

* AUTHORS
- Chad Granum <exodist@cpan.org> :: 

* COPYRIGHT
Copyright 2020 Chad Granum <exodist@cpan.org>.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See /http://dev.perl.org/licenses//
