#+TITLE: Manpages - DBI_Gofer_Request.3pm
#+DESCRIPTION: Linux manpage for DBI_Gofer_Request.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
DBI::Gofer::Request - Encapsulate a request from DBD::Gofer to
DBI::Gofer::Execute

* DESCRIPTION
This is an internal class.

* AUTHOR
Tim Bunce, <http://www.tim.bunce.name>

* LICENCE AND COPYRIGHT
Copyright (c) 2007, Tim Bunce, Ireland. All rights reserved.

This module is free software; you can redistribute it and/or modify it
under the same terms as Perl itself. See perlartistic.
