#+TITLE: Manpages - DBM_Filter_encode.3perl
#+DESCRIPTION: Linux manpage for DBM_Filter_encode.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
DBM_Filter::encode - filter for DBM_Filter

* SYNOPSIS
use SDBM_File; # or DB_File, GDBM_File, NDBM_File, ODBM_File use
DBM_Filter ; $db = tie %hash, ... $db->Filter_Push(encode =>
iso-8859-16);

* DESCRIPTION
This DBM filter allows you to choose the character encoding will be
store in the DBM file. The usage is

$db->Filter_Push(encode => ENCODING);

where ENCODING must be a valid encoding name that the Encode module
recognises.

A fatal error will be thrown if:

1. The Encode module is not available.

2. The encoding requested is not supported by the Encode module.

* SEE ALSO
DBM_Filter, perldbmfilter, Encode

* AUTHOR
Paul Marquess pmqs@cpan.org
