#+TITLE: Manpages - ts_libversion.3
#+DESCRIPTION: Linux manpage for ts_libversion.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ts_libversion - get version information on the currently running tslib

* SYNOPSIS
#+begin_example
  #include <tslib.h>

  struct ts_lib_version_data *ts_libversion();
#+end_example

* DESCRIPTION
*ts_libversion*() This function returns a pointer to a static copy of
the version info struct:

#+begin_example
  struct ts_lib_version_data {
          const char      *package_version;
          int             version_num;
          unsigned int    features;
  };
#+end_example

*package_version* is just the string containing the tarball release
number, e.g. "1.10".

*version_num* is a 24 bit number created like this: <8 bits major
number> | <8 bits minor number> | <8 bits patch number> of the library
libts. Version 0.7.1 is therefore returned as 0x000701.

*features* can have one or more bits set. The currently defined bits
are:

#+begin_quote
  *TSLIB_VERSION_MT* tslib filters support multitouch ts_read_mt() and
  there is at least one raw access module supporting it (currently evdev
  input) *TSLIB_VERSION_OPEN_RESTRICTED* ts_open_restricted and
  ts_close_restricted function pointers are available to implement
  *TSLIB_VERSION_EVENTPATH* ts_get_eventpath() is available since tslib
  can auto-detect a device *TSLIB_VERSION_VERSION* simple
  tslib_version() and ts_print_ascii_logo() are available
#+end_quote

* RETURN VALUE
This function returns a pointer to a static copy of the version info
struct.

* SEE ALSO
*ts_setup*(3), *ts_read*(3), *ts.conf*(5)
