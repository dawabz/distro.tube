#+TITLE: Manpages - psignal.3
#+DESCRIPTION: Linux manpage for psignal.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
psignal, psiginfo - print signal description

* SYNOPSIS
#+begin_example
  #include <signal.h>

  void psignal(int sig, const char *s);
  void psiginfo(const siginfo_t *pinfo, const char *s);
#+end_example

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*psignal*(): Since glibc 2.19: _DEFAULT_SOURCE Glibc 2.19 and earlier:
_BSD_SOURCE || _SVID_SOURCE

*psiginfo*():

#+begin_example
      _POSIX_C_SOURCE >= 200809L
#+end_example

* DESCRIPTION
The *psignal*() function displays a message on /stderr/ consisting of
the string /s/, a colon, a space, a string describing the signal number
/sig/, and a trailing newline. If the string /s/ is NULL or empty, the
colon and space are omitted. If /sig/ is invalid, the message displayed
will indicate an unknown signal.

The *psiginfo*() function is like *psignal*(), except that it displays
information about the signal described by /pinfo/, which should point to
a valid /siginfo_t/ structure. As well as the signal description,
*psiginfo*() displays information about the origin of the signal, and
other information relevant to the signal (e.g., the relevant memory
address for hardware-generated signals, the child process ID for
*SIGCHLD*, and the user ID and process ID of the sender, for signals set
using *kill*(2) or *sigqueue*(3)).

* RETURN VALUE
The *psignal*() and *psiginfo*() functions return no value.

* VERSIONS
The *psiginfo*() function was added to glibc in version 2.10.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                 | Attribute     | Value          |
| *psignal*(), *psiginfo*() | Thread safety | MT-Safe locale |

* CONFORMING TO
POSIX.1-2008, 4.3BSD.

* BUGS
In glibc versions up to 2.12, *psiginfo*() had the following bugs:

- In some circumstances, a trailing newline is not printed.

- Additional details are not displayed for real-time signals.

* SEE ALSO
*sigaction*(2), *perror*(3), *strsignal*(3), *signal*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
