#+TITLE: Manpages - pcap_offline_filter.3pcap
#+DESCRIPTION: Linux manpage for pcap_offline_filter.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_offline_filter - check whether a filter matches a packet

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  int pcap_offline_filter(const struct bpf_program *fp,
  const struct pcap_pkthdr *h, const u_char *pkt)
#+end_example

* DESCRIPTION
*pcap_offline_filter*() checks whether a filter matches a packet. /fp/
is a pointer to a /bpf_program/ struct, usually the result of a call to
*pcap_compile*(3PCAP). /h/ points to the /pcap_pkthdr/ structure for the
packet, and /pkt/ points to the data in the packet.

* RETURN VALUE
*pcap_offline_filter*() returns the return value of the filter program.
This will be zero if the packet doesn't match the filter and non-zero if
the packet matches the filter.

* SEE ALSO
*pcap*(3PCAP)
