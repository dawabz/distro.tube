#+TITLE: Manpages - hwloc_type_sscanf.3
#+DESCRIPTION: Linux manpage for hwloc_type_sscanf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about hwloc_type_sscanf.3 is found in manpage for: [[../man3/hwlocality_object_strings.3][man3/hwlocality_object_strings.3]]