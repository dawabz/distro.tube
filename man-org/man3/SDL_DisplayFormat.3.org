#+TITLE: Manpages - SDL_DisplayFormat.3
#+DESCRIPTION: Linux manpage for SDL_DisplayFormat.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_DisplayFormat - Convert a surface to the display format

* SYNOPSIS
*#include "SDL.h"*

*SDL_Surface *SDL_DisplayFormat*(*SDL_Surface *surface*);

* DESCRIPTION
This function takes a surface and copies it to a new surface of the
pixel format and colors of the video framebuffer, suitable for fast
blitting onto the display surface. It calls /SDL_ConvertSurface/

If you want to take advantage of hardware colorkey or alpha blit
acceleration, you should set the colorkey and alpha value before calling
this function.

If you want an alpha channel, see /SDL_DisplayFormatAlpha/.

* RETURN VALUE
If the conversion fails or runs out of memory, it returns *NULL*

* SEE ALSO
*SDL_ConvertSurface*, *SDL_DisplayFormatAlpha* *SDL_SetAlpha*,
*SDL_SetColorKey*, *SDL_Surface*
