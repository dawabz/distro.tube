#+TITLE: Manpages - Alien_Build_Plugin_PkgConfig_LibPkgConf.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Plugin_PkgConfig_LibPkgConf.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Plugin::PkgConfig::LibPkgConf - Probe system and determine
library or tool properties using PkgConfig::LibPkgConf

* VERSION
version 2.44

* SYNOPSIS
use alienfile; plugin PkgConfig::LibPkgConf => ( pkg_name => libfoo, );

* DESCRIPTION
Note: in most case you will want to use
Alien::Build::Plugin::PkgConfig::Negotiate instead. It picks the
appropriate fetch plugin based on your platform and environment. In some
cases you may need to use this plugin directly instead.

This plugin provides Probe and Gather steps for pkg-config based
packages. It uses PkgConfig::LibPkgConf to accomplish this task.

This plugin is part of the Alien::Build core For Now, but may be removed
in a future date. While It Seemed Like A Good Idea at the time, it may
not be appropriate to keep it in core. If it is spun off it will get its
own distribution some time in the future.

* PROPERTIES
** pkg_name
The package name. If this is a list reference then .pc files with all
those package names must be present. The first name will be the primary
and used by default once installed. For the subsequent =.pc= files you
can use the Alien::Base alt method to retrieve the alternate
configurations once the Alien is installed.

** atleast_version
The minimum required version that is acceptable version as provided by
the system.

** exact_version
The exact required version that is acceptable version as provided by the
system.

** max_version
The max required version that is acceptable version as provided by the
system.

** minimum_version
Alias for =atleast_version= for backward compatibility.

* METHODS
** available
my $bool = Alien::Build::Plugin::PkgConfig::LibPkgConf->available;

Returns true if the necessary prereqs for this plugin are /already/
installed.

* SEE ALSO
Alien::Build::Plugin::PkgConfig::Negotiate, Alien::Build, alienfile,
Alien::Build::MM, Alien

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
