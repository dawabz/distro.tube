#+TITLE: Manpages - EVP_PKEY_new.3ssl
#+DESCRIPTION: Linux manpage for EVP_PKEY_new.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
EVP_PKEY_new, EVP_PKEY_up_ref, EVP_PKEY_free,
EVP_PKEY_new_raw_private_key, EVP_PKEY_new_raw_public_key,
EVP_PKEY_new_CMAC_key, EVP_PKEY_new_mac_key,
EVP_PKEY_get_raw_private_key, EVP_PKEY_get_raw_public_key -
public/private key allocation and raw key handling functions

* SYNOPSIS
#include <openssl/evp.h> EVP_PKEY *EVP_PKEY_new(void); int
EVP_PKEY_up_ref(EVP_PKEY *key); void EVP_PKEY_free(EVP_PKEY *key);
EVP_PKEY *EVP_PKEY_new_raw_private_key(int type, ENGINE *e, const
unsigned char *key, size_t keylen); EVP_PKEY
*EVP_PKEY_new_raw_public_key(int type, ENGINE *e, const unsigned char
*key, size_t keylen); EVP_PKEY *EVP_PKEY_new_CMAC_key(ENGINE *e, const
unsigned char *priv, size_t len, const EVP_CIPHER *cipher); EVP_PKEY
*EVP_PKEY_new_mac_key(int type, ENGINE *e, const unsigned char *key, int
keylen); int EVP_PKEY_get_raw_private_key(const EVP_PKEY *pkey, unsigned
char *priv, size_t *len); int EVP_PKEY_get_raw_public_key(const EVP_PKEY
*pkey, unsigned char *pub, size_t *len);

* DESCRIPTION
The *EVP_PKEY_new()* function allocates an empty *EVP_PKEY* structure
which is used by OpenSSL to store public and private keys. The reference
count is set to *1*.

*EVP_PKEY_up_ref()* increments the reference count of *key*.

*EVP_PKEY_free()* decrements the reference count of *key* and, if the
reference count is zero, frees it up. If *key* is NULL, nothing is done.

*EVP_PKEY_new_raw_private_key()* allocates a new *EVP_PKEY*. If *e* is
non-NULL then the new *EVP_PKEY* structure is associated with the engine
*e*. The *type* argument indicates what kind of key this is. The value
should be a NID for a public key algorithm that supports raw private
keys, i.e. one of *EVP_PKEY_HMAC*, *EVP_PKEY_POLY1305*,
*EVP_PKEY_SIPHASH*, *EVP_PKEY_X25519*, *EVP_PKEY_ED25519*,
*EVP_PKEY_X448* or *EVP_PKEY_ED448*. *key* points to the raw private key
data for this *EVP_PKEY* which should be of length *keylen*. The length
should be appropriate for the type of the key. The public key data will
be automatically derived from the given private key data (if appropriate
for the algorithm type).

*EVP_PKEY_new_raw_public_key()* works in the same way as
*EVP_PKEY_new_raw_private_key()* except that *key* points to the raw
public key data. The *EVP_PKEY* structure will be initialised without
any private key information. Algorithm types that support raw public
keys are *EVP_PKEY_X25519*, *EVP_PKEY_ED25519*, *EVP_PKEY_X448* or
*EVP_PKEY_ED448*.

*EVP_PKEY_new_CMAC_key()* works in the same way as
*EVP_PKEY_new_raw_private_key()* except it is only for the
*EVP_PKEY_CMAC* algorithm type. In addition to the raw private key data,
it also takes a cipher algorithm to be used during creation of a CMAC in
the *cipher* argument. The cipher should be a standard encryption only
cipher. For example AEAD and XTS ciphers should not be used.

*EVP_PKEY_new_mac_key()* works in the same way as
*EVP_PKEY_new_raw_private_key()*. New applications should use
*EVP_PKEY_new_raw_private_key()* instead.

*EVP_PKEY_get_raw_private_key()* fills the buffer provided by *priv*
with raw private key data. The size of the *priv* buffer should be in
**len* on entry to the function, and on exit **len* is updated with the
number of bytes actually written. If the buffer *priv* is NULL then
**len* is populated with the number of bytes required to hold the key.
The calling application is responsible for ensuring that the buffer is
large enough to receive the private key data. This function only works
for algorithms that support raw private keys. Currently this is:
*EVP_PKEY_HMAC*, *EVP_PKEY_POLY1305*, *EVP_PKEY_SIPHASH*,
*EVP_PKEY_X25519*, *EVP_PKEY_ED25519*, *EVP_PKEY_X448* or
*EVP_PKEY_ED448*.

*EVP_PKEY_get_raw_public_key()* fills the buffer provided by *pub* with
raw public key data. The size of the *pub* buffer should be in **len* on
entry to the function, and on exit **len* is updated with the number of
bytes actually written. If the buffer *pub* is NULL then **len* is
populated with the number of bytes required to hold the key. The calling
application is responsible for ensuring that the buffer is large enough
to receive the public key data. This function only works for algorithms
that support raw public keys. Currently this is: *EVP_PKEY_X25519*,
*EVP_PKEY_ED25519*, *EVP_PKEY_X448* or *EVP_PKEY_ED448*.

* NOTES
The *EVP_PKEY* structure is used by various OpenSSL functions which
require a general private key without reference to any particular
algorithm.

The structure returned by *EVP_PKEY_new()* is empty. To add a private or
public key to this empty structure use the appropriate functions
described in *EVP_PKEY_set1_RSA* (3), EVP_PKEY_set1_DSA,
EVP_PKEY_set1_DH or EVP_PKEY_set1_EC_KEY.

* RETURN VALUES
*EVP_PKEY_new()*, *EVP_PKEY_new_raw_private_key()*,
*EVP_PKEY_new_raw_public_key()*, *EVP_PKEY_new_CMAC_key()* and
*EVP_PKEY_new_mac_key()* return either the newly allocated *EVP_PKEY*
structure or *NULL* if an error occurred.

*EVP_PKEY_up_ref()*, *EVP_PKEY_get_raw_private_key()* and
*EVP_PKEY_get_raw_public_key()* return 1 for success and 0 for failure.

* SEE ALSO
*EVP_PKEY_set1_RSA* (3), EVP_PKEY_set1_DSA, EVP_PKEY_set1_DH or
EVP_PKEY_set1_EC_KEY

* HISTORY
The *EVP_PKEY_new()* and *EVP_PKEY_free()* functions exist in all
versions of OpenSSL.

The *EVP_PKEY_up_ref()* function was added in OpenSSL 1.1.0.

The *EVP_PKEY_new_raw_private_key()*, *EVP_PKEY_new_raw_public_key()*,
*EVP_PKEY_new_CMAC_key()*, *EVP_PKEY_new_raw_private_key()* and
*EVP_PKEY_get_raw_public_key()* functions were added in OpenSSL 1.1.1.

* COPYRIGHT
Copyright 2002-2020 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
