#+TITLE: Manpages - gnutls_handshake_write.3
#+DESCRIPTION: Linux manpage for gnutls_handshake_write.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_handshake_write - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*int gnutls_handshake_write(gnutls_session_t */session/*,
gnutls_record_encryption_level_t */level/*, const void * */data/*,
size_t */data_size/*);*

* ARGUMENTS
- gnutls_session_t session :: is a *gnutls_session_t* type.

- gnutls_record_encryption_level_t level :: the current encryption level
  for reading a handshake message

- const void * data :: the (const) handshake data to be processed

- size_t data_size :: the size of data

* DESCRIPTION
This function processes a handshake message in the encryption level
specified with /level/ . Prior to calling this function, a handshake
read callback must be set on /session/ . Use
*gnutls_handshake_set_read_function()* to do this.

* SINCE
3.7.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
