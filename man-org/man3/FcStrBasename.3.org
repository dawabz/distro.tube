#+TITLE: Manpages - FcStrBasename.3
#+DESCRIPTION: Linux manpage for FcStrBasename.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcStrBasename - last component of filename

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcChar8 * FcStrBasename (const FcChar8 */file/*);*

* DESCRIPTION
Returns the filename of /file/ stripped of any leading directory names.
This is returned in newly allocated storage which should be freed when
no longer needed.
