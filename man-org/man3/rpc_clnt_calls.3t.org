#+TITLE: Manpages - rpc_clnt_calls.3t
#+DESCRIPTION: Linux manpage for rpc_clnt_calls.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
RPC library routines allow C language programs to make procedure calls
on other machines across the network. First, the client calls a
procedure to send a request to the server. Upon receipt of the request,
the server calls a dispatch routine to perform the requested service,
and then sends back a reply.

The

and

routines handle the client side of the procedure call. The remaining
routines deal with error handling in the case of errors.

Some of the routines take a

handle as one of the arguments. A

handle can be created by an RPC creation routine such as

(see

These routines are safe for use in multithreaded applications.

handles can be shared between threads, however in this implementation
requests by different threads are serialized (that is, the first request
will receive its results before the second request is sent).

See

for the definition of the

data structure.

A function macro that calls the remote procedure

associated with the client handle,

which is obtained with an RPC client creation routine such as

(see

The

argument is the XDR function used to encode the procedure's arguments,
and

is the XDR function used to decode the procedure's results;

is the address of the procedure's argument(s), and

is the address of where to place the result(s). The

argument is the time allowed for results to be returned, which is
overridden by a time-out set explicitly through

see

If the remote call succeeds, the status returned is

otherwise an appropriate status is returned.

A function macro that frees any data allocated by the RPC/XDR system
when it decoded the results of an RPC call. The

argument is the address of the results, and

is the XDR routine describing the results. This routine returns 1 if the
results were successfully freed, and 0 otherwise.

A function macro that copies the error structure out of the client
handle to the structure at address

Print a message to standard error corresponding to the condition
indicated by

A newline is appended. Normally used after a procedure call fails for a
routine for which a client handle is not needed, for instance

Print a message to the standard error indicating why an RPC call failed;

is the handle used to do the call. The message is prepended with string

and a colon. A newline is appended. Normally used after a remote
procedure call fails for a routine which requires a client handle, for
instance

Take the same arguments as

but instead of sending a message to the standard error indicating why an
RPC call failed, return a pointer to a string which contains the
message. The

function is normally used instead of

when the program does not have a standard error (as a program running as
a server quite likely does not), or if the programmer does not want the
message to be output with

(see

or if a message format different than that supported by

is to be used. Note: unlike

and

(see

does not return pointer to static data so the result will not get
overwritten on each call.

Like

except that (like

it returns a string instead of printing to standard error. However,

does not append a newline at the end of the message. Warning: returns
pointer to a buffer that is overwritten on each call.

Like

except the call message is broadcast to all the connectionless
transports specified by

If

is

it defaults to

Each time it receives a response, this routine calls

whose form is:

where

is the same as

passed to

except that the remote procedure's output is decoded there;

points to the address of the machine that sent the results, and

is the netconfig structure of the transport on which the remote server
responded. If

returns 0,

waits for more replies; otherwise it returns with appropriate status.
Warning: broadcast file descriptors are limited in size to the maximum
transfer size of that transport. For Ethernet, this value is 1500 bytes.
The

function uses

credentials by default (see

Like

except that the initial timeout,

and the maximum timeout,

are specified in milliseconds. The

argument is the initial time that

waits before resending the request. After the first resend, the
re-transmission interval increases exponentially until it exceeds

Call the remote procedure associated with

and

on the machine,

The

argument is used to encode the procedure's arguments, and

is used to decode the procedure's results;

is the address of the procedure's argument(s), and

is the address of where to place the result(s). The

argument can be any of the values listed on

This routine returns

if it succeeds, or an appropriate status is returned. Use the

routine to translate failure status into error messages. Warning:

uses the first available transport belonging to the class

on which it can create a connection. You do not have control of timeouts
or authentication using this routine.

These functions are part of libtirpc.
