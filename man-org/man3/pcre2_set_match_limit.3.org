#+TITLE: Manpages - pcre2_set_match_limit.3
#+DESCRIPTION: Linux manpage for pcre2_set_match_limit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  int pcre2_set_match_limit(pcre2_match_context *mcontext,
   uint32_t value);
#+end_example

* DESCRIPTION
This function sets the match limit field in a match context. The result
is always zero.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
