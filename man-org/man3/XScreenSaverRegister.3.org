#+TITLE: Manpages - XScreenSaverRegister.3
#+DESCRIPTION: Linux manpage for XScreenSaverRegister.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XScreenSaverRegister.3 is found in manpage for: [[../man3/Xss.3][man3/Xss.3]]