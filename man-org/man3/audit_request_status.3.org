#+TITLE: Manpages - audit_request_status.3
#+DESCRIPTION: Linux manpage for audit_request_status.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
audit_request_status - Request status of the audit system

* SYNOPSIS
*#include <libaudit.h>*

int audit_request_status(int fd);

* DESCRIPTION
audit_request_status requests that the kernel send status structure
describing various settings. The audit_status structure is as follows:

#+begin_quote
  #+begin_example

    struct audit_status {
       __u32   mask;           /* Bit mask for valid entries */
       __u32   enabled;        /* 1 = enabled, 0 = disabled */
       __u32   failure;        /* Failure-to-log action */
       __u32   pid;            /* pid of auditd process */
       __u32   rate_limit;     /* messages rate limit (per second) */
       __u32   backlog_limit;  /* waiting messages limit */
       __u32   lost;           /* messages lost */
       __u32   backlog;        /* messages waiting in queue */
    };
  #+end_example
#+end_quote

* RETURN VALUE
The return value is <= 0 on error, otherwise it is the netlink sequence
id number. This function can have any error that sendto would encounter.

* SEE ALSO
*audit_open*(3), *audit_get_reply*(3), *auditd*(8).

* AUTHOR
Steve Grubb
