#+TITLE: Manpages - TIFFtile.3tiff
#+DESCRIPTION: Linux manpage for TIFFtile.3tiff
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
TIFFTileSize, TIFFTileRowSize, TIFFVTileSize, TIFFDefaultTileSize,
TIFFComputeTile, TIFFCheckTile, TIFFNumberOfTiles - tile-related utility
routines

* SYNOPSIS
*#include <tiffio.h>*

*void TIFFDefaultTileSize(TIFF **/tif/*, uint32_t **/tw/*, uint32_t
**/th/*)*\\
*tsize_t TIFFTileSize(TIFF **/tif/*)*\\
*tsize_t TIFFTileRowSize(TIFF **/tif/*)*\\
*tsize_t TIFFVTileSize(TIFF **/tif/*, uint32_t */nrows/*)*\\
*ttile_t TIFFComputeTile(TIFF **/tif/*, uint32_t */x/*, uint32_t */y/*,
uint32_t */z/*, tsample_t */sample/*)*\\
*int TIFFCheckTile(TIFF **/tif/*, uint32_t */x/*, uint32_t */y/*,
uint32_t */z/*, tsample_t */sample/*)*\\
*ttile_t TIFFNumberOfTiles(TIFF **/tif/*)*\\

* DESCRIPTION
/TIFFDefaultTileSize/ returns the pixel width and height of a
reasonable-sized tile; suitable for setting up the /TileWidth/ and
/TileLength/ tags. If the /tw/ and /th/ values passed in are non-zero,
then they are adjusted to reflect any compression-specific requirements.
The returned width and height are constrained to be a multiple of 16
pixels to conform with the

specification.

/TIFFTileSize/ returns the equivalent size for a tile of data as it
would be returned in a call to /TIFFReadTile/ or as it would be expected
in a call to /TIFFWriteTile/.

/TIFFVTileSize/ returns the number of bytes in a row-aligned tile with
/nrows/ of data.

/TIFFTileRowSize/ returns the number of bytes of a row of data in a
tile.

/TIFFComputeTile/ returns the tile that contains the specified
coordinates. A valid tile is always returned; out-of-range coordinate
values are clamped to the bounds of the image. The /x/ and /y/
parameters are always used in calculating a tile. The /z/ parameter is
used if the image is deeper than 1 slice ( /ImageDepth/>1). The /sample/
parameter is used only if data are organized in separate planes (
/PlanarConfiguration/=2).

/TIFFCheckTile/ returns a non-zero value if the supplied coordinates are
within the bounds of the image and zero otherwise. The /x/ parameter is
checked against the value of the /ImageWidth/ tag. The /y/ parameter is
checked against the value of the /ImageLength/ tag. The /z/ parameter is
checked against the value of the /ImageDepth/ tag (if defined). The
/sample/ parameter is checked against the value of the /SamplesPerPixel/
parameter if the data are organized in separate planes.

/TIFFNumberOfTiles/ returns the number of tiles in the image.

* DIAGNOSTICS
None.

* SEE ALSO
*TIFFReadEncodedTile*(3TIFF), *TIFFReadRawTile*(3TIFF),
*TIFFReadTile*(3TIFF), *TIFFWriteEncodedTile*(3TIFF),
*TIFFWriteRawTile*(3TIFF), *TIFFWriteTile*(3TIFF), *libtiff*(3TIFF)

Libtiff library home page: *http://www.simplesystems.org/libtiff/*
