#+TITLE: Manpages - zip_stat_index.3
#+DESCRIPTION: Linux manpage for zip_stat_index.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function obtains information about the file named

in

The

argument specifies how the name lookup should be done. Its values are
described in

Also,

may be

to it to request information about the original file in the archive,
ignoring any changes made.

The

function obtains information about the file at position

The

argument is a pointer to a

(shown below), into which information about the file is placed.

struct zip_stat { zip_uint64_t valid; /* which fields have valid values
*/ const char *name; /* name of the file */ zip_uint64_t index; /* index
within archive */ zip_uint64_t size; /* size of file (uncompressed) */
zip_uint64_t comp_size; /* size of file (compressed) */ time_t mtime; /*
modification time */ zip_uint32_t crc; /* crc of file data */
zip_uint16_t comp_method; /* compression method used */ zip_uint16_t
encryption_method; /* encryption method used */ zip_uint32_t flags; /*
reserved for future use */ };

The structure pointed to by

must be allocated before calling

or

The

field of the structure specifies which other fields are valid. Check if
the flag defined by the following defines are in

before accessing the fields:

Upon successful completion 0 is returned. Otherwise, -1 is returned and
the error information in

is set to indicate the error.

The function

can fail for any of the errors specified for the routine

The function

fails and sets the error information to

if

is invalid. If

is not set and no information can be obtained from the source callback,
the error information is set to

was added in libzip 0.6. In libzip 0.11 the type of

was changed from

to

was added in libzip 0.6. In libzip 0.10 the type of

was changed from

to

In libzip 0.11 the type of

was changed from

to

and
