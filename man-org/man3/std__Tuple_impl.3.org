#+TITLE: Manpages - std__Tuple_impl.3
#+DESCRIPTION: Linux manpage for std__Tuple_impl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Tuple_impl< _Idx, _Elements >

* SYNOPSIS
\\

* Detailed Description
** "template<size_t _Idx, typename... _Elements>
\\
struct std::_Tuple_impl< _Idx, _Elements >"Contains the actual
implementation of the =tuple= template, stored as a recursive
inheritance hierarchy from the first element (most derived class) to the
last (least derived class). The =Idx= parameter gives the 0-based index
of the element stored at this point in the hierarchy; we use it to
implement a constant-time get() operation.

Definition at line *236* of file *std/tuple*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
