#+TITLE: Manpages - CURLOPT_HEADERDATA.3
#+DESCRIPTION: Linux manpage for CURLOPT_HEADERDATA.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_HEADERDATA - pointer to pass to header callback

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HEADERDATA, void
*pointer);

* DESCRIPTION
Pass a /pointer/ to be used to write the header part of the received
data to.

If /CURLOPT_WRITEFUNCTION(3)/ or /CURLOPT_HEADERFUNCTION(3)/ is used,
/pointer/ will be passed in to the respective callback.

If neither of those options are set, /pointer/ must be a valid FILE *
and it will be used by a plain fwrite() to write headers to.

* DEFAULT
NULL

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  struct my_info {
    int shoesize;
    char *secret;
  };

  static size_t header_callback(char *buffer, size_t size,
                                size_t nitems, void *userdata)
  {
    struct my_info *i = (struct my_info *)userdata;

    /* now this callback can access the my_info struct */

    return nitems * size;
  }

  CURL *curl = curl_easy_init();
  if(curl) {
    struct my_info my = { 10, "the cookies are in the cupboard" };
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, header_callback);

    /* pass in custom data to the callback */
    curl_easy_setopt(curl, CURLOPT_HEADERDATA, &my);

    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Always

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*CURLOPT_HEADERFUNCTION*(3), *CURLOPT_WRITEFUNCTION*(3),
