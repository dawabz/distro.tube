#+TITLE: Manpages - FcStrFree.3
#+DESCRIPTION: Linux manpage for FcStrFree.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcStrFree - free a string

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcStrFree (FcChar8 */s/*);*

* DESCRIPTION
This is just a wrapper around free(3) which helps track memory usage of
strings within the fontconfig library.
