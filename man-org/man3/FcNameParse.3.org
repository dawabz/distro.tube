#+TITLE: Manpages - FcNameParse.3
#+DESCRIPTION: Linux manpage for FcNameParse.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcNameParse - Parse a pattern string

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcPattern * FcNameParse (const FcChar8 */name/*);*

* DESCRIPTION
Converts /name/ from the standard text format described above into a
pattern.
