#+TITLE: Manpages - tar_extract_file.3
#+DESCRIPTION: Linux manpage for tar_extract_file.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tar_extract_file, tar_extract_regfile, tar_extract_hardlink,
tar_extract_symlink, tar_extract_chardev, tar_extract_blockdev,
tar_extract_dir, tar_extract_fifo, tar_skip_regfile,
tar_set_file_perms - extract files from a tar archive

* SYNOPSIS
*#include <libtar.h>*

*int tar_extract_file(TAR **/t/*, char **/realname/*);*

*int tar_extract_regfile(TAR **/t/*, char **/realname/*);*

*int tar_skip_regfile(TAR **/t/*);*

*int tar_extract_dir(TAR **/t/*, char **/realname/*);*

*int tar_extract_hardlink(TAR **/t/*, char **/realname/*);*

*int tar_extract_symlink(TAR **/t/*, char **/realname/*);*

*int tar_extract_blockdev(TAR **/t/*, char **/realname/*);*

*int tar_extract_chardev(TAR **/t/*, char **/realname/*);*

*int tar_extract_fifo(TAR **/t/*, char **/realname/*);*

*int tar_set_file_perms(TAR **/t/*, char **/realname/*);*

* VERSION
This man page documents version 1.2 of *libtar*.

* DESCRIPTION
The *tar_extract_file*() function acts as a front-end to the other
*tar_extract_**() functions. It checks the current tar header associated
with the /TAR/ handle /t/ (which must be initialized first by calling
*th_read*()) to determine what kind of file the header refers to. It
then calls the appropriate *tar_extract_**() function to extract that
kind of file.

The *tar_skip_regfile*() function skips over the file content blocks and
positions the file pointer at the expected location of the next tar
header block.

The *tar_set_file_perms*() function sets the attributes of the extracted
file to match the encoded values. This includes the file's modification
time, mode, owner, and group. This function is automatically called by
*tar_extract_file*(), but applications which call the other
*tar_extract_**() functions directly will need to call
*tar_set_file_perms*() manually if this behavior is desired.

* RETURN VALUES
On successful completion, the functions documented here will return 0.
On failure, they will return -1 and set /errno/ to an appropriate value.

The *tar_extract_dir*() function will return 1 if the directory already
exists.

* ERRORS
The *tar_extract_file*() function will fail if:

- EEXIST :: If the *O_NOOVERWRITE* flag is set and the file already
  exists.

The *tar_extract_**() functions will fail if:

- EINVAL :: An entry could not be added to the internal file hash.

- EINVAL :: Less than *T_BLOCKSIZE* bytes were read from the tar
  archive.

- EINVAL :: The current file header associated with /t/ refers to a kind
  of file other than the one which the called function knows about.

They may also fail if any of the following functions fail: *mkdir*(),
*write*(), *link*(), *symlink*(), *mknod*(), *mkfifo*(), *utime*(),
*chown*(), *lchown*(), *chmod*(), or *lstat*().

* SEE ALSO
*mkdir*(2), *write*(2), *link*(2), *symlink*(2), *mknod*(2),
*mkfifo*(2), *utime*(2), *chown*(2), *lchown*(2), *chmod*(2), *lstat*(2)
