#+TITLE: Manpages - rpc_svc_create.3t
#+DESCRIPTION: Linux manpage for rpc_svc_create.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
These routines are part of the RPC library which allows C language
programs to make procedure calls on servers across the network. These
routines deal with the creation of service handles. Once the handle is
created, the server can be invoked by calling

See

for the definition of the

data structure.

A function to change or retrieve various information about a service
object. The

argument indicates the type of operation and

is a pointer to the information. The supported values of

their argument types, and what they do are:

If a request is received for a program number served by this server but
the version number is outside the range registered with the server, an

error will normally be returned. The

argument should be a pointer to an integer. Upon successful completion
of the

request,

contains an integer which describes the server's current behavior: 0
indicates normal server behavior (that is, an

error will be returned); 1 indicates that the out of range request will
be silently ignored.

If a request is received for a program number served by this server but
the version number is outside the range registered with the server, an

error will normally be returned. It is sometimes desirable to change
this behavior. The

argument should be a pointer to an integer which is either 0 (indicating
normal server behavior - an

error will be returned), or 1 (indicating that the out of range request
should be silently ignored).

The

function creates server handles for all the transports belonging to the
class

The

argument defines a class of transports which can be used for a
particular application. The transports are tried in left to right order
in

variable or in top to bottom order in the netconfig database. If

is

it defaults to

The

function registers itself with the rpcbind service (see

The

function is called when there is a remote procedure call for the given

and

this requires calling

(see

in

If

succeeds, it returns the number of server handles it created, otherwise
it returns 0 and an error message is logged.

A function macro that destroys the RPC service handle

Destruction usually involves deallocation of private data structures,
including

itself. Use of

is undefined after calling this routine.

This routine creates a connectionless RPC service handle, and returns a
pointer to it. This routine returns

if it fails, and an error message is logged. The

and

arguments are arguments used to specify the size of the buffers. If they
are 0, suitable defaults are chosen. The file descriptor

should be open and bound. The server is not registered with

Warning: since connectionless-based RPC messages can only hold limited
amount of encoded data, this transport cannot be used for procedures
that take large arguments or return huge results.

This routine creates a service on top of an open and bound file
descriptor, and returns the handle to it. Typically, this descriptor is
a connected file descriptor for a connection-oriented transport. The

and

arguments indicate sizes for the send and receive buffers. If they are
0, reasonable defaults are chosen. This routine returns

if it fails, and an error message is logged.

This routine creates an RPC service handle and returns a pointer to it.
The transport is really a buffer within the process's address space, so
the corresponding RPC client should live in the same address space; (see

in

This routine allows simulation of RPC and acquisition of RPC overheads
(such as round trip times), without any kernel and networking
interference. This routine returns

if it fails, and an error message is logged.

Note:

should not be called when the raw interface is being used.

This routine creates an RPC server handle, and returns a pointer to it.
The

argument is the file descriptor on which the service is listening. If

is

it opens a file descriptor on the transport specified by

If the file descriptor is unbound and

is not

is bound to the address specified by

otherwise

is bound to a default address chosen by the transport.

Note: the

structure comes from the TLI/XTI SysV interface, which

does not use. The structure is defined in

for compatibility as:

struct t_bind { struct netbuf addr; /* network address, see rpc(3) */
unsigned int qlen; /* queue length (for listen(2)) */ };

In the case where the default address is chosen, the number of
outstanding connect requests is set to 8 for connection-oriented
transports. The user may specify the size of the send and receive
buffers with the arguments

and

values of 0 choose suitable defaults. This routine returns

if it fails, and an error message is logged. The server is not
registered with the

service.

The

function creates a server handle for the network specified by

and registers itself with the rpcbind service. The

function is called when there is a remote procedure call for the given

and

this requires calling

The

function returns the service handle if it succeeds, otherwise a

is returned and an error message is logged.

This routine creates a connection-oriented RPC service and returns a
pointer to it. This routine returns

if it fails, and an error message is logged. The users may specify the
size of the send and receive buffers with the arguments

and

values of 0 choose suitable defaults. The file descriptor

should be open and bound. The server is not registered with the

service.

These functions are part of libtirpc.
