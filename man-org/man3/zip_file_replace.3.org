#+TITLE: Manpages - zip_file_replace.3
#+DESCRIPTION: Linux manpage for zip_file_replace.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The function

adds a file to a zip archive, while

replaces an existing file in a zip archive. The argument

specifies the zip archive to which the file should be added.

is the file's name in the zip archive (for

while

specifies which file should be replaced (for

The

argument can be any combination of

with one of

Overwrite any existing file of the same name. For

only.

Guess encoding of

(default). (Only CP-437 and UTF-8 are recognized.)

Interpret

as UTF-8.

Interpret

as code page 437 (CP-437).

The data is obtained from the

argument, see

should not be called on a

after it was used successfully in a

or

call.

Upon successful completion,

returns the index of the new file in the archive, and

returns 0. Otherwise, -1 is returned and the error code in

is set to indicate the error.

zip_source_t *s; const char buf[]="teststring";

if ((s=zip_source_buffer(archive, buf, sizeof(buf), 0)) == NULL ||
zip_file_add(archive, name, s, ZIP_FL_ENC_UTF_8) < 0) {
zip_source_free(s); printf("error adding file: %s\n",
zip_strerror(archive)); }

and

fail if:

There is already a file called

in the archive. (Only applies to

and only if

is not provided).

or

are

or

is invalid.

Required memory could not be allocated.

Archive was opened in read-only mode.

and

were added in libzip 0.11.

and
