#+TITLE: Manpages - XUngrabServer.3
#+DESCRIPTION: Linux manpage for XUngrabServer.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XUngrabServer.3 is found in manpage for: [[../man3/XGrabServer.3][man3/XGrabServer.3]]