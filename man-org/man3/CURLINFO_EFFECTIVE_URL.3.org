#+TITLE: Manpages - CURLINFO_EFFECTIVE_URL.3
#+DESCRIPTION: Linux manpage for CURLINFO_EFFECTIVE_URL.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLINFO_EFFECTIVE_URL - get the last used URL

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_EFFECTIVE_URL, char
**urlp);

* DESCRIPTION
Pass in a pointer to a char pointer and get the last used effective URL.

In cases when you have asked libcurl to follow redirects, it may not be
the same value you set with /CURLOPT_URL(3)/.

The *urlp* pointer will be NULL or pointing to private memory you MUST
NOT free - it gets freed when you call /curl_easy_cleanup(3)/ on the
corresponding CURL handle.

* PROTOCOLS
HTTP(S)

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    CURLcode res;
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    res = curl_easy_perform(curl);
    if(res == CURLE_OK) {
      char *url = NULL;
      curl_easy_getinfo(curl, CURLINFO_EFFECTIVE_URL, &url);
      if(url)
        printf("Redirect to: %s\n", url);
    }
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.4

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*curl_easy_getinfo*(3), *curl_easy_setopt*(3),
