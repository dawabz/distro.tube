#+TITLE: Manpages - XtProcessLock.3
#+DESCRIPTION: Linux manpage for XtProcessLock.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtProcessLock, XtProcessUnlock - lock and unlock process

* SYNTAX
#include <X11/Intrinsic.h>

void XtProcessLock(void);

void XtProcessUnlock(void);

* DESCRIPTION
*XtProcessLock* is used to lock all process global data.

*XtProcessUnlock* unlocks the process.

* SEE ALSO
\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
