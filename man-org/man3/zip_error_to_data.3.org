#+TITLE: Manpages - zip_error_to_data.3
#+DESCRIPTION: Linux manpage for zip_error_to_data.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

function converts the zip_error

into data suitable as return value for

The data is written into the buffer

of size

If the buffer is not large enough to hold 2 ints, an error is returned.

returns 2*(sizeof int) on success, and -1 on error.

was added in libzip 1.0.

and
