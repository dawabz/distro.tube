#+TITLE: Manpages - y1f.3
#+DESCRIPTION: Linux manpage for y1f.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about y1f.3 is found in manpage for: [[../man3/y0.3][man3/y0.3]]