#+TITLE: Manpages - CURLOPT_TCP_NODELAY.3
#+DESCRIPTION: Linux manpage for CURLOPT_TCP_NODELAY.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_TCP_NODELAY - the TCP_NODELAY option

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TCP_NODELAY, long
nodelay);

* DESCRIPTION
Pass a long specifying whether the TCP_NODELAY option is to be set or
cleared (1L = set, 0 = clear). The option is set by default. This will
have no effect after the connection has been established.

Setting this option to 1L will disable TCP's Nagle algorithm on this
connection. The purpose of this algorithm is to try to minimize the
number of small packets on the network (where "small packets" means TCP
segments less than the Maximum Segment Size (MSS) for the network).

Maximizing the amount of data sent per TCP segment is good because it
amortizes the overhead of the send. However, in some cases small
segments may need to be sent without delay. This is less efficient than
sending larger amounts of data at a time, and can contribute to
congestion on the network if overdone.

* DEFAULT
1

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
    /* leave Nagle enabled */
    curl_easy_setopt(curl, CURLOPT_TCP_NODELAY, 0);
    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Always. The default was changed to 1 from 0 in 7.50.2.

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*CURLOPT_SOCKOPTFUNCTION*(3), *CURLOPT_TCP_KEEPALIVE*(3),
