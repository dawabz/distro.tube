#+TITLE: Manpages - gnutls_x509_ext_import_key_purposes.3
#+DESCRIPTION: Linux manpage for gnutls_x509_ext_import_key_purposes.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_x509_ext_import_key_purposes - API function

* SYNOPSIS
*#include <gnutls/x509-ext.h>*

*int gnutls_x509_ext_import_key_purposes(const gnutls_datum_t * */ext/*,
gnutls_x509_key_purposes_t */p/*, unsigned int */flags/*);*

* ARGUMENTS
- const gnutls_datum_t * ext :: The DER-encoded extension data

- gnutls_x509_key_purposes_t p :: The key purposes

- unsigned int flags :: should be zero

* DESCRIPTION
This function will extract the key purposes in the provided DER-encoded
ExtKeyUsageSyntax PKIX extension, to a *gnutls_x509_key_purposes_t*
type. The data must be initialized.

* RETURNS
On success, *GNUTLS_E_SUCCESS* (0) is returned, otherwise a negative
error value.

* SINCE
3.3.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
