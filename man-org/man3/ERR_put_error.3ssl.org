#+TITLE: Manpages - ERR_put_error.3ssl
#+DESCRIPTION: Linux manpage for ERR_put_error.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ERR_put_error, ERR_add_error_data, ERR_add_error_vdata - record an error

* SYNOPSIS
#include <openssl/err.h> void ERR_put_error(int lib, int func, int
reason, const char *file, int line); void ERR_add_error_data(int num,
...); void ERR_add_error_vdata(int num, va_list arg);

* DESCRIPTION
*ERR_put_error()* adds an error code to the thread's error queue. It
signals that the error of reason code *reason* occurred in function
*func* of library *lib*, in line number *line* of *file*. This function
is usually called by a macro.

*ERR_add_error_data()* associates the concatenation of its *num* string
arguments with the error code added last. *ERR_add_error_vdata()* is
similar except the argument is a *va_list*.

*ERR_load_strings* (3) can be used to register error strings so that the
application can a generate human-readable error messages for the error
code.

** Reporting errors
Each sub-library has a specific macro *XXXerr()* that is used to report
errors. Its first argument is a function code *XXX_F_...*, the second
argument is a reason code *XXX_R_...*. Function codes are derived from
the function names; reason codes consist of textual error descriptions.
For example, the function *ssl3_read_bytes()* reports a handshake
failure as follows:

SSLerr(SSL_F_SSL3_READ_BYTES, SSL_R_SSL_HANDSHAKE_FAILURE);

Function and reason codes should consist of uppercase characters,
numbers and underscores only. The error file generation script
translates function codes into function names by looking in the header
files for an appropriate function name, if none is found it just uses
the capitalized form such as SSL3_READ_BYTES in the above example.

The trailing section of a reason code (after the _R_) is translated into
lowercase and underscores changed to spaces.

Although a library will normally report errors using its own specific
XXXerr macro, another library's macro can be used. This is normally only
done when a library wants to include ASN1 code which must use the
*ASN1err()* macro.

* RETURN VALUES
*ERR_put_error()* and *ERR_add_error_data()* return no values.

* SEE ALSO
*ERR_load_strings* (3)

* COPYRIGHT
Copyright 2000-2020 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
