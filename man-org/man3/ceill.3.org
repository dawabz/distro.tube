#+TITLE: Manpages - ceill.3
#+DESCRIPTION: Linux manpage for ceill.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ceill.3 is found in manpage for: [[../man3/ceil.3][man3/ceil.3]]