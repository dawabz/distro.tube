#+TITLE: Manpages - libnetpbm_ug.3
#+DESCRIPTION: Linux manpage for libnetpbm_ug.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
libnetpbm_ug - netpbm sample code

* DESCRIPTION
The Libnetpbm programming library is part of *Netpbm*(1)

* Example
Here is an example of a C program that uses *libnetpbm* to read a Netpbm
image input and produce a Netpbm image output.

#+begin_example
     /* Example program fragment to read a PAM or PNM image
        from stdin, add up the values of every sample in it
        (I don't know why), and write the image unchanged to
        stdout. */

     #include <netpbm/pam.h>

     struct pam inpam, outpam;
     tuple * tuplerow;
     unsigned int row;

     pm_init(argv[0], 0);

     pnm_readpaminit(stdin, &inpam, PAM_STRUCT_SIZE(tuple_type));

     outpam = inpam; outpam.file = stdout;

     pnm_writepaminit(&outpam);

     tuplerow = pnm_allocpamrow(&inpam);

     for (row = 0; row < inpam.height; row++) {
         unsigned int column;
         pnm_readpamrow(&inpam, tuplerow);
         for (column = 0; column < inpam.width; ++column) {
             unsigned int plane;
             for (plane = 0; plane < inpam.depth; ++plane) {
                 grand_total += tuplerow[column][plane];
             }
         }
         pnm_writepamrow(&outpam, tuplerow); }

     pnm_freepamrow(tuplerow);
#+end_example

* Guide To Using Libnetpbm
** *libnetpbm* classes
In this section, Guide To Using Libnetpbm, we cover only the PAM
functions in *libnetpbm*. As described in *the* introduction to
*libnetpbm* (3) , there are four other classes of image processing
functions (PBM, PGM, PPM, PNM). They are less important, since you can
do everything more easily with the PAM functions, but if you're working
on old programs or need the extra efficiency those older functions can
sometimes provide, you can find them documented as here:
*PBM*Function*Manual*(3) , *PGM*Function*Manual*(3) ,
*PPM*Function*Manual*(3) , and *PNM*Function*Manual*(3)

In case you're wondering, what makes the PAM functions easier to use is:

- Each function handles all the formats. It does so without converting
  to a common format, so your program can treat the different formats
  differently if it wants. However, the interface makes it easy for your
  program to ignore the differences between the formats if that's what
  you want.

- The PAM function parameter lists convey most information about the
  image with which you're working with a single *pam* structure, which
  you can build once and use over and over, whereas the older functions
  require you to pass up to 5 pieces of image information (height,
  width, etc.) as separate arguments to every function.

** Library Initialization
Every program that uses the library must initialize the library, i.e.
set up the process to use the library, as described in
[[file:libpm.html#initialization][Initialization]] . That is the purpose
of the call to *pm_init()* in the example above.

** THE pam STRUCTURE
The PAM functions take most of their arguments in the form of a single
*pam* structure. This is not an opaque object, but just a convenient way
to organize the information upon which most the functions depend. So you
are free to access or set the elements of the structure however you
want. But you will find in most cases it is most convenient to call
*pnm_readpaminit()* or *pnm_writepaminit()* to set the fields in the
*pam* structure before calling any other pam functions, and then just to
pass the structure unchanged in all future calls to pam functions.

The fields are:

- *size* :: The storage size in bytes of this entire structure.

- *len* :: The length, in bytes, of the information in this structure.
  The information starts in the first byte and is contiguous. This
  cannot be greater than *size*. *size* and *len* can be used to make
  programs compatible with newer and older versions of the Netpbm
  libraries.

- *file* :: The file.

- *format* :: The format code of the image. This is *PAM_FORMAT* unless
  the PAM image is really a view of a PBM, PGM, or PPM image. Then it's
  *PBM_FORMAT*, *RPBM_FORMAT*, etc.

There is an important quirk in the meaning of this member when you use
the pam structure to write an image: Only the type portion of it is
meaningful. A Netpbm format code conveys two pieces of information: The
format type (PBM, PGM, PPM, or PAM) and the plainness (plain PBM vs raw
PBM, etc.). But when writing, *libnetpbm* ignores the plainness part and
instead takes the plainness from the *plainformat* member. So
*PBM_FORMAT* and *RPBM_FORMAT* are identical when writing.

This quirk exists for historical purposes; it's necessary for
consistency with the older functions such as *pnm_writepnmrow()* whose
/format/ and /forceplain/ arguments are analogous.

Before Netpbm 10.32 (February 2006), *libnetpbm* did not ignore the
plainness. This caused many programs to behave poorly, producing plain
format output when they should, for backward compatibility at the very
least, produce raw format output.

A common way to use this member is to copy it and the *plainformat*
member from a pam for an input image to a pam for an output image. When
you do that, your output image will be raw format regardless of whether
your input image was plain or raw, and this is the conventional behavior
of Netpbm programs.

- *plainformat* :: This is a boolean value (0 = false, 1 = true),
  meaningful only when writing an image file. It means to write in the
  plain (text) version of the format indicated by *format* as opposed to
  the raw (binary) version. Note that the format code in *format* would
  appear to completely specify the format, making *plainformat*
  redundant. But see the description of *format* for why that isn't
  true.

Until Netpbm 10.32 (February 2006), this was defined a little
differently. The *format* member did in fact completely identify the
format and *plainformat* was redundant and existed as a separate member
only for computational speed. But this was inconsistent with the older
*libnetpbm* interface (e.g. *pnm_writepnm()*, and it made it difficult
to write backward compatible programs. Before Netpbm 10.32, it affected
reading as well as writing.

*libnetpbm* image reading functions set this field to false, for your
convenience in building an output image pam from an input image pam.

- *height* :: The height of the image in rows.

- *width* :: The width of the image in number of columns (tuples per
  row).

- *depth* :: The depth of the image (degree of or number of samples in
  each tuple).

- *maxval* :: The maxval of the image. See definitions in *pam*(5)

- *bytes_per_sample* :: The number of bytes used to represent each
  sample in the image file. See the format definition in *pam*(5)

is entirely redundant with *maxval*. It exists as a separate member for
computational speed.

- *tuple_type* :: The tuple type of the image. See definitions in
  *pam*(5)

except the following, which are used for a PAM image which is really a
view of a PBM, PGM, or PPM image: *PAM_PBM_TUPLETYPE*,
*PAM_PGM_TUPLETYPE*, *PAM_PPM_TUPLETYPE*.

- *allocation_depth* :: The number of samples for which memory is
  allocated for any tuple associated with this PAM structure. This must
  be at least as great as 'depth'. Only the first 'depth' of the samples
  of a tuple are meaningful.

The purpose of this is to make it possible for a program to change the
type of a tuple to one with more or fewer planes.

0 means the allocation depth is the same as the image depth.

- *comment_p* :: Pointer to a pointer to a NUL-terminated ASCII string
  of comments. When reading an image, this contains the comments from
  the image's PAM header; when writing, the image gets these as
  comments, right after the magic number line. The individual comments
  are delimited by newlines and are in the same order as in the PAM
  header. The '#' at the beginning of a PAM header line that indicates
  the line is a comment is not part of the comment.

On output, NULL means no comments.

On input, libnetpbm mallocs storage for the comments and placed the
pointer at *comment_p. Caller must free it. NULL means libnetpbm does
not return comments and does not allocate any storage.

Examples:

#+begin_example
      const char * comments;
      ...
      pam.comment_p = &comments;
      pnm_readpaminit(fileP, &pam, PAM_STRUCT_SIZE(comment_p));
      printf('The comments are:\n');
      printf('%s', comments)
      free(comments);
#+end_example

#+begin_example
      const char * comments;
      ...
      comments = strdup('This is a comment 1\nThis is comment 2\n');
      pam.comment_p = &comments;
      pnm_writepaminit(&pam);
      free(comments);
#+end_example

This works only for PAM images. If you read a PNM image, you always get
back a null string. If you write a PNM image, you always get an image
that contains no comments.

This member does not exist before Netpbm 10.35 (August 2006). Before
that, there is no way with libnetpbm to get or set comments. The macro
*PAM_HAVE_COMMENT_P* is defined in *pam.h* where the member exists.

** PLAIN VERSUS RAW FORMAT
The PNM formats each come in two varieties: the older plain (text)
format and the newer raw (binary) format. There are different format
codes for the plain and raw formats, but which of the two formats the
pnm and pam functions write is independent of the format code you pass
to them.

The pam functions always write raw formats. If you specify the format
code for a plain format, a pam function assumes instead the raw version
of that format.

The pnm functions choose between plain and raw based on the /forceplain/
parameter that every write-type pnm function has. If this boolean value
is true, the function writes the plain version of the format specified
by the format code. If it is false, the function writes the raw version
of the format specified by the format code.

We are trying to stamp out the older plain formats, so it would be a
wise choice not to write a program that sets /forceplain/ true under any
circumstance. A user who needs a plain format can use the
*pnmtoplainpnm* program to convert the output of your program to plain
format.

** Reference
The *Libnetpbm*Netpbm*Image* Processing Manual (3) describes the the
*libnetpbm* functions for processing image data.

The *Libnetpbm*Utility*Manual*(3)

describes the functions that are not specifically related to the Netpbm
image formats.
