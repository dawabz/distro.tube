#+TITLE: Manpages - std_wbuffer_convert.3
#+DESCRIPTION: Linux manpage for std_wbuffer_convert.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::wbuffer_convert< _Codecvt, _Elem, _Tr > - Buffer conversions.

* SYNOPSIS
\\

=#include <locale_conv.h>=

Inherits *std::basic_streambuf< wchar_t, char_traits< wchar_t > >*.

** Public Types
typedef _Codecvt::state_type *state_type*\\

\\

typedef wchar_t *char_type*\\

typedef *char_traits*< wchar_t > *traits_type*\\

typedef traits_type::int_type *int_type*\\

typedef *traits_type::pos_type* *pos_type*\\

typedef traits_type::off_type *off_type*\\

\\

typedef *basic_streambuf*< *char_type*, *traits_type* >
*__streambuf_type*\\
This is a non-standard type.

** Public Member Functions
*wbuffer_convert* ()\\
Default constructor.

*wbuffer_convert* (const *wbuffer_convert* &)=delete\\

*wbuffer_convert* (*streambuf* *__bytebuf, _Codecvt *__pcvt=new
_Codecvt, state_type __state=state_type())\\

*locale* *getloc* () const\\
Locale access.

*streamsize* *in_avail* ()\\
Looking ahead into the stream.

*wbuffer_convert* & *operator=* (const *wbuffer_convert* &)=delete\\

*locale* *pubimbue* (const *locale* &__loc)\\
Entry point for imbue().

*streambuf* * *rdbuf* () const noexcept\\

*streambuf* * *rdbuf* (*streambuf* *__bytebuf) noexcept\\

*int_type* *sbumpc* ()\\
Getting the next character.

*int_type* *sgetc* ()\\
Getting the next character.

*streamsize* *sgetn* (*char_type* *__s, *streamsize* __n)\\
Entry point for xsgetn.

*int_type* *snextc* ()\\
Getting the next character.

*int_type* *sputbackc* (*char_type* __c)\\
Pushing characters back into the input stream.

*int_type* *sputc* (*char_type* __c)\\
Entry point for all single-character output functions.

*streamsize* *sputn* (const *char_type* *__s, *streamsize* __n)\\
Entry point for all single-character output functions.

state_type *state* () const noexcept\\
The conversion state following the last conversion.

*int_type* *sungetc* ()\\
Moving backwards in the input stream.

\\

*basic_streambuf* * *pubsetbuf* (*char_type* *__s, *streamsize* __n)\\
Entry points for derived buffer functions.

*pos_type* *pubseekoff* (*off_type* __off, *ios_base::seekdir* __way,
*ios_base::openmode* __mode=*ios_base::in*|*ios_base::out*)\\
Alters the stream position.

*pos_type* *pubseekpos* (*pos_type* __sp, *ios_base::openmode*
__mode=*ios_base::in*|*ios_base::out*)\\
Alters the stream position.

int *pubsync* ()\\
Calls virtual sync function.

** Protected Member Functions
void *__safe_gbump* (*streamsize* __n)\\

void *__safe_pbump* (*streamsize* __n)\\

void *gbump* (int __n)\\
Moving the read position.

virtual void *imbue* (const *locale* &__loc)\\
Changes translations.

*_Wide_streambuf::int_type* *overflow* (typename
*_Wide_streambuf::int_type* __out)\\
Consumes data from the buffer; writes to the controlled sequence.

virtual *int_type* *pbackfail* (*int_type* __c=traits_type::eof())\\
Tries to back up the input sequence.

void *pbump* (int __n)\\
Moving the write position.

virtual *pos_type* *seekoff* (*off_type*, *ios_base::seekdir*,
*ios_base::openmode*=*ios_base::in*|*ios_base::out*)\\
Alters the stream positions.

virtual *pos_type* *seekpos* (*pos_type*,
*ios_base::openmode*=*ios_base::in*|*ios_base::out*)\\
Alters the stream positions.

virtual *basic_streambuf*< *char_type*, *char_traits*< wchar_t > > *
*setbuf* (*char_type* *, *streamsize*)\\
Manipulates the buffer.

void *setg* (*char_type* *__gbeg, *char_type* *__gnext, *char_type*
*__gend)\\
Setting the three read area pointers.

void *setp* (*char_type* *__pbeg, *char_type* *__pend)\\
Setting the three write area pointers.

virtual *streamsize* *showmanyc* ()\\
Investigating the data available.

void *swap* (*basic_streambuf* &__sb)\\

int *sync* ()\\
Synchronizes the buffer arrays with the controlled sequences.

virtual *int_type* *uflow* ()\\
Fetches more data from the controlled sequence.

*_Wide_streambuf::int_type* *underflow* ()\\
Fetches more data from the controlled sequence.

virtual *streamsize* *xsgetn* (*char_type* *__s, *streamsize* __n)\\
Multiple character extraction.

virtual *streamsize* *xsputn* (const *char_type* *__s, *streamsize*
__n)\\
Multiple character insertion.

*streamsize* *xsputn* (const typename *_Wide_streambuf::char_type* *__s,
*streamsize* __n)\\

\\

*char_type* * *eback* () const\\
Access to the get area.

*char_type* * *gptr* () const\\
Access to the get area.

*char_type* * *egptr* () const\\
Access to the get area.

\\

*char_type* * *pbase* () const\\
Access to the put area.

*char_type* * *pptr* () const\\
Access to the put area.

*char_type* * *epptr* () const\\
Access to the put area.

** Protected Attributes
*locale* *_M_buf_locale*\\
Current locale setting.

*char_type* * *_M_in_beg*\\
Start of get area.

*char_type* * *_M_in_cur*\\
Current read area.

*char_type* * *_M_in_end*\\
End of get area.

*char_type* * *_M_out_beg*\\
Start of put area.

*char_type* * *_M_out_cur*\\
Current put area.

*char_type* * *_M_out_end*\\
End of put area.

* Detailed Description
** "template<typename _Codecvt, typename _Elem = wchar_t, typename _Tr =
char_traits<_Elem>>
\\
class std::wbuffer_convert< _Codecvt, _Elem, _Tr >"Buffer conversions.

Definition at line *387* of file *locale_conv.h*.

* Member Typedef Documentation
** typedef *basic_streambuf*<*char_type*, *traits_type*>
*std::basic_streambuf*< wchar_t , *char_traits*< wchar_t >
>::*__streambuf_type*= [inherited]=
This is a non-standard type.

Definition at line *140* of file *streambuf*.

** typedef wchar_t *std::basic_streambuf*< wchar_t , *char_traits*<
wchar_t > >::*char_type*= [inherited]=
These are standard types. They permit a standardized way of referring to
names of (or names dependent on) the template parameters, which are
specific to the implementation.

Definition at line *131* of file *streambuf*.

** typedef traits_type::int_type *std::basic_streambuf*< wchar_t ,
*char_traits*< wchar_t > >::*int_type*= [inherited]=
These are standard types. They permit a standardized way of referring to
names of (or names dependent on) the template parameters, which are
specific to the implementation.

Definition at line *133* of file *streambuf*.

** typedef traits_type::off_type *std::basic_streambuf*< wchar_t ,
*char_traits*< wchar_t > >::*off_type*= [inherited]=
These are standard types. They permit a standardized way of referring to
names of (or names dependent on) the template parameters, which are
specific to the implementation.

Definition at line *135* of file *streambuf*.

** typedef *traits_type::pos_type* *std::basic_streambuf*< wchar_t ,
*char_traits*< wchar_t > >::*pos_type*= [inherited]=
These are standard types. They permit a standardized way of referring to
names of (or names dependent on) the template parameters, which are
specific to the implementation.

Definition at line *134* of file *streambuf*.

** template<typename _Codecvt , typename _Elem = wchar_t, typename _Tr =
char_traits<_Elem>> typedef _Codecvt::state_type *std::wbuffer_convert*<
_Codecvt, _Elem, _Tr >::state_type
Definition at line *392* of file *locale_conv.h*.

** typedef *char_traits*< wchar_t > *std::basic_streambuf*< wchar_t ,
*char_traits*< wchar_t > >::*traits_type*= [inherited]=
These are standard types. They permit a standardized way of referring to
names of (or names dependent on) the template parameters, which are
specific to the implementation.

Definition at line *132* of file *streambuf*.

* Constructor & Destructor Documentation
** template<typename _Codecvt , typename _Elem = wchar_t, typename _Tr =
char_traits<_Elem>> *std::wbuffer_convert*< _Codecvt, _Elem, _Tr
>::*wbuffer_convert* ()= [inline]=
Default constructor.

Definition at line *395* of file *locale_conv.h*.

** template<typename _Codecvt , typename _Elem = wchar_t, typename _Tr =
char_traits<_Elem>> *std::wbuffer_convert*< _Codecvt, _Elem, _Tr
>::*wbuffer_convert* (*streambuf* * __bytebuf, _Codecvt * __pcvt =
=new _Codecvt=, state_type __state = =state_type()=)= [inline]=,
= [explicit]=
Constructor.

*Parameters*

#+begin_quote
  /__bytebuf/ The underlying byte stream buffer.\\
  /__pcvt/ The facet to use for conversions.\\
  /__state/ Initial conversion state.
#+end_quote

Takes ownership of =__pcvt= and will delete it in the destructor.

Definition at line *406* of file *locale_conv.h*.

References *std::basic_streambuf< wchar_t, char_traits< wchar_t >
>::setg()*, and *std::basic_streambuf< wchar_t, char_traits< wchar_t >
>::setp()*.

* Member Function Documentation
** void *std::basic_streambuf*< wchar_t , *char_traits*< wchar_t >
>::__safe_gbump (*streamsize* __n)= [inline]=, = [protected]=,
= [inherited]=
Definition at line *800* of file *streambuf*.

** void *std::basic_streambuf*< wchar_t , *char_traits*< wchar_t >
>::__safe_pbump (*streamsize* __n)= [inline]=, = [protected]=,
= [inherited]=
Definition at line *803* of file *streambuf*.

** *char_type* * *std::basic_streambuf*< wchar_t , *char_traits*<
wchar_t > >::eback () const= [inline]=, = [protected]=, = [inherited]=
Access to the get area. These functions are only available to other
protected functions, including derived classes.

- eback() returns the beginning pointer for the input sequence

- gptr() returns the next pointer for the input sequence

- egptr() returns the end pointer for the input sequence

Definition at line *487* of file *streambuf*.

** *char_type* * *std::basic_streambuf*< wchar_t , *char_traits*<
wchar_t > >::egptr () const= [inline]=, = [protected]=, = [inherited]=
Access to the get area. These functions are only available to other
protected functions, including derived classes.

- eback() returns the beginning pointer for the input sequence

- gptr() returns the next pointer for the input sequence

- egptr() returns the end pointer for the input sequence

Definition at line *493* of file *streambuf*.

** *char_type* * *std::basic_streambuf*< wchar_t , *char_traits*<
wchar_t > >::epptr () const= [inline]=, = [protected]=, = [inherited]=
Access to the put area. These functions are only available to other
protected functions, including derived classes.

- pbase() returns the beginning pointer for the output sequence

- pptr() returns the next pointer for the output sequence

- epptr() returns the end pointer for the output sequence

Definition at line *540* of file *streambuf*.

** void *std::basic_streambuf*< wchar_t , *char_traits*< wchar_t >
>::gbump (int __n)= [inline]=, = [protected]=, = [inherited]=
Moving the read position.

*Parameters*

#+begin_quote
  /__n/ The delta by which to move.
#+end_quote

This just advances the read position without returning any data.

Definition at line *503* of file *streambuf*.

** *locale* *std::basic_streambuf*< wchar_t , *char_traits*< wchar_t >
>::getloc () const= [inline]=, = [inherited]=
Locale access.

*Returns*

#+begin_quote
  The current locale in effect.
#+end_quote

If pubimbue(loc) has been called, then the most recent =loc= is
returned. Otherwise the global locale in effect at the time of
construction is returned.

Definition at line *231* of file *streambuf*.

** *char_type* * *std::basic_streambuf*< wchar_t , *char_traits*<
wchar_t > >::gptr () const= [inline]=, = [protected]=, = [inherited]=
Access to the get area. These functions are only available to other
protected functions, including derived classes.

- eback() returns the beginning pointer for the input sequence

- gptr() returns the next pointer for the input sequence

- egptr() returns the end pointer for the input sequence

Definition at line *490* of file *streambuf*.

** virtual void *std::basic_streambuf*< wchar_t , *char_traits*< wchar_t
> >::imbue (const *locale* & __loc)= [inline]=, = [protected]=,
= [virtual]=, = [inherited]=
Changes translations.

*Parameters*

#+begin_quote
  /__loc/ A new locale.
#+end_quote

Translations done during I/O which depend on the current locale are
changed by this call. The standard adds, /Between invocations of this
function a class derived from streambuf can safely cache results of
calls to locale functions and to members of facets so obtained./

*Note*

#+begin_quote
  Base class version does nothing.
#+end_quote

Definition at line *581* of file *streambuf*.

** *streamsize* *std::basic_streambuf*< wchar_t , *char_traits*< wchar_t
> >::in_avail ()= [inline]=, = [inherited]=
Looking ahead into the stream.

*Returns*

#+begin_quote
  The number of characters available.
#+end_quote

If a read position is available, returns the number of characters
available for reading before the buffer must be refilled. Otherwise
returns the derived =showmanyc()=.

Definition at line *289* of file *streambuf*.

** template<typename _Codecvt , typename _Elem = wchar_t, typename _Tr =
char_traits<_Elem>> *_Wide_streambuf::int_type* *std::wbuffer_convert*<
_Codecvt, _Elem, _Tr >::overflow (typename *_Wide_streambuf::int_type*
__c)= [inline]=, = [protected]=, = [virtual]=
Consumes data from the buffer; writes to the controlled sequence.

*Parameters*

#+begin_quote
  /__c/ An additional character to consume.
#+end_quote

*Returns*

#+begin_quote
  eof() to indicate failure, something else (usually /__c/, or
  not_eof())
#+end_quote

Informally, this function is called when the output buffer is full (or
does not exist, as buffering need not actually be done). If a buffer
exists, it is /consumed/, with /some effect/ on the controlled sequence.
(Typically, the buffer is written out to the sequence verbatim.) In
either case, the character /c/ is also written out, if /__c/ is not
=eof()=.

For a formal definition of this function, see a good text such as Langer
& Kreft, or [27.5.2.4.5]/3-7.

A functioning output streambuf can be created by overriding only this
function (no buffer area will be used).

*Note*

#+begin_quote
  Base class version does nothing, returns eof().
#+end_quote

Reimplemented from *std::basic_streambuf< wchar_t, char_traits< wchar_t
> >*.

Definition at line *450* of file *locale_conv.h*.

References *std::basic_streambuf< wchar_t, char_traits< wchar_t >
>::sputc()*.

** virtual *int_type* *std::basic_streambuf*< wchar_t , *char_traits*<
wchar_t > >::pbackfail (*int_type* __c =
=traits_type::eof()=)= [inline]=, = [protected]=, = [virtual]=,
= [inherited]=
Tries to back up the input sequence.

*Parameters*

#+begin_quote
  /__c/ The character to be inserted back into the sequence.
#+end_quote

*Returns*

#+begin_quote
  eof() on failure, /some other value/ on success
#+end_quote

*Postcondition*

#+begin_quote
  The constraints of =gptr()=, =eback()=, and =pptr()= are the same as
  for =underflow()=.
#+end_quote

*Note*

#+begin_quote
  Base class version does nothing, returns eof().
#+end_quote

Definition at line *729* of file *streambuf*.

** *char_type* * *std::basic_streambuf*< wchar_t , *char_traits*<
wchar_t > >::pbase () const= [inline]=, = [protected]=, = [inherited]=
Access to the put area. These functions are only available to other
protected functions, including derived classes.

- pbase() returns the beginning pointer for the output sequence

- pptr() returns the next pointer for the output sequence

- epptr() returns the end pointer for the output sequence

Definition at line *534* of file *streambuf*.

** void *std::basic_streambuf*< wchar_t , *char_traits*< wchar_t >
>::pbump (int __n)= [inline]=, = [protected]=, = [inherited]=
Moving the write position.

*Parameters*

#+begin_quote
  /__n/ The delta by which to move.
#+end_quote

This just advances the write position without returning any data.

Definition at line *550* of file *streambuf*.

** *char_type* * *std::basic_streambuf*< wchar_t , *char_traits*<
wchar_t > >::pptr () const= [inline]=, = [protected]=, = [inherited]=
Access to the put area. These functions are only available to other
protected functions, including derived classes.

- pbase() returns the beginning pointer for the output sequence

- pptr() returns the next pointer for the output sequence

- epptr() returns the end pointer for the output sequence

Definition at line *537* of file *streambuf*.

** *locale* *std::basic_streambuf*< wchar_t , *char_traits*< wchar_t >
>::pubimbue (const *locale* & __loc)= [inline]=, = [inherited]=
Entry point for imbue().

*Parameters*

#+begin_quote
  /__loc/ The new locale.
#+end_quote

*Returns*

#+begin_quote
  The previous locale.
#+end_quote

Calls the derived imbue(__loc).

Definition at line *214* of file *streambuf*.

** *pos_type* *std::basic_streambuf*< wchar_t , *char_traits*< wchar_t >
>::pubseekoff (*off_type* __off, ios_base::seekdir __way,
ios_base::openmode __mode = =ios_base::in | ios_base::out=)= [inline]=,
= [inherited]=
Alters the stream position.

*Parameters*

#+begin_quote
  /__off/ Offset.\\
  /__way/ Value for ios_base::seekdir.\\
  /__mode/ Value for ios_base::openmode.
#+end_quote

Calls virtual seekoff function.

Definition at line *256* of file *streambuf*.

** *pos_type* *std::basic_streambuf*< wchar_t , *char_traits*< wchar_t >
>::pubseekpos (*pos_type* __sp, ios_base::openmode __mode =
=ios_base::in | ios_base::out=)= [inline]=, = [inherited]=
Alters the stream position.

*Parameters*

#+begin_quote
  /__sp/ Position\\
  /__mode/ Value for ios_base::openmode.
#+end_quote

Calls virtual seekpos function.

Definition at line *268* of file *streambuf*.

** *basic_streambuf* * *std::basic_streambuf*< wchar_t , *char_traits*<
wchar_t > >::pubsetbuf (*char_type* * __s, *streamsize* __n)= [inline]=,
= [inherited]=
Entry points for derived buffer functions. The public versions of
=pubfoo= dispatch to the protected derived =foo= member functions,
passing the arguments (if any) and returning the result unchanged.

Definition at line *244* of file *streambuf*.

** int *std::basic_streambuf*< wchar_t , *char_traits*< wchar_t >
>::pubsync ()= [inline]=, = [inherited]=
Calls virtual sync function.

Definition at line *276* of file *streambuf*.

** template<typename _Codecvt , typename _Elem = wchar_t, typename _Tr =
char_traits<_Elem>> *streambuf* * *std::wbuffer_convert*< _Codecvt,
_Elem, _Tr >::rdbuf () const= [inline]=, = [noexcept]=
Definition at line *431* of file *locale_conv.h*.

** template<typename _Codecvt , typename _Elem = wchar_t, typename _Tr =
char_traits<_Elem>> *streambuf* * *std::wbuffer_convert*< _Codecvt,
_Elem, _Tr >::rdbuf (*streambuf* * __bytebuf)= [inline]=, = [noexcept]=
Definition at line *434* of file *locale_conv.h*.

** *int_type* *std::basic_streambuf*< wchar_t , *char_traits*< wchar_t >
>::sbumpc ()= [inline]=, = [inherited]=
Getting the next character.

*Returns*

#+begin_quote
  The next character, or eof.
#+end_quote

If the input read position is available, returns that character and
increments the read pointer, otherwise calls and returns =uflow()=.

Definition at line *321* of file *streambuf*.

** virtual *pos_type* *std::basic_streambuf*< wchar_t , *char_traits*<
wchar_t > >::seekoff (*off_type*, ios_base::seekdir, ios_base::openmode
= =ios_base::in | ios_base::out=)= [inline]=, = [protected]=,
= [virtual]=, = [inherited]=
Alters the stream positions. Each derived class provides its own
appropriate behavior.

*Note*

#+begin_quote
  Base class version does nothing, returns a =pos_type= that represents
  an invalid stream position.
#+end_quote

Definition at line *607* of file *streambuf*.

** virtual *pos_type* *std::basic_streambuf*< wchar_t , *char_traits*<
wchar_t > >::seekpos (*pos_type*, ios_base::openmode =
=ios_base::in | ios_base::out=)= [inline]=, = [protected]=,
= [virtual]=, = [inherited]=
Alters the stream positions. Each derived class provides its own
appropriate behavior.

*Note*

#+begin_quote
  Base class version does nothing, returns a =pos_type= that represents
  an invalid stream position.
#+end_quote

Definition at line *619* of file *streambuf*.

** virtual *basic_streambuf*< *char_type*, *char_traits*< wchar_t > > *
*std::basic_streambuf*< wchar_t , *char_traits*< wchar_t > >::setbuf
(*char_type* *, *streamsize*)= [inline]=, = [protected]=, = [virtual]=,
= [inherited]=
Manipulates the buffer. Each derived class provides its own appropriate
behavior. See the next-to-last paragraph of
https://gcc.gnu.org/onlinedocs/libstdc++/manual/streambufs.html#io.streambuf.buffering
for more on this function.

*Note*

#+begin_quote
  Base class version does nothing, returns =this=.
#+end_quote

Definition at line *596* of file *streambuf*.

** void *std::basic_streambuf*< wchar_t , *char_traits*< wchar_t >
>::setg (*char_type* * __gbeg, *char_type* * __gnext, *char_type* *
__gend)= [inline]=, = [protected]=, = [inherited]=
Setting the three read area pointers.

*Parameters*

#+begin_quote
  /__gbeg/ A pointer.\\
  /__gnext/ A pointer.\\
  /__gend/ A pointer.
#+end_quote

*Postcondition*

#+begin_quote
  /__gbeg/ == =eback()=, /__gnext/ == =gptr()=, and /__gend/ ==
  =egptr()=
#+end_quote

Definition at line *514* of file *streambuf*.

** void *std::basic_streambuf*< wchar_t , *char_traits*< wchar_t >
>::setp (*char_type* * __pbeg, *char_type* * __pend)= [inline]=,
= [protected]=, = [inherited]=
Setting the three write area pointers.

*Parameters*

#+begin_quote
  /__pbeg/ A pointer.\\
  /__pend/ A pointer.
#+end_quote

*Postcondition*

#+begin_quote
  /__pbeg/ == =pbase()=, /__pbeg/ == =pptr()=, and /__pend/ == =epptr()=
#+end_quote

Definition at line *560* of file *streambuf*.

** *int_type* *std::basic_streambuf*< wchar_t , *char_traits*< wchar_t >
>::sgetc ()= [inline]=, = [inherited]=
Getting the next character.

*Returns*

#+begin_quote
  The next character, or eof.
#+end_quote

If the input read position is available, returns that character,
otherwise calls and returns =underflow()=. Does not move the read
position after fetching the character.

Definition at line *343* of file *streambuf*.

** *streamsize* *std::basic_streambuf*< wchar_t , *char_traits*< wchar_t
> >::sgetn (*char_type* * __s, *streamsize* __n)= [inline]=,
= [inherited]=
Entry point for xsgetn.

*Parameters*

#+begin_quote
  /__s/ A buffer area.\\
  /__n/ A count.
#+end_quote

Returns xsgetn(__s,__n). The effect is to fill /__s/[0] through
/__s/[__n-1] with characters from the input sequence, if possible.

Definition at line *362* of file *streambuf*.

** virtual *streamsize* *std::basic_streambuf*< wchar_t , *char_traits*<
wchar_t > >::showmanyc ()= [inline]=, = [protected]=, = [virtual]=,
= [inherited]=
Investigating the data available.

*Returns*

#+begin_quote
  An estimate of the number of characters available in the input
  sequence, or -1.
#+end_quote

/If it returns a positive value, then successive calls to
/=underflow()=/ will not return /=traits::eof()=/ until at least that
number of characters have been supplied. If /=showmanyc()=/ returns -1,
then calls to /=underflow()=/ or /=uflow()=/ will
fail./= [27.5.2.4.3]/1=

*Note*

#+begin_quote
  Base class version does nothing, returns zero.

  The standard adds that /the intention is not only that the calls [to
  underflow or uflow] will not return /=eof()=/ but that they will
  return immediately./= =

  The standard adds that /the morphemes of /=showmanyc=/ are
  /*es-how-many-see*/, not /*show-manic*/./* *
#+end_quote

Definition at line *654 of file streambuf.*

** *int_type std::basic_streambuf< wchar_t , char_traits< wchar_t >
>::snextc ()*= [inline]=*, *= [inherited]=
Getting the next character.

*Returns*

#+begin_quote
  The next character, or eof.
#+end_quote

Calls =sbumpc()=*, and if that function returns *=traits::eof()=*, so
does this function. Otherwise, *=sgetc()=*. *

Definition at line *303 of file streambuf.*

** *int_type std::basic_streambuf< wchar_t , char_traits< wchar_t >
>::sputbackc (char_type __c)*= [inline]=*, *= [inherited]=
Pushing characters back into the input stream.

*Parameters*

#+begin_quote
  /__c/* The character to push back. *
#+end_quote

*Returns*

#+begin_quote
  The previous character, if possible.
#+end_quote

Similar to sungetc(), but /__c/* is pushed onto the stream instead of
*/the previous character./* If successful, the next character fetched
from the input stream will be */__c/*. *

Definition at line *377 of file streambuf.*

** *int_type std::basic_streambuf< wchar_t , char_traits< wchar_t >
>::sputc (char_type __c)*= [inline]=*, *= [inherited]=
Entry point for all single-character output functions.

*Parameters*

#+begin_quote
  /__c/* A character to output. *
#+end_quote

*Returns*

#+begin_quote
  /__c/*, if possible.*
#+end_quote

One of two public output functions.

If a write position is available for the output sequence (i.e., the
buffer is not full), stores /__c/* in that position, increments the
position, and returns *=traits::to_int_type(__c)=*. If a write position
is not available, returns *=overflow(__c)=*. *

Definition at line *429 of file streambuf.*

** *streamsize std::basic_streambuf< wchar_t , char_traits< wchar_t >
>::sputn (const char_type * __s, streamsize __n)*= [inline]=*,
*= [inherited]=
Entry point for all single-character output functions.

*Parameters*

#+begin_quote
  /__s/* A buffer read area. *\\
  /__n/* A count.*
#+end_quote

One of two public output functions.

Returns xsputn(__s,__n). The effect is to write /__s/*[0] through
*/__s/*[__n-1] to the output sequence, if possible. *

Definition at line *455 of file streambuf.*

** template<typename _Codecvt , typename _Elem = wchar_t, typename _Tr =
char_traits<_Elem>> state_type *std::wbuffer_convert< _Codecvt, _Elem,
_Tr >::state () const*= [inline]=*, *= [noexcept]=
The conversion state following the last conversion.

Definition at line *442 of file locale_conv.h.*

** *int_type std::basic_streambuf< wchar_t , char_traits< wchar_t >
>::sungetc ()*= [inline]=*, *= [inherited]=
Moving backwards in the input stream.

*Returns*

#+begin_quote
  The previous character, if possible.
#+end_quote

If a putback position is available, this function decrements the input
pointer and returns that character. Otherwise, calls and returns
pbackfail(). The effect is to /unget/* the last character */gotten/*. *

Definition at line *402 of file streambuf.*

** void *std::basic_streambuf< wchar_t , char_traits< wchar_t > >::swap
(basic_streambuf< wchar_t, char_traits< wchar_t > > &
__sb)*= [inline]=*, *= [protected]=*, *= [inherited]=
Definition at line *817 of file streambuf.*

** template<typename _Codecvt , typename _Elem = wchar_t, typename _Tr =
char_traits<_Elem>> int *std::wbuffer_convert< _Codecvt, _Elem, _Tr
>::sync ()*= [inline]=*, *= [protected]=*, *= [virtual]=
Synchronizes the buffer arrays with the controlled sequences.

*Returns*

#+begin_quote
  -1 on failure.
#+end_quote

Each derived class provides its own appropriate behavior, including the
definition of /failure/*. *

*Note*

#+begin_quote
  Base class version does nothing, returns zero.
#+end_quote

Reimplemented from *std::basic_streambuf< wchar_t, char_traits< wchar_t
> >.*

Definition at line *446 of file locale_conv.h.*

References *std::basic_streambuf< _CharT, _Traits >::pubsync().*

** virtual *int_type std::basic_streambuf< wchar_t , char_traits<
wchar_t > >::uflow ()*= [inline]=*, *= [protected]=*, *= [virtual]=*,
*= [inherited]=
Fetches more data from the controlled sequence.

*Returns*

#+begin_quote
  The first character from the /pending sequence/*.*
#+end_quote

Informally, this function does the same thing as =underflow()=*, and in
fact is required to call that function. It also returns the new
character, like *=underflow()=* does. However, this function also moves
the read position forward by one. *

Definition at line *705 of file streambuf.*

** template<typename _Codecvt , typename _Elem = wchar_t, typename _Tr =
char_traits<_Elem>> *_Wide_streambuf::int_type std::wbuffer_convert<
_Codecvt, _Elem, _Tr >::underflow ()*= [inline]=*, *= [protected]=*,
*= [virtual]=
Fetches more data from the controlled sequence.

*Returns*

#+begin_quote
  The first character from the /pending sequence/*.*
#+end_quote

Informally, this function is called when the input buffer is exhausted
(or does not exist, as buffering need not actually be done). If a buffer
exists, it is /refilled/*. In either case, the next available character
is returned, or *=traits::eof()=* to indicate a null pending sequence.*

For a formal definition of the pending sequence, see a good text such as
Langer & Kreft, or [27.5.2.4.3]/7-14.

A functioning input streambuf can be created by overriding only this
function (no buffer area will be used). For an example, see
https://gcc.gnu.org/onlinedocs/libstdc++/manual/streambufs.html

*Note*

#+begin_quote
  Base class version does nothing, returns eof().
#+end_quote

Reimplemented from *std::basic_streambuf< wchar_t, char_traits< wchar_t
> >.*

Definition at line *460 of file locale_conv.h.*

References *std::basic_streambuf< wchar_t, char_traits< wchar_t >
>::egptr(), and std::basic_streambuf< wchar_t, char_traits< wchar_t >
>::gptr().*

** *streamsize std::basic_streambuf< wchar_t , char_traits< wchar_t >
>::xsgetn (char_type * __s, streamsize __n)*= [protected]=*,
*= [virtual]=*, *= [inherited]=
Multiple character extraction.

*Parameters*

#+begin_quote
  /__s/* A buffer area. *\\
  /__n/* Maximum number of characters to assign. *
#+end_quote

*Returns*

#+begin_quote
  The number of characters assigned.
#+end_quote

Fills /__s/*[0] through */__s/*[__n-1] with characters from the input
sequence, as if by *=sbumpc()=*. Stops when either */__n/* characters
have been copied, or when *=traits::eof()=* would be copied.*

It is expected that derived classes provide a more efficient
implementation by overriding this definition.

Definition at line *670 of file streambuf.tcc.*

** *streamsize std::basic_streambuf< wchar_t , char_traits< wchar_t >
>::xsputn (const char_type * __s, streamsize __n)*= [protected]=*,
*= [virtual]=*, *= [inherited]=
Multiple character insertion.

*Parameters*

#+begin_quote
  /__s/* A buffer area. *\\
  /__n/* Maximum number of characters to write. *
#+end_quote

*Returns*

#+begin_quote
  The number of characters written.
#+end_quote

Writes /__s/*[0] through */__s/*[__n-1] to the output sequence, as if by
*=sputc()=*. Stops when either */n/* characters have been copied, or
when *=sputc()=* would return *=traits::eof()=*.*

It is expected that derived classes provide a more efficient
implementation by overriding this definition.

Definition at line *747 of file streambuf.tcc.*

** template<typename _Codecvt , typename _Elem = wchar_t, typename _Tr =
char_traits<_Elem>> *streamsize std::wbuffer_convert< _Codecvt, _Elem,
_Tr >::xsputn (const typename _Wide_streambuf::char_type * __s,
streamsize __n)*= [inline]=*, *= [protected]=
Definition at line *472 of file locale_conv.h.*

* Member Data Documentation
** *locale std::basic_streambuf< wchar_t , char_traits< wchar_t >
>::_M_buf_locale*= [protected]=*, *= [inherited]=
Current locale setting.

Definition at line *197 of file streambuf.*

** *char_type* std::basic_streambuf< wchar_t , char_traits< wchar_t >
>::_M_in_beg*= [protected]=*, *= [inherited]=
Start of get area.

Definition at line *189 of file streambuf.*

** *char_type* std::basic_streambuf< wchar_t , char_traits< wchar_t >
>::_M_in_cur*= [protected]=*, *= [inherited]=
Current read area.

Definition at line *190 of file streambuf.*

** *char_type* std::basic_streambuf< wchar_t , char_traits< wchar_t >
>::_M_in_end*= [protected]=*, *= [inherited]=
End of get area.

Definition at line *191 of file streambuf.*

** *char_type* std::basic_streambuf< wchar_t , char_traits< wchar_t >
>::_M_out_beg*= [protected]=*, *= [inherited]=
Start of put area.

Definition at line *192 of file streambuf.*

** *char_type* std::basic_streambuf< wchar_t , char_traits< wchar_t >
>::_M_out_cur*= [protected]=*, *= [inherited]=
Current put area.

Definition at line *193 of file streambuf.*

** *char_type* std::basic_streambuf< wchar_t , char_traits< wchar_t >
>::_M_out_end*= [protected]=*, *= [inherited]=
End of put area.

Definition at line *194 of file streambuf.*

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
