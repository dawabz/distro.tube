#+TITLE: Manpages - capng_get_caps_fd.3
#+DESCRIPTION: Linux manpage for capng_get_caps_fd.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
capng_get_caps_fd - Read file based capabilities

* SYNOPSIS
*#include <cap-ng.h>*

int capng_get_caps_fd(int fd);

* DESCRIPTION
This function will read the file based capabilities stored in extended
attributes of the file that the descriptor was opened against. The
bounding set is not included in file based capabilities operations. Note
that this function will only work if compiled on a kernel that supports
file based capabilities such as 2.6.26 and later. If the "magic" bit is
set, then all effect capability bits are set. Otherwise the bits are
cleared.

* RETURN VALUE
This returns 0 on success and -1 on failure.

* SEE ALSO
*capng_set_caps_fd*(3), *capabilities*(7)

* AUTHOR
Steve Grubb
