#+TITLE: Manpages - hwlocality_nvml.3
#+DESCRIPTION: Linux manpage for hwlocality_nvml.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwlocality_nvml - Interoperability with the NVIDIA Management Library

* SYNOPSIS
\\

** Functions
static int *hwloc_nvml_get_device_cpuset* (*hwloc_topology_t* topology,
nvmlDevice_t device, *hwloc_cpuset_t* set)\\

static *hwloc_obj_t* *hwloc_nvml_get_device_osdev_by_index*
(*hwloc_topology_t* topology, unsigned idx)\\

static *hwloc_obj_t* *hwloc_nvml_get_device_osdev* (*hwloc_topology_t*
topology, nvmlDevice_t device)\\

* Detailed Description
This interface offers ways to retrieve topology information about
devices managed by the NVIDIA Management Library (NVML).

* Function Documentation
** static int hwloc_nvml_get_device_cpuset (*hwloc_topology_t* topology,
nvmlDevice_t device, *hwloc_cpuset_t* set)= [inline]=, = [static]=
Get the CPU set of processors that are physically close to NVML device
=device=. Store in =set= the CPU-set describing the locality of the NVML
device =device=.

Topology =topology= and device =device= must match the local machine.
I/O devices detection and the NVML component are not needed in the
topology.

The function only returns the locality of the device. If more
information about the device is needed, OS objects should be used
instead, see *hwloc_nvml_get_device_osdev()* and
*hwloc_nvml_get_device_osdev_by_index()*.

This function is currently only implemented in a meaningful way for
Linux; other systems will simply get a full cpuset.

** static *hwloc_obj_t* hwloc_nvml_get_device_osdev (*hwloc_topology_t*
topology, nvmlDevice_t device)= [inline]=, = [static]=
Get the hwloc OS device object corresponding to NVML device =device=.

*Returns*

#+begin_quote
  The hwloc OS device object that describes the given NVML device
  =device=.

  =NULL= if none could be found.
#+end_quote

Topology =topology= and device =device= must match the local machine.
I/O devices detection and the NVML component must be enabled in the
topology. If not, the locality of the object may still be found using
*hwloc_nvml_get_device_cpuset()*.

*Note*

#+begin_quote
  The corresponding hwloc PCI device may be found by looking at the
  result parent pointer (unless PCI devices are filtered out).
#+end_quote

** static *hwloc_obj_t* hwloc_nvml_get_device_osdev_by_index
(*hwloc_topology_t* topology, unsigned idx)= [inline]=, = [static]=
Get the hwloc OS device object corresponding to the NVML device whose
index is =idx=.

*Returns*

#+begin_quote
  The hwloc OS device object describing the NVML device whose index is
  =idx=.

  =NULL= if none could be found.
#+end_quote

The topology =topology= does not necessarily have to match the current
machine. For instance the topology may be an XML import of a remote
host. I/O devices detection and the NVML component must be enabled in
the topology.

*Note*

#+begin_quote
  The corresponding PCI device object can be obtained by looking at the
  OS device parent object (unless PCI devices are filtered out).
#+end_quote

* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
