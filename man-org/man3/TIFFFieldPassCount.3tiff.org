#+TITLE: Manpages - TIFFFieldPassCount.3tiff
#+DESCRIPTION: Linux manpage for TIFFFieldPassCount.3tiff
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
TIFFFieldPassCount - Get whether to pass a count to TIFFGet/SetField

* SYNOPSIS
*#include <tiffio.h>*

*int TIFFFieldPassCount(const TIFFField* */fip/*)*

* DESCRIPTION
*TIFFFieldPassCount* returns true (nonzero) if *TIFFGetField* and
*TIFFSetField* expect a /count/ value to be passed before the actual
data pointer.

/fip/ is a field information pointer previously returned by
*TIFFFindField*, *TIFFFieldWithTag*, or *TIFFFieldWithName*.

When a /count/ is required, it will be of type *uint32_t* when
*TIFFFieldReadCount* reports *TIFF_VARIABLE2*, and of type *uint16_t*
otherwise. (This distinction is critical for use of *TIFFGetField*, but
normally not so for use of *TIFFSetField*.)\\

* RETURN VALUES
\\
*TIFFFieldPassCount* returns an integer that is always 1 (true) or 0
(false).\\

* SEE ALSO
*libtiff*(3TIFF),

Libtiff library home page: *http://www.simplesystems.org/libtiff/*
