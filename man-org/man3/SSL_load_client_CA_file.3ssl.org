#+TITLE: Manpages - SSL_load_client_CA_file.3ssl
#+DESCRIPTION: Linux manpage for SSL_load_client_CA_file.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_load_client_CA_file, SSL_add_file_cert_subjects_to_stack,
SSL_add_dir_cert_subjects_to_stack - load certificate names

* SYNOPSIS
#include <openssl/ssl.h> STACK_OF(X509_NAME)
*SSL_load_client_CA_file(const char *file); int
SSL_add_file_cert_subjects_to_stack(STACK_OF(X509_NAME) *stack, const
char *file) int SSL_add_dir_cert_subjects_to_stack(STACK_OF(X509_NAME)
*stack, const char *dir)

* DESCRIPTION
*SSL_load_client_CA_file()* reads certificates from /file/ and returns a
STACK_OF(X509_NAME) with the subject names found.

*SSL_add_file_cert_subjects_to_stack()* reads certificates from /file/,
and adds their subject name to the already existing /stack/.

*SSL_add_dir_cert_subjects_to_stack()* reads certificates from every
file in the directory /dir/, and adds their subject name to the already
existing /stack/.

* NOTES
*SSL_load_client_CA_file()* reads a file of PEM formatted certificates
and extracts the X509_NAMES of the certificates found. While the name
suggests the specific usage as support function for
*SSL_CTX_set_client_CA_list* (3), it is not limited to CA certificates.

* RETURN VALUES
The following return values can occur:

- NULL :: The operation failed, check out the error stack for the
  reason.

- Pointer to STACK_OF(X509_NAME) :: Pointer to the subject names of the
  successfully read certificates.

* EXAMPLES
Load names of CAs from file and use it as a client CA list:

SSL_CTX *ctx; STACK_OF(X509_NAME) *cert_names; ... cert_names =
SSL_load_client_CA_file("/path/to/CAfile.pem"); if (cert_names != NULL)
SSL_CTX_set_client_CA_list(ctx, cert_names); else /* error */ ...

* SEE ALSO
*ssl* (7), *SSL_CTX_set_client_CA_list* (3)

* COPYRIGHT
Copyright 2000-2019 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
