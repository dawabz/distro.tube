#+TITLE: Manpages - libraptor2.3
#+DESCRIPTION: Linux manpage for libraptor2.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libraptor2 - Raptor RDF syntax library 2.0

* SYNOPSIS
#+begin_example
  #include <raptor2.h>





  world=raptor_new_world();


  raptor_parser *p=raptor_new_parser(world,rdfxml);


  raptor_set_statement_handler(p,NULL,print_triples);


  raptor_uri *file_uri=raptor_new_uri(world,http://example.org/);


  raptor_parser_parse_file(p,file_uri,base_uri);


  raptor_parser_parse_uri(p,uri,NULL);


  raptor_free_parser(p);


  raptor_free_uri(file_uri);


  raptor_free_world(world);



  cc prog.c -o prog `pkg-config raptor2 --cflags` `pkg-config raptor2 --libs`
#+end_example

* DESCRIPTION
The /Raptor/ library provides a high-level interface to a set of parsers
and serializers that generate Resource Description Framework (RDF)
triples by parsing syntaxes or serialize the triples into syntaxes.

The supported parsing syntaxes include RDF/XML, N-Triples, Turtle, TRiG,
RSS tag soup (including all RSS and Atoms), GRDDL, RDF/JSON, RDFa and
the serializing syntaxes include RDF/XML (3 varieties), N-Quads,
N-Triples, Turtle, RSS 1.0, Atom 1.0, GraphViz DOT and RDF/JSON. The
RDF/XML parser can use either /expat/ or /libxml/ XML parsers for
providing the SAX event stream. The library functions are arranged in an
object-oriented style with constructors, destructors and method calls.
The statements and error messages are delivered via callback functions.

Raptor also contains classes to support the RDF graph triples: a
statement object containing term objects and support for RDF
URI-References for both parsing them and resolving / retrieval of URIs.

It some utility classes such as an I/O Stream abstraction for supporting
reading and writing to and from a variety of locations, AVL Trees,
String buffers and Sequences.

Raptor uses Unicode strings for RDF literals and URIs and preserves them
throughout the library. It uses the UTF-8 encoding of Unicode at the API
for passing in or returning Unicode strings. It is intended that the
preservation of Unicode for URIs supports Internationalized Resource
Identifiers (IRIs).

* API REFERENCE
See the HTML API docs that may be installed system wide at
/usr/share/gtk-doc/html/raptor2/ or on the web at
[[http://librdf.org/raptor/api/]]

* API CHANGES
See the Raptor API docs changes section at
[[http://librdf.org/raptor/api/raptor-changes.html][http://librdf.org/raptor/api/raptor-changes.html
]] and the upgrading information when converting from libraptor(1) code
at [[http://librdf.org/raptor/UPGRADING.html]]\\

* CONFORMING TO
/RDF/XML Syntax (Revised)/, Dave Beckett (ed.) W3C Recommendation,
[[http://www.w3.org/TR/rdf-syntax-grammar/]]

/N-Triples/, in /RDF Test Cases/, Jan Grant and Dave Beckett (eds.) W3C
Recommendation, [[http://www.w3.org/TR/rdf-testcases/#ntriples]]

/Turtle - Terse RDF Triple Language/, Dave Beckett,
[[http://www.dajobe.org/2004/01/turtle/]]

/RSS 0.91 spec revision 3/, Dan Libby, Netscape,
[[http://my.netscape.com/publish/formats/rss-spec-0.91.html]]

/RDF Site Summary (RSS) 1.0/, [[http://purl.org/rss/1.0/spec]]

/Atom 1.0 syndication format/, RFC 4287,
[[http://www.ietf.org/rfc/rfc4287.txt]]

/Gleaning Resource Descriptions from Dialects of Languages/ (GRDDL), Dan
Connolly (ed.), W3C Recommendation, 2007-09-11,
[[http://www.w3.org/TR/2007/REC-grddl-20070911/]]

/RDFa in XHTML: Syntax and Processing/, Ben Adida, Mark Birbeck, Shane
McCarron, Steven Pemberton (eds.) W3C Recommendation, 2008-10-14,
[[http://www.w3.org/TR/2008/REC-rdfa-syntax-20081014/]]

* SEE ALSO
*rapper(1)*

* AUTHOR
Dave Beckett - [[http://www.dajobe.org/]]
