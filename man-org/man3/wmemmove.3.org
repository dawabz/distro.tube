#+TITLE: Manpages - wmemmove.3
#+DESCRIPTION: Linux manpage for wmemmove.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
wmemmove - copy an array of wide-characters

* SYNOPSIS
#+begin_example
  #include <wchar.h>

  wchar_t *wmemmove(wchar_t *dest, const wchar_t *src, size_t n);
#+end_example

* DESCRIPTION
The *wmemmove*() function is the wide-character equivalent of the
*memmove*(3) function. It copies /n/ wide characters from the array
starting at /src/ to the array starting at /dest/. The arrays may
overlap.

The programmer must ensure that there is room for at least /n/ wide
characters at /dest/.

* RETURN VALUE
*wmemmove*() returns /dest/.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface    | Attribute     | Value   |
| *wmemmove*() | Thread safety | MT-Safe |

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008, C99.

* SEE ALSO
*memmove*(3), *wmemcpy*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
