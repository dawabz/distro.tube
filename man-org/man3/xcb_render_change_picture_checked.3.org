#+TITLE: Manpages - xcb_render_change_picture_checked.3
#+DESCRIPTION: Linux manpage for xcb_render_change_picture_checked.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_render_change_picture_checked.3 is found in manpage for: [[../man3/xcb_render_change_picture.3][man3/xcb_render_change_picture.3]]