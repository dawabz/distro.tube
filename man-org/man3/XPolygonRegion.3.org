#+TITLE: Manpages - XPolygonRegion.3
#+DESCRIPTION: Linux manpage for XPolygonRegion.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XPolygonRegion, XClipBox - generate regions

* SYNTAX
Region XPolygonRegion ( XPoint /points/[] , int /n/ , int /fill_rule/ );

int XClipBox ( Region /r/ , XRectangle */rect_return/ );

* ARGUMENTS
- fill_rule :: Specifies the fill-rule you want to set for the specified
  GC. You can pass *EvenOddRule* or *WindingRule*.

14. Specifies the number of points in the polygon.

- points :: Specifies an array of points.

18. Specifies the region.

- rect_return :: Returns the smallest enclosing rectangle.

* DESCRIPTION
The *XPolygonRegion* function returns a region for the polygon defined
by the points array. For an explanation of fill_rule, see *XCreateGC*.

The *XClipBox* function returns the smallest rectangle enclosing the
specified region.

* SEE ALSO
XCreateGC(3), XDrawPoint(3), XDrawRectangle(3)\\
/Xlib - C Language X Interface/
