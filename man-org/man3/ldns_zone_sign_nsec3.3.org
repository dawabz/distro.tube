#+TITLE: Manpages - ldns_zone_sign_nsec3.3
#+DESCRIPTION: Linux manpage for ldns_zone_sign_nsec3.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_zone_sign, ldns_zone_sign_nsec3 - dnssec sign a zone

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_zone* ldns_zone_sign(const ldns_zone *zone, ldns_key_list
*key_list);

ldns_zone* ldns_zone_sign_nsec3(ldns_zone *zone, ldns_key_list
*key_list, uint8_t algorithm, uint8_t flags, uint16_t iterations,
uint8_t salt_length, uint8_t *salt);

* DESCRIPTION
/ldns_zone_sign/() Signs the zone, and returns a newly allocated signed
zone .br *zone*: the zone to sign .br *key_list*: list of keys to sign
with .br Returns signed zone

/ldns_zone_sign_nsec3/() Signs the zone with NSEC3, and returns a newly
allocated signed zone .br *zone*: the zone to sign .br *key_list*: list
of keys to sign with .br *algorithm*: the NSEC3 hashing algorithm to use
.br *flags*: NSEC3 flags .br *iterations*: the number of NSEC3 hash
iterations to use .br *salt_length*: the length (in octets) of the NSEC3
salt .br *salt*: the NSEC3 salt data .br Returns signed zone

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
*perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*, *RFC4034* and
*RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
