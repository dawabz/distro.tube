#+TITLE: Manpages - MPI_File_read_all_begin.3
#+DESCRIPTION: Linux manpage for MPI_File_read_all_begin.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_File_read_all_begin* - Reads a file starting at the locations
specified by individual file pointers; beginning part of a split
collective routine (nonblocking).

* SYNTAX
#+begin_example
#+end_example

* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_File_read_all_begin(MPI_File fh, void *buf,
  	int count, MPI_Datatype datatype)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_FILE_READ_ALL_BEGIN(FH, BUF, COUNT, DATATYPE, IERROR)
  	<type>	BUF(*)
  	INTEGER	FH, COUNT, DATATYPE, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_File_read_all_begin(fh, buf, count, datatype, ierror)
  	TYPE(MPI_File), INTENT(IN) :: fh
  	TYPE(*), DIMENSION(..), ASYNCHRONOUS :: buf
  	INTEGER, INTENT(IN) :: count
  	TYPE(MPI_Datatype), INTENT(IN) :: datatype
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  void MPI::File::Read_all_begin(void* buf, int count,
  	const MPI::Datatype& datatype)
#+end_example

* INPUT/OUTPUT PARAMETER
- fh :: File handle (handle).

* INPUT PARAMETERS
- count :: Number of elements in buffer (integer).

- datatype :: Data type of each buffer element (handle).

* OUTPUT PARAMETERS
- buf :: Initial address of buffer (choice).

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
MPI_File_read_all_begin is the beginning part of a split collective
operation that attempts to read from the file associated with /fh/ (at
the current individual file pointer position maintained by the system) a
total number of /count/ data items having /datatype/ type into the
user's buffer /buf./ The data is taken out of those parts of the file
specified by the current view.

* NOTES
All the nonblocking collective routines for data access are "split" into
two routines, each with _begin or _end as a suffix. These split
collective routines are subject to the semantic rules described in
Section 9.4.5 of the MPI-2 standard.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. For MPI I/O function errors, the default error handler is set to
MPI_ERRORS_RETURN. The error handler may be changed with
MPI_File_set_errhandler; the predefined error handler
MPI_ERRORS_ARE_FATAL may be used to make I/O errors fatal. Note that MPI
does not guarantee that an MPI program can continue past an error.
