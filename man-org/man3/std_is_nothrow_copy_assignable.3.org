#+TITLE: Manpages - std_is_nothrow_copy_assignable.3
#+DESCRIPTION: Linux manpage for std_is_nothrow_copy_assignable.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::is_nothrow_copy_assignable< _Tp > - is_nothrow_copy_assignable

* SYNOPSIS
\\

Inherits std::__is_nt_copy_assignable_impl< _Tp, bool >.

* Detailed Description
** "template<typename _Tp>
\\
struct std::is_nothrow_copy_assignable< _Tp >"is_nothrow_copy_assignable

Definition at line *1112* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
