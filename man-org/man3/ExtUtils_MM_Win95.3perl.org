#+TITLE: Manpages - ExtUtils_MM_Win95.3perl
#+DESCRIPTION: Linux manpage for ExtUtils_MM_Win95.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ExtUtils::MM_Win95 - method to customize MakeMaker for Win9X

* SYNOPSIS
You should not be using this module directly.

* DESCRIPTION
This is a subclass of ExtUtils::MM_Win32 containing changes necessary to
get MakeMaker playing nice with command.com and other Win9Xisms.

** Overridden methods
Most of these make up for limitations in the Win9x/nmake command shell.

- max_exec_len :: Win98 chokes on things like Encode if we set the max
  length to nmake's max of 2K. So we go for a more conservative value of
  1K.

- os_flavor :: Win95 and Win98 and WinME are collectively Win9x and
  Win32

* AUTHOR
Code originally inside MM_Win32. Original author unknown.

Currently maintained by Michael G Schwern =schwern@pobox.com=.

Send patches and ideas to =makemaker@perl.org=.

See https://metacpan.org/release/ExtUtils-MakeMaker.
