#+TITLE: Manpages - ldns_dnssec_zone_add_rr.3
#+DESCRIPTION: Linux manpage for ldns_dnssec_zone_add_rr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_dnssec_zone_find_rrset, ldns_dnssec_zone_new,
ldns_dnssec_zone_free, ldns_dnssec_zone_add_rr,
ldns_dnssec_zone_names_print, ldns_dnssec_zone_print,
ldns_dnssec_zone_add_empty_nonterminals - functions for ldns_dnssec_zone

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_dnssec_rrsets* ldns_dnssec_zone_find_rrset(const ldns_dnssec_zone
*zone, const ldns_rdf *dname, ldns_rr_type type);

ldns_dnssec_zone* ldns_dnssec_zone_new(void);

void ldns_dnssec_zone_free(ldns_dnssec_zone *zone);

ldns_status ldns_dnssec_zone_add_rr(ldns_dnssec_zone *zone, ldns_rr
*rr);

void ldns_dnssec_zone_names_print(FILE *out, const ldns_rbtree_t *tree,
bool print_soa);

void ldns_dnssec_zone_print(FILE *out, const ldns_dnssec_zone *zone);

ldns_status ldns_dnssec_zone_add_empty_nonterminals(ldns_dnssec_zone
*zone);

* DESCRIPTION
/ldns_dnssec_zone_find_rrset/() Find the RRset with the given name and
type in the zone

.br *zone*: the zone structure to find the RRset in .br *dname*: the
domain name of the RRset to find .br *type*: the type of the RRset to
find .br Returns the RRset, or NULL if not present

/ldns_dnssec_zone_new/() Creates a new dnssec_zone structure .br Returns
the allocated structure

/ldns_dnssec_zone_free/() Frees the given zone structure, and its rbtree
of dnssec_names Individual ldns_rr RRs within those names are *not*
freed .br **zone*: the zone to free

/ldns_dnssec_zone_add_rr/() Adds the given RR to the zone. It find
whether there is a dnssec_name with that name present. If so, add it to
that, if not create a new one. Special handling of NSEC and RRSIG
provided

.br *zone*: the zone to add the RR to .br *rr*: The RR to add .br
Returns LDNS_STATUS_OK on success, an error code otherwise

/ldns_dnssec_zone_names_print/() Prints the rbtree of ldns_dnssec_name
structures to the file descriptor

.br *out*: the file descriptor to print the names to .br *tree*: the
tree of ldns_dnssec_name structures to print .br *print_soa*: if true,
print SOA records, if false, skip them

/ldns_dnssec_zone_print/() Prints the complete zone to the given file
descriptor

.br *out*: the file descriptor to print to .br *zone*: the dnssec_zone
to print

/ldns_dnssec_zone_add_empty_nonterminals/() Adds explicit dnssec_name
structures for the empty nonterminals in this zone. (this is needed for
NSEC3 generation)

.br *zone*: the zone to check for empty nonterminals return
LDNS_STATUS_OK on success.

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_dnssec_zone/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*,
*RFC4033*, *RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
