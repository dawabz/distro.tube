#+TITLE: Manpages - std__Bind.3
#+DESCRIPTION: Linux manpage for std__Bind.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Bind< _Signature > - Type of the function object returned from
bind().

* SYNOPSIS
\\

* Detailed Description
** "template<typename _Signature>
\\
class std::_Bind< _Signature >"Type of the function object returned from
bind().

Definition at line *402* of file *std/functional*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
