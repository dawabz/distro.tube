#+TITLE: Manpages - sd_journal_next_skip.3
#+DESCRIPTION: Linux manpage for sd_journal_next_skip.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sd_journal_next_skip.3 is found in manpage for: [[../sd_journal_next.3][sd_journal_next.3]]