#+TITLE: Manpages - std_tr2_bool_set.3
#+DESCRIPTION: Linux manpage for std_tr2_bool_set.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::tr2::bool_set

* SYNOPSIS
\\

** Public Member Functions
constexpr *bool_set* ()\\
Default constructor.

constexpr *bool_set* (bool __t)\\
Constructor from bool.

bool *contains* (*bool_set* __b) const\\

bool *equals* (*bool_set* __b) const\\
Return true if states are equal.

bool *is_emptyset* () const\\
Return true if this is empty.

bool *is_indeterminate* () const\\
Return true if this is indeterminate.

bool *is_singleton* () const\\
Return true if this is false or true (normal boolean).

*operator bool* () const\\
Conversion to bool.

** Static Public Member Functions
static *bool_set* *emptyset* ()\\

static *bool_set* *indeterminate* ()\\

** Friends
*bool_set* *operator!* (*bool_set* __b)\\

*bool_set* *operator&* (*bool_set* __s, *bool_set* __t)\\

template<typename CharT , typename Traits > *std::basic_ostream*< CharT,
Traits > & *operator<<* (*std::basic_ostream*< CharT, Traits > &__out,
*bool_set* __b)\\

*bool_set* *operator==* (*bool_set* __s, *bool_set* __t)\\

template<typename CharT , typename Traits > *std::basic_istream*< CharT,
Traits > & *operator>>* (*std::basic_istream*< CharT, Traits > &__in,
*bool_set* &__b)\\

*bool_set* *operator^* (*bool_set* __s, *bool_set* __t)\\

*bool_set* *operator|* (*bool_set* __s, *bool_set* __t)\\

* Detailed Description
bool_set

See N2136, Bool_set: multi-valued logic by Hervé Brönnimann, Guillaume
Melquiond, Sylvain Pion.

The implicit conversion to bool is slippery! I may use the new explicit
conversion. This has been specialized in the language so that in
contexts requiring a bool the conversion happens implicitly. Thus most
objections should be eliminated.

Definition at line *54* of file *bool_set*.

* Constructor & Destructor Documentation
** constexpr std::tr2::bool_set::bool_set ()= [inline]=, = [constexpr]=
Default constructor.

Definition at line *59* of file *bool_set*.

** constexpr std::tr2::bool_set::bool_set (bool __t)= [inline]=,
= [constexpr]=
Constructor from bool.

Definition at line *62* of file *bool_set*.

* Member Function Documentation
** bool std::tr2::bool_set::contains (*bool_set* __b) const= [inline]=
Definition at line *65* of file *bool_set*.

** static *bool_set* std::tr2::bool_set::emptyset ()= [inline]=,
= [static]=
Definition at line *102* of file *bool_set*.

** bool std::tr2::bool_set::equals (*bool_set* __b) const= [inline]=
Return true if states are equal.

Definition at line *69* of file *bool_set*.

** static *bool_set* std::tr2::bool_set::indeterminate ()= [inline]=,
= [static]=
Definition at line *94* of file *bool_set*.

** bool std::tr2::bool_set::is_emptyset () const= [inline]=
Return true if this is empty.

Definition at line *73* of file *bool_set*.

** bool std::tr2::bool_set::is_indeterminate () const= [inline]=
Return true if this is indeterminate.

Definition at line *77* of file *bool_set*.

** bool std::tr2::bool_set::is_singleton () const= [inline]=
Return true if this is false or true (normal boolean).

Definition at line *81* of file *bool_set*.

** std::tr2::bool_set::operator bool () const= [inline]=
Conversion to bool.

Definition at line *86* of file *bool_set*.

* Friends And Related Function Documentation
** *bool_set* operator! (*bool_set* __b)= [friend]=
Definition at line *110* of file *bool_set*.

** *bool_set* operator& (*bool_set* __s, *bool_set* __t)= [friend]=
Definition at line *122* of file *bool_set*.

** template<typename CharT , typename Traits > *std::basic_ostream*<
CharT, Traits > & operator<< (*std::basic_ostream*< CharT, Traits > &
__out, *bool_set* __b)= [friend]=
Definition at line *133* of file *bool_set*.

** *bool_set* operator== (*bool_set* __s, *bool_set* __t)= [friend]=
Definition at line *126* of file *bool_set*.

** template<typename CharT , typename Traits > *std::basic_istream*<
CharT, Traits > & operator>> (*std::basic_istream*< CharT, Traits > &
__in, *bool_set* & __b)= [friend]=
Definition at line *142* of file *bool_set*.

** *bool_set* operator^ (*bool_set* __s, *bool_set* __t)= [friend]=
Definition at line *114* of file *bool_set*.

** *bool_set* operator| (*bool_set* __s, *bool_set* __t)= [friend]=
Definition at line *118* of file *bool_set*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
