#+TITLE: Manpages - CURLOPT_HTTP_TRANSFER_DECODING.3
#+DESCRIPTION: Linux manpage for CURLOPT_HTTP_TRANSFER_DECODING.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_HTTP_TRANSFER_DECODING - HTTP transfer decoding control

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>

  CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HTTP_TRANSFER_DECODING,
                           long enabled);
#+end_example

* DESCRIPTION
Pass a long to tell libcurl how to act on transfer decoding. If set to
zero, transfer decoding will be disabled, if set to 1 it is enabled
(default). libcurl does chunked transfer decoding by default unless this
option is set to zero.

* DEFAULT
1

* PROTOCOLS
HTTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    CURLcode ret;
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    curl_easy_setopt(curl, CURLOPT_HTTP_TRANSFER_DECODING, 0L);
    ret = curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.16.2 Does not work with the hyper backend (it will always
have transfer decoding enabled).

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_HTTP_CONTENT_DECODING*(3), *CURLOPT_ACCEPT_ENCODING*(3),
