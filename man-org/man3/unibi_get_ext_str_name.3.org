#+TITLE: Manpages - unibi_get_ext_str_name.3
#+DESCRIPTION: Linux manpage for unibi_get_ext_str_name.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
unibi_get_ext_bool_name, unibi_set_ext_bool_name,
unibi_get_ext_num_name, unibi_set_ext_num_name, unibi_get_ext_str_name,
unibi_set_ext_str_name - access the names of extended capabilities of a
terminal object

* SYNOPSIS
#include <unibilium.h> const char *unibi_get_ext_bool_name(const
unibi_term *ut, size_t i); const char *unibi_get_ext_num_name(const
unibi_term *ut, size_t i); const char *unibi_get_ext_str_name(const
unibi_term *ut, size_t i); void unibi_set_ext_bool_name(unibi_term *ut,
size_t i, const char *s); void unibi_set_ext_num_name(unibi_term *ut,
size_t i, const char *s); void unibi_set_ext_str_name(unibi_term *ut,
size_t i, const char *s);

* DESCRIPTION
Get/set the names of extended boolean, numeric, and string capabilities.
/i/ is the index of the extended capability to act on; it must be less
than =unibi_count_ext_bool(ut)=, =unibi_count_ext_num(ut)=, or
=unibi_count_ext_str(ut)=, respectively.

Note that =unibi_set_ext_bool_name=, =unibi_set_ext_num_name=, and
=unibi_set_ext_str_name= simply store the pointer they are given; they
will not free /s/ or make a copy of the string.

* SEE ALSO
*unibilium.h* (3)
