#+TITLE: Manpages - sd_uid_get_sessions.3
#+DESCRIPTION: Linux manpage for sd_uid_get_sessions.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sd_uid_get_sessions.3 is found in manpage for: [[../sd_uid_get_state.3][sd_uid_get_state.3]]