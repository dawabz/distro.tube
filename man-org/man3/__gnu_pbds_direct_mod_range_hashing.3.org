#+TITLE: Manpages - __gnu_pbds_direct_mod_range_hashing.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_direct_mod_range_hashing.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::direct_mod_range_hashing< Size_Type > - A mod range-hashing
class (uses the modulo function).

* SYNOPSIS
\\

=#include <hash_policy.hpp>=

Inherits *__gnu_pbds::detail::mod_based_range_hashing< std::size_t >*.

** Public Types
typedef Size_Type *size_type*\\

** Public Member Functions
void *swap* (*direct_mod_range_hashing*< Size_Type > &other)\\

** Protected Member Functions
void *notify_resized* (size_type s)\\

void *notify_resized* (size_type size)\\

size_type *operator()* (size_type hash) const\\
Transforms the __hash value hash into a ranged-hash value (using a
modulo operation).

size_type *range_hash* (size_type s) const\\

void *swap* (mod_based_range_hashing &other)\\

* Detailed Description
** "template<typename Size_Type = std::size_t>
\\
class __gnu_pbds::direct_mod_range_hashing< Size_Type >"A mod
range-hashing class (uses the modulo function).

Definition at line *141* of file *hash_policy.hpp*.

* Member Typedef Documentation
** template<typename Size_Type = std::size_t> typedef Size_Type
*__gnu_pbds::direct_mod_range_hashing*< Size_Type >::size_type
Definition at line *145* of file *hash_policy.hpp*.

* Member Function Documentation
** void *__gnu_pbds::detail::mod_based_range_hashing*< std::size_t
>::notify_resized (size_type s)= [inline]=, = [protected]=,
= [inherited]=
Definition at line *60* of file *mod_based_range_hashing.hpp*.

** template<typename Size_Type = std::size_t> size_type
*__gnu_pbds::direct_mod_range_hashing*< Size_Type >::operator()
(size_type hash) const= [inline]=, = [protected]=
Transforms the __hash value hash into a ranged-hash value (using a
modulo operation).

** size_type *__gnu_pbds::detail::mod_based_range_hashing*< std::size_t
>::range_hash (size_type s) const= [inline]=, = [protected]=,
= [inherited]=
Definition at line *64* of file *mod_based_range_hashing.hpp*.

** void *__gnu_pbds::detail::mod_based_range_hashing*< std::size_t
>::swap (*mod_based_range_hashing*< std::size_t > & other)= [inline]=,
= [protected]=, = [inherited]=
Definition at line *56* of file *mod_based_range_hashing.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
