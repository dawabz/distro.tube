#+TITLE: Manpages - XFetchName.3
#+DESCRIPTION: Linux manpage for XFetchName.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XFetchName.3 is found in manpage for: [[../man3/XSetWMName.3][man3/XSetWMName.3]]