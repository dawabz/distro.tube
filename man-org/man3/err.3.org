#+TITLE: Manpages - err.3
#+DESCRIPTION: Linux manpage for err.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
err, verr, errx, verrx, warn, vwarn, warnx, vwarnx - formatted error
messages

* SYNOPSIS
#+begin_example
  #include <err.h>

  noreturn void err(int eval, const char *fmt, ...);
  noreturn void errx(int eval, const char *fmt, ...);

  void warn(const char *fmt, ...);
  void warnx(const char *fmt, ...);

  #include <stdarg.h>

  noreturn void verr(int eval, const char *fmt, va_list args);
  noreturn void verrx(int eval, const char *fmt, va_list args);

  void vwarn(const char *fmt, va_list args);
  void vwarnx(const char *fmt, va_list args);
#+end_example

* DESCRIPTION
The *err*() and *warn*() family of functions display a formatted error
message on the standard error output. In all cases, the last component
of the program name, a colon character, and a space are output. If the
/fmt/ argument is not NULL, the *printf*(3)-like formatted error message
is output. The output is terminated by a newline character.

The *err*(), *verr*(), *warn*(), and *vwarn*() functions append an error
message obtained from *strerror*(3) based on the global variable
/errno/, preceded by another colon and space unless the /fmt/ argument
is NULL.

The *errx*() and *warnx*() functions do not append an error message.

The *err*(), *verr*(), *errx*(), and *verrx*() functions do not return,
but exit with the value of the argument /eval/.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                                                                          | Attribute     | Value          |
| *err*(), *errx*(), *warn*(), *warnx*(), *verr*(), *verrx*(), *vwarn*(), *vwarnx*() | Thread safety | MT-Safe locale |

* CONFORMING TO
These functions are nonstandard BSD extensions.

* EXAMPLES
Display the current /errno/ information string and exit:

#+begin_example
  p = malloc(size);
  if (p == NULL)
      err(EXIT_FAILURE, NULL);
  fd = open(file_name, O_RDONLY, 0);
  if (fd == -1)
      err(EXIT_FAILURE, "%s", file_name);
#+end_example

Display an error message and exit:

#+begin_example
  if (tm.tm_hour < START_TIME)
      errx(EXIT_FAILURE, "too early, wait until %s",
              start_time_string);
#+end_example

Warn of an error:

#+begin_example
  fd = open(raw_device, O_RDONLY, 0);
  if (fd == -1)
      warnx("%s: %s: trying the block device",
              raw_device, strerror(errno));
  fd = open(block_device, O_RDONLY, 0);
  if (fd == -1)
      err(EXIT_FAILURE, "%s", block_device);
#+end_example

* SEE ALSO
*error*(3), *exit*(3), *perror*(3), *printf*(3), *strerror*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
