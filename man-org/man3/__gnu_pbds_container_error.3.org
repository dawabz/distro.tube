#+TITLE: Manpages - __gnu_pbds_container_error.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_container_error.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::container_error - Base class for exceptions.

* SYNOPSIS
\\

=#include <exception.hpp>=

Inherits *std::logic_error*.

Inherited by *__gnu_pbds::insert_error*, *__gnu_pbds::join_error*, and
*__gnu_pbds::resize_error*.

** Public Member Functions
virtual const char * *what* () const noexcept\\

* Detailed Description
Base class for exceptions.

Definition at line *57* of file *exception.hpp*.

* Constructor & Destructor Documentation
** __gnu_pbds::container_error::container_error ()= [inline]=
Definition at line *59* of file *exception.hpp*.

* Member Function Documentation
** virtual const char * std::logic_error::what () const= [virtual]=,
= [noexcept]=, = [inherited]=
Returns a C-style character string describing the general cause of the
current error (the same string passed to the ctor).\\

Reimplemented from *std::exception*.

Reimplemented in *std::future_error*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
