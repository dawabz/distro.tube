#+TITLE: Manpages - backtrace_symbols_fd.3
#+DESCRIPTION: Linux manpage for backtrace_symbols_fd.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about backtrace_symbols_fd.3 is found in manpage for: [[../man3/backtrace.3][man3/backtrace.3]]