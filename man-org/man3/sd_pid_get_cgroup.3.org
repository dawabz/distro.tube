#+TITLE: Manpages - sd_pid_get_cgroup.3
#+DESCRIPTION: Linux manpage for sd_pid_get_cgroup.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sd_pid_get_cgroup.3 is found in manpage for: [[../sd_pid_get_owner_uid.3][sd_pid_get_owner_uid.3]]