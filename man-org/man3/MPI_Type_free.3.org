#+TITLE: Manpages - MPI_Type_free.3
#+DESCRIPTION: Linux manpage for MPI_Type_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Type_free* - Frees a data type.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Type_free(MPI_Datatype *datatype)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_TYPE_FREE(DATATYPE, IERROR)
  	INTEGER	DATATYPE, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Type_free(datatype, ierror)
  	TYPE(MPI_Datatype), INTENT(INOUT) :: datatype
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  void Datatype::Free()
#+end_example

* INPUT/OUTPUT PARAMETER
- datatype :: Datatype that is freed (handle).

* OUTPUT PARAMETER
- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
Marks the datatype object associated with datatype for de-allocation and
sets datatype to MPI_DATATYPE_NULL. Any communication that is currently
using this datatype will complete normally. Derived datatypes that were
defined from the freed datatype are not affected.

Freeing a datatype does not affect any other datatype that was built
from the freed datatype. The system behaves as if input datatype
arguments to derived datatype constructors are passed by value.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.
