#+TITLE: Manpages - pcre2_match_context_create.3
#+DESCRIPTION: Linux manpage for pcre2_match_context_create.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  pcre2_match_context *pcre2_match_context_create(
   pcre2_general_context *gcontext);
#+end_example

* DESCRIPTION
This function creates and initializes a new match context. If its
argument is NULL, *malloc()* is used to get the necessary memory;
otherwise the memory allocation function within the general context is
used. The result is NULL if the memory could not be obtained.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
