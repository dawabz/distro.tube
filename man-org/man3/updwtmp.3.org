#+TITLE: Manpages - updwtmp.3
#+DESCRIPTION: Linux manpage for updwtmp.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
updwtmp, logwtmp - append an entry to the wtmp file

* SYNOPSIS
#+begin_example
  #include <utmp.h>

  void updwtmp(const char *wtmp_file, const struct utmp *ut);
  void logwtmp(const char *line, const char *name",constchar*"host);
#+end_example

For *logwtmp*(), link with /-lutil/.

* DESCRIPTION
*updwtmp*() appends the utmp structure /ut/ to the wtmp file.

*logwtmp*() constructs a utmp structure using /line/, /name/, /host/,
current time, and current process ID. Then it calls *updwtmp*() to
append the structure to the wtmp file.

* FILES
- //var/log/wtmp/ :: database of past user logins

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                | Attribute     | Value                    |
| *updwtmp*(), *logwtmp*() | Thread safety | MT-Unsafe sig:ALRM timer |

* CONFORMING TO
Not in POSIX.1. Present on Solaris, NetBSD, and perhaps other systems.

* NOTES
For consistency with the other "utmpx" functions (see *getutxent*(3)),
glibc provides (since version 2.1):

#+begin_example
  #include <utmpx.h>
  void updwtmpx (const char *wtmpx_file, const struct utmpx *utx);
#+end_example

This function performs the same task as *updwtmp*(), but differs in that
it takes a /utmpx/ structure as its last argument.

* SEE ALSO
*getutxent*(3), *wtmp*(5)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
