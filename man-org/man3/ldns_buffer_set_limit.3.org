#+TITLE: Manpages - ldns_buffer_set_limit.3
#+DESCRIPTION: Linux manpage for ldns_buffer_set_limit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_buffer_limit, ldns_buffer_set_limit, ldns_buffer_capacity,
ldns_buffer_set_capacity, ldns_buffer_reserve, ldns_buffer_at,
ldns_buffer_begin, ldns_buffer_end, ldns_buffer_current - buffer limits
and pointers

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

size_t ldns_buffer_limit(const ldns_buffer *buffer);

void ldns_buffer_set_limit(ldns_buffer *buffer, size_t limit);

size_t ldns_buffer_capacity(const ldns_buffer *buffer);

bool ldns_buffer_set_capacity(ldns_buffer *buffer, size_t capacity);

bool ldns_buffer_reserve(ldns_buffer *buffer, size_t amount);

uint8_t * ldns_buffer_at(const ldns_buffer *buffer, size_t at);

uint8_t * ldns_buffer_begin(const ldns_buffer *buffer);

uint8_t * ldns_buffer_end(const ldns_buffer *buffer);

uint8_t * ldns_buffer_current(const ldns_buffer *buffer);

* DESCRIPTION
/ldns_buffer_limit/() returns the maximum size of the buffer \param[in]
buffer .br Returns the size

/ldns_buffer_set_limit/() changes the buffer's limit. If the buffer's
position is greater than the new limit the position is set to the limit.
.br *buffer*: the buffer .br *limit*: the new limit

/ldns_buffer_capacity/() returns the number of bytes the buffer can
hold. .br *buffer*: the buffer .br Returns the number of bytes

/ldns_buffer_set_capacity/() changes the buffer's capacity. The data is
reallocated so any pointers to the data may become invalid. The buffer's
limit is set to the buffer's new capacity. .br *buffer*: the buffer .br
*capacity*: the capacity to use .br Returns whether this failed or
succeeded

/ldns_buffer_reserve/() ensures BUFFER can contain at least AMOUNT more
bytes. The buffer's capacity is increased if necessary using
buffer_set_capacity().

The buffer's limit is always set to the (possibly increased) capacity.
.br *buffer*: the buffer .br *amount*: amount to use .br Returns whether
this failed or succeeded

/ldns_buffer_at/() returns a pointer to the data at the indicated
position. .br *buffer*: the buffer .br *at*: position .br Returns the
pointer to the data

/ldns_buffer_begin/() returns a pointer to the beginning of the buffer
(the data at position 0). .br *buffer*: the buffer .br Returns the
pointer

/ldns_buffer_end/() returns a pointer to the end of the buffer (the data
at the buffer's limit). .br *buffer*: the buffer .br Returns the pointer

/ldns_buffer_current/() returns a pointer to the data at the buffer's
current position. .br *buffer*: the buffer .br Returns the pointer

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_buffer/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*,
*RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
