#+TITLE: Manpages - gnutls_db_check_entry_time.3
#+DESCRIPTION: Linux manpage for gnutls_db_check_entry_time.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_db_check_entry_time - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*time_t gnutls_db_check_entry_time(gnutls_datum_t * */entry/*);*

* ARGUMENTS
- gnutls_datum_t * entry :: is a pointer to a *gnutls_datum_t* type.

* DESCRIPTION
This function returns the time that this entry was active. It can be
used for database entry expiration.

* RETURNS
The time this entry was created, or zero on error.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
