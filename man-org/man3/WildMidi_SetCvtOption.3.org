#+TITLE: Manpages - WildMidi_SetCvtOption.3
#+DESCRIPTION: Linux manpage for WildMidi_SetCvtOption.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
WildMidi_SetCvtOption - Set a conversion option for a specific midi

* LIBRARY
*libWildMidi*

* SYNOPSIS
*#include <wildmidi_lib.h>*

*int WildMidi_SetCvtOption (uint16_t */tag/, uint16_t /setting/)*

* DESCRIPTION
Set a conversion option for a specific midi.

*/tag/* The type of option you wish to change.

#+begin_quote
  - WM_CO_XMI_TYPE :: The the conversion type of an XMI to MIDI.

  - WM_CO_FREQUENCY :: The frequency to be used for a MUS file.
#+end_quote

- setting :: Value for the particular tag.

* RETURN VALUE
Returns -1 on error, otherwise returns 0.

* SEE ALSO
*WildMidi_GetVersion*(3)*,* *WildMidi_Init*(3)*,*
*WildMidi_MasterVolume*(3)*,* *WildMidi_Open*(3)*,*
*WildMidi_OpenBuffer*(3)*,* *WildMidi_SetCvtOption*(3)*,*
*WildMidi_GetOutput*(3)*,* *WildMidi_GetMidiOutput*(3)*,*
*WildMidi_GetInfo*(3)*,* *WildMidi_FastSeek*(3)*,*
*WildMidi_Close*(3)*,* *WildMidi_Shutdown*(3)*,* *wildmidi.cfg*(5)

* AUTHOR
Chris Ison <chrisisonwildcode@gmail.com> Bret Curtis <psi29a@gmail.com>

* COPYRIGHT
Copyright (C) WildMidi Developers 2001-2016

This file is part of WildMIDI.

WildMIDI is free software: you can redistribute and/or modify the player
under the terms of the GNU General Public License and you can
redistribute and/or modify the library under the terms of the GNU Lesser
General Public License as published by the Free Software Foundation,
either version 3 of the licenses, or(at your option) any later version.

WildMIDI is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License and
the GNU Lesser General Public License for more details.

You should have received a copy of the GNU General Public License and
the GNU Lesser General Public License along with WildMIDI. If not, see
<http://www.gnu.org/licenses/>.

This manpage is licensed under the Creative Commons Attribution-Share
Alike 3.0 Unported License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/3.0/ or send a letter to
Creative Commons, 171 Second Street, Suite 300, San Francisco,
California, 94105, USA.
