#+TITLE: Manpages - ldns_rr_dnskey_protocol.3
#+DESCRIPTION: Linux manpage for ldns_rr_dnskey_protocol.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_rr_dnskey_flags, ldns_rr_dnskey_set_flags, ldns_rr_dnskey_protocol,
ldns_rr_dnskey_set_protocol, ldns_rr_dnskey_algorithm,
ldns_rr_dnskey_set_algorithm, ldns_rr_dnskey_key,
ldns_rr_dnskey_set_key - get and set DNSKEY RR rdata fields

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_rdf* ldns_rr_dnskey_flags(const ldns_rr *r);

bool ldns_rr_dnskey_set_flags(ldns_rr *r, ldns_rdf *f);

ldns_rdf* ldns_rr_dnskey_protocol(const ldns_rr *r);

bool ldns_rr_dnskey_set_protocol(ldns_rr *r, ldns_rdf *f);

ldns_rdf* ldns_rr_dnskey_algorithm(const ldns_rr *r);

bool ldns_rr_dnskey_set_algorithm(ldns_rr *r, ldns_rdf *f);

ldns_rdf* ldns_rr_dnskey_key(const ldns_rr *r);

bool ldns_rr_dnskey_set_key(ldns_rr *r, ldns_rdf *f);

* DESCRIPTION
/ldns_rr_dnskey_flags/() returns the flags of a LDNS_RR_TYPE_DNSKEY rr
.br *r*: the resource record .br Returns a ldns_rdf* with the flags or
NULL on failure

/ldns_rr_dnskey_set_flags/() sets the flags of a LDNS_RR_TYPE_DNSKEY rr
.br *r*: the rr to use .br *f*: the flags to set .br Returns true on
success, false otherwise

/ldns_rr_dnskey_protocol/() returns the protocol of a
LDNS_RR_TYPE_DNSKEY rr .br *r*: the resource record .br Returns a
ldns_rdf* with the protocol or NULL on failure

/ldns_rr_dnskey_set_protocol/() sets the protocol of a
LDNS_RR_TYPE_DNSKEY rr .br *r*: the rr to use .br *f*: the protocol to
set .br Returns true on success, false otherwise

/ldns_rr_dnskey_algorithm/() returns the algorithm of a
LDNS_RR_TYPE_DNSKEY rr .br *r*: the resource record .br Returns a
ldns_rdf* with the algorithm or NULL on failure

/ldns_rr_dnskey_set_algorithm/() sets the algorithm of a
LDNS_RR_TYPE_DNSKEY rr .br *r*: the rr to use .br *f*: the algorithm to
set .br Returns true on success, false otherwise

/ldns_rr_dnskey_key/() returns the key data of a LDNS_RR_TYPE_DNSKEY rr
.br *r*: the resource record .br Returns a ldns_rdf* with the key data
or NULL on failure

/ldns_rr_dnskey_set_key/() sets the key data of a LDNS_RR_TYPE_DNSKEY rr
.br *r*: the rr to use .br *f*: the key data to set .br Returns true on
success, false otherwise

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_rr/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*,
*RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
