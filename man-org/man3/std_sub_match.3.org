#+TITLE: Manpages - std_sub_match.3
#+DESCRIPTION: Linux manpage for std_sub_match.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::sub_match< _BiIter >

* SYNOPSIS
\\

=#include <regex.h>=

Inherits *std::pair< _BiIter, _BiIter >*.

** Public Types
typedef __iter_traits::difference_type *difference_type*\\

typedef _BiIter *first_type*\\
The type of the =first= member.

typedef _BiIter *iterator*\\

typedef _BiIter *second_type*\\
The type of the =second= member.

typedef *basic_string*< value_type > *string_type*\\

typedef __iter_traits::value_type *value_type*\\

** Public Member Functions
int *compare* (const *sub_match* &__s) const\\
Compares this and another matched sequence.

difference_type *length* () const noexcept\\
Gets the length of the matching sequence.

*operator string_type* () const\\
Gets the matching sequence as a string.

*string_type* *str* () const\\
Gets the matching sequence as a string.

constexpr void *swap* (*pair* &__p) noexcept(__and_<
__is_nothrow_swappable< _BiIter >, __is_nothrow_swappable< _BiIter >
>::value)\\
Swap the first members and then the second members.

\\

int *compare* (const *string_type* &__s) const\\
Compares this =sub_match= to a string.

int *compare* (const value_type *__s) const\\
Compares this =sub_match= to a string.

** Public Attributes
_BiIter *first*\\
The first member.

bool *matched*\\

_BiIter *second*\\
The second member.

** Related Functions
(Note that these are not member functions.)

constexpr *pair*< typename __decay_and_strip< _BiIter >::__type,
typename __decay_and_strip< _BiIter >::__type > *make_pair* (_BiIter
&&__x, _BiIter &&__y)\\
A convenience wrapper for creating a pair from two objects.

\\

template<typename _BiIter > bool *operator==* (const *sub_match*<
_BiIter > &__lhs, const *sub_match*< _BiIter > &__rhs)\\
Tests the equivalence of two regular expression submatches.

template<typename _BiIter > bool *operator!=* (const *sub_match*<
_BiIter > &__lhs, const *sub_match*< _BiIter > &__rhs)\\
Tests the inequivalence of two regular expression submatches.

template<typename _BiIter > bool *operator<* (const *sub_match*< _BiIter
> &__lhs, const *sub_match*< _BiIter > &__rhs)\\
Tests the ordering of two regular expression submatches.

template<typename _BiIter > bool *operator<=* (const *sub_match*<
_BiIter > &__lhs, const *sub_match*< _BiIter > &__rhs)\\
Tests the ordering of two regular expression submatches.

template<typename _BiIter > bool *operator>=* (const *sub_match*<
_BiIter > &__lhs, const *sub_match*< _BiIter > &__rhs)\\
Tests the ordering of two regular expression submatches.

template<typename _BiIter > bool *operator>* (const *sub_match*< _BiIter
> &__lhs, const *sub_match*< _BiIter > &__rhs)\\
Tests the ordering of two regular expression submatches.

template<typename _Bi_iter , typename _Ch_traits , typename _Ch_alloc >
bool *operator==* (const __sub_match_string< _Bi_iter, _Ch_traits,
_Ch_alloc > &__lhs, const *sub_match*< _Bi_iter > &__rhs)\\
Tests the equivalence of a string and a regular expression submatch.

template<typename _Bi_iter , typename _Ch_traits , typename _Ch_alloc >
bool *operator!=* (const __sub_match_string< _Bi_iter, _Ch_traits,
_Ch_alloc > &__lhs, const *sub_match*< _Bi_iter > &__rhs)\\
Tests the inequivalence of a string and a regular expression submatch.

template<typename _Bi_iter , typename _Ch_traits , typename _Ch_alloc >
bool *operator<* (const __sub_match_string< _Bi_iter, _Ch_traits,
_Ch_alloc > &__lhs, const *sub_match*< _Bi_iter > &__rhs)\\
Tests the ordering of a string and a regular expression submatch.

template<typename _Bi_iter , typename _Ch_traits , typename _Ch_alloc >
bool *operator>* (const __sub_match_string< _Bi_iter, _Ch_traits,
_Ch_alloc > &__lhs, const *sub_match*< _Bi_iter > &__rhs)\\
Tests the ordering of a string and a regular expression submatch.

template<typename _Bi_iter , typename _Ch_traits , typename _Ch_alloc >
bool *operator>=* (const __sub_match_string< _Bi_iter, _Ch_traits,
_Ch_alloc > &__lhs, const *sub_match*< _Bi_iter > &__rhs)\\
Tests the ordering of a string and a regular expression submatch.

template<typename _Bi_iter , typename _Ch_traits , typename _Ch_alloc >
bool *operator<=* (const __sub_match_string< _Bi_iter, _Ch_traits,
_Ch_alloc > &__lhs, const *sub_match*< _Bi_iter > &__rhs)\\
Tests the ordering of a string and a regular expression submatch.

template<typename _Bi_iter , typename _Ch_traits , typename _Ch_alloc >
bool *operator==* (const *sub_match*< _Bi_iter > &__lhs, const
__sub_match_string< _Bi_iter, _Ch_traits, _Ch_alloc > &__rhs)\\
Tests the equivalence of a regular expression submatch and a string.

template<typename _Bi_iter , typename _Ch_traits , typename _Ch_alloc >
bool *operator!=* (const *sub_match*< _Bi_iter > &__lhs, const
__sub_match_string< _Bi_iter, _Ch_traits, _Ch_alloc > &__rhs)\\
Tests the inequivalence of a regular expression submatch and a string.

template<typename _Bi_iter , typename _Ch_traits , typename _Ch_alloc >
bool *operator<* (const *sub_match*< _Bi_iter > &__lhs, const
__sub_match_string< _Bi_iter, _Ch_traits, _Ch_alloc > &__rhs)\\
Tests the ordering of a regular expression submatch and a string.

template<typename _Bi_iter , typename _Ch_traits , typename _Ch_alloc >
bool *operator>* (const *sub_match*< _Bi_iter > &__lhs, const
__sub_match_string< _Bi_iter, _Ch_traits, _Ch_alloc > &__rhs)\\
Tests the ordering of a regular expression submatch and a string.

template<typename _Bi_iter , typename _Ch_traits , typename _Ch_alloc >
bool *operator>=* (const *sub_match*< _Bi_iter > &__lhs, const
__sub_match_string< _Bi_iter, _Ch_traits, _Ch_alloc > &__rhs)\\
Tests the ordering of a regular expression submatch and a string.

template<typename _Bi_iter , typename _Ch_traits , typename _Ch_alloc >
bool *operator<=* (const *sub_match*< _Bi_iter > &__lhs, const
__sub_match_string< _Bi_iter, _Ch_traits, _Ch_alloc > &__rhs)\\
Tests the ordering of a regular expression submatch and a string.

template<typename _Bi_iter > bool *operator==* (typename
*iterator_traits*< _Bi_iter >::value_type const *__lhs, const
*sub_match*< _Bi_iter > &__rhs)\\
Tests the equivalence of a C string and a regular expression submatch.

template<typename _Bi_iter > bool *operator!=* (typename
*iterator_traits*< _Bi_iter >::value_type const *__lhs, const
*sub_match*< _Bi_iter > &__rhs)\\
Tests the inequivalence of a C string and a regular expression submatch.

template<typename _Bi_iter > bool *operator<* (typename
*iterator_traits*< _Bi_iter >::value_type const *__lhs, const
*sub_match*< _Bi_iter > &__rhs)\\
Tests the ordering of a C string and a regular expression submatch.

template<typename _Bi_iter > bool *operator>* (typename
*iterator_traits*< _Bi_iter >::value_type const *__lhs, const
*sub_match*< _Bi_iter > &__rhs)\\
Tests the ordering of a C string and a regular expression submatch.

template<typename _Bi_iter > bool *operator>=* (typename
*iterator_traits*< _Bi_iter >::value_type const *__lhs, const
*sub_match*< _Bi_iter > &__rhs)\\
Tests the ordering of a C string and a regular expression submatch.

template<typename _Bi_iter > bool *operator<=* (typename
*iterator_traits*< _Bi_iter >::value_type const *__lhs, const
*sub_match*< _Bi_iter > &__rhs)\\
Tests the ordering of a C string and a regular expression submatch.

template<typename _Bi_iter > bool *operator==* (const *sub_match*<
_Bi_iter > &__lhs, typename *iterator_traits*< _Bi_iter >::value_type
const *__rhs)\\
Tests the equivalence of a regular expression submatch and a C string.

template<typename _Bi_iter > bool *operator!=* (const *sub_match*<
_Bi_iter > &__lhs, typename *iterator_traits*< _Bi_iter >::value_type
const *__rhs)\\
Tests the inequivalence of a regular expression submatch and a string.

template<typename _Bi_iter > bool *operator<* (const *sub_match*<
_Bi_iter > &__lhs, typename *iterator_traits*< _Bi_iter >::value_type
const *__rhs)\\
Tests the ordering of a regular expression submatch and a C string.

template<typename _Bi_iter > bool *operator>* (const *sub_match*<
_Bi_iter > &__lhs, typename *iterator_traits*< _Bi_iter >::value_type
const *__rhs)\\
Tests the ordering of a regular expression submatch and a C string.

template<typename _Bi_iter > bool *operator>=* (const *sub_match*<
_Bi_iter > &__lhs, typename *iterator_traits*< _Bi_iter >::value_type
const *__rhs)\\
Tests the ordering of a regular expression submatch and a C string.

template<typename _Bi_iter > bool *operator<=* (const *sub_match*<
_Bi_iter > &__lhs, typename *iterator_traits*< _Bi_iter >::value_type
const *__rhs)\\
Tests the ordering of a regular expression submatch and a C string.

template<typename _Bi_iter > bool *operator==* (typename
*iterator_traits*< _Bi_iter >::value_type const &__lhs, const
*sub_match*< _Bi_iter > &__rhs)\\
Tests the equivalence of a character and a regular expression submatch.

template<typename _Bi_iter > bool *operator!=* (typename
*iterator_traits*< _Bi_iter >::value_type const &__lhs, const
*sub_match*< _Bi_iter > &__rhs)\\
Tests the inequivalence of a character and a regular expression
submatch.

template<typename _Bi_iter > bool *operator<* (typename
*iterator_traits*< _Bi_iter >::value_type const &__lhs, const
*sub_match*< _Bi_iter > &__rhs)\\
Tests the ordering of a character and a regular expression submatch.

template<typename _Bi_iter > bool *operator>* (typename
*iterator_traits*< _Bi_iter >::value_type const &__lhs, const
*sub_match*< _Bi_iter > &__rhs)\\
Tests the ordering of a character and a regular expression submatch.

template<typename _Bi_iter > bool *operator>=* (typename
*iterator_traits*< _Bi_iter >::value_type const &__lhs, const
*sub_match*< _Bi_iter > &__rhs)\\
Tests the ordering of a character and a regular expression submatch.

template<typename _Bi_iter > bool *operator<=* (typename
*iterator_traits*< _Bi_iter >::value_type const &__lhs, const
*sub_match*< _Bi_iter > &__rhs)\\
Tests the ordering of a character and a regular expression submatch.

template<typename _Bi_iter > bool *operator==* (const *sub_match*<
_Bi_iter > &__lhs, typename *iterator_traits*< _Bi_iter >::value_type
const &__rhs)\\
Tests the equivalence of a regular expression submatch and a character.

template<typename _Bi_iter > bool *operator!=* (const *sub_match*<
_Bi_iter > &__lhs, typename *iterator_traits*< _Bi_iter >::value_type
const &__rhs)\\
Tests the inequivalence of a regular expression submatch and a
character.

template<typename _Bi_iter > bool *operator<* (const *sub_match*<
_Bi_iter > &__lhs, typename *iterator_traits*< _Bi_iter >::value_type
const &__rhs)\\
Tests the ordering of a regular expression submatch and a character.

template<typename _Bi_iter > bool *operator>* (const *sub_match*<
_Bi_iter > &__lhs, typename *iterator_traits*< _Bi_iter >::value_type
const &__rhs)\\
Tests the ordering of a regular expression submatch and a character.

template<typename _Bi_iter > bool *operator>=* (const *sub_match*<
_Bi_iter > &__lhs, typename *iterator_traits*< _Bi_iter >::value_type
const &__rhs)\\
Tests the ordering of a regular expression submatch and a character.

template<typename _Bi_iter > bool *operator<=* (const *sub_match*<
_Bi_iter > &__lhs, typename *iterator_traits*< _Bi_iter >::value_type
const &__rhs)\\
Tests the ordering of a regular expression submatch and a character.

template<typename _Ch_type , typename _Ch_traits , typename _Bi_iter >
*basic_ostream*< _Ch_type, _Ch_traits > & *operator<<* (*basic_ostream*<
_Ch_type, _Ch_traits > &__os, const *sub_match*< _Bi_iter > &__m)\\
Inserts a matched string into an output stream.

\\

constexpr *enable_if*< __and_< __is_swappable< _BiIter >,
__is_swappable< _BiIter > >::value >::type *swap* (*pair*< _BiIter,
_BiIter > &__x, *pair*< _BiIter, _BiIter > &__y)
noexcept(noexcept(__x.swap(__y)))\\

constexpr bool *operator==* (const *pair*< _BiIter, _BiIter > &__x,
const *pair*< _BiIter, _BiIter > &__y)\\

constexpr bool *operator<* (const *pair*< _BiIter, _BiIter > &__x, const
*pair*< _BiIter, _BiIter > &__y)\\

constexpr bool *operator!=* (const *pair*< _BiIter, _BiIter > &__x,
const *pair*< _BiIter, _BiIter > &__y)\\
Uses =operator=== to find the result.

constexpr bool *operator>* (const *pair*< _BiIter, _BiIter > &__x, const
*pair*< _BiIter, _BiIter > &__y)\\
Uses =operator<= to find the result.

constexpr bool *operator<=* (const *pair*< _BiIter, _BiIter > &__x,
const *pair*< _BiIter, _BiIter > &__y)\\
Uses =operator<= to find the result.

constexpr bool *operator>=* (const *pair*< _BiIter, _BiIter > &__x,
const *pair*< _BiIter, _BiIter > &__y)\\
Uses =operator<= to find the result.

* Detailed Description
** "template<typename _BiIter>
\\
class std::sub_match< _BiIter >"A sequence of characters matched by a
particular marked sub-expression.

An object of this class is essentially a pair of iterators marking a
matched subexpression within a regular expression pattern match. Such
objects can be converted to and compared with std::basic_string objects
of a similar base character type as the pattern matched by the regular
expression.

The iterators that make up the pair are the usual half-open interval
referencing the actual original pattern matched.

Definition at line *869* of file *regex.h*.

* Member Typedef Documentation
** template<typename _BiIter > typedef __iter_traits::difference_type
*std::sub_match*< _BiIter >::difference_type
Definition at line *875* of file *regex.h*.

** typedef _BiIter *std::pair*< _BiIter , _BiIter
>::*first_type*= [inherited]=
The type of the =first= member.

Definition at line *214* of file *stl_pair.h*.

** template<typename _BiIter > typedef _BiIter *std::sub_match*< _BiIter
>::iterator
Definition at line *876* of file *regex.h*.

** typedef _BiIter *std::pair*< _BiIter , _BiIter
>::*second_type*= [inherited]=
The type of the =second= member.

Definition at line *215* of file *stl_pair.h*.

** template<typename _BiIter > typedef *basic_string*<value_type>
*std::sub_match*< _BiIter >::*string_type*
Definition at line *877* of file *regex.h*.

** template<typename _BiIter > typedef __iter_traits::value_type
*std::sub_match*< _BiIter >::value_type
Definition at line *874* of file *regex.h*.

* Constructor & Destructor Documentation
** template<typename _BiIter > constexpr *std::sub_match*< _BiIter
>::*sub_match* ()= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *881* of file *regex.h*.

* Member Function Documentation
** template<typename _BiIter > int *std::sub_match*< _BiIter >::compare
(const *string_type* & __s) const= [inline]=
Compares this =sub_match= to a string.

*Parameters*

#+begin_quote
  /__s/ A string to compare to this =sub_match=.
#+end_quote

*Return values*

#+begin_quote
  /negative/ This matched sequence will collate before =__s=.\\
  /zero/ This matched sequence is equivalent to =__s=.\\
  /positive/ This matched sequence will collate after =__s=.
#+end_quote

Definition at line *938* of file *regex.h*.

** template<typename _BiIter > int *std::sub_match*< _BiIter >::compare
(const *sub_match*< _BiIter > & __s) const= [inline]=
Compares this and another matched sequence.

*Parameters*

#+begin_quote
  /__s/ Another matched sequence to compare to this one.
#+end_quote

*Return values*

#+begin_quote
  /negative/ This matched sequence will collate before =__s=.\\
  /zero/ This matched sequence is equivalent to =__s=.\\
  /positive/ This matched sequence will collate after =__s=.
#+end_quote

Definition at line *924* of file *regex.h*.

Referenced by *std::sub_match< _BiIter >::operator!=()*,
*std::sub_match< _BiIter >::operator<()*, *std::sub_match< _BiIter
>::operator<=()*, *std::sub_match< _BiIter >::operator==()*,
*std::sub_match< _BiIter >::operator>()*, and *std::sub_match< _BiIter
>::operator>=()*.

** template<typename _BiIter > int *std::sub_match*< _BiIter >::compare
(const value_type * __s) const= [inline]=
Compares this =sub_match= to a string.

*Parameters*

#+begin_quote
  /__s/ A string to compare to this =sub_match=.
#+end_quote

*Return values*

#+begin_quote
  /negative/ This matched sequence will collate before =__s=.\\
  /zero/ This matched sequence is equivalent to =__s=.\\
  /positive/ This matched sequence will collate after =__s=.
#+end_quote

Definition at line *942* of file *regex.h*.

** template<typename _BiIter > difference_type *std::sub_match*< _BiIter
>::length () const= [inline]=, = [noexcept]=
Gets the length of the matching sequence.

Definition at line *885* of file *regex.h*.

References *std::pair< _BiIter, _BiIter >::first*, and *std::pair<
_BiIter, _BiIter >::second*.

** template<typename _BiIter > *std::sub_match*< _BiIter >::operator
*string_type* () const= [inline]=
Gets the matching sequence as a string.

*Returns*

#+begin_quote
  the matching sequence as a string.
#+end_quote

This is the implicit conversion operator. It is identical to the str()
member function except that it will want to pop up in unexpected places
and cause a great deal of confusion and cursing from the unwary.

Definition at line *898* of file *regex.h*.

References *std::sub_match< _BiIter >::str()*.

** template<typename _BiIter > *string_type* *std::sub_match*< _BiIter
>::str () const= [inline]=
Gets the matching sequence as a string.

*Returns*

#+begin_quote
  the matching sequence as a string.
#+end_quote

Definition at line *907* of file *regex.h*.

References *std::pair< _BiIter, _BiIter >::first*, and *std::pair<
_BiIter, _BiIter >::second*.

Referenced by *std::sub_match< _BiIter >::operator string_type()*.

** constexpr void *std::pair*< _BiIter , _BiIter >::swap (*pair*<
_BiIter, _BiIter > & __p)= [inline]=, = [constexpr]=, = [noexcept]=,
= [inherited]=
Swap the first members and then the second members.

Definition at line *439* of file *stl_pair.h*.

* Friends And Related Function Documentation
** constexpr *pair*< typename __decay_and_strip< _BiIter >::__type,
typename __decay_and_strip< _BiIter >::__type > make_pair (_BiIter &&
__x, _BiIter && __y)= [related]=
A convenience wrapper for creating a pair from two objects.

*Parameters*

#+begin_quote
  /__x/ The first object.\\
  /__y/ The second object.
#+end_quote

*Returns*

#+begin_quote
  A newly-constructed pair<> object of the appropriate type.
#+end_quote

The C++98 standard says the objects are passed by reference-to-const,
but C++03 says they are passed by value (this was LWG issue #181).

Since C++11 they have been passed by forwarding reference and then
forwarded to the new members of the pair. To create a pair with a member
of reference type, pass a =reference_wrapper= to this function.

Definition at line *567* of file *stl_pair.h*.

** constexpr bool operator!= (const *pair*< _BiIter , _BiIter > & __x,
const *pair*< _BiIter , _BiIter > & __y)= [related]=
Uses =operator=== to find the result.

Definition at line *496* of file *stl_pair.h*.

** constexpr bool operator< (const *pair*< _BiIter , _BiIter > & __x,
const *pair*< _BiIter , _BiIter > & __y)= [related]=
Defines a lexicographical order for pairs.

For two pairs of the same type, =P= is ordered before =Q= if =P.first=
is less than =Q.first=, or if =P.first= and =Q.first= are equivalent
(neither is less than the other) and =P.second= is less than =Q.second=.

Definition at line *488* of file *stl_pair.h*.

** constexpr bool operator<= (const *pair*< _BiIter , _BiIter > & __x,
const *pair*< _BiIter , _BiIter > & __y)= [related]=
Uses =operator<= to find the result.

Definition at line *507* of file *stl_pair.h*.

** constexpr bool operator== (const *pair*< _BiIter , _BiIter > & __x,
const *pair*< _BiIter , _BiIter > & __y)= [related]=
Two pairs of the same type are equal iff their members are equal.

Definition at line *466* of file *stl_pair.h*.

** constexpr bool operator> (const *pair*< _BiIter , _BiIter > & __x,
const *pair*< _BiIter , _BiIter > & __y)= [related]=
Uses =operator<= to find the result.

Definition at line *502* of file *stl_pair.h*.

** constexpr bool operator>= (const *pair*< _BiIter , _BiIter > & __x,
const *pair*< _BiIter , _BiIter > & __y)= [related]=
Uses =operator<= to find the result.

Definition at line *514* of file *stl_pair.h*.

** constexpr *enable_if*< __and_< __is_swappable< _BiIter >,
__is_swappable< _BiIter > >::value >::type swap (*pair*< _BiIter ,
_BiIter > & __x, *pair*< _BiIter , _BiIter > & __y)= [related]=
Swap overload for pairs. Calls std::pair::swap().

*Note*

#+begin_quote
  This std::swap overload is not declared in C++03 mode, which has
  performance implications, e.g. see https://gcc.gnu.org/PR38466
#+end_quote

Definition at line *533* of file *stl_pair.h*.

* Member Data Documentation
** _BiIter *std::pair*< _BiIter , _BiIter >::first= [inherited]=
The first member.

Definition at line *217* of file *stl_pair.h*.

** template<typename _BiIter > bool *std::sub_match*< _BiIter >::matched
Definition at line *879* of file *regex.h*.

** _BiIter *std::pair*< _BiIter , _BiIter >::second= [inherited]=
The second member.

Definition at line *218* of file *stl_pair.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
