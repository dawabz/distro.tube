#+TITLE: Manpages - zip_file_get_error.3
#+DESCRIPTION: Linux manpage for zip_file_get_error.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

function returns the zip_error associated with the zip_file

was added in libzip 1.0.

and
