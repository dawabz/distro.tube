#+TITLE: Manpages - SDL_GetRGBA.3
#+DESCRIPTION: Linux manpage for SDL_GetRGBA.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_GetRGBA - Get RGBA values from a pixel in the specified pixel
format.

* SYNOPSIS
*#include "SDL.h"*

*void SDL_GetRGBA*(*Uint32 pixel, SDL_PixelFormat *fmt, Uint8 *r, Uint8
*g, Uint8 *b, Uint8 *a*);

* DESCRIPTION
Get RGBA component values from a pixel stored in the specified pixel
format.

This function uses the entire 8-bit [0..255] range when converting color
components from pixel formats with less than 8-bits per RGB component
(e.g., a completely white pixel in 16-bit RGB565 format would return
[0xff, 0xff, 0xff] not [0xf8, 0xfc, 0xf8]).

If the surface has no alpha component, the alpha will be returned as
0xff (100% opaque).

* SEE ALSO
/SDL_GetRGB/, /SDL_MapRGB/, /SDL_MapRGBA/, /SDL_PixelFormat/
