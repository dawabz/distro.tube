#+TITLE: Manpages - ts_error_fn.3
#+DESCRIPTION: Linux manpage for ts_error_fn.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ts_error_fn - use a custom error function for libts library errors

* SYNOPSIS
#+begin_example
  #include <tslib.h>

  "int(*ts_error_fn)(constchar*fmt,va_listap);
#+end_example

* DESCRIPTION
*ts_error_fn*() is by default implemented similar like this:

#+begin_example

  static int errfn(const char *fmt, va_list ap)
  {
          return vfprintf(stderr, fmt, ap);
  }       
#+end_example

inside the library. It is exposed to the user and can be replaced by a
custom error function. Simply assign your custom implementation to
*ts_error_fn* like

#+begin_example

  ts_error_fn = my_custom_errfn;
#+end_example

It can be used to write the system log files, for example. The
ts_print_mt test program has an example.

* RETURN VALUE
user defined.

* SEE ALSO
*ts_read*(3), *ts_open*(3), *ts_setup*(3), *ts_close*(3), *ts.conf*(5)
