#+TITLE: Manpages - curs_addchstr.3x
#+DESCRIPTION: Linux manpage for curs_addchstr.3x
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*addchstr*, *addchnstr*, *waddchstr*, *waddchnstr*, *mvaddchstr*,
*mvaddchnstr*, *mvwaddchstr*, *mvwaddchnstr* - add a string of
characters (and attributes) to a *curses* window

* SYNOPSIS
#+begin_example
  #include <curses.h>

  int addchstr(const chtype *chstr);


  int addchnstr(const chtype *chstr, int n);


  int waddchstr(WINDOW *win, const chtype *chstr);


  int waddchnstr(WINDOW *win, const chtype *chstr, int n);

  int mvaddchstr(int y, int x, const chtype *chstr);


  int mvaddchnstr(int y, int x, const chtype *chstr, int n);


  int mvwaddchstr(WINDOW *win, int y, int x, const chtype *chstr);


  int mvwaddchnstr(WINDOW *win, int y, int x, const chtype *chstr, int n);
#+end_example

* DESCRIPTION
These functions copy the (null-terminated) /chstr/ array into the window
image structure starting at the current cursor position. The four
functions with /n/ as the last argument copy at most /n/ elements, but
no more than will fit on the line. If *n*=*-1* then the whole array is
copied, to the maximum number of characters that will fit on the line.

The window cursor is /not/ advanced. These functions work faster than
*waddnstr*. On the other hand:

- they do not perform checking (such as for the newline, backspace, or
  carriage return characters),

- they do not advance the current cursor position,

- they do not expand other control characters to ^-escapes, and

- they truncate the string if it crosses the right margin, rather than
  wrapping it around to the new line.

* RETURN VALUE
All functions return the integer *ERR* upon failure and *OK* on success.

X/Open does not define any error conditions. This implementation returns
an error if the window pointer is null.

Functions with a mv prefix first perform a cursor movement using
*wmove*, and return an error if the position is outside the window, or
if the window pointer is null.

* NOTES
All functions except *waddchnstr* may be macros.

* PORTABILITY
These entry points are described in the XSI Curses standard, Issue 4.

* SEE ALSO
*curses*(3X), *curs_addstr*(3X).

Comparable functions in the wide-character (ncursesw) library are
described in *curs_add_wchstr*(3X).
