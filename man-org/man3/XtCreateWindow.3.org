#+TITLE: Manpages - XtCreateWindow.3
#+DESCRIPTION: Linux manpage for XtCreateWindow.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtCreateWindow - window creation convenience function

* SYNTAX
#include <X11/Intrinsic.h>

void XtCreateWindow(Widget /w/, unsigned int /window_class/, Visual
*/visual/, XtValueMask /value_mask/, XSetWindowAttributes
*/attributes/);

* ARGUMENTS
- attributes :: Specifies the window attributes to use in the
  *XCreateWindow* call.

- value_mask :: Specifies which attribute fields to use.

- visual :: Specifies the visual type (usually *CopyFromParent*).

23. Specifies the widget that is used to set the x,y coordinates and so
    on.

- window_class :: Specifies the Xlib window class (for example,
  *InputOutput*, *InputOnly*, or *CopyFromParent*).

* DESCRIPTION
The *XtCreateWindow* function calls the Xlib *XCreateWindow* function
with values from the widget structure and the passed parameters. Then,
it assigns the created window to the widget's window field.

*XtCreateWindow* evaluates the following fields of the *Core* widget
structure:

- depth

- screen

- parent -> core.window

- x

- y

- width

- height

- border_width

* SEE ALSO
\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
