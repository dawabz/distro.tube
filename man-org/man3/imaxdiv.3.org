#+TITLE: Manpages - imaxdiv.3
#+DESCRIPTION: Linux manpage for imaxdiv.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about imaxdiv.3 is found in manpage for: [[../man3/div.3][man3/div.3]]