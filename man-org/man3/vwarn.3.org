#+TITLE: Manpages - vwarn.3
#+DESCRIPTION: Linux manpage for vwarn.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about vwarn.3 is found in manpage for: [[../man3/err.3][man3/err.3]]