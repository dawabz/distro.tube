#+TITLE: Manpages - FcCharSetCount.3
#+DESCRIPTION: Linux manpage for FcCharSetCount.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcCharSetCount - Count entries in a charset

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcChar32 FcCharSetCount (const FcCharSet */a/*);*

* DESCRIPTION
Returns the total number of Unicode chars in /a/.
