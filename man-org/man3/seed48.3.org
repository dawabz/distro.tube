#+TITLE: Manpages - seed48.3
#+DESCRIPTION: Linux manpage for seed48.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about seed48.3 is found in manpage for: [[../man3/drand48.3][man3/drand48.3]]