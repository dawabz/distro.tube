#+TITLE: Manpages - CURLOPT_NOSIGNAL.3
#+DESCRIPTION: Linux manpage for CURLOPT_NOSIGNAL.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_NOSIGNAL - skip all signal handling

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_NOSIGNAL, long onoff);

* DESCRIPTION
If /onoff/ is 1, libcurl will not use any functions that install signal
handlers or any functions that cause signals to be sent to the process.
This option is here to allow multi-threaded unix applications to still
set/use all timeout options etc, without risking getting signals.

If this option is set and libcurl has been built with the standard name
resolver, timeouts will not occur while the name resolve takes place.
Consider building libcurl with the c-ares or threaded resolver backends
to enable asynchronous DNS lookups, to enable timeouts for name resolves
without the use of signals.

Setting /CURLOPT_NOSIGNAL(3)/ to 1 makes libcurl NOT ask the system to
ignore SIGPIPE signals, which otherwise are sent by the system when
trying to send data to a socket which is closed in the other end.
libcurl makes an effort to never cause such SIGPIPEs to trigger, but
some operating systems have no way to avoid them and even on those that
have there are some corner cases when they may still happen, contrary to
our desire. In addition, using /CURLAUTH_NTLM_WB/ authentication could
cause a SIGCHLD signal to be raised.

* DEFAULT
0

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");

    curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1L);

    ret = curl_easy_perform(curl);

    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.10

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_TIMEOUT*(3),
