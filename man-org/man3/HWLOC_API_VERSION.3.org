#+TITLE: Manpages - HWLOC_API_VERSION.3
#+DESCRIPTION: Linux manpage for HWLOC_API_VERSION.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about HWLOC_API_VERSION.3 is found in manpage for: [[../man3/hwlocality_api_version.3][man3/hwlocality_api_version.3]]