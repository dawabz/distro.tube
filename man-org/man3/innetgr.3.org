#+TITLE: Manpages - innetgr.3
#+DESCRIPTION: Linux manpage for innetgr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about innetgr.3 is found in manpage for: [[../man3/setnetgrent.3][man3/setnetgrent.3]]