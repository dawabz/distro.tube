#+TITLE: Manpages - XGetDeviceDontPropagateList.3
#+DESCRIPTION: Linux manpage for XGetDeviceDontPropagateList.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XGetDeviceDontPropagateList.3 is found in manpage for: [[../man3/XChangeDeviceDontPropagateList.3][man3/XChangeDeviceDontPropagateList.3]]