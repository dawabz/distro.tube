#+TITLE: Manpages - FcWeightFromOpenTypeDouble.3
#+DESCRIPTION: Linux manpage for FcWeightFromOpenTypeDouble.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcWeightFromOpenTypeDouble - Convert from OpenType weight values to
fontconfig ones

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

double FcWeightFromOpenTypeDouble (double/ot_weight/*);*

* DESCRIPTION
*FcWeightFromOpenTypeDouble* returns an double value to use with
FC_WEIGHT, from an double in the 1..1000 range, resembling the numbers
from OpenType specification's OS/2 usWeight numbers, which are also
similar to CSS font-weight numbers. If input is negative, zero, or
greater than 1000, returns -1. This function linearly interpolates
between various FC_WEIGHT_* constants. As such, the returned value does
not necessarily match any of the predefined constants.

* SINCE
version 2.12.92
