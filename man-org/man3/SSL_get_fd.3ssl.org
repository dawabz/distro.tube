#+TITLE: Manpages - SSL_get_fd.3ssl
#+DESCRIPTION: Linux manpage for SSL_get_fd.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_get_fd, SSL_get_rfd, SSL_get_wfd - get file descriptor linked to an
SSL object

* SYNOPSIS
#include <openssl/ssl.h> int SSL_get_fd(const SSL *ssl); int
SSL_get_rfd(const SSL *ssl); int SSL_get_wfd(const SSL *ssl);

* DESCRIPTION
*SSL_get_fd()* returns the file descriptor which is linked to *ssl*.
*SSL_get_rfd()* and *SSL_get_wfd()* return the file descriptors for the
read or the write channel, which can be different. If the read and the
write channel are different, *SSL_get_fd()* will return the file
descriptor of the read channel.

* RETURN VALUES
The following return values can occur:

- -1 :: The operation failed, because the underlying BIO is not of the
  correct type (suitable for file descriptors).

- >=0 :: The file descriptor linked to *ssl*.

* SEE ALSO
*SSL_set_fd* (3), *ssl* (7) , *bio* (7)

* COPYRIGHT
Copyright 2000-2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
