#+TITLE: Manpages - fmodf.3
#+DESCRIPTION: Linux manpage for fmodf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about fmodf.3 is found in manpage for: [[../man3/fmod.3][man3/fmod.3]]