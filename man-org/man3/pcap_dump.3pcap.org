#+TITLE: Manpages - pcap_dump.3pcap
#+DESCRIPTION: Linux manpage for pcap_dump.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_dump - write a packet to a capture file

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  void pcap_dump(u_char *user, struct pcap_pkthdr *h,
  u_char *sp);
#+end_example

* DESCRIPTION
*pcap_dump*() outputs a packet to the ``savefile'' opened with
*pcap_dump_open*(3PCAP). Note that its calling arguments are suitable
for use with *pcap_dispatch*(3PCAP) or *pcap_loop*(3PCAP). If called
directly, the /user/ parameter is of type *pcap_dumper_t* as returned by
*pcap_dump_open*().

* SEE ALSO
*pcap*(3PCAP)
