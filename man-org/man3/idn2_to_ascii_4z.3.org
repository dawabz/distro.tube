#+TITLE: Manpages - idn2_to_ascii_4z.3
#+DESCRIPTION: Linux manpage for idn2_to_ascii_4z.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
idn2_to_ascii_4z - API function

* SYNOPSIS
*#include <idn2.h>*

*int idn2_to_ascii_4z(const uint32_t * */input/*, char ** */output/*,
int */flags/*);*

* ARGUMENTS
- const uint32_t * input :: zero terminated input Unicode (UCS-4)
  string.

- char ** output :: pointer to newly allocated zero-terminated output
  string.

- int flags :: optional *idn2_flags* to modify behaviour.

* DESCRIPTION
Convert UCS-4 domain name to ASCII string using the IDNA2008 rules. The
domain name may contain several labels, separated by dots. The output
buffer must be deallocated by the caller.

The default behavior of this function (when flags are zero) is to apply
the IDNA2008 rules without the TR46 amendments. As the TR46
non-transitional processing is nowadays ubiquitous, when unsure, it is
recommended to call this function with the *IDN2_NONTRANSITIONAL* and
the *IDN2_NFC_INPUT* flags for compatibility with other software.

Return value: Returns *IDN2_OK* on success, or error code.

* SINCE
2.0.0

* SEE ALSO
The full documentation for *libidn2* is maintained as a Texinfo manual.
If the *info* and *libidn2* programs are properly installed at your
site, the command

#+begin_quote
  *info libidn2*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *https://www.gnu.org/software/libidn/libidn2/manual/*
#+end_quote
