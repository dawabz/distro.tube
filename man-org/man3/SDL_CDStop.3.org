#+TITLE: Manpages - SDL_CDStop.3
#+DESCRIPTION: Linux manpage for SDL_CDStop.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CDStop - Stops a CDROM

* SYNOPSIS
*#include "SDL.h"*

*int SDL_CDStop*(*SDL_CD *cdrom*);

* DESCRIPTION
Stops play on the given *cdrom*.

* RETURN VALUE
Returns *0* on success, or *-1* on an error.

* SEE ALSO
*SDL_CDPlay*,
