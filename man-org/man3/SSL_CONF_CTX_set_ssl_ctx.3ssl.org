#+TITLE: Manpages - SSL_CONF_CTX_set_ssl_ctx.3ssl
#+DESCRIPTION: Linux manpage for SSL_CONF_CTX_set_ssl_ctx.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_CONF_CTX_set_ssl_ctx, SSL_CONF_CTX_set_ssl - set context to
configure

* SYNOPSIS
#include <openssl/ssl.h> void SSL_CONF_CTX_set_ssl_ctx(SSL_CONF_CTX
*cctx, SSL_CTX *ctx); void SSL_CONF_CTX_set_ssl(SSL_CONF_CTX *cctx, SSL
*ssl);

* DESCRIPTION
*SSL_CONF_CTX_set_ssl_ctx()* sets the context associated with *cctx* to
the *SSL_CTX* structure *ctx*. Any previous *SSL* or *SSL_CTX*
associated with *cctx* is cleared. Subsequent calls to *SSL_CONF_cmd()*
will be sent to *ctx*.

*SSL_CONF_CTX_set_ssl()* sets the context associated with *cctx* to the
*SSL* structure *ssl*. Any previous *SSL* or *SSL_CTX* associated with
*cctx* is cleared. Subsequent calls to *SSL_CONF_cmd()* will be sent to
*ssl*.

* NOTES
The context need not be set or it can be set to *NULL* in which case
only syntax checking of commands is performed, where possible.

* RETURN VALUES
*SSL_CONF_CTX_set_ssl_ctx()* and *SSL_CTX_set_ssl()* do not return a
value.

* SEE ALSO
*SSL_CONF_CTX_new* (3), *SSL_CONF_CTX_set_flags* (3),
*SSL_CONF_CTX_set1_prefix* (3), *SSL_CONF_cmd* (3),
*SSL_CONF_cmd_argv* (3)

* HISTORY
These functions were added in OpenSSL 1.0.2.

* COPYRIGHT
Copyright 2012-2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
