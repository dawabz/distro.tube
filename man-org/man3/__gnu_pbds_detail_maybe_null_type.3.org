#+TITLE: Manpages - __gnu_pbds_detail_maybe_null_type.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_detail_maybe_null_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::detail::maybe_null_type< Key, Mapped, _Alloc, Store_Hash > -
Base class for conditionally defining a static data member.

* SYNOPSIS
\\

=#include <types_traits.hpp>=

Inherited by *__gnu_pbds::detail::types_traits< Key, Mapped, _Alloc,
false >*, and *__gnu_pbds::detail::types_traits< Key, Mapped, _Alloc,
Store_Hash >*.

* Detailed Description
** "template<typename Key, typename Mapped, typename _Alloc, bool
Store_Hash>
\\
struct __gnu_pbds::detail::maybe_null_type< Key, Mapped, _Alloc,
Store_Hash >"Base class for conditionally defining a static data member.

Definition at line *121* of file *types_traits.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
