#+TITLE: Manpages - sd_bus_message_get_member.3
#+DESCRIPTION: Linux manpage for sd_bus_message_get_member.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sd_bus_message_get_member.3 is found in manpage for: [[../sd_bus_message_set_destination.3][sd_bus_message_set_destination.3]]