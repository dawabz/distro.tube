#+TITLE: Manpages - xcb_xfixes_create_region_from_picture.3
#+DESCRIPTION: Linux manpage for xcb_xfixes_create_region_from_picture.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_xfixes_create_region_from_picture -

* SYNOPSIS
*#include <xcb/xfixes.h>*

** Request function
xcb_void_cookie_t
*xcb_xfixes_create_region_from_picture*(xcb_connection_t */conn/,
xcb_xfixes_region_t /region/, xcb_render_picture_t /picture/);\\

* REQUEST ARGUMENTS
- conn :: The XCB connection to X11.

- region :: TODO: NOT YET DOCUMENTED.

- picture :: TODO: NOT YET DOCUMENTED.

* DESCRIPTION
* RETURN VALUE
Returns an /xcb_void_cookie_t/. Errors (if any) have to be handled in
the event loop.

If you want to handle errors directly with /xcb_request_check/ instead,
use /xcb_xfixes_create_region_from_picture_checked/. See
*xcb-requests(3)* for details.

* ERRORS
This request does never generate any errors.

* SEE ALSO
* AUTHOR
Generated from xfixes.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
