#+TITLE: Manpages - CURLINFO_RESPONSE_CODE.3
#+DESCRIPTION: Linux manpage for CURLINFO_RESPONSE_CODE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLINFO_RESPONSE_CODE - get the last response code

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_RESPONSE_CODE, long
*codep);

* DESCRIPTION
Pass a pointer to a long to receive the last received HTTP, FTP or SMTP
response code. This option was previously known as CURLINFO_HTTP_CODE in
libcurl 7.10.7 and earlier. The stored value will be zero if no server
response code has been received. Note that a proxy's CONNECT response
should be read with /CURLINFO_HTTP_CONNECTCODE(3)/ and not this.

Support for SMTP responses added in 7.25.0.

* PROTOCOLS
HTTP, FTP and SMTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    CURLcode res;
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
    res = curl_easy_perform(curl);
    if(res == CURLE_OK) {
      long response_code;
      curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
    }
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.10.8. CURLINFO_HTTP_CODE was added in 7.4.1.

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*curl_easy_getinfo*(3), *curl_easy_setopt*(3),
*CURLINFO_HTTP_CONNECTCODE*(3),
