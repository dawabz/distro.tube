#+TITLE: Man3 - Z
#+DESCRIPTION: Man3 - Z
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* Z
#+begin_src bash :exports results
readarray -t starts_with_z < <(find . -type f -iname "z*" | sort)

for x in "${starts_with_z[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./zip_add.3.org][zip_add.3]]                                   |
| [[file:./zip_add_dir.3.org][zip_add_dir.3]]                               |
| [[file:./zip_close.3.org][zip_close.3]]                                 |
| [[file:./zip_compression_method_supported.3.org][zip_compression_method_supported.3]]          |
| [[file:./zip_delete.3.org][zip_delete.3]]                                |
| [[file:./zip_dir_add.3.org][zip_dir_add.3]]                               |
| [[file:./zip_discard.3.org][zip_discard.3]]                               |
| [[file:./zip_encryption_method_supported.3.org][zip_encryption_method_supported.3]]           |
| [[file:./zip_error_clear.3.org][zip_error_clear.3]]                           |
| [[file:./zip_error_code_system.3.org][zip_error_code_system.3]]                     |
| [[file:./zip_error_code_zip.3.org][zip_error_code_zip.3]]                        |
| [[file:./zip_error_fini.3.org][zip_error_fini.3]]                            |
| [[file:./zip_error_get.3.org][zip_error_get.3]]                             |
| [[file:./zip_error_get_sys_type.3.org][zip_error_get_sys_type.3]]                    |
| [[file:./zip_error_init.3.org][zip_error_init.3]]                            |
| [[file:./zip_error_init_with_code.3.org][zip_error_init_with_code.3]]                  |
| [[file:./zip_errors.3.org][zip_errors.3]]                                |
| [[file:./zip_error_set.3.org][zip_error_set.3]]                             |
| [[file:./zip_error_strerror.3.org][zip_error_strerror.3]]                        |
| [[file:./zip_error_system_type.3.org][zip_error_system_type.3]]                     |
| [[file:./zip_error_to_data.3.org][zip_error_to_data.3]]                         |
| [[file:./zip_error_to_str.3.org][zip_error_to_str.3]]                          |
| [[file:./zip_fclose.3.org][zip_fclose.3]]                                |
| [[file:./zip_fdopen.3.org][zip_fdopen.3]]                                |
| [[file:./zip_file_add.3.org][zip_file_add.3]]                              |
| [[file:./zip_file_attributes_init.3.org][zip_file_attributes_init.3]]                  |
| [[file:./zip_file_error_clear.3.org][zip_file_error_clear.3]]                      |
| [[file:./zip_file_error_get.3.org][zip_file_error_get.3]]                        |
| [[file:./zip_file_extra_field_delete.3.org][zip_file_extra_field_delete.3]]               |
| [[file:./zip_file_extra_field_delete_by_id.3.org][zip_file_extra_field_delete_by_id.3]]         |
| [[file:./zip_file_extra_field_get.3.org][zip_file_extra_field_get.3]]                  |
| [[file:./zip_file_extra_field_get_by_id.3.org][zip_file_extra_field_get_by_id.3]]            |
| [[file:./zip_file_extra_fields_count.3.org][zip_file_extra_fields_count.3]]               |
| [[file:./zip_file_extra_fields_count_by_id.3.org][zip_file_extra_fields_count_by_id.3]]         |
| [[file:./zip_file_extra_field_set.3.org][zip_file_extra_field_set.3]]                  |
| [[file:./zip_file_get_comment.3.org][zip_file_get_comment.3]]                      |
| [[file:./zip_file_get_error.3.org][zip_file_get_error.3]]                        |
| [[file:./zip_file_get_external_attributes.3.org][zip_file_get_external_attributes.3]]          |
| [[file:./zip_file_rename.3.org][zip_file_rename.3]]                           |
| [[file:./zip_file_replace.3.org][zip_file_replace.3]]                          |
| [[file:./zip_file_set_comment.3.org][zip_file_set_comment.3]]                      |
| [[file:./zip_file_set_dostime.3.org][zip_file_set_dostime.3]]                      |
| [[file:./zip_file_set_encryption.3.org][zip_file_set_encryption.3]]                   |
| [[file:./zip_file_set_external_attributes.3.org][zip_file_set_external_attributes.3]]          |
| [[file:./zip_file_set_mtime.3.org][zip_file_set_mtime.3]]                        |
| [[file:./zip_file_strerror.3.org][zip_file_strerror.3]]                         |
| [[file:./zip_fopen.3.org][zip_fopen.3]]                                 |
| [[file:./zip_fopen_encrypted.3.org][zip_fopen_encrypted.3]]                       |
| [[file:./zip_fopen_index.3.org][zip_fopen_index.3]]                           |
| [[file:./zip_fopen_index_encrypted.3.org][zip_fopen_index_encrypted.3]]                 |
| [[file:./zip_fread.3.org][zip_fread.3]]                                 |
| [[file:./zip_fseek.3.org][zip_fseek.3]]                                 |
| [[file:./zip_ftell.3.org][zip_ftell.3]]                                 |
| [[file:./zip_get_archive_comment.3.org][zip_get_archive_comment.3]]                   |
| [[file:./zip_get_archive_flag.3.org][zip_get_archive_flag.3]]                      |
| [[file:./zip_get_error.3.org][zip_get_error.3]]                             |
| [[file:./zip_get_file_comment.3.org][zip_get_file_comment.3]]                      |
| [[file:./zip_get_name.3.org][zip_get_name.3]]                              |
| [[file:./zip_get_num_entries.3.org][zip_get_num_entries.3]]                       |
| [[file:./zip_get_num_files.3.org][zip_get_num_files.3]]                         |
| [[file:./zip_libzip_version.3.org][zip_libzip_version.3]]                        |
| [[file:./zip_name_locate.3.org][zip_name_locate.3]]                           |
| [[file:./zip_open.3.org][zip_open.3]]                                  |
| [[file:./zip_open_from_source.3.org][zip_open_from_source.3]]                      |
| [[file:./zip_register_cancel_callback_with_state.3.org][zip_register_cancel_callback_with_state.3]]   |
| [[file:./zip_register_progress_callback.3.org][zip_register_progress_callback.3]]            |
| [[file:./zip_register_progress_callback_with_state.3.org][zip_register_progress_callback_with_state.3]] |
| [[file:./zip_rename.3.org][zip_rename.3]]                                |
| [[file:./zip_replace.3.org][zip_replace.3]]                               |
| [[file:./zip_set_archive_comment.3.org][zip_set_archive_comment.3]]                   |
| [[file:./zip_set_archive_flag.3.org][zip_set_archive_flag.3]]                      |
| [[file:./zip_set_default_password.3.org][zip_set_default_password.3]]                  |
| [[file:./zip_set_file_comment.3.org][zip_set_file_comment.3]]                      |
| [[file:./zip_set_file_compression.3.org][zip_set_file_compression.3]]                  |
| [[file:./zip_source.3.org][zip_source.3]]                                |
| [[file:./zip_source_begin_write.3.org][zip_source_begin_write.3]]                    |
| [[file:./zip_source_begin_write_cloning.3.org][zip_source_begin_write_cloning.3]]            |
| [[file:./zip_source_buffer.3.org][zip_source_buffer.3]]                         |
| [[file:./zip_source_buffer_create.3.org][zip_source_buffer_create.3]]                  |
| [[file:./zip_source_buffer_fragment.3.org][zip_source_buffer_fragment.3]]                |
| [[file:./zip_source_buffer_fragment_create.3.org][zip_source_buffer_fragment_create.3]]         |
| [[file:./zip_source_close.3.org][zip_source_close.3]]                          |
| [[file:./zip_source_commit_write.3.org][zip_source_commit_write.3]]                   |
| [[file:./zip_source_error.3.org][zip_source_error.3]]                          |
| [[file:./zip_source_file.3.org][zip_source_file.3]]                           |
| [[file:./zip_source_file_create.3.org][zip_source_file_create.3]]                    |
| [[file:./zip_source_filep.3.org][zip_source_filep.3]]                          |
| [[file:./zip_source_filep_create.3.org][zip_source_filep_create.3]]                   |
| [[file:./zip_source_free.3.org][zip_source_free.3]]                           |
| [[file:./zip_source_function.3.org][zip_source_function.3]]                       |
| [[file:./zip_source_function_create.3.org][zip_source_function_create.3]]                |
| [[file:./ZIP_SOURCE_GET_ARGS.3.org][ZIP_SOURCE_GET_ARGS.3]]                       |
| [[file:./zip_source_is_deleted.3.org][zip_source_is_deleted.3]]                     |
| [[file:./zip_source_keep.3.org][zip_source_keep.3]]                           |
| [[file:./zip_source_make_command_bitmap.3.org][zip_source_make_command_bitmap.3]]            |
| [[file:./zip_source_open.3.org][zip_source_open.3]]                           |
| [[file:./zip_source_read.3.org][zip_source_read.3]]                           |
| [[file:./zip_source_rollback_write.3.org][zip_source_rollback_write.3]]                 |
| [[file:./zip_source_seek.3.org][zip_source_seek.3]]                           |
| [[file:./zip_source_seek_compute_offset.3.org][zip_source_seek_compute_offset.3]]            |
| [[file:./zip_source_seek_write.3.org][zip_source_seek_write.3]]                     |
| [[file:./zip_source_stat.3.org][zip_source_stat.3]]                           |
| [[file:./zip_source_tell.3.org][zip_source_tell.3]]                           |
| [[file:./zip_source_tell_write.3.org][zip_source_tell_write.3]]                     |
| [[file:./zip_source_win32a.3.org][zip_source_win32a.3]]                         |
| [[file:./zip_source_win32a_create.3.org][zip_source_win32a_create.3]]                  |
| [[file:./zip_source_win32handle.3.org][zip_source_win32handle.3]]                    |
| [[file:./zip_source_win32handle_create.3.org][zip_source_win32handle_create.3]]             |
| [[file:./zip_source_win32w.3.org][zip_source_win32w.3]]                         |
| [[file:./zip_source_win32w_create.3.org][zip_source_win32w_create.3]]                  |
| [[file:./zip_source_window.3.org][zip_source_window.3]]                         |
| [[file:./zip_source_write.3.org][zip_source_write.3]]                          |
| [[file:./zip_source_zip.3.org][zip_source_zip.3]]                            |
| [[file:./zip_stat.3.org][zip_stat.3]]                                  |
| [[file:./zip_stat_index.3.org][zip_stat_index.3]]                            |
| [[file:./zip_stat_init.3.org][zip_stat_init.3]]                             |
| [[file:./zip_strerror.3.org][zip_strerror.3]]                              |
| [[file:./zip_unchange.3.org][zip_unchange.3]]                              |
| [[file:./zip_unchange_all.3.org][zip_unchange_all.3]]                          |
| [[file:./zip_unchange_archive.3.org][zip_unchange_archive.3]]                      |
| [[file:./zlib.3.org][zlib.3]]                                      |
| [[file:./zmq_atomic_counter_dec.3.org][zmq_atomic_counter_dec.3]]                    |
| [[file:./zmq_atomic_counter_destroy.3.org][zmq_atomic_counter_destroy.3]]                |
| [[file:./zmq_atomic_counter_inc.3.org][zmq_atomic_counter_inc.3]]                    |
| [[file:./zmq_atomic_counter_new.3.org][zmq_atomic_counter_new.3]]                    |
| [[file:./zmq_atomic_counter_set.3.org][zmq_atomic_counter_set.3]]                    |
| [[file:./zmq_atomic_counter_value.3.org][zmq_atomic_counter_value.3]]                  |
| [[file:./zmq_bind.3.org][zmq_bind.3]]                                  |
| [[file:./zmq_close.3.org][zmq_close.3]]                                 |
| [[file:./zmq_connect.3.org][zmq_connect.3]]                               |
| [[file:./zmq_connect_peer.3.org][zmq_connect_peer.3]]                          |
| [[file:./zmq_ctx_get.3.org][zmq_ctx_get.3]]                               |
| [[file:./zmq_ctx_new.3.org][zmq_ctx_new.3]]                               |
| [[file:./zmq_ctx_set.3.org][zmq_ctx_set.3]]                               |
| [[file:./zmq_ctx_shutdown.3.org][zmq_ctx_shutdown.3]]                          |
| [[file:./zmq_ctx_term.3.org][zmq_ctx_term.3]]                              |
| [[file:./zmq_curve_keypair.3.org][zmq_curve_keypair.3]]                         |
| [[file:./zmq_curve_public.3.org][zmq_curve_public.3]]                          |
| [[file:./zmq_disconnect.3.org][zmq_disconnect.3]]                            |
| [[file:./zmq_errno.3.org][zmq_errno.3]]                                 |
| [[file:./zmq_getsockopt.3.org][zmq_getsockopt.3]]                            |
| [[file:./zmq_has.3.org][zmq_has.3]]                                   |
| [[file:./zmq_msg_close.3.org][zmq_msg_close.3]]                             |
| [[file:./zmq_msg_copy.3.org][zmq_msg_copy.3]]                              |
| [[file:./zmq_msg_data.3.org][zmq_msg_data.3]]                              |
| [[file:./zmq_msg_get.3.org][zmq_msg_get.3]]                               |
| [[file:./zmq_msg_gets.3.org][zmq_msg_gets.3]]                              |
| [[file:./zmq_msg_init.3.org][zmq_msg_init.3]]                              |
| [[file:./zmq_msg_init_buffer.3.org][zmq_msg_init_buffer.3]]                       |
| [[file:./zmq_msg_init_data.3.org][zmq_msg_init_data.3]]                         |
| [[file:./zmq_msg_init_size.3.org][zmq_msg_init_size.3]]                         |
| [[file:./zmq_msg_more.3.org][zmq_msg_more.3]]                              |
| [[file:./zmq_msg_move.3.org][zmq_msg_move.3]]                              |
| [[file:./zmq_msg_recv.3.org][zmq_msg_recv.3]]                              |
| [[file:./zmq_msg_routing_id.3.org][zmq_msg_routing_id.3]]                        |
| [[file:./zmq_msg_send.3.org][zmq_msg_send.3]]                              |
| [[file:./zmq_msg_set.3.org][zmq_msg_set.3]]                               |
| [[file:./zmq_msg_set_routing_id.3.org][zmq_msg_set_routing_id.3]]                    |
| [[file:./zmq_msg_size.3.org][zmq_msg_size.3]]                              |
| [[file:./zmq_poll.3.org][zmq_poll.3]]                                  |
| [[file:./zmq_poller.3.org][zmq_poller.3]]                                |
| [[file:./zmq_proxy.3.org][zmq_proxy.3]]                                 |
| [[file:./zmq_proxy_steerable.3.org][zmq_proxy_steerable.3]]                       |
| [[file:./zmq_recv.3.org][zmq_recv.3]]                                  |
| [[file:./zmq_recvmsg.3.org][zmq_recvmsg.3]]                               |
| [[file:./zmq_send.3.org][zmq_send.3]]                                  |
| [[file:./zmq_send_const.3.org][zmq_send_const.3]]                            |
| [[file:./zmq_sendmsg.3.org][zmq_sendmsg.3]]                               |
| [[file:./zmq_setsockopt.3.org][zmq_setsockopt.3]]                            |
| [[file:./zmq_socket.3.org][zmq_socket.3]]                                |
| [[file:./zmq_socket_monitor.3.org][zmq_socket_monitor.3]]                        |
| [[file:./zmq_socket_monitor_versioned.3.org][zmq_socket_monitor_versioned.3]]              |
| [[file:./zmq_strerror.3.org][zmq_strerror.3]]                              |
| [[file:./zmq_timers.3.org][zmq_timers.3]]                                |
| [[file:./zmq_unbind.3.org][zmq_unbind.3]]                                |
| [[file:./zmq_version.3.org][zmq_version.3]]                               |
| [[file:./zmq_z85_decode.3.org][zmq_z85_decode.3]]                            |
| [[file:./zmq_z85_encode.3.org][zmq_z85_encode.3]]                            |
