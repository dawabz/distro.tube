#+TITLE: Manpages - ldns_sign_public.3
#+DESCRIPTION: Linux manpage for ldns_sign_public.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_sign_public - sign an rrset

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_rr_list* ldns_sign_public(ldns_rr_list *rrset, ldns_key_list
*keys);

* DESCRIPTION
/ldns_sign_public/() Sign an rrset .br *rrset*: the rrset .br *keys*:
the keys to use .br Returns a rr_list with the signatures

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_sign_public_dsa/, /ldns_sign_public_rsamd5/,
/ldns_sign_public_rsasha1/, /ldns_verify/, /ldns_verify_rrsig/,
/ldns_key/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*,
*RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
