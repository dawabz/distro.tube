#+TITLE: Manpages - FcConfigUptoDate.3
#+DESCRIPTION: Linux manpage for FcConfigUptoDate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigUptoDate - Check timestamps on config files

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcConfigUptoDate (FcConfig */config/*);*

* DESCRIPTION
Checks all of the files related to /config/ and returns whether any of
them has been modified since the configuration was created. If /config/
is NULL, the current configuration is used.
