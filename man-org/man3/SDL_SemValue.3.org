#+TITLE: Manpages - SDL_SemValue.3
#+DESCRIPTION: Linux manpage for SDL_SemValue.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_SemValue - Return the current value of a semaphore.

* SYNOPSIS
*#include "SDL.h"* #include "SDL/SDL_thread.h"

*Uint32 SDL_SemValue*(*SDL_sem *sem*);

* DESCRIPTION
*SDL_SemValue()* returns the current semaphore value from the semaphore
pointed to by *sem*.

* RETURN VALUE
Returns current value of the semaphore.

* EXAMPLES
#+begin_example
    sem_value = SDL_SemValue(my_sem);
#+end_example

* SEE ALSO
*SDL_CreateSemaphore*, *SDL_DestroySemaphore*, *SDL_SemWait*,
*SDL_SemTryWait*, *SDL_SemWaitTimeout*, *SDL_SemPost*
