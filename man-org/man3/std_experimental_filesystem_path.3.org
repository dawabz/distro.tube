#+TITLE: Manpages - std_experimental_filesystem_path.3
#+DESCRIPTION: Linux manpage for std_experimental_filesystem_path.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::experimental::filesystem::path - A filesystem path.

* SYNOPSIS
\\

=#include <fs_path.h>=

** Classes
class *iterator*\\
An iterator for the components of a path.

** Public Types
typedef *iterator* *const_iterator*\\

typedef *std::basic_string*< value_type > *string_type*\\

typedef char *value_type*\\

** Public Member Functions
template<typename _InputIterator , typename _Require =
__detail::_Path<_InputIterator, _InputIterator>> *path* (_InputIterator
__first, _InputIterator __last)\\

template<typename _InputIterator , typename _Require =
__detail::_Path<_InputIterator, _InputIterator>, typename _Require2 =
__detail::__value_type_is_char<_InputIterator>> *path* (_InputIterator
__first, _InputIterator __last, const *locale* &__loc)\\

template<typename _Source , typename _Require =
__detail::_Path<_Source>> *path* (_Source const &__source)\\

template<typename _Source , typename _Require =
__detail::_Path<_Source>, typename _Require2 =
__detail::__value_type_is_char<_Source>> *path* (_Source const
&__source, const *locale* &__loc)\\

*path* (const *path* &__p)=default\\

*path* (*path* &&__p) noexcept\\

*path* (*string_type* &&__source)\\

template<typename _InputIterator > __detail::_Path< _InputIterator,
_InputIterator > & *append* (_InputIterator __first, _InputIterator
__last)\\

template<typename _Source > __detail::_Path< _Source > & *append*
(_Source const &__source)\\

template<typename _InputIterator > __detail::_Path< _InputIterator,
_InputIterator > & *assign* (_InputIterator __first, _InputIterator
__last)\\

template<typename _Source > __detail::_Path< _Source > & *assign*
(_Source const &__source)\\

*path* & *assign* (*string_type* &&__source)\\

*iterator* *begin* () const\\

const value_type * *c_str* () const noexcept\\

void *clear* () noexcept\\

int *compare* (const *basic_string_view*< value_type > __s) const\\

int *compare* (const *path* &__p) const noexcept\\

int *compare* (const *string_type* &__s) const\\

int *compare* (const value_type *__s) const\\

template<typename _InputIterator > __detail::_Path< _InputIterator,
_InputIterator > & *concat* (_InputIterator __first, _InputIterator
__last)\\

template<typename _Source > __detail::_Path< _Source > & *concat*
(_Source const &__x)\\

bool *empty* () const noexcept\\

*iterator* *end* () const\\

*path* *extension* () const\\

*path* *filename* () const\\

*std::string* *generic_string* () const\\

template<typename _CharT , typename _Traits = std::char_traits<_CharT>,
typename _Allocator = std::allocator<_CharT>> *std::basic_string*<
_CharT, _Traits, _Allocator > *generic_string* (const _Allocator
&__a=_Allocator()) const\\

*std::u16string* *generic_u16string* () const\\

*std::u32string* *generic_u32string* () const\\

*std::string* *generic_u8string* () const\\

*std::wstring* *generic_wstring* () const\\

bool *has_extension* () const\\

bool *has_filename* () const\\

bool *has_parent_path* () const\\

bool *has_relative_path* () const\\

bool *has_root_directory* () const\\

bool *has_root_name* () const\\

bool *has_root_path* () const\\

bool *has_stem* () const\\

bool *is_absolute* () const\\

bool *is_relative* () const\\

*path* & *make_preferred* ()\\

const *string_type* & *native* () const noexcept\\

*operator string_type* () const\\

template<typename _CharT > __detail::_Path< _CharT *, _CharT * > &
*operator+=* (_CharT __x)\\

template<typename _Source > __detail::_Path< _Source > & *operator+=*
(_Source const &__x)\\

*path* & *operator+=* (*basic_string_view*< value_type > __x)\\

*path* & *operator+=* (const *path* &__x)\\

*path* & *operator+=* (const *string_type* &__x)\\

*path* & *operator+=* (const value_type *__x)\\

*path* & *operator+=* (value_type __x)\\

template<typename _Source > __detail::_Path< _Source > & *operator/=*
(_Source const &__source)\\

*path* & *operator/=* (const *path* &__p)\\

template<typename _Source > __detail::_Path< _Source > & *operator=*
(_Source const &__source)\\

*path* & *operator=* (const *path* &__p)=default\\

*path* & *operator=* (*path* &&__p) noexcept\\

*path* & *operator=* (*string_type* &&__source)\\

*path* *parent_path* () const\\

*path* *relative_path* () const\\

*path* & *remove_filename* ()\\

*path* & *replace_extension* (const *path* &__replacement=*path*())\\

*path* & *replace_filename* (const *path* &__replacement)\\

*path* *root_directory* () const\\

*path* *root_name* () const\\

*path* *root_path* () const\\

*path* *stem* () const\\

*std::string* *string* () const\\

template<typename _CharT , typename _Traits = std::char_traits<_CharT>,
typename _Allocator = std::allocator<_CharT>> *std::basic_string*<
_CharT, _Traits, _Allocator > *string* (const _Allocator
&__a=_Allocator()) const\\

void *swap* (*path* &__rhs) noexcept\\

*std::u16string* *u16string* () const\\

*std::u32string* *u32string* () const\\

*std::string* *u8string* () const\\

*std::wstring* *wstring* () const\\

** Static Public Attributes
static constexpr value_type *preferred_separator*\\

* Detailed Description
A filesystem path.

Definition at line *195* of file *experimental/bits/fs_path.h*.

* Member Typedef Documentation
** typedef *iterator*
*std::experimental::filesystem::path::const_iterator*
Definition at line *420* of file *experimental/bits/fs_path.h*.

** typedef *std::basic_string*<value_type>
*std::experimental::filesystem::path::string_type*
Definition at line *205* of file *experimental/bits/fs_path.h*.

** typedef char std::experimental::filesystem::path::value_type
Definition at line *202* of file *experimental/bits/fs_path.h*.

* Constructor & Destructor Documentation
** std::experimental::filesystem::path::path ()= [inline]=,
= [noexcept]=
Definition at line *209* of file *experimental/bits/fs_path.h*.

** std::experimental::filesystem::path::path (*path* && __p)= [inline]=,
= [noexcept]=
Definition at line *213* of file *experimental/bits/fs_path.h*.

** std::experimental::filesystem::path::path (*string_type* &&
__source)= [inline]=
Definition at line *221* of file *experimental/bits/fs_path.h*.

** template<typename _Source , typename _Require =
__detail::_Path<_Source>> std::experimental::filesystem::path::path
(_Source const & __source)= [inline]=
Definition at line *227* of file *experimental/bits/fs_path.h*.

** template<typename _InputIterator , typename _Require =
__detail::_Path<_InputIterator, _InputIterator>>
std::experimental::filesystem::path::path (_InputIterator __first,
_InputIterator __last)= [inline]=
Definition at line *234* of file *experimental/bits/fs_path.h*.

** template<typename _Source , typename _Require =
__detail::_Path<_Source>, typename _Require2 =
__detail::__value_type_is_char<_Source>>
std::experimental::filesystem::path::path (_Source const & __source,
const *locale* & __loc)= [inline]=
Definition at line *241* of file *experimental/bits/fs_path.h*.

** template<typename _InputIterator , typename _Require =
__detail::_Path<_InputIterator, _InputIterator>, typename _Require2 =
__detail::__value_type_is_char<_InputIterator>>
std::experimental::filesystem::path::path (_InputIterator __first,
_InputIterator __last, const *locale* & __loc)= [inline]=
Definition at line *249* of file *experimental/bits/fs_path.h*.

* Member Function Documentation
** template<typename _InputIterator > __detail::_Path< _InputIterator,
_InputIterator > & std::experimental::filesystem::path::append
(_InputIterator __first, _InputIterator __last)= [inline]=
Definition at line *296* of file *experimental/bits/fs_path.h*.

** template<typename _Source > __detail::_Path< _Source > &
std::experimental::filesystem::path::append (_Source const &
__source)= [inline]=
Definition at line *288* of file *experimental/bits/fs_path.h*.

** template<typename _InputIterator > __detail::_Path< _InputIterator,
_InputIterator > & std::experimental::filesystem::path::assign
(_InputIterator __first, _InputIterator __last)= [inline]=
Definition at line *274* of file *experimental/bits/fs_path.h*.

** template<typename _Source > __detail::_Path< _Source > &
std::experimental::filesystem::path::assign (_Source const &
__source)= [inline]=
Definition at line *269* of file *experimental/bits/fs_path.h*.

** const value_type * std::experimental::filesystem::path::c_str ()
const= [inline]=, = [noexcept]=
Definition at line *344* of file *experimental/bits/fs_path.h*.

** void std::experimental::filesystem::path::clear ()= [inline]=,
= [noexcept]=
Definition at line *332* of file *experimental/bits/fs_path.h*.

** template<typename _InputIterator > __detail::_Path< _InputIterator,
_InputIterator > & std::experimental::filesystem::path::concat
(_InputIterator __first, _InputIterator __last)= [inline]=
Definition at line *327* of file *experimental/bits/fs_path.h*.

** template<typename _Source > __detail::_Path< _Source > &
std::experimental::filesystem::path::concat (_Source const &
__x)= [inline]=
Definition at line *319* of file *experimental/bits/fs_path.h*.

** bool std::experimental::filesystem::path::empty () const= [inline]=,
= [noexcept]=
Definition at line *406* of file *experimental/bits/fs_path.h*.

** bool std::experimental::filesystem::path::is_relative ()
const= [inline]=
Definition at line *416* of file *experimental/bits/fs_path.h*.

** const *string_type* & std::experimental::filesystem::path::native ()
const= [inline]=, = [noexcept]=
Definition at line *343* of file *experimental/bits/fs_path.h*.

** std::experimental::filesystem::path::operator *string_type* ()
const= [inline]=
Definition at line *345* of file *experimental/bits/fs_path.h*.

** template<typename _Source > __detail::_Path< _Source > &
std::experimental::filesystem::path::operator+= (_Source const &
__x)= [inline]=
Definition at line *311* of file *experimental/bits/fs_path.h*.

** template<typename _Source > __detail::_Path< _Source > &
std::experimental::filesystem::path::operator/= (_Source const &
__source)= [inline]=
Definition at line *283* of file *experimental/bits/fs_path.h*.

** *path* & std::experimental::filesystem::path::operator/= (const
*path* & __p)= [inline]=
Definition at line *279* of file *experimental/bits/fs_path.h*.

** template<typename _Source > __detail::_Path< _Source > &
std::experimental::filesystem::path::operator= (_Source const &
__source)= [inline]=
Definition at line *264* of file *experimental/bits/fs_path.h*.

* Member Data Documentation
** constexpr value_type
std::experimental::filesystem::path::preferred_separator= [static]=,
= [constexpr]=
Definition at line *203* of file *experimental/bits/fs_path.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
