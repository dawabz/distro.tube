#+TITLE: Manpages - Text_Abbrev.3perl
#+DESCRIPTION: Linux manpage for Text_Abbrev.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Text::Abbrev - abbrev - create an abbreviation table from a list

* SYNOPSIS
use Text::Abbrev; abbrev $hashref, LIST

* DESCRIPTION
Stores all unambiguous truncations of each element of LIST as keys in
the associative array referenced by =$hashref=. The values are the
original list elements.

* EXAMPLE
$hashref = abbrev qw(list edit send abort gripe); %hash = abbrev qw(list
edit send abort gripe); abbrev $hashref, qw(list edit send abort gripe);
abbrev(*hash, qw(list edit send abort gripe));
