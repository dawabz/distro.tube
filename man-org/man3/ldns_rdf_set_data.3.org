#+TITLE: Manpages - ldns_rdf_set_data.3
#+DESCRIPTION: Linux manpage for ldns_rdf_set_data.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_rdf_set_size, ldns_rdf_set_type, ldns_rdf_set_data - set rdf
attributes

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

void ldns_rdf_set_size(ldns_rdf *rd, size_t size);

void ldns_rdf_set_type(ldns_rdf *rd, ldns_rdf_type type);

void ldns_rdf_set_data(ldns_rdf *rd, void *data);

* DESCRIPTION
/ldns_rdf_set_size/() sets the size of the rdf. .br **rd*: the rdf to
operate on .br *size*: the new size .br Returns void

/ldns_rdf_set_type/() sets the size of the rdf. .br **rd*: the rdf to
operate on .br *type*: the new type .br Returns void

/ldns_rdf_set_data/() sets the size of the rdf. .br **rd*: the rdf to
operate on .br **data*: pointer to the new data .br Returns void

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_rdf/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*,
*RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
