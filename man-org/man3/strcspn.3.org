#+TITLE: Manpages - strcspn.3
#+DESCRIPTION: Linux manpage for strcspn.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about strcspn.3 is found in manpage for: [[../man3/strspn.3][man3/strspn.3]]