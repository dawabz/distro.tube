#+TITLE: Manpages - TAP_Formatter_File.3perl
#+DESCRIPTION: Linux manpage for TAP_Formatter_File.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
TAP::Formatter::File - Harness output delegate for file output

* VERSION
Version 3.43

* DESCRIPTION
This provides file orientated output formatting for TAP::Harness.

* SYNOPSIS
use TAP::Formatter::File; my $harness = TAP::Formatter::File->new(
\%args );

** "open_test"
See TAP::Formatter::Base
