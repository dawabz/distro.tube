#+TITLE: Manpages - CURLMOPT_PIPELINING_SITE_BL.3
#+DESCRIPTION: Linux manpage for CURLMOPT_PIPELINING_SITE_BL.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLMOPT_PIPELINING_SITE_BL - pipelining host block list

* SYNOPSIS
#include <curl/curl.h>

CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_PIPELINING_SITE_BL,
char **hosts);

* DESCRIPTION
No function since pipelining was removed in 7.62.0.

Pass a *hosts* array of char *, ending with a NULL entry. This is a list
of sites that are blocked from pipelining, i.e sites that are known to
not support HTTP pipelining. The array is copied by libcurl.

Pass a NULL pointer to clear the block list.

* DEFAULT
The default value is NULL, which means that there is no block list.

* PROTOCOLS
HTTP(S)

* EXAMPLE
#+begin_example
    char *site_block_list[] =
    {
      "www.haxx.se",
      "www.example.com:1234",
      NULL
    };

    curl_multi_setopt(m, CURLMOPT_PIPELINING_SITE_BL, site_block_list);
#+end_example

* AVAILABILITY
Added in 7.30.0

* RETURN VALUE
Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLMOPT_PIPELINING*(3), *CURLMOPT_PIPELINING_SERVER_BL*(3),
