#+TITLE: Manpages - editline.3
#+DESCRIPTION: Linux manpage for editline.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The

library provides generic line editing, history and tokenization
functions, similar to those found in

These functions are available in the

library (which needs the

library). Programs should be linked with

The

library respects the

locale set by the application program and never uses

to change the locale.

The line editing functions use a common data structure,

which is created by

or

and freed by

The wide-character functions behave the same way as their narrow
counterparts.

The following functions are available:

Initialize the line editor, and return a data structure to be used by
all other line editing functions, or

on failure.

is the name of the invoking program, used when reading the

file to determine which settings to use.

and

are the input, output, and error streams (respectively) to use. In this
documentation, references to

are actually to this input/output stream combination.

Like

but allows specifying file descriptors for the

corresponding streams, in case those were created with

Clean up and finish with

assumed to have been created with

or

Reset the tty and the parser. This should be called after an error which
may have upset the tty's state.

Read a line from the tty.

is modified to contain the number of characters read. Returns the line
read if successful, or

if no characters were read or if an error occurred. If an error
occurred,

is set to -1 and

contains the error code that caused it. The return value may not remain
valid across calls to

and must be copied if the data is to be retained.

Read a wide character from the tty, respecting the current locale, or
from the input queue described in

if that is not empty, and store it in

If an invalid or incomplete character is found, it is discarded,

is set to

and the next character is read and stored in

Returns 1 if a valid character was read, 0 on end of file, or -1 on

failure. In the latter case,

is set to indicate the error.

Read a wide character as described for

and return 0 on end of file or -1 on failure. If the wide character can
be represented as a single-byte character, convert it with

store the result in

and return 1; otherwise, set

to

and return -1. In the C or POSIX locale, this simply reads a byte, but
for any other locale, including UTF-8, this is rarely useful.

Push the wide character string

back onto the input queue described in

If the queue overflows, for example due to a recursive macro, or if an
error occurs, for example because

is

or memory allocation fails, the function beeps at the user, but does not
report the problem to the caller.

Use the current locale to convert the multibyte string

to a wide character string, and pass the result to

Parses the

array (which is

elements in size) to execute builtin

commands. If the command is prefixed with

then

will only execute the command if

matches the

argument supplied to

The return value is -1 if the command is unknown, 0 if there was no
error or

didn't match, or 1 if the command returned an error. Refer to

for more information.

Set

parameters.

determines which parameter to set, and each operation has its own
parameter list. Returns 0 on success, -1 on failure.

The following values for

are supported, along with the required argument list:

Define prompt printing function as

which is to return a string that contains the prompt.

Same as

but the

argument indicates the start/stop literal prompt character.

If a start/stop literal character is found in the prompt, the character
itself is not printed, but characters after it are printed directly to
the terminal without affecting the state of the current line. A
subsequent second start/stop literal character ends this behavior. This
is typically used to embed literal escape sequences that change the
color/style of the terminal in the prompt. Note that the literal escape
character cannot be the last character in the prompt, as the escape
sequence is attached to the next character in the prompt.

unsets it.

Re-display the current line on the next terminal line.

Define right side prompt printing function as

which is to return a string that contains the prompt.

Define the right prompt printing function but with a literal escape
character.

Define terminal type of the tty to be

or to

if

is

Set editing mode to

which must be one of

or

If

is non-zero,

will install its own signal handler for the following signals when
reading command input:

and

Otherwise, the current signal handlers will be used.

Perform the

builtin command. Refer to

for more information.

Perform the

builtin command. Refer to

for more information.

Perform the

builtin command. Refer to

for more information.

Perform the

builtin command. Refer to

for more information.

Perform the

builtin command. Refer to

for more information.

Add a user defined function,

referred to as

which is invoked when a key which is bound to

is entered.

is a description of

At invocation time,

is the key which caused the invocation. The return value of

should be one of:

Add a normal character.

End of line was entered.

EOF was entered.

Expecting further command input as arguments, do nothing visually.

Refresh display.

Refresh display, and beep.

Cursor moved, so update and perform

Redisplay entire input line. This is useful if a key binding outputs
extra information.

An error occurred. Beep, and flush tty.

Fatal error, reset tty to known state.

Defines which history function to use, which is usually

should be the value returned by

If

is non-zero, editing is enabled (the default). Note that this is only an
indication, and does not affect the operation of

At this time, it is the caller's responsibility to check this (using

to determine if editing should be enabled or not.

If

is zero, unbuffered mode is disabled (the default). In unbuffered mode,

will return immediately after processing a single character.

If the

argument is non-zero, then

attempts to recover from read errors, ignoring the first interrrupted
error, and trying to reset the input file descriptor to reset
non-blocking I/O. This is disabled by default, and desirable only when

is used in shell-like applications.

Whenever reading a character, use the function

which stores the character in

and returns 1 on success, 0 on end of file, or -1 on I/O or encoding
errors. Functions internally using it include

and

Initially, a builtin function is installed, and replacing it is
discouraged because writing such a function is very error prone. The
builtin function can be restored at any time by passing the special
value

instead of a function pointer.

Register

to be associated with this EditLine structure. It can be retrieved with
the corresponding

call.

Set the current

file pointer for

=

=

or

=

from

Get

parameters.

determines which parameter to retrieve into

Returns 0 if successful, -1 otherwise.

The following values for

are supported, along with actual type of

Set

to a pointer to the function that displays the prompt. If

is not

set it to the start/stop literal prompt character.

Set

to a pointer to the function that displays the prompt. If

is not

set it to the start/stop literal prompt character.

Set the name of the editor in

which will be one of

or

If

is a valid

capability set

to the current value of that capability.

Set

to non-zero if

has installed private signal handlers (see

above).

Set

to non-zero if editing is enabled.

Set

to a pointer to the function that reads characters, or to

if the builtin function is in use.

Set

to the previously registered client data set by an

call.

Set

to non-zero if unbuffered mode is enabled.

Set

to non-zero if safe read is set.

Set

to the current

file pointer for

=

=

or

=

Initialize

by reading the contents of

is called for each line in

If

is

try

and if that is not set

Refer to

for details on the format of

returns 0 on success and -1 on error.

Must be called if the terminal size changes. If

has been set with

then this is done automatically. Otherwise, it's the responsibility of
the application to call

on the appropriate occasions.

Move the cursor to the right (if positive) or to the left (if negative)

characters. Returns the resulting offset of the cursor from the
beginning of the line.

Return the editing information for the current line in a

structure, which is defined as follows:

typedef struct lineinfo { const char *buffer; /* address of buffer */
const char *cursor; /* address of cursor */ const char *lastchar; /*
address of last character */ } LineInfo;

is not NUL terminated. This function may be called after

to obtain the

structure pertaining to line returned by that function, and from within
user defined functions added with

Insert

into the line at the cursor. Returns -1 if

is empty or won't fit, and 0 otherwise.

Delete

characters before the cursor.

The history functions use a common data structure,

which is created by

and freed by

The following functions are available:

Initialize the history list, and return a data structure to be used by
all other history list functions, or

on failure.

Clean up and finish with

assumed to have been created with

Perform operation

on the history list, with optional arguments as needed by the operation.

is changed accordingly to operation. The following values for

are supported, along with the required argument list:

Set size of history to

elements.

Get number of events currently in history.

Cleans up and finishes with

assumed to be created with

Clear the history.

Define functions to perform various history operations.

is the argument given to a function when it's invoked.

Return the first element in the history.

Return the last element in the history.

Return the previous element in the history. It is newer than the current
one.

Return the next element in the history. It is older than the current
one.

Return the current element in the history.

Set the cursor to point to the requested element.

Append

to the current element of the history, or perform the

operation with argument

if there is no current element.

Append

to the last new element of the history.

Add

as a new element to the history and, if necessary, removing the oldest
entry to keep the list to the created size. If

has been called with a non-zero argument, the element will not be
entered into the history if its contents match the ones of the current
history element. If the element is entered

returns 1; if it is ignored as a duplicate returns 0. Finally

returns -1 if an error occurred.

Return the closest previous event that starts with

Return the closest next event that starts with

Return the previous event numbered

Return the next event numbered

Load the history list stored in

Save the history list to

Save the history list to the opened

pointer

Save the last

history entries to the opened

pointer

Set flag that adjacent identical event strings should not be entered
into the history.

Retrieve the current setting if adjacent identical elements should be
entered into the history.

Delete the event numbered

This function is only provided for

compatibility. The caller is responsible for free'ing the string in the
returned

returns >= 0 if the operation

succeeds. Otherwise, -1 is returned and

is updated to contain more details about the error.

The tokenization functions use a common data structure,

which is created by

and freed by

The following functions are available:

Initialize the tokenizer, and return a data structure to be used by all
other tokenizer functions.

contains the Input Field Separators, which defaults to

and

if

Clean up and finish with

assumed to have been created with

Reset the tokenizer state. Use after a line has been successfully
tokenized by

or

and before a new line is to be tokenized.

Tokenize

If successful, modify:

to contain the words,

to contain the number of words,

(if not

to contain the index of the word containing the cursor, and

(if not

to contain the offset within

of the cursor.

Returns 0 if successful, -1 for an internal error, 1 for an unmatched
single quote, 2 for an unmatched double quote, and 3 for a backslash
quoted

A positive exit code indicates that another line should be read and
tokenization attempted again.

A simpler form of

is a NUL terminated string to tokenize.

The

library first appeared in

appeared in

and the readline emulation appeared in

appeared in

The

library was written by

wrote this manual and implemented

and

implemented the readline emulation.

implemented wide-character support.

At this time, it is the responsibility of the caller to check the result
of the

operation of

(after an

or

to determine if

should be used for further input. I.e.,

is purely an indication of the result of the most recent

command.
