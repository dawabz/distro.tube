#+TITLE: Manpages - FcDirCacheUnlink.3
#+DESCRIPTION: Linux manpage for FcDirCacheUnlink.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcDirCacheUnlink - Remove all caches related to dir

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcDirCacheUnlink (const FcChar8 */dir/*, FcConfig **/config/*);*

* DESCRIPTION
Scans the cache directories in /config/, removing any instances of the
cache file for /dir/. Returns FcFalse when some internal error occurs
(out of memory, etc). Errors actually unlinking any files are ignored.
