#+TITLE: Manpages - afInitChannels.3
#+DESCRIPTION: Linux manpage for afInitChannels.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about afInitChannels.3 is found in manpage for: [[../afInitSampleFormat.3][afInitSampleFormat.3]]