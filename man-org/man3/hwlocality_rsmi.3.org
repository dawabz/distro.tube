#+TITLE: Manpages - hwlocality_rsmi.3
#+DESCRIPTION: Linux manpage for hwlocality_rsmi.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwlocality_rsmi - Interoperability with the ROCm SMI Management Library

* SYNOPSIS
\\

** Functions
static int *hwloc_rsmi_get_device_cpuset* (*hwloc_topology_t* topology,
uint32_t dv_ind, *hwloc_cpuset_t* set)\\

static *hwloc_obj_t* *hwloc_rsmi_get_device_osdev_by_index*
(*hwloc_topology_t* topology, uint32_t dv_ind)\\

static *hwloc_obj_t* *hwloc_rsmi_get_device_osdev* (*hwloc_topology_t*
topology, uint32_t dv_ind)\\

* Detailed Description
This interface offers ways to retrieve topology information about
devices managed by the ROCm SMI Management Library.

* Function Documentation
** static int hwloc_rsmi_get_device_cpuset (*hwloc_topology_t* topology,
uint32_t dv_ind, *hwloc_cpuset_t* set)= [inline]=, = [static]=
Get the CPU set of logical processors that are physically close to AMD
GPU device whose index is =dv_ind=. Store in =set= the CPU-set
describing the locality of the AMD GPU device whose index is =dv_ind=.

Topology =topology= and device =dv_ind= must match the local machine.
I/O devices detection and the ROCm SMI component are not needed in the
topology.

The function only returns the locality of the device. If more
information about the device is needed, OS objects should be used
instead, see *hwloc_rsmi_get_device_osdev()* and
*hwloc_rsmi_get_device_osdev_by_index()*.

This function is currently only implemented in a meaningful way for
Linux; other systems will simply get a full cpuset.

** static *hwloc_obj_t* hwloc_rsmi_get_device_osdev (*hwloc_topology_t*
topology, uint32_t dv_ind)= [inline]=, = [static]=
Get the hwloc OS device object corresponding to AMD GPU device, whose
index is =dv_ind=.

*Returns*

#+begin_quote
  The hwloc OS device object that describes the given AMD GPU, whose
  index is =dv_ind=.

  =NULL= if none could be found.
#+end_quote

Topology =topology= and device =dv_ind= must match the local machine.
I/O devices detection and the ROCm SMI component must be enabled in the
topology. If not, the locality of the object may still be found using
*hwloc_rsmi_get_device_cpuset()*.

*Note*

#+begin_quote
  The corresponding hwloc PCI device may be found by looking at the
  result parent pointer (unless PCI devices are filtered out).
#+end_quote

** static *hwloc_obj_t* hwloc_rsmi_get_device_osdev_by_index
(*hwloc_topology_t* topology, uint32_t dv_ind)= [inline]=, = [static]=
Get the hwloc OS device object corresponding to the AMD GPU device whose
index is =dv_ind=.

*Returns*

#+begin_quote
  The hwloc OS device object describing the AMD GPU device whose index
  is =dv_ind=.

  =NULL= if none could be found.
#+end_quote

The topology =topology= does not necessarily have to match the current
machine. For instance the topology may be an XML import of a remote
host. I/O devices detection and the ROCm SMI component must be enabled
in the topology.

*Note*

#+begin_quote
  The corresponding PCI device object can be obtained by looking at the
  OS device parent object (unless PCI devices are filtered out).
#+end_quote

* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
