#+TITLE: Manpages - Test_Alien_Synthetic.3pm
#+DESCRIPTION: Linux manpage for Test_Alien_Synthetic.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test::Alien::Synthetic - A mock alien object for testing

* VERSION
version 2.44

* SYNOPSIS
use Test2::V0; use Test::Alien; my $alien = synthetic { cflags =>
-I/foo/bar/include, libs => -L/foo/bar/lib -lbaz, }; alien_ok $alien;
done_testing;

* DESCRIPTION
This class is used to model a synthetic Alien class that implements the
minimum Alien::Base interface needed by Test::Alien.

It can be useful if you have a non-Alien::Base based Alien distribution
that you need to test.

*NOTE*: The name of this class may move in the future, so do not refer
to this class name directly. Instead create instances of this class
using the Test::Alien#synthetic function.

* ATTRIBUTES
** cflags
String containing the compiler flags

** cflags_static
String containing the static compiler flags

** libs
String containing the linker and library flags

** libs_static
String containing the static linker and library flags

** dynamic_libs
List reference containing the dynamic libraries.

** bin_dir
Tool binary directory.

** runtime_prop
Runtime properties.

* EXAMPLE
Here is a complete example using Alien::Libarchive which is a
non-Alien::Base based Alien distribution.

use strict; use warnings; use Test2::V0; use Test::Alien; use
Alien::Libarchive; my $real = Alien::Libarchive->new; my $alien =
synthetic { cflags => scalar $real->cflags, libs => scalar $real->libs,
dynamic_libs => [$real->dlls], }; alien_ok $alien; xs_ok do { local $/;
<DATA> }, with_subtest { my($module) = @_; my $ptr =
$module->archive_read_new; like $ptr, qr{^[0-9]+$};
$module->archive_read_free($ptr); }; ffi_ok { symbols => [qw(
archive_read_new )] }, with_subtest { my($ffi) = @_; my $new =
$ffi->function(archive_read_new => [] => opaque); my $free =
$ffi->function(archive_read_close => [opaque] => void); my $ptr =
$new->(); like $ptr, qr{^[0-9]+$}; $free->($ptr); }; done_testing;
_ _DATA_ _ #include "EXTERN.h" #include "perl.h" #include "XSUB.h"
#include <archive.h> MODULE = TA_MODULE PACKAGE = TA_MODULE void
*archive_read_new(class); const char *class; CODE: RETVAL = (void*)
archive_read_new(); OUTPUT: RETVAL void archive_read_free(class, ptr);
const char *class; void *ptr; CODE: archive_read_free(ptr);

* SEE ALSO
- Test::Alien :: 

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
