#+TITLE: Manpages - pcre2_code_copy.3
#+DESCRIPTION: Linux manpage for pcre2_code_copy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  pcre2_code *pcre2_code_copy(const pcre2_code *code);
#+end_example

* DESCRIPTION
This function makes a copy of the memory used for a compiled pattern,
excluding any memory used by the JIT compiler. Without a subsequent call
to *pcre2_jit_compile()*, the copy can be used only for non-JIT
matching. The pointer to the character tables is copied, not the tables
themselves (see *pcre2_code_copy_with_tables()*). The yield of the
function is NULL if /code/ is NULL or if sufficient memory cannot be
obtained.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
