#+TITLE: Manpages - std_negative_binomial_distribution_param_type.3
#+DESCRIPTION: Linux manpage for std_negative_binomial_distribution_param_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::negative_binomial_distribution< _IntType >::param_type

* SYNOPSIS
\\

=#include <random.h>=

** Public Types
typedef *negative_binomial_distribution*< _IntType >
*distribution_type*\\

** Public Member Functions
*param_type* (_IntType __k, double __p=0.5)\\

_IntType *k* () const\\

double *p* () const\\

** Friends
bool *operator!=* (const *param_type* &__p1, const *param_type* &__p2)\\

bool *operator==* (const *param_type* &__p1, const *param_type* &__p2)\\

* Detailed Description
** "template<typename _IntType = int>
\\
struct std::negative_binomial_distribution< _IntType
>::param_type"Parameter type.

Definition at line *4200* of file *random.h*.

* Member Typedef Documentation
** template<typename _IntType = int> typedef
*negative_binomial_distribution*<_IntType>
*std::negative_binomial_distribution*< _IntType
>::*param_type::distribution_type*
Definition at line *4202* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _IntType = int>
*std::negative_binomial_distribution*< _IntType
>::param_type::param_type ()= [inline]=
Definition at line *4204* of file *random.h*.

** template<typename _IntType = int>
*std::negative_binomial_distribution*< _IntType
>::param_type::param_type (_IntType __k, double __p = =0.5=)= [inline]=,
= [explicit]=
Definition at line *4207* of file *random.h*.

* Member Function Documentation
** template<typename _IntType = int> _IntType
*std::negative_binomial_distribution*< _IntType >::param_type::k ()
const= [inline]=
Definition at line *4214* of file *random.h*.

** template<typename _IntType = int> double
*std::negative_binomial_distribution*< _IntType >::param_type::p ()
const= [inline]=
Definition at line *4218* of file *random.h*.

* Friends And Related Function Documentation
** template<typename _IntType = int> bool operator!= (const *param_type*
& __p1, const *param_type* & __p2)= [friend]=
Definition at line *4226* of file *random.h*.

** template<typename _IntType = int> bool operator== (const *param_type*
& __p1, const *param_type* & __p2)= [friend]=
Definition at line *4222* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
