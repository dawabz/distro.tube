#+TITLE: Manpages - xcb_xv_port_notify_event_t.3
#+DESCRIPTION: Linux manpage for xcb_xv_port_notify_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_xv_port_notify_event_t -

* SYNOPSIS
*#include <xcb/xv.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_xv_port_notify_event_t {
      uint8_t         response_type;
      uint8_t         pad0;
      uint16_t        sequence;
      xcb_timestamp_t time;
      xcb_xv_port_t   port;
      xcb_atom_t      attribute;
      int32_t         value;
  } xcb_xv_port_notify_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_XV_PORT_NOTIFY/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- time :: NOT YET DOCUMENTED.

- port :: NOT YET DOCUMENTED.

- attribute :: NOT YET DOCUMENTED.

- value :: NOT YET DOCUMENTED.

* DESCRIPTION
* SEE ALSO
* AUTHOR
Generated from xv.xml. Contact xcb@lists.freedesktop.org for corrections
and improvements.
