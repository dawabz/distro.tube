#+TITLE: Manpages - XkbNoteDeviceChanges.3
#+DESCRIPTION: Linux manpage for XkbNoteDeviceChanges.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbNoteDeviceChanges - Note device changes reported in an
XkbExtensionDeviceNotify event

* SYNOPSIS
*void XkbNoteDeviceChanges* *( XkbDeviceChangesPtr */old/* ,*
*XkbExtensionDeviceNotifyEvent **/new/* ,* *unsigned int */wanted/* );*

* ARGUMENTS
- /- old/ :: structure tracking state changes

- /- new/ :: event indicating state changes

- /- wanted/ :: mask indicating changes to note

* DESCRIPTION
The /wanted/ field specifies the changes that should be noted in /old,/
and is composed of the bitwise inclusive OR of one or more of the masks
from Table 1. The /reason/ field of the event in /new/ indicates the
types of changes the event is reporting. /XkbNoteDeviceChanges/ updates
the XkbDeviceChangesRec specified by /old/ with the changes that are
both specified in /wanted/ and contained in /new->reason./

#+begin_example
                           Table 1 XkbDeviceInfoRec Mask Bits                        
  ____________________________________________________________________________________
  Name                         XkbDeviceInfoRec Value     Capability If Set
                               Fields Effected            
  ____________________________________________________________________________________
  XkbXI_KeyboardsMask                           (1L <<0) Clients can use all
                                                         Xkb requests and events
                                                         with KeyClass devices 
                                                         supported by the input
                                                         device extension.

  XkbXI_ButtonActionsMask       num_btns        (1L <<1) Clients can assign key 
                                btn_acts                 actions to buttons 
                                                         non-KeyClass input
                                                         extension devices.
                             
  XkbXI_IndicatorNamesMask      leds->names     (1L <<2) Clients can assign
                                                         names to indicators on
                                                         non-KeyClass input
                                                         extension devices.
                                                               
  XkbXI_IndicatorMapsMask       leds->maps      (1L <<3) Clients can assign
                                                         indicator maps to
                                                         indicators on 
                                                         non-KeyClass input
                                                         extension devices.

  XkbXI_IndicatorStateMask      leds->state     (1L <<4) Clients can request 
                                                         the status of indicators
                                                         on non-KeyClass input
                                                         extension devices.

  XkbXI_IndicatorsMask          sz_leds         (0x1c)   XkbXI_IndicatorNamesMask |
                                num_leds                 XkbXI_IndicatorMapsMask |
                                leds->*                  XkbXI_IndicatorStateMask

  XkbXI_UnsupportedFeaturesMask unsupported     (1L <<15)

  XkbXI_AllDeviceFeaturesMask   Those selected  (0x1e)   XkbXI_IndicatorsMask |
                                by Value Column          XkbSI_ButtonActionsMask
                                masks 

  XkbXI_AllFeaturesMask         Those selected  (0x1f)   XkbSI_AllDeviceFeaturesMask |
                                by Value Column          XkbSI_KeyboardsMask
                                masks

  XkbXI_AllDetailsMask          Those selected  (0x801f) XkbXI_AllFeaturesMask |
                                by Value column          XkbXI_UnsupportedFeaturesMask
                                masks
#+end_example

To update a local copy of the state and configuration of an X input
extension device with the changes previously noted in an
XkbDeviceChangesRec structure, use /XkbGetDeviceInfoChanges./

* STRUCTURES
Changes to an Xkb extension device may be tracked by listening to
XkbDeviceExtensionNotify events and accumulating the changes in an
XkbDeviceChangesRec structure. The changes noted in the structure may
then be used in subsequent operations to update either a server
configuration or a local copy of an Xkb extension device configuration.
The changes structure is defined as follows:

#+begin_example

  typedef struct _XkbDeviceChanges {
      unsigned int         changed;       /* bits indicating what has changed */
      unsigned short       first_btn;     /* number of first button which changed, if any */
      unsigned short       num_btns;      /* number of buttons that have changed */
      XkbDeviceLedChangesRec leds;
  } XkbDeviceChangesRec,*XkbDeviceChangesPtr;
#+end_example

* SEE ALSO
*XkbGetDeviceInfoChanges*(3)
