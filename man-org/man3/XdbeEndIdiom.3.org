#+TITLE: Manpages - XdbeEndIdiom.3
#+DESCRIPTION: Linux manpage for XdbeEndIdiom.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XdbeEndIdiom - marks the end of a DBE idiom sequence.

* SYNOPSIS
#include <X11/extensions/Xdbe.h>

Status XdbeEndIdiom( Display *dpy)

* DESCRIPTION
This function marks the end of an idiom sequence.

* SEE ALSO
DBE, /XdbeAllocateBackBufferName(),/ /XdbeBeginIdiom(),/
/XdbeDeallocateBackBufferName(),/ /XdbeFreeVisualInfo(),/
/XdbeGetBackBufferAttributes(),/ /XdbeGetVisualInfo(),/
/XdbeQueryExtension(),/ /XdbeSwapBuffers()./
