#+TITLE: Manpages - CURLOPT_PROXYUSERPWD.3
#+DESCRIPTION: Linux manpage for CURLOPT_PROXYUSERPWD.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_PROXYUSERPWD - user name and password to use for proxy
authentication

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXYUSERPWD, char
*userpwd);

* DESCRIPTION
Pass a char * as parameter, which should be [user name]:[password] to
use for the connection to the HTTP proxy. Both the name and the password
will be URL decoded before use, so to include for example a colon in the
user name you should encode it as %3A. (This is different to how
/CURLOPT_USERPWD(3)/ is used - beware.)

Use /CURLOPT_PROXYAUTH(3)/ to specify the authentication method.

The application does not have to keep the string around after setting
this option.

* DEFAULT
This is NULL by default.

* PROTOCOLS
Used with all protocols that can use a proxy

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
    curl_easy_setopt(curl, CURLOPT_PROXY, "http://localhost:8080");
    curl_easy_setopt(curl, CURLOPT_PROXYUSERPWD, "clark%20kent:superman");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Always

* RETURN VALUE
Returns CURLE_OK if proxies are supported, CURLE_UNKNOWN_OPTION if not,
or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_PROXY*(3), *CURLOPT_PROXYTYPE*(3),
