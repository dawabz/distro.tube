#+TITLE: Manpages - udev_enumerate_new.3
#+DESCRIPTION: Linux manpage for udev_enumerate_new.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
udev_enumerate_new, udev_enumerate_ref, udev_enumerate_unref - Create,
acquire and release a udev enumerate object

* SYNOPSIS
#+begin_example
  #include <libudev.h>
#+end_example

*struct udev_enumerate *udev_enumerate_new(struct udev **/udev/*);*

*struct udev_enumerate *udev_enumerate_ref(struct udev_enumerate
**/udev_enumerate/*);*

*struct udev_enumerate *udev_enumerate_unref(struct udev_enumerate
**/udev_enumerate/*);*

* RETURN VALUE
On success, *udev_enumerate_new()* returns a pointer to the allocated
udev monitor. On failure, *NULL* is returned. *udev_enumerate_ref()*
returns the argument that it was passed, unmodified.
*udev_enumerate_unref()* always returns *NULL*.

* SEE ALSO
*udev_new*(3), *udev_device_new_from_syspath*(3),
*udev_enumerate_add_match_subsystem*(3),
*udev_enumerate_scan_devices*(3), *udev_monitor_new_from_netlink*(3),
*udev_list_entry*(3), *systemd*(1),
