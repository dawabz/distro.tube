#+TITLE: Manpages - capng_apply_caps_fd.3
#+DESCRIPTION: Linux manpage for capng_apply_caps_fd.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
capng_apply_caps_fd -

* SYNOPSIS
*#include <cap-ng.h>*

int capng_apply_caps_fd(int fd);

* DESCRIPTION
This function will write the file based capabilities to the extended
attributes of the file that the descriptor was opened against. The
bounding set is not included in file based capabilities operations. Note
that this function will only work if compiled on a kernel that supports
file based capabilities such as 2.6.2 6 and later.

* RETURN VALUE
This returns 0 on success, -1 if something besides a regular file is
passed, and -2 if a non-root namespace id is being used for rootid.

* SEE ALSO
*capng_get_caps_fd*(3), *capabilities*(7)

* AUTHOR
Steve Grubb
