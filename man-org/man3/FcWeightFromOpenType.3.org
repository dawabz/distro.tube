#+TITLE: Manpages - FcWeightFromOpenType.3
#+DESCRIPTION: Linux manpage for FcWeightFromOpenType.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcWeightFromOpenType - Convert from OpenType weight values to fontconfig
ones

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

int FcWeightFromOpenType (int/ot_weight/*);*

* DESCRIPTION
*FcWeightFromOpenType* is like *FcWeightFromOpenTypeDouble* but with
integer arguments. Use the other function instead.

* SINCE
version 2.11.91
