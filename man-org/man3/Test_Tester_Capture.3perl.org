#+TITLE: Manpages - Test_Tester_Capture.3perl
#+DESCRIPTION: Linux manpage for Test_Tester_Capture.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test::Tester::Capture - Help testing test modules built with
Test::Builder

* DESCRIPTION
This is a subclass of Test::Builder that overrides many of the methods
so that they don't output anything. It also keeps track of its own set
of test results so that you can use Test::Builder based modules to
perform tests on other Test::Builder based modules.

* AUTHOR
Most of the code here was lifted straight from Test::Builder and then
had chunks removed by Fergal Daly <fergal@esatclear.ie>.

* LICENSE
Under the same license as Perl itself

See http://www.perl.com/perl/misc/Artistic.html
