#+TITLE: Manpages - acl_calc_mask.3
#+DESCRIPTION: Linux manpage for acl_calc_mask.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function calculates and sets the permissions associated with the
ACL_MASK ACL entry of the ACL referred to by

The value of the new permissions is the union of the permissions granted
by all entries of tag type ACL_GROUP, ACL_GROUP_OBJ, or ACL_USER. If the
ACL referred to by

already contains an ACL_MASK entry, its permissions are overwritten; if
it does not contain an ACL_MASK entry, one is added.

If the ACL referred to by

does not contain enough space for the new ACL entry, then additional
working storage may be allocated. If the working storage cannot be
increased in the current location, then it may be relocated and the
previous working storage is released and a pointer to the new working
storage is returned via

The order of existing entries in the ACL is undefined after this
function.

Any existing ACL entry descriptors that refer to entries in the ACL
continue to refer to those entries. Any existing ACL pointers that refer
to the ACL referred to by

continue to refer to the ACL.

If any of the following conditions occur, the

function returns

and sets

to the corresponding value:

The argument

is not a valid pointer to an ACL.

The

function is unable to allocate the memory required for an ACL_MASK ACL
entry.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
