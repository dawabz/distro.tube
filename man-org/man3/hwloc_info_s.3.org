#+TITLE: Manpages - hwloc_info_s.3
#+DESCRIPTION: Linux manpage for hwloc_info_s.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwloc_info_s

* SYNOPSIS
\\

=#include <hwloc.h>=

** Data Fields
char * *name*\\

char * *value*\\

* Detailed Description
Object info.

*See also*

#+begin_quote
  *Consulting and Adding Key-Value Info Attributes*
#+end_quote

* Field Documentation
** char* hwloc_info_s::name
Info name.

** char* hwloc_info_s::value
Info value.

* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
