#+TITLE: Manpages - offsetof.3
#+DESCRIPTION: Linux manpage for offsetof.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
offsetof - offset of a structure member

* SYNOPSIS
#+begin_example
  #include <stddef.h>

  size_t offsetof(type, member);
#+end_example

* DESCRIPTION
The macro *offsetof*() returns the offset of the field /member/ from the
start of the structure /type/.

This macro is useful because the sizes of the fields that compose a
structure can vary across implementations, and compilers may insert
different numbers of padding bytes between fields. Consequently, an
element's offset is not necessarily given by the sum of the sizes of the
previous elements.

A compiler error will result if /member/ is not aligned to a byte
boundary (i.e., it is a bit field).

* RETURN VALUE
*offsetof*() returns the offset of the given /member/ within the given
/type/, in units of bytes.

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008, C89, C99.

* EXAMPLES
On a Linux/i386 system, when compiled using the default *gcc*(1)
options, the program below produces the following output:

#+begin_example
  $ ./a.out
  offsets: i=0; c=4; d=8 a=16
  sizeof(struct s)=16
#+end_example

** Program source
#+begin_example
  #include <stddef.h>
  #include <stdio.h>
  #include <stdlib.h>

  int
  main(void)
  {
      struct s {
          int i;
          char c;
          double d;
          char a[];
      };

      /* Output is compiler dependent */

      printf("offsets: i=%zu; c=%zu; d=%zu a=%zu\n",
              offsetof(struct s, i), offsetof(struct s, c),
              offsetof(struct s, d), offsetof(struct s, a));
      printf("sizeof(struct s)=%zu\n", sizeof(struct s));

      exit(EXIT_SUCCESS);
  }
#+end_example

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
