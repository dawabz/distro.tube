#+TITLE: Manpages - std__Mu.3
#+DESCRIPTION: Linux manpage for std__Mu.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Mu< _Arg, _IsBindExp, _IsPlaceholder >

* SYNOPSIS
\\

* Detailed Description
** "template<typename _Arg, bool _IsBindExp =
is_bind_expression<_Arg>::value, bool _IsPlaceholder =
(is_placeholder<_Arg>::value > 0)>
\\
class std::_Mu< _Arg, _IsBindExp, _IsPlaceholder >"Maps an argument to
bind() into an actual argument to the bound function object
[func.bind.bind]/10. Only the first parameter should be specified: the
rest are used to determine among the various implementations. Note that,
although this class is a function object, it isn't entirely normal
because it takes only two parameters regardless of the number of
parameters passed to the bind expression. The first parameter is the
bound argument and the second parameter is a tuple containing references
to the rest of the arguments.

Definition at line *292* of file *std/functional*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
