#+TITLE: Manpages - hwloc_topology_export_synthetic.3
#+DESCRIPTION: Linux manpage for hwloc_topology_export_synthetic.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about hwloc_topology_export_synthetic.3 is found in manpage for: [[../man3/hwlocality_syntheticexport.3][man3/hwlocality_syntheticexport.3]]