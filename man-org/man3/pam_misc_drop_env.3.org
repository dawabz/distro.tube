#+TITLE: Manpages - pam_misc_drop_env.3
#+DESCRIPTION: Linux manpage for pam_misc_drop_env.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_misc_drop_env - liberating a locally saved environment

* SYNOPSIS
#+begin_example
  #include <security/pam_misc.h>
#+end_example

*int pam_misc_drop_env(char ***/env/*);*

* DESCRIPTION
This function is defined to complement the *pam_getenvlist*(3) function.
It liberates the memory associated with /env/, /overwriting/ with /0/
all memory before *free()*ing it.

* SEE ALSO
*pam_getenvlist*(3), *pam*(8)

* STANDARDS
The *pam_misc_drop_env* function is part of the *libpam_misc* Library
and not defined in any standard.
