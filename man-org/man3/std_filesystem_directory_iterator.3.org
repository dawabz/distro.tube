#+TITLE: Manpages - std_filesystem_directory_iterator.3
#+DESCRIPTION: Linux manpage for std_filesystem_directory_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::filesystem::directory_iterator - Iterator type for traversing the
entries in a single directory.

* SYNOPSIS
\\

=#include <fs_dir.h>=

** Public Types
typedef ptrdiff_t *difference_type*\\

typedef *input_iterator_tag* *iterator_category*\\

typedef const *directory_entry* * *pointer*\\

typedef const *directory_entry* & *reference*\\

typedef *directory_entry* *value_type*\\

** Public Member Functions
*directory_iterator* (const *directory_iterator* &__rhs)=default\\

*directory_iterator* (const *path* &__p)\\

*directory_iterator* (const *path* &__p, directory_options __options)\\

*directory_iterator* (const *path* &__p, directory_options __options,
*error_code* &__ec)\\

*directory_iterator* (const *path* &__p, *error_code* &__ec)\\

*directory_iterator* (*directory_iterator* &&__rhs) noexcept=default\\

*directory_iterator* & *increment* (*error_code* &__ec)\\

const *directory_entry* & *operator** () const noexcept\\

*directory_iterator* & *operator++* ()\\

*__directory_iterator_proxy* *operator++* (int)\\

const *directory_entry* * *operator->* () const noexcept\\

*directory_iterator* & *operator=* (const *directory_iterator*
&__rhs)=default\\

*directory_iterator* & *operator=* (*directory_iterator* &&__rhs)
noexcept=default\\

** Friends
bool *operator!=* (const *directory_iterator* &__lhs, const
*directory_iterator* &__rhs) noexcept\\

bool *operator==* (const *directory_iterator* &__lhs, const
*directory_iterator* &__rhs) noexcept\\

class *recursive_directory_iterator*\\

** Related Functions
(Note that these are not member functions.)

\\

*directory_iterator* *begin* (*directory_iterator* __iter) noexcept\\
Enable range-based =for= using directory_iterator.

*directory_iterator* *end* (*directory_iterator*) noexcept\\
Return a past-the-end directory_iterator.

* Detailed Description
Iterator type for traversing the entries in a single directory.

Definition at line *374* of file *bits/fs_dir.h*.

* Member Typedef Documentation
** typedef ptrdiff_t
std::filesystem::directory_iterator::difference_type
Definition at line *378* of file *bits/fs_dir.h*.

** typedef *input_iterator_tag*
*std::filesystem::directory_iterator::iterator_category*
Definition at line *381* of file *bits/fs_dir.h*.

** typedef const *directory_entry**
*std::filesystem::directory_iterator::pointer*
Definition at line *379* of file *bits/fs_dir.h*.

** typedef const *directory_entry*&
*std::filesystem::directory_iterator::reference*
Definition at line *380* of file *bits/fs_dir.h*.

** typedef *directory_entry*
*std::filesystem::directory_iterator::value_type*
Definition at line *377* of file *bits/fs_dir.h*.

* Constructor & Destructor Documentation
** std::filesystem::directory_iterator::directory_iterator (const *path*
& __p)= [inline]=, = [explicit]=
Definition at line *386* of file *bits/fs_dir.h*.

** std::filesystem::directory_iterator::directory_iterator (const *path*
& __p, directory_options __options)= [inline]=
Definition at line *389* of file *bits/fs_dir.h*.

** std::filesystem::directory_iterator::directory_iterator (const *path*
& __p, *error_code* & __ec)= [inline]=
Definition at line *392* of file *bits/fs_dir.h*.

** std::filesystem::directory_iterator::directory_iterator (const *path*
& __p, directory_options __options, *error_code* & __ec)= [inline]=
Definition at line *395* of file *bits/fs_dir.h*.

* Member Function Documentation
** *__directory_iterator_proxy*
std::filesystem::directory_iterator::operator++ (int)= [inline]=
Definition at line *416* of file *bits/fs_dir.h*.

** const *directory_entry* *
std::filesystem::directory_iterator::operator-> () const= [inline]=,
= [noexcept]=
Definition at line *412* of file *bits/fs_dir.h*.

* Friends And Related Function Documentation
** bool operator!= (const *directory_iterator* & __lhs, const
*directory_iterator* & __rhs)= [friend]=
Definition at line *435* of file *bits/fs_dir.h*.

** bool operator== (const *directory_iterator* & __lhs, const
*directory_iterator* & __rhs)= [friend]=
Definition at line *427* of file *bits/fs_dir.h*.

** friend class *recursive_directory_iterator*= [friend]=
Definition at line *439* of file *bits/fs_dir.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
