#+TITLE: Manpages - __gnu_parallel__LoserTreePointerBase.3
#+DESCRIPTION: Linux manpage for __gnu_parallel__LoserTreePointerBase.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::_LoserTreePointerBase< _Tp, _Compare > - Base class of
_Loser Tree implementation using pointers.

* SYNOPSIS
\\

=#include <losertree.h>=

Inherited by *__gnu_parallel::_LoserTreePointer< __stable, _Tp, _Compare
>*, and *__gnu_parallel::_LoserTreePointer< false, _Tp, _Compare >*.

** Classes
struct *_Loser*\\
Internal representation of _LoserTree __elements.

** Public Member Functions
*_LoserTreePointerBase* (unsigned int __k, _Compare __comp=*std::less*<
_Tp >())\\

int *__get_min_source* ()\\

void *__insert_start* (const _Tp &__key, int __source, bool __sup)\\

** Protected Attributes
_Compare *_M_comp*\\

unsigned int *_M_ik*\\

unsigned int *_M_k*\\

*_Loser* * *_M_losers*\\

unsigned int *_M_offset*\\

* Detailed Description
** "template<typename _Tp, typename _Compare>
\\
class __gnu_parallel::_LoserTreePointerBase< _Tp, _Compare >"Base class
of _Loser Tree implementation using pointers.

Definition at line *357* of file *losertree.h*.

* Constructor & Destructor Documentation
** template<typename _Tp , typename _Compare >
*__gnu_parallel::_LoserTreePointerBase*< _Tp, _Compare
>::*_LoserTreePointerBase* (unsigned int __k, _Compare __comp =
*std::less*=<_Tp>()=*)*= [inline]=
Definition at line *373 of file losertree.h.*

** template<typename _Tp , typename _Compare >
*__gnu_parallel::_LoserTreePointerBase< _Tp, _Compare
>::~_LoserTreePointerBase ()*= [inline]=
Definition at line *387 of file losertree.h.*

* Member Function Documentation
** template<typename _Tp , typename _Compare > int
*__gnu_parallel::_LoserTreePointerBase< _Tp, _Compare
>::__get_min_source ()*= [inline]=
Definition at line *390 of file losertree.h.*

** template<typename _Tp , typename _Compare > void
*__gnu_parallel::_LoserTreePointerBase< _Tp, _Compare >::__insert_start
(const _Tp & __key, int __source, bool __sup)*= [inline]=
Definition at line *393 of file losertree.h.*

* Member Data Documentation
** template<typename _Tp , typename _Compare > _Compare
*__gnu_parallel::_LoserTreePointerBase< _Tp, _Compare
>::_M_comp*= [protected]=
Definition at line *370 of file losertree.h.*

** template<typename _Tp , typename _Compare > unsigned int
*__gnu_parallel::_LoserTreePointerBase< _Tp, _Compare
>::_M_ik*= [protected]=
Definition at line *368 of file losertree.h.*

** template<typename _Tp , typename _Compare > unsigned int
*__gnu_parallel::_LoserTreePointerBase< _Tp, _Compare
>::_M_k*= [protected]=
Definition at line *368 of file losertree.h.*

** template<typename _Tp , typename _Compare > *_Loser*
__gnu_parallel::_LoserTreePointerBase< _Tp, _Compare
>::_M_losers*= [protected]=
Definition at line *369 of file losertree.h.*

** template<typename _Tp , typename _Compare > unsigned int
*__gnu_parallel::_LoserTreePointerBase< _Tp, _Compare
>::_M_offset*= [protected]=
Definition at line *368 of file losertree.h.*

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
