#+TITLE: Manpages - pam.3
#+DESCRIPTION: Linux manpage for pam.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam - Pluggable Authentication Modules Library

* SYNOPSIS
#+begin_example
  #include <security/pam_appl.h>
#+end_example

#+begin_example
  #include <security/pam_modules.h>
#+end_example

#+begin_example
  #include <security/pam_ext.h>
#+end_example

* DESCRIPTION
*PAM* is a system of libraries that handle the authentication tasks of
applications (services) on the system. The library provides a stable
general interface (Application Programming Interface - API) that
privilege granting programs (such as *login*(1) and *su*(1)) defer to to
perform standard authentication tasks.

** Initialization and Cleanup
The *pam_start*(3) function creates the PAM context and initiates the
PAM transaction. It is the first of the PAM functions that needs to be
called by an application. The transaction state is contained entirely
within the structure identified by this handle, so it is possible to
have multiple transactions in parallel. But it is not possible to use
the same handle for different transactions, a new one is needed for
every new context.

The *pam_end*(3) function terminates the PAM transaction and is the last
function an application should call in the PAM context. Upon return the
handle pamh is no longer valid and all memory associated with it will be
invalid. It can be called at any time to terminate a PAM transaction.

** Authentication
The *pam_authenticate*(3) function is used to authenticate the user. The
user is required to provide an authentication token depending upon the
authentication service, usually this is a password, but could also be a
finger print.

The *pam_setcred*(3) function manages the users credentials.

** Account Management
The *pam_acct_mgmt*(3) function is used to determine if the users
account is valid. It checks for authentication token and account
expiration and verifies access restrictions. It is typically called
after the user has been authenticated.

** Password Management
The *pam_chauthtok*(3) function is used to change the authentication
token for a given user on request or because the token has expired.

** Session Management
The *pam_open_session*(3) function sets up a user session for a
previously successful authenticated user. The session should later be
terminated with a call to *pam_close_session*(3).

** Conversation
The PAM library uses an application-defined callback to allow a direct
communication between a loaded module and the application. This callback
is specified by the /struct pam_conv/ passed to *pam_start*(3) at the
start of the transaction. See *pam_conv*(3) for details.

** Data Objects
The *pam_set_item*(3) and *pam_get_item*(3) functions allows
applications and PAM service modules to set and retrieve PAM
information.

The *pam_get_user*(3) function is the preferred method to obtain the
username.

The *pam_set_data*(3) and *pam_get_data*(3) functions allows PAM service
modules to set and retrieve free-form data from one invocation to
another.

** Environment and Error Management
The *pam_putenv*(3), *pam_getenv*(3) and *pam_getenvlist*(3) functions
are for maintaining a set of private environment variables.

The *pam_strerror*(3) function returns a pointer to a string describing
the given PAM error code.

* RETURN VALUES
The following return codes are known by PAM:

PAM_ABORT

#+begin_quote
  Critical error, immediate abort.
#+end_quote

PAM_ACCT_EXPIRED

#+begin_quote
  User account has expired.
#+end_quote

PAM_AUTHINFO_UNAVAIL

#+begin_quote
  Authentication service cannot retrieve authentication info.
#+end_quote

PAM_AUTHTOK_DISABLE_AGING

#+begin_quote
  Authentication token aging disabled.
#+end_quote

PAM_AUTHTOK_ERR

#+begin_quote
  Authentication token manipulation error.
#+end_quote

PAM_AUTHTOK_EXPIRED

#+begin_quote
  Authentication token expired.
#+end_quote

PAM_AUTHTOK_LOCK_BUSY

#+begin_quote
  Authentication token lock busy.
#+end_quote

PAM_AUTHTOK_RECOVERY_ERR

#+begin_quote
  Authentication information cannot be recovered.
#+end_quote

PAM_AUTH_ERR

#+begin_quote
  Authentication failure.
#+end_quote

PAM_BUF_ERR

#+begin_quote
  Memory buffer error.
#+end_quote

PAM_CONV_ERR

#+begin_quote
  Conversation failure.
#+end_quote

PAM_CRED_ERR

#+begin_quote
  Failure setting user credentials.
#+end_quote

PAM_CRED_EXPIRED

#+begin_quote
  User credentials expired.
#+end_quote

PAM_CRED_INSUFFICIENT

#+begin_quote
  Insufficient credentials to access authentication data.
#+end_quote

PAM_CRED_UNAVAIL

#+begin_quote
  Authentication service cannot retrieve user credentials.
#+end_quote

PAM_IGNORE

#+begin_quote
  The return value should be ignored by PAM dispatch.
#+end_quote

PAM_MAXTRIES

#+begin_quote
  Have exhausted maximum number of retries for service.
#+end_quote

PAM_MODULE_UNKNOWN

#+begin_quote
  Module is unknown.
#+end_quote

PAM_NEW_AUTHTOK_REQD

#+begin_quote
  Authentication token is no longer valid; new one required.
#+end_quote

PAM_NO_MODULE_DATA

#+begin_quote
  No module specific data is present.
#+end_quote

PAM_OPEN_ERR

#+begin_quote
  Failed to load module.
#+end_quote

PAM_PERM_DENIED

#+begin_quote
  Permission denied.
#+end_quote

PAM_SERVICE_ERR

#+begin_quote
  Error in service module.
#+end_quote

PAM_SESSION_ERR

#+begin_quote
  Cannot make/remove an entry for the specified session.
#+end_quote

PAM_SUCCESS

#+begin_quote
  Success.
#+end_quote

PAM_SYMBOL_ERR

#+begin_quote
  Symbol not found.
#+end_quote

PAM_SYSTEM_ERR

#+begin_quote
  System error.
#+end_quote

PAM_TRY_AGAIN

#+begin_quote
  Failed preliminary check by password service.
#+end_quote

PAM_USER_UNKNOWN

#+begin_quote
  User not known to the underlying authentication module.
#+end_quote

* SEE ALSO
*pam_acct_mgmt*(3), *pam_authenticate*(3), *pam_chauthtok*(3),
*pam_close_session*(3), *pam_conv*(3), *pam_end*(3), *pam_get_data*(3),
*pam_getenv*(3), *pam_getenvlist*(3), *pam_get_item*(3),
*pam_get_user*(3), *pam_open_session*(3), *pam_putenv*(3),
*pam_set_data*(3), *pam_set_item*(3), *pam_setcred*(3), *pam_start*(3),
*pam_strerror*(3)

* NOTES
The /libpam/ interfaces are only thread-safe if each thread within the
multithreaded application uses its own PAM handle.
