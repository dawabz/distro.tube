#+TITLE: Manpages - wcsrchr.3
#+DESCRIPTION: Linux manpage for wcsrchr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
wcsrchr - search a wide character in a wide-character string

* SYNOPSIS
#+begin_example
  #include <wchar.h>

  wchar_t *wcsrchr(const wchar_t *wcs, wchar_t wc);
#+end_example

* DESCRIPTION
The *wcsrchr*() function is the wide-character equivalent of the
*strrchr*(3) function. It searches the last occurrence of /wc/ in the
wide-character string pointed to by /wcs/.

* RETURN VALUE
The *wcsrchr*() function returns a pointer to the last occurrence of
/wc/ in the wide-character string pointed to by /wcs/, or NULL if /wc/
does not occur in the string.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface   | Attribute     | Value   |
| *wcsrchr*() | Thread safety | MT-Safe |

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008, C99.

* SEE ALSO
*strrchr*(3), *wcschr*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
