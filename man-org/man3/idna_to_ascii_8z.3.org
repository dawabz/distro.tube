#+TITLE: Manpages - idna_to_ascii_8z.3
#+DESCRIPTION: Linux manpage for idna_to_ascii_8z.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
idna_to_ascii_8z - API function

* SYNOPSIS
*#include <idna.h>*

*int idna_to_ascii_8z(const char * */input/*, char ** */output/*, int
*/flags/*);*

* ARGUMENTS
- const char * input :: zero terminated input UTF-8 string.

- char ** output :: pointer to newly allocated output string.

- int flags :: an *Idna_flags* value, e.g., *IDNA_ALLOW_UNASSIGNED* or
  *IDNA_USE_STD3_ASCII_RULES*.

* DESCRIPTION
Convert UTF-8 domain name to ASCII string. The domain name may contain
several labels, separated by dots. The output buffer must be deallocated
by the caller.

Return value: Returns *IDNA_SUCCESS* on success, or error code.

* REPORTING BUGS
Report bugs to <help-libidn@gnu.org>.\\
General guidelines for reporting bugs: http://www.gnu.org/gethelp/\\
GNU Libidn home page: http://www.gnu.org/software/libidn/

* COPYRIGHT
Copyright © 2002-2021 Simon Josefsson.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libidn* is maintained as a Texinfo manual.
If the *info* and *libidn* programs are properly installed at your site,
the command

#+begin_quote
  *info libidn*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *http://www.gnu.org/software/libidn/manual/*
#+end_quote
