#+TITLE: Manpages - __gnu_parallel___multiway_merge_k_variant_sentinel_switch.3
#+DESCRIPTION: Linux manpage for __gnu_parallel___multiway_merge_k_variant_sentinel_switch.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::__multiway_merge_k_variant_sentinel_switch< __sentinels,
__stable, _RAIterIterator, _RAIter3, _DifferenceTp, _Compare > - Switch
for k-way merging with __sentinels turned on.

* SYNOPSIS
\\

=#include <multiway_merge.h>=

** Public Member Functions
_RAIter3 *operator()* (_RAIterIterator __seqs_begin, _RAIterIterator
__seqs_end, _RAIter3 __target, const typename *std::iterator_traits*<
typename *std::iterator_traits*< _RAIterIterator
>::value_type::first_type >::value_type &__sentinel, _DifferenceTp
__length, _Compare __comp)\\

* Detailed Description
** "template<bool __sentinels, bool __stable, typename _RAIterIterator,
typename _RAIter3, typename _DifferenceTp, typename _Compare>
\\
struct __gnu_parallel::__multiway_merge_k_variant_sentinel_switch<
__sentinels, __stable, _RAIterIterator, _RAIter3, _DifferenceTp,
_Compare >"Switch for k-way merging with __sentinels turned on.

Definition at line *837* of file *multiway_merge.h*.

* Member Function Documentation
** template<bool __sentinels, bool __stable, typename _RAIterIterator ,
typename _RAIter3 , typename _DifferenceTp , typename _Compare >
_RAIter3 *__gnu_parallel::__multiway_merge_k_variant_sentinel_switch*<
__sentinels, __stable, _RAIterIterator, _RAIter3, _DifferenceTp,
_Compare >::operator() (_RAIterIterator __seqs_begin, _RAIterIterator
__seqs_end, _RAIter3 __target, const typename *std::iterator_traits*<
typename *std::iterator_traits*< _RAIterIterator
>::value_type::first_type >::value_type & __sentinel, _DifferenceTp
__length, _Compare __comp)= [inline]=
Definition at line *840* of file *multiway_merge.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
