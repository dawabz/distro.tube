#+TITLE: Manpages - CURLOPT_DOH_URL.3
#+DESCRIPTION: Linux manpage for CURLOPT_DOH_URL.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_DOH_URL - provide the DNS-over-HTTPS URL

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DOH_URL, char *URL);

* DESCRIPTION
Pass in a pointer to a /URL/ for the DoH server to use for name
resolving. The parameter should be a char * to a null-terminated string
which must be URL-encoded in the following format:
"https://host:port/path". It MUST specify a HTTPS URL.

libcurl does not validate the syntax or use this variable until the
transfer is issued. Even if you set a crazy value here,
/curl_easy_setopt(3)/ will still return /CURLE_OK/.

curl sends POST requests to the given DNS-over-HTTPS URL.

To find the DoH server itself, which might be specified using a name,
libcurl will use the default name lookup function. You can bootstrap
that by providing the address for the DoH server with
/CURLOPT_RESOLVE(3)/.

Disable DoH use again by setting this option to NULL.

* INHERIT OPTIONS
DoH lookups use SSL and some SSL settings from your transfer are
inherited, like /CURLOPT_SSL_CTX_FUNCTION(3)/.

The hostname and peer certificate verification settings are not
inherited but can be controlled separately via
/CURLOPT_DOH_SSL_VERIFYHOST(3)/ and /CURLOPT_DOH_SSL_VERIFYPEER(3)/.

A set /CURLOPT_OPENSOCKETFUNCTION(3)/ callback is not inherited.

* DEFAULT
NULL - there is no default DoH URL. If this option is not set, libcurl
will use the default name resolver.

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
    curl_easy_setopt(curl, CURLOPT_DOH_URL, "https://dns.example.com");
    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.62.0

* RETURN VALUE
Returns CURLE_OK on success or CURLE_OUT_OF_MEMORY if there was
insufficient heap space.

Note that /curl_easy_setopt(3)/ will not actually parse the given string
so given a bad DoH URL, curl will not detect a problem until it tries to
resolve a name with it.

* SEE ALSO
*CURLOPT_VERBOSE*(3), *CURLOPT_RESOLVE*(3),
