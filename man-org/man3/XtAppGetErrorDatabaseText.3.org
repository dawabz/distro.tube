#+TITLE: Manpages - XtAppGetErrorDatabaseText.3
#+DESCRIPTION: Linux manpage for XtAppGetErrorDatabaseText.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtAppGetErrorDatabaseText.3 is found in manpage for: [[../man3/XtAppGetErrorDatabase.3][man3/XtAppGetErrorDatabase.3]]