#+TITLE: Manpages - termkey_interpret_position.3
#+DESCRIPTION: Linux manpage for termkey_interpret_position.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
termkey_interpret_position - interpret opaque cursor position event data

* SYNOPSIS
#+begin_example
  #include <termkey.h>

  TermKeyResult termkey_interpret_position(TermKey *tk, const TermKeyKey *key, 
   int *line, int *col);
#+end_example

Link with /-ltermkey/.

* DESCRIPTION
*termkey_interpret_position*() fills in variables in the passed pointers
according to the cursor position report event found in /key/. It should
be called if *termkey_getkey*(3) or similar have returned a key event
with the type of *TERMKEY_TYPE_POSITION*.

Any pointer may instead be given as *NULL* to not return that value.

The /line/ and /col/ variables will be filled in with the cursor
position, indexed from 1. Note that due to the limited number of bytes
in the *TermKeyKey* structure, the line and column numbers are limited
to 2047 and 4095 respectively.

* RETURN VALUE
If passed a /key/ event of the type *TERMKEY_TYPE_POSITION*, this
function will return *TERMKEY_RES_KEY* and will affect the variables
whose pointers were passed in, as described above.

For other event types it will return *TERMKEY_RES_NONE*, and its effects
on any variables whose pointers were passed in, are undefined.

* SEE ALSO
*termkey_waitkey*(3), *termkey_getkey*(3), *termkey*(7)
