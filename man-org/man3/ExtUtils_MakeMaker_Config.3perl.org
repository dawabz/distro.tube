#+TITLE: Manpages - ExtUtils_MakeMaker_Config.3perl
#+DESCRIPTION: Linux manpage for ExtUtils_MakeMaker_Config.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ExtUtils::MakeMaker::Config - Wrapper around Config.pm

* SYNOPSIS
use ExtUtils::MakeMaker::Config; print $Config{installbin}; # or
whatever

* DESCRIPTION
*FOR INTERNAL USE ONLY*

A very thin wrapper around Config.pm so MakeMaker is easier to test.
