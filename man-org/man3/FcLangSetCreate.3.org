#+TITLE: Manpages - FcLangSetCreate.3
#+DESCRIPTION: Linux manpage for FcLangSetCreate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcLangSetCreate - create a langset object

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcLangSet * FcLangSetCreate (void*);*

* DESCRIPTION
*FcLangSetCreate* creates a new FcLangSet object.
