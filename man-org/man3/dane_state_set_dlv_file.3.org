#+TITLE: Manpages - dane_state_set_dlv_file.3
#+DESCRIPTION: Linux manpage for dane_state_set_dlv_file.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
dane_state_set_dlv_file - API function

* SYNOPSIS
*#include <gnutls/dane.h>*

*int dane_state_set_dlv_file(dane_state_t */s/*, const char *
*/file/*);*

* ARGUMENTS
- dane_state_t s :: The structure to be deinitialized

- const char * file :: The file holding the DLV keys.

* DESCRIPTION
This function will set a file with trusted keys for DLV (DNSSEC
Lookaside Validation).

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
