#+TITLE: Manpages - curl_multi_cleanup.3
#+DESCRIPTION: Linux manpage for curl_multi_cleanup.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_multi_cleanup - close down a multi session

* SYNOPSIS
*#include <curl/curl.h>*

*CURLMcode curl_multi_cleanup( CURLM *multi_handle );*

* DESCRIPTION
Cleans up and removes a whole multi stack. It does not free or touch any
individual easy handles in any way - they still need to be closed
individually, using the usual /curl_easy_cleanup(3)/ way. The order of
cleaning up should be:

1 - /curl_multi_remove_handle(3)/ before any easy handles are cleaned up

2 - /curl_easy_cleanup(3)/ can now be called independently since the
easy handle is no longer connected to the multi handle

3 - /curl_multi_cleanup(3)/ should be called when all easy handles are
removed

Passing in a NULL pointer in /multi_handle/ will make this function
return CURLM_BAD_HANDLE immediately with no other action.

* EXAMPLE
#+begin_example
   /* when the multi transfer is done ... */

   /* remove all easy handles, then: */
   curl_multi_cleanup(multi_handle);
#+end_example

* AVAILABILITY
Added in 7.9.6

* RETURN VALUE
CURLMcode type, general libcurl multi interface error code. On success,
CURLM_OK is returned.

* SEE ALSO
*curl_multi_init*(3),*curl_easy_cleanup*(3),*curl_easy_init*(3)
