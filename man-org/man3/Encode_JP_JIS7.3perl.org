#+TITLE: Manpages - Encode_JP_JIS7.3perl
#+DESCRIPTION: Linux manpage for Encode_JP_JIS7.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Encode::JP::JIS7 -- internally used by Encode::JP
