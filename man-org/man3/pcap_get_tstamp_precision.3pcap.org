#+TITLE: Manpages - pcap_get_tstamp_precision.3pcap
#+DESCRIPTION: Linux manpage for pcap_get_tstamp_precision.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_get_tstamp_precision - get the time stamp precision returned in
captures

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  int pcap_get_tstamp_precision(pcap_t *p);
#+end_example

* DESCRIPTION
*pcap_get_tstamp_precision*() returns the precision of the time stamp
returned in packet captures on the pcap descriptor.

* RETURN VALUE
*pcap_get_tstamp_precision*() returns *PCAP_TSTAMP_PRECISION_MICRO* or
*PCAP_TSTAMP_PRECISION_NANO*, which indicates that pcap captures
contains time stamps in microseconds or nanoseconds respectively.

* BACKWARD COMPATIBILITY
This function became available in libpcap release 1.5.1. In previous
releases, time stamps from a capture device or savefile are always given
in seconds and microseconds.

* SEE ALSO
*pcap*(3PCAP), *pcap_set_tstamp_precision*(3PCAP), *pcap-tstamp*(7)
