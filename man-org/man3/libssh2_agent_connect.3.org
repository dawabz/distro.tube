#+TITLE: Manpages - libssh2_agent_connect.3
#+DESCRIPTION: Linux manpage for libssh2_agent_connect.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_agent_connect - connect to an ssh-agent

* SYNOPSIS
#include <libssh2.h>

int libssh2_agent_connect(LIBSSH2_AGENT *agent);

* DESCRIPTION
Connect to an ssh-agent running on the system.

Call *libssh2_agent_disconnect(3)* to close the connection after you're
doing using it.

* RETURN VALUE
Returns 0 if succeeded, or a negative value for error.

* AVAILABILITY
Added in libssh2 1.2

* SEE ALSO
*libssh2_agent_init(3)* *libssh2_agent_disconnect(3)*
