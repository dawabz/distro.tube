#+TITLE: Manpages - FcRangeDestroy.3
#+DESCRIPTION: Linux manpage for FcRangeDestroy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcRangeDestroy - destroy a range object

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcRangeDestroy (FcRange */range/*);*

* DESCRIPTION
*FcRangeDestroy* destroys a FcRange object, freeing all memory
associated with it.

* SINCE
version 2.11.91
