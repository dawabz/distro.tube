#+TITLE: Manpages - __freadable.3
#+DESCRIPTION: Linux manpage for __freadable.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about __freadable.3 is found in manpage for: [[../man3/stdio_ext.3][man3/stdio_ext.3]]