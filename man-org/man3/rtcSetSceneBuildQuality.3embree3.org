#+TITLE: Manpages - rtcSetSceneBuildQuality.3embree3
#+DESCRIPTION: Linux manpage for rtcSetSceneBuildQuality.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcSetSceneBuildQuality - sets the build quality for
    the scene
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcSetSceneBuildQuality(
    RTCScene scene,
    enum RTCBuildQuality quality
  );
#+end_example

** DESCRIPTION
The =rtcSetSceneBuildQuality= function sets the build quality (=quality=
argument) for the specified scene (=scene= argument). Possible values
for the build quality are:

- =RTC_BUILD_QUALITY_LOW=: Create lower quality data structures, e.g.
  for dynamic scenes. A two-level spatial index structure is built when
  enabling this mode, which supports fast partial scene updates, and
  allows for setting a per-geometry build quality through the
  =rtcSetGeometryBuildQuality= function.

- =RTC_BUILD_QUALITY_MEDIUM=: Default build quality for most usages.
  Gives a good compromise between build and render performance.

- =RTC_BUILD_QUALITY_HIGH=: Create higher quality data structures for
  final-frame rendering. For certain geometry types this enables a
  spatial split BVH.

Selecting a higher build quality results in better rendering performance
but slower scene commit times. The default build quality for a scene is
=RTC_BUILD_QUALITY_MEDIUM=.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcSetGeometryBuildQuality]
