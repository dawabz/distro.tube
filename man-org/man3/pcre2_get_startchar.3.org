#+TITLE: Manpages - pcre2_get_startchar.3
#+DESCRIPTION: Linux manpage for pcre2_get_startchar.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  PCRE2_SIZE pcre2_get_startchar(pcre2_match_data *match_data);
#+end_example

* DESCRIPTION
After a successful call of *pcre2_match()* that was passed the match
block that is this function's argument, this function returns the code
unit offset of the character at which the successful match started. For
a non-partial match, this can be different to the value of /ovector[0]/
if the pattern contains the \K escape sequence. After a partial match,
however, this value is always the same as /ovector[0]/ because \K does
not affect the result of a partial match.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
