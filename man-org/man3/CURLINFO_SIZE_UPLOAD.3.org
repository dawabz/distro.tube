#+TITLE: Manpages - CURLINFO_SIZE_UPLOAD.3
#+DESCRIPTION: Linux manpage for CURLINFO_SIZE_UPLOAD.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLINFO_SIZE_UPLOAD - get the number of uploaded bytes

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_SIZE_UPLOAD, double
*uploadp);

* DESCRIPTION
Pass a pointer to a double to receive the total amount of bytes that
were uploaded.

/CURLINFO_SIZE_UPLOAD_T(3)/ is a newer replacement that returns a more
sensible variable type.

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* Perform the request */
    res = curl_easy_perform(curl);

    if(!res) {
      double ul;
      res = curl_easy_getinfo(curl, CURLINFO_SIZE_UPLOAD, &ul);
      if(!res) {
        printf("Uploaded %.0f bytes\n", ul);
      }
    }
  }
#+end_example

* AVAILABILITY
Added in 7.4.1

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*curl_easy_getinfo*(3), *curl_easy_setopt*(3),
*CURLINFO_SIZE_DOWNLOAD_T*(3), *CURLINFO_SIZE_UPLOAD_T*(3),
