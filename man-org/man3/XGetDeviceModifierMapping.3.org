#+TITLE: Manpages - XGetDeviceModifierMapping.3
#+DESCRIPTION: Linux manpage for XGetDeviceModifierMapping.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XGetDeviceModifierMapping, XSetDeviceModifierMapping - query or change
device modifier mappings

* SYNOPSIS
#+begin_example
  #include <X11/extensions/XInput.h>
#+end_example

#+begin_example
  int XSetDeviceModifierMapping( Display *display,
                                 XDevice *device,
                                 XModifierKeymap *modmap);
#+end_example

#+begin_example
  XModifierKeymap *XGetDeviceModifierMapping( Display *display,
                                              XDevice *device);
#+end_example

#+begin_example
  display
         Specifies the connection to the X server.
#+end_example

#+begin_example
  device
         Specifies the device whose modifier mapping is to be
         queried or modified.
#+end_example

#+begin_example
  modmap
         Specifies a pointer to the XModifierKeymap structure.
#+end_example

* DESCRIPTION

#+begin_quote
  #+begin_example
    The XSetDeviceModifierMapping request specifies the KeyCodes of
    the keys (if any) that are to be used as modifiers for the
    specified device. If it succeeds, the X server generates a
    DeviceMappingNotify event, and XSetDeviceModifierMapping
    returns MappingSuccess. X permits at most eight modifier keys.
    If more than eight are specified in the XModifierKeymap
    structure, a BadLength error results.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The modifiermap member of the XModifierKeymap structure
    contains eight sets of max_keypermod KeyCodes, one for each
    modifier in the order Shift, Lock, Control, Mod1, Mod2, Mod3,
    Mod4, and Mod5. Only nonzero KeyCodes have meaning in each set,
    and zero KeyCodes are ignored. In addition, all of the nonzero
    KeyCodes must be in the range specified by min_keycode and
    max_keycode as returned by XListInputDevices, or a BadValue
    error results. No KeyCode may appear twice in the entire map,
    or a BadValue error results.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    An X server can impose restrictions on how modifiers can be
    changed, for example, if certain keys do not generate up
    transitions in hardware, if auto-repeat cannot be disabled on
    certain keys, or if multiple modifier keys are not supported.
    If some such restriction is violated, the status reply is
    MappingFailed, and none of the modifiers are changed. If the
    new KeyCodes specified for a modifier differ from those
    currently defined and any (current or new) keys for that
    modifier are in the logically down state,
    XSetDeviceModifierMapping returns MappingBusy, and none of the
    modifiers is changed.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XSetDeviceModifierMapping can generate BadLength, BadDevice,
    BadMatch, BadAlloc, and BadValue errors.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The XGetDeviceModifierMapping request returns a pointer to a
    newly created XModifierKeymap structure that contains the keys
    being used as modifiers. The structure should be freed after
    use by calling XFreeModifierMapping . If only zero values
    appear in the set for any modifier, that modifier is disabled.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XGetDeviceModifierMapping can generate BadDevice and BadMatch
    errors.
  #+end_example
#+end_quote

Structures

#+begin_quote
  #+begin_example
    The XModifierKeymap structure contains:
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    typedef struct {
    int max_keypermod;
    KeyCode *modifiermap;
    } XModifierKeymap;
  #+end_example
#+end_quote

* DIAGNOSTICS

#+begin_quote
  #+begin_example
    BadLength
           More than eight keys were specified in the
           XModifierKeymap structure.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadAlloc
           The server failed to allocate the requested resource or
           server memory.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadDevice
           An invalid device was specified. The specified device
           does not exist or has not been opened by this client via
           XOpenInputDevice. This error may also occur if the
           specified device is the X keyboard or X pointer device.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadMatch
           This error may occur if an XGetDeviceModifierMapping or
           XChangeDeviceModifierMapping request was made specifying
           a device that has no keys.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadValue
           Some numeric value falls outside the range of values
           accepted by the request. Unless a specific range is
           specified for an argument, the full range defined by the
           arguments type is accepted. Any argument defined as a
           set of alternatives can generate this error.
  #+end_example
#+end_quote

* SEE ALSO

#+begin_quote
  #+begin_example
    XSetDeviceKeyMapping(3), XSetDeviceButtonMapping(3)
  #+end_example
#+end_quote
