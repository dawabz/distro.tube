#+TITLE: Manpages - MPI_Win_free_keyval.3
#+DESCRIPTION: Linux manpage for MPI_Win_free_keyval.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Win_free_keyval* - Frees a window keyval.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Win_free_keyval(int *win_keyval)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_WIN_FREE_KEYVAL(WIN_KEYVAL, IERROR)
  	INTEGER WIN_KEYVAL, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Win_free_keyval(win_keyval, ierror)
  	INTEGER, INTENT(INOUT) :: win_keyval
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  static void MPI::Win::Free_keyval(int& win_keyval)
#+end_example

* INPUT/OUTPUT PARAMETER
- win_keyval :: Key value (integer).

* OUTPUT PARAMETER
- IERROR :: Fortran only: Error status (integer).

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.
