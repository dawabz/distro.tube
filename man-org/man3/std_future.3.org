#+TITLE: Manpages - std_future.3
#+DESCRIPTION: Linux manpage for std_future.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::future< _Res > - Primary template for future.

* SYNOPSIS
\\

Inherits *std::__basic_future< _Res >*.

** Public Types
template<typename _Res > using *_Ptr* = *unique_ptr*< _Res,
_Result_base::_Deleter >\\
A unique_ptr for result objects.

using *_State_base* = _State_baseV2\\

** Public Member Functions
*future* (const *future* &)=delete\\

*future* (*future* &&__uf) noexcept\\
Move constructor.

_Res *get* ()\\
Retrieving the value.

*future* & *operator=* (const *future* &)=delete\\

*future* & *operator=* (*future* &&__fut) noexcept\\

*shared_future*< _Res > *share* () noexcept\\

bool *valid* () const noexcept\\

void *wait* () const\\

template<typename _Rep , typename _Period > *future_status* *wait_for*
(const *chrono::duration*< _Rep, _Period > &__rel) const\\

template<typename _Clock , typename _Duration > *future_status*
*wait_until* (const *chrono::time_point*< _Clock, _Duration > &__abs)
const\\

** Static Public Member Functions
template<typename _Res , typename _Allocator > static *_Ptr*<
*_Result_alloc*< _Res, _Allocator > > *_S_allocate_result* (const
_Allocator &__a)\\

template<typename _Res , typename _Tp > static *_Ptr*< *_Result*< _Res >
> *_S_allocate_result* (const *std::allocator*< _Tp > &__a)\\

template<typename _Res_ptr , typename _BoundFn > static _Task_setter<
_Res_ptr, _BoundFn > *_S_task_setter* (_Res_ptr &__ptr, _BoundFn
&__call)\\

** Protected Types
typedef *__future_base::_Result*< _Res > & *__result_type*\\

** Protected Member Functions
*__result_type* *_M_get_result* () const\\
Wait for the state to be ready and rethrow any stored exception.

void *_M_swap* (*__basic_future* &__that) noexcept\\

** Friends
template<typename _Fn , typename... _Args> *future*< __async_result_of<
_Fn, _Args... > > *async* (*launch*, _Fn &&, _Args &&...)\\

template<typename > class *packaged_task*\\

class *promise< _Res >*\\

* Detailed Description
** "template<typename _Res>
\\
class std::future< _Res >"Primary template for future.

Definition at line *763* of file *future*.

* Member Typedef Documentation
** template<typename _Res > typedef *__future_base::_Result*<_Res>&
*std::__basic_future*< _Res >::*__result_type*= [protected]=,
= [inherited]=
Definition at line *677* of file *future*.

** template<typename _Res > using *std::__future_base::_Ptr* =
*unique_ptr*<_Res, _Result_base::_Deleter>= [inherited]=
A unique_ptr for result objects.

Definition at line *222* of file *future*.

** using std::__future_base::_State_base = _State_baseV2= [inherited]=
Definition at line *597* of file *future*.

* Constructor & Destructor Documentation
** template<typename _Res > constexpr *std::future*< _Res >::*future*
()= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *785* of file *future*.

** template<typename _Res > *std::future*< _Res >::*future* (*future*<
_Res > && __uf)= [inline]=, = [noexcept]=
Move constructor.

Definition at line *788* of file *future*.

* Member Function Documentation
** template<typename _Res > *__result_type* *std::__basic_future*< _Res
>::_M_get_result () const= [inline]=, = [protected]=, = [inherited]=
Wait for the state to be ready and rethrow any stored exception.

Definition at line *716* of file *future*.

** template<typename _Res > void *std::__basic_future*< _Res >::_M_swap
(*__basic_future*< _Res > & __that)= [inline]=, = [protected]=,
= [noexcept]=, = [inherited]=
Definition at line *725* of file *future*.

** template<typename _Res , typename _Allocator > static *_Ptr*<
*_Result_alloc*< _Res, _Allocator > >
std::__future_base::_S_allocate_result (const _Allocator &
__a)= [inline]=, = [static]=, = [inherited]=
Definition at line *287* of file *future*.

** template<typename _Res , typename _Tp > static *_Ptr*< *_Result*<
_Res > > std::__future_base::_S_allocate_result (const *std::allocator*<
_Tp > & __a)= [inline]=, = [static]=, = [inherited]=
Definition at line *300* of file *future*.

** template<typename _Res_ptr , typename _BoundFn > static _Task_setter<
_Res_ptr, _BoundFn > std::__future_base::_S_task_setter (_Res_ptr &
__ptr, _BoundFn & __call)= [inline]=, = [static]=, = [inherited]=
Definition at line *621* of file *future*.

** template<typename _Res > _Res *std::future*< _Res >::get
()= [inline]=
Retrieving the value.

Definition at line *802* of file *future*.

** template<typename _Res > *future* & *std::future*< _Res >::operator=
(*future*< _Res > && __fut)= [inline]=, = [noexcept]=
Definition at line *794* of file *future*.

** template<typename _Res > bool *std::__basic_future*< _Res >::valid ()
const= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *688* of file *future*.

** template<typename _Res > void *std::__basic_future*< _Res >::wait ()
const= [inline]=, = [inherited]=
Definition at line *691* of file *future*.

** template<typename _Res > template<typename _Rep , typename _Period >
*future_status* *std::__basic_future*< _Res >::wait_for (const
*chrono::duration*< _Rep, _Period > & __rel) const= [inline]=,
= [inherited]=
Definition at line *699* of file *future*.

** template<typename _Res > template<typename _Clock , typename
_Duration > *future_status* *std::__basic_future*< _Res >::wait_until
(const *chrono::time_point*< _Clock, _Duration > & __abs)
const= [inline]=, = [inherited]=
Definition at line *707* of file *future*.

* Friends And Related Function Documentation
** template<typename _Res > template<typename > friend class
packaged_task= [friend]=
Definition at line *773* of file *future*.

** template<typename _Res > friend class *promise*< _Res >= [friend]=
Definition at line *769* of file *future*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
