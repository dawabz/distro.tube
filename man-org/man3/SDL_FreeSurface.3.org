#+TITLE: Manpages - SDL_FreeSurface.3
#+DESCRIPTION: Linux manpage for SDL_FreeSurface.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_FreeSurface - Frees (deletes) a SDL_Surface

* SYNOPSIS
*#include "SDL.h"*

*void SDL_FreeSurface*(*SDL_Surface *surface*);

* DESCRIPTION
Frees the resources used by a previously created *SDL_Surface*. If the
surface was created using /SDL_CreateRGBSurfaceFrom/ then the pixel data
is not freed.

* SEE ALSO
*SDL_CreateRGBSurface* *SDL_CreateRGBSurfaceFrom*
