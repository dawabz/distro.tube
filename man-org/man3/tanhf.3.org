#+TITLE: Manpages - tanhf.3
#+DESCRIPTION: Linux manpage for tanhf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about tanhf.3 is found in manpage for: [[../man3/tanh.3][man3/tanh.3]]