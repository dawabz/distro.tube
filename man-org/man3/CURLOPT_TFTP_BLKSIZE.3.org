#+TITLE: Manpages - CURLOPT_TFTP_BLKSIZE.3
#+DESCRIPTION: Linux manpage for CURLOPT_TFTP_BLKSIZE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_TFTP_BLKSIZE - TFTP block size

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TFTP_BLKSIZE, long
blocksize);

* DESCRIPTION
Specify /blocksize/ to use for TFTP data transmission. Valid range as
per RFC2348 is 8-65464 bytes. The default of 512 bytes will be used if
this option is not specified. The specified block size will only be used
pending support by the remote server. If the server does not return an
option acknowledgement or returns an option acknowledgement with no
blksize, the default of 512 bytes will be used.

* DEFAULT
512

* PROTOCOLS
TFTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "tftp://example.com/bootimage");
    /* try using larger blocks */
    curl_easy_setopt(curl, CURLOPT_TFTP_BLKSIZE, 2048L);
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.19.4

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_MAXFILESIZE*(3),
