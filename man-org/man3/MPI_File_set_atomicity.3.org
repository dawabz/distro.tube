#+TITLE: Manpages - MPI_File_set_atomicity.3
#+DESCRIPTION: Linux manpage for MPI_File_set_atomicity.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_File_set_atomicity* - Sets consistency semantics for data-access
operations (collective).

* SYNTAX
#+begin_example
#+end_example

* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_File_set_atomicity(MPI_File fh, int flag)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_FILE_SET_ATOMICITY(FH, FLAG, IERROR)
  	INTEGER	FH, FLAG, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_File_set_atomicity(fh, flag, ierror)
  	TYPE(MPI_File), INTENT(IN) :: fh
  	LOGICAL, INTENT(IN) :: flag
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  void MPI::File::Set_atomicity(bool flag)
#+end_example

* INPUT PARAMETERS
- fh :: File handle (handle).

- flag :: *true* to enable atomic mode, *false* to enable nonatomic mode
  (boolean).

* OUTPUT PARAMETER
- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
The consistency semantics for data-access operations using the set of
file handles created by one collective MPI_File_open is set by
collectively calling MPI_File_set_atomicity. All processes in the group
must pass identical values for /fh/ and /flag./ If /flag/ is /true,/
atomic mode is set; if /flag/ is /false,/ nonatomic mode is set.

The default value on a call to MPI_File_open in Open MPI is /true/ for
jobs running on more than one node, /false/ for jobs running on a single
SMP. For more information, see the MPI-2 standard.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. For MPI I/O function errors, the default error handler is set to
MPI_ERRORS_RETURN. The error handler may be changed with
MPI_File_set_errhandler; the predefined error handler
MPI_ERRORS_ARE_FATAL may be used to make I/O errors fatal. Note that MPI
does not guarantee that an MPI program can continue past an error.
