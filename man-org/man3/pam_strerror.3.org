#+TITLE: Manpages - pam_strerror.3
#+DESCRIPTION: Linux manpage for pam_strerror.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_strerror - return string describing PAM error code

* SYNOPSIS
#+begin_example
  #include <security/pam_appl.h>
#+end_example

*const char *pam_strerror(pam_handle_t **/pamh/*, int */errnum/*);*

* DESCRIPTION
The *pam_strerror* function returns a pointer to a string describing the
error code passed in the argument /errnum/, possibly using the
LC_MESSAGES part of the current locale to select the appropriate
language. This string must not be modified by the application. No
library function will modify this string.

* RETURN VALUES
This function returns always a pointer to a string.

* SEE ALSO
*pam*(8)
