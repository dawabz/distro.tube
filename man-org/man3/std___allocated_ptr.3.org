#+TITLE: Manpages - std___allocated_ptr.3
#+DESCRIPTION: Linux manpage for std___allocated_ptr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__allocated_ptr< _Alloc > - Non-standard RAII type for managing
pointers obtained from allocators.

* SYNOPSIS
\\

=#include <allocated_ptr.h>=

** Public Types
using *pointer* = typename *allocator_traits*< _Alloc >::pointer\\

using *value_type* = typename *allocator_traits*< _Alloc >::value_type\\

** Public Member Functions
*__allocated_ptr* (*__allocated_ptr* &&__gd) noexcept\\
Transfer ownership of the owned pointer.

template<typename _Ptr , typename _Req = _Require<is_same<_Ptr,
value_type*>>> *__allocated_ptr* (_Alloc &__a, _Ptr __ptr)\\
Convert __ptr to allocator's pointer type and take ownership of it.

*__allocated_ptr* (_Alloc &__a, pointer __ptr) noexcept\\
Take ownership of __ptr.

*~__allocated_ptr* ()\\
Deallocate the owned pointer.

value_type * *get* ()\\
Get the address that the owned pointer refers to.

*__allocated_ptr* & *operator=* (std::nullptr_t) noexcept\\
Release ownership of the owned pointer.

* Detailed Description
** "template<typename _Alloc>
\\
struct std::__allocated_ptr< _Alloc >"Non-standard RAII type for
managing pointers obtained from allocators.

Definition at line *46* of file *allocated_ptr.h*.

* Member Typedef Documentation
** template<typename _Alloc > using *std::__allocated_ptr*< _Alloc
>::pointer = typename *allocator_traits*<_Alloc>::pointer
Definition at line *48* of file *allocated_ptr.h*.

** template<typename _Alloc > using *std::__allocated_ptr*< _Alloc
>::value_type = typename *allocator_traits*<_Alloc>::value_type
Definition at line *49* of file *allocated_ptr.h*.

* Constructor & Destructor Documentation
** template<typename _Alloc > *std::__allocated_ptr*< _Alloc
>::*__allocated_ptr* (_Alloc & __a, pointer __ptr)= [inline]=,
= [noexcept]=
Take ownership of __ptr.

Definition at line *52* of file *allocated_ptr.h*.

** template<typename _Alloc > template<typename _Ptr , typename _Req =
_Require<is_same<_Ptr, value_type*>>> *std::__allocated_ptr*< _Alloc
>::*__allocated_ptr* (_Alloc & __a, _Ptr __ptr)= [inline]=
Convert __ptr to allocator's pointer type and take ownership of it.

Definition at line *59* of file *allocated_ptr.h*.

** template<typename _Alloc > *std::__allocated_ptr*< _Alloc
>::*__allocated_ptr* (*__allocated_ptr*< _Alloc > && __gd)= [inline]=,
= [noexcept]=
Transfer ownership of the owned pointer.

Definition at line *65* of file *allocated_ptr.h*.

** template<typename _Alloc > *std::__allocated_ptr*< _Alloc
>::~*__allocated_ptr* ()= [inline]=
Deallocate the owned pointer.

Definition at line *70* of file *allocated_ptr.h*.

References *std::allocator_traits< _Alloc >::deallocate()*.

* Member Function Documentation
** template<typename _Alloc > value_type * *std::__allocated_ptr*<
_Alloc >::get ()= [inline]=
Get the address that the owned pointer refers to.

Definition at line *85* of file *allocated_ptr.h*.

** template<typename _Alloc > *__allocated_ptr* &
*std::__allocated_ptr*< _Alloc >::operator= (std::nullptr_t)= [inline]=,
= [noexcept]=
Release ownership of the owned pointer.

Definition at line *78* of file *allocated_ptr.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
