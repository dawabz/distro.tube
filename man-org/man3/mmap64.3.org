#+TITLE: Manpages - mmap64.3
#+DESCRIPTION: Linux manpage for mmap64.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about mmap64.3 is found in manpage for: [[../man2/mmap.2][man2/mmap.2]]