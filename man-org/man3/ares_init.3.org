#+TITLE: Manpages - ares_init.3
#+DESCRIPTION: Linux manpage for ares_init.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ares_init - Initialize a resolver channel

* SYNOPSIS
#+begin_example
  #include <ares.h>

  int ares_init(ares_channel *channelptr)
#+end_example

* DESCRIPTION
The *ares_init(3)* function initializes a communications channel for
name service lookups. If it returns successfully, *ares_init(3)* will
set the variable pointed to by /channelptr/ to a handle used to identify
the name service channel. The caller should invoke /ares_destroy(3)/ on
the handle when the channel is no longer needed.

The /ares_init_options(3)/ function is provide to offer more init
alternatives.

* RETURN VALUES
/ares_init(3)/ can return any of the following values:

- *ARES_SUCCESS* :: Initialization succeeded.

- *ARES_EFILE* :: A configuration file could not be read.

- *ARES_ENOMEM* :: The process's available memory was exhausted.

- *ARES_ENOTINITIALIZED* :: c-ares library initialization not yet
  performed.

* NOTES
When initializing from */etc/resolv.conf,* *ares_init*(3) reads the
/domain/ and /search/ directives to allow lookups of short names
relative to the domains specified. The /domain/ and /search/ directives
override one another. If more that one instance of either /domain/ or
/search/ directives is specified, the last occurrence wins. For more
information, please see the *resolv.conf*(5) manual page.

* SEE ALSO
*ares_init_options(3),* *ares_destroy(3),* *ares_dup(3),*
*ares_library_init(3),* *ares_set_servers(3)*

* AUTHOR
Greg Hudson, MIT Information Systems\\
Copyright 1998 by the Massachusetts Institute of Technology.\\
Copyright (C) 2004-2010 by Daniel Stenberg.
