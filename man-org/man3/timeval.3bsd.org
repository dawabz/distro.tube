#+TITLE: Manpages - timeval.3bsd
#+DESCRIPTION: Linux manpage for timeval.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

The

header, included by

defines various structures related to time and timers.

The following structure is used by

among others:

struct timeval { time_t tv_sec; suseconds_t tv_usec; };

The

member represents the elapsed time, in whole seconds. The

member captures rest of the elapsed time, represented as the number of
microseconds.

The following structure is used by

among others:

struct timespec { time_t tv_sec; long tv_nsec; };

The

member is again the elapsed time in whole seconds. The

member represents the rest of the elapsed time in nanoseconds.

A microsecond is equal to one millionth of a second, 1000 nanoseconds,
or 1/1000 milliseconds. To ease the conversions, the macros

and

can be used to convert between

and

It can be stressed that the traditional

and

structures represent elapsed time, measured by the system clock. The
following sketch implements a function suitable for use in a context
where the

structure is required for a conditional timeout:

static void example(struct timespec *spec, time_t minutes) { struct
timeval elapsed;

(void)gettimeofday(&elapsed, NULL);

_DIAGASSERT(spec != NULL); TIMEVAL_TO_TIMESPEC(&elapsed, spec);

/* Add the offset for timeout in minutes. */ spec->tv_sec = spec->tv_sec
+ minutes * 60; }

A better alternative would use the more precise
