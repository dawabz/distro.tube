#+TITLE: Manpages - xcb_unmap_notify_event_t.3
#+DESCRIPTION: Linux manpage for xcb_unmap_notify_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_unmap_notify_event_t - a window is unmapped

* SYNOPSIS
*#include <xcb/xproto.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_unmap_notify_event_t {
      uint8_t      response_type;
      uint8_t      pad0;
      uint16_t     sequence;
      xcb_window_t event;
      xcb_window_t window;
      uint8_t      from_configure;
      uint8_t      pad1[3];
  } xcb_unmap_notify_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_UNMAP_NOTIFY/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- event :: The reconfigured window or its parent, depending on whether
  /StructureNotify/ or /SubstructureNotify/ was selected.

- window :: The window that was unmapped.

- from_configure :: Set to 1 if the event was generated as a result of a
  resizing of the window's parent when /window/ had a win_gravity of
  /UnmapGravity/.

* DESCRIPTION
* SEE ALSO
*xcb_generic_event_t*(3), *xcb_unmap_window*(3)

* AUTHOR
Generated from xproto.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
