#+TITLE: Manpages - std_underlying_type.3
#+DESCRIPTION: Linux manpage for std_underlying_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::underlying_type< _Tp > - The underlying type of an enum.

* SYNOPSIS
\\

Inherits std::__underlying_type_impl< _Tp, bool >.

** Public Types
using *type* = __underlying_type(_Tp)\\

* Detailed Description
** "template<typename _Tp>
\\
struct std::underlying_type< _Tp >"The underlying type of an enum.

Definition at line *2284* of file *std/type_traits*.

* Member Typedef Documentation
** template<typename _Tp , bool = is_enum<_Tp>::value> using
std::__underlying_type_impl< _Tp, bool >::type =
__underlying_type(_Tp)= [inherited]=
Definition at line *2275* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
