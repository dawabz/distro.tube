#+TITLE: Manpages - SLIST_HEAD_INITIALIZER.3bsd
#+DESCRIPTION: Linux manpage for SLIST_HEAD_INITIALIZER.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about SLIST_HEAD_INITIALIZER.3bsd is found in manpage for: [[../man3/queue.3bsd][man3/queue.3bsd]]