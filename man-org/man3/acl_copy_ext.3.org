#+TITLE: Manpages - acl_copy_ext.3
#+DESCRIPTION: Linux manpage for acl_copy_ext.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function copies the ACL pointed to by

from system-managed space to the user managed space pointed to by

The

parameter represents the size in bytes of the buffer pointed to by

The format of the ACL placed in the buffer pointed to by

is a contiguous, persistent data item, the format of which is
unspecified. It is the responsibility of the invoker to allocate an area
large enough to hold the copied ACL. The size of the exportable,
contiguous, persistent form of the ACL may be obtained by invoking the

function.

Any ACL entry descriptors that refer to an entry in the ACL referenced
by

continue to refer to those entries. Any existing ACL pointers that refer
to the ACL referenced by

continue to refer to the ACL.

Upon success, this function returns the number of bytes placed in the
buffer pointed to by

On error, a value of

is returned and

is set appropriately.

If any of the following conditions occur, the

function returns a value of

and sets

to the corresponding value:

The

parameter is zero or negative.

The argument

is not a valid pointer to an ACL.

The ACL referenced by

contains one or more improperly formed ACL entries, or for some other
reason cannot be translated into the external form of an ACL.

The

parameter is greater than zero but smaller than the length of the
contiguous, persistent form of the ACL.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
