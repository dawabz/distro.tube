#+TITLE: Manpages - audit_set_backlog_limit.3
#+DESCRIPTION: Linux manpage for audit_set_backlog_limit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
audit_set_backlog_limit - Set the audit backlog limit

* SYNOPSIS
*#include <libaudit.h>*

int audit_set_backlog_limit(int fd, uint32_t limit);

* DESCRIPTION
audit_set_backlog_limit sets the queue length for audit events awaiting
transfer to the audit daemon. The default value is 64 which can
potentially be overrun by bursts of activity. When the backlog limit is
reached, the kernel consults the failure_flag to see what action to
take.

* RETURN VALUE
The return value is <= 0 on error, otherwise it is the netlink sequence
id number. This function can have any error that sendto would encounter.

* SEE ALSO
*audit_set_failure*(3), *audit_open*(3), *auditd*(8), *auditctl*(8).

* AUTHOR
Steve Grubb
