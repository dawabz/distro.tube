#+TITLE: Manpages - hdestroy_r.3
#+DESCRIPTION: Linux manpage for hdestroy_r.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about hdestroy_r.3 is found in manpage for: [[../man3/hsearch.3][man3/hsearch.3]]