#+TITLE: Manpages - RTCHit.3embree3
#+DESCRIPTION: Linux manpage for RTCHit.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  RTCHit - single hit structure
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  struct RTCHit
  {
    float Ng_x;                                        // x coordinate of geometry normal
    float Ng_y;                                        // y coordinate of geometry normal
    float Ng_z;                                        // z coordinate of geometry normal

    float u;                                           // barycentric u coordinate of hit
    float v;                                           // barycentric v coordinate of hit

    unsigned int primID;                               // geometry ID
    unsigned int geomID;                               // primitive ID
    unsigned int instID[RTC_MAX_INSTANCE_LEVEL_COUNT]; // instance ID
  };
#+end_example

** DESCRIPTION
The =RTCHit= type defines the type of a ray/primitive intersection
result. The hit contains the unnormalized geometric normal in object
space at the hit location (=Ng_x=, =Ng_y=, =Ng_z= members), the
barycentric u/v coordinates of the hit (=u= and =v= members), as well as
the primitive ID (=primID= member), geometry ID (=geomID= member), and
instance ID stack (=instID= member) of the hit. The parametric
intersection distance is not stored inside the hit, but stored inside
the =tfar= member of the ray.

The =embree3/rtcore_ray.h= header additionally defines the same hit
structure in structure of array (SOA) layout for hit packets of size 4
(=RTCHit4= type), size 8 (=RTCHit8= type), and size 16 (=RTCHit16=
type). The header additionally defines an =RTCHitNt= template for hit
packets of an arbitrary compile-time size.

** EXIT STATUS
** SEE ALSO
[RTCRay], [Multi-Level Instancing]
