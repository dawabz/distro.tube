#+TITLE: Manpages - logbf.3
#+DESCRIPTION: Linux manpage for logbf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about logbf.3 is found in manpage for: [[../man3/logb.3][man3/logb.3]]