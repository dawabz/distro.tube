#+TITLE: Manpages - gnutls_certificate_set_flags.3
#+DESCRIPTION: Linux manpage for gnutls_certificate_set_flags.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_certificate_set_flags - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*void gnutls_certificate_set_flags(gnutls_certificate_credentials_t
*/res/*, unsigned int */flags/*);*

* ARGUMENTS
- gnutls_certificate_credentials_t res :: is a
  gnutls_certificate_credentials_t type

- unsigned int flags :: are the flags of *gnutls_certificate_flags* type

* DESCRIPTION
This function will set flags to tweak the operation of the credentials
structure. See the *gnutls_certificate_flags* enumerations for more
information on the available flags.

* SINCE
3.4.7

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
