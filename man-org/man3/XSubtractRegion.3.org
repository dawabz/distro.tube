#+TITLE: Manpages - XSubtractRegion.3
#+DESCRIPTION: Linux manpage for XSubtractRegion.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XSubtractRegion.3 is found in manpage for: [[../man3/XIntersectRegion.3][man3/XIntersectRegion.3]]