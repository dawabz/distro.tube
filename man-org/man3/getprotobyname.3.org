#+TITLE: Manpages - getprotobyname.3
#+DESCRIPTION: Linux manpage for getprotobyname.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about getprotobyname.3 is found in manpage for: [[../man3/getprotoent.3][man3/getprotoent.3]]