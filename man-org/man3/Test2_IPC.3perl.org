#+TITLE: Manpages - Test2_IPC.3perl
#+DESCRIPTION: Linux manpage for Test2_IPC.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test2::IPC - Turn on IPC for threading or forking support.

* SYNOPSIS
You should =use Test2::IPC;= as early as possible in your test file. If
you import this module after API initialization it will attempt to
retrofit IPC onto the existing hubs.

** DISABLING IT
You can use =no Test2::IPC;= to disable IPC for good. You can also use
the T2_NO_IPC env var.

* EXPORTS
All exports are optional.

- cull() :: Cull allows you to collect results from other processes or
  threads on demand.

* SOURCE
The source code repository for Test2 can be found at
/http://github.com/Test-More/test-more//.

* MAINTAINERS
- Chad Granum <exodist@cpan.org> :: 

* AUTHORS
- Chad Granum <exodist@cpan.org> :: 

* COPYRIGHT
Copyright 2020 Chad Granum <exodist@cpan.org>.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See /http://dev.perl.org/licenses//
