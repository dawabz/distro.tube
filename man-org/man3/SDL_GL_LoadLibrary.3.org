#+TITLE: Manpages - SDL_GL_LoadLibrary.3
#+DESCRIPTION: Linux manpage for SDL_GL_LoadLibrary.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_GL_LoadLibrary - Specify an OpenGL library

* SYNOPSIS
*#include "SDL.h"*

*int SDL_GL_LoadLibrary*(*const char *path*);

* DESCRIPTION
If you wish, you may load the OpenGL library at runtime, this must be
done before *SDL_SetVideoMode* is called. The *path* of the GL library
is passed to *SDL_GL_LoadLibrary* and it returns *0* on success, or *-1*
on an error. You must then use *SDL_GL_GetProcAddress* to retrieve
function pointers to GL functions.

* SEE ALSO
*SDL_GL_GetProcAddress*
