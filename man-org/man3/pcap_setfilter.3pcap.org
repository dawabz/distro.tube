#+TITLE: Manpages - pcap_setfilter.3pcap
#+DESCRIPTION: Linux manpage for pcap_setfilter.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_setfilter - set the filter

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  int pcap_setfilter(pcap_t *p, struct bpf_program *fp);
#+end_example

* DESCRIPTION
*pcap_setfilter*() is used to specify a filter program. /fp/ is a
pointer to a /bpf_program/ struct, usually the result of a call to
*pcap_compile*(3PCAP).

* RETURN VALUE
*pcap_setfilter*() returns *0* on success and *PCAP_ERROR* on failure.
If *PCAP_ERROR* is returned, *pcap_geterr*(3PCAP) or
*pcap_perror*(3PCAP) may be called with /p/ as an argument to fetch or
display the error text.

* SEE ALSO
*pcap*(3PCAP)
