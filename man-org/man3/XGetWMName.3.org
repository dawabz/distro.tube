#+TITLE: Manpages - XGetWMName.3
#+DESCRIPTION: Linux manpage for XGetWMName.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XGetWMName.3 is found in manpage for: [[../man3/XSetWMName.3][man3/XSetWMName.3]]