#+TITLE: Manpages - FcFreeTypeQuery.3
#+DESCRIPTION: Linux manpage for FcFreeTypeQuery.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcFreeTypeQuery - compute pattern from font file (and index)

* SYNOPSIS
*#include <fontconfig.h>* #include <fcfreetype.h>

FcPattern * FcFreeTypeQuery (const FcChar8 */file/*, int */id/*,
FcBlanks **/blanks/*, int **/count/*);*

* DESCRIPTION
Constructs a pattern representing the 'id'th face in 'file'. The number
of faces in 'file' is returned in 'count'. FcBlanks is deprecated,
/blanks/ is ignored and accepted only for compatibility with older code.
