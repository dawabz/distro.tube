#+TITLE: Manpages - XDrawText16.3
#+DESCRIPTION: Linux manpage for XDrawText16.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XDrawText16.3 is found in manpage for: [[../man3/XDrawText.3][man3/XDrawText.3]]