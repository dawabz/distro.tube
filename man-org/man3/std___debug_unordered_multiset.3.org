#+TITLE: Manpages - std___debug_unordered_multiset.3
#+DESCRIPTION: Linux manpage for std___debug_unordered_multiset.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__debug::unordered_multiset< _Value, _Hash, _Pred, _Alloc > - Class
std::unordered_multiset with safety/checking/debug instrumentation.

* SYNOPSIS
\\

Inherits *__gnu_debug::_Safe_container< unordered_multiset< _Value,
std::hash< _Value >, std::equal_to< _Value >, std::allocator< _Value >
>, std::allocator< _Value >, __gnu_debug::_Safe_unordered_container >*,
and unordered_multiset< _Value, std::hash< _Value >, std::equal_to<
_Value >, std::allocator< _Value > >.

** Public Types
typedef _Base::allocator_type *allocator_type*\\

typedef *__gnu_debug::_Safe_iterator*< _Base_const_iterator,
*unordered_multiset* > *const_iterator*\\

typedef *__gnu_debug::_Safe_local_iterator*< _Base_const_local_iterator,
*unordered_multiset* > *const_local_iterator*\\

typedef _Base::hasher *hasher*\\

typedef *__gnu_debug::_Safe_iterator*< _Base_iterator,
*unordered_multiset* > *iterator*\\

typedef _Base::key_equal *key_equal*\\

typedef _Base::key_type *key_type*\\

typedef *__gnu_debug::_Safe_local_iterator*< _Base_local_iterator,
*unordered_multiset* > *local_iterator*\\

using *node_type* = typename _Base::node_type\\

typedef _Base::size_type *size_type*\\

typedef _Base::value_type *value_type*\\

** Public Member Functions
*unordered_multiset* (_Base_ref __x)\\

template<typename _InputIterator > *unordered_multiset* (_InputIterator
__first, _InputIterator __last, size_type __n, const allocator_type
&__a)\\

template<typename _InputIterator > *unordered_multiset* (_InputIterator
__first, _InputIterator __last, size_type __n, const hasher &__hf, const
allocator_type &__a)\\

template<typename _InputIterator > *unordered_multiset* (_InputIterator
__first, _InputIterator __last, size_type __n=0, const hasher
&__hf=hasher(), const key_equal &__eql=key_equal(), const allocator_type
&__a=allocator_type())\\

*unordered_multiset* (const allocator_type &__a)\\

*unordered_multiset* (const *unordered_multiset* &)=default\\

*unordered_multiset* (const *unordered_multiset* &__uset, const
allocator_type &__a)\\

*unordered_multiset* (*initializer_list*< value_type > __l, size_type
__n, const allocator_type &__a)\\

*unordered_multiset* (*initializer_list*< value_type > __l, size_type
__n, const hasher &__hf, const allocator_type &__a)\\

*unordered_multiset* (*initializer_list*< value_type > __l, size_type
__n=0, const hasher &__hf=hasher(), const key_equal &__eql=key_equal(),
const allocator_type &__a=allocator_type())\\

*unordered_multiset* (size_type __n, const allocator_type &__a)\\

*unordered_multiset* (size_type __n, const hasher &__hf, const
allocator_type &__a)\\

*unordered_multiset* (size_type __n, const hasher &__hf=hasher(), const
key_equal &__eql=key_equal(), const allocator_type
&__a=allocator_type())\\

*unordered_multiset* (*unordered_multiset* &&)=default\\

*unordered_multiset* (*unordered_multiset* &&__uset, const
allocator_type &__a)
noexcept(noexcept(*_Base*(*std::move*(__uset._M_base()), __a)))\\

const *_Base* & *_M_base* () const noexcept\\

*_Base* & *_M_base* () noexcept\\

void *_M_swap* (_Safe_container &__x) noexcept\\

*const_iterator* *begin* () const noexcept\\

*iterator* *begin* () noexcept\\

*local_iterator* *begin* (size_type __b)\\

*const_local_iterator* *begin* (size_type __b) const\\

size_type *bucket_size* (size_type __b) const\\

*const_iterator* *cbegin* () const noexcept\\

*const_local_iterator* *cbegin* (size_type __b) const\\

*const_iterator* *cend* () const noexcept\\

*const_local_iterator* *cend* (size_type __b) const\\

void *clear* () noexcept\\

template<typename... _Args> *iterator* *emplace* (_Args &&... __args)\\

template<typename... _Args> *iterator* *emplace_hint* (*const_iterator*
__hint, _Args &&... __args)\\

*const_iterator* *end* () const noexcept\\

*iterator* *end* () noexcept\\

*local_iterator* *end* (size_type __b)\\

*const_local_iterator* *end* (size_type __b) const\\

*std::pair*< *iterator*, *iterator* > *equal_range* (const key_type
&__key)\\

*std::pair*< *const_iterator*, *const_iterator* > *equal_range* (const
key_type &__key) const\\

size_type *erase* (const key_type &__key)\\

*iterator* *erase* (*const_iterator* __first, *const_iterator* __last)\\

*iterator* *erase* (*const_iterator* __it)\\

*iterator* *erase* (*iterator* __it)\\

node_type *extract* (const key_type &__key)\\

node_type *extract* (*const_iterator* __position)\\

*iterator* *find* (const key_type &__key)\\

*const_iterator* *find* (const key_type &__key) const\\

template<typename _InputIterator > void *insert* (_InputIterator
__first, _InputIterator __last)\\

*iterator* *insert* (const value_type &__obj)\\

*iterator* *insert* (*const_iterator* __hint, const value_type &__obj)\\

*iterator* *insert* (*const_iterator* __hint, node_type &&__nh)\\

*iterator* *insert* (*const_iterator* __hint, value_type &&__obj)\\

*iterator* *insert* (node_type &&__nh)\\

void *insert* (*std::initializer_list*< value_type > __l)\\

*iterator* *insert* (value_type &&__obj)\\

float *max_load_factor* () const noexcept\\

void *max_load_factor* (float __f)\\

*unordered_multiset* & *operator=* (const *unordered_multiset*
&)=default\\

*unordered_multiset* & *operator=* (*initializer_list*< value_type >
__l)\\

*unordered_multiset* & *operator=* (*unordered_multiset* &&)=default\\

void *swap* (*unordered_multiset* &__x) noexcept(noexcept(declval<
*_Base* & >().swap(__x)))\\

** Public Attributes
_Safe_iterator_base * *_M_const_iterators*\\
The list of constant iterators that reference this container.

_Safe_iterator_base * *_M_const_local_iterators*\\
The list of constant local iterators that reference this container.

_Safe_iterator_base * *_M_iterators*\\
The list of mutable iterators that reference this container.

_Safe_iterator_base * *_M_local_iterators*\\
The list of mutable local iterators that reference this container.

unsigned int *_M_version*\\
The container version number. This number may never be 0.

** Protected Member Functions
void *_M_detach_all* ()\\

void *_M_detach_singular* ()\\

__gnu_cxx::__mutex & *_M_get_mutex* () throw ()\\

void *_M_invalidate_all* ()\\

void *_M_invalidate_all* () const\\

template<typename _Predicate > void *_M_invalidate_if* (_Predicate
__pred)\\

template<typename _Predicate > void *_M_invalidate_local_if* (_Predicate
__pred)\\

void *_M_invalidate_locals* ()\\

void *_M_revalidate_singular* ()\\

_Safe_container & *_M_safe* () noexcept\\

void *_M_swap* (_Safe_sequence_base &__x) noexcept\\

void *_M_swap* (_Safe_unordered_container_base &__x) noexcept\\

** Friends
template<typename _ItT , typename _SeqT , typename _CatT > class
*::__gnu_debug::_Safe_iterator*\\

template<typename _ItT , typename _SeqT > class
*::__gnu_debug::_Safe_local_iterator*\\

* Detailed Description
** "template<typename _Value, typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>>
\\
class std::__debug::unordered_multiset< _Value, _Hash, _Pred, _Alloc
>"Class std::unordered_multiset with safety/checking/debug
instrumentation.

Definition at line *679* of file *debug/unordered_set*.

* Member Typedef Documentation
** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef _Base::allocator_type
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::allocator_type
Definition at line *712* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef *__gnu_debug::_Safe_iterator*<
_Base_const_iterator, *unordered_multiset*>
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::*const_iterator*
Definition at line *720* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef *__gnu_debug::_Safe_local_iterator*<
_Base_const_local_iterator, *unordered_multiset*>
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::*const_local_iterator*
Definition at line *724* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef _Base::hasher
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::hasher
Definition at line *710* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef *__gnu_debug::_Safe_iterator*<
_Base_iterator, *unordered_multiset*>
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::*iterator*
Definition at line *718* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef _Base::key_equal
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::key_equal
Definition at line *711* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef _Base::key_type
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::key_type
Definition at line *714* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef *__gnu_debug::_Safe_local_iterator*<
_Base_local_iterator, *unordered_multiset*>
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::*local_iterator*
Definition at line *722* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> using *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::node_type = typename _Base::node_type
Definition at line *1011* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef _Base::size_type
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::size_type
Definition at line *709* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> typedef _Base::value_type
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::value_type
Definition at line *715* of file *debug/unordered_set*.

* Constructor & Destructor Documentation
** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_multiset*< _Value,
_Hash, _Pred, _Alloc >::*unordered_multiset* (size_type __n, const
hasher & __hf = =hasher()=, const key_equal & __eql = =key_equal()=,
const allocator_type & __a = =allocator_type()=)= [inline]=,
= [explicit]=
Definition at line *729* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> template<typename _InputIterator >
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::*unordered_multiset* (_InputIterator __first, _InputIterator __last,
size_type __n = =0=, const hasher & __hf = =hasher()=, const key_equal &
__eql = =key_equal()=, const allocator_type & __a =
=allocator_type()=)= [inline]=
Definition at line *736* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_multiset*< _Value,
_Hash, _Pred, _Alloc >::*unordered_multiset* (_Base_ref __x)= [inline]=
Definition at line *748* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_multiset*< _Value,
_Hash, _Pred, _Alloc >::*unordered_multiset* (const allocator_type &
__a)= [inline]=, = [explicit]=
Definition at line *754* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_multiset*< _Value,
_Hash, _Pred, _Alloc >::*unordered_multiset* (const
*unordered_multiset*< _Value, _Hash, _Pred, _Alloc > & __uset, const
allocator_type & __a)= [inline]=
Definition at line *757* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_multiset*< _Value,
_Hash, _Pred, _Alloc >::*unordered_multiset* (*unordered_multiset*<
_Value, _Hash, _Pred, _Alloc > && __uset, const allocator_type &
__a)= [inline]=, = [noexcept]=
Definition at line *761* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_multiset*< _Value,
_Hash, _Pred, _Alloc >::*unordered_multiset* (*initializer_list*<
value_type > __l, size_type __n = =0=, const hasher & __hf = =hasher()=,
const key_equal & __eql = =key_equal()=, const allocator_type & __a =
=allocator_type()=)= [inline]=
Definition at line *767* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_multiset*< _Value,
_Hash, _Pred, _Alloc >::*unordered_multiset* (size_type __n, const
allocator_type & __a)= [inline]=
Definition at line *774* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_multiset*< _Value,
_Hash, _Pred, _Alloc >::*unordered_multiset* (size_type __n, const
hasher & __hf, const allocator_type & __a)= [inline]=
Definition at line *778* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> template<typename _InputIterator >
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::*unordered_multiset* (_InputIterator __first, _InputIterator __last,
size_type __n, const allocator_type & __a)= [inline]=
Definition at line *784* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> template<typename _InputIterator >
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::*unordered_multiset* (_InputIterator __first, _InputIterator __last,
size_type __n, const hasher & __hf, const allocator_type &
__a)= [inline]=
Definition at line *791* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_multiset*< _Value,
_Hash, _Pred, _Alloc >::*unordered_multiset* (*initializer_list*<
value_type > __l, size_type __n, const allocator_type & __a)= [inline]=
Definition at line *797* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::__debug::unordered_multiset*< _Value,
_Hash, _Pred, _Alloc >::*unordered_multiset* (*initializer_list*<
value_type > __l, size_type __n, const hasher & __hf, const
allocator_type & __a)= [inline]=
Definition at line *803* of file *debug/unordered_set*.

* Member Function Documentation
** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> const *_Base* &
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::_M_base () const= [inline]=, = [noexcept]=
Definition at line *1155* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *_Base* & *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::_M_base ()= [inline]=, = [noexcept]=
Definition at line *1152* of file *debug/unordered_set*.

** void __gnu_debug::_Safe_unordered_container_base::_M_detach_all
()= [protected]=, = [inherited]=
Detach all iterators, leaving them singular.

** void __gnu_debug::_Safe_sequence_base::_M_detach_singular
()= [protected]=, = [inherited]=
Detach all singular iterators.

*Postcondition*

#+begin_quote
  for all iterators i attached to this sequence, i->_M_version ==
  _M_version.
#+end_quote

** __gnu_cxx::__mutex & __gnu_debug::_Safe_sequence_base::_M_get_mutex
()= [protected]=, = [inherited]=
For use in _Safe_sequence.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** template<typename _Container > void
*__gnu_debug::_Safe_unordered_container*< _Container
>::_M_invalidate_all ()= [inline]=, = [protected]=, = [inherited]=
Definition at line *76* of file *safe_unordered_container.h*.

** void __gnu_debug::_Safe_sequence_base::_M_invalidate_all ()
const= [inline]=, = [protected]=, = [inherited]=
Invalidates all iterators.

Definition at line *256* of file *safe_base.h*.

References *__gnu_debug::_Safe_sequence_base::_M_version*.

** template<typename _Container > template<typename _Predicate > void
*__gnu_debug::_Safe_unordered_container*< _Container >::_M_invalidate_if
(_Predicate __pred)= [protected]=, = [inherited]=
Invalidates all iterators =x= that reference this container, are not
singular, and for which =__pred(x)= returns =true=. =__pred= will be
invoked with the normal iterators nested in the safe ones.

Definition at line *37* of file *safe_unordered_container.tcc*.

** template<typename _Container > template<typename _Predicate > void
*__gnu_debug::_Safe_unordered_container*< _Container
>::_M_invalidate_local_if (_Predicate __pred)= [protected]=,
= [inherited]=
Invalidates all local iterators =x= that reference this container, are
not singular, and for which =__pred(x)= returns =true=. =__pred= will be
invoked with the normal local iterators nested in the safe ones.

Definition at line *69* of file *safe_unordered_container.tcc*.

** template<typename _Container > void
*__gnu_debug::_Safe_unordered_container*< _Container
>::_M_invalidate_locals ()= [inline]=, = [protected]=, = [inherited]=
Definition at line *67* of file *safe_unordered_container.h*.

** void __gnu_debug::_Safe_sequence_base::_M_revalidate_singular
()= [protected]=, = [inherited]=
Revalidates all attached singular iterators. This method may be used to
validate iterators that were invalidated before (but for some reason,
such as an exception, need to become valid again).

** _Safe_container & *__gnu_debug::_Safe_container*<
*unordered_multiset*< _Value, *std::hash*< _Value >, *std::equal_to*<
_Value >, *std::allocator*< _Value > > , *std::allocator*< _Value > ,
*__gnu_debug::_Safe_unordered_container* , true >::_M_safe
()= [inline]=, = [protected]=, = [noexcept]=, = [inherited]=
Definition at line *52* of file *safe_container.h*.

** void *__gnu_debug::_Safe_container*< *unordered_multiset*< _Value,
*std::hash*< _Value >, *std::equal_to*< _Value >, *std::allocator*<
_Value > > , *std::allocator*< _Value > ,
*__gnu_debug::_Safe_unordered_container* , true >::_M_swap
(*_Safe_container*< *unordered_multiset*< _Value, *std::hash*< _Value >,
*std::equal_to*< _Value >, *std::allocator*< _Value > >,
*std::allocator*< _Value >, *__gnu_debug::_Safe_unordered_container* > &
__x)= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *111* of file *safe_container.h*.

** void __gnu_debug::_Safe_sequence_base::_M_swap (*_Safe_sequence_base*
& __x)= [protected]=, = [noexcept]=, = [inherited]=
Swap this sequence with the given sequence. This operation also swaps
ownership of the iterators, so that when the operation is complete all
iterators that originally referenced one container now reference the
other container.

** void __gnu_debug::_Safe_unordered_container_base::_M_swap
(*_Safe_unordered_container_base* & __x)= [protected]=, = [noexcept]=,
= [inherited]=
Swap this container with the given container. This operation also swaps
ownership of the iterators, so that when the operation is complete all
iterators that originally referenced one container now reference the
other container.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_iterator*
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::begin () const= [inline]=, = [noexcept]=
Definition at line *845* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::begin ()= [inline]=, = [noexcept]=
Definition at line *841* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *local_iterator*
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::begin (size_type __b)= [inline]=
Definition at line *866* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_local_iterator*
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::begin (size_type __b) const= [inline]=
Definition at line *880* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> size_type *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::bucket_size (size_type __b)
const= [inline]=
Definition at line *908* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_iterator*
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::cbegin () const= [inline]=, = [noexcept]=
Definition at line *857* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_local_iterator*
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::cbegin (size_type __b) const= [inline]=
Definition at line *894* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_iterator*
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc >::cend
() const= [inline]=, = [noexcept]=
Definition at line *861* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_local_iterator*
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc >::cend
(size_type __b) const= [inline]=
Definition at line *901* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> void *std::__debug::unordered_multiset*< _Value,
_Hash, _Pred, _Alloc >::clear ()= [inline]=, = [noexcept]=
Definition at line *834* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> template<typename... _Args> *iterator*
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::emplace (_Args &&... __args)= [inline]=
Definition at line *927* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> template<typename... _Args> *iterator*
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::emplace_hint (*const_iterator* __hint, _Args &&... __args)= [inline]=
Definition at line *937* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_iterator*
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc >::end
() const= [inline]=, = [noexcept]=
Definition at line *853* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::end ()= [inline]=, = [noexcept]=
Definition at line *849* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *local_iterator*
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc >::end
(size_type __b)= [inline]=
Definition at line *873* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_local_iterator*
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc >::end
(size_type __b) const= [inline]=
Definition at line *887* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::pair*< *iterator*, *iterator* >
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::equal_range (const key_type & __key)= [inline]=
Definition at line *1070* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *std::pair*< *const_iterator*, *const_iterator*
> *std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::equal_range (const key_type & __key) const= [inline]=
Definition at line *1089* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> size_type *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::erase (const key_type &
__key)= [inline]=
Definition at line *1108* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::erase (*const_iterator* __first,
*const_iterator* __last)= [inline]=
Definition at line *1137* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::erase (*const_iterator* __it)= [inline]=
Definition at line *1123* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::erase (*iterator* __it)= [inline]=
Definition at line *1130* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> node_type *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::extract (const key_type &
__key)= [inline]=
Definition at line *1021* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> node_type *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::extract (*const_iterator*
__position)= [inline]=
Definition at line *1014* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::find (const key_type & __key)= [inline]=
Definition at line *1044* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *const_iterator*
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc >::find
(const key_type & __key) const= [inline]=
Definition at line *1057* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> template<typename _InputIterator > void
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::insert (_InputIterator __first, _InputIterator __last)= [inline]=
Definition at line *995* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::insert (const value_type &
__obj)= [inline]=
Definition at line *948* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::insert (*const_iterator* __hint, const
value_type & __obj)= [inline]=
Definition at line *957* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::insert (*const_iterator* __hint,
node_type && __nh)= [inline]=
Definition at line *1034* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::insert (*const_iterator* __hint,
value_type && __obj)= [inline]=
Definition at line *976* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::insert (node_type && __nh)= [inline]=
Definition at line *1030* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> void *std::__debug::unordered_multiset*< _Value,
_Hash, _Pred, _Alloc >::insert (*std::initializer_list*< value_type >
__l)= [inline]=
Definition at line *986* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *iterator* *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::insert (value_type && __obj)= [inline]=
Definition at line *967* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> float *std::__debug::unordered_multiset*<
_Value, _Hash, _Pred, _Alloc >::max_load_factor () const= [inline]=,
= [noexcept]=
Definition at line *915* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> void *std::__debug::unordered_multiset*< _Value,
_Hash, _Pred, _Alloc >::max_load_factor (float __f)= [inline]=
Definition at line *919* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> *unordered_multiset* &
*std::__debug::unordered_multiset*< _Value, _Hash, _Pred, _Alloc
>::operator= (*initializer_list*< value_type > __l)= [inline]=
Definition at line *818* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> void *std::__debug::unordered_multiset*< _Value,
_Hash, _Pred, _Alloc >::swap (*unordered_multiset*< _Value, _Hash,
_Pred, _Alloc > & __x)= [inline]=, = [noexcept]=
Definition at line *826* of file *debug/unordered_set*.

* Friends And Related Function Documentation
** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> template<typename _ItT , typename _SeqT ,
typename _CatT > friend class ::*__gnu_debug::_Safe_iterator*= [friend]=
Definition at line *696* of file *debug/unordered_set*.

** template<typename _Value , typename _Hash = std::hash<_Value>,
typename _Pred = std::equal_to<_Value>, typename _Alloc =
std::allocator<_Value>> template<typename _ItT , typename _SeqT > friend
class ::*__gnu_debug::_Safe_local_iterator*= [friend]=
Definition at line *698* of file *debug/unordered_set*.

* Member Data Documentation
** _Safe_iterator_base*
__gnu_debug::_Safe_sequence_base::_M_const_iterators= [inherited]=
The list of constant iterators that reference this container.

Definition at line *197* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** _Safe_iterator_base*
__gnu_debug::_Safe_unordered_container_base::_M_const_local_iterators= [inherited]=
The list of constant local iterators that reference this container.

Definition at line *130* of file *safe_unordered_base.h*.

** _Safe_iterator_base*
__gnu_debug::_Safe_sequence_base::_M_iterators= [inherited]=
The list of mutable iterators that reference this container.

Definition at line *194* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** _Safe_iterator_base*
__gnu_debug::_Safe_unordered_container_base::_M_local_iterators= [inherited]=
The list of mutable local iterators that reference this container.

Definition at line *127* of file *safe_unordered_base.h*.

** unsigned int
__gnu_debug::_Safe_sequence_base::_M_version= [mutable]=, = [inherited]=
The container version number. This number may never be 0.

Definition at line *200* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence_base::_M_invalidate_all()*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
