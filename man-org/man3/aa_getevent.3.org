#+TITLE: Manpages - aa_getevent.3
#+DESCRIPTION: Linux manpage for aa_getevent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
aa_getevent - keyboard functions

* SYNOPSIS
#include <aalib.h>

int aa_getevent\\
(\\
aa_context *c,\\
int wait\\
);

* PARAMETERS
- *aa_context *c* :: Specifies the AA-lib context to operate on.

- *int wait* :: 1 if you wish to wait for the even when queue is empty.

* DESCRIPTION
Return next event from queue. Return next even from queue. Optionally
wait for even when queue is empty.

* RETURNS
Next event from queue (values lower than 256 are used to report ascii
values of pressed keys and higher values have special meanings) See the
AA-lib texinfo documentation for more details. 0 is returned when queue
is empty and wait is set to 0.

* SEE ALSO
save_d(3), mem_d(3), aa_help(3), aa_formats(3), aa_fonts(3),
aa_dithernames(3), aa_drivers(3), aa_kbddrivers(3), aa_mousedrivers(3),
aa_kbdrecommended(3), aa_mouserecommended(3), aa_displayrecommended(3),
aa_defparams(3), aa_defrenderparams(3), aa_scrwidth(3), aa_scrheight(3),
aa_mmwidth(3), aa_mmheight(3), aa_imgwidth(3), aa_imgheight(3),
aa_image(3), aa_text(3), aa_attrs(3), aa_currentfont(3), aa_autoinit(3),
aa_autoinitkbd(3), aa_autoinitmouse(3), aa_recommendhi(3),
aa_recommendlow(3), aa_init(3), aa_initkbd(3), aa_initmouse(3),
aa_close(3), aa_uninitkbd(3), aa_uninitmouse(3), aa_fastrender(3),
aa_render(3), aa_puts(3), aa_printf(3), aa_gotoxy(3), aa_hidecursor(3),
aa_showcursor(3), aa_getmouse(3), aa_hidemouse(3), aa_showmouse(3),
aa_registerfont(3), aa_setsupported(3), aa_setfont(3), aa_getkey(3),
aa_resize(3), aa_resizehandler(3), aa_parseoptions(3), aa_edit(3),
aa_createedit(3), aa_editkey(3), aa_putpixel(3), aa_recommendhikbd(3),
aa_recommendlowkbd(3), aa_recommendhimouse(3), aa_recommendlowmouse(3),
aa_recommendhidisplay(3), aa_recommendlowdisplay(3)
