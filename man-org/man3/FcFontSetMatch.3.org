#+TITLE: Manpages - FcFontSetMatch.3
#+DESCRIPTION: Linux manpage for FcFontSetMatch.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcFontSetMatch - Return the best font from a set of font sets

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcPattern * FcFontSetMatch (FcConfig */config/*, FcFontSet ***/sets/*,
int*/nsets/*, FcPattern **/pattern/*, FcResult **/result/*);*

* DESCRIPTION
Finds the font in /sets/ most closely matching /pattern/ and returns the
result of *FcFontRenderPrepare* for that font and the provided pattern.
This function should be called only after *FcConfigSubstitute* and
*FcDefaultSubstitute* have been called for /pattern/; otherwise the
results will not be correct. If /config/ is NULL, the current
configuration is used. Returns NULL if an error occurs during this
process.
