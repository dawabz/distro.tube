#+TITLE: Manpages - xcb_xfixes_set_gc_clip_region.3
#+DESCRIPTION: Linux manpage for xcb_xfixes_set_gc_clip_region.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_xfixes_set_gc_clip_region -

* SYNOPSIS
*#include <xcb/xfixes.h>*

** Request function
xcb_void_cookie_t *xcb_xfixes_set_gc_clip_region*(xcb_connection_t
*/conn/, xcb_gcontext_t /gc/, xcb_xfixes_region_t /region/, int16_t
/x_origin/, int16_t /y_origin/);\\

* REQUEST ARGUMENTS
- conn :: The XCB connection to X11.

- gc :: TODO: NOT YET DOCUMENTED.

- region :: TODO: NOT YET DOCUMENTED.

- x_origin :: TODO: NOT YET DOCUMENTED.

- y_origin :: TODO: NOT YET DOCUMENTED.

* DESCRIPTION
* RETURN VALUE
Returns an /xcb_void_cookie_t/. Errors (if any) have to be handled in
the event loop.

If you want to handle errors directly with /xcb_request_check/ instead,
use /xcb_xfixes_set_gc_clip_region_checked/. See *xcb-requests(3)* for
details.

* ERRORS
This request does never generate any errors.

* SEE ALSO
* AUTHOR
Generated from xfixes.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
