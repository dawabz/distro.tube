#+TITLE: Manpages - getpeereid.3bsd
#+DESCRIPTION: Linux manpage for getpeereid.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

The

function returns the effective user and group IDs of the peer connected
to a

socket. The argument

must be a

socket

of type

on which either

or

have been called. The effective used ID is placed in

and the effective group ID in

The credentials returned to the

caller are those of its peer at the time it called

the credentials returned to the

caller are those of its peer at the time it called

This mechanism is reliable; there is no way for either side to influence
the credentials returned to its peer except by calling the appropriate
system call (i.e., either

or

under different effective credentials.

One common use of this routine is for a

server to verify the credentials of its client. Likewise, the client can
verify the credentials of the server.

On

is implemented in terms of the

socket option.

The

function fails if:

The argument

is not a valid descriptor.

The argument

is a file, not a socket.

The argument

does not refer to a socket on which

or

have been called.

The argument

does not refer to a socket of type

or the kernel returned invalid data.

The

function appeared in
