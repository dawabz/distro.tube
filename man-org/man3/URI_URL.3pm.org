#+TITLE: Manpages - URI_URL.3pm
#+DESCRIPTION: Linux manpage for URI_URL.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
URI::URL - Uniform Resource Locators

* SYNOPSIS
$u1 = URI::URL->new($str, $base); $u2 = $u1->abs;

* DESCRIPTION
This module is provided for backwards compatibility with modules that
depend on the interface provided by the =URI::URL= class that used to be
distributed with the libwww-perl library.

The following differences exist compared to the =URI= class interface:

- The URI::URL module exports the *url()* function as an alternate
  constructor interface.

- The constructor takes an optional =$base= argument. The =URI::URL=
  class is a subclass of =URI::WithBase=.

- The URI::URL->newlocal class method is the same as URI::file->new_abs.

- *URI::URL::strict* (1)

- =$url=->print_on method

- =$url=->crack method

- =$url=->full_path: same as ($uri->abs_path || /)

- =$url=->netloc: same as =$uri=->authority

- =$url=->epath, =$url=->equery: same as =$uri=->path, =$uri=->query

- =$url=->path and =$url=->query pass unescaped strings.

- =$url=->path_components: same as =$uri=->path_segments (if you don't
  consider path segment parameters)

- =$url=->params and =$url=->eparams methods

- =$url=->base method. See URI::WithBase.

- =$url=->abs and =$url=->rel have an optional =$base= argument. See
  URI::WithBase.

- =$url=->frag: same as =$uri=->fragment

- =$url=->keywords: same as =$uri=->query_keywords

- =$url=->localpath and friends map to =$uri=->file.

- =$url=->address and =$url=->encoded822addr: same as =$uri=->to for
  mailto URI

- =$url=->groupart method for news URI

- =$url=->article: same as =$uri=->message

* SEE ALSO
URI, URI::WithBase

* COPYRIGHT
Copyright 1998-2000 Gisle Aas.
