#+TITLE: Manpages - libssh2_agent_userauth.3
#+DESCRIPTION: Linux manpage for libssh2_agent_userauth.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_agent_userauth - authenticate a session with a public key, with
the help of ssh-agent

* SYNOPSIS
#include <libssh2.h>

int libssh2_agent_userauth(LIBSSH2_AGENT *agent, const char *username,
struct libssh2_agent_publickey *identity);

* DESCRIPTION
/agent/ - ssh-agent handle as returned by *libssh2_agent_init(3)*

/username/ - Remote user name to authenticate as.

/identity/ - Public key to authenticate with, as returned by
*libssh2_agent_get_identity(3)*

Attempt public key authentication with the help of ssh-agent.

* RETURN VALUE
Returns 0 if succeeded, or a negative value for error.

* AVAILABILITY
Added in libssh2 1.2

* SEE ALSO
*libssh2_agent_init(3)* *libssh2_agent_get_identity(3)*
