#+TITLE: Manpages - FcObjectSetBuild.3
#+DESCRIPTION: Linux manpage for FcObjectSetBuild.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcObjectSetBuild, FcObjectSetVaBuild, FcObjectSetVapBuild - Build object
set from args

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcObjectSet * FcObjectSetBuild (const char */first/*, ...);*

FcObjectSet * FcObjectSetVaBuild (const char */first/*, va_list
*/va/*);*

void FcObjectSetVapBuild (FcObjectSet */result/*, const char **/first/*,
va_list */va/*);*

* DESCRIPTION
These build an object set from a null-terminated list of property names.
FcObjectSetVapBuild is a macro version of FcObjectSetVaBuild which
returns the result in the /result/ variable directly.
