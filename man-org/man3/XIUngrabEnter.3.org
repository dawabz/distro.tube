#+TITLE: Manpages - XIUngrabEnter.3
#+DESCRIPTION: Linux manpage for XIUngrabEnter.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XIUngrabEnter.3 is found in manpage for: [[../man3/XIGrabEnter.3][man3/XIGrabEnter.3]]