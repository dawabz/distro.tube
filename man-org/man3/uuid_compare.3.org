#+TITLE: Manpages - uuid_compare.3
#+DESCRIPTION: Linux manpage for uuid_compare.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
uuid_compare - compare whether two UUIDs are the same

* SYNOPSIS
*#include <uuid.h>*

*int uuid_compare(uuid_t */uu1/*, uuid_t */uu2/*)*

* DESCRIPTION
The *uuid_compare*/() function compares the two supplied uuid variables
uu1 and uu2 to each other./

* RETURN VALUE
Returns an integer less than, equal to, or greater than zero if /uu1 is
found, respectively, to be lexicographically less than, equal, or
greater than uu2./

* AUTHORS
Theodore Y. Ts'o

* SEE ALSO
*uuid*/(3),/ *uuid_clear*/(3),/ *uuid_copy*/(3),/ *uuid_generate*/(3),/
*uuid_is_null*/(3),/ *uuid_parse*/(3),/ *uuid_unparse*/(3)/

* AVAILABILITY
The *libuuid*/ library is part of the util-linux package since version
2.15.1. It can be downloaded from / /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
