#+TITLE: Manpages - eventfd_read.3
#+DESCRIPTION: Linux manpage for eventfd_read.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about eventfd_read.3 is found in manpage for: [[../man2/eventfd.2][man2/eventfd.2]]