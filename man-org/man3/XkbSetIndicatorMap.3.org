#+TITLE: Manpages - XkbSetIndicatorMap.3
#+DESCRIPTION: Linux manpage for XkbSetIndicatorMap.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbSetIndicatorMap - Downloads the changes to the server based on
modifications to a local copy of the keyboard description which will
update the maps for one or more indicators

* SYNOPSIS
*Bool XkbSetIndicatorMap* *( Display **/dpy/* ,* *unsigned int
*/which/* ,* *XkbDescPtr */desc/* );*

* ARGUMENTS
- /- dpy/ :: connection to the X server

- /- which/ :: mask of indicators to change

- /- desc/ :: keyboard description from which the maps are taken

* DESCRIPTION
This section discusses the effects of explicitly changing indicators
depending upon different settings in the indicator map. See Tables 1 and
Table 2 for information on the effects of the indicator map fields when
explicit changes are made.

TABLE

TABLE

If XkbIM_LEDDrivesKB is set and XkbIM_NoExplicit is not, and if you call
a function that updates the server's image of the indicator map (such as
/XkbSetIndicatorMap/ or /XkbSetNamedIndicator),/ Xkb changes the
keyboard state and controls to reflect the other fields of the indicator
map. If you attempt to explicitly change the value of an indicator for
which XkbIM_LEDDrivesKB is absent or for which XkbIM_NoExplicit is
present, keyboard state or controls are unaffected. If neither
XkbIM_NoAutomatic nor XkbIM_NoExplicit is set in an indicator map, Xkb
honors any request to change the state of the indicator, but the new
state might be immediately superseded by automatic changes to the
indicator state if the keyboard state or controls change.

The effects of changing an indicator that drives the keyboard are
cumulative; it is possible for a single change to affect keyboard group,
modifiers, and controls simultaneously.

If you change an indicator for which both the XkbIM_LEDDrivesKB and
XkbIM_NoAutomatic flags are specified, Xkb applies the keyboard changes
specified in the other indicator map fields and changes the indicator to
reflect the state that was explicitly requested. The indicator remains
in the new state until it is explicitly changed again.

If the XkbIM_NoAutomatic flag is not set and XkbIM_LEDDrivesKB is set,
Xkb applies the changes specified in the other indicator map fields and
sets the state of the indicator to the values specified by the indicator
map. Note that it is possible in this case for the indicator to end up
in a different state than the one that was explicitly requested. For
example, Xkb does not extinguish an indicator with /which_mods/ of
XkbIM_UseBase and /mods/ of Shift if, at the time Xkb processes the
request to extinguish the indicator, one of the Shift keys is physically
depressed.

If you explicitly light an indicator for which XkbIM_LEDDrivesKB is set,
Xkb enables all of the boolean controls specified in the /ctrls/ field
of its indicator map. Explicitly extinguishing such an indicator causes
Xkb to disable all of the boolean controls specified in /ctrls./

For each bit set in the /which/ parameter, /XkbSetIndicatorMap/ sends
the corresponding indicator map from the /desc/ parameter to the server.

* SEE ALSO
*XkbSetNamedIndicator*(3)
