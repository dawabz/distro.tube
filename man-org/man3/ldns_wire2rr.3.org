#+TITLE: Manpages - ldns_wire2rr.3
#+DESCRIPTION: Linux manpage for ldns_wire2rr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_wire2rr, ldns_wire2pkt, ldns_wire2rdf, ldns_wire2dname - convert
from wire format to host type

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_status ldns_wire2rr(ldns_rr **rr, const uint8_t *wire, size_t max,
size_t *pos, ldns_pkt_section section);

ldns_status ldns_wire2pkt(ldns_pkt **packet, const uint8_t *data, size_t
len);

ldns_status ldns_wire2rdf(ldns_rr *rr, const uint8_t *wire, size_t max,
size_t *pos);

ldns_status ldns_wire2dname(ldns_rdf **dname, const uint8_t *wire,
size_t max, size_t *pos);

* DESCRIPTION
/ldns_wire2rr/() converts the data on the uint8_t bytearray (in wire
format) to a DNS resource record. This function will initialize and
allocate memory space for the rr structure. The length of the wiredata
of this rr is added to the *pos value.

.br *rr*: pointer to the structure to hold the rdata value .br *wire*:
pointer to the buffer with the data .br *max*: the length of the data
buffer (in bytes) .br *pos*: the position of the rr in the buffer (ie.
the number of bytes from the start of the buffer) .br *section*: the
section in the packet the rr is meant for .br Returns LDNS_STATUS_OK if
everything succeeds, error otherwise

/ldns_wire2pkt/() converts the data on the uint8_t bytearray (in wire
format) to a DNS packet. This function will initialize and allocate
memory space for the packet structure.

.br *packet*: pointer to the structure to hold the packet .br *data*:
pointer to the buffer with the data .br *len*: the length of the data
buffer (in bytes) .br Returns LDNS_STATUS_OK if everything succeeds,
error otherwise

/ldns_wire2rdf/() converts the data on the uint8_t bytearray (in wire
format) to DNS rdata fields, and adds them to the list of rdfs of the
given rr. This function will initialize and allocate memory space for
the dname structures. The length of the wiredata of these rdfs is added
to the *pos value.

All rdfs belonging to the RR are read; the rr should have no rdfs yet.
An error is returned if the format cannot be parsed.

.br *rr*: pointer to the ldns_rr structure to hold the rdata value .br
*wire*: pointer to the buffer with the data .br *max*: the length of the
data buffer (in bytes) .br *pos*: the position of the rdf in the buffer
(ie. the number of bytes from the start of the buffer) .br Returns
LDNS_STATUS_OK if everything succeeds, error otherwise

/ldns_wire2dname/() converts the data on the uint8_t bytearray (in wire
format) to a DNS dname rdata field. This function will initialize and
allocate memory space for the dname structure. The length of the
wiredata of this rdf is added to the *pos value.

.br *dname*: pointer to the structure to hold the rdata value .br
*wire*: pointer to the buffer with the data .br *max*: the length of the
data buffer (in bytes) .br *pos*: the position of the rdf in the buffer
(ie. the number of bytes from the start of the buffer) .br Returns
LDNS_STATUS_OK if everything succeeds, error otherwise

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_rr2wire/, /ldns_pkt2wire/, /ldns_rdf2wire/, /ldns_dname2wire/. And
*perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*, *RFC4034* and
*RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
