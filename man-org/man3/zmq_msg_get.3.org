#+TITLE: Manpages - zmq_msg_get.3
#+DESCRIPTION: Linux manpage for zmq_msg_get.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_msg_get - get message property

* SYNOPSIS
*int zmq_msg_get (zmq_msg_t */*message/*, int */property/*);*

* DESCRIPTION
The /zmq_msg_get()/ function shall return the value for the property
specified by the /property/ argument for the message pointed to by the
/message/ argument.

The following properties can be retrieved with the /zmq_msg_get()/
function:

*ZMQ_MORE*

#+begin_quote
  Indicates that there are more message frames to follow after the
  /message/.
#+end_quote

*ZMQ_SRCFD*

#+begin_quote
  Returns the file descriptor of the socket the /message/ was read from.
  This allows application to retrieve the remote endpoint via
  /getpeername(2)/. Be aware that the respective socket might be closed
  already, reused even. Currently only implemented for TCP sockets.
#+end_quote

*ZMQ_SHARED*

#+begin_quote
  Indicates that a message MAY share underlying storage with another
  copy of this message.
#+end_quote

* RETURN VALUE
The /zmq_msg_get()/ function shall return the value for the property if
successful. Otherwise it shall return -1 and set /errno/ to one of the
values defined below.

* ERRORS
*EINVAL*

#+begin_quote
  The requested /property/ is unknown.
#+end_quote

* EXAMPLE
*Receiving a multi-frame message*.

#+begin_quote
  #+begin_example
    zmq_msg_t frame;
    while (true) {
        //  Create an empty 0MQ message to hold the message frame
        int rc = zmq_msg_init (&frame);
        assert (rc == 0);
        //  Block until a message is available to be received from socket
        rc = zmq_msg_recv (socket, &frame, 0);
        assert (rc != -1);
        if (zmq_msg_get (&frame, ZMQ_MORE))
            fprintf (stderr, "more\n");
        else {
            fprintf (stderr, "end\n");
            break;
        }
        zmq_msg_close (&frame);
    }
  #+end_example
#+end_quote

* SEE ALSO
*zmq_msg_set*(3) *zmq_msg_init*(3) *zmq_msg_close*(3) *zmq*(7)

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
