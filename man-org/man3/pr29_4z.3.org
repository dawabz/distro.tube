#+TITLE: Manpages - pr29_4z.3
#+DESCRIPTION: Linux manpage for pr29_4z.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pr29_4z - API function

* SYNOPSIS
*#include <pr29.h>*

*int pr29_4z(const uint32_t * */in/*);*

* ARGUMENTS
- const uint32_t * in :: zero terminated array of Unicode code points.

* DESCRIPTION
Check the input to see if it may be normalized into different strings by
different NFKC implementations, due to an anomaly in the NFKC
specifications.

Return value: Returns the *Pr29_rc* value *PR29_SUCCESS* on success, and
*PR29_PROBLEM* if the input sequence is a "problem sequence" (i.e., may
be normalized into different strings by different implementations).

* REPORTING BUGS
Report bugs to <help-libidn@gnu.org>.\\
General guidelines for reporting bugs: http://www.gnu.org/gethelp/\\
GNU Libidn home page: http://www.gnu.org/software/libidn/

* COPYRIGHT
Copyright © 2002-2021 Simon Josefsson.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libidn* is maintained as a Texinfo manual.
If the *info* and *libidn* programs are properly installed at your site,
the command

#+begin_quote
  *info libidn*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *http://www.gnu.org/software/libidn/manual/*
#+end_quote
