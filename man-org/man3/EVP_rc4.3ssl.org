#+TITLE: Manpages - EVP_rc4.3ssl
#+DESCRIPTION: Linux manpage for EVP_rc4.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
EVP_rc4, EVP_rc4_40, EVP_rc4_hmac_md5 - EVP RC4 stream cipher

* SYNOPSIS
#include <openssl/evp.h> const EVP_CIPHER *EVP_rc4(void) const
EVP_CIPHER *EVP_rc4_40(void) const EVP_CIPHER *EVP_rc4_hmac_md5(void)

* DESCRIPTION
The RC4 stream cipher for EVP.

- EVP_rc4() :: RC4 stream cipher. This is a variable key length cipher
  with a default key length of 128 bits.

- EVP_rc4_40() :: RC4 stream cipher with 40 bit key length. WARNING:
  this function is obsolete. Its usage should be replaced with the
  *EVP_rc4()* and the *EVP_CIPHER_CTX_set_key_length()* functions.

- EVP_rc4_hmac_md5() :: Authenticated encryption with the RC4 stream
  cipher with MD5 as HMAC. WARNING: this is not intended for usage
  outside of TLS and requires calling of some undocumented ctrl
  functions. These ciphers do not conform to the EVP AEAD interface.

* RETURN VALUES
These functions return an *EVP_CIPHER* structure that contains the
implementation of the symmetric cipher. See *EVP_CIPHER_meth_new* (3)
for details of the *EVP_CIPHER* structure.

* SEE ALSO
*evp* (7), *EVP_EncryptInit* (3), *EVP_CIPHER_meth_new* (3)

* COPYRIGHT
Copyright 2017 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
