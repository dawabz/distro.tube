#+TITLE: Manpages - unibi_from_term.3
#+DESCRIPTION: Linux manpage for unibi_from_term.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
unibi_from_term - read a terminfo entry for a named terminal

* SYNOPSIS
#include <unibilium.h> unibi_term *unibi_from_term(const char *name);

* DESCRIPTION
This function locates the terminfo file for /name/, then calls
=unibi_from_file=.

It looks in the following places:

1. If the environment variable =TERMINFO= is set, it is interpreted as
   the name of the directory to search for local terminal definitions
   before checking in the standard place.

2. Otherwise, =$HOME/.terminfo= is tried.

3. If that was not successful and =TERMINFO_DIRS= is set, it is
   interpreted as a colon-separated list of directories to search.

4. If =TERMINFO_DIRS= is not set, a compiled-in fallback
   (=unibi_terminfo_dirs=) is used instead.

* RETURN VALUE
See *unibi_from_file* (3).

* SEE ALSO
*unibilium.h* (3), *unibi_from_file* (3), *unibi_terminfo_dirs* (3),
*unibi_destroy* (3)
