#+TITLE: Manpages - std_locale_id.3
#+DESCRIPTION: Linux manpage for std_locale_id.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::locale::id - Facet ID class.

* SYNOPSIS
\\

=#include <locale_classes.h>=

** Public Member Functions
*id* ()\\
Constructor.

size_t *_M_id* () const throw ()\\

** Friends
template<typename _Facet > bool *has_facet* (const *locale* &) throw
()\\
Test for the presence of a facet.

class *locale*\\

class *locale::_Impl*\\

template<typename _Facet > const _Facet & *use_facet* (const *locale*
&)\\
Return a facet.

* Detailed Description
Facet ID class.

The ID class provides facets with an index used to identify them. Every
facet class must define a public static member locale::id, or be derived
from a facet that provides this member, otherwise the facet cannot be
used in a locale. The locale::id ensures that each class type gets a
unique identifier.

Definition at line *485* of file *locale_classes.h*.

* Constructor & Destructor Documentation
** std::locale::id::id ()= [inline]=
Constructor.

Definition at line *516* of file *locale_classes.h*.

* Friends And Related Function Documentation
** template<typename _Facet > bool has_facet (const *locale*
&)= [friend]=
Test for the presence of a facet. has_facet tests the locale argument
for the presence of the facet type provided as the template parameter.
Facets derived from the facet parameter will also return true.

*Template Parameters*

#+begin_quote
  /_Facet/ The facet type to test the presence of.
#+end_quote

*Parameters*

#+begin_quote
  /__loc/ The locale to test.
#+end_quote

*Returns*

#+begin_quote
  true if =__loc= contains a facet of type _Facet, else false.
#+end_quote

Definition at line *104* of file *locale_classes.tcc*.

** friend class *locale*= [friend]=
Definition at line *488* of file *locale_classes.h*.

** friend class locale::_Impl= [friend]=
Definition at line *489* of file *locale_classes.h*.

** template<typename _Facet > const _Facet & use_facet (const *locale*
&)= [friend]=
Return a facet. use_facet looks for and returns a reference to a facet
of type Facet where Facet is the template parameter. If
has_facet(locale) is true, there is a suitable facet to return. It
throws std::bad_cast if the locale doesn't contain a facet of type
Facet.

*Template Parameters*

#+begin_quote
  /_Facet/ The facet type to access.
#+end_quote

*Parameters*

#+begin_quote
  /__loc/ The locale to use.
#+end_quote

*Returns*

#+begin_quote
  Reference to facet of type Facet.
#+end_quote

*Exceptions*

#+begin_quote
  /std::bad_cast/ if =__loc= doesn't contain a facet of type _Facet.
#+end_quote

Definition at line *132* of file *locale_classes.tcc*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
