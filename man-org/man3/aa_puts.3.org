#+TITLE: Manpages - aa_puts.3
#+DESCRIPTION: Linux manpage for aa_puts.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
aa_puts - output string to AA-lib output buffers.

* SYNOPSIS
#include <aalib.h>

void aa_puts\\
(\\
aa_context *c,\\
int x,\\
int y,\\
enum aa_attribute attr,\\
const char *s\\
);

* PARAMETERS
- *aa_context *c* :: Specifies the AA-lib context to operate on.

- *int x* :: X coordinate of the first character.

- *int y* :: Y coordinate of the first character.

- *enum aa_attribute attr* :: Attribute to use.

Possible values for an *enum aa_attribute* are as follows:

#+begin_quote
  - *AA_NORMAL* :: Normal characters.

  - *AA_DIM* :: Dark characters.

  - *AA_BOLD* :: Bright characters.

  - *AA_BOLDFONT* :: Characters rendered in bold font.

  - *AA_REVERSE* :: Reversed (black on whilte) characters.

  - *AA_SPECIAL* :: Render characters in a way easilly visible on the
    screen. The exact rendering is driver dependent, but this mode ought
    to be used to output texts you want to make easilly visible in the
    image.
#+end_quote

- *const char *s* :: String to output.

* DESCRIPTION
Output given string to AA-lib output buffers. To see the effect you need
to call aa_flush too.

* SEE ALSO
save_d(3), mem_d(3), aa_help(3), aa_formats(3), aa_fonts(3),
aa_dithernames(3), aa_drivers(3), aa_kbddrivers(3), aa_mousedrivers(3),
aa_kbdrecommended(3), aa_mouserecommended(3), aa_displayrecommended(3),
aa_defparams(3), aa_defrenderparams(3), aa_scrwidth(3), aa_scrheight(3),
aa_mmwidth(3), aa_mmheight(3), aa_imgwidth(3), aa_imgheight(3),
aa_image(3), aa_text(3), aa_attrs(3), aa_currentfont(3), aa_autoinit(3),
aa_autoinitkbd(3), aa_autoinitmouse(3), aa_recommendhi(3),
aa_recommendlow(3), aa_init(3), aa_initkbd(3), aa_initmouse(3),
aa_close(3), aa_uninitkbd(3), aa_uninitmouse(3), aa_fastrender(3),
aa_render(3), aa_printf(3), aa_gotoxy(3), aa_hidecursor(3),
aa_showcursor(3), aa_getmouse(3), aa_hidemouse(3), aa_showmouse(3),
aa_registerfont(3), aa_setsupported(3), aa_setfont(3), aa_getevent(3),
aa_getkey(3), aa_resize(3), aa_resizehandler(3), aa_parseoptions(3),
aa_edit(3), aa_createedit(3), aa_editkey(3), aa_putpixel(3),
aa_recommendhikbd(3), aa_recommendlowkbd(3), aa_recommendhimouse(3),
aa_recommendlowmouse(3), aa_recommendhidisplay(3),
aa_recommendlowdisplay(3)
