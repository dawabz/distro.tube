#+TITLE: Manpages - MPI_Is_thread_main.3
#+DESCRIPTION: Linux manpage for MPI_Is_thread_main.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Is_thread_main* - Determines if thread called MPI_Init

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Is_thread_main(int *flag)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_IS_THREAD_MAIN(FLAG, IERROR)
  	LOGICAL	FLAG
  	INTEGER	IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Is_thread_main(flag, ierror)
  	LOGICAL, INTENT(OUT) :: flag
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  bool MPI::Is_thread_main()
#+end_example

* OUTPUT PARAMETERS
- flag :: True if calling thread is main thread (boolean).

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
MPI_Is_thread_main is called by a thread to find out whether the caller
is the main thread (that is, the thread that called MPI_Init or
MPI_Init_thread).

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.

See the MPI man page for a full list of MPI error codes.

* SEE ALSO
#+begin_example
  MPI_Init
  MPI_Init_thread
#+end_example
