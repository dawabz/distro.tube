#+TITLE: Manpages - unw_set_cache_size.3
#+DESCRIPTION: Linux manpage for unw_set_cache_size.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
unw_set_cache_size -- set unwind cache size

* SYNOPSIS
#include <libunwind.h>\\

int unw_set_cache_size(unw_addr_space_t as, size_t size, int flag);\\

* DESCRIPTION
The unw_set_cache_size() routine sets the cache size of address space as
to hold at least as many items as given by argument size. It may hold
more items as determined by the implementation. To disable caching, call
unw_set_caching_policy) with a policy of UNW_CACHE_NONE. Flag is
currently unused and must be 0.

* RETURN VALUE
On successful completion, unw_set_cache_size() returns 0. Otherwise the
negative value of one of the error-codes below is returned.

* THREAD AND SIGNAL SAFETY
unw_set_cache_size() is thread-safe but /not/ safe to use from a signal
handler.

* ERRORS
- UNW_ENOMEM ::  The desired cache size could not be established because
  the application is out of memory.

* SEE ALSO
libunwind(3), unw_create_addr_space(3), unw_set_caching_policy(3),
unw_flush_cache(3)

* AUTHOR
Dave Watson\\
Email: *dade.watson@gmail.com*\\
WWW: *http://www.nongnu.org/libunwind/*.
