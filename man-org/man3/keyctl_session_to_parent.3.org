#+TITLE: Manpages - keyctl_session_to_parent.3
#+DESCRIPTION: Linux manpage for keyctl_session_to_parent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
keyctl_session_to_parent - set the parent process's session keyring

* SYNOPSIS
#+begin_example
  #include <keyutils.h>

  long keyctl_session_to_parent();
#+end_example

* DESCRIPTION
*keyctl_session_to_parent*() changes the session keyring to which the
calling process's parent subscribes to be the that of the calling
process.

The keyring must have *link* permission available to the calling
process, the parent process must have the same UIDs/GIDs as the calling
process, and the LSM must not reject the replacement. Furthermore, this
may not be used to affect init or a kernel thread.

Note that the replacement will not take immediate effect upon the parent
process, but will rather be deferred to the next time it returns to
userspace from kernel space.

* RETURN VALUE
On success *keyctl_session_to_parent*() returns 0. On error, the value
*-1* will be returned and /errno/ will have been set to an appropriate
error.

* ERRORS
- *ENOMEM* :: Insufficient memory to create a key.

- *EPERM* :: The credentials of the parent don't match those of the
  caller.

- *EACCES* :: The named keyring exists, but is not *linkable* by the
  calling process.

* LINKING
This is a library function that can be found in /libkeyutils/. When
linking, *-lkeyutils* should be specified to the linker.

* SEE ALSO
*keyctl*(1), *add_key*(2), *keyctl*(2), *request_key*(2), *keyctl*(3),
*keyrings*(7), *keyutils*(7), *session-keyring*(7),
*user-session-keyring*(7)
