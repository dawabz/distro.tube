#+TITLE: Manpages - FcBlanksIsMember.3
#+DESCRIPTION: Linux manpage for FcBlanksIsMember.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcBlanksIsMember - Query membership in an FcBlanks

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcBlanksIsMember (FcBlanks */b/*, FcChar32 */ucs4/*);*

* DESCRIPTION
FcBlanks is deprecated. This function always returns FALSE.
