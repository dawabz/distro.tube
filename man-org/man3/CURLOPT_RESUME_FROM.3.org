#+TITLE: Manpages - CURLOPT_RESUME_FROM.3
#+DESCRIPTION: Linux manpage for CURLOPT_RESUME_FROM.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_RESUME_FROM - offset to resume transfer from

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_RESUME_FROM, long from);

* DESCRIPTION
Pass a long as parameter. It contains the offset in number of bytes that
you want the transfer to start from. Set this option to 0 to make the
transfer start from the beginning (effectively disabling resume). For
FTP, set this option to -1 to make the transfer start from the end of
the target file (useful to continue an interrupted upload).

When doing uploads with FTP, the resume position is where in the
local/source file libcurl should try to resume the upload from and it
will then append the source file to the remote target file.

If you need to resume a transfer beyond the 2GB limit, use
/CURLOPT_RESUME_FROM_LARGE(3)/ instead.

* DEFAULT
0, not used

* PROTOCOLS
HTTP, FTP, SFTP, FILE

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com");

    /* resume upload at byte index 200 */
    curl_easy_setopt(curl, CURLOPT_RESUME_FROM, 200L);

    /* ask for upload */
    curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);

    /* set total data amount to expect */
    curl_easy_setopt(curl, CURLOPT_INFILESIZE, size_of_file);

    /* Perform the request */
    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Always

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*CURLOPT_RESUME_FROM_LARGE*(3), *CURLOPT_RANGE*(3),
*CURLOPT_INFILESIZE*(3),
