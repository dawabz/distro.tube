#+TITLE: Manpages - FcLangSetCompare.3
#+DESCRIPTION: Linux manpage for FcLangSetCompare.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcLangSetCompare - compare language sets

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcLangResult FcLangSetCompare (const FcLangSet */ls_a/*, const FcLangSet
**/ls_b/*);*

* DESCRIPTION
*FcLangSetCompare* compares language coverage for /ls_a/ and /ls_b/. If
they share any language and territory pair, this function returns
FcLangEqual. If they share a language but differ in which territory that
language is for, this function returns FcLangDifferentTerritory. If they
share no languages in common, this function returns FcLangDifferentLang.
