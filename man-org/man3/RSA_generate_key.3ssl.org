#+TITLE: Manpages - RSA_generate_key.3ssl
#+DESCRIPTION: Linux manpage for RSA_generate_key.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
RSA_generate_key_ex, RSA_generate_key, RSA_generate_multi_prime_key -
generate RSA key pair

* SYNOPSIS
#include <openssl/rsa.h> int RSA_generate_key_ex(RSA *rsa, int bits,
BIGNUM *e, BN_GENCB *cb); int RSA_generate_multi_prime_key(RSA *rsa, int
bits, int primes, BIGNUM *e, BN_GENCB *cb);

Deprecated:

#if OPENSSL_API_COMPAT < 0x00908000L RSA *RSA_generate_key(int bits,
unsigned long e, void (*callback)(int, int, void *), void *cb_arg);
#endif

* DESCRIPTION
*RSA_generate_key_ex()* generates a 2-prime RSA key pair and stores it
in the *RSA* structure provided in *rsa*. The pseudo-random number
generator must be seeded prior to calling *RSA_generate_key_ex()*.

*RSA_generate_multi_prime_key()* generates a multi-prime RSA key pair
and stores it in the *RSA* structure provided in *rsa*. The number of
primes is given by the *primes* parameter. The random number generator
must be seeded when calling *RSA_generate_multi_prime_key()*. If the
automatic seeding or reseeding of the OpenSSL CSPRNG fails due to
external circumstances (see *RAND* (7)), the operation will fail.

The modulus size will be of length *bits*, the number of primes to form
the modulus will be *primes*, and the public exponent will be *e*. Key
sizes with *num* < 1024 should be considered insecure. The exponent is
an odd number, typically 3, 17 or 65537.

In order to maintain adequate security level, the maximum number of
permitted *primes* depends on modulus bit length:

<1024 | >=1024 | >=4096 | >=8192 ------+--------+--------+------- 2 | 3
| 4 | 5

A callback function may be used to provide feedback about the progress
of the key generation. If *cb* is not *NULL*, it will be called as
follows using the *BN_GENCB_call()* function described on the
*BN_generate_prime* (3) page.

*RSA_generate_key()* is similar to *RSA_generate_key_ex()* but expects
an old-style callback function; see *BN_generate_prime* (3) for
information on the old-style callback.

- While a random prime number is generated, it is called as described in
  *BN_generate_prime* (3).

- When the n-th randomly generated prime is rejected as not suitable for
  the key, *BN_GENCB_call(cb, 2, n)* is called.

- When a random p has been found with p-1 relatively prime to *e*, it is
  called as *BN_GENCB_call(cb, 3, 0)*.

The process is then repeated for prime q and other primes (if any) with
*BN_GENCB_call(cb, 3, i)* where *i* indicates the i-th prime.

* RETURN VALUES
*RSA_generate_multi_prime_key()* returns 1 on success or 0 on error.
*RSA_generate_key_ex()* returns 1 on success or 0 on error. The error
codes can be obtained by *ERR_get_error* (3).

*RSA_generate_key()* returns a pointer to the RSA structure or *NULL* if
the key generation fails.

* BUGS
*BN_GENCB_call(cb, 2, x)* is used with two different meanings.

* SEE ALSO
*ERR_get_error* (3), *RAND_bytes* (3), *BN_generate_prime* (3),
*RAND* (7)

* HISTORY
*RSA_generate_key()* was deprecated in OpenSSL 0.9.8; use
*RSA_generate_key_ex()* instead.

* COPYRIGHT
Copyright 2000-2019 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
