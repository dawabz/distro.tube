#+TITLE: Manpages - xcb_create_notify_event_t.3
#+DESCRIPTION: Linux manpage for xcb_create_notify_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_create_notify_event_t -

* SYNOPSIS
*#include <xcb/xproto.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_create_notify_event_t {
      uint8_t      response_type;
      uint8_t      pad0;
      uint16_t     sequence;
      xcb_window_t parent;
      xcb_window_t window;
      int16_t      x;
      int16_t      y;
      uint16_t     width;
      uint16_t     height;
      uint16_t     border_width;
      uint8_t      override_redirect;
      uint8_t      pad1;
  } xcb_create_notify_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_CREATE_NOTIFY/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- parent :: NOT YET DOCUMENTED.

- window :: NOT YET DOCUMENTED.

24. NOT YET DOCUMENTED.

25. NOT YET DOCUMENTED.

- width :: NOT YET DOCUMENTED.

- height :: NOT YET DOCUMENTED.

- border_width :: NOT YET DOCUMENTED.

- override_redirect :: NOT YET DOCUMENTED.

* DESCRIPTION
* SEE ALSO
* AUTHOR
Generated from xproto.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
