#+TITLE: Manpages - libspiro.3
#+DESCRIPTION: Linux manpage for libspiro.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libspiro - A clothoid to bezier spline converter

* SYNOPSIS
*#include <spiroentrypoints.h>* and then compile and link with
*-lspiro*\\

* DESCRIPTION
*Spiro* library for curve design which simplifies drawing of beautiful
curves.

*libspiro* takes an array of spiro control points which can be easier
for an artist to create and modify, and then converts these into a
series of bezier splines which can then be used in the myriad of ways
the world has come to use beziers.

/spiroentrypoints.h/ has technical information on how to connect with
/libspiro/ and further information can be found at
/http://github.com/fontforge/libspiro/

* REPORTING PROBLEMS
Before reporting a problem, please check the libspiro web site to verify
that you have the latest version of /libspiro/

Great care has been taken to maintain backwards compatibility so it is
recommended to upgrade if you experience problems with earlier
/libspiro/ versions.

* AUTHORS AND LICENSE
*libspiro* originated from *ppedit* which was a pattern plate editor for
Spiro splines. Copyright (C) 2007 Raph Levien. GNU GPL version 2 or
higher.

This version of *libspiro* Copyright (C) 2007-2020 is GNU GPL version 3
or higher and contains a number of significant improvements and fixes.

Please see /AUTHORS/ file for everyone involved in making and improving
/libspiro/

Further details (on why and what) can also be seen in git history.
