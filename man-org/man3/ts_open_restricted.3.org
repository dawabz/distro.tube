#+TITLE: Manpages - ts_open_restricted.3
#+DESCRIPTION: Linux manpage for ts_open_restricted.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ts_open_restricted - use a custom function for opening the touchscreen's
input device file

* SYNOPSIS
#+begin_example
  #include <tslib.h>

  "int(*ts_open_restricted)(constchar*path,intflags,void*user_data);
#+end_example

* DESCRIPTION
*ts_open_restricted*() is useful if libts should not be run as root. If
such a function is implemented by the user and assigned to the
ts_open_restricted pointer, it will be called by ts_open() instead of
the open() system call directly.

It should open the input device at *path* with *flags* while *user_data*
is currently unused.

* RETURN VALUE
the touchscreen input device' file descriptor

* SEE ALSO
*ts_close_restricted*(3), *ts_open*(3), *ts_setup*(3), *ts_close*(3),
*ts.conf*(5)
