#+TITLE: Manpages - __gnu_debug__BeforeBeginHelper.3
#+DESCRIPTION: Linux manpage for __gnu_debug__BeforeBeginHelper.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_debug::_BeforeBeginHelper< _Sequence >

* SYNOPSIS
\\

=#include <safe_iterator.h>=

** Static Public Member Functions
template<typename _Iterator , typename _Category > static bool *_S_Is*
(const *_Safe_iterator*< _Iterator, _Sequence, _Category > &)\\

template<typename _Iterator , typename _Category > static bool
*_S_Is_Beginnest* (const *_Safe_iterator*< _Iterator, _Sequence,
_Category > &__it)\\

* Detailed Description
** "template<typename _Sequence>
\\
struct __gnu_debug::_BeforeBeginHelper< _Sequence >"Helper struct to
deal with sequence offering a before_begin iterator.

Definition at line *72* of file *safe_iterator.h*.

* Member Function Documentation
** template<typename _Sequence > template<typename _Iterator , typename
_Category > static bool *__gnu_debug::_BeforeBeginHelper*< _Sequence
>::_S_Is (const *_Safe_iterator*< _Iterator, _Sequence, _Category >
&)= [inline]=, = [static]=
Definition at line *76* of file *safe_iterator.h*.

** template<typename _Sequence > template<typename _Iterator , typename
_Category > static bool *__gnu_debug::_BeforeBeginHelper*< _Sequence
>::_S_Is_Beginnest (const *_Safe_iterator*< _Iterator, _Sequence,
_Category > & __it)= [inline]=, = [static]=
Definition at line *81* of file *safe_iterator.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
