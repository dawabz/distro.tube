#+TITLE: Manpages - libgphoto2.3
#+DESCRIPTION: Linux manpage for libgphoto2.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libgphoto2 - cross-platform digital camera library

* SYNOPSIS
#+begin_example
  #include <gphoto2/gphoto2.h>
#+end_example

* DESCRIPTION
The gphoto2 library provides applications with access to a variety of
cameras.

This man page will be extended with autogenerated documentation of the
interface types and methods used for communication between the gphoto2
library and a frontend.

* FILES
- /~/.gphoto/settings/ :: Here gphoto2 applications may store their
  configuration used to access gphoto2.

To track down errors, you can add the *--debug* parameter to the
*gphoto2* command line and, if dealing with USB problems, setting the
environment variable *USB_DEBUG*/=1/.

* ENVIRONMENT VARIABLES
- *CAMLIBS* :: If set, defines the directory where the libgphoto2
  library looks for its camera drivers (camlibs). You only need to set
  this on OS/2 systems and broken/test installations.

- *IOLIBS* :: If set, defines the directory where the libgphoto2_port
  library looks for its I/O drivers (iolibs). You only need to set this
  on OS/2 systems and broken/test installations.

- *LD_DEBUG* :: Set this to /all/ to receive lots of debug information
  regarding library loading on *ld* based systems.

- *USB_DEBUG* :: If set, defines the numeric debug level with which the
  libusb library will print messages. In order to get some debug output,
  set it to /1/.

* SEE ALSO
gphoto2(1), libgphoto2_port(3), The gPhoto2 Manual, automatically
generated API docs, [1]/gphoto website/

* AUTHOR
The gPhoto2 Team.\\
Hans Ulrich Niedermann <gp@n-dimensional.de>. (man page)

* REFERENCES
- 1. gphoto website :: http://www.gphoto.org/
