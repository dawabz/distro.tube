#+TITLE: Manpages - ts_fd.3
#+DESCRIPTION: Linux manpage for ts_fd.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ts_fd - get the file descriptor to a touchscreen device

* SYNOPSIS
#+begin_example
  #include <tslib.h>

  int ts_fd(struct tsdev *);
#+end_example

* DESCRIPTION
*ts_fd*() gets an open touchscreen device' file descriptor.

* RETURN VALUE
The file descriptor value is returned. *struct tsdev* is returned.

* SEE ALSO
*ts_setup*(3), *ts_close*(3), *ts_read*(3), *ts.conf*(5)
