#+TITLE: Manpages - FcStrListNext.3
#+DESCRIPTION: Linux manpage for FcStrListNext.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcStrListNext - get next string in iteration

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcChar8 * FcStrListNext (FcStrList */list/*);*

* DESCRIPTION
Returns the next string in /list/.
