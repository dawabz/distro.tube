#+TITLE: Manpages - XtMergeArgLists.3
#+DESCRIPTION: Linux manpage for XtMergeArgLists.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtMergeArgLists.3 is found in manpage for: [[../man3/XtSetArg.3][man3/XtSetArg.3]]