#+TITLE: Manpages - svcerr_auth.3
#+DESCRIPTION: Linux manpage for svcerr_auth.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about svcerr_auth.3 is found in manpage for: [[../man3/rpc.3][man3/rpc.3]]