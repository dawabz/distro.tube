#+TITLE: Manpages - std_experimental_filesystem_filesystem_error.3
#+DESCRIPTION: Linux manpage for std_experimental_filesystem_filesystem_error.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::experimental::filesystem::filesystem_error - Exception type thrown
by the Filesystem TS library.

* SYNOPSIS
\\

=#include <fs_path.h>=

Inherits *std::system_error*.

** Public Member Functions
*filesystem_error* (const *string* &__what_arg, const *path* &__p1,
const *path* &__p2, *error_code* __ec)\\

*filesystem_error* (const *string* &__what_arg, const *path* &__p1,
*error_code* __ec)\\

*filesystem_error* (const *string* &__what_arg, *error_code* __ec)\\

const *error_code* & *code* () const noexcept\\

const *path* & *path1* () const noexcept\\

const *path* & *path2* () const noexcept\\

const char * *what* () const noexcept\\

* Detailed Description
Exception type thrown by the Filesystem TS library.

Definition at line *703* of file *experimental/bits/fs_path.h*.

* Constructor & Destructor Documentation
** std::experimental::filesystem::filesystem_error::filesystem_error
(const *string* & __what_arg, *error_code* __ec)= [inline]=
Definition at line *706* of file *experimental/bits/fs_path.h*.

** std::experimental::filesystem::filesystem_error::filesystem_error
(const *string* & __what_arg, const *path* & __p1, *error_code*
__ec)= [inline]=
Definition at line *709* of file *experimental/bits/fs_path.h*.

** std::experimental::filesystem::filesystem_error::filesystem_error
(const *string* & __what_arg, const *path* & __p1, const *path* & __p2,
*error_code* __ec)= [inline]=
Definition at line *713* of file *experimental/bits/fs_path.h*.

* Member Function Documentation
** const *path* & std::experimental::filesystem::filesystem_error::path1
() const= [inline]=, = [noexcept]=
Definition at line *720* of file *experimental/bits/fs_path.h*.

** const *path* & std::experimental::filesystem::filesystem_error::path2
() const= [inline]=, = [noexcept]=
Definition at line *721* of file *experimental/bits/fs_path.h*.

** const char * std::experimental::filesystem::filesystem_error::what ()
const= [inline]=, = [virtual]=, = [noexcept]=
Returns a C-style character string describing the general cause of the
current error (the same string passed to the ctor).\\

Reimplemented from *std::runtime_error*.

Definition at line *722* of file *experimental/bits/fs_path.h*.

References *std::basic_string< _CharT, _Traits, _Alloc >::c_str()*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
