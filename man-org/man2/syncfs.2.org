#+TITLE: Manpages - syncfs.2
#+DESCRIPTION: Linux manpage for syncfs.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about syncfs.2 is found in manpage for: [[../man2/sync.2][man2/sync.2]]