#+TITLE: Manpages - sgetmask.2
#+DESCRIPTION: Linux manpage for sgetmask.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sgetmask, ssetmask - manipulation of signal mask (obsolete)

* SYNOPSIS
#+begin_example
  #include <sys/syscall.h> /* Definition of SYS_* constants */
  #include <unistd.h>

  long syscall(SYS_sgetmask, void);
  long syscall(SYS_ssetmask, long newmask);
#+end_example

/Note/: glibc provides no wrappers for these functions, necessitating
the use of *syscall*(2).

* DESCRIPTION
These system calls are obsolete. /Do not use them/; use *sigprocmask*(2)
instead.

*sgetmask*() returns the signal mask of the calling process.

*ssetmask*() sets the signal mask of the calling process to the value
given in /newmask/. The previous signal mask is returned.

The signal masks dealt with by these two system calls are plain bit
masks (unlike the /sigset_t/ used by *sigprocmask*(2)); use *sigmask*(3)
to create and inspect these masks.

* RETURN VALUE
*sgetmask*() always successfully returns the signal mask. *ssetmask*()
always succeeds, and returns the previous signal mask.

* ERRORS
These system calls always succeed.

* VERSIONS
Since Linux 3.16, support for these system calls is optional, depending
on whether the kernel was built with the *CONFIG_SGETMASK_SYSCALL*
option.

* CONFORMING TO
These system calls are Linux-specific.

* NOTES
These system calls are unaware of signal numbers greater than 31 (i.e.,
real-time signals).

These system calls do not exist on x86-64.

It is not possible to block *SIGSTOP* or *SIGKILL*.

* SEE ALSO
*sigprocmask*(2), *signal*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
