#+TITLE: Manpages - openat.2
#+DESCRIPTION: Linux manpage for openat.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about openat.2 is found in manpage for: [[../man2/open.2][man2/open.2]]