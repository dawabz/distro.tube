#+TITLE: Manpages - _sysctl.2
#+DESCRIPTION: Linux manpage for _sysctl.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about _sysctl.2 is found in manpage for: [[../man2/sysctl.2][man2/sysctl.2]]