#+TITLE: Manpages - ioctl_xfs_fssetxattr.2
#+DESCRIPTION: Linux manpage for ioctl_xfs_fssetxattr.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ioctl_xfs_fssetxattr.2 is found in manpage for: [[../man2/ioctl_xfs_fsgetxattr.2][man2/ioctl_xfs_fsgetxattr.2]]