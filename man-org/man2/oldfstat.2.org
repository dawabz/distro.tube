#+TITLE: Manpages - oldfstat.2
#+DESCRIPTION: Linux manpage for oldfstat.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about oldfstat.2 is found in manpage for: [[../man2/stat.2][man2/stat.2]]