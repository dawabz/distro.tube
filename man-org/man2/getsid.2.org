#+TITLE: Manpages - getsid.2
#+DESCRIPTION: Linux manpage for getsid.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
getsid - get session ID

* SYNOPSIS
#+begin_example
  #include <unistd.h>

  pid_t getsid(pid_t pid);
#+end_example

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*getsid*():

#+begin_example
      _XOPEN_SOURCE >= 500
          || /* Since glibc 2.12: */ _POSIX_C_SOURCE >= 200809L
#+end_example

* DESCRIPTION
/getsid(0)/ returns the session ID of the calling process. *getsid*()
returns the session ID of the process with process ID /pid/. If /pid/ is
0, *getsid*() returns the session ID of the calling process.

* RETURN VALUE
On success, a session ID is returned. On error, /(pid_t) -1/ is
returned, and /errno/ is set to indicate the error.

* ERRORS
- *EPERM* :: A process with process ID /pid/ exists, but it is not in
  the same session as the calling process, and the implementation
  considers this an error.

- *ESRCH* :: No process with process ID /pid/ was found.

* VERSIONS
This system call is available on Linux since version 2.0.

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008, SVr4.

* NOTES
Linux does not return *EPERM*.

See *credentials*(7) for a description of sessions and session IDs.

* SEE ALSO
*getpgid*(2), *setsid*(2), *credentials*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
