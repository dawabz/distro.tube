#+TITLE: Manpages - fchmodat.2
#+DESCRIPTION: Linux manpage for fchmodat.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about fchmodat.2 is found in manpage for: [[../man2/chmod.2][man2/chmod.2]]