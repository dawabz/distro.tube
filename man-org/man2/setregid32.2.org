#+TITLE: Manpages - setregid32.2
#+DESCRIPTION: Linux manpage for setregid32.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about setregid32.2 is found in manpage for: [[../man2/setregid.2][man2/setregid.2]]