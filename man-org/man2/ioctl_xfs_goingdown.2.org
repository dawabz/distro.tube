#+TITLE: Manpages - ioctl_xfs_goingdown.2
#+DESCRIPTION: Linux manpage for ioctl_xfs_goingdown.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ioctl_xfs_goingdown - shut down an XFS filesystem

* SYNOPSIS
\\
*#include <xfs/xfs_fs.h>*

*int ioctl(int */fd/*, XFS_IOC_GOINGDOWN, uint32_t */flags/*);*

* DESCRIPTION
Shuts down a live XFS filesystem. This is a software initiated hard
shutdown and should be avoided whenever possible. After this call
completes, the filesystem ill be totally unusable until the filesystem
has been unmounted and remounted.

/flags/ can be one of the following:

#+begin_quote
  - *XFS_FSOP_GOING_FLAGS_DEFAULT* :: Flush all dirty data and metadata
    to disk, flush pending transactions to the log, and shut down.

  - *XFS_FSOP_GOING_FLAGS_LOGFLUSH* :: Flush all pending metadata
    transactions to the log and shut down, leaving all dirty data
    unwritten.

  - *XFS_FSOP_GOING_FLAGS_NOLOGFLUSH* :: Shut down immediately, without
    writing pending transactions or dirty data to disk.
#+end_quote

* RETURN VALUE
On error, -1 is returned, and /errno/ is set to indicate the error.

* ERRORS
Error codes can be one of, but are not limited to, the following:

- *EFSBADCRC* :: Metadata checksum validation failed while performing
  the query.

- *EFSCORRUPTED* :: Metadata corruption was encountered while performing
  the query.

- *EIO* :: An I/O error was encountered while performing the query.

- *EPERM* :: Caller did not have permission to shut down the filesystem.

* CONFORMING TO
This API is specific to XFS filesystem on the Linux kernel.

* SEE ALSO
*ioctl*(2)
