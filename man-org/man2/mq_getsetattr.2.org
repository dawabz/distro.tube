#+TITLE: Manpages - mq_getsetattr.2
#+DESCRIPTION: Linux manpage for mq_getsetattr.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
mq_getsetattr - get/set message queue attributes

* SYNOPSIS
#+begin_example
  #include <mqueue.h> /* Definition of struct mq_attr */
  #include <sys/syscall.h> /* Definition of SYS_* constants */
  #include <unistd.h>

  int syscall(SYS_mq_getsetattr, mqd_t mqdes,
   const struct mq_attr *newattr, struct mq_attr *oldattr);
#+end_example

* DESCRIPTION
Do not use this system call.

This is the low-level system call used to implement *mq_getattr*(3) and
*mq_setattr*(3). For an explanation of how this system call operates,
see the description of *mq_setattr*(3).

* CONFORMING TO
This interface is nonstandard; avoid its use.

* NOTES
Never call it unless you are writing a C library!

* SEE ALSO
*mq_getattr*(3), *mq_overview*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
