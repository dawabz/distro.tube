#+TITLE: Manpages - capset.2
#+DESCRIPTION: Linux manpage for capset.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about capset.2 is found in manpage for: [[../man2/capget.2][man2/capget.2]]