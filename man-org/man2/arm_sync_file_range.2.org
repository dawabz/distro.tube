#+TITLE: Manpages - arm_sync_file_range.2
#+DESCRIPTION: Linux manpage for arm_sync_file_range.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about arm_sync_file_range.2 is found in manpage for: [[../man2/sync_file_range.2][man2/sync_file_range.2]]