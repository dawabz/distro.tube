#+TITLE: Manpages - umount2.2
#+DESCRIPTION: Linux manpage for umount2.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about umount2.2 is found in manpage for: [[../man2/umount.2][man2/umount.2]]