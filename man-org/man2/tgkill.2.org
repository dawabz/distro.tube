#+TITLE: Manpages - tgkill.2
#+DESCRIPTION: Linux manpage for tgkill.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about tgkill.2 is found in manpage for: [[../man2/tkill.2][man2/tkill.2]]