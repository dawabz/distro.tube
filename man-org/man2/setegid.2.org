#+TITLE: Manpages - setegid.2
#+DESCRIPTION: Linux manpage for setegid.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about setegid.2 is found in manpage for: [[../man2/seteuid.2][man2/seteuid.2]]