#+TITLE: Manpages - setrlimit.2
#+DESCRIPTION: Linux manpage for setrlimit.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about setrlimit.2 is found in manpage for: [[../man2/getrlimit.2][man2/getrlimit.2]]