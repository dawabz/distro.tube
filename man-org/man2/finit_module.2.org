#+TITLE: Manpages - finit_module.2
#+DESCRIPTION: Linux manpage for finit_module.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about finit_module.2 is found in manpage for: [[../man2/init_module.2][man2/init_module.2]]