#+TITLE: Manpages - ioctl_xfs_getresblks.2
#+DESCRIPTION: Linux manpage for ioctl_xfs_getresblks.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ioctl_xfs_getresblks - query and set XFS free space reservation
information

* SYNOPSIS
\\
*#include <xfs/xfs_fs.h>*

*int ioctl(int */fd/*, XFS_IOC_GET_RESBLKS, struct xfs_fsop_resblks
**/arg/*);*\\
*int ioctl(int */fd/*, XFS_IOC_SET_RESBLKS, struct xfs_fsop_resblks
**/arg/*);*

* DESCRIPTION
Query or set the free space reservation information. These blocks are
reserved by the filesystem as a final attempt to prevent metadata update
failures due to insufficient space. Only the system administrator can
use these ioctls, because overriding the defaults is extremely
dangerous.

This functionality is intended only for use by XFS filesystem
developers.

The reservation information is conveyed in a structure of the following
form:

#+begin_example
  struct xfs_fsop_resblks {
  	__u64  resblks;
  	__u64  resblks_avail;
  };
#+end_example

/resblks/ is the number of blocks that the filesystem will try to
maintain to prevent critical out of space situations.

/resblks_avail/ is the number of reserved blocks remaining.

* RETURN VALUE
On error, -1 is returned, and /errno/ is set to indicate the error.

* ERRORS
Error codes can be one of, but are not limited to, the following:

- *EFSBADCRC* :: Metadata checksum validation failed while performing
  the query.

- *EFSCORRUPTED* :: Metadata corruption was encountered while performing
  the query.

- *EINVAL* :: The specified allocation group number is not valid for
  this filesystem.

- *EIO* :: An I/O error was encountered while performing the query.

- *EPERM* :: Caller does not have permission to call this ioctl.

* CONFORMING TO
This API is specific to XFS filesystem on the Linux kernel.

* SEE ALSO
*ioctl*(2)
