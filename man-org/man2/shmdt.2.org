#+TITLE: Manpages - shmdt.2
#+DESCRIPTION: Linux manpage for shmdt.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about shmdt.2 is found in manpage for: [[../man2/shmop.2][man2/shmop.2]]