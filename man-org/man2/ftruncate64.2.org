#+TITLE: Manpages - ftruncate64.2
#+DESCRIPTION: Linux manpage for ftruncate64.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ftruncate64.2 is found in manpage for: [[../man2/ftruncate.2][man2/ftruncate.2]]