#+TITLE: Manpages - pciconfig_write.2
#+DESCRIPTION: Linux manpage for pciconfig_write.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about pciconfig_write.2 is found in manpage for: [[../man2/pciconfig_read.2][man2/pciconfig_read.2]]