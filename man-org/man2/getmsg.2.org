#+TITLE: Manpages - getmsg.2
#+DESCRIPTION: Linux manpage for getmsg.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about getmsg.2 is found in manpage for: [[../man2/unimplemented.2][man2/unimplemented.2]]