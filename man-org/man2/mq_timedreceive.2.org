#+TITLE: Manpages - mq_timedreceive.2
#+DESCRIPTION: Linux manpage for mq_timedreceive.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about mq_timedreceive.2 is found in manpage for: [[../man3/mq_timedreceive.3][man3/mq_timedreceive.3]]