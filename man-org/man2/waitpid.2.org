#+TITLE: Manpages - waitpid.2
#+DESCRIPTION: Linux manpage for waitpid.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about waitpid.2 is found in manpage for: [[../man2/wait.2][man2/wait.2]]