#+TITLE: Manpages - symlinkat.2
#+DESCRIPTION: Linux manpage for symlinkat.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about symlinkat.2 is found in manpage for: [[../man2/symlink.2][man2/symlink.2]]