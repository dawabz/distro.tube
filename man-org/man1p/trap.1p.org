#+TITLE: Manpages - trap.1p
#+DESCRIPTION: Linux manpage for trap.1p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
trap --- trap signals

* SYNOPSIS
#+begin_example
  trap n [condition...]
  trap [action condition...]
#+end_example

* DESCRIPTION
If the first operand is an unsigned decimal integer, the shell shall
treat all operands as conditions, and shall reset each condition to the
default value. Otherwise, if there are operands, the first is treated as
an action and the remaining as conditions.

If /action/ is *'-'*, the shell shall reset each /condition/ to the
default value. If /action/ is null ( *" "*), the shell shall ignore each
specified /condition/ if it arises. Otherwise, the argument /action/
shall be read and executed by the shell when one of the corresponding
conditions arises. The action of /trap/ shall override a previous action
(either default action or one explicitly set). The value of *"$?"* after
the /trap/ action completes shall be the value it had before /trap/ was
invoked.

The condition can be EXIT, 0 (equivalent to EXIT), or a signal specified
using a symbolic name, without the SIG prefix, as listed in the tables
of signal names in the /<signal.h>/ header defined in the Base
Definitions volume of POSIX.1‐2017, /Chapter 13/, /Headers/; for
example, HUP, INT, QUIT, TERM. Implementations may permit names with the
SIG prefix or ignore case in signal names as an extension. Setting a
trap for SIGKILL or SIGSTOP produces undefined results.

The environment in which the shell executes a /trap/ on EXIT shall be
identical to the environment immediately after the last command executed
before the /trap/ on EXIT was taken.

Each time /trap/ is invoked, the /action/ argument shall be processed in
a manner equivalent to:

#+begin_quote
  #+begin_example

    eval action
  #+end_example
#+end_quote

Signals that were ignored on entry to a non-interactive shell cannot be
trapped or reset, although no error need be reported when attempting to
do so. An interactive shell may reset or catch signals ignored on entry.
Traps shall remain in place for a given shell until explicitly changed
with another /trap/ command.

When a subshell is entered, traps that are not being ignored shall be
set to the default actions, except in the case of a command substitution
containing only a single /trap/ command, when the traps need not be
altered. Implementations may check for this case using only lexical
analysis; for example, if /`trap`/ and /$( trap -- )/ do not alter the
traps in the subshell, cases such as assigning /var=trap/ and then using
/$($var)/ may still alter them. This does not imply that the /trap/
command cannot be used within the subshell to set new traps.

The /trap/ command with no operands shall write to standard output a
list of commands associated with each condition. If the command is
executed in a subshell, the implementation does not perform the optional
check described above for a command substitution containing only a
single /trap/ command, and no /trap/ commands with operands have been
executed since entry to the subshell, the list shall contain the
commands that were associated with each condition immediately before the
subshell environment was entered. Otherwise, the list shall contain the
commands currently associated with each condition. The format shall be:

#+begin_quote
  #+begin_example

    "trap -- %s %s ...\n", <action>, <condition> ...
  #+end_example
#+end_quote

The shell shall format the output, including the proper use of quoting,
so that it is suitable for reinput to the shell as commands that achieve
the same trapping results. For example:

#+begin_quote
  #+begin_example

    save_traps=$(trap)
    ...
    eval "$save_traps"
  #+end_example
#+end_quote

XSI-conformant systems also allow numeric signal numbers for the
conditions corresponding to the following signal names:

1. SIGHUP

2. SIGINT

3. SIGQUIT

4. SIGABRT

5. SIGKILL

6. SIGALRM

7. SIGTERM

The /trap/ special built-in shall conform to the Base Definitions volume
of POSIX.1‐2017, /Section 12.2/, /Utility Syntax Guidelines/.

* OPTIONS
None.

* OPERANDS
See the DESCRIPTION.

* STDIN
Not used.

* INPUT FILES
None.

* ENVIRONMENT VARIABLES
None.

* ASYNCHRONOUS EVENTS
Default.

* STDOUT
See the DESCRIPTION.

* STDERR
The standard error shall be used only for diagnostic messages.

* OUTPUT FILES
None.

* EXTENDED DESCRIPTION
None.

* EXIT STATUS
If the trap name or number is invalid, a non-zero exit status shall be
returned; otherwise, zero shall be returned. For both interactive and
non-interactive shells, invalid signal names or numbers shall not be
considered a syntax error and do not cause the shell to abort.

* CONSEQUENCES OF ERRORS
Default.

/The following sections are informative./

* APPLICATION USAGE
None.

* EXAMPLES
Write out a list of all traps and actions:

#+begin_quote
  #+begin_example

    trap
  #+end_example
#+end_quote

Set a trap so the /logout/ utility in the directory referred to by the
/HOME/ environment variable executes when the shell terminates:

#+begin_quote
  #+begin_example

    trap '"$HOME"/logout' EXIT
  #+end_example
#+end_quote

or:

#+begin_quote
  #+begin_example

    trap '"$HOME"/logout' 0
  #+end_example
#+end_quote

Unset traps on INT, QUIT, TERM, and EXIT:

#+begin_quote
  #+begin_example

    trap - INT QUIT TERM EXIT
  #+end_example
#+end_quote

* RATIONALE
Implementations may permit lowercase signal names as an extension.
Implementations may also accept the names with the SIG prefix; no known
historical shell does so. The /trap/ and /kill/ utilities in this volume
of POSIX.1‐2017 are now consistent in their omission of the SIG prefix
for signal names. Some /kill/ implementations do not allow the prefix,
and /kill/ *-l* lists the signals without prefixes.

Trapping SIGKILL or SIGSTOP is syntactically accepted by some historical
implementations, but it has no effect. Portable POSIX applications
cannot attempt to trap these signals.

The output format is not historical practice. Since the output of
historical /trap/ commands is not portable (because numeric signal
values are not portable) and had to change to become so, an opportunity
was taken to format the output in a way that a shell script could use to
save and then later reuse a trap if it wanted.

The KornShell uses an *ERR* trap that is triggered whenever /set/ *-e*
would cause an exit. This is allowable as an extension, but was not
mandated, as other shells have not used it.

The text about the environment for the EXIT trap invalidates the
behavior of some historical versions of interactive shells which, for
example, close the standard input before executing a trap on 0. For
example, in some historical interactive shell sessions the following
trap on 0 would always print *"--"*:

#+begin_quote
  #+begin_example

    trap 'read foo; echo "-$foo-"' 0
  #+end_example
#+end_quote

The command:

#+begin_quote
  #+begin_example

    trap 'eval " $cmd"' 0
  #+end_example
#+end_quote

causes the contents of the shell variable /cmd/ to be executed as a
command when the shell exits. Using:

#+begin_quote
  #+begin_example

    trap '$cmd' 0
  #+end_example
#+end_quote

does not work correctly if /cmd/ contains any special characters such as
quoting or redirections. Using:

#+begin_quote
  #+begin_example

    trap " $cmd" 0
  #+end_example
#+end_quote

also works (the leading <space> character protects against unlikely
cases where /cmd/ is a decimal integer or begins with *'-'*), but it
expands the /cmd/ variable when the /trap/ command is executed, not when
the exit action is executed.

* FUTURE DIRECTIONS
None.

* SEE ALSO
/Section 2.14/, /Special Built-In Utilities/

The Base Definitions volume of POSIX.1‐2017, /Section 12.2/, /Utility
Syntax Guidelines/, /*<signal.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
