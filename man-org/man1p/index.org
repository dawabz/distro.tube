#+TITLE: Man1p - index
#+DESCRIPTION: Man1p - index
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* man1p
Section 1p is for executable programs or shell commands (POSIX).

#+begin_src bash :exports results
readarray -t orgfiles < <(find . -type f -iname "*" | sort)

for x in "${orgfiles[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./admin.1p.org][admin.1p]]      |
| [[file:./alias.1p.org][alias.1p]]      |
| [[file:./ar.1p.org][ar.1p]]         |
| [[file:./asa.1p.org][asa.1p]]        |
| [[file:./at.1p.org][at.1p]]         |
| [[file:./awk.1p.org][awk.1p]]        |
| [[file:./basename.1p.org][basename.1p]]   |
| [[file:./batch.1p.org][batch.1p]]      |
| [[file:./bc.1p.org][bc.1p]]         |
| [[file:./bg.1p.org][bg.1p]]         |
| [[file:./break.1p.org][break.1p]]      |
| [[file:./c99.1p.org][c99.1p]]        |
| [[file:./cal.1p.org][cal.1p]]        |
| [[file:./cat.1p.org][cat.1p]]        |
| [[file:./cd.1p.org][cd.1p]]         |
| [[file:./cflow.1p.org][cflow.1p]]      |
| [[file:./chgrp.1p.org][chgrp.1p]]      |
| [[file:./chmod.1p.org][chmod.1p]]      |
| [[file:./chown.1p.org][chown.1p]]      |
| [[file:./cksum.1p.org][cksum.1p]]      |
| [[file:./cmp.1p.org][cmp.1p]]        |
| [[file:./colon.1p.org][colon.1p]]      |
| [[file:./comm.1p.org][comm.1p]]       |
| [[file:./command.1p.org][command.1p]]    |
| [[file:./compress.1p.org][compress.1p]]   |
| [[file:./continue.1p.org][continue.1p]]   |
| [[file:./cp.1p.org][cp.1p]]         |
| [[file:./crontab.1p.org][crontab.1p]]    |
| [[file:./csplit.1p.org][csplit.1p]]     |
| [[file:./ctags.1p.org][ctags.1p]]      |
| [[file:./cut.1p.org][cut.1p]]        |
| [[file:./cxref.1p.org][cxref.1p]]      |
| [[file:./date.1p.org][date.1p]]       |
| [[file:./dd.1p.org][dd.1p]]         |
| [[file:./delta.1p.org][delta.1p]]      |
| [[file:./df.1p.org][df.1p]]         |
| [[file:./diff.1p.org][diff.1p]]       |
| [[file:./dirname.1p.org][dirname.1p]]    |
| [[file:./dot.1p.org][dot.1p]]        |
| [[file:./du.1p.org][du.1p]]         |
| [[file:./echo.1p.org][echo.1p]]       |
| [[file:./ed.1p.org][ed.1p]]         |
| [[file:./env.1p.org][env.1p]]        |
| [[file:./eval.1p.org][eval.1p]]       |
| [[file:./ex.1p.org][ex.1p]]         |
| [[file:./exec.1p.org][exec.1p]]       |
| [[file:./exit.1p.org][exit.1p]]       |
| [[file:./expand.1p.org][expand.1p]]     |
| [[file:./export.1p.org][export.1p]]     |
| [[file:./expr.1p.org][expr.1p]]       |
| [[file:./false.1p.org][false.1p]]      |
| [[file:./fc.1p.org][fc.1p]]         |
| [[file:./fg.1p.org][fg.1p]]         |
| [[file:./file.1p.org][file.1p]]       |
| [[file:./find.1p.org][find.1p]]       |
| [[file:./fold.1p.org][fold.1p]]       |
| [[file:./fort77.1p.org][fort77.1p]]     |
| [[file:./fuser.1p.org][fuser.1p]]      |
| [[file:./gencat.1p.org][gencat.1p]]     |
| [[file:./get.1p.org][get.1p]]        |
| [[file:./getconf.1p.org][getconf.1p]]    |
| [[file:./getopts.1p.org][getopts.1p]]    |
| [[file:./grep.1p.org][grep.1p]]       |
| [[file:./hash.1p.org][hash.1p]]       |
| [[file:./head.1p.org][head.1p]]       |
| [[file:./iconv.1p.org][iconv.1p]]      |
| [[file:./id.1p.org][id.1p]]         |
| [[file:./index.org][index]]         |
| [[file:./ipcrm.1p.org][ipcrm.1p]]      |
| [[file:./ipcs.1p.org][ipcs.1p]]       |
| [[file:./jobs.1p.org][jobs.1p]]       |
| [[file:./join.1p.org][join.1p]]       |
| [[file:./kill.1p.org][kill.1p]]       |
| [[file:./lex.1p.org][lex.1p]]        |
| [[file:./link.1p.org][link.1p]]       |
| [[file:./ln.1p.org][ln.1p]]         |
| [[file:./locale.1p.org][locale.1p]]     |
| [[file:./localedef.1p.org][localedef.1p]]  |
| [[file:./logger.1p.org][logger.1p]]     |
| [[file:./logname.1p.org][logname.1p]]    |
| [[file:./lp.1p.org][lp.1p]]         |
| [[file:./ls.1p.org][ls.1p]]         |
| [[file:./m4.1p.org][m4.1p]]         |
| [[file:./mailx.1p.org][mailx.1p]]      |
| [[file:./make.1p.org][make.1p]]       |
| [[file:./man.1p.org][man.1p]]        |
| [[file:./mesg.1p.org][mesg.1p]]       |
| [[file:./mkdir.1p.org][mkdir.1p]]      |
| [[file:./mkfifo.1p.org][mkfifo.1p]]     |
| [[file:./more.1p.org][more.1p]]       |
| [[file:./mv.1p.org][mv.1p]]         |
| [[file:./newgrp.1p.org][newgrp.1p]]     |
| [[file:./nice.1p.org][nice.1p]]       |
| [[file:./nl.1p.org][nl.1p]]         |
| [[file:./nm.1p.org][nm.1p]]         |
| [[file:./nohup.1p.org][nohup.1p]]      |
| [[file:./od.1p.org][od.1p]]         |
| [[file:./paste.1p.org][paste.1p]]      |
| [[file:./patch.1p.org][patch.1p]]      |
| [[file:./pathchk.1p.org][pathchk.1p]]    |
| [[file:./pax.1p.org][pax.1p]]        |
| [[file:./pr.1p.org][pr.1p]]         |
| [[file:./printf.1p.org][printf.1p]]     |
| [[file:./prs.1p.org][prs.1p]]        |
| [[file:./ps.1p.org][ps.1p]]         |
| [[file:./pwd.1p.org][pwd.1p]]        |
| [[file:./qalter.1p.org][qalter.1p]]     |
| [[file:./qdel.1p.org][qdel.1p]]       |
| [[file:./qhold.1p.org][qhold.1p]]      |
| [[file:./qmove.1p.org][qmove.1p]]      |
| [[file:./qmsg.1p.org][qmsg.1p]]       |
| [[file:./qrerun.1p.org][qrerun.1p]]     |
| [[file:./qrls.1p.org][qrls.1p]]       |
| [[file:./qselect.1p.org][qselect.1p]]    |
| [[file:./qsig.1p.org][qsig.1p]]       |
| [[file:./qstat.1p.org][qstat.1p]]      |
| [[file:./qsub.1p.org][qsub.1p]]       |
| [[file:./read.1p.org][read.1p]]       |
| [[file:./readonly.1p.org][readonly.1p]]   |
| [[file:./renice.1p.org][renice.1p]]     |
| [[file:./return.1p.org][return.1p]]     |
| [[file:./rm.1p.org][rm.1p]]         |
| [[file:./rmdel.1p.org][rmdel.1p]]      |
| [[file:./rmdir.1p.org][rmdir.1p]]      |
| [[file:./sact.1p.org][sact.1p]]       |
| [[file:./sccs.1p.org][sccs.1p]]       |
| [[file:./sed.1p.org][sed.1p]]        |
| [[file:./set.1p.org][set.1p]]        |
| [[file:./sh.1p.org][sh.1p]]         |
| [[file:./shift.1p.org][shift.1p]]      |
| [[file:./sleep.1p.org][sleep.1p]]      |
| [[file:./sort.1p.org][sort.1p]]       |
| [[file:./split.1p.org][split.1p]]      |
| [[file:./strings.1p.org][strings.1p]]    |
| [[file:./strip.1p.org][strip.1p]]      |
| [[file:./stty.1p.org][stty.1p]]       |
| [[file:./tabs.1p.org][tabs.1p]]       |
| [[file:./tail.1p.org][tail.1p]]       |
| [[file:./talk.1p.org][talk.1p]]       |
| [[file:./tee.1p.org][tee.1p]]        |
| [[file:./test.1p.org][test.1p]]       |
| [[file:./time.1p.org][time.1p]]       |
| [[file:./times.1p.org][times.1p]]      |
| [[file:./touch.1p.org][touch.1p]]      |
| [[file:./tput.1p.org][tput.1p]]       |
| [[file:./tr.1p.org][tr.1p]]         |
| [[file:./trap.1p.org][trap.1p]]       |
| [[file:./true.1p.org][true.1p]]       |
| [[file:./tsort.1p.org][tsort.1p]]      |
| [[file:./tty.1p.org][tty.1p]]        |
| [[file:./type.1p.org][type.1p]]       |
| [[file:./ulimit.1p.org][ulimit.1p]]     |
| [[file:./umask.1p.org][umask.1p]]      |
| [[file:./unalias.1p.org][unalias.1p]]    |
| [[file:./uname.1p.org][uname.1p]]      |
| [[file:./uncompress.1p.org][uncompress.1p]] |
| [[file:./unexpand.1p.org][unexpand.1p]]   |
| [[file:./unget.1p.org][unget.1p]]      |
| [[file:./uniq.1p.org][uniq.1p]]       |
| [[file:./unlink.1p.org][unlink.1p]]     |
| [[file:./unset.1p.org][unset.1p]]      |
| [[file:./uucp.1p.org][uucp.1p]]       |
| [[file:./uudecode.1p.org][uudecode.1p]]   |
| [[file:./uuencode.1p.org][uuencode.1p]]   |
| [[file:./uustat.1p.org][uustat.1p]]     |
| [[file:./uux.1p.org][uux.1p]]        |
| [[file:./val.1p.org][val.1p]]        |
| [[file:./vi.1p.org][vi.1p]]         |
| [[file:./wait.1p.org][wait.1p]]       |
| [[file:./wc.1p.org][wc.1p]]         |
| [[file:./what.1p.org][what.1p]]       |
| [[file:./who.1p.org][who.1p]]        |
| [[file:./write.1p.org][write.1p]]      |
| [[file:./xargs.1p.org][xargs.1p]]      |
| [[file:./yacc.1p.org][yacc.1p]]       |
| [[file:./zcat.1p.org][zcat.1p]]       |
