#+TITLE: Manpages - cal.1p
#+DESCRIPTION: Linux manpage for cal.1p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
cal --- print a calendar

* SYNOPSIS
#+begin_example
  cal [[month] year]
#+end_example

* DESCRIPTION
The /cal/ utility shall write a calendar to standard output using the
Julian calendar for dates from January 1, 1 through September 2, 1752
and the Gregorian calendar for dates from September 14, 1752 through
December 31, 9999 as though the Gregorian calendar had been adopted on
September 14, 1752.

If no operands are given, /cal/ shall produce a one-month calendar for
the current month in the current year. If only the /year/ operand is
given, /cal/ shall produce a calendar for all twelve months in the given
calendar year. If both /month/ and /year/ operands are given, /cal/
shall produce a one-month calendar for the given month in the given
year.

* OPTIONS
None.

* OPERANDS
The following operands shall be supported:

- month :: Specify the month to be displayed, represented as a decimal
  integer from 1 (January) to 12 (December).

- year :: Specify the year for which the calendar is displayed,
  represented as a decimal integer from 1 to 9999.

* STDIN
Not used.

* INPUT FILES
None.

* ENVIRONMENT VARIABLES
The following environment variables shall affect the execution of /cal/:

- LANG :: Provide a default value for the internationalization variables
  that are unset or null. (See the Base Definitions volume of
  POSIX.1‐2017, /Section 8.2/, /Internationalization Variables/ for the
  precedence of internationalization variables used to determine the
  values of locale categories.)

- LC_ALL :: If set to a non-empty string value, override the values of
  all the other internationalization variables.

- LC_CTYPE :: Determine the locale for the interpretation of sequences
  of bytes of text data as characters (for example, single-byte as
  opposed to multi-byte characters in arguments).

- LC_MESSAGES :: \\
  Determine the locale that should be used to affect the format and
  contents of diagnostic messages written to standard error, and
  informative messages written to standard output.

- LC_TIME :: Determine the format and contents of the calendar.

- NLSPATH :: Determine the location of message catalogs for the
  processing of /LC_MESSAGES/.

- TZ :: Determine the timezone used to calculate the value of the
  current month.

* ASYNCHRONOUS EVENTS
Default.

* STDOUT
The standard output shall be used to display the calendar, in an
unspecified format.

* STDERR
The standard error shall be used only for diagnostic messages.

* OUTPUT FILES
None.

* EXTENDED DESCRIPTION
None.

* EXIT STATUS
The following exit values shall be returned:

-  0 :: Successful completion.

- >0 :: An error occurred.

* CONSEQUENCES OF ERRORS
Default.

/The following sections are informative./

* APPLICATION USAGE
Note that:

#+begin_quote
  #+begin_example

    cal 83
  #+end_example
#+end_quote

refers to A.D. 83, not 1983.

* EXAMPLES
None.

* RATIONALE
Earlier versions of this standard incorrectly required that the command:

#+begin_quote
  #+begin_example

    cal 2000
  #+end_example
#+end_quote

write a one-month calendar for the current calendar month (no matter
what the current year is) in the year 2000 to standard output. This did
not match historic practice in any known version of the /cal/ utility.
The description has been updated to match historic practice. When only
the /year/ operand is given, /cal/ writes a twelve-month calendar for
the specified year.

* FUTURE DIRECTIONS
A future version of this standard may support locale-specific
recognition of the date of adoption of the Gregorian calendar.

* SEE ALSO
The Base Definitions volume of POSIX.1‐2017, /Chapter 8/, /Environment
Variables/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
