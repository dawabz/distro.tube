#+TITLE: Manpages - rmdel.1p
#+DESCRIPTION: Linux manpage for rmdel.1p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
rmdel --- remove a delta from an SCCS file (*DEVELOPMENT*)

* SYNOPSIS
#+begin_example
  rmdel -r SID file...
#+end_example

* DESCRIPTION
The /rmdel/ utility shall remove the delta specified by the SID from
each named SCCS file. The delta to be removed shall be the most recent
delta in its branch in the delta chain of each named SCCS file. In
addition, the application shall ensure that the SID specified is not
that of a version being edited for the purpose of making a delta; that
is, if a /p-file/ (see //get/ /) exists for the named SCCS file, the SID
specified shall not appear in any entry of the /p-file/.

Removal of a delta shall be restricted to:

-  1. :: The user who made the delta

-  2. :: The owner of the SCCS file

-  3. :: The owner of the directory containing the SCCS file

* OPTIONS
The /rmdel/ utility shall conform to the Base Definitions volume of
POSIX.1‐2017, /Section 12.2/, /Utility Syntax Guidelines/.

The following option shall be supported:

- -r SID :: Specify the SCCS identification string ( /SID/) of the delta
  to be deleted.

* OPERANDS
The following operand shall be supported:

- file :: A pathname of an existing SCCS file or a directory. If /file/
  is a directory, the /rmdel/ utility shall behave as though each file
  in the directory were specified as a named file, except that non-SCCS
  files (last component of the pathname does not begin with *s.*) and
  unreadable files shall be silently ignored.

  If exactly one /file/ operand appears, and it is *'-'*, the standard
  input shall be read; each line of the standard input is taken to be
  the name of an SCCS file to be processed. Non-SCCS files and
  unreadable files shall be silently ignored.

* STDIN
The standard input shall be a text file used only when the /file/
operand is specified as *'-'*. Each line of the text file shall be
interpreted as an SCCS pathname.

* INPUT FILES
The SCCS files shall be files of unspecified format.

* ENVIRONMENT VARIABLES
The following environment variables shall affect the execution of
/rmdel/:

- LANG :: Provide a default value for the internationalization variables
  that are unset or null. (See the Base Definitions volume of
  POSIX.1‐2017, /Section 8.2/, /Internationalization Variables/ for the
  precedence of internationalization variables used to determine the
  values of locale categories.)

- LC_ALL :: If set to a non-empty string value, override the values of
  all the other internationalization variables.

- LC_CTYPE :: Determine the locale for the interpretation of sequences
  of bytes of text data as characters (for example, single-byte as
  opposed to multi-byte characters in arguments and input files).

- LC_MESSAGES :: \\
  Determine the locale that should be used to affect the format and
  contents of diagnostic messages written to standard error.

- NLSPATH :: Determine the location of message catalogs for the
  processing of /LC_MESSAGES/.

* ASYNCHRONOUS EVENTS
Default.

* STDOUT
Not used.

* STDERR
The standard error shall be used only for diagnostic messages.

* OUTPUT FILES
The SCCS files shall be files of unspecified format. During processing
of a /file/, a temporary /x-file/, as described in //admin/ /, may be
created and deleted; a locking /z-file/, as described in //get/ /, may
be created and deleted.

* EXTENDED DESCRIPTION
None.

* EXIT STATUS
The following exit values shall be returned:

-  0 :: Successful completion.

- >0 :: An error occurred.

* CONSEQUENCES OF ERRORS
Default.

/The following sections are informative./

* APPLICATION USAGE
None.

* EXAMPLES
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//admin/ /, //delta/ /, //get/ /, //prs/ /

The Base Definitions volume of POSIX.1‐2017, /Chapter 8/, /Environment
Variables/, /Section 12.2/, /Utility Syntax Guidelines/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
