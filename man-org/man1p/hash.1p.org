#+TITLE: Manpages - hash.1p
#+DESCRIPTION: Linux manpage for hash.1p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
hash --- remember or report utility locations

* SYNOPSIS
#+begin_example
  hash [utility...]
  hash -r
#+end_example

* DESCRIPTION
The /hash/ utility shall affect the way the current shell environment
remembers the locations of utilities found as described in /Section
2.9.1.1/, /Command Search and Execution/. Depending on the arguments
specified, it shall add utility locations to its list of remembered
locations or it shall purge the contents of the list. When no arguments
are specified, it shall report on the contents of the list.

Utilities provided as built-ins to the shell shall not be reported by
/hash/.

* OPTIONS
The /hash/ utility shall conform to the Base Definitions volume of
POSIX.1‐2017, /Section 12.2/, /Utility Syntax Guidelines/.

The following option shall be supported:

- -r :: Forget all previously remembered utility locations.

* OPERANDS
The following operand shall be supported:

- utility :: The name of a utility to be searched for and added to the
  list of remembered locations. If /utility/ contains one or more
  <slash> characters, the results are unspecified.

* STDIN
Not used.

* INPUT FILES
None.

* ENVIRONMENT VARIABLES
The following environment variables shall affect the execution of
/hash/:

- LANG :: Provide a default value for the internationalization variables
  that are unset or null. (See the Base Definitions volume of
  POSIX.1‐2017, /Section 8.2/, /Internationalization Variables/ for the
  precedence of internationalization variables used to determine the
  values of locale categories.)

- LC_ALL :: If set to a non-empty string value, override the values of
  all the other internationalization variables.

- LC_CTYPE :: Determine the locale for the interpretation of sequences
  of bytes of text data as characters (for example, single-byte as
  opposed to multi-byte characters in arguments).

- LC_MESSAGES :: \\
  Determine the locale that should be used to affect the format and
  contents of diagnostic messages written to standard error.

- NLSPATH :: Determine the location of message catalogs for the
  processing of /LC_MESSAGES/.

- PATH :: Determine the location of /utility/, as described in the Base
  Definitions volume of POSIX.1‐2017, /Chapter 8/, /Environment
  Variables/.

* ASYNCHRONOUS EVENTS
Default.

* STDOUT
The standard output of /hash/ shall be used when no arguments are
specified. Its format is unspecified, but includes the pathname of each
utility in the list of remembered locations for the current shell
environment. This list shall consist of those utilities named in
previous /hash/ invocations that have been invoked, and may contain
those invoked and found through the normal command search process.

* STDERR
The standard error shall be used only for diagnostic messages.

* OUTPUT FILES
None.

* EXTENDED DESCRIPTION
None.

* EXIT STATUS
The following exit values shall be returned:

-  0 :: Successful completion.

- >0 :: An error occurred.

* CONSEQUENCES OF ERRORS
Default.

/The following sections are informative./

* APPLICATION USAGE
Since /hash/ affects the current shell execution environment, it is
always provided as a shell regular built-in. If it is called in a
separate utility execution environment, such as one of the following:

#+begin_quote
  #+begin_example

    nohup hash -r
    find . -type f | xargs hash
  #+end_example
#+end_quote

it does not affect the command search process of the caller's
environment.

The /hash/ utility may be implemented as an alias---for example, /alias/
*-t -*, in which case utilities found through normal command search are
not listed by the /hash/ command.

The effects of /hash/ *-r* can also be achieved portably by resetting
the value of /PATH/; in the simplest form, this can be:

#+begin_quote
  #+begin_example

    PATH="$PATH"
  #+end_example
#+end_quote

The use of /hash/ with /utility/ names is unnecessary for most
applications, but may provide a performance improvement on a few
implementations; normally, the hashing process is included by default.

* EXAMPLES
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
/Section 2.9.1.1/, /Command Search and Execution/

The Base Definitions volume of POSIX.1‐2017, /Chapter 8/, /Environment
Variables/, /Section 12.2/, /Utility Syntax Guidelines/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
