#+TITLE: Manpages - passwd.5
#+DESCRIPTION: Linux manpage for passwd.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
passwd - the password file

* DESCRIPTION
/etc/passwd contains one line for each user account, with seven fields
delimited by colons (“:”). These fields are:

#+begin_quote
  ·

  login name
#+end_quote

#+begin_quote
  ·

  optional encrypted password
#+end_quote

#+begin_quote
  ·

  numerical user ID
#+end_quote

#+begin_quote
  ·

  numerical group ID
#+end_quote

#+begin_quote
  ·

  user name or comment field
#+end_quote

#+begin_quote
  ·

  user home directory
#+end_quote

#+begin_quote
  ·

  optional user command interpreter
#+end_quote

If the /password/ field is a lower-case “x”, then the encrypted password
is actually stored in the *shadow*(5) file instead; there /must/ be a
corresponding line in the /etc/shadow file, or else the user account is
invalid.

The encrypted /password/ field may be empty, in which case no password
is required to authenticate as the specified login name. However, some
applications which read the /etc/passwd file may decide not to permit
/any/ access at all if the /password/ field is blank.

A /password/ field which starts with an exclamation mark means that the
password is locked. The remaining characters on the line represent the
/password/ field before the password was locked.

Refer to *crypt*(3) for details on how this string is interpreted.

If the password field contains some string that is not a valid result of
*crypt*(3), for instance ! or *, the user will not be able to use a unix
password to log in (but the user may log in the system by other means).

The comment field is used by various system utilities, such as
*finger*(1).

The home directory field provides the name of the initial working
directory. The *login* program uses this information to set the value of
the *$HOME* environmental variable.

The command interpreter field provides the name of the users command
language interpreter, or the name of the initial program to execute. The
*login* program uses this information to set the value of the *$SHELL*
environmental variable. If this field is empty, it defaults to the value
/bin/sh.

* FILES
/etc/passwd

#+begin_quote
  User account information.
#+end_quote

/etc/shadow

#+begin_quote
  optional encrypted password file
#+end_quote

/etc/passwd-

#+begin_quote
  Backup file for /etc/passwd.

  Note that this file is used by the tools of the shadow toolsuite, but
  not by all user and password management tools.
#+end_quote

* SEE ALSO
*crypt*(3), *getent*(1), *getpwnam*(3), *login*(1), *passwd*(1),
*pwck*(8), *pwconv*(8), *pwunconv*(8), *shadow*(5), *su*(1),
*sulogin*(8).
