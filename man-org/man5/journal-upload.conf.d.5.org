#+TITLE: Manpages - journal-upload.conf.d.5
#+DESCRIPTION: Linux manpage for journal-upload.conf.d.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about journal-upload.conf.d.5 is found in manpage for: [[../journal-upload.conf.5][journal-upload.conf.5]]