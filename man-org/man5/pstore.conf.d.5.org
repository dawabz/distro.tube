#+TITLE: Manpages - pstore.conf.d.5
#+DESCRIPTION: Linux manpage for pstore.conf.d.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about pstore.conf.d.5 is found in manpage for: [[../pstore.conf.5][pstore.conf.5]]