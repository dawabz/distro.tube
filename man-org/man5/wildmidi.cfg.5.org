#+TITLE: Manpages - wildmidi.cfg.5
#+DESCRIPTION: Linux manpage for wildmidi.cfg.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
wildmidi.cfg - Config file for libWildMidi

* LIBRARY
*libWildMidi*

* DESCRIPTION
Contains the patch configuration for libWildMidi and location of Gravis
Ultrasound compatible patch files.

#+begin_example
  dir ~/guspats/

  source /etc/wildmidi.cfg

  bank 0
  0 acpiano.pat amp=110 env_time4=300
  1 brpiano.pat amp=100

  drumset 0
  25 snarerol keep=env amp=225 note=60
  26 snap	note=65
#+end_example

* SYNTAX
- guspat_editor_author_cant_read_so_fix_release_time_for_me :: Some
  patch file editors switch the 4th and 5th envelopes around making the
  sound play much longer than intended in players that stuck to the
  Gravis Ultrasound patch standard. Including this option in the config
  enables a fix that detects this oversight, playing the sound samples
  as if they were correct.

  NOTE: This is a global setting. If it is found to affect good patch
  samples it will be moved to a per patch setting in a future version.

- auto_amp :: Forces WildMIDI to amplify samples to their maximum level
  ignoring the amp=% in the patch lines of the config.

- auto_amp_with_amp :: Forces WildMIDI to amplify samples to their
  maximum level then apply the amp=% in the patch lines of the config.

- dir dir-name :: Change the search path for config and patch files to
  /dir-name/. This is specific to the current config file and carried to
  any included config file unless they have their own *dir* setting. Any
  included file that has its own *dir* setting does not effect the *dir*
  setting of the current config file.

- source include-confg :: Include the settings from /include-config/.
  Any patch already set will be over-ridden by the included config file.

- bank N :: The patches following this setting belong to MIDI instrument
  bank /N/.

- drumset N :: The patches following this setting belong to MIDI drum
  bank /N/.

- patchno patchfile [amp=volume] [note=miodinte] [keep=loop] [keep=env]
  [remove=sustain] [remove=clamped] [env_level[0-5]=level]
  [env_time[0-5]=time] :: 

Example: 0 acpiano.pat amp=110

#+begin_quote
  - patchno :: This is the MIDI patch number the instrument belongs to.

  - patchfile :: The filename of the Gravis Ultrasound compatible patch
    file. If the filename is missing the .pat extension, libWildMidi
    will add .pat when attempting to load the file.

  - amp=volume :: Force the volume of the samples in this patch to
    /volume/% prior to using it.

  - note=midinote :: Use note /midinote/ when playing the samples in
    this patch. NOTE: this is for instruments listed under drumset.

  - keep=loop :: Play the samples in this patch with the loop, when
    normally we would not for this instrument.

  - keep=env :: Use the envelope data in this patch, when normally we
    wouldn't for this instrument.

  - remove=sustain :: Do note hold the note after the 3rd envelope until
    note off, which is what happens if the sustain bit is set in the
    patch file.

  - remove=clamped :: Do not jump to 6th envelope on note off, which is
    what happens if the clamped bit is set in the patch file.

  - env_level[0-5]=level :: Set the envelope level to /level/ with 1.0
    being maximum, and 0.0 being minimum.

    Example: set 5th envelope level to 10% - *env_level[0-5]=*0.1

  - env_time[0-5]=time :: Set the envelope time to /time/ with a
    resolution of 1/1000th of a second. This setting is the time it
    should take for the envelope to reach maximum level.

    Example: set 1st envelope time to 1sec - *env_time0=*1000

    Example: set 3rd envelope time to 0.5secs - *env_time2=*500
#+end_quote

- reverb_room_width fval :: Set the room width for the reverb engine in
  meters. /fval/ is a float value in meters. Minimum setting is 1.0
  meter, maximum setting is 100.0 meters, and default is 15.0 meters.

  Example: set room width to 30 meters - *reverb_room_width 30*

- reverb_room_length fval :: Set the room length for the reverb engine
  in meters. /fval/ is a float value in meters. Minimum setting is 1.0
  meter, maximum setting is 100.0 meters, and default is 20.0 meters.

  Example: set room length to 40 meters - *reverb_room_length 40*

* SEE ALSO
*wildmidi*(1)

* AUTHOR
Chris Ison <wildcode@users.sf.net> Bret Curtis <psi29a@gmail.com>

* COPYRIGHT
Copyright (C) Chris Ison 2001-2010 Copyright (C) Bret Curtis 2013-2016

This file is part of WildMIDI.

WildMIDI is free software: you can redistribute and/or modify the player
under the terms of the GNU General Public License and you can
redistribute and/or modify the library under the terms of the GNU Lesser
General Public License as published by the Free Software Foundation,
either version 3 of the licenses, or(at your option) any later version.

WildMIDI is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License and
the GNU Lesser General Public License for more details.

You should have received a copy of the GNU General Public License and
the GNU Lesser General Public License along with WildMIDI. If not, see
<http://www.gnu.org/licenses/>.

This manpage is licensed under the Creative Commons Attribution-Share
Alike 3.0 Unported License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/3.0/ or send a letter to
Creative Commons, 171 Second Street, Suite 300, San Francisco,
California, 94105, USA.
