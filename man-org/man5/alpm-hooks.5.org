#+TITLE: Manpages - alpm-hooks.5
#+DESCRIPTION: Linux manpage for alpm-hooks.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
alpm-hooks - alpm hook file format

* SYNOPSIS
#+begin_example
  [Trigger] (Required, Repeatable)
  Operation = Install|Upgrade|Remove (Required, Repeatable)
  Type = Path|Package (Required)
  Target = <Path|PkgName> (Required, Repeatable)

  [Action] (Required)
  Description = ... (Optional)
  When = PreTransaction|PostTransaction (Required)
  Exec = <Command> (Required)
  Depends = <PkgName> (Optional)
  AbortOnFail (Optional, PreTransaction only)
  NeedsTargets (Optional)
#+end_example

* DESCRIPTION
libalpm provides the ability to specify hooks to run before or after
transactions based on the packages and/or files being modified. Hooks
consist of a single /[Action]/ section describing the action to be run
and one or more /[Trigger]/ section describing which transactions it
should be run for.

Hooks are read from files located in the system hook directory
/usr/share/libalpm/hooks, and additional custom directories specified in
*pacman.conf*(5) (the default is /etc/pacman.d/hooks). The file names
are required to have the suffix ".hook". Hooks are run in alphabetical
order of their file name, where the ordering ignores the suffix.

* TRIGGERS
Hooks must contain at least one /[Trigger]/ section that determines
which transactions will cause the hook to run. If multiple trigger
sections are defined the hook will run if the transaction matches *any*
of the triggers.

*Operation =* Install|Upgrade|Remove

#+begin_quote
  Select the type of operation to match targets against. May be
  specified multiple times. Installations are considered an upgrade if
  the package or file is already present on the system regardless of
  whether the new package version is actually greater than the currently
  installed version. For Path triggers, this is true even if the file
  changes ownership from one package to another. Required.
#+end_quote

*Type =* Path|Package

#+begin_quote
  Select whether targets are matched against transaction packages or
  files. See CAVEATS for special notes regarding Path triggers. /File/
  is a deprecated alias for /Path/ and will be removed in a future
  release. Required.
#+end_quote

*Target =* <path|package>

#+begin_quote
  The path or package name to match against the active transaction.
  Paths refer to the files in the package archive; the installation root
  should *not* be included in the path. Shell-style glob patterns are
  allowed. It is possible to invert matches by prepending a target with
  an exclamation mark. May be specified multiple times. Required.
#+end_quote

* ACTIONS
*Description =* ...

#+begin_quote
  An optional description that describes the action being taken by the
  hook for use in front-end output.
#+end_quote

*Exec =* <command>

#+begin_quote
  Command to run. Command arguments are split on whitespace. Values
  containing whitespace should be enclosed in quotes. Required.
#+end_quote

*When =* PreTransaction|PostTransaction

#+begin_quote
  When to run the hook. Required.
#+end_quote

*Depends =* <package>

#+begin_quote
  Packages that must be installed for the hook to run. May be specified
  multiple times.
#+end_quote

*AbortOnFail*

#+begin_quote
  Causes the transaction to be aborted if the hook exits non-zero. Only
  applies to PreTransaction hooks.
#+end_quote

*NeedsTargets*

#+begin_quote
  Causes the list of matched trigger targets to be passed to the running
  hook on /stdin/.
#+end_quote

* OVERRIDING HOOKS
Hooks may be overridden by placing a file with the same name in a higher
priority hook directory. Hooks may be disabled by overriding them with a
symlink to //dev/null/.

* EXAMPLES

#+begin_quote
  #+begin_example
    # Force disks to sync to reduce the risk of data corruption

    [Trigger]
    Operation = Install
    Operation = Upgrade
    Operation = Remove
    Type = Package
    Target = *

    [Action]
    Depends = coreutils
    When = PostTransaction
    Exec = /usr/bin/sync
  #+end_example
#+end_quote

* CAVEATS
There are situations when path triggers may act in unexpected ways.
Hooks are triggered using the file list of the installed, upgraded, or
removed package. When installing or upgrading a file that is extracted
with a /.pacnew/ extension, the original file name is used in triggering
the hook. When removing a package, all files owned by that package can
trigger a hook whether or not they were actually present on the file
system before package removal.

PostTransaction hooks will *not* run if the transaction fails to
complete for any reason.

See the pacman website at https://archlinux.org/pacman/ for current
information on pacman and its related tools.

* BUGS
Bugs? You must be kidding; there are no bugs in this software. But if we
happen to be wrong, submit a bug report with as much detail as possible
at the Arch Linux Bug Tracker in the Pacman section.

* AUTHORS
Current maintainers:

#+begin_quote
  ·

  Allan McRae <allan@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Andrew Gregory <andrew.gregory.8@gmail.com>
#+end_quote

#+begin_quote
  ·

  Eli Schwartz <eschwartz@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Morgan Adamiec <morganamilo@archlinux.org>
#+end_quote

Past major contributors:

#+begin_quote
  ·

  Judd Vinet <jvinet@zeroflux.org>
#+end_quote

#+begin_quote
  ·

  Aurelien Foret <aurelien@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Aaron Griffin <aaron@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Dan McGee <dan@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Xavier Chantry <shiningxc@gmail.com>
#+end_quote

#+begin_quote
  ·

  Nagy Gabor <ngaba@bibl.u-szeged.hu>
#+end_quote

#+begin_quote
  ·

  Dave Reisner <dreisner@archlinux.org>
#+end_quote

For additional contributors, use git shortlog -s on the pacman.git
repository.
