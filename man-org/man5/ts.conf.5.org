#+TITLE: Manpages - ts.conf.5
#+DESCRIPTION: Linux manpage for ts.conf.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ts.conf - Configuration file for tslib, controlling touch screens for
embedded devices.

* DESCRIPTION
The idea of tslib is to have a core library that provides standard
services, and a set of plugins to manage the conversion and filtering as
needed.

The plugins for a particular touchscreen are loaded automatically by the
library under the control of a static configuration file, /etc/ts.conf.
/etc/ts.conf gives the library basic configuration information. Each
line specifies one module, and the parameters for that module. The
modules are loaded in order, with the first one processing the
touchscreen data first. For example:

#+begin_quote
  #+begin_example
      module_raw input
      module variance delta=30
      module dejitter delta=100
      module linear
       
  #+end_example
#+end_quote

* ENVIRONMENT VARIABLES
Latest versions of the Xorg tslib input driver use *hal* to configure
the touchscreen within Xorg. Environment variables are only needed for
the tslib commands.

*TSLIB_TSDEVICE*

#+begin_quote
  If the default touchscreen device cannot be found, set the
  *TSLIB_TSDEVICE* environment variable to the touchscreen device to
  use.

  Default when using ts_setup(): We try to open /dev/input/ts,
  /dev/input/touchscreen and /dev/touchscreen/ucb1x00 and on Linux, we
  then scan /dev/input/event* for the first device with property
  INPUT_PROP_DIRECT.
#+end_quote

*TSLIB_CONSOLEDEVICE*

#+begin_quote
  Tslib default console device.

  Default: /dev/tty
#+end_quote

*TSLIB_CALIBFILE *

#+begin_quote
  Stores calibration data obtained using ts_calibrate.

  Default: /etc/pointercal
#+end_quote

*TSLIB_CONFFILE*

#+begin_quote
  Set a different location for the ts.conf configuration file itself.

  Default; /etc/ts.conf.
#+end_quote

*TSLIB_FBDEVICE*

#+begin_quote
  Framebuffer device to use for the touchscreen support.

  Default: /dev/fb0.
#+end_quote

*TSLIB_PLUGINDIR*

#+begin_quote
  Plugin directory for tslib.

  Default: /usr/lib/$triplet/ts0/ where triplet is the MultiArch path,
  e.g. arm-linux-gnueabi.
#+end_quote

* MODULE PARAMETERS
*dejitter*

#+begin_quote
  Removes jitter on the X and Y co-ordinates. This is achieved by
  applying a weighted smoothing filter. The latest samples have most
  weight; earlier samples have less weight. This allows one to achieve
  1:1 input->output rate.

  #+begin_quote
    ·

    *delta*

    Squared distance between two samples ((X2-X1)^2 + (Y2-Y1)^2) that
    defines the quick motion threshold. If the pen moves quick, it is
    not feasible to smooth pen motion, besides quick motion is not
    precise anyway; so if quick motion is detected the module just
    discards the backlog and simply copies input to output.
  #+end_quote
#+end_quote

*linear*

#+begin_quote
  Linear scaling module, primarily used for conversion of touch screen
  co-ordinates to screen co-ordinates.

  #+begin_quote
    ·

    *rot*

    Rotation of touch coorinates. 0=no, 1=CW, 2=UD, 3=CCW. Default: the
    screen-rotation that was used with ts_calibrate (-r option).
  #+end_quote

  #+begin_quote
    ·

    *xyswap*

    Interchange the X and Y co-ordinates -- no longer used or needed if
    the new linear calibration utility ts_calibrate is used.
  #+end_quote

  #+begin_quote
    ·

    *pressure_offset*

    Offset applied to the pressure value. Default: 0.
  #+end_quote

  #+begin_quote
    ·

    *pressure_mul*

    Factor to multiply the pressure value with. Default: 1.
  #+end_quote

  #+begin_quote
    ·

    *pressure_div*

    Value to divide the pressure value by. Default: 1.
  #+end_quote
#+end_quote

*iir*

#+begin_quote
  Infinite impulse response filter. Similar to dejitter, this is a
  smoothing filter to remove low-level noise. There is a trade-off
  between noise removal (smoothing) and responsiveness. The parameters N
  and D specify the level of smoothing in the form of a fraction (N/D).

  #+begin_quote
    ·

    *N*

    numerator of the smoothing fraction. Default: 0.
  #+end_quote

  #+begin_quote
    ·

    *D*

    denominator of the smoothing fraction. Default: 1.
  #+end_quote
#+end_quote

*pthres*

#+begin_quote
  Pressure threshold filter. Given a release is always pressure 0 and a
  press is always >= 1, this discards samples below / above the
  specified pressure threshold.

  #+begin_quote
    ·

    *pmin*

    Minimum pressure value for a sample to be valid. Default: 1.
  #+end_quote

  #+begin_quote
    ·

    *pmax*

    Maximum pressure value for a sample to be valid. Default: INT_MAX.
  #+end_quote
#+end_quote

*debounce*

#+begin_quote
  Simple debounce mechanism that drops input events for the specified
  time after a touch gesture stopped.

  #+begin_quote
    ·

    *drop_threshold*

    drop events up to this number of milliseconds after the last release
    event. Default: 0.
  #+end_quote
#+end_quote

*skip*

#+begin_quote
  Skip nhead samples after press and ntail samples before release. This
  should help if for the device the first or last samples are
  unreliable.

  #+begin_quote
    ·

    *nhead*

    Number of events to drop after pressure. Default: 1.
  #+end_quote

  #+begin_quote
    ·

    *ntail*

    Number of events to drop before release. Default: 1.
  #+end_quote
#+end_quote

*median*

#+begin_quote
  Similar to what the variance filter does, the median filter suppresses
  spikes in the gesture.

  #+begin_quote
    ·

    *depth*

    Number of samples to apply the median filter to. Default: 3.
  #+end_quote
#+end_quote

*invert*

#+begin_quote
  Invert values in X and/or Y direction around a given value.

  #+begin_quote
    ·

    *x0*

    X-axis (horizontal) value around which to invert. Default: 0.
  #+end_quote

  #+begin_quote
    ·

    *y0*

    Y-axis (horizontal) value around which to invert. Default: 0.
  #+end_quote
#+end_quote

*lowpass*

#+begin_quote
  simple exponential averaging lowpass filter

  #+begin_quote
    ·

    *factor*

    floating point values betwenn 0 and 1. Default: 0.4.
  #+end_quote

  #+begin_quote
    ·

    *threshold*

    x or y minimum distance between two samples to start applying the
    filter. Default: 2.
  #+end_quote
#+end_quote

*evthres*

#+begin_quote
  Number of samples needed from the device after considered a valid
  touch. This filter will drop a tapping when too little samples are
  between "down" and "up".

  #+begin_quote
    ·

    *N*

    Minimum number of events needed between "down" and "up". Default: 5.
  #+end_quote
#+end_quote

*variance*

#+begin_quote
  Tries to do it's best in order to filter out random noise coming from
  touchscreen ADCs. This is achieved by limiting the sample movement
  speed to some value (e.g. the pen is not supposed to move quicker than
  some threshold).

  This is a greedy filter, e.g. it gives less samples on output than
  receives on input. There is *no multitouch* support for this filter.

  #+begin_quote
    ·

    *delta*

    Set the squared distance in touchscreen units between previous and
    current pen position (e.g. (X2-X1)^2 + (Y2-Y1)^2). This defines the
    criteria for determining whenever two samples are near or far to
    each other.

    If the distance between previous and current sample is far, the
    sample is marked as potential noise. This doesn't mean yet that it
    will be discarded; if the next reading will be close to it, this
    will be considered just a regular quick motion event, and it will
    sneak to the next layer. Also, if the sample after the potential
    noise is far from both previously discussed samples, this is also
    considered a quick motion event and the sample sneaks into the
    output stream.
  #+end_quote
#+end_quote

*hardware* *support*

On Linux, use the *module_raw input* if you can. The other raw access
modules are device specific userspace drivers. If you need one of those,
enable it explicitly when building tslib. The list of modules enabled by
default might shrink in the future. *module_raw input* supports
multitouch (MT) too.

| module_raw          | supported devices                 | interface                | platforms               | MT  | how to enable              |
| *input*             | all with Linux evdev drivers      | any (driver) /dev/input/ | Linux, FreeBSD          | yes | enabled by default         |
| *arctic2*           | IBM Arctic II                     | .                        | Linux, BSD, Hurd, Haiku | no  | --enable-arctic2           |
| *collie*            | Sharp Zaurus SL-5000d/SL-5500     | .                        | Linux, BSD, Hurd, Haiku | no  | --enable-collie            |
| *corgi*             | Sharp Zaurus SL-C700              | .                        | Linux, BSD, Hurd, Haiku | no  | --enable-corgi             |
| *dmc_dus3000*       | DMC DUS Series (DUS3000, ...)     | UART                     | Linux                   | no  | --enable-dmc_dus3000       |
| *dmc*               | DMC (others)                      | .                        | Linux, BSD, Hurd, Haiku | no  | --enable-dmc               |
| *galax*             | eGalax 100, 112, 210              | any (driver)             | Linux, BSD              | no  | --enable-galax             |
| *h3600*             | Compaq IPAQ                       | .                        | Linux, BSD, Hurd, Haiku | no  | --enable-h3600             |
| *mk712*             | Hitachi Webpad                    | .                        | Linux, BSD, Hurd, Haiku | no  | --enable-mk712             |
| *tatung*            | Tatung Webpad                     | .                        | Linux, BSD, Hurd, Haiku | no  | --enable-tatung            |
| *touchkit*          | Touchkit SAT4000UR                | RS232                    | Linux, BSD, Hurd        | no  | enabled by default         |
| *ucb1x00*           | UCB1x00 Touchscreens              | .                        | Linux, BSD, Hurd, Haiku | no  | --enable-ucb1x00           |
| *waveshare*         | Waveshare Touchscreens            | /dev/hidrawX             | Linux                   | no  | enabled by default         |
| *cy8mrln_palmpre*   | in Palm Pre/Pre Plus/Pre 2        | .                        | Linux                   | no  | --enable-cy8mrln-palmpre   |
| *one_wire_ts_input* | FriendlyARM one-wire touch screen | .                        | Linux                   | no  | --enable-one-wire-ts-input |
| *input_evdev*       | Linux evdev drivers (libevdev)    | .                        | Linux                   | yes | --enable-input-evdev       |

* SEE ALSO
*ts_calibrate*(1), *ts_test*(1), *ts_test_mt*(1)
