#+TITLE: Manpages - synctex.5
#+DESCRIPTION: Linux manpage for synctex.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Synchronize TeXnology help file

are text files that help input/output synchronization during document
preparation with the TeX typesetting system.

The structure of this file should not be considered public, in the sense
that no one should need to parse its contents, except the synctex
command line utility, and the synctex_parser library which are both
publicly available. Unless it is absolutely not avoidable, access to the
contents of the synctex file should only be made through requests made
to the synctex command line utility.

The element structure of a synctex file is a list of text line records
as follows.

and

have their usual EBNF meanings:

means zero or more,

means one or more, and

means zero or one

Each section starts with the first occurrence of a sectioning line, and
ends with the next section, if any. In the following definitions, we do
not mention the section ending condition.

<Version Number> <EOL>

<TeX magnification> <EOL>

<unit in scaled point> <EOL>

<horizontal offset in scaled point> <EOL>

<vertical offset in scaled point> <EOL>

<tag>

<File Name> <EOL>

<EOL>

<byte offset> <end of record>

<the integer n> <end of record>

<the integer n> <end of record>

<form tag> <end of record>

<end of record>

Forms are available with pdfTeX. All the numbers are integers encoded
using the decimal representation with "C" locale. The <box content>
describes what is inside a box. It is either a vertical or horizontal
box, with some records related to glue, kern or math nodes.

<link>

<point>

<size> <end of record>

<end of record>

<link>

<point>

<size> <end of record>

<end of record>

Void boxes:

<link>

<point>

<size> <end of record>

<link>

<point>

<size> <end of record>

<line>(

<column>)?

<integer>

<integer>

<integer>

<Height>

<Depth>

The forthcoming records are basic one liners.

<link>

<point> <end of record>

<link>

<point>

<Width> <end of record>

<link>

<point> <end of record>

<link>

<point> <end of record>

<form tag>

<point> <end of record>

The postamble closes the file If there is no postamble, it means that
the typesetting process did not end correctly.

<Number of records> <EOL>

The post scriptum contains material possibly added by 3rd parties. It
allows to append some transformation (shift and magnify). Typically, one
applies a dvi to pdf filter with offset options and magnification, then
he appends the same options to the synctex file, for example

synctex update -o foo.pdf -m 0.486 -x 9472573sp -y 13.3dd source.dvi

<EOL>

<number> <EOL>

<dimension> <EOL>

<dimension> <EOL>

This second information will override the offset and magnification
previously available in the preamble section. All the numbers are
encoded using the decimal representation with "C" locale.

The <current record> is used to compute the visible size of hbox's. The
byte offset is an implicit anchor to navigate the synctex file from
sheet to sheet. The second coordinate of a compressed point has been
replaced by a

character which means that it is the second coordinate of the last full
point available above.

The Synchronize TeXnology is essentially due to Jerome Laurens, with
useful suggestions by some well known actors of the TeX world.

This document has been updated on Sat Apr 22 09:57:20 UTC 2017 to
include \pdfxform support.
