#+TITLE: Manpages - gshadow.5
#+DESCRIPTION: Linux manpage for gshadow.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gshadow - shadowed group file

* DESCRIPTION
/etc/gshadow contains the shadowed information for group accounts.

This file must not be readable by regular users if password security is
to be maintained.

Each line of this file contains the following colon-separated fields:

*group name*

#+begin_quote
  It must be a valid group name, which exist on the system.
#+end_quote

*encrypted password*

#+begin_quote
  Refer to *crypt*(3) for details on how this string is interpreted.

  If the password field contains some string that is not a valid result
  of *crypt*(3), for instance ! or *, users will not be able to use a
  unix password to access the group (but group members do not need the
  password).

  The password is used when a user who is not a member of the group
  wants to gain the permissions of this group (see *newgrp*(1)).

  This field may be empty, in which case only the group members can gain
  the group permissions.

  A password field which starts with an exclamation mark means that the
  password is locked. The remaining characters on the line represent the
  password field before the password was locked.

  This password supersedes any password specified in /etc/group.
#+end_quote

*administrators*

#+begin_quote
  It must be a comma-separated list of user names.

  Administrators can change the password or the members of the group.

  Administrators also have the same permissions as the members (see
  below).
#+end_quote

*members*

#+begin_quote
  It must be a comma-separated list of user names.

  Members can access the group without being prompted for a password.

  You should use the same list of users as in /etc/group.
#+end_quote

* FILES
/etc/group

#+begin_quote
  Group account information.
#+end_quote

/etc/gshadow

#+begin_quote
  Secure group account information.
#+end_quote

* SEE ALSO
*gpasswd*(5), *group*(5), *grpck*(8), *grpconv*(8), *newgrp*(1).
