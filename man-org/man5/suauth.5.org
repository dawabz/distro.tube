#+TITLE: Manpages - suauth.5
#+DESCRIPTION: Linux manpage for suauth.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
suauth - detailed su control file

* SYNOPSIS
*/etc/suauth*

* DESCRIPTION
The file /etc/suauth is referenced whenever the su command is called. It
can change the behaviour of the su command, based upon:

#+begin_quote
  #+begin_example
          1) the user su is targeting
        
  #+end_example
#+end_quote

2) the user executing the su command (or any groups he might be a member
of)

The file is formatted like this, with lines starting with a # being
treated as comment lines and ignored;

#+begin_quote
  #+begin_example
          to-id:from-id:ACTION
        
  #+end_example
#+end_quote

Where to-id is either the word /ALL/, a list of usernames delimited by
"," or the words /ALL EXCEPT/ followed by a list of usernames delimited
by ",".

from-id is formatted the same as to-id except the extra word /GROUP/ is
recognized. /ALL EXCEPT GROUP/ is perfectly valid too. Following /GROUP/
appears one or more group names, delimited by ",". It is not sufficient
to have primary group id of the relevant group, an entry in
*/etc/group*(5) is necessary.

Action can be one only of the following currently supported options.

/DENY/

#+begin_quote
  The attempt to su is stopped before a password is even asked for.
#+end_quote

/NOPASS/

#+begin_quote
  The attempt to su is automatically successful; no password is asked
  for.
#+end_quote

/OWNPASS/

#+begin_quote
  For the su command to be successful, the user must enter his or her
  own password. They are told this.
#+end_quote

Note there are three separate fields delimited by a colon. No whitespace
must surround this colon. Also note that the file is examined
sequentially line by line, and the first applicable rule is used without
examining the file further. This makes it possible for a system
administrator to exercise as fine control as he or she wishes.

* EXAMPLE

#+begin_quote
  #+begin_example
          # sample /etc/suauth file
          #
          # A couple of privileged usernames may
          # su to root with their own password.
          #
          root:chris,birddog:OWNPASS
          #
          # Anyone else may not su to root unless in
          # group wheel. This is how BSD does things.
          #
          root:ALL EXCEPT GROUP wheel:DENY
          #
          # Perhaps terry and birddog are accounts
          # owned by the same person.
          # Access can be arranged between them
          # with no password.
          #
          terry:birddog:NOPASS
          birddog:terry:NOPASS
          #
        
  #+end_example
#+end_quote

* FILES
/etc/suauth

#+begin_quote
#+end_quote

* BUGS
There could be plenty lurking. The file parser is particularly
unforgiving about syntax errors, expecting no spurious whitespace (apart
from beginning and end of lines), and a specific token delimiting
different things.

* DIAGNOSTICS
An error parsing the file is reported using *syslogd*(8) as level ERR on
facility AUTH.

* SEE ALSO
*su*(1).
