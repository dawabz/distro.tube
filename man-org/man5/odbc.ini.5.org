#+TITLE: Manpages - odbc.ini.5
#+DESCRIPTION: Linux manpage for odbc.ini.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
/etc/odbc.ini, $HOME/.odbc.ini - unixODBC data sources configuration

* DESCRIPTION
*/etc/odbc.ini* is text configuration file for the system wide ODBC data
sources (i. e. database connections). *$HOME/.odbc.ini* contains the
configuration for user-specific data sources. Both paths may be
overridden by /unixODBC/ build options, see *odbcinst -j* for the
definitive paths on your system.

* NOTES
** Templates
Where possible, install ODBC DSNs using the *odbcinst* utility from a
template .ini file. Many drivers supply templates.

** FILE FORMAT
*odbc.ini* follows the pesudo-standard /ini file/* syntax convention of*
one or more *[section headings]*, each followed by zero or more *key =*
value attributes.

- [ODBC Data Sources] section :: The required section *[ODBC Data
  Sources]* lists each data source name (/DSN/) as a key. The associated
  values serve as comments. Each entry must be matched by an ini file
  *[section]* describing the data source.

- [dsn] sections :: Each data source is identified by a *[section
  header]*, which is the DSN name used by applications. Each DSN
  definition section may contain values for the keys:

  - · Driver (REQUIRED) :: Name of the ODBC driver to use for this DSN.
    The name must exactly match the *[section name]* of the driver
    definition in *odbcinst.ini* as listed by *odbcinst -q -d*.

  - · Description :: Human-readable data source description.

  - · Database :: Database name or identifier. Meaning is
    driver-specific. May specify a file path, unix socket path,
    identifier relative to a server name, etc.

  - · Servername :: Server name. Meaning is driver specific, but
    generally specifies a DNS name, IP network address, or
    driver-specific discovery identifier.

For a full list of supported parameters see the HTML-format
"Administrator Manual" shipped with /unixODBC/, the documentation for
your driver, and any datasource templates supplied by your driver.

* EXAMPLES
An example *odbc.init* is shown in the "Administrator Manual" shipped
with /unixODBC/.

* SEE ALSO
*unixODBC*(7), *odbcinst*(1), *isql*(1), *iusql*(1), *odbcinst.ini*(5)

®

* AUTHORS
The authors of unixODBC are Peter Harvey </pharvey@codebydesign.com/>
and Nick Gorham </nick@lurcher.org/>. For the full list of contributors
see the AUTHORS file.

* COPYRIGHT
unixODBC is licensed under the GNU Lesser General Public License. For
details about the license, see the COPYING file.
