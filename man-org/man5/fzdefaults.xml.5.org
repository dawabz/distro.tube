#+TITLE: Manpages - fzdefaults.xml.5
#+DESCRIPTION: Linux manpage for fzdefaults.xml.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fzdefaults.xml - Default options for FileZilla

* SYNOPSIS
*/etc/filezilla/fzdefaults.xml*\\
*~/.filezilla/fzdefaults.xml*

* DESCRIPTION
This configuration file can be used to specify some system‐wide default
options for FileZilla:

- Location of user's settings directory

- Predefined list of Sitemanager entries

- Kiosk mode to prevent FileZilla from saving any passwords

- Disabling of update check capablity

Please search for the file *fzdefaults.xml.example* for further
information. It is often found in the /usr/share/filezilla/docs
directory.

* SUPPORT
Please visit https://filezilla-project.org/ for further information.
Report bugs only if you are using the latest version available from the
FileZilla website.

* COPYRIGHT
Copyright (C) 2004-2018 Tim Kosse

FileZilla is distributed under the terms of the GNU General Public
License version 2 or later.

* SEE ALSO
filezilla(1)
