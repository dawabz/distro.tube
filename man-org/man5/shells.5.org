#+TITLE: Manpages - shells.5
#+DESCRIPTION: Linux manpage for shells.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
shells - pathnames of valid login shells

* DESCRIPTION
//etc/shells/ is a text file which contains the full pathnames of valid
login shells. This file is consulted by *chsh*(1) and available to be
queried by other programs.

Be aware that there are programs which consult this file to find out if
a user is a normal user; for example, FTP daemons traditionally disallow
access to users with shells not included in this file.

* FILES
//etc/shells/

* EXAMPLES
//etc/shells/ may contain the following paths:

#+begin_example
  /bin/sh
  /bin/bash
  /bin/csh
#+end_example

* SEE ALSO
*chsh*(1), *getusershell*(3), *pam_shells*(8)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
