#+TITLE: Manpages - os-release.5
#+DESCRIPTION: Linux manpage for os-release.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
os-release, initrd-release - Operating system identification

* SYNOPSIS
/etc/os-release

/usr/lib/os-release

/etc/initrd-release

* DESCRIPTION
The /etc/os-release and /usr/lib/os-release files contain operating
system identification data.

The basic file format of os-release is a newline-separated list of
environment-like shell-compatible variable assignments. It is possible
to source the configuration from shell scripts, however, beyond mere
variable assignments, no shell features are supported (this means
variable expansion is explicitly not supported), allowing applications
to read the file without implementing a shell compatible execution
engine. Variable assignment values must be enclosed in double or single
quotes if they include spaces, semicolons or other special characters
outside of A--Z, a--z, 0--9. Shell special characters ("$", quotes,
backslash, backtick) must be escaped with backslashes, following shell
style. All strings should be in UTF-8 format, and non-printable
characters should not be used. It is not supported to concatenate
multiple individually quoted strings. Lines beginning with "#" shall be
ignored as comments. Blank lines are permitted and ignored.

The file /etc/os-release takes precedence over /usr/lib/os-release.
Applications should check for the former, and exclusively use its data
if it exists, and only fall back to /usr/lib/os-release if it is
missing. Applications should not read data from both files at the same
time. /usr/lib/os-release is the recommended place to store OS release
information as part of vendor trees. /etc/os-release should be a
relative symlink to /usr/lib/os-release, to provide compatibility with
applications only looking at /etc/. A relative symlink instead of an
absolute symlink is necessary to avoid breaking the link in a chroot or
initrd environment such as dracut.

os-release contains data that is defined by the operating system vendor
and should generally not be changed by the administrator.

As this file only encodes names and identifiers it should not be
localized.

The /etc/os-release and /usr/lib/os-release files might be symlinks to
other files, but it is important that the file is available from
earliest boot on, and hence must be located on the root file system.

For a longer rationale for os-release please refer to the *Announcement
of /etc/os-release*[1].

** /etc/initrd-release
In the *initrd*[2], /etc/initrd-release plays the same role as
os-release in the main system. Additionally, the presence of that file
means that the system is in the initrd phase. /etc/os-release should be
symlinked to /etc/initrd-release (or vice versa), so programs that only
look for /etc/os-release (as described above) work correctly. The rest
of this document that talks about os-release should be understood to
apply to initrd-release too.

* OPTIONS
The following OS identifications parameters may be set using os-release:

** General information identifying the operating system
/NAME=/

#+begin_quote
  A string identifying the operating system, without a version
  component, and suitable for presentation to the user. If not set, a
  default of "NAME=Linux" may be used.

  Examples: "NAME=Fedora", "NAME="Debian GNU/Linux"".
#+end_quote

/ID=/

#+begin_quote
  A lower-case string (no spaces or other characters outside of 0--9,
  a--z, ".", "_" and "-") identifying the operating system, excluding
  any version information and suitable for processing by scripts or
  usage in generated filenames. If not set, a default of "ID=linux" may
  be used.

  Examples: "ID=fedora", "ID=debian".
#+end_quote

/ID_LIKE=/

#+begin_quote
  A space-separated list of operating system identifiers in the same
  syntax as the /ID=/ setting. It should list identifiers of operating
  systems that are closely related to the local operating system in
  regards to packaging and programming interfaces, for example listing
  one or more OS identifiers the local OS is a derivative from. An OS
  should generally only list other OS identifiers it itself is a
  derivative of, and not any OSes that are derived from it, though
  symmetric relationships are possible. Build scripts and similar should
  check this variable if they need to identify the local operating
  system and the value of /ID=/ is not recognized. Operating systems
  should be listed in order of how closely the local operating system
  relates to the listed ones, starting with the closest. This field is
  optional.

  Examples: for an operating system with "ID=centos", an assignment of
  "ID_LIKE="rhel fedora"" would be appropriate. For an operating system
  with "ID=ubuntu", an assignment of "ID_LIKE=debian" is appropriate.
#+end_quote

/PRETTY_NAME=/

#+begin_quote
  A pretty operating system name in a format suitable for presentation
  to the user. May or may not contain a release code name or OS version
  of some kind, as suitable. If not set, a default of
  "PRETTY_NAME="Linux"" may be used

  Example: "PRETTY_NAME="Fedora 17 (Beefy Miracle)"".
#+end_quote

/CPE_NAME=/

#+begin_quote
  A CPE name for the operating system, in URI binding syntax, following
  the *Common Platform Enumeration Specification*[3] as proposed by the
  NIST. This field is optional.

  Example: "CPE_NAME="cpe:/o:fedoraproject:fedora:17""
#+end_quote

/VARIANT=/

#+begin_quote
  A string identifying a specific variant or edition of the operating
  system suitable for presentation to the user. This field may be used
  to inform the user that the configuration of this system is subject to
  a specific divergent set of rules or default configuration settings.
  This field is optional and may not be implemented on all systems.

  Examples: "VARIANT="Server Edition"", "VARIANT="Smart Refrigerator
  Edition"".

  Note: this field is for display purposes only. The /VARIANT_ID/ field
  should be used for making programmatic decisions.
#+end_quote

/VARIANT_ID=/

#+begin_quote
  A lower-case string (no spaces or other characters outside of 0--9,
  a--z, ".", "_" and "-"), identifying a specific variant or edition of
  the operating system. This may be interpreted by other packages in
  order to determine a divergent default configuration. This field is
  optional and may not be implemented on all systems.

  Examples: "VARIANT_ID=server", "VARIANT_ID=embedded".
#+end_quote

** Information about the version of the operating system
/VERSION=/

#+begin_quote
  A string identifying the operating system version, excluding any OS
  name information, possibly including a release code name, and suitable
  for presentation to the user. This field is optional.

  Examples: "VERSION=17", "VERSION="17 (Beefy Miracle)"".
#+end_quote

/VERSION_ID=/

#+begin_quote
  A lower-case string (mostly numeric, no spaces or other characters
  outside of 0--9, a--z, ".", "_" and "-") identifying the operating
  system version, excluding any OS name information or release code
  name, and suitable for processing by scripts or usage in generated
  filenames. This field is optional.

  Examples: "VERSION_ID=17", "VERSION_ID=11.04".
#+end_quote

/VERSION_CODENAME=/

#+begin_quote
  A lower-case string (no spaces or other characters outside of 0--9,
  a--z, ".", "_" and "-") identifying the operating system release code
  name, excluding any OS name information or release version, and
  suitable for processing by scripts or usage in generated filenames.
  This field is optional and may not be implemented on all systems.

  Examples: "VERSION_CODENAME=buster", "VERSION_CODENAME=xenial".
#+end_quote

/BUILD_ID=/

#+begin_quote
  A string uniquely identifying the system image originally used as the
  installation base. In most cases, /VERSION_ID/ or
  /IMAGE_ID/+/IMAGE_VERSION/ are updated when the entire system image is
  replaced during an update. /BUILD_ID/ may be used in distributions
  where the original installation image version is important:
  /VERSION_ID/ would change during incremental system updates, but
  /BUILD_ID/ would not. This field is optional.

  Examples: "BUILD_ID="2013-03-20.3"", "BUILD_ID=201303203".
#+end_quote

/IMAGE_ID=/

#+begin_quote
  A lower-case string (no spaces or other characters outside of 0--9,
  a--z, ".", "_" and "-"), identifying a specific image of the operating
  system. This is supposed to be used for environments where OS images
  are prepared, built, shipped and updated as comprehensive, consistent
  OS images. This field is optional and may not be implemented on all
  systems, in particularly not on those that are not managed via images
  but put together and updated from individual packages and on the local
  system.

  Examples: "IMAGE_ID=vendorx-cashier-system", "IMAGE_ID=netbook-image".
#+end_quote

/IMAGE_VERSION=/

#+begin_quote
  A lower-case string (mostly numeric, no spaces or other characters
  outside of 0--9, a--z, ".", "_" and "-") identifying the OS image
  version. This is supposed to be used together with /IMAGE_ID/
  described above, to discern different versions of the same image.

  Examples: "IMAGE_VERSION=33", "IMAGE_VERSION=47.1rc1".
#+end_quote

To summarize: if the image updates are built and shipped as
comprehensive units, /IMAGE_ID/+/IMAGE_VERSION/ is the best fit.
Otherwise, if updates eventually completely replace previously installed
contents, as in a typical binary distribution, /VERSION_ID/ should be
used to identify major releases of the operating system. /BUILD_ID/ may
be used instead or in addition to /VERSION_ID/ when the original system
image version is important.

** Presentation information and links
/HOME_URL=/, /DOCUMENTATION_URL=/, /SUPPORT_URL=/, /BUG_REPORT_URL=/,
/PRIVACY_POLICY_URL=/

#+begin_quote
  Links to resources on the Internet related to the operating system.
  /HOME_URL=/ should refer to the homepage of the operating system, or
  alternatively some homepage of the specific version of the operating
  system. /DOCUMENTATION_URL=/ should refer to the main documentation
  page for this operating system. /SUPPORT_URL=/ should refer to the
  main support page for the operating system, if there is any. This is
  primarily intended for operating systems which vendors provide support
  for. /BUG_REPORT_URL=/ should refer to the main bug reporting page for
  the operating system, if there is any. This is primarily intended for
  operating systems that rely on community QA. /PRIVACY_POLICY_URL=/
  should refer to the main privacy policy page for the operating system,
  if there is any. These settings are optional, and providing only some
  of these settings is common. These URLs are intended to be exposed in
  "About this system" UIs behind links with captions such as "About this
  Operating System", "Obtain Support", "Report a Bug", or "Privacy
  Policy". The values should be in *RFC3986 format*[4], and should be
  "http:" or "https:" URLs, and possibly "mailto:" or "tel:". Only one
  URL shall be listed in each setting. If multiple resources need to be
  referenced, it is recommended to provide an online landing page
  linking all available resources.

  Examples: "HOME_URL="https://fedoraproject.org/"",
  "BUG_REPORT_URL="https://bugzilla.redhat.com/"".
#+end_quote

/LOGO=/

#+begin_quote
  A string, specifying the name of an icon as defined by
  *freedesktop.org Icon Theme Specification*[5]. This can be used by
  graphical applications to display an operating systems or distributors
  logo. This field is optional and may not necessarily be implemented on
  all systems.

  Examples: "LOGO=fedora-logo", "LOGO=distributor-logo-opensuse"
#+end_quote

/ANSI_COLOR=/

#+begin_quote
  A suggested presentation color when showing the OS name on the
  console. This should be specified as string suitable for inclusion in
  the ESC [ m ANSI/ECMA-48 escape code for setting graphical rendition.
  This field is optional.

  Examples: "ANSI_COLOR="0;31"" for red, "ANSI_COLOR="1;34"" for light
  blue, or "ANSI_COLOR="0;38;2;60;110;180"" for Fedora blue.
#+end_quote

** Distribution-level defaults and metadata
/DEFAULT_HOSTNAME=/

#+begin_quote
  A string specifying the hostname if *hostname*(5) is not present and
  no other configuration source specifies the hostname. Must be either a
  single DNS label (a string composed of 7-bit ASCII lower-case
  characters and no spaces or dots, limited to the format allowed for
  DNS domain name labels), or a sequence of such labels separated by
  single dots that forms a valid DNS FQDN. The hostname must be at most
  64 characters, which is a Linux limitation (DNS allows longer names).

  See *org.freedesktop.hostname1*(5) for a description of how
  *systemd-hostnamed.service*(8) determines the fallback hostname.
#+end_quote

/SYSEXT_LEVEL=/

#+begin_quote
  A lower-case string (mostly numeric, no spaces or other characters
  outside of 0--9, a--z, ".", "_" and "-") identifying the operating
  system extensions support level, to indicate which extension images
  are supported. See *systemd-sysext*(8)) for more information.

  Examples: "SYSEXT_LEVEL=2", "SYSEXT_LEVEL=15.14".
#+end_quote

** Notes
If you are using this file to determine the OS or a specific version of
it, use the /ID/ and /VERSION_ID/ fields, possibly with /ID_LIKE/ as
fallback for /ID/. When looking for an OS identification string for
presentation to the user use the /PRETTY_NAME/ field.

Note that operating system vendors may choose not to provide version
information, for example to accommodate for rolling releases. In this
case, /VERSION/ and /VERSION_ID/ may be unset. Applications should not
rely on these fields to be set.

Operating system vendors may extend the file format and introduce new
fields. It is highly recommended to prefix new fields with an OS
specific name in order to avoid name clashes. Applications reading this
file must ignore unknown fields.

Example: "DEBIAN_BTS="debbugs://bugs.debian.org/"".

Container and sandbox runtime managers may make the hosts identification
data available to applications by providing the hosts /etc/os-release
(if available, otherwise /usr/lib/os-release as a fallback) as
/run/host/os-release.

* EXAMPLES
*Example 1. os-release file for Fedora Workstation*

#+begin_quote
  #+begin_example
    NAME=Fedora
    VERSION="32 (Workstation Edition)"
    ID=fedora
    VERSION_ID=32
    PRETTY_NAME="Fedora 32 (Workstation Edition)"
    ANSI_COLOR="0;38;2;60;110;180"
    LOGO=fedora-logo-icon
    CPE_NAME="cpe:/o:fedoraproject:fedora:32"
    HOME_URL="https://fedoraproject.org/"
    DOCUMENTATION_URL="https://docs.fedoraproject.org/en-US/fedora/f32/system-administrators-guide/"
    SUPPORT_URL="https://fedoraproject.org/wiki/Communicating_and_getting_help"
    BUG_REPORT_URL="https://bugzilla.redhat.com/"
    REDHAT_BUGZILLA_PRODUCT="Fedora"
    REDHAT_BUGZILLA_PRODUCT_VERSION=32
    REDHAT_SUPPORT_PRODUCT="Fedora"
    REDHAT_SUPPORT_PRODUCT_VERSION=32
    PRIVACY_POLICY_URL="https://fedoraproject.org/wiki/Legal:PrivacyPolicy"
    VARIANT="Workstation Edition"
    VARIANT_ID=workstation
  #+end_example
#+end_quote

*Example 2. Reading os-release in sh(1)*

#+begin_quote
  #+begin_example
    #!/bin/sh -eu

    test -e /etc/os-release && os_release=/etc/os-release || os_release=/usr/lib/os-release
    . "${os_release}"

    echo "Running on ${PRETTY_NAME:-Linux}"

    if [ "${ID:-linux}" = "debian" ] || [ "${ID_LIKE#*debian*}" != "${ID_LIKE}" ]; then
        echo "Looks like Debian!"
    fi
  #+end_example
#+end_quote

*Example 3. Reading os-release in python(1)*

#+begin_quote
  #+begin_example
    #!/usr/bin/python

    import ast
    import re
    import sys

    def read_os_release():
        try:
            filename = /etc/os-release
            f = open(filename)
        except FileNotFoundError:
            filename = /usr/lib/os-release
            f = open(filename)

        for line_number, line in enumerate(f):
            line = line.rstrip()
            if not line or line.startswith(#):
                continue
            if m := re.match(r([A-Z][A-Z_0-9]+)=(.*), line):
                name, val = m.groups()
                if val and val[0] in "\:
                    val = ast.literal_eval(val)
                yield name, val
            else:
                print(f{filename}:{line_number + 1}: bad line {line!r},
                      file=sys.stderr)

    os_release = dict(read_os_release())

    pretty_name = os_release.get(PRETTY_NAME, Linux)
    print(fRunning on {pretty_name})

    if debian in [os_release.get(ID, linux),
                    *os_release.get(ID_LIKE, ).split()]:
        print(Looks like Debian!)
  #+end_example
#+end_quote

* SEE ALSO
*systemd*(1), *lsb_release*(1), *hostname*(5), *machine-id*(5),
*machine-info*(5)

* NOTES
-  1. :: Announcement of /etc/os-release

  http://0pointer.de/blog/projects/os-release

-  2. :: initrd

  https://www.kernel.org/doc/html/latest/admin-guide/initrd.html

-  3. :: Common Platform Enumeration Specification

  http://scap.nist.gov/specifications/cpe/

-  4. :: RFC3986 format

  https://tools.ietf.org/html/rfc3986

-  5. :: freedesktop.org Icon Theme Specification

  http://standards.freedesktop.org/icon-theme-spec/latest
