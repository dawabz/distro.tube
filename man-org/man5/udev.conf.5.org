#+TITLE: Manpages - udev.conf.5
#+DESCRIPTION: Linux manpage for udev.conf.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
udev.conf - Configuration for device event managing daemon

* SYNOPSIS
/etc/udev/udev.conf

* DESCRIPTION
*systemd-udevd*(8) expects its main configuration file at
/etc/udev/udev.conf. It consists of a set of variables allowing the user
to override default udev values. All empty lines or lines beginning with
# are ignored. The following variables can be set:

/udev_log=/

#+begin_quote
  The log level. Valid values are the numerical syslog priorities or
  their textual representations: *err*, *info* and *debug*.
#+end_quote

/children_max=/

#+begin_quote
  An integer. The maximum number of events executed in parallel.

  This is the same as the *--children-max=* option.
#+end_quote

/exec_delay=/

#+begin_quote
  An integer. Delay the execution of each /RUN{program}/ parameter by
  the given number of seconds. This option might be useful when
  debugging system crashes during coldplug caused by loading non-working
  kernel modules.

  This is the same as the *--exec-delay=* option.
#+end_quote

/event_timeout=/

#+begin_quote
  An integer. The number of seconds to wait for events to finish. After
  this time, the event will be terminated. The default is 180 seconds.

  This is the same as the *--event-timeout=* option.
#+end_quote

/resolve_names=/

#+begin_quote
  Specifies when systemd-udevd should resolve names of users and groups.
  When set to *early* (the default), names will be resolved when the
  rules are parsed. When set to *late*, names will be resolved for every
  event. When set to *never*, names will never be resolved and all
  devices will be owned by root.

  This is the same as the *--resolve-names=* option.
#+end_quote

/timeout_signal=/

#+begin_quote
  Specifies a signal that systemd-udevd will send on worker timeouts.
  Note that both workers and spawned processes will be killed using this
  signal. Defaults to *SIGKILL*.
#+end_quote

In addition, systemd-udevd can be configured by command line options and
the kernel command line (see *systemd-udevd*(8)).

* SEE ALSO
*systemd-udevd*(8), *udev*(7), *udevadm*(8)
