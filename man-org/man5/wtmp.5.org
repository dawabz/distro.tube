#+TITLE: Manpages - wtmp.5
#+DESCRIPTION: Linux manpage for wtmp.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about wtmp.5 is found in manpage for: [[../man5/utmp.5][man5/utmp.5]]