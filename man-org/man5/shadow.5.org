#+TITLE: Manpages - shadow.5
#+DESCRIPTION: Linux manpage for shadow.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
shadow - shadowed password file

* DESCRIPTION
shadow is a file which contains the password information for the systems
accounts and optional aging information.

This file must not be readable by regular users if password security is
to be maintained.

Each line of this file contains 9 fields, separated by colons (“:”), in
the following order:

*login name*

#+begin_quote
  It must be a valid account name, which exist on the system.
#+end_quote

*encrypted password*

#+begin_quote
  This field may be empty, in which case no passwords are required to
  authenticate as the specified login name. However, some applications
  which read the /etc/shadow file may decide not to permit any access at
  all if the password field is empty.

  A password field which starts with an exclamation mark means that the
  password is locked. The remaining characters on the line represent the
  password field before the password was locked.

  Refer to *crypt*(3) for details on how this string is interpreted.

  If the password field contains some string that is not a valid result
  of *crypt*(3), for instance ! or *, the user will not be able to use a
  unix password to log in (but the user may log in the system by other
  means).
#+end_quote

*date of last password change*

#+begin_quote
  The date of the last password change, expressed as the number of days
  since Jan 1, 1970.

  The value 0 has a special meaning, which is that the user should
  change her password the next time she will log in the system.

  An empty field means that password aging features are disabled.
#+end_quote

*minimum password age*

#+begin_quote
  The minimum password age is the number of days the user will have to
  wait before she will be allowed to change her password again.

  An empty field and value 0 mean that there are no minimum password
  age.
#+end_quote

*maximum password age*

#+begin_quote
  The maximum password age is the number of days after which the user
  will have to change her password.

  After this number of days is elapsed, the password may still be valid.
  The user should be asked to change her password the next time she will
  log in.

  An empty field means that there are no maximum password age, no
  password warning period, and no password inactivity period (see
  below).

  If the maximum password age is lower than the minimum password age,
  the user cannot change her password.
#+end_quote

*password warning period*

#+begin_quote
  The number of days before a password is going to expire (see the
  maximum password age above) during which the user should be warned.

  An empty field and value 0 mean that there are no password warning
  period.
#+end_quote

*password inactivity period*

#+begin_quote
  The number of days after a password has expired (see the maximum
  password age above) during which the password should still be accepted
  (and the user should update her password during the next login).

  After expiration of the password and this expiration period is
  elapsed, no login is possible using the current users password. The
  user should contact her administrator.

  An empty field means that there are no enforcement of an inactivity
  period.
#+end_quote

*account expiration date*

#+begin_quote
  The date of expiration of the account, expressed as the number of days
  since Jan 1, 1970.

  Note that an account expiration differs from a password expiration. In
  case of an account expiration, the user shall not be allowed to login.
  In case of a password expiration, the user is not allowed to login
  using her password.

  An empty field means that the account will never expire.

  The value 0 should not be used as it is interpreted as either an
  account with no expiration, or as an expiration on Jan 1, 1970.
#+end_quote

*reserved field*

#+begin_quote
  This field is reserved for future use.
#+end_quote

* FILES
/etc/passwd

#+begin_quote
  User account information.
#+end_quote

/etc/shadow

#+begin_quote
  Secure user account information.
#+end_quote

/etc/shadow-

#+begin_quote
  Backup file for /etc/shadow.

  Note that this file is used by the tools of the shadow toolsuite, but
  not by all user and password management tools.
#+end_quote

* SEE ALSO
*chage*(1), *login*(1), *passwd*(1), *passwd*(5), *pwck*(8),
*pwconv*(8), *pwunconv*(8), *su*(1), *sulogin*(8).
