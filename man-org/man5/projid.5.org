#+TITLE: Manpages - projid.5
#+DESCRIPTION: Linux manpage for projid.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
projid - the project name mapping file

* DESCRIPTION
The //etc/projid/ file provides a mapping between numeric project
identifiers and a simple human readable name (similar relationship to
the one that exists between usernames and uids). Its format is simply:

#+begin_example

  # comments are hash-prefixed
  # ...
  cage:10
  logfiles:42
#+end_example

This file is optional, if a project identifier cannot be mapped to a
name, it will be parsed and displayed as a numeric value.

* SEE ALSO
*xfs_quota*(8), *xfsctl*(3), *projects*(5).
