#+TITLE: Manpages - sane-kvs20xx.5
#+DESCRIPTION: Linux manpage for sane-kvs20xx.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sane-kvs20xx - SANE backend for Panasonic KV-S20xxC USB/SCSI ADF
scanners

* DESCRIPTION
The *sane-kvs20xx* library implements a SANE (Scanner Access Now Easy)
backend which provides access to the Panasonic KV-S202xC and KV-S204xC
scanners.

* KNOWN ISSUES
This document was written by the SANE project, which has no information
regarding the capabilities or reliability of the backend. All
information contained here is suspect.

* CREDITS
The backend was written by Panasonic Russia Ltd.

The backend was ported to sane-backends 1.0.22 and downgraded to C89 by
m. allan noah.

* SEE ALSO
*sane*(7), *sane-usb*(5), *sane-scsi*(5)

* AUTHOR
m. allan noah: </kitno455 a t gmail d o t com/>
