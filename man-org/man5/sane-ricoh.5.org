#+TITLE: Manpages - sane-ricoh.5
#+DESCRIPTION: Linux manpage for sane-ricoh.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sane-ricoh - SANE backend for Ricoh flatbed scanners

* DESCRIPTION
The *sane-ricoh* library implements a SANE (Scanner Access Now Easy)
backend that provides access to the following Ricoh flatbed scanners:

#+begin_quote
  IS50\\
  IS60\\
#+end_quote

* DEVICE NAMES
This backend expects device names of the form:

#+begin_quote
  /special/
#+end_quote

Where /special/ is the path-name for the special device that corresponds
to a SCSI scanner. The special device name must be a generic SCSI device
or a symlink to such a device. The program *sane-find-scanner*(1) helps
to find out the correct device. Under Linux, such a device name could be
//dev/sga/ or //dev/sge/, for example. See *sane-scsi*(5) for details.

* FILES
- //etc/sane.d/ricoh.conf/ :: The backend configuration file (see also
  description of *SANE_CONFIG_DIR* below).

- //usr/lib/sane/libsane-ricoh.a/ :: The static library implementing
  this backend.

- //usr/lib/sane/libsane-ricoh.so/ :: The shared library implementing
  this backend (present on systems that support dynamic loading).

* ENVIRONMENT
- *SANE_CONFIG_DIR* :: This environment variable specifies the list of
  directories that may contain the configuration file. Under UNIX, the
  directories are separated by a colon (`:'), under OS/2, they are
  separated by a semi-colon (`;'). If this variable is not set, the
  configuration file is searched in two default directories: first, the
  current working directory (".") and then in //etc/sane.d/. If the
  value of the environment variable ends with the directory separator
  character, then the default directories are searched after the
  explicitly specified directories. For example, setting
  *SANE_CONFIG_DIR* to "/tmp/config:" would result in directories
  /tmp/config/, /./, and //etc/sane.d/ being searched (in this order).

- *SANE_DEBUG_RICOH* :: If the library was compiled with debug support
  enabled, this environment variable controls the debug level for this
  backend. Higher debug levels increase the verbosity of the output.

Example: export SANE_DEBUG_RICOH=4

* SEE ALSO
*sane*(7), *sane-scsi*(5), *sane-find-scanner*(1)

* AUTHOR
Feico W. Dillema
