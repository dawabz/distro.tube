#+TITLE: Manpages - systemd.automount.5
#+DESCRIPTION: Linux manpage for systemd.automount.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd.automount - Automount unit configuration

* SYNOPSIS
/automount/.automount

* DESCRIPTION
A unit configuration file whose name ends in ".automount" encodes
information about a file system automount point controlled and
supervised by systemd.

This man page lists the configuration options specific to this unit
type. See *systemd.unit*(5) for the common options of all unit
configuration files. The common configuration items are configured in
the generic [Unit] and [Install] sections. The automount specific
configuration options are configured in the [Automount] section.

Automount units must be named after the automount directories they
control. Example: the automount point /home/lennart must be configured
in a unit file home-lennart.automount. For details about the escaping
logic used to convert a file system path to a unit name see
*systemd.unit*(5). Note that automount units cannot be templated, nor is
it possible to add multiple names to an automount unit by creating
additional symlinks to its unit file.

For each automount unit file a matching mount unit file (see
*systemd.mount*(5) for details) must exist which is activated when the
automount path is accessed. Example: if an automount unit
home-lennart.automount is active and the user accesses /home/lennart the
mount unit home-lennart.mount will be activated.

Automount units may be used to implement on-demand mounting as well as
parallelized mounting of file systems.

Note that automount units are separate from the mount itself, so you
should not set /After=/ or /Requires=/ for mount dependencies here. For
example, you should not set /After=network-online.target/ or similar on
network filesystems. Doing so may result in an ordering cycle.

Note that automount support on Linux is privileged, automount units are
hence only available in the system service manager (and roots user
service manager), but not in unprivileged users service manager.

* AUTOMATIC DEPENDENCIES
** Implicit Dependencies
The following dependencies are implicitly added:

#+begin_quote
  ·

  If an automount unit is beneath another mount unit in the file system
  hierarchy, both a requirement and an ordering dependency between both
  units are created automatically.
#+end_quote

#+begin_quote
  ·

  An implicit /Before=/ dependency is created between an automount unit
  and the mount unit it activates.
#+end_quote

** Default Dependencies
The following dependencies are added unless /DefaultDependencies=no/ is
set:

#+begin_quote
  ·

  Automount units acquire automatic /Before=/ and /Conflicts=/ on
  umount.target in order to be stopped during shutdown.
#+end_quote

#+begin_quote
  ·

  Automount units automatically gain an /After=/ dependency on
  local-fs-pre.target, and a /Before=/ dependency on local-fs.target.
#+end_quote

* FSTAB
Automount units may either be configured via unit files, or via
/etc/fstab (see *fstab*(5) for details).

For details how systemd parses /etc/fstab see *systemd.mount*(5).

If an automount point is configured in both /etc/fstab and a unit file,
the configuration in the latter takes precedence.

* OPTIONS
Automount files must include an [Automount] section, which carries
information about the file system automount points it supervises. The
options specific to the [Automount] section of automount units are the
following:

/Where=/

#+begin_quote
  Takes an absolute path of a directory of the automount point. If the
  automount point does not exist at time that the automount point is
  installed, it is created. This string must be reflected in the unit
  filename. (See above.) This option is mandatory.
#+end_quote

/DirectoryMode=/

#+begin_quote
  Directories of automount points (and any parent directories) are
  automatically created if needed. This option specifies the file system
  access mode used when creating these directories. Takes an access mode
  in octal notation. Defaults to 0755.
#+end_quote

/TimeoutIdleSec=/

#+begin_quote
  Configures an idle timeout. Once the mount has been idle for the
  specified time, systemd will attempt to unmount. Takes a unit-less
  value in seconds, or a time span value such as "5min 20s". Pass 0 to
  disable the timeout logic. The timeout is disabled by default.
#+end_quote

* SEE ALSO
*systemd*(1), *systemctl*(1), *systemd.unit*(5), *systemd.mount*(5),
*mount*(8), *automount*(8), *systemd.directives*(7)
