#+TITLE: Manpages - sudoers_timestamp.5
#+DESCRIPTION: Linux manpage for sudoers_timestamp.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The

plugin uses per-user time stamp files for credential caching. Once a
user has been authenticated, they may use

without a password for a short period of time

minutes unless overridden by the

option

By default,

uses a separate record for each terminal, which means that a user's
login sessions are authenticated separately. The

option can be used to select the type of time stamp record

will use.

A multi-record time stamp file format was introduced in

1.8.10 that uses a single file per user. Previously, a separate file was
used for each user and terminal combination unless tty-based time stamps
were disabled. The new format is extensible and records of multiple
types and versions may coexist within the same file.

All records, regardless of type or version, begin with a 16-bit version
number and a 16-bit record size.

Time stamp records have the following structure:

/* Time stamp entry types */ #define TS_GLOBAL 0x01 /* not restricted by
tty or ppid */ #define TS_TTY 0x02 /* restricted by tty */ #define
TS_PPID 0x03 /* restricted by ppid */ #define TS_LOCKEXCL 0x04 /*
special lock record */

/* Time stamp flags */ #define TS_DISABLED 0x01 /* entry disabled */
#define TS_ANYUID 0x02 /* ignore uid, only valid in key */

struct timestamp_entry { unsigned short version; /* version number */
unsigned short size; /* entry size */ unsigned short type; /* TS_GLOBAL,
TS_TTY, TS_PPID */ unsigned short flags; /* TS_DISABLED, TS_ANYUID */
uid_t auth_uid; /* uid to authenticate as */ pid_t sid; /* session ID
associated with tty/ppid */ struct timespec start_time; /* session/ppid
start time */ struct timespec ts; /* time stamp (CLOCK_MONOTONIC) */
union { dev_t ttydev; /* tty device number */ pid_t ppid; /* parent pid
*/ } u; };

The timestamp_entry struct fields are as follows:

The version number of the timestamp_entry struct. New entries are
created with a version number of 2. Records with different version
numbers may coexist in the same file but are not inter-operable.

The size of the record in bytes.

The record type, currently

or

Zero or more record flags which can be bit-wise ORed together. Supported
flags are

for records disabled via

and

which is used only when matching records.

The user-ID that was used for authentication. Depending on the value of
the

and

options, the user-ID may be that of the invoking user, the root user,
the default runas user or the target user.

The ID of the user's terminal session, if present. The session ID is
only used when matching records of type

The start time of the session leader for records of type

or of the parent process for records of type

The

is used to help prevent re-use of a time stamp record after a user has
logged out. Not all systems support a method to easily retrieve a
process's start time. The

field was added in

version 1.8.22 for the second revision of the timestamp_entry struct.

The actual time stamp. A monotonic time source (which does not move
backward) is used if the system supports it. Where possible,

uses a monotonic timer that increments even while the system is
suspended. The value of

is updated each time a command is run via

If the difference between

and the current time is less than the value of the

option, no password is required.

The device number of the terminal associated with the session for
records of type

The ID of the parent process for records of type

In

versions 1.8.10 through 1.8.14, the entire time stamp file was locked
for exclusive access when reading or writing to the file. Starting in

1.8.15, individual records are locked in the time stamp file instead of
the entire file and the lock is held for a longer period of time. This
scheme is described below.

The first record in the time stamp file is of type

and is used as a

record to prevent more than one

process from adding a new record at the same time. Once the desired time
stamp record has been located or created (and locked), the

record is unlocked. The lock on the individual time stamp record,
however, is held until authentication is complete. This allows

to avoid prompting for a password multiple times when it is used more
than once in a pipeline.

Records of type

cannot be locked for a long period of time since doing so would
interfere with other

processes. Instead, a separate lock record is used to prevent multiple

processes using the same terminal (or parent process ID) from prompting
for a password as the same time.

Originally,

used a single zero-length file per user and the file's modification time
was used as the time stamp. Later versions of

added restrictions on the ownership of the time stamp files and
directory as well as checks on the validity of the time stamp itself.
Notable changes were introduced in the following

versions:

Support for tty-based time stamp file was added by appending the
terminal name to the time stamp file name.

The time stamp file was replaced by a per-user directory which contained
any tty-based time stamp files.

The target user name was added to the time stamp file name when the

option was set.

Information about the terminal device was stored in tty-based time stamp
files for validity checks. This included the terminal device numbers,
inode number and, on systems where it was not updated when the device
was written to, the inode change time. This helped prevent re-use of the
time stamp file after logout.

The terminal session ID was added to tty-based time stamp files to
prevent re-use of the time stamp by the same user in a different
terminal session. It also helped prevent re-use of the time stamp file
on systems where the terminal device's inode change time was updated by
writing.

A new, multi-record time stamp file format was introduced that uses a
single file per user. The terminal device's change time was not included
since most systems now update the change time after a write is performed
as required by POSIX.

Individual records are locked in the time stamp file instead of the
entire file and the lock is held until authentication is complete.

The start time of the terminal session leader or parent process is now
stored in non-global time stamp records. This prevents re-use of the
time stamp file after logout in most cases.

Support was added for the kernel-based tty time stamps available in

which do not use an on-disk time stamp file.

Many people have worked on

over the years; this version consists of code written primarily by:

See the CONTRIBUTORS file in the

distribution (https://www.sudo.ws/contributors.html) for an exhaustive
list of people who have contributed to

If you feel you have found a bug in

please submit a bug report at https://bugzilla.sudo.ws/

Limited free support is available via the sudo-users mailing list, see
https://www.sudo.ws/mailman/listinfo/sudo-users to subscribe or search
the archives.

is provided

and any express or implied warranties, including, but not limited to,
the implied warranties of merchantability and fitness for a particular
purpose are disclaimed. See the LICENSE file distributed with

or https://www.sudo.ws/license.html for complete details.
