#+TITLE: Manpages - faillock.conf.5
#+DESCRIPTION: Linux manpage for faillock.conf.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
faillock.conf - pam_faillock configuration file

* DESCRIPTION
*faillock.conf* provides a way to configure the default settings for
locking the user after multiple failed authentication attempts. This
file is read by the /pam_faillock/ module and is the preferred method
over configuring /pam_faillock/ directly.

The file has a very simple /name = value/ format with possible comments
starting with /#/ character. The whitespace at the beginning of line,
end of line, and around the /=/ sign is ignored.

* OPTIONS
*dir=*//path/to/tally-directory/

#+begin_quote
  The directory where the user files with the failure records are kept.
  The default is /var/run/faillock.
#+end_quote

*audit*

#+begin_quote
  Will log the user name into the system log if the user is not found.
#+end_quote

*silent*

#+begin_quote
  Dont print informative messages to the user. Please note that when
  this option is not used there will be difference in the authentication
  behavior for users which exist on the system and non-existing users.
#+end_quote

*no_log_info*

#+begin_quote
  Dont log informative messages via *syslog*(3).
#+end_quote

*local_users_only*

#+begin_quote
  Only track failed user authentications attempts for local users in
  /etc/passwd and ignore centralized (AD, IdM, LDAP, etc.) users. The
  *faillock*(8) command will also no longer track user failed
  authentication attempts. Enabling this option will prevent a
  double-lockout scenario where a user is locked out locally and in the
  centralized mechanism.
#+end_quote

*nodelay*

#+begin_quote
  Dont enforce a delay after authentication failures.
#+end_quote

*deny=*/n/

#+begin_quote
  Deny access if the number of consecutive authentication failures for
  this user during the recent interval exceeds /n/. The default is 3.
#+end_quote

*fail_interval=*/n/

#+begin_quote
  The length of the interval during which the consecutive authentication
  failures must happen for the user account lock out is /n/ seconds. The
  default is 900 (15 minutes).
#+end_quote

*unlock_time=*/n/

#+begin_quote
  The access will be re-enabled after /n/ seconds after the lock out.
  The value 0 has the same meaning as value /never/ - the access will
  not be re-enabled without resetting the faillock entries by the
  *faillock*(8) command. The default is 600 (10 minutes).

  Note that the default directory that /pam_faillock/ uses is usually
  cleared on system boot so the access will be also re-enabled after
  system reboot. If that is undesirable a different tally directory must
  be set with the *dir* option.

  Also note that it is usually undesirable to permanently lock out users
  as they can become easily a target of denial of service attack unless
  the usernames are random and kept secret to potential attackers.
#+end_quote

*even_deny_root*

#+begin_quote
  Root account can become locked as well as regular accounts.
#+end_quote

*root_unlock_time=*/n/

#+begin_quote
  This option implies *even_deny_root* option. Allow access after /n/
  seconds to root account after the account is locked. In case the
  option is not specified the value is the same as of the *unlock_time*
  option.
#+end_quote

*admin_group=*/name/

#+begin_quote
  If a group name is specified with this option, members of the group
  will be handled by this module the same as the root account (the
  options *even_deny_root* and *root_unlock_time* will apply to them. By
  default the option is not set.
#+end_quote

* EXAMPLES
/etc/security/faillock.conf file example:

#+begin_quote
  #+begin_example
    deny=4
    unlock_time=1200
    silent
        
  #+end_example
#+end_quote

* FILES
/etc/security/faillock.conf

#+begin_quote
  the config file for custom options
#+end_quote

* SEE ALSO
*faillock*(8), *pam_faillock*(8), *pam.conf*(5), *pam.d*(5), *pam*(8)

* AUTHOR
pam_faillock was written by Tomas Mraz. The support for faillock.conf
was written by Brian Ward.
