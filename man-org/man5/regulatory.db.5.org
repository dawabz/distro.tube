#+TITLE: Manpages - regulatory.db.5
#+DESCRIPTION: Linux manpage for regulatory.db.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about regulatory.db.5 is found in manpage for: [[../man5/regulatory.bin.5.gz][man5/regulatory.bin.5.gz]]