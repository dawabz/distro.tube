#+TITLE: Manpages - homed.conf.d.5
#+DESCRIPTION: Linux manpage for homed.conf.d.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about homed.conf.d.5 is found in manpage for: [[../homed.conf.5][homed.conf.5]]