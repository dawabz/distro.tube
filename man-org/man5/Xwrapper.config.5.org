#+TITLE: Manpages - Xwrapper.config.5
#+DESCRIPTION: Linux manpage for Xwrapper.config.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about Xwrapper.config.5 is found in manpage for: [[../man1/Xorg.wrap.1][man1/Xorg.wrap.1]]