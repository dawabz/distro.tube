#+TITLE: Linux Manpages - Y
#+DESCRIPTION: Linux manpages - Y
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"

* Y
#+begin_src bash :exports results
readarray -t starts_with_y < <(find ./man* -type f -iname "y*" | sort -t '/' -k 3)

for x in "${starts_with_y[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./man3/y0.3.org][y0.3]]              |
| [[file:./man3p/y0.3p.org][y0.3p]]             |
| [[file:./man3/y0f.3.org][y0f.3]]             |
| [[file:./man3/y0l.3.org][y0l.3]]             |
| [[file:./man3/y1.3.org][y1.3]]              |
| [[file:./man3/y1f.3.org][y1f.3]]             |
| [[file:./man3/y1l.3.org][y1l.3]]             |
| [[file:./man1/y4mcolorbars.1.org][y4mcolorbars.1]]    |
| [[file:./man1/y4mdenoise.1.org][y4mdenoise.1]]      |
| [[file:./man1/y4mscaler.1.org][y4mscaler.1]]       |
| [[file:./man1/y4mtopnm.1.org][y4mtopnm.1]]        |
| [[file:./man1/y4mtoppm.1.org][y4mtoppm.1]]        |
| [[file:./man1/y4munsharp.1.org][y4munsharp.1]]      |
| [[file:./man1/yacc.1.org][yacc.1]]            |
| [[file:./man1p/yacc.1p.org][yacc.1p]]           |
| [[file:./man1/yad.1.org][yad.1]]             |
| [[file:./man1/yad-tools.1.org][yad-tools.1]]       |
| [[file:./man1/yasm.1.org][yasm.1]]            |
| [[file:./man7/yasm_arch.7.org][yasm_arch.7]]       |
| [[file:./man7/yasm_dbgfmts.7.org][yasm_dbgfmts.7]]    |
| [[file:./man7/yasm_objfmts.7.org][yasm_objfmts.7]]    |
| [[file:./man7/yasm_parsers.7.org][yasm_parsers.7]]    |
| [[file:./man1/ybmtopbm.1.org][ybmtopbm.1]]        |
| [[file:./man1/yes.1.org][yes.1]]             |
| [[file:./man3/yn.3.org][yn.3]]              |
| [[file:./man3/ynf.3.org][ynf.3]]             |
| [[file:./man3/ynl.3.org][ynl.3]]             |
| [[file:./man1/youtube-dl.1.org][youtube-dl.1]]      |
| [[file:./man1/yuv2lav.1.org][yuv2lav.1]]         |
| [[file:./man5/yuv4mpeg.5.org][yuv4mpeg.5]]        |
| [[file:./man1/yuvdenoise.1.org][yuvdenoise.1]]      |
| [[file:./man1/yuvinactive.1.org][yuvinactive.1]]     |
| [[file:./man1/yuvkineco.1.org][yuvkineco.1]]       |
| [[file:./man1/yuvmedianfilter.1.org][yuvmedianfilter.1]] |
| [[file:./man1/yuvplay.1.org][yuvplay.1]]         |
| [[file:./man1/yuvscaler.1.org][yuvscaler.1]]       |
| [[file:./man1/yuvsplittoppm.1.org][yuvsplittoppm.1]]   |
| [[file:./man1/yuvtoppm.1.org][yuvtoppm.1]]        |
| [[file:./man1/yuvycsnoise.1.org][yuvycsnoise.1]]     |
