#+TITLE: Manpages - port.4
#+DESCRIPTION: Linux manpage for port.4
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about port.4 is found in manpage for: [[../man4/mem.4][man4/mem.4]]