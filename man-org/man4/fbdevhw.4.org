#+TITLE: Manpages - fbdevhw.4
#+DESCRIPTION: Linux manpage for fbdevhw.4
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fbdevhw - os-specific submodule for framebuffer device access

* DESCRIPTION
*fbdevhw* provides functions for talking to a framebuffer device. It is
os-specific. It is a submodule used by other video drivers. A *fbdevhw*
module is currently available for linux framebuffer devices.

fbdev(4) is a non-accelerated driver which runs on top of the fbdevhw
module. fbdevhw can be used by other drivers too, this is usually
activated with `Option "UseFBDev"' in the device section.

* SEE ALSO
Xorg(1), xorg.conf(5), xorgconfig(1), Xserver(1), X(7), fbdev(4)

* AUTHORS
Authors include: Gerd Knorr, based on the XF68_FBDev Server code (Martin
Schaller, Geert Uytterhoeven).
