#+TITLE: Manpages - intro.4
#+DESCRIPTION: Linux manpage for intro.4
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
intro - introduction to special files

* DESCRIPTION
Section 4 of the manual describes special files (devices).

* FILES
/dev/* --- device files

* NOTES
** Authors and copyright conditions
Look at the header of the manual page source for the author(s) and
copyright conditions. Note that these can be different from page to
page!

* SEE ALSO
*mknod*(1), *mknod*(2), *standards*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
