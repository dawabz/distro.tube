#+TITLE: Manpages - atmsigd.conf.4
#+DESCRIPTION: Linux manpage for atmsigd.conf.4
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
atmsigd.conf - configuration file for the ATM signaling demon

* SYNOPSIS
*/etc/atmsigd.conf*

* DESCRIPTION
*atmsigd.conf* contains configuration data for *atmsigd*. *atmsigd*
reads *atmsigd.conf* after parsing the command line options, before
connecting to the ATM network.

Configuration parameters are arranged in functional groups. In order to
set a parameter, the name of the group, the name of the parameter, and
the parameter value(s) have to be specified, e.g.

#+begin_example

       sig level debug
#+end_example

decreases the logging threshold for messages related to signaling to the
*debug* level. The following options are recognized:

- debug dump path :: Specifies the directory to which *atmsigd will
  write status and trace* dumps. If tracing is not yet enabled, the
  trace size is automatically set to a (small) default value.

- debug level level :: Sets the default debug level to /level/*.
  */level/* can be any of* *debug, info, warn, error, and fatal. Only*
  messages with the same or a higher priority than the debug level are
  printed. Note that the command-line option *-d generates even more
  output (e.g. hexdumps* of all packets passing between *atmsigd and the
  network) than* *debug level debug.*

- debug log path :: Specifies the file to which *atmsigd writes logging
  messages. When* using the special file name *syslog, messages are send
  to the* system logger instead. Log messages are written to standard
  output if no log file is specified. Writing to standard output can
  also be explicitly requested by using the special file name *stderr.*

- debug trace [number] :: Enables tracing and optionally sets the number
  of entries that should be kept in the trace buffer. A (small) default
  is used if the number is omitted.

- io level level :: Sets the debug level for IO-related messages to
  /level/*.*

- io max_rate rate :: Sets the rate to signal if an application requests
  the "maximum". See qos(7) for the syntax to use for expressing the
  rate. The default maximum rate is 353207 cells/second (OC3). (Note
  that the value of *max_rate does not* constrain the rates an
  application can explicitly request.)

- io qos qos :: Configures the signaling VC to use the specified QOS
  (see qos(7) for the syntax). By default, UBR at link speed is used on
  the signaling VC.

- io vc [itf.]vpi.vci :: Uses the specified VC for signaling messages
  instead of the usual 0.0.5.

- saal level level :: Sets the debug level for messages related to SAAL
  (i.e. SSCF and SSCOP) to /level/*.*

- sig level level :: Sets the debug level for messages related to
  signaling (Q.2931 or ISP) to /level/*.*

- sig mode mode :: Set the mode of operation. The following modes are
  available: *user for* the user side, *network for the network side,
  and switch for* operation in a switch. The default behaviour is
  *user.*

- sig uni30 :: Use UNI 3.0 signaling. If specified together with *sig
  uni31, this* option sets UNI 3.1 signaling with a few
  backwards-compatibility extensions.

- sig uni31 :: Use UNI 3.1 signaling. This option can be combined with
  *sig uni30 (see* above).

- sig uni40 :: Use UNI 4.0 signaling. This option can be combined with
  *sig q.2963.1* (see below).

- sig q.2963.1 :: Enable peak cell rate renegotiation. This option is
  only available with UNI 4.0 signaling.

- sig vpci vpci itf itf :: Sets up a very simplistic type of routing.
  All calls with VPCI values equal to or greater than /vpci/* will be
  routed to */itf/*, and their* VPI values will be set to the signaled
  VPCI minus /vpci/*. Multiple* *sig vpci entries can be used to support
  an arbitrary number of* interfaces. Example: with *sig vpci 4 itf 1, a
  call signaled for* VPCI/VCI 0.x is routed to 0.0.x, a call signaled
  for 6.y is routed to 1.2.y, etc.

- policy level level :: Sets the debug level for messages related to
  policy decisions to /level/*.*

- policy decision direction address :: Takes the specified /decision/*
  for calls from or to */address/*.* /decision/* can be either allow or
  reject. */direction/* is* either *from or to. The */address/* may be
  wildcarded by* prepending a slash and the number of significant bits
  (NSAP) or digits (E.164). The rules are searched in the order in which
  they appear in *atmsigd.conf* until the first match. If no rule
  matches, the call is allowed.

- entity [itf.]vpi.vci { options ... } :: Activates a signaling entity
  on that specific VC. Multiple *entity* clauses can appear in the same
  configuration. When using *entity,* the option *io vc is not
  available.* *entity is optionally followed by the following options,
  enclosed in* curly braces: *vpci (corresponds to sig vpci),* *mode
  (corresponds to sig mode), qos (corresponds to vc* qos*), route, and
  default. The last two options determine how* outbound calls are
  routed. The *route option is followed by an address* in the format
  used for addresses in *policy. If no route entry matches* the called
  party number of an outbound call, the entry marked with *default is
  selected.*

When setting multiple parameters in the same group, the group name
doesn't have to be repeated if it is followed by the parameters in curly
braces. Example:

#+begin_example

      debug {
          level warn
          dump /var/tmp
          log syslog
          trace 100
      }
#+end_example

Line breaks can be inserted in *atmsigd.conf wherever spaces or tabs*
are allowed. Everything between a `#' and the end of the line is
considered a comment. The `#' character cannot be escaped.

If an option is specified in *atmsigd.conf and on the command* line, the
command line has priority.

* COMPATIBILITY
Certain options used by past versions of *atmsigd but no longer
documented* on the man page are still recognized and supported, but they
also yield a warning message. Future versions of *atmsigd will not
recognize those* options.

* AUTHOR
Werner Almesberger, EPFL ICA <Werner.Almesberger@epfl.ch>

* SEE ALSO
atmsigd(8), qos(7), syslogd(8)
