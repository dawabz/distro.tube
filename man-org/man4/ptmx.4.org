#+TITLE: Manpages - ptmx.4
#+DESCRIPTION: Linux manpage for ptmx.4
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ptmx.4 is found in manpage for: [[../man4/pts.4][man4/pts.4]]