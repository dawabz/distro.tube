#+TITLE: TEE

* TEE
'tee' is like 'cat' but it writes to both STDOUT and to files.  Be careful with 'tee' if new to using it.  Unlike 'cat', 'tee filename' writes to a file instead of just printing it.  'tee filename' without any other arguments is going to empty the file.  'tee' is really meant to be used as the second command in a pipe.

#+begin_example
echo "Hello, World!" | tee newfile.txt
echo "Appending a line" | tee -a newfile.txt
#+end_example

#+begin_example
ls | tee newfile.txt
ls > newfile.txt && cat newfile.txt
ls | tee -a newfile.txt
#+end_example

#+begin_example
command vim /etc/pacman.d/arcolinux-mirrorlist
:w !sudo tee %
#+end_example

=NOTE:= The above works in 'vim' but will not work with 'neovim'.  For 'neovim' users, it's probably best to install the suda.vim plugin which allows you to read/write with sudo privileges.

#+begin_example
cat /proc/sys/kernel/sysrq
sudo echo 1 > /proc/sys/kernel/sysrq
echo 1 | sudo tee /proc/sys/kernel/sysrq
#+end_example
