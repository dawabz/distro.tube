#+TITLE: Arch Linux Install
#+DESCRIPTION: Knowledge Base - Arch Linux Install
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

* About this installation guide
These are DT's personal notes for installing Arch Linux inside a virtual machine on video.  Obviously, it does not cover every possible scenario for installing Arch Linux.  For a more in-depth guide, it is strongly recommended that you follow the official [[https://wiki.archlinux.org/title/installation_guide][Arch Wiki Installation Guide]].
* Getting Started
** Download the ISO
Verify the ISO:
+ gpg --keyserver-options auto-key-retrieve --verify archlinux-2020.08.01-x86_64.iso
+ gpg --keyserver keyserver.ubuntu.com --recv-keys 4AA4767BBC9C4B1D18AE28B77F2D434B9741E8AC
** Arch Wiki Installation Guide
You should have the official Arch Wiki Installation Guide in front of you.  The Installation Guide is not very long at all, so please read everything closely.  The Guide does have links to other pages for more in-depth discussions on certain topics, such as "boot loaders" for example.
** Install it in VirtualBox if first time.
It is strongly recommended that you install Arch Linux in a virtual machine if this is your first time installing it, and especially if this is your first time installing any Linux distribution from a command line installer.

Here is what I will do:
+ Set up the VM on camera to show them correct settings.
+ Make a 20G virtual disk, 18G for primary, 2G for swap.
* Partioning with fdisk
+ The Installation Guide recommends using 'fdisk' to partition your drives.
+ fdisk -l : lists out the partitions (/dev/sda and a loopback disk associated with live environment)
+ fdisk /dev/sda : enters into fdisk
+ "m" for help
+ "o" for DOS partition or "g" for GPT

=NOTE:= MBR only works on =>2TB disks and up to four primary partitions.
GPT can be used on much larger disks, the size limit depends on the OS and filesystem.
GPT can have up to 128 partitions (you'd have to be a complete nutter to do 128 partitions on a disk)

+ "n" for add new partition
+ "p" for primary partition
+ Partition number: "1" to create sda1
+ First sector defaults to 2048, hit ENTER.
+ Last sector defaults to entire disk, change it to: +550M
+ Last sector defaults to entire disk, change it to: +2G
+ Last sector defaults to entire disk, change it to: +14G (or hit ENTER for all remaining space)
+ "n" -> "p" -> partition 2 defaults, hit ENTER
+ "m" for help to change partition type, "t"
+ "1" -> "L" -> "?" (EFI System)
+ "2" -> "L" -> "82"
+ "3" -> "L" -> "?" (may already default to Linux filesystem)
+ "m" -> "a" -> "1" -> "w" (write table to disk)
* Make filesystem
+ mkfs.fat -F32 /dev/sda1
+ mkswap /dev/sda2
+ swapon /dev/sda2
+ mkfs.ext4 /dev/sda3
* Base Install
+ mount /dev/sda3 /mnt (mounts it to mnt on live image)
+ pacstrap /mnt base linux linux-firmware
+ genfstab -U /mnt >> /mnt/etc/fstab
* Chroot
+ arch-chroot /mnt (change into root directory of our new installation)
=NOTE:= Chroot is used for installation or system rescue.
Prompt will change, letting us know we are logged in as root on new system.
+ ln -sf /usr/share/zoneinfo/America/Chicago /etc/localtime
+ hwclock --systohc (sets the hardware clock)
+ nano /etc/locale.gen
+ nano /etc/locale.conf
  #+begin_example
  LANG=en_US.UTF-8
  LC_CTYPE=en_US.UTF-8
  #+end_example

+ locale-gen
+ sudo pacman -S nano (still chrooted so nano will be installed on new system)
+ nano /etc/hostname (archvbox)
+ nano /etc/hosts
  #+begin_example
  127.0.0.1	localhost
  ::1		localhost
  127.0.1.1	archvbox.localdomain	archvbox
  #+end_example
* Users and passwords
+ passwd (set root pass)
+ make normal user: useradd -m dt
+ passwd dt
+ usermod -aG wheel,audio,video,optical,storage dt
+ pacman -S sudo
+ EDITOR=nano visudo
* GRUB
+ pacman -S grub
+ grub-install /dev/sda
+ grub-mkconfig -o /boot/grub/grub.cfg
* Installing a few things before reboot
+ networkmanager (sudo systemctl enable --now NetworkManager)
+ xorg xorg-init
+ lightdm lightdm-gtk-greeter
+ A lightweight window manager for a GUI (awesome, fluxbox, etc.)
+ A terminal emulator (such as alacritty)
+ A text editor (such as vim/neovim)
+ A web browser (such as firefox)
+ A few other things I may want: git, nitrogen, picom
+ xf86-video-fbdev (graphics driver for the VM)
* Systemctl enable ...
+ Edit the lightdm config file
  /etc/lightdm/lightdm.conf
  [Seat:*]

  #+begin_example
  ...
  greeter-session=lightdm-yourgreeter-greeter
  ...
  #+end_example
+ systemctl enable lightdm
+ systemctl enable NetworkManager
* Reboot
+ exit the chroot
+ Unmount: umount /mnt
+ reboot (shutdown now to detach the ISO in VBOX)
* Yay AUR Helper
  #+begin_example
  git clone https://aur.archlinux.org/yay-git.git
  ls -la
  cd yay-git
  makepkg -si
  yay -S name-of-package (for installing a package)
  #+end_example
* Xinit
+ cp /etc/X11/xinit/xinitrc /home/dt/.xinitrc
+ sudo chown dt:dt .xinitrc
+ vim .xinitrc
+ In .bash_profile:
  #+begin_example
  [[ [[ $(fgconsole 2>/dev/null) == 1 ]] ]] && exec startx -- vt1
  #+end_example

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
