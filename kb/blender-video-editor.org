#+TITLE: Blender Video Editor


* Basic setup
** The file manager
The file manager defaults to your user's $HOME directory on Linux.  You can view files as large icons, or you can view them in either a horizontal or vertical list.

** Resolution and FPS
Blender defaults to 1920x1080 at 24 fps.  Most people recording 1080p video will probably want 60fps or 30fps.  Of course, you can record in any resolution you like, including 4K if you have a computer that can handle it.

=NOTE:= Many report that blender has issues with handling clips with varying frame rates.

** Output
Blender defaults to /tmp as the directory for output (rendered video).  You may want to choose a different location, such as ~/Videos on Linux.  Also, make sure the output file format is set to =FFmpeg Video= which should be the default, but there are other non-video options such as PNG and TIFF
.
* Preferences
Go into Edit > Preferences to change basic settings.

** Interface > Display > Resolution Scale
This allows you to increase/decrease the font sizes in Blender.  This is important because many people will find the default font size a bit too small.

** File Paths
You will need to set the default fonts directory.  This should be /usr/share/fonts on Linux.  You will also want to change the default render output directory, if you don't want it to be /tmp.  Also, tick on auto-save preferences in the lower hamburger menu so your changes in preferences are always saved.

* Editing

** Playback settings
+ Tick on =Audio Scrubbing= so you can hear audio while you scrub through the playhead.
+ Tick on =Playback Follow Current Frame= so the current frame is always on screen during playback.

** Audio tracks
In the Audio tool, which is to the right of the sequencer, you need to:
+ Tick on =mono=
+ Tick on =display wave form=
This has to be done on every clip that has audio.

** Fades

** Add text

** Picture-in-picture
Hit "A" on the keyboard to =Add= an effect.  We want to add Effect Strip > Transform.

** Color correction
*** Saturation and Multiply
+ Click on the video strip
+ Go to the tools area and choose =color=
+ You have two values: Saturation and Multiply
*** Add Modifiers
This gives you many more modifier options, including:
+ Color balance
+ Curves
+ Hue Correct
+ Brightness/Contrast
+ Mask
+ White Balance
+ Tone Map
* Rendering
