#+TITLE: Nano Vs Vim? Which Terminal Text Editor Should You Choose?
#+DESCRIPTION: DT Articles - Nano Vs Vim? Which Terminal Text Editor Should You Choose?
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

* Nano Vs Vim? Which Terminal Text Editor Should You Choose?
/By Derek Taylor at July 29, 2021/

* About this article
The following are the show notes from my video [[https://odysee.com/@DistroTube:2/nano-or-vim-which-terminal-text-editor:3][Nano or Vim? Which Terminal Text Editor Should You Use?]]  Since these are just show notes, it's not really written in a typical article-like narrative.

* An overview of these editors
** What is Nano?
GNU Nano, usually just called "nano," is an improvement on a earlier text editor named Pico, which was initially released in 1989.  GNU Nano was created in 1999 because the original Nano developer wanted a free software replacement for Pico, because Pico wasn't distributed under a free software license.  Nano is a simple WYSIWYG plain text editor.  It lacks most of the advanced features that more powerful editors include these days, such as window splitting, macros, auto-completion, viewing multiple files at once, vertical block editing, etc.
** What is Vim?
Vim is an acronym for Vi Improved.  Vi is a text editor that dates back to the 1970s and is almost always pre-installed on Unix-like OSes such as Linux.  Vim is essentially a better Vi.  Vim was originally released in 1991.  One of the most well-known things about Vi/Vim is that they use hjkl as motion keys, because Vi was created in a time where many computer terminals didn't have arrow keys.  hjkl served double duty as both their characters and as arrow keys.  The other well-known aspect of Vim is that no one knows how to exit Vim.  This is a joke, of course.  :wq writes your document and quits.  :q! will quit without saving a document.  And :w of course would write to the file, it saves your work.
* Vim has modes, Nano does not.
** Explain Vim's modes
The biggest difference between Vim and Nano is that Vim is modal, meaning it has modes.  For example, in Vim you have what is called "normal mode" for doing things like moving around the document, copy/paste, searching, etc...anything that's not you actually typing text into the document.  For actually typing text to insert into the document, you need to be in "insert mode".  You get into insert mode by typing "i".  Then you can type normally.  When you are done typing what you needed to insert, hit ESC to get back into normal mode.
** Nano's keybindings are more complicated due to being non-modal
Because you don't have modes, Nano's keybindings for things like moving forward/backward by word, or moving up/down by line, or searching...those keybindings are actually more complicated than Vim's keybindings.
** Searching
+ / vs C-W
+ n/N vs M-W
* Speed and Productivity
Even a brand new Vim user will probably notice that they get things done faster in Vim compared to Nano.
** Nano
+ C-d to delete char.
+ C-space move forward by word.
** Vim
+ Show visual mode and do some advanced edit.
+ Do a global substitution
+ Add line numbers to each line.
  :%s/^/\=printf('%-4d', line('.'))
+ Then reverse the order of the lines.
  :g/^/m0
* Learning Curve
** For basic use
Honestly, I think Nano and Vim are not that different as far as a learning curve.  If all you want to do is basic text editing, you can learn how to do basic text editing in either nano or vim rather quickly.  The only reason Vim is seen as having a learning curve is because it acts unlike any other editor that you've probably used.  Nano acts like other plain text editors that you've used...like Notepad.  But both are easy to learn the basics.  Nano gives you a cheat sheet at the bottom of the page.  Vim comes with a vimtutor.  Just open a terminal and type vimtutor, and go through that document.  It takes maybe 20-30 minutes to learn the basics of Vim.
** For advanced use
Now, if you want to learn the intermediate to advanced features of Vim, like macros and global substitution and things like that, you may have to spend a little more time learning these with Vim, but just a little more time.  These are not complicated things to learn.  And if you need these kinds of advanced features, then you have to learn Vim because these features don't exist in Nano.
* Which should you use?
** Nano
Nano is a great text editor for beginners who are new to terminal-based text editing, or for those who only use their text editor occasionally to make simple edits.  If you consider yourself a casual Linux user, then Nano might be a good choice for you.  Some Linux users will tell you that using nano is "uncool" and everyone should use Vim or Emacs.  While the I agree that learning Vim is worth it, if you change a few lines of text once in a blue moon, then nano is all you need for that.
** Vim
+ Vim is a necessary tool to be familiar with if you are a Linux sysadmin.
+ Good for programmers who write a lot of text and need Vim's powerful toolset to manipulate that text.
+ Vim can also be a great editor for writers, who may want the power of Vim, but not the distraction of a word processor, while writing their works.

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
