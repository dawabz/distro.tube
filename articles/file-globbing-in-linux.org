#+TITLE: File Globbing In Linux
#+DESCRIPTION: DT Articles - File Globbing In Linux
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

* File Globbing in Linux
/By Derek Taylor at February 26, 2020/

File globbing refers to “global” patterns that specify sets of filenames with wildcard characters. Globbing is the * and ? and some other pattern matchers you may be familiar with. An example would be doing something like the following in the Bash shell:

#+begin_example
cp *.txt /Documents/text/
#+end_example

The above command moves all files that end in “.txt” from the current directory to the directory “/Documents/text/". The * is a wildcard that stands for “any string of characters”. *.txt is a glob pattern. When the shell sees a glob, it will perform pathname expansion and replace the glob with matching filenames when it invokes the program.

* Globbing Syntax

| WILDCARD | DESCRIPTION                                         |
|----------+-----------------------------------------------------+
| *        | matches any number of any characters including none |
| ?        | matches any single character                        |
| [abc]    | matches one character given in the bracket          |
| [a-z]    | matches one character from the range given          |
| [!abc]   | matches one character not given in the bracket      |
| [!a-z]   | matches one character not from the range given      |

| Example    | Matches             |
|------------+---------------------+
| Bar*       | Bar, Bars, Barter   |
| ?at        | Bat, bat, Cat, cat  |
| [CB]at     | Cat or Bat          |
| File[0-9]  | File1, File2, etc.  |
| [!B]at     | bat, cat, Cat       |
| File[!4-9] | File1, File2, File3 |

| Example    | Does Not Match       |
|------------+----------------------|
| Bar*       | FooBar, Ba, ar       |
| ?at        | at                   |
| [CB]at     | cat or bat           |
| File[0-9]  | File, Files, File 10 |
| [!B]at     | Bat                  |
| File[!4-9] | File4, File5, Filexx |

* Globbing in SQL
SQL has the equivalent of * and ?. In SQL % equates to * and _ equates to ?. There is no equivalent to [...].

* Globs Are Not Regular Expressions
Though they rememble regex, glob patterns are not regular expressions. Unlike regular expressions, globbing is specifically for pattern matching filenames. Regex has much more syntax to it and is much more complicated. Also, the syntax that is shared with globbing is not the same thing in regex.

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
