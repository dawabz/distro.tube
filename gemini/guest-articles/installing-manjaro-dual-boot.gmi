```
                          __       _______ _______ _______ ___ _______ ___     _______ _______
 .-----.--.--.-----.-----|  |_    |   _   |   _   |       |   |   _   |   |   |   _   |   _   |
 |  _  |  |  |  -__|__ --|   _|   |.  1   |.  l   |.|   | |.  |.  1___|.  |   |.  1___|   1___|
 |___  |_____|_____|_____|____|   |.  _   |.  _   `-|.  |-|.  |.  |___|.  |___|.  __)_|____   |
 |_____|                          |:  |   |:  |   | |:  | |:  |:  1   |:  1   |:  1   |:  1   |
                                  |::.|:. |::.|:. | |::.| |::.|::.. . |::.. . |::.. . |::.. . |
                                  `--- ---`--- ---' `---' `---`-------`-------`-------`-------'
```

# Installing Manjaro in Dual Booting Environment
By Primož Ajdišek "Bigpod" at April 22, 2020

## MANJARO
Manjaro is arch based distribution targeted at users ranging from low intermediate all the way to experts. Compared to arch it has 3 branches of package repositories. First and least stable branch is called unstable and this branch is somewhat closest to arch model rolling release, all packages will first arive here. After days of testing in unstable branch they will get pushed to Testing branch where a bigger userbase will recieve them. After manjaro team gets enought of a confirmation that package is working as intended and stable it will get pushed to Stable brach which is its main branch and is branch most users use. One of the great benefits of manjaro are its preconfigured editions. XFCE, Gnome and KDE Plasma 5 are manjaros 3 main editions with XFCE being its flagship. They also support some editions maintained by community. Among these aptly named community editions you can find i3 edition, openbox edition, mate edition and many more. You can install Manjaro two ways first one being trought Calamares installer which is Manjaro's main way of installing or you can use advanced architect installer. Architect installed is an advanced TUI based installer with witch you can control almost every aspect of your manjaro install making it truly your own.

## INSTALLING
### QUICK NOTES
* For dual booting I recommend usage of UEFI based systems if possible. This is why I will focus only on EFI installations.
* I installed manjaro besides Windows but you can install it beside any operating system.
* This tutorial can be used for any distribution using calamares installer.

There are two ways of tackling this problem if you have already prepartitioned space for your manjaro install or if you dont we will tackle both.

### WITH PREPARTITIONED SPACE
So if you already prepartitioned your drives and made space for your Linux distro you can go trought your installation normaly until you hit the Partitioning page where you select MANUAL PARTITIONING where you can select your free space and press create button in lower right corner of installer. Now in window that opens in the mount point section we do /. Now comes the hard part we need to find the EFI partition for me this partition was 99MB in size created automaticly by windows installer. With FAT32 as its file system. When you find your EFI partition you can click edit button that sits besides create button we pressed earlier. You can also use edit button to identify which partition is EFI since it should have boot flag. When window opens for the right partition we type in Mount Point section /boot/efi and press OK with this we can do additional modifications if we want or just press next and follow installation procedure further.

### WITHOUT PREPARTITIONED SPACE
Now what if we dont have prepartitioned space we have 2 options we can do manual partitioning and manualy split a partition by selecting MANUAL PARTITIONING then selecting partition we would like to split. By pressing edit we open a windows where we can in size section define a smaller size. This will make the existing partition the size of what we defined and create new free space when we press ok we come back to MANUAL PARTITIONING where we can continue by instructions from the part of article about prepartitioned space.

Other way is using INSTALL ALONGSIDE which we can find on partitioning page of installer. When we select this option two bars appear in the bottom of the page. First bar shows how drive is currently partitioned. On this bar we can select which partition we want to split. When we select one second bar will change and split that partition down the middle with a verticle line with 2 arrows showing where one partition ends and other begins now you can move that verticle line to get both partitions however sized you want when you are happy you press next and follow the installation procedure further.

### AFTER INSTALL
So now installation is complete when we reboot your system will open into a boot loader called GRUB here you can with your arrow keys select which operating system you want in my case this was Manjaro and Windows. Select the one you want and press enter and your system will boot into the selected environment.
