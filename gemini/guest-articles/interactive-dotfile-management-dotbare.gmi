```
                          __       _______ _______ _______ ___ _______ ___     _______ _______
 .-----.--.--.-----.-----|  |_    |   _   |   _   |       |   |   _   |   |   |   _   |   _   |
 |  _  |  |  |  -__|__ --|   _|   |.  1   |.  l   |.|   | |.  |.  1___|.  |   |.  1___|   1___|
 |___  |_____|_____|_____|____|   |.  _   |.  _   `-|.  |-|.  |.  |___|.  |___|.  __)_|____   |
 |_____|                          |:  |   |:  |   | |:  | |:  |:  1   |:  1   |:  1   |:  1   |
                                  |::.|:. |::.|:. | |::.| |::.|::.. . |::.. . |::.. . |::.. . |
                                  `--- ---`--- ---' `---' `---`-------`-------`-------`-------'
```

# Interactive Dotfile Management With Dotbare
By Kevin Zhuang at May 25, 2020

When it comes to dotfile management, I find that the method of using a git bare repository is the easiest and most straight forward method to use. However, it comes with two limitations that bothered me quite a lot. It doesn't have any command line completion by default and it has the annoying checkout issue during migration to new systems.

dotbare is a command line utility that by default wraps around git bare repository and aims to provide an interactive experience with the help of fzf. Don't worry about migration if you are using a Symlink method/GNU stow, dotbare could easily be integrated with them. In this blog, I will go through how to easily set up and use dotbare.

## How it works
In a nutshell, dotbare is a wrapper around a .git repository and provides a series of scripts that process git information and populate into fzf. DT's video really helped me understand how the underlying git bare repository works.

## Set up
For first time usage of git bare method, run the following command and dotbare would setup everything for you.

```
# By default, dotbare will initialise a bare repository in $HOME/.cfg
# To change this location, put export DOTBARE_DIR=<Your location> in shellrc (e.g. .zshrc, .bashrc)
dotbare finit

# after initialisation, you can start staging/committing files and push it to remote repo.
dotbare fadd  # interactive stage files
dotbare commit -m <message>
dotbare push
```

For existing git bare user, simply check your existing git bare alias for reference and set up the following ENV variable in your shellrc (e.g. .zshrc, .bashrc) to inform dotbare. After setting up the ENV variable you could remove the alias or alias the original config to dotbare so that you keep your muscle memory.

```
# Below is an example alias, check yours for reference
alias config=/usr/bin/git --git-dir=$HOME/.cfg --work-tree=$HOME
export DOTBARE_DIR="$HOME/.cfg"
export DOTBARE_TREE="$HOME"
```

## Usage
Treat dotbare as normal git commands(e.g. dotbare commit dotbare reset HEAD~ etc). dotbare comes with a set of interactive commands with a prefix f, it supports most of the everyday git usage interactively.

```
dotbare fadd        # interactive stage files
dotbare fedit       # interactive select and edit dotfiles
dotbare flog        # interactive log viewer and editor
dotbare fcheckout   # interactive reset file changes to HEAD or checkout branch/commits.
dotbare fbackup     # backup dotfiles
dotbare freset      # interactive unstage files
dotbare finit       # initialise dotbare or migration to new system
dotbare fstash      # interactive stash management
dotbare funtrack    # interactive select files and untrack from your index
dotbare fstat       # toggle stage/unstage of files
```

## Migration made easy
When migrating to new systems, traditional git bare method has the annoying checkout issue that you will need to solve when checking out the git bare repository. dotbare made this migrating super easy.

```
dotbare finit -u [URL]
```

Simply run the command with your remote url and dotbare will automatically handle the checkout, backup and set everything up for you.

## Integration with Symlink setup/GNU stow setup
If you are currently using this type of method to mange dotfiles, but are interested in dotbare, you don't need to migrate anything, you simply need to set two environment variables to let dotbare aware where to query the git information.

```
# e.g. I have all my dotfiles stored in folder $HOME/.myworld and symlinks all of them to appropriate location.
# export DOTBARE_DIR="$HOME/.myworld/.git"
# export DOTBARE_TREE="$HOME/.myworld"
export DOTBARE_DIR=<Path to your .git directory>
export DOTBARE_TREE=<Path to directory which contains all your dotfiles>
```

## Conclusion
dotbare is definitely not for everyone, it requires bash on the system as well as an installation of fzf. This is only a quick start on dotbare, you could find out more information about usage, customization, demo and various installation method over on GitHub.

## Links
=> https://github.com/kazhala/dotbare Dotbare on GitHub
