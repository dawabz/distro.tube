```
     __ __       _______ _______ _____ _______ 
 .--|  |  |_    |       |   _   | _   |   _   |
 |  _  |   _|   |___|   |.  |   |.|   |___|   |
 |_____|____|    /  ___/|.  |   `-|.  |  /   / 
                |:  1  \|:  1   | |:  | |   |  
                |::.. . |::.. . | |::.| |   |  
                `-------`-------' `---' `---'  
                                               
```

# DistroTube Videos from 2017 (sorted in reverse order)

TITLE: Calculate Linux 17.12 XFCE - Great Way To End 2017
=> https://odysee.com/@DistroTube:2/calculate-linux-17-12-xfce-great-way-to:0

TITLE: Measuring Distro Popularity And Why Distrowatch's Page Hit Rankings Suck.
=> https://odysee.com/@DistroTube:2/measuring-distro-popularity-and-why:9

TITLE: My Manjaro KDE Journey, Day One
=> https://odysee.com/@DistroTube:2/my-manjaro-kde-journey-day-one:1

TITLE: Made my decision. The distro I'm installing for 2018 is....
=> https://odysee.com/@DistroTube:2/made-my-decision-the-distro-i-m:8

TITLE: How To Install Debian Unstable
=> https://odysee.com/@DistroTube:2/how-to-install-debian-unstable:3

TITLE: Top Five Linux Distros of 2017
=> https://odysee.com/@DistroTube:2/top-five-linux-distros-of-2017:7

TITLE: Archbang (systemd) Install & Review
=> https://odysee.com/@DistroTube:2/archbang-systemd-install-review:5

TITLE: Ulauncher Fast Application Launcher
=> https://odysee.com/@DistroTube:2/ulauncher-fast-application-launcher:2

TITLE: Endless OS 3.3.5 First Impression Install & Review
=> https://odysee.com/@DistroTube:2/endless-os-3-3-5-first-impression:e

TITLE: A Look at How I Setup Virtualbox For Testing Linux Distros
=> https://odysee.com/@DistroTube:2/a-look-at-how-i-setup-virtualbox-for:7

TITLE: SalentOS Neriton First Impression Install & Review
=> https://odysee.com/@DistroTube:2/salentos-neriton-first-impression:3

TITLE: Rofi - Application Launcher, Window Switcher and Run Command Utility
=> https://odysee.com/@DistroTube:2/rofi-application-launcher-window:e

TITLE: Puzzle Moppet - 3D Puzzle Game for Linux (and Mac and Windows)
=> https://odysee.com/@DistroTube:2/puzzle-moppet-3d-puzzle-game-for-linux:5

TITLE: A Quick Look at the Official Release of MX Linux 17
=> https://odysee.com/@DistroTube:2/a-quick-look-at-the-official-release-of:a

TITLE: Sabayon LXQt Daily Build Install & Review
=> https://odysee.com/@DistroTube:2/sabayon-lxqt-daily-build-install-review:d

TITLE: SwagArch GNU/Linux First Impression Install & Review
=> https://odysee.com/@DistroTube:2/swagarch-gnu-linux-first-impression:c

TITLE: Revisiting Solus. Why It Isn't Right For Me, Yet.
=> https://odysee.com/@DistroTube:2/revisiting-solus-why-it-isn-t-right-for:d

TITLE: CrunchBangPlusPlus (#!++) Install & Review
=> https://odysee.com/@DistroTube:2/crunchbangplusplus-install-review:a

TITLE: A Little SuperTuxKart With a Bit of Talk About Linux Gaming of the Past
=> https://odysee.com/@DistroTube:2/a-little-supertuxkart-with-a-bit-of-talk:1

TITLE: I Install unity7sl and Discuss the Pros of the Unity Desktop and Its Future.
=> https://odysee.com/@DistroTube:2/i-install-unity7sl-and-discuss-the-pros:8

TITLE: Bodhi Linux 4.4.0 AppPack Release Install & Review
=> https://odysee.com/@DistroTube:2/bodhi-linux-4-4-0-apppack-release:2

TITLE: The Openbox Window Manager - Install & Setup
=> https://odysee.com/@DistroTube:2/the-openbox-window-manager-install-setup:f

TITLE: Peppermint OS 8 Install & Review
=> https://odysee.com/@DistroTube:2/peppermint-os-8-install-review:9

TITLE: ArchMerge First Impression Install & Review
=> https://odysee.com/@DistroTube:2/archmerge-first-impression-install:1

TITLE: ROSA R10 Fresh KDE First Impression Install & Review
=> https://odysee.com/@DistroTube:2/rosa-r10-fresh-kde-first-impression:a

TITLE: Gradio - Searching and Listening to  Internet Radio Stations
=> https://odysee.com/@DistroTube:2/gradio-searching-and-listening-to:9

TITLE: Distro-Hopping.  Which Distro Should I Go With?  Help Me Decide!
=> https://odysee.com/@DistroTube:2/distro-hopping-which-distro-should-i-go:f

TITLE: Solus First Impression Install & Review
=> https://odysee.com/@DistroTube:2/solus-first-impression-install-review:0

TITLE: Stacer - System Monitor, Optimizer and Cleaner
=> https://odysee.com/@DistroTube:2/stacer-system-monitor-optimizer-and:2

TITLE: AntiX 17 First Impression Install & Review
=> https://odysee.com/@DistroTube:2/antix-17-first-impression-install-review:5

TITLE: Deepin 15.5 First Impression Install & Review
=> https://odysee.com/@DistroTube:2/deepin-15-5-first-impression-install:6

TITLE: Stress Terminal UI (s-tui) - Stress-Testing System Monitor
=> https://odysee.com/@DistroTube:2/stress-terminal-ui-s-tui-stress-testing:c

TITLE: Nmon - Terminal-Based System Performance Monitor
=> https://odysee.com/@DistroTube:2/nmon-terminal-based-system-performance:8

TITLE: Xebian First Impression Install & Review
=> https://odysee.com/@DistroTube:2/xebian-first-impression-install-review:8

TITLE: Linux Mint 18.3 Sylvia Install & Review
=> https://odysee.com/@DistroTube:2/linux-mint-18-3-sylvia-install-review:2

TITLE: PCLinuxOS First Impression Install & Review
=> https://odysee.com/@DistroTube:2/pclinuxos-first-impression-install:d

TITLE: BunsenLabs Linux Install & Review
=> https://odysee.com/@DistroTube:2/bunsenlabs-linux-install-review:b

TITLE: LXLE 16.04.3 Install & Review
=> https://odysee.com/@DistroTube:2/lxle-16-04-3-install-review:8

TITLE: Glances - Terminal-Based System Monitoring Tool
=> https://odysee.com/@DistroTube:2/glances-terminal-based-system-monitoring:3

TITLE: Chakra Linux First Impression Install & Review
=> https://odysee.com/@DistroTube:2/chakra-linux-first-impression-install:5

TITLE: Htop - Terminal-Based Interactive Process Viewing Program
=> https://odysee.com/@DistroTube:2/htop-terminal-based-interactive-process:f

TITLE: Calculate Linux First Impression Install & Review
=> https://odysee.com/@DistroTube:2/calculate-linux-first-impression-install:9

TITLE: Korora 26 First Impression Install & Review
=> https://odysee.com/@DistroTube:2/korora-26-first-impression-install:1

TITLE: Ranger - A Minimal Terminal-Based File Manager
=> https://odysee.com/@DistroTube:2/ranger-a-minimal-terminal-based-file:2

TITLE: Fedora 27 Workstation Installation and Review
=> https://odysee.com/@DistroTube:2/fedora-27-workstation-installation-and:b

TITLE: MX Linux 17 Beta 1 First Impression Install & Review
=> https://odysee.com/@DistroTube:2/mx-linux-17-beta-1-first-impression:0

TITLE: Redcore Linux First Impression Install & Review
=> https://odysee.com/@DistroTube:2/redcore-linux-first-impression-install:0

TITLE: Canto - Terminal-Based RSS Feed Reader
=> https://odysee.com/@DistroTube:2/canto-terminal-based-rss-feed-reader:4

TITLE: TruesOS (formerly PC-BSD) First Impression Install & Review
=> https://odysee.com/@DistroTube:2/truesos-formerly-pc-bsd-first-impression:3

TITLE: KaOS First Impression Install & Review
=> https://odysee.com/@DistroTube:2/kaos-first-impression-install-review:9

TITLE: Cmus - Terminal-Based Music Player - Lightweight and Super-Fast
=> https://odysee.com/@DistroTube:2/cmus-terminal-based-music-player:8

TITLE: Terminal Commands Lesson 04 - Editing Text Files - echo, cat, nano, vi
=> https://odysee.com/@DistroTube:2/terminal-commands-lesson-04-editing-text:c

TITLE: Terminal Commands Lesson 03 - which, whereis, locate, find
=> https://odysee.com/@DistroTube:2/terminal-commands-lesson-03-which:2

TITLE: Lynx - Text-based Web Browser - Surf the Web From a Terminal
=> https://odysee.com/@DistroTube:2/lynx-text-based-web-browser-surf-the-web:3

TITLE: Tails Live Operating System - Privacy For Anyone Anywhere
=> https://odysee.com/@DistroTube:2/tails-live-operating-system-privacy-for:5

TITLE: Irssi - Terminal IRC Client - Who Needs a GUI to Chat!
=> https://odysee.com/@DistroTube:2/irssi-terminal-irc-client-who-needs-a:d

TITLE: ClamAV - Anti-Virus for Linux - Is It Necessary?
=> https://odysee.com/@DistroTube:2/clamav-anti-virus-for-linux-is-it:2

TITLE: Calcurse - Organizer and Scheduling App
=> https://odysee.com/@DistroTube:2/calcurse-organizer-and-scheduling-app:9

TITLE: Ubuntu Studio 17.10 Artful Aardvark - Install and Review
=> https://odysee.com/@DistroTube:2/ubuntu-studio-17-10-artful-aardvark:e

TITLE: Ubuntu Budgie 17.10 Artful Aarvark - Install and Review
=> https://odysee.com/@DistroTube:2/ubuntu-budgie-17-10-artful-aarvark:f

TITLE: Ubuntu MATE 17.10 Artful Aarvark - Install and Review
=> https://odysee.com/@DistroTube:2/ubuntu-mate-17-10-artful-aarvark-install:a

TITLE: Lubuntu 17.10 Artful Aarvark - Install and Review
=> https://odysee.com/@DistroTube:2/lubuntu-17-10-artful-aarvark-install-and:5

TITLE: Xubuntu 17.10 Artful Aarvark - Install and Review
=> https://odysee.com/@DistroTube:2/xubuntu-17-10-artful-aarvark-install-and:4

TITLE: Kubuntu 17.10 Artful Aarvark - Install and Review
=> https://odysee.com/@DistroTube:2/kubuntu-17-10-artful-aarvark-install-and:4

TITLE: Feren OS First Impression Install & Review
=> https://odysee.com/@DistroTube:2/feren-os-first-impression-install-review:a

TITLE: OpenSUSE Tumbleweed Install and Review KDE edition
=> https://odysee.com/@DistroTube:2/opensuse-tumbleweed-install-and-review:2

TITLE: Qtile - Tiling Window Manager Written in Python
=> https://odysee.com/@DistroTube:2/qtile-tiling-window-manager-written-in:c

TITLE: ArchLabs Minimo First Impression Install & Review
=> https://odysee.com/@DistroTube:2/archlabs-minimo-first-impression-install:a

TITLE: Sabayon Minimal Edition - Most Painful Linux Install Ever?
=> https://odysee.com/@DistroTube:2/sabayon-minimal-edition-most-painful:5

TITLE: Siduction LXQt First Impression Install & Review
=> https://odysee.com/@DistroTube:2/siduction-lxqt-first-impression-install:8

TITLE: Categorizing Linux Distros - Which One Is Best For Beginners?
=> https://odysee.com/@DistroTube:2/categorizing-linux-distros-which-one-is:0

TITLE: Antergos Cinnamon First Impression Install & Review
=> https://odysee.com/@DistroTube:2/antergos-cinnamon-first-impression:6

TITLE: Welcome to DistroTube
=> https://odysee.com/@DistroTube:2/welcome-to-distrotube:0

TITLE: Terminal Commands Lesson 02 - touch, mkdir, mv, cp, rm, rmdir
=> https://odysee.com/@DistroTube:2/terminal-commands-lesson-02-touch-mkdir:7

TITLE: Terminal Commands Lesson 01 - pwd, cd, ls, man, --help
=> https://odysee.com/@DistroTube:2/terminal-commands-lesson-01-pwd-cd-ls:7

TITLE: Manjaro Linux KDE First Impression Install & Review
=> https://odysee.com/@DistroTube:2/manjaro-linux-kde-first-impression:2

## Video Categories

=> ../videos/2017-videos.gmi Videos from 2017
=> ../videos/2018-videos.gmi Videos from 2018
=> ../videos/2019-videos.gmi Videos from 2019
=> ../videos/2020-videos.gmi Videos from 2020
=> ../videos/2021-videos.gmi Videos from 2021
