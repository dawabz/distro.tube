```
     __ __       _______ _______ _____ _______ 
 .--|  |  |_    |       |   _   | _   |   _   |
 |  _  |   _|   |___|   |.  |   |.|   |.  |   |
 |_____|____|    /  ___/|.  |   `-|.  |.  _   |
                |:  1  \|:  1   | |:  |:  1   |
                |::.. . |::.. . | |::.|::.. . |
                `-------`-------' `---`-------'
                                               
```

# DistroTube Videos from 2018 (sorted in reverse order)

TITLE: AntiX 17.3 Installation And First Look
=> https://odysee.com/@DistroTube:2/antix-17-3-installation-and-first-look:1

TITLE: My 2019 Window Manager Will Be...
=> https://odysee.com/@DistroTube:2/my-2019-window-manager-will-be:9

TITLE: Taking Into Account, Ep. 22 - Lubuntu 32-bit, The Fake Internet, FSF, Distros in 2018, Polo
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-22-lubuntu-32-bit:4

TITLE: vifm - The Terminal File Manager For The Vim-Centric User
=> https://odysee.com/@DistroTube:2/vifm-the-terminal-file-manager-for-the:0

TITLE: Christmas Eve Live - Mabox Linux, Fatdog64, Season's Givings, Q&A, etc.
=> https://odysee.com/@DistroTube:2/christmas-eve-live-mabox-linux-fatdog64:5

TITLE: MX Linux 18 "Continuum" - Taking It For A Quick Spin
=> https://odysee.com/@DistroTube:2/mx-linux-18-continuum-taking-it-for-a:b

TITLE: ArchLabs 2018.12.17 (bspwm) - Installation and First Look
=> https://odysee.com/@DistroTube:2/archlabs-2018-12-17-bspwm-installation:4

TITLE: Twitter Impersonation - I Need Your Help Fighting Impersonator And Twitter
=> https://odysee.com/@DistroTube:2/twitter-impersonation-i-need-your-help:6

TITLE: Taking Into Account, Ep. 21 - Debian SJWs, Ubuntu on Dell, FOSS photography, Windows 0-day, Xmonad
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-21-debian-sjws:9

TITLE: NCurses Pulse Audio Mixer (ncpamixer)
=> https://odysee.com/@DistroTube:2/ncurses-pulse-audio-mixer-ncpamixer:b

TITLE: Free And Open Chat Sunday (December 16, 2018)
=> https://odysee.com/@DistroTube:2/free-and-open-chat-sunday-december-16:9

TITLE: Taking Into Account, Ep. 20 - Windows Privacy, IRS Migration, Adobe on Linux, MS Linux
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-20-windows:7

TITLE: Haiku OS - What Is It?
=> https://odysee.com/@DistroTube:2/haiku-os-what-is-it:5

TITLE: The Vim Tutorial - Part One - Basic Commands
=> https://odysee.com/@DistroTube:2/the-vim-tutorial-part-one-basic-commands:5

TITLE: Want To Kick The Habit?  Quit WindowsToday!
=> https://odysee.com/@DistroTube:2/want-to-kick-the-habit-quit-windowstoday:a

TITLE: Taking Into Account, Ep. 19 - A.I., F-Bombs, Hugs, Necuno, Quora, Slate
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-19-a-i-f-bombs:1

TITLE: Free And Open Chat Sunday (December 2, 2018)
=> https://odysee.com/@DistroTube:2/free-and-open-chat-sunday-december-2:6

TITLE: Taking Into Account, Ep. 18 - Linux in 2019, Malware, Windows 10, YouTube, LibreHunt
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-18-linux-in-2019:9

TITLE: Chat With Patrons (November 25, 2018)
=> https://odysee.com/@DistroTube:2/chat-with-patrons-november-25-2018:2

TITLE: DT LIVE - Fedora Jam - Could It Be My Next Distro?
=> https://odysee.com/@DistroTube:2/dt-live-fedora-jam-could-it-be-my-next:0

TITLE: Taking Into Account, Ep. 17 - Ubuntu, Unikernels, Patents, Unity Shell, Thanksgiving
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-17-ubuntu:f

TITLE: Why I'm Thankful This Holiday Season
=> https://odysee.com/@DistroTube:2/why-i-m-thankful-this-holiday-season:d

TITLE: Installing Void Linux And Taking A Quick Look Around
=> https://odysee.com/@DistroTube:2/installing-void-linux-and-taking-a-quick:1

TITLE: Taking Into Account, Ep. 16 - LIVE - Shuttleworth, Linux salaries, Malware, Ubuntu
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-16-live:a

TITLE: Lubuntu 18.10 "Cosmic Cuttlefish" Installation and First Look
=> https://odysee.com/@DistroTube:2/lubuntu-18-10-cosmic-cuttlefish:e

TITLE: Open Broadcaster Software (OBS) In Linux
=> https://odysee.com/@DistroTube:2/open-broadcaster-software-obs-in-linux:1

TITLE: Bye, Bye Blue Yeti.  Hello, Blue Baby Bottle SL!
=> https://odysee.com/@DistroTube:2/bye-bye-blue-yeti-hello-blue-baby-bottle:7

TITLE: Why Use Mac When Linux Exists?
=> https://odysee.com/@DistroTube:2/why-use-mac-when-linux-exists:5

TITLE: Pardus 17.4 Deepin Installation and First Look
=> https://odysee.com/@DistroTube:2/pardus-17-4-deepin-installation-and:f

TITLE: Taking Into Account, Ep. 15 - LIVE - Linux on Mac, Virtualbox, Facebook, Arch
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-15-live-linux-on:a

TITLE: Inside The Making Of DistroTube
=> https://odysee.com/@DistroTube:2/inside-the-making-of-distrotube:0

TITLE: Taking Into Account, Ep 14  - IBM, Red Hat, Ikey Doherty, Thelio, Steam
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-14-ibm-red-hat:9"

TITLE: Free And Open Chat Sunday (October 28, 2018)
=> https://odysee.com/@DistroTube:2/free-and-open-chat-sunday-october-28:f

TITLE: Taking Into Account, Ep. 13 - Linus is back, Snaps, Ubuntu stats, Microsoft patents, Arch Linux
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-13-linus-is-back:3

TITLE: Ubuntu Budgie 18.10 Replaces Mint 19 On My ThinkPad
=> https://odysee.com/@DistroTube:2/ubuntu-budgie-18-10-replaces-mint-19-on:e

TITLE: Ubuntu Mate 18.10 "Cosmic Cuttlefish" Installation and First Look
=> https://odysee.com/@DistroTube:2/ubuntu-mate-18-10-cosmic-cuttlefish:d

TITLE: Ubuntu 18.10 "Cosmic Cuttlefish" Installation and First Look
=> https://odysee.com/@DistroTube:2/ubuntu-18-10-cosmic-cuttlefish:b

TITLE: Xubuntu 18.10 "Cosmic Cuttlefish" Installation and First Look
=> https://odysee.com/@DistroTube:2/xubuntu-18-10-cosmic-cuttlefish:0

TITLE: Kubuntu 18.10 "Cosmic Cuttlefish" Installation and First Look
=> https://odysee.com/@DistroTube:2/kubuntu-18-10-cosmic-cuttlefish:0

TITLE: Emmabuntüs 9-1.03 Installation and First Look
=> https://odysee.com/@DistroTube:2/emmabunt-s-9-1-03-installation-and-first:2

TITLE: Taking Into Account, Ep. 12 - Apple Repairs, Microsoft Patents, GNOME Menus, Flatpak Security
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-12-apple-repairs:d

TITLE: Mageia 6.1 KDE Installation and First Look
=> https://odysee.com/@DistroTube:2/mageia-6-1-kde-installation-and-first:1

TITLE: Artix LXQt with Runit - Installation and First Look
=> https://odysee.com/@DistroTube:2/artix-lxqt-with-runit-installation-and:4

TITLE: Taking Into Account, Ep. 11 - MS loves Linux, Ubuntu kernel patching, System76 Thelio, Steam survey
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-11-ms-loves-linux:6

TITLE: Taking Into Account, Ep. 10 - CoC Myths, Chrome Privacy, Firefox Monitor, Mint, Fedora, Vivaldi
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-10-coc-myths:0

TITLE: The Elgato Cam Link - Turn Your DSLR Camera Into A Webcam
=> https://odysee.com/@DistroTube:2/the-elgato-cam-link-turn-your-dslr:3

TITLE: Free And Open Chat Sunday (September 23, 2018)
=> https://odysee.com/@DistroTube:2/free-and-open-chat-sunday-september-23:d

TITLE: Elementary OS Is The Distro We Should Be Showing Potential New Linux Users
=> https://odysee.com/@DistroTube:2/elementary-os-is-the-distro-we-should-be:0

TITLE: Taking Into Account, Ep 9 - Ubuntu Hyper-V, Open Source Contributors, ChromeOS, Windows 7, Blizzard
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-9-ubuntu-hyper-v:9

TITLE: Surfraw Lets You Search The Web...From The Terminal
=> https://odysee.com/@DistroTube:2/surfraw-lets-you-search-the-web-from-the:f

TITLE: How Did You React To The Code Of Conduct News? Proud Of Your Actions?
=> https://odysee.com/@DistroTube:2/how-did-you-react-to-the-code-of-conduct:0

TITLE: Linus Leaves Linux, A New Code Of Conduct and Community In Conflict
=> https://odysee.com/@DistroTube:2/linus-leaves-linux-a-new-code-of-conduct:1

TITLE: SSH, SCP, SFTP and FileZilla
=> https://odysee.com/@DistroTube:2/ssh-scp-sftp-and-filezilla:4

TITLE: Parrot Home 4.2.2 Installation and First Look
=> https://odysee.com/@DistroTube:2/parrot-home-4-2-2-installation-and-first:9

TITLE: Taking Into Account, Ep. 8 - Anticompetitive Microsoft, Social Spam, Linux Gaming, New Releases
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-8-anticompetitive:0

TITLE: Linux Mint Debian Edition 3 "Cindy" Installation and First Look
=> https://odysee.com/@DistroTube:2/linux-mint-debian-edition-3-cindy:4

TITLE: My First Time Looking At OpenIndiana
=> https://odysee.com/@DistroTube:2/my-first-time-looking-at-openindiana:2

TITLE: Chat With Patrons (September 9, 2018)
=> https://odysee.com/@DistroTube:2/chat-with-patrons-september-9-2018:f

TITLE: YouTube and Patreon - Mistakes Made and Lessons Learned
=> https://odysee.com/@DistroTube:2/youtube-and-patreon-mistakes-made-and:2

TITLE: Taking Into Account, Ep. 7 - Linus on Intel, NSA's Speck, NVIDIA RTX, Zero Phone, Releases, Peertube
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-7-linus-on-intel:3

TITLE: Top Five Reasons To Run Ubuntu
=> https://odysee.com/@DistroTube:2/top-five-reasons-to-run-ubuntu:7

TITLE: Nitrux 1.0.15 Installation and First Look
=> https://odysee.com/@DistroTube:2/nitrux-1-0-15-installation-and-first:6

TITLE: Triple Distro Extravanganza - Hackman Linux, Trinity PCLOS, TempleOS
=> https://odysee.com/@DistroTube:2/triple-distro-extravanganza-hackman:7

TITLE: Taking Into Account, Ep. 6 - Commons Clause, Microsoft Profits, Chromebooks, Bash Aliases, Joplin
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-6-commons-clause:4

TITLE: Free And Open Chat Tuesday
=> https://odysee.com/@DistroTube:2/free-and-open-chat-tuesday:4

TITLE: Panasonic Lumix G7 Plus Accessories
=> https://odysee.com/@DistroTube:2/panasonic-lumix-g7-plus-accessories:2

TITLE: Dtrx - An Easier Way To Extract Compressed Files
=> https://odysee.com/@DistroTube:2/dtrx-an-easier-way-to-extract-compressed:d

TITLE: Slackware Package Management With Serge
=> https://odysee.com/@DistroTube:2/slackware-package-management-with-serge:8

TITLE: Taking Into Account. Ep. 5 - Valve, Gimp, GNOME, Summer Camp, App Store Tax
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-5-valve-gimp:f

TITLE: Bodhi Linux 5.0.0 Released - Let's Take A Look
=> https://odysee.com/@DistroTube:2/bodhi-linux-5-0-0-released-let-s-take-a:5

TITLE: Why Linux Failed On The Desktop
=> https://odysee.com/@DistroTube:2/why-linux-failed-on-the-desktop:1

TITLE: Free And Open Chat Sunday
=> https://odysee.com/@DistroTube:2/free-and-open-chat-sunday:7

TITLE: BlankOn Linux XI Installation and First Look
=> https://odysee.com/@DistroTube:2/blankon-linux-xi-installation-and-first:9

TITLE: Fake Hacking Tools -  Learn How Hollywood Does It
=> https://odysee.com/@DistroTube:2/fake-hacking-tools-learn-how-hollywood:f

TITLE: Taking Into Account, Ep. 4 - AMD, Intel, Debian, Dropbox, Linux Gaming
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-4-amd-intel:4

TITLE: Steam On Linux - Insurgency
=> https://odysee.com/@DistroTube:2/steam-on-linux-insurgency:7

TITLE: My First Rice - Giving My Qtile Desktop A New Look
=> https://odysee.com/@DistroTube:2/my-first-rice-giving-my-qtile-desktop-a:0

TITLE: Like A Phoenix Rising From The Ashes - Korora 28 Xfce
=> https://odysee.com/@DistroTube:2/like-a-phoenix-rising-from-the-ashes:b

TITLE: Taking Into Account, Ep. 3 - LibreOffice, Winepak, Desktop Linux, i3, Snapchat
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-3-libreoffice:b

TITLE: Bliss, An Open Source Android-Based OS That Runs On A PC
=> https://odysee.com/@DistroTube:2/bliss-an-open-source-android-based-os:e

TITLE: A Closer Look at Mastodon, The Twitter Killer!
=> https://odysee.com/@DistroTube:2/a-closer-look-at-mastodon-the-twitter:3

TITLE: A Quick Look At Zoonity OS Britannia - DT LIVE
=> https://odysee.com/@DistroTube:2/a-quick-look-at-zoonity-os-britannia-dt:8

TITLE: Taking Into Account, Ep. 2 - Lubuntu, Windows DaaS, Firefox, Steam
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-2-lubuntu-windows:e

TITLE: Presentarms Custom PCLinuxOS With Openbox
=> https://odysee.com/@DistroTube:2/presentarms-custom-pclinuxos-with:e

TITLE: Slackware Current With Help From Serge
=> https://odysee.com/@DistroTube:2/slackware-current-with-help-from-serge:2

TITLE: Playing Overload on Linux - DT LIVE
=> https://odysee.com/@DistroTube:2/playing-overload-on-linux-dt-live:5

TITLE: Taking Into Account, Ep. 1 - A New Show, Slackware, Deepin Store, Windows Winning, Ubuntu 18.04.1
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-1-a-new-show:6

TITLE: Live Q&A Plus Maybe Reaching 10K
=> https://odysee.com/@DistroTube:2/live-q-a-plus-maybe-reaching-10k:f

TITLE: Top Five Reasons To Run Arch Linux
=> https://odysee.com/@DistroTube:2/top-five-reasons-to-run-arch-linux:b

TITLE: A Quick Look At CloverOS GNU/Linux
=> https://odysee.com/@DistroTube:2/a-quick-look-at-cloveros-gnu-linux:f

TITLE: Playing Some Team Fortress 2
=> https://odysee.com/@DistroTube:2/playing-some-team-fortress-2:9

TITLE: Deepin To Remove Web Analytics From App Store Due To Spyware Claims
=> https://odysee.com/@DistroTube:2/deepin-to-remove-web-analytics-from-app:0

TITLE: Obscure Window Manager Project - IceWM
=> https://odysee.com/@DistroTube:2/obscure-window-manager-project-icewm:3

TITLE: An Initial Look at ArcoLinuxB Bspwm Edition
=> https://odysee.com/@DistroTube:2/an-initial-look-at-arcolinuxb-bspwm:7

TITLE: Revisiting Shark Linux With Build 2018-07-06
=> https://odysee.com/@DistroTube:2/revisiting-shark-linux-with-build-2018:9

TITLE: My Favorite Linux Distro and Window Manager
=> https://odysee.com/@DistroTube:2/my-favorite-linux-distro-and-window:c

TITLE: Linux Fails on the Desktop, Plus Arch Malware and the Browsh Browser
=> https://odysee.com/@DistroTube:2/linux-fails-on-the-desktop-plus-arch:2

TITLE: Namib GNU/Linux 1806 GNOME
=> https://odysee.com/@DistroTube:2/namib-gnu-linux-1806-gnome:b

TITLE: Might Have To Give Up On GhostBSD, Plus Some New Equipment
=> https://odysee.com/@DistroTube:2/might-have-to-give-up-on-ghostbsd-plus:7

TITLE: And My Next OS is...
=> https://odysee.com/@DistroTube:2/and-my-next-os-is:a

TITLE: July 4th Linux Distro Extravaganza - Bodhi, Elementary, and Pinguy
=> https://odysee.com/@DistroTube:2/july-4th-linux-distro-extravaganza-bodhi:6

TITLE: Free And Open Chat Tuesday - DT LIVE
=> https://odysee.com/@DistroTube:2/free-and-open-chat-tuesday-dt-live:4

TITLE: Tiny Core Linux - Sometimes Size Does Matter
=> https://odysee.com/@DistroTube:2/tiny-core-linux-sometimes-size-does:f

TITLE: Redcore Linux 1806 Installation and First Look
=> https://odysee.com/@DistroTube:2/redcore-linux-1806-installation-and:5

TITLE: What OS Should I Run?  Vote and Decide!
=> https://odysee.com/@DistroTube:2/what-os-should-i-run-vote-and-decide:9

TITLE: Linux Mint 19 "Cinnamon" Installation and First Look
=> https://odysee.com/@DistroTube:2/linux-mint-19-cinnamon-installation-and:8

TITLE: The Github Gentoo Organization Hacked!
=> https://odysee.com/@DistroTube:2/the-github-gentoo-organization-hacked:0

TITLE: Interpreting Ubuntu's Desktop Metrics  - DT LIVE
=> https://odysee.com/@DistroTube:2/interpreting-ubuntu-s-desktop-metrics-dt:6

TITLE: KaOS 2018.06 Installation and First Look  - DT LIVE
=> https://odysee.com/@DistroTube:2/kaos-2018-06-installation-and-first-look:6

TITLE: Obscure WIndow Manager Project - i3 Gaps - DT LIVE
=> https://odysee.com/@DistroTube:2/obscure-window-manager-project-i3-gaps:7

TITLE: Peppermint OS 9 Installation and First Look
=> https://odysee.com/@DistroTube:2/peppermint-os-9-installation-and-first:5

TITLE: Laptop With Intel i7, 4GB of RAM and a 256GB SSD - Under $200
=> https://odysee.com/@DistroTube:2/laptop-with-intel-i7-4gb-of-ram-and-a:f

TITLE: How To Trash Your Linux Machine - DT LIVE
=> https://odysee.com/@DistroTube:2/how-to-trash-your-linux-machine-dt-live:0

TITLE: Open Chat Monday - DT LIVE
=> https://odysee.com/@DistroTube:2/open-chat-monday-dt-live:2

TITLE: Installing Antergos Openbox On My Laptop
=> https://odysee.com/@DistroTube:2/installing-antergos-openbox-on-my-laptop:4

TITLE: Deepin 15.6 Installation and First Look
=> https://odysee.com/@DistroTube:2/deepin-15-6-installation-and-first-look:f

TITLE: Let's Chat About Our Favorite Distros - DT LIVE
=> https://odysee.com/@DistroTube:2/let-s-chat-about-our-favorite-distros-dt:a

TITLE: Resurrecting an Old Laptop with Linux
=> https://odysee.com/@DistroTube:2/resurrecting-an-old-laptop-with-linux:4

TITLE: Playing Ballistic Overkill, Counter Strike and the Trombone - DT LIVE
=> https://odysee.com/@DistroTube:2/playing-ballistic-overkill-counter:6

TITLE: Microsoft Packages Program for Debian Which Could Wreck Your System
=> https://odysee.com/@DistroTube:2/microsoft-packages-program-for-debian:0

TITLE: Leave the GUI Behind With These Basic Terminal Commands - DT LIVE
=> https://odysee.com/@DistroTube:2/leave-the-gui-behind-with-these-basic:c

TITLE: GeckoLinux 150 Rolling Cinnamon Installation and First Look - DT LIVE
=> https://odysee.com/@DistroTube:2/geckolinux-150-rolling-cinnamon:6

TITLE: The Command Line Does It Better Than The GUI
=> https://odysee.com/@DistroTube:2/the-command-line-does-it-better-than-the:b

TITLE: Installing and Configuring Openbox in Arch Linux
=> https://odysee.com/@DistroTube:2/installing-and-configuring-openbox-in:a

TITLE: VSIDO Installation and First Look
=> https://odysee.com/@DistroTube:2/vsido-installation-and-first-look:7

TITLE: SliTaz GNU/Linux First Look - DT LIVE
=> https://odysee.com/@DistroTube:2/slitaz-gnu-linux-first-look-dt-live:7

TITLE: Playing a Little Team Fortress 2 - DT LIVE
=> https://odysee.com/@DistroTube:2/playing-a-little-team-fortress-2-dt-live:1

TITLE: Do Linux Fanboys Hate Large Corporations? - DT LIVE
=> https://odysee.com/@DistroTube:2/do-linux-fanboys-hate-large-corporations:f

TITLE: Microsoft Considering Buying GitHub, So I'm Moving Over to GitLab
=> https://odysee.com/@DistroTube:2/microsoft-considering-buying-github-so-i:b

TITLE: Add Bling to Your Terminal With Neofetch and Powerline Shell
=> https://odysee.com/@DistroTube:2/add-bling-to-your-terminal-with-neofetch:a

TITLE: Linux Lite 4.0 Installation and First Look
=> https://odysee.com/@DistroTube:2/linux-lite-4-0-installation-and-first:8

TITLE: Obscure Window Manager Project - Herbstluftwm
=> https://odysee.com/@DistroTube:2/obscure-window-manager-project:7

TITLE: 0 A.D. Alpha 23 - D.T. Versus the A.I. - DT LIVE
=> https://odysee.com/@DistroTube:2/0-a-d-alpha-23-d-t-versus-the-a-i-dt:a

TITLE: Parrot Security and Parrot Studio 4.0.1 First Look
=> https://odysee.com/@DistroTube:2/parrot-security-and-parrot-studio-4-0-1:f

TITLE: Checking Out Ben's Custom Manjaro Spin and Presentarms PCLinuxOS Trinity - DT LIVE
=> https://odysee.com/@DistroTube:2/checking-out-ben-s-custom-manjaro-spin:b

TITLE: OpenSUSE Leap 15 Installation and First Look
=> https://odysee.com/@DistroTube:2/opensuse-leap-15-installation-and-first:0

TITLE: Hannah Montana Linux Installation and First Look
=> https://odysee.com/@DistroTube:2/hannah-montana-linux-installation-and:5

TITLE: Channel Growth, Merchandise and Future Plans
=> https://odysee.com/@DistroTube:2/channel-growth-merchandise-and-future:2

TITLE: Virtual Machine Updates and Late Night Chat - DT LIVE
=> https://odysee.com/@DistroTube:2/virtual-machine-updates-and-late-night:7

TITLE: Archman 18.05 Deepin Installation and First Look
=> https://odysee.com/@DistroTube:2/archman-18-05-deepin-installation-and:c

TITLE: Voyager Live 18.04 Installation and First Look
=> https://odysee.com/@DistroTube:2/voyager-live-18-04-installation-and:2

TITLE: Playing Some Serious Same BFE - DT LIVE
=> https://odysee.com/@DistroTube:2/playing-some-serious-sam-3-bfe-dt-live:c

TITLE: TAILS, Tor and the Dark Web - DT LIVE
=> https://odysee.com/@DistroTube:2/tails-tor-and-the-dark-web-dt-live:6

TITLE: Social Media Causes Mental Illness
=> https://odysee.com/@DistroTube:2/social-media-causes-mental-illness:1

TITLE: Reborn OS Installation and First Look
=> https://odysee.com/@DistroTube:2/reborn-os-installation-and-first-look:8

TITLE: Bryan Lunduke's Decision to Lock Content Behind Patreon Paywall
=> https://odysee.com/@DistroTube:2/bryan-lunduke-s-decision-to-lock-content:a

TITLE: Playing a Little 0 A.D. - DT LIVE
=> https://odysee.com/@DistroTube:2/playing-a-little-0-a-d-dt-live:d

TITLE: Installing Gentoo Linux in Virtualbox (Part 2)
=> https://odysee.com/@DistroTube:2/installing-gentoo-linux-in-virtualbox-2:e

TITLE: Installing Gentoo Linux in Virtualbox (Part 1)
=> https://odysee.com/@DistroTube:2/installing-gentoo-linux-in-virtualbox:1

TITLE: Installing Arch Linux in Virtualbox - DT LIVE
=> https://odysee.com/@DistroTube:2/installing-arch-linux-in-virtualbox-dt:c

TITLE: Live May 13, 2018 - Malware Found in Ubuntu Snaps
=> https://odysee.com/@DistroTube:2/live-may-13-2018-malware-found-in-ubuntu:3

TITLE: Day 30 - Pianobar, Toot and the End of the Challenge
=> https://odysee.com/@DistroTube:2/day-30-pianobar-toot-and-the-end-of-the:c

TITLE: Manjaro Openbox Install and Review
=> https://odysee.com/@DistroTube:2/manjaro-openbox-install-and-review:d

TITLE: Deleting Twitter and Joining Mastodon
=> https://odysee.com/@DistroTube:2/deleting-twitter-and-joining-mastodon:c

TITLE: Day 27 - Switching from Termite to URXVT
=> https://odysee.com/@DistroTube:2/day-27-switching-from-termite-to-urxvt:9

TITLE: on the NVIDIA 1060
=> https://odysee.com/@DistroTube:2/live-may-6-2018-cs-go-on-the-nvidia-1060:d

TITLE: The Linux Directory Structure Simply Explained
=> https://odysee.com/@DistroTube:2/the-linux-directory-structure-simply:2

TITLE: Day 25 of Tiling Window Managers and Terminal Apps
=> https://odysee.com/@DistroTube:2/day-25-of-tiling-window-managers-and:9

TITLE: Deleting Facebook and Joining Diaspora
=> https://odysee.com/@DistroTube:2/deleting-facebook-and-joining-diaspora:e

TITLE: Obscure Window Manager Project - Fluxbox
=> https://odysee.com/@DistroTube:2/obscure-window-manager-project-fluxbox:1

TITLE: Live May 3, 2018 - My 200th Video and a New Graphics Card
=> https://odysee.com/@DistroTube:2/live-may-3-2018-my-200th-video-and-a-new:b

TITLE: Fedora 28 Workstation Install and Review
=> https://odysee.com/@DistroTube:2/fedora-28-workstation-install-and-review:e

TITLE: How to Give Back to Linux and the Free and Open Source Communities
=> https://odysee.com/@DistroTube:2/how-to-give-back-to-linux-and-the-free:1

TITLE: Lubuntu 18.04 "Bionic Beaver" Install and Review
=> https://odysee.com/@DistroTube:2/lubuntu-18-04-bionic-beaver-install-and:a

TITLE: Kubuntu 18.04 "Bionic Beaver" Install and Review
=> https://odysee.com/@DistroTube:2/kubuntu-18-04-bionic-beaver-install-and:b

TITLE: Ubuntu MATE 18.04 "Bionic Beaver" Install and Review
=> https://odysee.com/@DistroTube:2/ubuntu-mate-18-04-bionic-beaver-install:1

TITLE: Ubuntu 18.04 LTS "Bionic Beaver" Install and Review
=> https://odysee.com/@DistroTube:2/ubuntu-18-04-lts-bionic-beaver-install:a

TITLE: Xubuntu 18.04 "Bionic Beaver" Install and Review
=> https://odysee.com/@DistroTube:2/xubuntu-18-04-bionic-beaver-install-and:5

TITLE: Ubuntu Budgie 18.04 "Bionic Beaver" Install and Review
=> https://odysee.com/@DistroTube:2/ubuntu-budgie-18-04-bionic-beaver:1

TITLE: MagpieOS 2.2 GNOME Install and Review
=> https://odysee.com/@DistroTube:2/magpieos-2-2-gnome-install-and-review:a

TITLE: Stallman Proposes New Laws Against Data Accumulation
=> https://odysee.com/@DistroTube:2/stallman-proposes-new-laws-against-data:4

TITLE: Namib GNU/Linux First Impression Install and Review
=> https://odysee.com/@DistroTube:2/namib-gnu-linux-first-impression-install:4

TITLE: Live Apr 20, 2018 - OBS streaming problems in Arch
=> https://odysee.com/@DistroTube:2/live-apr-20-2018-obs-streaming-problems:e

TITLE: Trisquel GNU/Linux 8.0 Install and Review
=> https://odysee.com/@DistroTube:2/trisquel-gnu-linux-8-0-install-and:e

TITLE: Live Apr 17, 2018 - Installing DEs and WMs on Arch
=> https://odysee.com/@DistroTube:2/live-apr-17-2018-installing-des-and-2:1

TITLE: Live Apr 17, 2018 - Installing DEs and WMs on Arch
=> https://odysee.com/@DistroTube:2/live-apr-17-2018-installing-des-and-wms:0

TITLE: Sabayon 18.05 GNOME Install and Review
=> https://odysee.com/@DistroTube:2/sabayon-18-05-gnome-install-and-review:2

TITLE: Day 6 - Twitch Curses and WOPR
=> https://odysee.com/@DistroTube:2/day-6-twitch-curses-and-wopr:0

TITLE: Redcore Linux 1803 Installation and Review
=> https://odysee.com/@DistroTube:2/redcore-linux-1803-installation-and:2

TITLE: lobal Offensive on Arch Linux
=> https://odysee.com/@DistroTube:2/live-apr-14-2018-counter-strike-global:f

TITLE: Day 4 - Googler, Rainbowstream and Haxor News
=> https://odysee.com/@DistroTube:2/day-4-googler-rainbowstream-and-haxor:f

TITLE: Day 2 - Newsboat RSS Newsfeed Reader
=> https://odysee.com/@DistroTube:2/day-2-newsboat-rss-newsfeed-reader:0

TITLE: Arch Linux Installation Tutorial
=> https://odysee.com/@DistroTube:2/arch-linux-installation-tutorial:6

TITLE: Day 1 - Terminal Color Scheme and Mutt Email
=> https://odysee.com/@DistroTube:2/day-1-terminal-color-scheme-and-mutt:9

TITLE: 30 Day Challenge - Tiling Windows and TUI Programs
=> https://odysee.com/@DistroTube:2/30-day-challenge-tiling-windows-and-tui:4

TITLE: Obscure Window Manager Project - Awesome WM
=> https://odysee.com/@DistroTube:2/obscure-window-manager-project-awesome:4

TITLE: Free and Open Source Games on Linux
=> https://odysee.com/@DistroTube:2/free-and-open-source-games-on-linux:1

TITLE: Live Apr 6, 2018 - Installing BunsenLabs 'Helium' Beta 1
=> https://odysee.com/@DistroTube:2/live-apr-6-2018-installing-bunsenlabs:9

TITLE: Making GNOME Tolerable in Fedora 28
=> https://odysee.com/@DistroTube:2/making-gnome-tolerable-in-fedora-28:d

TITLE: Five Reasons Linux is Superior to Windows
=> https://odysee.com/@DistroTube:2/five-reasons-linux-is-superior-to:7

TITLE: A Brief Look at the Beta Release of Fedora 28
=> https://odysee.com/@DistroTube:2/a-brief-look-at-the-beta-release-of:0

TITLE: A Look at GParted Live 0.31.0-1
=> https://odysee.com/@DistroTube:2/a-look-at-gparted-live-0-31-0-1:7

TITLE: Windows 10 First Impression Install and Review
=> https://odysee.com/@DistroTube:2/windows-10-first-impression-install-and:f

TITLE: Simplifying the Shell with Bash Aliases
=> https://odysee.com/@DistroTube:2/simplifying-the-shell-with-bash-aliases:f

TITLE: Live Mar 29, 2018 - Updating all my VMs
=> https://odysee.com/@DistroTube:2/live-mar-29-2018-updating-all-my-vms:c

TITLE: The Pacman Package Manager in Arch Linux
=> https://odysee.com/@DistroTube:2/the-pacman-package-manager-in-arch-linux:7

TITLE: Etcher USB/SD Image Writer
=> https://odysee.com/@DistroTube:2/etcher-usb-sd-image-writer:4

TITLE: First Impression of the NixOS Installation Procedure
=> https://odysee.com/@DistroTube:2/first-impression-of-the-nixos:0

TITLE: Shark Linux First Impression Install and Review
=> https://odysee.com/@DistroTube:2/shark-linux-first-impression-install-and:5

TITLE: Live Mar 25, 2018 - Playing with Calculate Linux XFCE
=> https://odysee.com/@DistroTube:2/live-mar-25-2018-playing-with-calculate:d

TITLE: Freestyle Rap and the Blue Yeti Mic
=> https://odysee.com/@DistroTube:2/freestyle-rap-and-the-blue-yeti-mic:d

TITLE: Play Video Games and Watch Movies with Telnet
=> https://odysee.com/@DistroTube:2/play-video-games-and-watch-movies-with:8

TITLE: Linux Sucks For The New User
=> https://odysee.com/@DistroTube:2/linux-sucks-for-the-new-user:8

TITLE: My AntiX Experience After Three Days
=> https://odysee.com/@DistroTube:2/my-antix-experience-after-three-days:c

TITLE: GhostBSD First Impression Install and Review
=> https://odysee.com/@DistroTube:2/ghostbsd-first-impression-install-and:1

TITLE: Live Mar 20, 2018 - Playing with AntiX and JWM
=> https://odysee.com/@DistroTube:2/live-mar-20-2018-playing-with-antix-and:2

TITLE: Just Installed AntiX 17.1 - My First Video
=> https://odysee.com/@DistroTube:2/just-installed-antix-17-1-my-first-video:e

TITLE: Richard Stallman on Pedophilia
=> https://odysee.com/@DistroTube:2/richard-stallman-on-pedophilia:e

TITLE: Made my decision.  Installing AntiX 17 on my production machine.
=> https://odysee.com/@DistroTube:2/made-my-decision-installing-antix-17-on:9

TITLE: MX 17.1 Installation and Review
=> https://odysee.com/@DistroTube:2/mx-17-1-installation-and-review:6

TITLE: Rode PSA1 Scissor Arm and VocalBeat Windscreen for Blue Yeti
=> https://odysee.com/@DistroTube:2/rode-psa1-scissor-arm-and-vocalbeat:9

TITLE: Quit Installing Arch on Family and Friend's Computers
=> https://odysee.com/@DistroTube:2/quit-installing-arch-on-family-and:5

TITLE: Neptune 5.0 First Impression Install and Review
=> https://odysee.com/@DistroTube:2/neptune-5-0-first-impression-install-and:3

TITLE: lobal Offensive
=> https://odysee.com/@DistroTube:2/live-mar-14-2018-counter-strike-global:f

TITLE: Logitech c922x Pro Stream Webcam
=> https://odysee.com/@DistroTube:2/logitech-c922x-pro-stream-webcam:1

TITLE: Scion Linux Beta First Impression Install & Review
=> https://odysee.com/@DistroTube:2/scion-linux-beta-first-impression:1

TITLE: Distrohopped on the Laptop.  Installed Solus!
=> https://odysee.com/@DistroTube:2/distrohopped-on-the-laptop-installed:6

TITLE: Live Mar 11, 2018 - Installing Pardus 17.2 Deepin in Virtualbox
=> https://odysee.com/@DistroTube:2/live-mar-11-2018-installing-pardus-17-2:c

TITLE: Time to distrohop! What should I install?
=> https://odysee.com/@DistroTube:2/time-to-distrohop-what-should-i-install:a

TITLE: Obscure Window Manager Project - Xmonad
=> https://odysee.com/@DistroTube:2/obscure-window-manager-project-xmonad:6

TITLE: ArcoLinuxD (ArchMergeD) - Installing Awesome Window Manager
=> https://odysee.com/@DistroTube:2/arcolinuxd-archmerged-installing-awesome:e

TITLE: Ubuntu 18.04 Minimal Install Versus Full Install - Dueling VMs
=> https://odysee.com/@DistroTube:2/ubuntu-18-04-minimal-install-versus-full:b

TITLE: The Linux Community is Toxic!  Let's Patch This.
=> https://odysee.com/@DistroTube:2/the-linux-community-is-toxic-let-s-patch:6

TITLE: KDE Neon First Impression Install & Review
=> https://odysee.com/@DistroTube:2/kde-neon-first-impression-install-review:d

TITLE: RTV - Reddit Terminal Viewer
=> https://odysee.com/@DistroTube:2/rtv-reddit-terminal-viewer:7

TITLE: Live Mar 3, 2018 - Obscure Window Manager Project -TWM
=> https://odysee.com/@DistroTube:2/live-mar-3-2018-obscure-window-manager:9

TITLE: Windows Does It Better Than Linux
=> https://odysee.com/@DistroTube:2/windows-does-it-better-than-linux:3

TITLE: The APT Package Manager in Debian and Ubuntu
=> https://odysee.com/@DistroTube:2/the-apt-package-manager-in-debian-and:b

TITLE: Sabayon MATE Daily Snapshot Mar 1, 2018
=> https://odysee.com/@DistroTube:2/sabayon-mate-daily-snapshot-mar-1-2018:6

TITLE: ArchLabs 2018.02 Install & Review
=> https://odysee.com/@DistroTube:2/archlabs-2018-02-install-review:c

TITLE: The Big Seven Linux Distributions
=> https://odysee.com/@DistroTube:2/the-big-seven-linux-distributions:4

TITLE: Behind the Scenes Look - What do I use to create my videos?
=> https://odysee.com/@DistroTube:2/behind-the-scenes-look-what-do-i-use-to:b

TITLE: Live Feb 25, 2018 - Goofing around in Qtile, installing CLI apps
=> https://odysee.com/@DistroTube:2/live-feb-25-2018-goofing-around-in-qtile:e

TITLE: Cava, aafire and unixporn
=> https://odysee.com/@DistroTube:2/cava-aafire-and-unixporn:7

TITLE: Calculate Linux 17 12 2 MATE Install & Review
=> https://odysee.com/@DistroTube:2/calculate-linux-17-12-2-mate-install:4

TITLE: Linux Non-Gamer Attempting Counter Strike Global Offensive
=> https://odysee.com/@DistroTube:2/linux-non-gamer-attempting-counter:b

TITLE: Obscure Window Manager Project - Qtile
=> https://odysee.com/@DistroTube:2/obscure-window-manager-project-qtile:c

TITLE: Welcome to DistroTube - Help This Channel Grow
=> https://odysee.com/@DistroTube:2/welcome-to-distrotube-help-this-channel:3

TITLE: Other Useful Terminal Commands - lolcat, toilet, ponysay and more!
=> https://odysee.com/@DistroTube:2/other-useful-terminal-commands-lolcat:0

TITLE: A Quick Glance at Kali Linux
=> https://odysee.com/@DistroTube:2/a-quick-glance-at-kali-linux:5

TITLE: Live Feb 20, 2018 - Updating some VMs and discussing Linux-type stuff
=> https://odysee.com/@DistroTube:2/live-feb-20-2018-updating-some-vms-and:e

TITLE: Bluestar Linux First Impression Install & Review
=> https://odysee.com/@DistroTube:2/bluestar-linux-first-impression-install:9

TITLE: Antergos GNOME Install & Review
=> https://odysee.com/@DistroTube:2/antergos-gnome-install-review:7

TITLE: Obscure Window Manager Project - PekWM
=> https://odysee.com/@DistroTube:2/obscure-window-manager-project-pekwm:0

TITLE: The G in GNOME is silent and Linux is just a kernel
=> https://odysee.com/@DistroTube:2/the-g-in-gnome-is-silent-and-linux-is:a

TITLE: Nitrux Linux 1.0.8 First Impression Install & Review
=> https://odysee.com/@DistroTube:2/nitrux-linux-1-0-8-first-impression:8

TITLE: Live Feb 11, 2018 - Obscure Window Manager Project - Openbox
=> https://odysee.com/@DistroTube:2/live-feb-11-2018-obscure-window-manager:4

TITLE: Some Useful Terminal Commands - apropos, history, column, yes, fortune, cowsay
=> https://odysee.com/@DistroTube:2/some-useful-terminal-commands-apropos:5

TITLE: Netrunner Rolling 2018.01 First Impression Install & Review
=> https://odysee.com/@DistroTube:2/netrunner-rolling-2018-01-first:5

TITLE: Obscure Window Manager Project - JWM
=> https://odysee.com/@DistroTube:2/obscure-window-manager-project-jwm:6

TITLE: SystemRescueCd - A Linux System Rescue CD/DVD/USB
=> https://odysee.com/@DistroTube:2/systemrescuecd-a-linux-system-rescue-cd:9

TITLE: Live Stream Feb 4, 2018 - Updating all my VMs rather than watching SuperBowl
=> https://odysee.com/@DistroTube:2/live-stream-feb-4-2018-updating-all-my:2

TITLE: Q4OS 3 1 Testing First Impression Install & Review
=> https://odysee.com/@DistroTube:2/q4os-3-1-testing-first-impression:e

TITLE: How Do I Run My Windows Programs on Linux?
=> https://odysee.com/@DistroTube:2/how-do-i-run-my-windows-programs-on:f

TITLE: Zorin OS 12 2 Ultimate First Impression Install & Review
=> https://odysee.com/@DistroTube:2/zorin-os-12-2-ultimate-first-impression:e

TITLE: Obscure Window Manager Project - FVWM
=> https://odysee.com/@DistroTube:2/obscure-window-manager-project-fvwm:1

TITLE: Redcore Linux 1801 Install & Review - Improving With Each Release
=> https://odysee.com/@DistroTube:2/redcore-linux-1801-install-review:5

TITLE: The Obscure Window Manager Project
=> https://odysee.com/@DistroTube:2/the-obscure-window-manager-project:f

TITLE: Live Stream Jan 27, 2018 - New User Install Linux Lite and Openbox
=> https://odysee.com/@DistroTube:2/live-stream-jan-27-2018-new-user-install:2

TITLE: Live Stream Announcement Jan 27, 2018
=> https://odysee.com/@DistroTube:2/live-stream-announcement-jan-27-2018:4

TITLE: Blue Yeti Blackout Edition USB Microphone on Linux
=> https://odysee.com/@DistroTube:2/blue-yeti-blackout-edition-usb:b

TITLE: Elementary OS "Loki" Install & Review
=> https://odysee.com/@DistroTube:2/elementary-os-loki-install-review:4

TITLE: My First Stream - YouTube Community Guideline Strike - 90 Day Ban
=> https://odysee.com/@DistroTube:2/my-first-stream-youtube-community:e

TITLE: DistroTube Live Stream Jan 24, 2018 - Windows Users Who Can't Escape
=> https://odysee.com/@DistroTube:2/distrotube-live-stream-jan-24-2018:6

TITLE: Switching from KDE back to Openbox. And a channel milestone!
=> https://odysee.com/@DistroTube:2/switching-from-kde-back-to-openbox-and-a:a

TITLE: Manjaro i3 Edition Install & Review
=> https://odysee.com/@DistroTube:2/manjaro-i3-edition-install-review:2

TITLE: SolydX 201801 First Impression Install & Review
=> https://odysee.com/@DistroTube:2/solydx-201801-first-impression-install:f

TITLE: My Manjaro KDE Journey, After Three Weeks
=> https://odysee.com/@DistroTube:2/my-manjaro-kde-journey-after-three-weeks:b

TITLE: YouTube Sent Me A Termination Letter. New Monetization Policy.
=> https://odysee.com/@DistroTube:2/youtube-sent-me-a-termination-letter-new:a

TITLE: MakuluLinux 14 LinDoz First Impression Install & Review
=> https://odysee.com/@DistroTube:2/makululinux-14-lindoz-first-impression:a

TITLE: Virtualbox Guest Additions To Be Included In Linux Kernel
=> https://odysee.com/@DistroTube:2/virtualbox-guest-additions-to-be:7

TITLE: SparkyLinux 5.2 LXQt First Impresion Install & Review
=> https://odysee.com/@DistroTube:2/sparkylinux-5-2-lxqt-first-impresion:c

TITLE: Mabox Linux 17.02 First Impression Install & Review
=> https://odysee.com/@DistroTube:2/mabox-linux-17-02-first-impression:d

TITLE: Listing some of the best Linux Youtubers in 2018. Who did I miss?
=> https://odysee.com/@DistroTube:2/listing-some-of-the-best-linux-youtubers:e

TITLE: Pop!_OS First Impression Install & Review
=> https://odysee.com/@DistroTube:2/pop-os-first-impression-install-review:4

TITLE: Gecko Linux 423 CInnamon Install & Review
=> https://odysee.com/@DistroTube:2/gecko-linux-423-cinnamon-install-review:8

TITLE: How to setup Openbox on a minimal install of Debian
=> https://odysee.com/@DistroTube:2/how-to-setup-openbox-on-a-minimal:f

TITLE: My Manjaro KDE Journey, After Ten Days
=> https://odysee.com/@DistroTube:2/my-manjaro-kde-journey-after-ten-days:f

TITLE: Siduction LXDE 2018.1.0 Install & Review
=> https://odysee.com/@DistroTube:2/siduction-lxde-2018-1-0-install-review:3

TITLE: Linux Lite 3.8 Beta Install & Review
=> https://odysee.com/@DistroTube:2/linux-lite-3-8-beta-install-review:1

TITLE: Quick Update on Void Linux - Changing Mirrors and Octoxbps
=> https://odysee.com/@DistroTube:2/quick-update-on-void-linux-changing:b

TITLE: My Manjaro KDE Journey, After Five Days
=> https://odysee.com/@DistroTube:2/my-manjaro-kde-journey-after-five-days:b

TITLE: Void Linux First Impression Install & Review
=> https://odysee.com/@DistroTube:2/void-linux-first-impression-install:2

## Video Categories

=> ../videos/2017-videos.gmi Videos from 2017
=> ../videos/2018-videos.gmi Videos from 2018
=> ../videos/2019-videos.gmi Videos from 2019
=> ../videos/2020-videos.gmi Videos from 2020
=> ../videos/2021-videos.gmi Videos from 2021
