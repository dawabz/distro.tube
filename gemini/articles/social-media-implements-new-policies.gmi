```
     __ __       _______ _______ _______ ___ _______ ___     _______ _______ 
 .--|  |  |_    |   _   |   _   |       |   |   _   |   |   |   _   |   _   |
 |  _  |   _|   |.  1   |.  l   |.|   | |.  |.  1___|.  |   |.  1___|   1___|
 |_____|____|   |.  _   |.  _   `-|.  |-|.  |.  |___|.  |___|.  __)_|____   |
                |:  |   |:  |   | |:  | |:  |:  1   |:  1   |:  1   |:  1   |
                |::.|:. |::.|:. | |::.| |::.|::.. . |::.. . |::.. . |::.. . |
                `--- ---`--- ---' `---' `---`-------`-------`-------`-------'
                                                                             
```

# Social Media Sites Implement New Policies To Combat Wrong-Think
By Derek Taylor at July 20, 2020

Recently, I noticed that several of the big social media sites are starting to implement new policies to help combat some of the bad, toxic people that hangout on those social networking sites. The following was taken from one of the big social site's new policies:

## Insensitivity Monitoring
Our advanced algorithms will monitor all conversations for certain keywords and phrases that we deem as bigoted, sexist, racist or mean-spirited.

## Hate Speech Inference
Our algorithms are able to recognize certain speech patterns and terminolgy, that may not get caught by our Insensitity Monitoring, and extrapolate the racist intentions behind those words.

## Political Party & Religious Affiliation
All members and guests on this site are asked to identify with a political party and a religious affiliation. While some people will choose to give false information, no one is exempt from this rule. Therefore, our algorithms will assign the correct party and church affiliation to any user if we feel that the user has not been truthful based on his account history.

## Audio & Video Validation
All users of this site must have a working microphone and camera which will be used for voice recognition and speech pattern monitoring. This is used to validate that you are, in fact, the person that you claim to be, and that you actually hold the opinions that you espouse.

## Automatic Reporting
All violations of our Code of Conduct will be automatically reported to the National Database of Deplorable Persons. This online database is made publicly available and can be used by employers as an aid in making certain decisions including job offers, promotions and/or termination.

## Is this really happening? No, not yet.
Now, the above policies are not real. They are fake. But a few months or even a few weeks might be all we have until some of the major social networks actually start implementing policies such as the ones I foretold above. Facebook, Twitter, Google. They could implement this stuff tomorrow if they chose to do so. They certainly have the infrastructure already in place.

And over 3 billion people around the world (almost half the global population) would allow such policies to be implemented. Why? Because those billions of people are so addicted to social media that they would allow themselves to be taken advantage of by the social network if the choice was between that and not using that social network.

In fact, billions of people have already made that choice. Look at the privacy and security scandals involving Facebook and Twitter. How many users have those sites lost due some of their immoral and often illegal practices? Hardly any. These people are spending an average of two hours every day sharing, liking, tweeting and updating their status. They can't imagine a world without the social network.

In fact, I think this "insensitivity monitoring" is probably already a thing. When you have so many people spending hours a day typing on social media, or messaging on their phones, or doing voice chats on Discord and Skype--do you think that this information isn't being collected and that some of what you say and type isn't being flagged as potential threats?

## We have allowed this to go too far.
We already have social networks that ban people for inferring the wrong meaning of their words. Twitter started banning every user that was flagged for violent content which included anyone who used phrases like "kill myself" or "cut myself" and "they deserve to be shot". Apparently, any time such a phrase is used, it must be a serious threat of violence towards yourself or another person; it can't be a joke.

We've also seen social sites ban people for trying to circumvent the policy against certain bad words. So replacing some letters with asterisks or just abbreviating a bad word or bad phrase will also trigger the ban hammer.

And with the current climate, I think we are ripe for the taking here. I think if the social networks wanted to implement this complete and total 1984-style big brother code of conduct, people will go along with it, because right now everyone wants to fight racism and anti-semeticism and bigotry. The fight to eradicate all forms of "hate-speech" has never been stronger.

But who determines what is and is not "hate-speech"? After all, it's more than just the words on a screen. It's the context. And you can't eliminate this stuff anyway. People will simply adjust their speech and develop new terminology, so the social network will have to keep adding new words and phrases to ban. So the hate-mongerers will keep evolving their terminology, and the social sites will keep banning words. Until the entire spoken word is deemed as hate-speech and we all start communicating with hand signals. But be careful with that one finger, that will get you banned.
