#!/usr/bin/env bash

# An array of all files in the 'mandb' directory
readarray -t manfiles < <(find ../mandb/man3 -iname "*.*")

# For each file, convert to from 'man' to 'org'.
for file in "${manfiles[@]}"; do
    if grep '\.so .*' "$file";then
        name=$(echo "$file" | awk -F / '{print $NF}')
        redirect_name=$(grep '\.so .*' ../mandb/man3/"$name" | cut -d ' ' -f 2-)
        pandoc -o ../man-org/man3-sourced/"$name".org -f man -t org "$file" && \
            printf "\nInformation about %s is found in manpage for: [%s]" "${name}" "[../${redirect_name}][${redirect_name}]" >> ../man-org/man3-sourced/"$name".org && \
            echo "$name.org created with (.so) substitution." || echo "Error converting $file."

    else
        echo "Skipped creating $name"
    fi
 
done
