#!/usr/bin/env bash

readarray -t ends_with_html < <(find ../man-org/ -type f -iname "*.html")

for x in "${ends_with_html[@]}"; do
   rm $x
   echo "$x has been deleted."
done
