#+TITLE: DT Videos
#+DESCRIPTION: DistroTube Videos From 2021
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

#+begin_example
     __ __       _______ _______ _______ _______
 .--|  |  |_    |       |   _   |       |       |
 |  _  |   _|   |___|   |.  |   |___|   |___|   |
 |_____|____|    /  ___/|.  |   |/  ___/ /  ___/
                |:  1  \|:  1   |:  1  \|:  1  \
                |::.. . |::.. . |::.. . |::.. . |
                `-------`-------`-------`-------'
#+end_example

* DistroTube Videos from 2022
DT's videos are hosted on YouTube and on Odysee.  The links on this page are Odysee links.  Videos are sorted in reverse order.
+ [[https://odysee.com/@DistroTube:2/install-arch-linux-the-easy-way-with-the:3][Install Arch Linux The Easy Way With The Official Install Script]]
+ [[https://odysee.com/@DistroTube:2/a-first-look-at-ubuntu-unity-22.04-(yes,:0][A First Look At Ubuntu Unity 22.04 (Yes, Unity Lives!)]]
+ [[https://odysee.com/@DistroTube:2/do-you-want-a-cloud-operating-system:c][Do You Want A Cloud Operating System? (SPOILER: I Don't!)]]
+ [[https://odysee.com/@DistroTube:2/doom-emacs-on-day-one-(learn-these:f][Doom Emacs On Day One (Learn These Things FIRST!)]]
+ [[https://odysee.com/@DistroTube:2/openarena-is-an-open-source-quake-like:9][OpenArena Is An Open Source Quake Like Shooter]]
+ [[https://odysee.com/@DistroTube:2/linux-distros-fail-at-educating-their:c][Linux Distros Fail At Educating Their Users]]
+ [[https://odysee.com/@DistroTube:2/solved-pacman-wouldn't-let-me-run-an:0][SOLVED: Pacman Wouldn't Let Me Run An Update!]]
+ [[https://odysee.com/@DistroTube:2/a-first-look-at-pop!_os-22.04-lts:4][A First Look At Pop!_OS 22.04 LTS]]
+ [[https://odysee.com/@DistroTube:2/my-config-files-may-not-work-for-you:e][My Config Files May Not Work For You]]
+ [[https://odysee.com/@DistroTube:2/is-ubuntu-22.04-the-best-ubuntu-in-years:f][Is Ubuntu 22.04 The Best Ubuntu In Years?]]
+ [[https://odysee.com/@DistroTube:2/how-to-be-a-linux-user:3][How To Be A Linux User]]
+ [[https://odysee.com/@DistroTube:2/you-don't-choose-your-linux-distro.-it:b][You Don't Choose Your Linux Distro. It Chooses You!]]
+ [[https://odysee.com/@DistroTube:2/essential-linux-commands-uname,:8][Essential Linux Commands - uname, hostname, basename, dirname & logname]]
+ [[https://odysee.com/@DistroTube:2/a-chat-with-darkxero,-creator-of:b][A Chat With DarkXero, Creator of XeroLinux]]
+ [[https://odysee.com/@DistroTube:2/emacs-has-builtin-chat-)-those-other:3][Emacs Has Builtin Chat :) Those Other IDEs Don't :(]]
+ [[https://odysee.com/@DistroTube:2/fact-checking-common-myths-about-open:f][Fact Checking Common Myths About Open Source Software]]
+ [[https://odysee.com/@DistroTube:2/the-5-things-that-taught-me-the-most:7][The 5 Things That Taught Me The Most About Linux]]
+ [[https://odysee.com/@DistroTube:2/are-we-taking-linux-for-granted:5][Are We Taking Linux For Granted?]]
+ [[https://odysee.com/@DistroTube:2/git-annex-is-the-coolest-program-you've:8][Git Annex Is The Coolest Program You've Never Heard Of]]
+ [[https://odysee.com/@DistroTube:2/hey,-dt!-what's-your-thoughts-on:0]["Hey, DT! What's Your Thoughts On Elementary OS?" (And Other Questions)]]
+ [[https://odysee.com/@DistroTube:2/youtube's-unfair-treatment-pushes:2][YouTube's Unfair Treatment Pushes Creators To Leave For Odysee]]
+ [[https://odysee.com/@DistroTube:2/what-do-windows-user-think-about-linux:7][What Do Windows User Think About Linux? Let's Ask Them!]]
+ [[https://odysee.com/@DistroTube:2/can-this-non-developer-create-his-own:d][Can This Non-Developer Create His Own GTK App? (Haskell & Python)]]
+ [[https://odysee.com/@DistroTube:2/you-can't-afford-not-to-see-this-movie!:2][You Can't Afford NOT To See This Movie! (Save Yourself and Your Data)]]
+ [[https://odysee.com/@DistroTube:2/i-need-more-audio-jacks-on-my-computer:b][I Need More Audio Jacks On My Computer]]
+ [[https://odysee.com/@DistroTube:2/how-to-autostart-programs-on-linux:e][How To Autostart Programs On Linux]]
+ [[https://odysee.com/@DistroTube:2/should-linux-mint-debian-edition-be-the:7][Should Linux Mint "Debian" Edition Be The "Main" Edition?]]
+ [[https://odysee.com/@DistroTube:2/you-must-be-to-do-,-and-do-to-have:2][You Must "Be" To "Do", And "Do" To "Have"]]
+ [[https://odysee.com/@DistroTube:2/let's-talk,-just-you-and-me-dt-live!:d][Let's Talk, Just You And Me - DT LIVE!]]
+ [[https://odysee.com/@DistroTube:2/keep-your-snaps-and-flatpaks,-i'll-take:f][Keep Your Snaps and Flatpaks, I'll Take AppImages!]]
+ [[https://odysee.com/@DistroTube:2/information-wants-to-be-free:5][Information Wants To Be Free]]
+ [[https://odysee.com/@DistroTube:2/unboxing-silver-play-button-and-the:e][Unboxing Silver Play Button And The Journey To Get Here]]
+ [[https://odysee.com/@DistroTube:2/essential-linux-commands-cat,-tac-and:2][Essential Linux Commands - Cat, Tac and Tee]]
+ [[https://odysee.com/@DistroTube:2/why-is-there-no-software-for-linux!!!:2][Why Is There No Software For Linux!!!]]
+ [[https://odysee.com/@DistroTube:2/manjaro-is-nice.-manjaro-with-dtos-even:0][Manjaro Is Nice. Manjaro With DTOS? Even Better!]]
+ [[https://odysee.com/@DistroTube:2/stormos-is-a-beginner-friendly-arch:a][StormOS Is A Beginner Friendly Arch Based Distro]]
+ [[https://odysee.com/@DistroTube:2/want-to-run-a-script-remember-the-dot:a][Want To Run A Script? Remember The Dot Slash (./) !]]
+ [[https://odysee.com/@DistroTube:2/easily-run-windows-apps-on-linux-with:4][Easily Run Windows Apps On Linux With Bottles]]
+ [[https://odysee.com/@DistroTube:2/hey,-dt!-what-would-your-company-look:0]["Hey, DT! What Would Your Company Look Like?" (And Other Questions)]]
+ [[https://odysee.com/@DistroTube:2/geany-text-editor-for-windows,-mac-linux:f][Geany Text Editor For Windows, Mac & Linux]]
+ [[https://odysee.com/@DistroTube:2/emacs-plugins-that-impressed-me:3][Emacs Plugins That Impressed Me]]
+ [[https://odysee.com/@DistroTube:2/is-the-hate-for-snap-packages-warranted:2][Is The Hate For Snap Packages Warranted?]]
+ [[https://odysee.com/@DistroTube:2/people-that-say-linux-is-hard-make-me:d][People That Say "Linux Is Hard" Make Me Laugh]]
+ [[https://odysee.com/@DistroTube:2/20,000-page-static-website-written-in:2][20,000 Page Static Website Written In Org Mode]]
+ [[https://odysee.com/@DistroTube:2/tools-for-renaming-files-in-linux:7][Tools For Renaming Files In Linux]]
+ [[https://odysee.com/@DistroTube:2/trick-out-your-terminal-with-shell-color:d][Trick Out Your Terminal With Shell Color Scripts]]
+ [[https://odysee.com/@DistroTube:2/stop-confusing-potential-new-linux-users:9][Stop Confusing Potential New Linux Users]]
+ [[https://odysee.com/@DistroTube:2/bash-script-for-listening-to-online:3][Bash Script For Listening To Online Radio]]
+ [[https://odysee.com/@DistroTube:2/commands-to-shutdown-and-reboot-on-linux:c][Commands To Shutdown And Reboot On Linux]]
+ [[https://odysee.com/@DistroTube:2/xerolinux.-is-it-just-about-the-eyecandy:d][XeroLinux. Is It Just About The Eyecandy?]]
+ [[https://odysee.com/@DistroTube:2/experimenting-with-a-two-camera-setup:4][Experimenting With A Two Camera Setup (Not Real Content!)]]
+ [[https://odysee.com/@DistroTube:2/make-money-selling-open-source-software:4][Make Money Selling Open Source Software?]]
+ [[https://odysee.com/@DistroTube:2/new-to-linux-learn-the-jargon!:2][New To Linux? Learn The Jargon!]]
+ [[https://odysee.com/@DistroTube:2/hey,-dt!-would-you-use-windows-if-open:6]["Hey, DT! Would You Use Windows If Open Source?" And Other Questions.]]
+ [[https://odysee.com/@DistroTube:2/a-new-release-of-slackware!-worth-the:4][A New Release of Slackware! Worth The Wait?]]
+ [[https://odysee.com/@DistroTube:2/is-adblock-piracy-is-adblock-illegal:5][Is Adblock Piracy? Is Adblock Illegal?]]
+ [[https://odysee.com/@DistroTube:2/lock-your-terminal-sessions-with-vlock:0][Lock Your Terminal Sessions With Vlock]]
+ [[https://odysee.com/@DistroTube:2/linuxfx-brings-everything-wrong-with:6][Linuxfx Brings Everything Wrong With Windows To Linux]]
+ [[https://odysee.com/@DistroTube:2/the-worst-types-of-youtube-comments:3][The Worst Types of YouTube Comments]]
+ [[https://odysee.com/@DistroTube:2/nuclear-music-streaming-app-for-windows,:1][Nuclear Music Streaming App For Windows, Mac, Linux]]
+ [[https://odysee.com/@DistroTube:2/shell-aliases-every-linux-user-needs:4][Shell Aliases Every Linux User Needs]]
+ [[https://odysee.com/@DistroTube:2/five-screenshot-applications-for-linux:1][Five Screenshot Applications For Linux]]
+ [[https://odysee.com/@DistroTube:2/the-off-topic-live-stream-(no-linux:d][The Off-Topic Live Stream (No Linux Allowed!) - DT LIVE]]
+ [[https://odysee.com/@DistroTube:2/sauerbraten-is-an-insanely-fun-first:b][Sauerbraten Is An Insanely Fun First Person Shooter]]
+ [[https://odysee.com/@DistroTube:2/top-five-arch-based-linux-distros-2022:0][Top Five Arch-Based Linux Distros 2022]]
+ [[https://odysee.com/@DistroTube:2/why-do-windows-users-think-linux-users:0][Why Do Windows Users Think Linux Users Are Weird]]
+ [[https://odysee.com/@DistroTube:2/you-don't-own-your-movies,-music,-books,:5][You Don't Own Your Movies, Music, Books, Games (DRM Is Evil!)]]
+ [[https://odysee.com/@DistroTube:2/gecko-linux-takes-opensuse-to-the-next:8][Gecko Linux Takes OpenSUSE To The Next Level]]
+ [[https://odysee.com/@DistroTube:2/a-youtube-studio-tour-2022-(warning-way:6][A YouTube Studio Tour 2022 (Warning: Way Too Long!)]]
+ [[https://odysee.com/@DistroTube:2/useless-use-of-cat-isn't-useless:c][Useless Use Of Cat Isn't Useless]]
+ [[https://odysee.com/@DistroTube:2/hey,-dt!-which-distro-should-i-use-which:5][Hey, DT! Which Distro Should I Use? Which Music Player?]]
+ [[https://odysee.com/@DistroTube:2/so-many-linux-terminal-commands-do-the:b][So Many Linux Terminal Commands Do The Same Thing]]
+ [[https://odysee.com/@DistroTube:2/linux-mint-still-the-best-distro-to:a][Linux Mint Still The Best Distro To Convert Windows Users]]
+ [[https://odysee.com/@DistroTube:2/vim-and-emacs-are-the-most-important:1][Vim And Emacs Are The Most Important Skills You Should Learn]]
+ [[https://odysee.com/@DistroTube:2/ubuntu-no-longer-seen-as-viable-gaming:4][Ubuntu No Longer Seen As Viable Gaming Distro (I Blame Snaps)]]
+ [[https://odysee.com/@DistroTube:2/the-most-popular-linux-distro-that:e][The Most Popular Linux Distro That Doesn't Exist]]
+ [[https://odysee.com/@DistroTube:2/ten-linux-predictions-for-2022:7][Ten Linux Predictions For 2022]]

* More Videos From DistroTube
+ [[file:2022-videos.org][Videos from 2022]]
+ [[file:2021-videos.org][Videos from 2021]]
+ [[file:2020-videos.org][Videos from 2020]]
+ [[file:2019-videos.org][Videos from 2019]]
+ [[file:2018-videos.org][Videos from 2018]]
+ [[file:2017-videos.org][Videos from 2017]]

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
